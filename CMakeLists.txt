# Main ArmarX CMake file

cmake_minimum_required(VERSION 2.8.12)

if(${CMAKE_MAJOR_VERSION}.${CMAKE_MINOR_VERSION} EQUAL 3.9)
    if(${CMAKE_PATCH_VERSION} EQUAL 0 OR ${CMAKE_PATCH_VERSION} EQUAL 1)
        message(FATAL_ERROR "cmake versions 3.9.{0,1} are not usable with armarx")
    endif()
endif()

# Set base directory for custom cmake stuff
get_filename_component(ArmarXCore_CMAKE_DIR "${PROJECT_SOURCE_DIR}/etc/cmake" ABSOLUTE)
get_filename_component(ArmarXCore_TEMPLATES_DIR "${PROJECT_SOURCE_DIR}/etc/templates" ABSOLUTE)

include(${ArmarXCore_CMAKE_DIR}/ArmarXProject.cmake)

set(ARMARX_ENABLE_AUTO_CODE_FORMATTING TRUE)

# Start of the ArmarXCore project
armarx_project(ArmarXCore)

add_subdirectory(etc)
add_subdirectory(source)

list(APPEND CPACK_DEBIAN_PACKAGE_DEPENDS "zeroc-ice")
list(APPEND CPACK_DEBIAN_PACKAGE_DEPENDS "cmake")

# Generate stuff for export and installation (see Installation.cmake)
install_project()

add_subdirectory(scenarios)
