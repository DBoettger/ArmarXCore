  /*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Core
* @author     Kai Welke <welke@kit.edu>
* @copyright  2012 Humanoids Group, HIS, KIT
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <ArmarXCore/interface/observers/ObserverInterface.ice>
#include <ArmarXCore/interface/observers/ChannelRefBase.ice>

module armarx
{
    exception InvalidTimerException extends UserException
    {
    };

    exception InvalidCounterException extends UserException
    {
    };

    interface SystemObserverInterface extends ObserverInterface
    {
        // timers
        ChannelRefBase startTimer(string timerName) throws InvalidTimerException;
        void resetTimer(ChannelRefBase timer) throws InvalidTimerException;
        void pauseTimer(ChannelRefBase timer) throws InvalidTimerException;
        void unpauseTimer(ChannelRefBase timer) throws InvalidTimerException;
        void removeTimer(ChannelRefBase timer);


        // counters
        ChannelRefBase startCounter(int initialValue, string counterName) throws InvalidCounterException;
        int incrementCounter(ChannelRefBase counter) throws InvalidCounterException;
        int decrementCounter(ChannelRefBase counter) throws InvalidCounterException;
        void resetCounter(ChannelRefBase counter) throws InvalidCounterException;
        void setCounter(ChannelRefBase counter, int counterValue) throws InvalidCounterException;
        void removeCounter(ChannelRefBase counter);

    };
};

