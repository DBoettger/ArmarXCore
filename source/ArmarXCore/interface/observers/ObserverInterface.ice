/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Core
* @author     Kai Welke <welke@kit.edu>
* @copyright  2011 Humanoids Group, HIS, KIT
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once
[["suppress-warning:deprecated"]]
#include <ArmarXCore/interface/observers/VariantBase.ice>
#include <ArmarXCore/interface/observers/Event.ice>
#include <ArmarXCore/interface/observers/ConditionCheckBase.ice>
#include <ArmarXCore/interface/observers/ChannelRefBase.ice>

module armarx
{

    exception InvalidChannelException extends UserException
    {
        string channelName;
    };

    exception InvalidDatafieldException extends UserException
    {
        string channelName;
        string datafieldName;
    };

    exception DatafieldExistsAlreadyException extends UserException
    {
        string channelName;
        string datafieldName;
    };

    interface ObserverInterface;

    class ChannelRefBase extends VariantDataClass
    {
        bool initialized;

        string observerName;
        string channelName;
        Ice::StringSeq datafieldNames;

        ObserverInterface*	observerProxy;
    };



    ["cpp:virtual"]
    class DatafieldRefBase extends VariantDataClass
    {
        string datafieldName;
        ChannelRefBase channelRef;

        ["cpp:const"]
        bool getBool();
        ["cpp:const"]
        int getInt();
        ["cpp:const"]
        float getFloat();
        ["cpp:const"]
        double getDouble();
        ["cpp:const"]
        string getString();

    };
    sequence<DatafieldRefBase> DatafieldRefList;

    dictionary<string, TimedVariantBase> StringTimedVariantBaseMap;


    class DatafieldFilterBase
    {
//        TimeVariantBaseMap dataHistory;
        int windowFilterSize = 11;
        int minSampleTimeDelta = 0;
        VariantBase filteredValue;
        void update(long timestamp, VariantBase value);
        ["cpp:const"]
        VariantBase getValue();
        ["cpp:const"]
        VariantBase calculate();
        ["cpp:const"]
        ParameterTypeList getSupportedTypes();
        ["cpp:const"]
        bool checkTypeSupport(int variantType);

        ["cpp:const"] idempotent
        StringFloatDictionary getProperties();
        void setProperties(StringFloatDictionary newProperties);

    };

    struct DataFieldRegistryEntry
    {
        DatafieldRefBase identifier;
        string typeName;
        string description;
        VariantBase value;
    };

    dictionary<string, DataFieldRegistryEntry> DataFieldRegistry;

    struct ChannelRegistryEntry
    {
        string name;
        string description;
        DataFieldRegistry dataFields;
        ConditionCheckRegistry conditionChecks;
        bool initialized;
    };

    dictionary<string, ChannelRegistryEntry> ChannelRegistry;
    dictionary<long, DataFieldRegistry> ChannelHistory;

    sequence<DataFieldIdentifierBase> DataFieldIdentifierBaseList;

    interface ObserverInterface
    {
        ["cpp:const"] idempotent
        string getObserverName();

        CheckIdentifier installCheck(CheckConfiguration config)  throws InvalidConditionException;
        void removeCheck(CheckIdentifier id);

        ["cpp:const"] idempotent
        TimedVariantBase getDataField(DataFieldIdentifierBase identifier) throws InvalidDatafieldException, InvalidChannelException;
        ["cpp:const"] idempotent
        DatafieldRefBase getDataFieldRef(DataFieldIdentifierBase identifier) throws InvalidDatafieldException, InvalidChannelException;
        ["cpp:const"] idempotent
        TimedVariantBase getDatafieldByName(string channelName, string datafieldName) throws InvalidDatafieldException, InvalidChannelException;
        ["cpp:const"] idempotent
        DatafieldRefBase getDatafieldRefByName(string channelName, string datafieldName) throws InvalidDatafieldException, InvalidChannelException;
        TimedVariantBaseList getDataFields(DataFieldIdentifierBaseList identifiers) throws InvalidDatafieldException, InvalidChannelException;
        ["cpp:const"] idempotent
        StringTimedVariantBaseMap getDatafieldsOfChannel(string channelName) throws InvalidChannelException;
        ["cpp:const"] idempotent
        ChannelRegistryEntry getChannel(string channelName) throws InvalidChannelException;
        ChannelRegistry getAvailableChannels(bool includeMetaChannels);
        StringConditionCheckMap getAvailableChecks();

        ["cpp:const"] idempotent
        ChannelHistory getChannelHistory(string channelName, float timeStepMs);
        ["cpp:const"] idempotent
        ChannelHistory getPartialChannelHistory(string channelName, long startTimestamp, long endTimestamp, float timeStepMs);
        ["cpp:const"] idempotent
        TimedVariantBaseList getDatafieldHistory(string channelName, string datafieldName, float timeStepMs);
        ["cpp:const"] idempotent
        TimedVariantBaseList getPartialDatafieldHistory(string channelName, string datafieldName, long startTimestamp, long endTimestamp, float timeStepMs);

        ["cpp:const"]
        bool existsChannel(string channelName);
        ["cpp:const"]
        bool existsDataField(string channelName, string datafieldName);

        DatafieldRefBase createFilteredDatafield(DatafieldFilterBase filter, DatafieldRefBase datafielRef) throws UnsupportedTypeException, InvalidDatafieldException, InvalidChannelException;
        DatafieldRefBase createNamedFilteredDatafield(string filterDatafieldName, DatafieldFilterBase filter, DatafieldRefBase datafielRef) throws UnsupportedTypeException, InvalidDatafieldException, InvalidChannelException;
        void removeFilteredDatafield(DatafieldRefBase datafielRef);
    };

    interface DebugObserverInterface extends ObserverInterface
    {
        void setDebugDatafield(string channelName, string datafieldName, VariantBase value);
        void setDebugChannel(string channelName, StringVariantBaseMap valueMap);
        void removeDebugChannel(string channelname);
        void removeDebugDatafield(string channelName, string datafieldName);
        void removeAllChannels();
    };
};

