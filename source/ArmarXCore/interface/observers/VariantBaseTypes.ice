/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Core
* @author     Kai Welke <welke@kit.edu>
* @copyright  2011 Humanoids Group, HIS, KIT
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once
[["suppress-warning:deprecated"]]

#include <ArmarXCore/interface/core/BasicTypes.ice>
#include <ArmarXCore/interface/core/UserException.ice>
#include <ArmarXCore/interface/observers/Serialization.ice>

module armarx
{

    class VariantData
    {
    };



    class InvalidVariantData extends VariantData
    {
    };

    class IntVariantData extends VariantData
    {
        int n = 0;
    };

    const int zeroInt = 0; // needed to init float values due to bug in SLICE for initialization of floats
    class FloatVariantData extends VariantData
    {
        float f = zeroInt;
    };

    class DoubleVariantData extends VariantData
    {
        double d = zeroInt;
    };

    class StringVariantData extends VariantData
    {
        string s;
    };

    class BoolVariantData extends VariantData
    {
        bool b = false;
    };

    class VariantDataClass extends VariantData implements Serializable
    {
        ["cpp:const"]
        VariantDataClass clone();
        ["cpp:const"]
        string output();
        ["cpp:const"]
        int getType();

        /**
     * @brief Function to validate the sanity of the data.
     *
     * If your variant does not this, just return true;
     * @return true, if valid.
     */
        bool validate();

    };



};

