/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Core
* @author     Mirko Waechter < waechter at kit dot edu >
* @copyright  2011 Humanoids Group, HIS, KIT
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <ArmarXCore/interface/observers/ObserverInterface.ice>

module armarx
{
    enum PassType
    {
        Highpass,
        Lowpass,
    };

    ["cpp:virtual"]
    class MinFilterBase extends DatafieldFilterBase
    {

    };

    ["cpp:virtual"]
    class MaxFilterBase extends DatafieldFilterBase
    {

    };


    ["cpp:virtual"]
    class MedianFilterBase extends DatafieldFilterBase
    {

    };

    ["cpp:virtual"]
    class AverageFilterBase extends DatafieldFilterBase
    {

    };


    ["cpp:virtual"]
    class GaussianFilterBase extends DatafieldFilterBase
    {
        int filterSizeInMs = 200;
    };

    ["cpp:virtual"]
    class DerivationFilterBase extends DatafieldFilterBase
    {

    };    

    ["cpp:virtual"]
    class ButterworthFilterBase extends DatafieldFilterBase
    {
         /// rez amount, from sqrt(2) to ~ 0.1
        float resonance = 1;

        float frequency = 10;
        int sampleRate = 100;
        PassType filterPassType = Lowpass;

        float c;
        float a1;  float a2;  float a3;  float b1;  float b2;
    };


};

