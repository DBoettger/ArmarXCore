/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Core
* @author     Kai Welke <welke@kit.edu>
* @copyright  2011 Humanoids Group, HIS, KIT
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <ArmarXCore/interface/observers/VariantBase.ice>
#include <ArmarXCore/interface/observers/TermImplBase.ice>
#include <ArmarXCore/interface/observers/Event.ice>

module armarx
{
    module checks
    {
        const string valid = "valid";
        const string updated = "updated";
        const string changed = "changed";
        const string equals = "equals";
        const string approx = "approx";
        const string smaller = "smaller";
        const string larger = "larger";
        const string inrange = "inrange";
    };

    /**
     * Interfaces for condition checks
     */
    sequence<int> ParameterTypeList;
    struct SupportedType
    {
        int dataFieldType;
        ParameterTypeList parameterTypes;
    };

    sequence<SupportedType> SupportedTypeList;

    struct CheckIdentifier
    {
        int uniqueId;
        string channelName;
        string observerName;
    };

    class ConditionCheckBase
    {
        CheckConfiguration configuration;
        SupportedTypeList supportedTypes;
        int numberParameters;
        bool fulFilled;
    };

    dictionary<int,ConditionCheckBase> ConditionCheckRegistry;
    dictionary<string,ConditionCheckBase> StringConditionCheckMap;
};

