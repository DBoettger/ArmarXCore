/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Core
* @author     Kai Welke <welke@kit.edu>
* @copyright  2011 Humanoids Group, HIS, KIT
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once
[["suppress-warning:deprecated"]]
#include <ArmarXCore/interface/core/BasicTypes.ice>
#include <ArmarXCore/interface/core/UserException.ice>
#include <ArmarXCore/interface/observers/Serialization.ice>
#include <ArmarXCore/interface/observers/VariantBaseTypes.ice>
#include <ArmarXCore/interface/observers/VariantContainers.ice>

module armarx
{



  ["cpp:virtual"]
  class VariantBase//extends VariantContainerBase
  {
    void setType(int type);
    void setInt(int i) throws InvalidTypeException;
    void setFloat(float f) throws InvalidTypeException;
    void setDouble(double f) throws InvalidTypeException;
    void setString(string s) throws InvalidTypeException;
    void setBool(bool b) throws InvalidTypeException;

    ["cpp:const"]
    idempotent int getInt() throws InvalidTypeException;
    ["cpp:const"]
    idempotent float getFloat() throws InvalidTypeException;
    ["cpp:const"]
    idempotent double getDouble() throws InvalidTypeException;
    ["cpp:const"]
    idempotent string getString() throws InvalidTypeException;
    ["cpp:const"]
    idempotent bool getBool() throws InvalidTypeException;

    ["cpp:const"]
    bool validate();

    ["cpp:const"]
    idempotent int getType();
    ["cpp:const"]
    idempotent string getTypeName();
    ["cpp:const"]
    idempotent bool getInitialized();

    VariantData data;
    ["protected"]
    int typeId = -1;
  };

  dictionary<string, VariantBase> StringVariantBaseMap;
  dictionary<string, StringVariantBaseMap> StringToStringVariantBaseMapMap;
  sequence<VariantBase> VariantBaseList;
  sequence<VariantBase> VariantBaseSeq;//we commonly have the suffix seq for sequences

  ["cpp:virtual"]
  class TimedVariantBase extends VariantBase
  {
      ["cpp:const"]
      long getTimestamp();
      ["protected"]
      long timestamp = 0;
  };
  sequence<TimedVariantBase> TimedVariantBaseList;

  ["cpp:virtual"]
  class SingleVariantBase extends VariantContainerBase
  {
      ["cpp:const"]
      VariantBase getElementBase();
      ["protected"]
      VariantBase element;
  };

};

