#pragma once

#include <type_traits>
#include <boost/mpl/range_c.hpp>

#include <boost/fusion/adapted.hpp>
#include <boost/fusion/include/at.hpp>
#include <boost/fusion/include/for_each.hpp>

#include <Ice/StreamHelpers.h>

namespace armarx
{
    namespace IceSerialization
    {
        template <class T>
        using MinWireSize = std::integral_constant<int, Ice::StreamableTraits<T>::minWireSize>;

        template <class T>
        using FixedLength = std::integral_constant<bool, Ice::StreamableTraits<T>::fixedLength>;

        template <class T>
        using Helper = std::integral_constant<::Ice::StreamHelperCategory, Ice::StreamableTraits<T>::helper>;

        template <class T>
        using KnownCategory = std::integral_constant<bool,  Helper<T>::value != ::Ice::StreamHelperCategoryUnknown>;
    }
}
//fusion extractors
namespace armarx
{
    namespace IceSerialization
    {
        namespace detail
        {
            template <class T>
            using rem_cvr = typename std::remove_reference<typename std::remove_cv<T>::type>::type;
            template <class Seq, std::size_t I>
            using type = rem_cvr<typename boost::fusion::result_of::at_c<Seq, I>::type>;

            template<std::size_t V> using IConst = std::integral_constant<int , V>;
            template<std::size_t V> using BConst = std::integral_constant<bool, V>;

            template<class Seq, std::size_t I> struct mwsz         : IConst < MinWireSize  < type < Seq, I - 1 >>::value +  mwsz < Seq, I - 1 >::value > {};
            template<class Seq               > struct mwsz<Seq, 0> : IConst<0                                                         > {};

            template<class Seq, std::size_t I> struct fxln         : BConst < FixedLength  < type < Seq, I - 1 >>::value && fxln < Seq, I - 1 >::value > {};
            template<class Seq>                struct fxln<Seq, 0> : BConst<true                                                      > {};

            template<class Seq, std::size_t I> struct knwn         : BConst < KnownCategory < type < Seq, I - 1 >>::value && knwn < Seq, I - 1 >::value > {};
            template<class Seq>                struct knwn<Seq, 0> : BConst<true                                                      > {};
        }

        template <class Seq>
        struct fusion_struct_size
        {
        public:
            static constexpr std::size_t number_of_elements = boost::fusion::result_of::size<Seq>::type::value;

            template<std::size_t I> using   MinWireSize_I = ::armarx::IceSerialization::MinWireSize  <detail::type<Seq, I>>;
            template<std::size_t I> using KnownCategory_I = ::armarx::IceSerialization::KnownCategory<detail::type<Seq, I>>;
            template<std::size_t I> using   FixedLength_I = ::armarx::IceSerialization::FixedLength  <detail::type<Seq, I>>;
            template<std::size_t I> using type_i          = ::armarx::IceSerialization::detail::type<Seq, I>;

                                    using MinWireSize     = ::armarx::IceSerialization::detail::mwsz<Seq, number_of_elements>;
                                    using KnownCategory   = ::armarx::IceSerialization::detail::knwn<Seq, number_of_elements>;
                                    using FixedLength     = ::armarx::IceSerialization::detail::fxln<Seq, number_of_elements>;
        };
    }
}
