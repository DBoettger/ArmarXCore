#pragma once

#include "Meta.h"

#define ARMARX_MAKE_STRUCT_ADAPTOR_StreamWriter(Type)                                                  \
    namespace Ice                                                                                      \
    {                                                                                                  \
        template<class S>                                                                              \
        struct StreamWriter<Type, S>                                                                   \
        {                                                                                              \
            using Helper = ::armarx::IceSerialization::fusion_struct_size<Type>;                       \
            static_assert (Helper::KnownCategory::value, "Not all members of '" #Type "' are known");  \
            struct Write                                                                               \
            {                                                                                          \
                S* s;                                                                                  \
                template<typename T>                                                                   \
                void operator()(const T& t) const                                                      \
                {                                                                                      \
                    s->write(t);                                                                       \
                }                                                                                      \
            };                                                                                         \
            static void write(S* s, const Type& v)                                                     \
            {                                                                                          \
                boost::fusion::for_each(v, Write{s});                                                  \
            }                                                                                          \
        };                                                                                             \
    }
