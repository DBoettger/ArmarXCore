#pragma once

#include <Ice/StreamHelpers.h>

#include <Eigen/Core>

namespace Ice
{
    template<class Scalar, int Rows, int Cols>
    struct StreamableTraits<::Eigen::Matrix<Scalar, Rows, Cols>>
    {
        static constexpr StreamHelperCategory helper = StreamHelperCategoryStruct;
        static constexpr int minWireSize = Rows * Cols * sizeof(Scalar);
        static constexpr bool fixedLength = true;
    };
    template<class Scalar, int Rows>
    struct StreamableTraits <::Eigen::Matrix < Scalar, Rows, -1 >>
    {
        static constexpr StreamHelperCategory helper = StreamHelperCategoryStruct;
        static constexpr int minWireSize = Rows * sizeof(Scalar);
        static constexpr bool fixedLength = false;
    };
    template<class Scalar, int Cols>
    struct StreamableTraits <::Eigen::Matrix < Scalar, -1, Cols >>
    {
        static constexpr StreamHelperCategory helper = StreamHelperCategoryStruct;
        static constexpr int minWireSize = Cols * sizeof(Scalar);
        static constexpr bool fixedLength = false;
    };
    template<class Scalar>
    struct StreamableTraits <::Eigen::Matrix < Scalar, -1, -1 >>
    {
        static constexpr StreamHelperCategory helper = StreamHelperCategoryStruct;
        static constexpr int minWireSize = sizeof(Scalar);
        static constexpr bool fixedLength = false;
    };
}
