#pragma once

#include <Ice/StreamHelpers.h>

#include <Eigen/Core>

namespace Ice
{
    template<class Scalar, int Rows, int Cols, class S>
    struct StreamReader<::Eigen::Matrix<Scalar, Rows, Cols>, S>
    {
        static void read(S* str, ::Eigen::Matrix<Scalar, Rows, Cols>& m)
        {
            for (int col = 0; col < Cols; ++col)
            {
                for (int row = 0; row < Rows; ++row)
                {
                    str->read(m(col, row));
                }
            }
        }
    };
    template<class Scalar, int Rows, class S>
    struct StreamReader<::Eigen::Matrix<Scalar, Rows, 1   >, S>
    {
        static void read(S* str, ::Eigen::Matrix<Scalar, Rows, 1>& m)
        {
            for (int row = 0; row < Rows; ++row)
            {
                str->read(m(row));
            }
        }
    };
    template<class Scalar, int Cols, class S>
    struct StreamReader<::Eigen::Matrix<Scalar, 1   , Cols>, S>
    {
        static void read(S* str, ::Eigen::Matrix<Scalar, 1, Cols>& m)
        {
            for (int col = 0; col < Cols; ++col)
            {
                str->read(m(col));
            }
        }
    };

    template<class Scalar, int Rows, class S>
    struct StreamReader <::Eigen::Matrix < Scalar, Rows, -1  >, S >
    {
        static void read(S* str, ::Eigen::Matrix < Scalar, Rows, -1 > & m)
        {
            int cols;
            str->read(cols);
            for (int col = 0; col < cols; ++col)
            {
                for (int row = 0; row < Rows; ++row)
                {
                    str->read(m(col, row));
                }
            }
        }
    };
    template<class Scalar, int Cols, class S>
    struct StreamReader <::Eigen::Matrix < Scalar, -1  , Cols >, S >
    {
        static void read(S* str, ::Eigen::Matrix < Scalar, -1, Cols > & m)
        {
            int rows;
            str->read(rows);
            for (int col = 0; col < Cols; ++col)
            {
                for (int row = 0; row < rows; ++row)
                {
                    str->read(m(col, row));
                }
            }
        }
    };
    template<class Scalar, class S>
    struct StreamReader <::Eigen::Matrix < Scalar, -1  , -1  >, S >
    {
        static void read(S* str, ::Eigen::Matrix < Scalar, -1, -1 > & m)
        {
            int rows;
            int cols;
            str->read(rows);
            str->read(cols);
            for (int col = 0; col < cols; ++col)
            {
                for (int row = 0; row < rows; ++row)
                {
                    str->read(m(col, row));
                }
            }
        }
    };

    template<class Scalar, class S>
    struct StreamReader <::Eigen::Matrix < Scalar, 1   , -1  >, S >
    {
        static void read(S* str, ::Eigen::Matrix < Scalar, 1, -1 > & m)
        {
            int cols;
            str->read(cols);
            for (int col = 0; col < cols; ++col)
            {
                str->read(m(col));
            }
        }
    };
    template<class Scalar, class S>
    struct StreamReader <::Eigen::Matrix < Scalar, -1  , 1   >, S >
    {
        static void read(S* str, ::Eigen::Matrix < Scalar, -1, 1 > & m)
        {
            int rows;
            str->read(rows);
            for (int row = 0; row < rows; ++row)
            {
                str->read(m(row));
            }
        }
    };
}
