#pragma once

#include <Ice/StreamHelpers.h>

#include <Eigen/Core>

namespace Ice
{
    template<class Scalar, int Rows, int Cols, class S>
    struct StreamWriter<::Eigen::Matrix<Scalar, Rows, Cols>, S>
    {
        static void write(S* str, const ::Eigen::Matrix<Scalar, Rows, Cols>& m)
        {
            for (int col = 0; col < Cols; ++col)
            {
                for (int row = 0; row < Rows; ++row)
                {
                    str->write(m(col, row));
                }
            }
        }
    };
    template<class Scalar, int Rows, class S>
    struct StreamWriter<::Eigen::Matrix<Scalar, Rows, 1   >, S>
    {
        static void write(S* str, const ::Eigen::Matrix<Scalar, Rows, 1>& m)
        {
            for (int row = 0; row < Rows; ++row)
            {
                str->write(m(row));
            }
        }
    };
    template<class Scalar, int Cols, class S>
    struct StreamWriter<::Eigen::Matrix<Scalar, 1   , Cols>, S>
    {
        static void write(S* str, const ::Eigen::Matrix<Scalar, 1, Cols>& m)
        {
            for (int col = 0; col < Cols; ++col)
            {
                str->write(m(col));
            }
        }
    };

    template<class Scalar, int Rows, class S>
    struct StreamWriter <::Eigen::Matrix < Scalar, Rows, -1  >, S >
    {
        static void write(S* str, const ::Eigen::Matrix < Scalar, Rows, -1 > & m)
        {
            str->write(m.cols());
            for (int col = 0; col < m.cols(); ++col)
            {
                for (int row = 0; row < Rows; ++row)
                {
                    str->write(m(col, row));
                }
            }
        }
    };
    template<class Scalar, int Cols, class S>
    struct StreamWriter <::Eigen::Matrix < Scalar, -1  , Cols >, S >
    {
        static void write(S* str, const ::Eigen::Matrix < Scalar, -1, Cols > & m)
        {
            str->write(m.rows());
            for (int col = 0; col < Cols; ++col)
            {
                for (int row = 0; row < m.rows(); ++row)
                {
                    str->write(m(col, row));
                }
            }
        }
    };
    template<class Scalar, class S>
    struct StreamWriter <::Eigen::Matrix < Scalar, -1  , -1  >, S >
    {
        static void write(S* str, const ::Eigen::Matrix < Scalar, -1, -1 > & m)
        {
            str->write(m.rows());
            str->write(m.cols());
            for (int col = 0; col < m.cols(); ++col)
            {
                for (int row = 0; row < m.rows(); ++row)
                {
                    str->write(m(col, row));
                }
            }
        }
    };

    template<class Scalar, class S>
    struct StreamWriter <::Eigen::Matrix < Scalar, 1   , -1  >, S >
    {
        static void write(S* str, const ::Eigen::Matrix < Scalar, 1, -1 > & m)
        {
            str->write(m.cols());
            for (int col = 0; col < m.cols(); ++col)
            {
                str->write(m(col));
            }
        }
    };
    template<class Scalar, class S>
    struct StreamWriter <::Eigen::Matrix < Scalar, -1  , 1   >, S >
    {
        static void write(S* str, const ::Eigen::Matrix < Scalar, -1, 1 > & m)
        {
            str->write(m.rows());
            for (int row = 0; row < m.rows(); ++row)
            {
                str->write(m(row));
            }
        }
    };
}
