/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Core::Statechart
* @author     Mirko Waechter <mirko.waechter@kit.edu>
* @copyright  2011-2012 Humanoids Group, HIS, KIT
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <ArmarXCore/interface/observers/Event.ice>
#include <ArmarXCore/interface/observers/ConditionHandlerInterface.ice>
#include <ArmarXCore/interface/observers/VariantBase.ice>
#include <ArmarXCore/interface/observers/ParameterBase.ice>
#include <ArmarXCore/interface/statechart/StatechartIce.ice>
#include <ArmarXCore/interface/statechart/RemoteStateIce.ice>




module armarx
{
    dictionary<int, string> StateIdNameMap;
    interface RemoteStateOffererInterface
    {
//        EventBase startStatechart(string statename, StringVariantContainerBaseMap inputs);
        int createRemoteStateInstance(string statename,
                            RemoteStateIceBase* remoteState,
                            string parentStateItentifierStr,
                            string instanceName);
        void updateGlobalStateIdRecursive(int stateId, string parentId);
        void callRemoteState(int stateId, StringVariantContainerBaseMap properties);
        bool breakRemoteState(int stateId, EventBase evt);
        void exitRemoteState(int stateId);
        bool breakActiveSubstateRemotely(int stateId, EventBase evt);
        void notifyEventBufferedDueToUnbreakableStateRemote(int stateId, bool eventBuffered);

        StateParameterMap getRemoteInputParameters(string stateName);
        StateParameterMap getRemoteInputParametersById(int stateId);
        StateParameterMap getRemoteOutputParameters(string stateName);
        StateParameterMap getRemoteOutputParametersById(int stateId);
        StateIceBase refetchRemoteSubstates(int stateId);

        void issueEvent(int receivingStateId, EventBase evt);
        void issueEventWithGlobalIdStr(string globalStateIdStr, EventBase evt);
        void removeInstance(int stateId);
        ["cpp:const"]idempotent bool hasSubstatesRemote(string stateName);
        idempotent bool hasActiveSubstateRemote(int stateId);
        StateIceBase getStatechart(string stateName);
        Ice::StringSeq getAvailableStates();
        StateIdNameMap getAvailableStateInstances();
        StateIceBase getStatechartInstance(int stateId);
        StateIceBase getStatechartInstanceByGlobalIdStr(string globalStateIdStr);
        bool isHostOfStateByGlobalIdStr(string globalStateIdStr);
    };

    ["cpp:virtual"]
    class RemoteStateOffererIceBase extends StateIceBase implements RemoteStateOffererInterface
    {
    };
};

