/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Core::Statechart
* @author     Mirko Waechter <mirko.waechter@kit.edu>
* @copyright  2011-2012 Humanoids Group, HIS, KIT
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <ArmarXCore/interface/observers/Event.ice>
#include <ArmarXCore/interface/observers/ConditionHandlerInterface.ice>
#include <ArmarXCore/interface/observers/VariantBase.ice>
#include <ArmarXCore/interface/observers/ParameterBase.ice>
#include <ArmarXCore/interface/statechart/StatechartIce.ice>




module armarx
{
    interface RemoteStateInterface
    {
        ["cpp:const"] idempotent int getRemoteUnbreakableBufferSize();
        ["cpp:const"] idempotent bool getRemoteUnbreakableBufferStati();
        ["cpp:const"] idempotent StateIceBase getParentStateLayout();

        void remoteProcessBufferedEvents();
        void remoteEnqueueEvent(EventBase evt);
        void remoteProcessEvent(EventBase evt, bool buffered);
        void remoteFinalize(StringVariantContainerBaseMap properties, EventBase evt);
    };

    ["cpp:virtual"] class RemoteStateIceBase extends StateIceBase implements RemoteStateInterface
    {
        //! Id of the state that this state is communicating with.
        int remoteStateId;
        //! Name of the Component that this state is communicating with.
        string proxyName;
    };
};

