/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Core::Statechart
* @author     Mirko Waechter <mirko.waechter@kit.edu>
* @copyright  2011-2012 Humanoids Group, HIS, KIT
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <ArmarXCore/interface/observers/Event.ice>
#include <ArmarXCore/interface/observers/TermImplBase.ice>
#include <ArmarXCore/interface/observers/ConditionHandlerInterface.ice>
#include <ArmarXCore/interface/observers/VariantBase.ice>
#include <ArmarXCore/interface/observers/VariantContainers.ice>
#include <ArmarXCore/interface/observers/ParameterBase.ice>



module armarx
{
    enum MappingSource
    {
        eParent,
        eOutput,
        eDataField,
        eEvent,
        eValue,
        eMappingSourcesCount
    };

    class StateParameterIceBase
    {
        VariantContainerBase value;
        VariantContainerBase defaultValue;
        bool optionalParam = false;
        bool set = false;
    };


    dictionary<string, StateParameterIceBase> StateParameterMap;

    dictionary<string,string> Mapping;
    struct MappingEntry
    {
        MappingSource mappingSrc;
        string sourceKey;
        string targetKey;
        VariantContainerBase value;

    };
    dictionary<int, MappingSource> PriorityMap;
    sequence<MappingEntry> MappingList;


    dictionary<MappingSource, StringVariantContainerBaseMap> SourceDictionaryMap;
    class ParameterMappingIceBase;
    class ParameterMappingIceBase
    {
        SourceDictionaryMap sourceDictionaries;
        //Mapping paramMapping;
        MappingList mappings;
        bool useParentsInput = false;
        PriorityMap priorities;
    };



    class StateIceBase;
    class RemoteStateIceBase;
    struct TransitionIceBase
    {
        StateIceBase sourceState;
        EventBase evt;
        StateIceBase destinationState;
        ParameterMappingIceBase mappingToNextStatesInput;
        ParameterMappingIceBase mappingToParentStatesLocal;
        ParameterMappingIceBase mappingToParentStatesOutput;

        /**
         * @brief This flag indicates if this transition should be executed
         * regardless of the current state.
         *
         * So the sourceState member is not
         * used, when this flag is true.
         */
        bool fromAll = false;
    };

    sequence<TransitionIceBase> TransitionTable;
    sequence<StateIceBase> StateList;
    dictionary<int, string> HierarchyMap;

    enum eStateType
    {
        eNormalState,
        eFinalState,
        eRemoteState,
        eDynamicRemoteState,
        eUndefined
    };


    class StateIceBase
    {
        /**
            Name of this state. Should be unique in this hierarchy level.
        **/
        string stateName;
        /**
            Classname of this state. For easier association of a state with it's implementation.
        **/
        string stateClassName;
        eStateType stateType = eNormalState;
        /**
         *  globalStateIdentifier contains a string with all the parent states to the root
         */
        string globalStateIdentifier;
        /**
         *  Holds the status whether this state was initialized.
         * Only after initialization the substates, transition, input/output parameters are created.
         */
        bool initialized = false;
        /**
         *  States whether this state is unbreakable or not.
         * Unbreakable means, a parent state cannot be left while an
         * unbreakable substate has executed onEnter() but not onExit()
         */
        bool unbreakable = false;
        /**
         *  States whether this state has delayed processing of events in parent states.
         * Only for internal purposes.
         */
        bool eventsDelayed = false;
        /**
         *  If set to true, this state will take any parameter of input sources
         * as an input parameter instead of only parameters that were specified
         * specifically beforehand.
         */
        bool greedyInputDictionary = false;
        StateParameterMap inputParameters;
        StateParameterMap localParameters;
        StateParameterMap outputParameters;
        StateList subStateList;
        ParameterMappingIceBase initialStateMapping;
        /** Initial substate. MUST be set, if the state has any substates.

            initStates's OnEnter() is automatically called if this (initState's parent) state is entered.
          **/
        StateIceBase initState;
        /**
            The currently active substate. That means OnEnter() has been called, but not OnExit() or OnBreak()
        **/
        StateIceBase activeSubstate;
        /**  Vector that holds all transition of substates of this state.

         If unbreakable is true, then transitions of parent states of any level are prohibited and these transitions are delayed.
         **/
        TransitionTable transitions;

        TermImplSequence installedConditions;
    };

    interface StatechartInterface
    {
        StateIceBase getStatechart();
    };
};

