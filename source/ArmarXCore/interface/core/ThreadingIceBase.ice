/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Core
* @author     Mirko Waechter < waechter at kit dot edu >
* @copyright  2013 Humanoids Group, HIS, KIT
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <Ice/BuiltinSequences.ice>

module armarx
{

    struct RunningTaskIceBase
    {
        /**
         * @brief Name of the RunningTask instance.
         */
        string name;

        /**
         * @brief Time when this thread was started.  In MicroSeconds.
         */
        long startTime = 0;

        /**
         * @brief Time, when isStopped() was last called.  In MicroSeconds.
         */
        long lastFeedbackTime = 0;

        // state
        bool running;
        bool stopped;
        bool finished;

        int threadId = 0;
        /**
         * @brief Holds a list of workload-floats of this RunningTask.
         *
         * Stores the percentage of cpu time that was used for this thread.
         *
         */
        float workload;
    };



    struct PeriodicTaskIceBase
    {
        /**
         * @brief Name of the PeriodicTask instance.
         */
        string name;

        int intervalMs = 0;
        /**
         * @brief Time when this thread was started. In MicroSeconds.
         */
        long startTime = 0;

        /**
         * @brief Time, when the current cycle has started.  In MicroSeconds.
         */
        long lastCycleStartTime = 0;

        /**
         * @brief Duration of the last cycle.  In MicroSeconds.
         */
        long lastCycleDuration = 0;


        /**
         * @brief Holds a list of workload-floats of this PeriodicTask.
         *
         * Stores the percentage of time that was used for one cycle until the
         * next cycle started.<br/>
         * One value per cycle, but only stores the x-last cycles.
         */
        Ice::FloatSeq workloadList;

        bool shutdown;
        bool running;

        int threadId = 0;

    };

    sequence<RunningTaskIceBase> RunningTaskList;
    sequence<PeriodicTaskIceBase> PeriodicTaskList;

    /**
     * This interface is used to retreive a list of names or proxies of
     * all RunningTaskIceBase or PeriodicTaskIceBase instances.
     */
    interface ThreadListInterface{
        Ice::StringSeq getRunningTaskNames();
        Ice::StringSeq getPeriodicTaskNames();

        double getCpuUsage();

        RunningTaskList getRunningTasks();
        PeriodicTaskList getPeriodicTasks();
    };

};

