/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore
 * @author     Raphael Grimm ( ufdrv at student dot kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */

#pragma once

#include <Ice/Identity.ice>
#include <Ice/Properties.ice>

#include <ArmarXCore/interface/core/UserException.ice>
#include <ArmarXCore/interface/core/ManagedIceObjectDefinitions.ice>

#include <ArmarXCore/interface/core/util/distributed/RemoteHandle/ClientSideRemoteHandleControlBlock.ice>
#include <ArmarXCore/interface/core/UserException.ice>

module armarx
{

    /**
     * @brief Contains the parameters used to create a component.
     */
    struct ComponentParameter
    {
            Ice::PropertyDict prop;
            string configName = "";
            string configDomain = "ArmarX";
    };

    /**
     * @brief Offers an interface to create objects or components.
     * Both, depending on their type, are registered with ice or the \ref armarx::ArmarXManager.
     * Both can either be created until the server shuts down, or until the last handle is deleted by the client.
     */
    interface RemoteObjectNodeInterface
    {
        Object* createPersistentComponent(string componentFactoryName, string registrationName, ComponentParameter params) throws NoSuchComponentFactory;

        ClientSideRemoteHandleControlBlockBase createRemoteHandledComponent(string componentFactoryName, string registrationName, ComponentParameter params) throws NoSuchComponentFactory;

        Object* registerPersistentObjectWithIdentity   (Ice::Identity ident, Object registree);
        ClientSideRemoteHandleControlBlockBase registerRemoteHandledObjectWithIdentity(Ice::Identity ident, Object registree);

        Object* registerPersistentObject(string registrationName, Object registree);
        ClientSideRemoteHandleControlBlockBase registerRemoteHandledObject(string registrationName, Object registree);

        /**
         * @brief Returns the number of cores of the remote object node's machine.
         * (it is only a hint! may be 1 when it has 10 cores)
         * @return The number of cores of the remote object node's machine.
         */
        ["cpp:const"] idempotent long getNumberOfCores               ();
        ["cpp:const"] idempotent long getNumberOfObjects             ();
        ["cpp:const"] idempotent long getNumberOfPersistentObjects   ();
        ["cpp:const"] idempotent long getNumberOfRemoteHandledObjects();

        ["cpp:const"] idempotent Ice::StringSeq getKnownComponentFactories();
    };
    sequence<RemoteObjectNodeInterface*> RemoteObjectNodePrxList;
};
