/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */

#pragma once

#include <type_traits>
#include <tuple>
#include <ostream>

#include <Eigen/Core>

#include <ArmarXCore/interface/core/BasicVectorTypes.h>

namespace armarx
{
    namespace detail
    {
        namespace bvt
        {
            template<class Targ, class Src>
            using is_eigen_assignable = std::integral_constant < bool,
                  Targ::ColsAtCompileTime == Src::ColsAtCompileTime &&
                  Targ::RowsAtCompileTime == Src::RowsAtCompileTime &&
                  std::is_same<typename Targ::Scalar, typename Src::Scalar>::value >;
            template<class Targ, class Src, class Result = void>
            using enable_if_assignable_t = typename std::enable_if<is_eigen_assignable<Targ, Src>::value, Result>::type;

            inline void AllOut(std::ostream&, char) {}
            template<class E0, class...Es>
            inline void AllOut(std::ostream& o, char delim, const E0& e0, const Es...es)
            {
                o << e0 << delim;
                AllOut(o, delim, es...);
            }
        }
    }
}

// The content of this file was generated using cpp and this input (this was done to reduce build time)
#if 0
namespace armarx
{

#define remove_paren(...) __VA_ARGS__

#define make_Operator(Type, cmp, blockl, blockr)                                                                            \
    inline bool operator cmp(const Type& l, const Type& r)                                                                  \
    {                                                                                                                       \
        return std::tie blockl cmp std::tie blockr;                                                                         \
    }

#define make_ostream_Operator(Type, delim, blockr)                                                                          \
    inline std::ostream& operator<<(std::ostream& l, const Type& r)                                                         \
    {                                                                                                                       \
        detail::bvt::AllOut(l, delim, remove_paren blockr);                                                                 \
        return l;                                                                                                           \
    }

#define make_Operators(Type, blockl, blockr)                                                                                \
    make_Operator        (Type, < , blockl, blockr)                                                                         \
    make_Operator        (Type, > , blockl, blockr)                                                                         \
    make_Operator        (Type, ==, blockl, blockr)                                                                         \
    make_Operator        (Type, !=, blockl, blockr)                                                                         \
    make_Operator        (Type, >=, blockl, blockr)                                                                         \
    make_Operator        (Type, <=, blockl, blockr)


#define make_Operators_1_to_6(Pre, Suff)                                                                                    \
    make_Operators(Pre##1##Suff, (l.e0                              ), (r.e0                              ))                \
    make_Operators(Pre##2##Suff, (l.e0, l.e1                        ), (r.e0, r.e1                        ))                \
    make_Operators(Pre##3##Suff, (l.e0, l.e1, l.e2                  ), (r.e0, r.e1, r.e2                  ))                \
    make_Operators(Pre##4##Suff, (l.e0, l.e1, l.e2, l.e3            ), (r.e0, r.e1, r.e2, r.e3            ))                \
    make_Operators(Pre##5##Suff, (l.e0, l.e1, l.e2, l.e3, l.e4      ), (r.e0, r.e1, r.e2, r.e3, r.e4      ))                \
    make_Operators(Pre##6##Suff, (l.e0, l.e1, l.e2, l.e3, l.e4, l.e5), (r.e0, r.e1, r.e2, r.e3, r.e4, r.e5))

#define make_Operator_ostream_1_to_6(Pre, Suff, delim)                                                                      \
    make_ostream_Operator(Pre##1##Suff, delim, (r.e0                              ))                                        \
    make_ostream_Operator(Pre##2##Suff, delim, (r.e0, r.e1                        ))                                        \
    make_ostream_Operator(Pre##3##Suff, delim, (r.e0, r.e1, r.e2                  ))                                        \
    make_ostream_Operator(Pre##4##Suff, delim, (r.e0, r.e1, r.e2, r.e3            ))                                        \
    make_ostream_Operator(Pre##5##Suff, delim, (r.e0, r.e1, r.e2, r.e3, r.e4      ))                                        \
    make_ostream_Operator(Pre##6##Suff, delim, (r.e0, r.e1, r.e2, r.e3, r.e4, r.e5))
    // //////////////////////////////////////////////////////////////////////////////////////////////////////////////////// //
    // /////////////////////////////////////////////////// vector types /////////////////////////////////////////////////// //
    // //////////////////////////////////////////////////////////////////////////////////////////////////////////////////// //

#define make_ToBasicVectorType(EType, BType, ...)                                                                           \
    template<class Derived>                                                                                                 \
    inline detail::bvt::enable_if_assignable_t<remove_paren EType, Derived, BType>                                          \
    ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)                                                                  \
    {                                                                                                                       \
        return {__VA_ARGS__};                                                                                               \
    }

#define make_Vector_ToBasicVectorType(Type, Suff)                                                                           \
    make_ToBasicVectorType((::Eigen::Matrix<Type, 1, 1>), ::armarx::Vector1##Suff, e(0)                              )      \
    make_ToBasicVectorType((::Eigen::Matrix<Type, 2, 1>), ::armarx::Vector2##Suff, e(0), e(1)                        )      \
    make_ToBasicVectorType((::Eigen::Matrix<Type, 3, 1>), ::armarx::Vector3##Suff, e(0), e(1), e(2)                  )      \
    make_ToBasicVectorType((::Eigen::Matrix<Type, 4, 1>), ::armarx::Vector4##Suff, e(0), e(1), e(2), e(3)            )      \
    make_ToBasicVectorType((::Eigen::Matrix<Type, 5, 1>), ::armarx::Vector5##Suff, e(0), e(1), e(2), e(3), e(4)      )      \
    make_ToBasicVectorType((::Eigen::Matrix<Type, 6, 1>), ::armarx::Vector6##Suff, e(0), e(1), e(2), e(3), e(4), e(5))

#define make_ToEigen(EType, BType, ...)                                                                                     \
    inline remove_paren EType ToEigen(const BType& b)                                                                       \
    {                                                                                                                       \
        remove_paren EType m;                                                                                               \
        m << __VA_ARGS__;                                                                                                   \
        return m;                                                                                                           \
    }

#define make_Vector_ToEigen(Type, Suff)                                                                                     \
    make_ToEigen((::Eigen::Matrix<Type, 1, 1>), ::armarx::Vector1##Suff, b.e0)                                              \
    make_ToEigen((::Eigen::Matrix<Type, 2, 1>), ::armarx::Vector2##Suff, b.e0, b.e1)                                        \
    make_ToEigen((::Eigen::Matrix<Type, 3, 1>), ::armarx::Vector3##Suff, b.e0, b.e1, b.e2)                                  \
    make_ToEigen((::Eigen::Matrix<Type, 4, 1>), ::armarx::Vector4##Suff, b.e0, b.e1, b.e2, b.e3)                            \
    make_ToEigen((::Eigen::Matrix<Type, 5, 1>), ::armarx::Vector5##Suff, b.e0, b.e1, b.e2, b.e3, b.e4)                      \
    make_ToEigen((::Eigen::Matrix<Type, 6, 1>), ::armarx::Vector6##Suff, b.e0, b.e1, b.e2, b.e3, b.e4, b.e5)

    // //////////////////////////////////////////////////////////////////////////////////////////////////////////////////// //
    // /////////////////////////////////////////////////// matrix types /////////////////////////////////////////////////// //
    // //////////////////////////////////////////////////////////////////////////////////////////////////////////////////// //
    //unrolls j for the given i
#define make_matrix_unroll_j1(g, e, i, d) g(e(i, 0, d))
#define make_matrix_unroll_j2(g, e, i, d) g(e(i, 0, d), e(i, 1, d))
#define make_matrix_unroll_j3(g, e, i, d) g(e(i, 0, d), e(i, 1, d), e(i, 2, d))
#define make_matrix_unroll_j4(g, e, i, d) g(e(i, 0, d), e(i, 1, d), e(i, 2, d), e(i, 3, d))
#define make_matrix_unroll_j5(g, e, i, d) g(e(i, 0, d), e(i, 1, d), e(i, 2, d), e(i, 3, d), e(i, 4, d))
#define make_matrix_unroll_j6(g, e, i, d) g(e(i, 0, d), e(i, 1, d), e(i, 2, d), e(i, 3, d), e(i, 4, d), e(i, 5, d))

    //unrolls i times the unroll j function
#define make_matrix_unroll_i1_invoke(m, g, e, d) m(g, e, 0, d)
#define make_matrix_unroll_i2_invoke(m, g, e, d) m(g, e, 0, d), m(g, e, 1, d)
#define make_matrix_unroll_i3_invoke(m, g, e, d) m(g, e, 0, d), m(g, e, 1, d), m(g, e, 2, d)
#define make_matrix_unroll_i4_invoke(m, g, e, d) m(g, e, 0, d), m(g, e, 1, d), m(g, e, 2, d), m(g, e, 3, d)
#define make_matrix_unroll_i5_invoke(m, g, e, d) m(g, e, 0, d), m(g, e, 1, d), m(g, e, 2, d), m(g, e, 3, d), m(g, e, 4, d)
#define make_matrix_unroll_i6_invoke(m, g, e, d) m(g, e, 0, d), m(g, e, 1, d), m(g, e, 2, d), m(g, e, 3, d), m(g, e, 4, d), m(g, e, 5, d)

#define make_matrix_unroll_i1(g, e, j, d) make_matrix_unroll_i1_invoke(make_matrix_unroll_j##j, g, e, d)
#define make_matrix_unroll_i2(g, e, j, d) make_matrix_unroll_i2_invoke(make_matrix_unroll_j##j, g, e, d)
#define make_matrix_unroll_i3(g, e, j, d) make_matrix_unroll_i3_invoke(make_matrix_unroll_j##j, g, e, d)
#define make_matrix_unroll_i4(g, e, j, d) make_matrix_unroll_i4_invoke(make_matrix_unroll_j##j, g, e, d)
#define make_matrix_unroll_i5(g, e, j, d) make_matrix_unroll_i5_invoke(make_matrix_unroll_j##j, g, e, d)
#define make_matrix_unroll_i6(g, e, j, d) make_matrix_unroll_i6_invoke(make_matrix_unroll_j##j, g, e, d)

    //do unrolling to get eigen matrix elements to construct a matrix
#define make_matrix_btype_init_e(i, j, name) name(i, j)
#define make_matrix_btype_init_g(...)        {__VA_ARGS__}
#define make_matrix_btype_init_invoke(m, j, name)  m(make_matrix_btype_init_g, make_matrix_btype_init_e, j, name)
#define make_matrix_btype_init(i, j, name) make_matrix_btype_init_invoke(make_matrix_unroll_i##i, j, name)

    //do unrolling to get basis vector type matrix elements
#define make_matrix_btype_elem_e(i, j, name) name.e##i.e##j
#define make_matrix_btype_elem_g(...)        __VA_ARGS__
#define make_matrix_btype_elem_invoke(m, j, name)  m(make_matrix_btype_elem_g, make_matrix_btype_elem_e, j, name)
#define make_matrix_btype_elem(i, j, name) make_matrix_btype_elem_invoke(make_matrix_unroll_i##i, j, name)


#define make_Matrix_ToBasicVectorType_j(Type, Suff, j)                                                                      \
    make_ToBasicVectorType((::Eigen::Matrix<Type, 1, j>), ::armarx::Matrix_1_##j##Suff, make_matrix_btype_init(1, j, e))    \
    make_ToBasicVectorType((::Eigen::Matrix<Type, 2, j>), ::armarx::Matrix_2_##j##Suff, make_matrix_btype_init(2, j, e))    \
    make_ToBasicVectorType((::Eigen::Matrix<Type, 3, j>), ::armarx::Matrix_3_##j##Suff, make_matrix_btype_init(3, j, e))    \
    make_ToBasicVectorType((::Eigen::Matrix<Type, 4, j>), ::armarx::Matrix_4_##j##Suff, make_matrix_btype_init(4, j, e))    \
    make_ToBasicVectorType((::Eigen::Matrix<Type, 5, j>), ::armarx::Matrix_5_##j##Suff, make_matrix_btype_init(5, j, e))    \
    make_ToBasicVectorType((::Eigen::Matrix<Type, 6, j>), ::armarx::Matrix_6_##j##Suff, make_matrix_btype_init(6, j, e))

#define make_Matrix_ToBasicVectorType(Type, Suff)                                                                           \
    make_Matrix_ToBasicVectorType_j(Type, _##Suff, 2)                                                                       \
    make_Matrix_ToBasicVectorType_j(Type, _##Suff, 3)                                                                       \
    make_Matrix_ToBasicVectorType_j(Type, _##Suff, 4)                                                                       \
    make_Matrix_ToBasicVectorType_j(Type, _##Suff, 5)                                                                       \
    make_Matrix_ToBasicVectorType_j(Type, _##Suff, 6)

#define make_Matrix_ToEigen_j(Type, Suff, j)                                                                                \
    make_ToEigen((::Eigen::Matrix<Type, 1, j>), ::armarx::Matrix_1_##j##Suff, make_matrix_btype_elem(1, j, b))              \
    make_ToEigen((::Eigen::Matrix<Type, 2, j>), ::armarx::Matrix_2_##j##Suff, make_matrix_btype_elem(2, j, b))              \
    make_ToEigen((::Eigen::Matrix<Type, 3, j>), ::armarx::Matrix_3_##j##Suff, make_matrix_btype_elem(3, j, b))              \
    make_ToEigen((::Eigen::Matrix<Type, 4, j>), ::armarx::Matrix_4_##j##Suff, make_matrix_btype_elem(4, j, b))              \
    make_ToEigen((::Eigen::Matrix<Type, 5, j>), ::armarx::Matrix_5_##j##Suff, make_matrix_btype_elem(5, j, b))              \
    make_ToEigen((::Eigen::Matrix<Type, 6, j>), ::armarx::Matrix_6_##j##Suff, make_matrix_btype_elem(6, j, b))

#define make_Matrix_ToEigen(Type, Suff)                                                                                     \
    make_Matrix_ToEigen_j(Type, _##Suff, 1)                                                                                 \
    make_Matrix_ToEigen_j(Type, _##Suff, 2)                                                                                 \
    make_Matrix_ToEigen_j(Type, _##Suff, 3)                                                                                 \
    make_Matrix_ToEigen_j(Type, _##Suff, 4)                                                                                 \
    make_Matrix_ToEigen_j(Type, _##Suff, 5)                                                                                 \
    make_Matrix_ToEigen_j(Type, _##Suff, 6)

    // //////////////////////////////////////////////////////////////////////////////////////////////////////////////////// //
    // ////////////////////////////////////////////////// call generators ///////////////////////////////////////////////// //
    // //////////////////////////////////////////////////////////////////////////////////////////////////////////////////// //
#define make_VectorAndMatrixTypes(Type, Suff)                                                                               \
    make_Vector_ToBasicVectorType(Type, Suff)                                                                               \
    make_Matrix_ToBasicVectorType(Type, Suff)                                                                               \
    make_Vector_ToEigen(Type, Suff)                                                                                         \
    make_Matrix_ToEigen(Type, Suff)                                                                                         \
    make_Operator_ostream_1_to_6(::armarx::Vector, Suff, '\t')                                                              \
    make_Operator_ostream_1_to_6(::armarx::Matrix_, _1_##Suff, '\n')                                                        \
    make_Operator_ostream_1_to_6(::armarx::Matrix_, _2_##Suff, '\n')                                                        \
    make_Operator_ostream_1_to_6(::armarx::Matrix_, _3_##Suff, '\n')                                                        \
    make_Operator_ostream_1_to_6(::armarx::Matrix_, _4_##Suff, '\n')                                                        \
    make_Operator_ostream_1_to_6(::armarx::Matrix_, _5_##Suff, '\n')                                                        \
    make_Operator_ostream_1_to_6(::armarx::Matrix_, _6_##Suff, '\n')

    make_VectorAndMatrixTypes(::Ice::Byte, byte)
    make_VectorAndMatrixTypes(::Ice::Short, s)
    make_VectorAndMatrixTypes(::Ice::Int, i)
    make_VectorAndMatrixTypes(::Ice::Long, l)
    make_VectorAndMatrixTypes(::Ice::Float, f)
    make_VectorAndMatrixTypes(::Ice::Double, d)

    //make ops
    make_Operators_1_to_6(::armarx::Vector, f)
    make_Operators_1_to_6(::armarx::Matrix_, _1_f)
    make_Operators_1_to_6(::armarx::Matrix_, _2_f)
    make_Operators_1_to_6(::armarx::Matrix_, _3_f)
    make_Operators_1_to_6(::armarx::Matrix_, _4_f)
    make_Operators_1_to_6(::armarx::Matrix_, _5_f)
    make_Operators_1_to_6(::armarx::Matrix_, _6_f)
    make_Operators_1_to_6(::armarx::Vector, d)
    make_Operators_1_to_6(::armarx::Matrix_, _1_d)
    make_Operators_1_to_6(::armarx::Matrix_, _2_d)
    make_Operators_1_to_6(::armarx::Matrix_, _3_d)
    make_Operators_1_to_6(::armarx::Matrix_, _4_d)
    make_Operators_1_to_6(::armarx::Matrix_, _5_d)
    make_Operators_1_to_6(::armarx::Matrix_, _6_d)

    template<class T, class R = decltype(ToEigen(std::declval<T>()))>
    inline std::vector<R> ToEigen(const std::vector<T>& v)
    {
        std::vector<R> r;
        r.reserve(v.size());
        for (const auto& e : v)
        {
            r.emplace_back(ToEigen(e));
        }
        return r;
    }

    template <
        class S,
        int Row,
        int Col,
        class R = decltype(ToBasicVectorType(std::declval<Eigen::Matrix<S, Row, Col>>()))
        >
    inline std::vector<R>  ToBasicVectorType(const std::vector<Eigen::Matrix<S, Row, Col>>& v)
    {
        std::vector<R> r;
        r.reserve(v.size());
        for (const auto& e : v)
        {
            r.emplace_back(ToBasicVectorType(e));
        }
        return r;
    }

#undef remove_paren
#undef make_Operator
#undef make_ostream_Operator
#undef make_Operators
#undef make_Operators_1_to_6
#undef make_Operator_ostream_1_to_6
#undef make_ToBasicVectorType
#undef make_Vector_ToBasicVectorType
#undef make_ToEigen
#undef make_Vector_ToEigen
#undef make_matrix_unroll_j1
#undef make_matrix_unroll_j2
#undef make_matrix_unroll_j3
#undef make_matrix_unroll_j4
#undef make_matrix_unroll_j5
#undef make_matrix_unroll_j6
#undef make_matrix_unroll_i1_invoke
#undef make_matrix_unroll_i2_invoke
#undef make_matrix_unroll_i3_invoke
#undef make_matrix_unroll_i4_invoke
#undef make_matrix_unroll_i5_invoke
#undef make_matrix_unroll_i6_invoke
#undef make_matrix_unroll_i1
#undef make_matrix_unroll_i2
#undef make_matrix_unroll_i3
#undef make_matrix_unroll_i4
#undef make_matrix_unroll_i5
#undef make_matrix_unroll_i6
#undef make_matrix_btype_init_e
#undef make_matrix_btype_init_g
#undef make_matrix_btype_init_invoke
#undef make_matrix_btype_init
#undef make_matrix_btype_elem_e
#undef make_matrix_btype_elem_g
#undef make_matrix_btype_elem_invoke
#undef make_matrix_btype_elem
#undef make_Matrix_ToBasicVectorType_j
#undef make_Matrix_ToBasicVectorType
#undef make_Matrix_ToEigen_j
#undef make_Matrix_ToEigen
#undef make_VectorAndMatrixTypes
}
#endif
namespace armarx
{

    template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Byte, 1, 1>, Derived, ::armarx::Vector1byte> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {e(0)};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Byte, 2, 1>, Derived, ::armarx::Vector2byte> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {e(0), e(1)};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Byte, 3, 1>, Derived, ::armarx::Vector3byte> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {e(0), e(1), e(2)};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Byte, 4, 1>, Derived, ::armarx::Vector4byte> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {e(0), e(1), e(2), e(3)};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Byte, 5, 1>, Derived, ::armarx::Vector5byte> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {e(0), e(1), e(2), e(3), e(4)};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Byte, 6, 1>, Derived, ::armarx::Vector6byte> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {e(0), e(1), e(2), e(3), e(4), e(5)};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Byte, 1, 2>, Derived, ::armarx::Matrix_1_2_byte> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Byte, 2, 2>, Derived, ::armarx::Matrix_2_2_byte> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1)}, {e(1, 0), e(1, 1)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Byte, 3, 2>, Derived, ::armarx::Matrix_3_2_byte> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1)}, {e(1, 0), e(1, 1)}, {e(2, 0), e(2, 1)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Byte, 4, 2>, Derived, ::armarx::Matrix_4_2_byte> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1)}, {e(1, 0), e(1, 1)}, {e(2, 0), e(2, 1)}, {e(3, 0), e(3, 1)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Byte, 5, 2>, Derived, ::armarx::Matrix_5_2_byte> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1)}, {e(1, 0), e(1, 1)}, {e(2, 0), e(2, 1)}, {e(3, 0), e(3, 1)}, {e(4, 0), e(4, 1)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Byte, 6, 2>, Derived, ::armarx::Matrix_6_2_byte> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1)}, {e(1, 0), e(1, 1)}, {e(2, 0), e(2, 1)}, {e(3, 0), e(3, 1)}, {e(4, 0), e(4, 1)}, {e(5, 0), e(5, 1)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Byte, 1, 3>, Derived, ::armarx::Matrix_1_3_byte> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Byte, 2, 3>, Derived, ::armarx::Matrix_2_3_byte> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2)}, {e(1, 0), e(1, 1), e(1, 2)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Byte, 3, 3>, Derived, ::armarx::Matrix_3_3_byte> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2)}, {e(1, 0), e(1, 1), e(1, 2)}, {e(2, 0), e(2, 1), e(2, 2)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Byte, 4, 3>, Derived, ::armarx::Matrix_4_3_byte> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2)}, {e(1, 0), e(1, 1), e(1, 2)}, {e(2, 0), e(2, 1), e(2, 2)}, {e(3, 0), e(3, 1), e(3, 2)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Byte, 5, 3>, Derived, ::armarx::Matrix_5_3_byte> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2)}, {e(1, 0), e(1, 1), e(1, 2)}, {e(2, 0), e(2, 1), e(2, 2)}, {e(3, 0), e(3, 1), e(3, 2)}, {e(4, 0), e(4, 1), e(4, 2)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Byte, 6, 3>, Derived, ::armarx::Matrix_6_3_byte> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2)}, {e(1, 0), e(1, 1), e(1, 2)}, {e(2, 0), e(2, 1), e(2, 2)}, {e(3, 0), e(3, 1), e(3, 2)}, {e(4, 0), e(4, 1), e(4, 2)}, {e(5, 0), e(5, 1), e(5, 2)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Byte, 1, 4>, Derived, ::armarx::Matrix_1_4_byte> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2), e(0, 3)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Byte, 2, 4>, Derived, ::armarx::Matrix_2_4_byte> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2), e(0, 3)}, {e(1, 0), e(1, 1), e(1, 2), e(1, 3)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Byte, 3, 4>, Derived, ::armarx::Matrix_3_4_byte> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2), e(0, 3)}, {e(1, 0), e(1, 1), e(1, 2), e(1, 3)}, {e(2, 0), e(2, 1), e(2, 2), e(2, 3)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Byte, 4, 4>, Derived, ::armarx::Matrix_4_4_byte> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2), e(0, 3)}, {e(1, 0), e(1, 1), e(1, 2), e(1, 3)}, {e(2, 0), e(2, 1), e(2, 2), e(2, 3)}, {e(3, 0), e(3, 1), e(3, 2), e(3, 3)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Byte, 5, 4>, Derived, ::armarx::Matrix_5_4_byte> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2), e(0, 3)}, {e(1, 0), e(1, 1), e(1, 2), e(1, 3)}, {e(2, 0), e(2, 1), e(2, 2), e(2, 3)}, {e(3, 0), e(3, 1), e(3, 2), e(3, 3)}, {e(4, 0), e(4, 1), e(4, 2), e(4, 3)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Byte, 6, 4>, Derived, ::armarx::Matrix_6_4_byte> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2), e(0, 3)}, {e(1, 0), e(1, 1), e(1, 2), e(1, 3)}, {e(2, 0), e(2, 1), e(2, 2), e(2, 3)}, {e(3, 0), e(3, 1), e(3, 2), e(3, 3)}, {e(4, 0), e(4, 1), e(4, 2), e(4, 3)}, {e(5, 0), e(5, 1), e(5, 2), e(5, 3)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Byte, 1, 5>, Derived, ::armarx::Matrix_1_5_byte> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2), e(0, 3), e(0, 4)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Byte, 2, 5>, Derived, ::armarx::Matrix_2_5_byte> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2), e(0, 3), e(0, 4)}, {e(1, 0), e(1, 1), e(1, 2), e(1, 3), e(1, 4)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Byte, 3, 5>, Derived, ::armarx::Matrix_3_5_byte> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2), e(0, 3), e(0, 4)}, {e(1, 0), e(1, 1), e(1, 2), e(1, 3), e(1, 4)}, {e(2, 0), e(2, 1), e(2, 2), e(2, 3), e(2, 4)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Byte, 4, 5>, Derived, ::armarx::Matrix_4_5_byte> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2), e(0, 3), e(0, 4)}, {e(1, 0), e(1, 1), e(1, 2), e(1, 3), e(1, 4)}, {e(2, 0), e(2, 1), e(2, 2), e(2, 3), e(2, 4)}, {e(3, 0), e(3, 1), e(3, 2), e(3, 3), e(3, 4)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Byte, 5, 5>, Derived, ::armarx::Matrix_5_5_byte> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2), e(0, 3), e(0, 4)}, {e(1, 0), e(1, 1), e(1, 2), e(1, 3), e(1, 4)}, {e(2, 0), e(2, 1), e(2, 2), e(2, 3), e(2, 4)}, {e(3, 0), e(3, 1), e(3, 2), e(3, 3), e(3, 4)}, {e(4, 0), e(4, 1), e(4, 2), e(4, 3), e(4, 4)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Byte, 6, 5>, Derived, ::armarx::Matrix_6_5_byte> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2), e(0, 3), e(0, 4)}, {e(1, 0), e(1, 1), e(1, 2), e(1, 3), e(1, 4)}, {e(2, 0), e(2, 1), e(2, 2), e(2, 3), e(2, 4)}, {e(3, 0), e(3, 1), e(3, 2), e(3, 3), e(3, 4)}, {e(4, 0), e(4, 1), e(4, 2), e(4, 3), e(4, 4)}, {e(5, 0), e(5, 1), e(5, 2), e(5, 3), e(5, 4)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Byte, 1, 6>, Derived, ::armarx::Matrix_1_6_byte> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2), e(0, 3), e(0, 4), e(0, 5)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Byte, 2, 6>, Derived, ::armarx::Matrix_2_6_byte> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2), e(0, 3), e(0, 4), e(0, 5)}, {e(1, 0), e(1, 1), e(1, 2), e(1, 3), e(1, 4), e(1, 5)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Byte, 3, 6>, Derived, ::armarx::Matrix_3_6_byte> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2), e(0, 3), e(0, 4), e(0, 5)}, {e(1, 0), e(1, 1), e(1, 2), e(1, 3), e(1, 4), e(1, 5)}, {e(2, 0), e(2, 1), e(2, 2), e(2, 3), e(2, 4), e(2, 5)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Byte, 4, 6>, Derived, ::armarx::Matrix_4_6_byte> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2), e(0, 3), e(0, 4), e(0, 5)}, {e(1, 0), e(1, 1), e(1, 2), e(1, 3), e(1, 4), e(1, 5)}, {e(2, 0), e(2, 1), e(2, 2), e(2, 3), e(2, 4), e(2, 5)}, {e(3, 0), e(3, 1), e(3, 2), e(3, 3), e(3, 4), e(3, 5)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Byte, 5, 6>, Derived, ::armarx::Matrix_5_6_byte> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2), e(0, 3), e(0, 4), e(0, 5)}, {e(1, 0), e(1, 1), e(1, 2), e(1, 3), e(1, 4), e(1, 5)}, {e(2, 0), e(2, 1), e(2, 2), e(2, 3), e(2, 4), e(2, 5)}, {e(3, 0), e(3, 1), e(3, 2), e(3, 3), e(3, 4), e(3, 5)}, {e(4, 0), e(4, 1), e(4, 2), e(4, 3), e(4, 4), e(4, 5)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Byte, 6, 6>, Derived, ::armarx::Matrix_6_6_byte> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2), e(0, 3), e(0, 4), e(0, 5)}, {e(1, 0), e(1, 1), e(1, 2), e(1, 3), e(1, 4), e(1, 5)}, {e(2, 0), e(2, 1), e(2, 2), e(2, 3), e(2, 4), e(2, 5)}, {e(3, 0), e(3, 1), e(3, 2), e(3, 3), e(3, 4), e(3, 5)}, {e(4, 0), e(4, 1), e(4, 2), e(4, 3), e(4, 4), e(4, 5)}, {e(5, 0), e(5, 1), e(5, 2), e(5, 3), e(5, 4), e(5, 5)}};
    } inline ::Eigen::Matrix< ::Ice::Byte, 1, 1> ToEigen(const ::armarx::Vector1byte& b)
    {
        ::Eigen::Matrix< ::Ice::Byte, 1, 1> m;
        m << b.e0;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Byte, 2, 1> ToEigen(const ::armarx::Vector2byte& b)
    {
        ::Eigen::Matrix< ::Ice::Byte, 2, 1> m;
        m << b.e0, b.e1;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Byte, 3, 1> ToEigen(const ::armarx::Vector3byte& b)
    {
        ::Eigen::Matrix< ::Ice::Byte, 3, 1> m;
        m << b.e0, b.e1, b.e2;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Byte, 4, 1> ToEigen(const ::armarx::Vector4byte& b)
    {
        ::Eigen::Matrix< ::Ice::Byte, 4, 1> m;
        m << b.e0, b.e1, b.e2, b.e3;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Byte, 5, 1> ToEigen(const ::armarx::Vector5byte& b)
    {
        ::Eigen::Matrix< ::Ice::Byte, 5, 1> m;
        m << b.e0, b.e1, b.e2, b.e3, b.e4;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Byte, 6, 1> ToEigen(const ::armarx::Vector6byte& b)
    {
        ::Eigen::Matrix< ::Ice::Byte, 6, 1> m;
        m << b.e0, b.e1, b.e2, b.e3, b.e4, b.e5;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Byte, 1, 1> ToEigen(const ::armarx::Matrix_1_1_byte& b)
    {
        ::Eigen::Matrix< ::Ice::Byte, 1, 1> m;
        m << b.e0.e0;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Byte, 2, 1> ToEigen(const ::armarx::Matrix_2_1_byte& b)
    {
        ::Eigen::Matrix< ::Ice::Byte, 2, 1> m;
        m << b.e0.e0, b.e1.e0;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Byte, 3, 1> ToEigen(const ::armarx::Matrix_3_1_byte& b)
    {
        ::Eigen::Matrix< ::Ice::Byte, 3, 1> m;
        m << b.e0.e0, b.e1.e0, b.e2.e0;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Byte, 4, 1> ToEigen(const ::armarx::Matrix_4_1_byte& b)
    {
        ::Eigen::Matrix< ::Ice::Byte, 4, 1> m;
        m << b.e0.e0, b.e1.e0, b.e2.e0, b.e3.e0;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Byte, 5, 1> ToEigen(const ::armarx::Matrix_5_1_byte& b)
    {
        ::Eigen::Matrix< ::Ice::Byte, 5, 1> m;
        m << b.e0.e0, b.e1.e0, b.e2.e0, b.e3.e0, b.e4.e0;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Byte, 6, 1> ToEigen(const ::armarx::Matrix_6_1_byte& b)
    {
        ::Eigen::Matrix< ::Ice::Byte, 6, 1> m;
        m << b.e0.e0, b.e1.e0, b.e2.e0, b.e3.e0, b.e4.e0, b.e5.e0;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Byte, 1, 2> ToEigen(const ::armarx::Matrix_1_2_byte& b)
    {
        ::Eigen::Matrix< ::Ice::Byte, 1, 2> m;
        m << b.e0.e0, b.e0.e1;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Byte, 2, 2> ToEigen(const ::armarx::Matrix_2_2_byte& b)
    {
        ::Eigen::Matrix< ::Ice::Byte, 2, 2> m;
        m << b.e0.e0, b.e0.e1, b.e1.e0, b.e1.e1;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Byte, 3, 2> ToEigen(const ::armarx::Matrix_3_2_byte& b)
    {
        ::Eigen::Matrix< ::Ice::Byte, 3, 2> m;
        m << b.e0.e0, b.e0.e1, b.e1.e0, b.e1.e1, b.e2.e0, b.e2.e1;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Byte, 4, 2> ToEigen(const ::armarx::Matrix_4_2_byte& b)
    {
        ::Eigen::Matrix< ::Ice::Byte, 4, 2> m;
        m << b.e0.e0, b.e0.e1, b.e1.e0, b.e1.e1, b.e2.e0, b.e2.e1, b.e3.e0, b.e3.e1;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Byte, 5, 2> ToEigen(const ::armarx::Matrix_5_2_byte& b)
    {
        ::Eigen::Matrix< ::Ice::Byte, 5, 2> m;
        m << b.e0.e0, b.e0.e1, b.e1.e0, b.e1.e1, b.e2.e0, b.e2.e1, b.e3.e0, b.e3.e1, b.e4.e0, b.e4.e1;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Byte, 6, 2> ToEigen(const ::armarx::Matrix_6_2_byte& b)
    {
        ::Eigen::Matrix< ::Ice::Byte, 6, 2> m;
        m << b.e0.e0, b.e0.e1, b.e1.e0, b.e1.e1, b.e2.e0, b.e2.e1, b.e3.e0, b.e3.e1, b.e4.e0, b.e4.e1, b.e5.e0, b.e5.e1;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Byte, 1, 3> ToEigen(const ::armarx::Matrix_1_3_byte& b)
    {
        ::Eigen::Matrix< ::Ice::Byte, 1, 3> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Byte, 2, 3> ToEigen(const ::armarx::Matrix_2_3_byte& b)
    {
        ::Eigen::Matrix< ::Ice::Byte, 2, 3> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2, b.e1.e0, b.e1.e1, b.e1.e2;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Byte, 3, 3> ToEigen(const ::armarx::Matrix_3_3_byte& b)
    {
        ::Eigen::Matrix< ::Ice::Byte, 3, 3> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2, b.e1.e0, b.e1.e1, b.e1.e2, b.e2.e0, b.e2.e1, b.e2.e2;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Byte, 4, 3> ToEigen(const ::armarx::Matrix_4_3_byte& b)
    {
        ::Eigen::Matrix< ::Ice::Byte, 4, 3> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2, b.e1.e0, b.e1.e1, b.e1.e2, b.e2.e0, b.e2.e1, b.e2.e2, b.e3.e0, b.e3.e1, b.e3.e2;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Byte, 5, 3> ToEigen(const ::armarx::Matrix_5_3_byte& b)
    {
        ::Eigen::Matrix< ::Ice::Byte, 5, 3> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2, b.e1.e0, b.e1.e1, b.e1.e2, b.e2.e0, b.e2.e1, b.e2.e2, b.e3.e0, b.e3.e1, b.e3.e2, b.e4.e0, b.e4.e1, b.e4.e2;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Byte, 6, 3> ToEigen(const ::armarx::Matrix_6_3_byte& b)
    {
        ::Eigen::Matrix< ::Ice::Byte, 6, 3> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2, b.e1.e0, b.e1.e1, b.e1.e2, b.e2.e0, b.e2.e1, b.e2.e2, b.e3.e0, b.e3.e1, b.e3.e2, b.e4.e0, b.e4.e1, b.e4.e2, b.e5.e0, b.e5.e1, b.e5.e2;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Byte, 1, 4> ToEigen(const ::armarx::Matrix_1_4_byte& b)
    {
        ::Eigen::Matrix< ::Ice::Byte, 1, 4> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2, b.e0.e3;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Byte, 2, 4> ToEigen(const ::armarx::Matrix_2_4_byte& b)
    {
        ::Eigen::Matrix< ::Ice::Byte, 2, 4> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2, b.e0.e3, b.e1.e0, b.e1.e1, b.e1.e2, b.e1.e3;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Byte, 3, 4> ToEigen(const ::armarx::Matrix_3_4_byte& b)
    {
        ::Eigen::Matrix< ::Ice::Byte, 3, 4> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2, b.e0.e3, b.e1.e0, b.e1.e1, b.e1.e2, b.e1.e3, b.e2.e0, b.e2.e1, b.e2.e2, b.e2.e3;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Byte, 4, 4> ToEigen(const ::armarx::Matrix_4_4_byte& b)
    {
        ::Eigen::Matrix< ::Ice::Byte, 4, 4> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2, b.e0.e3, b.e1.e0, b.e1.e1, b.e1.e2, b.e1.e3, b.e2.e0, b.e2.e1, b.e2.e2, b.e2.e3, b.e3.e0, b.e3.e1, b.e3.e2, b.e3.e3;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Byte, 5, 4> ToEigen(const ::armarx::Matrix_5_4_byte& b)
    {
        ::Eigen::Matrix< ::Ice::Byte, 5, 4> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2, b.e0.e3, b.e1.e0, b.e1.e1, b.e1.e2, b.e1.e3, b.e2.e0, b.e2.e1, b.e2.e2, b.e2.e3, b.e3.e0, b.e3.e1, b.e3.e2, b.e3.e3, b.e4.e0, b.e4.e1, b.e4.e2, b.e4.e3;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Byte, 6, 4> ToEigen(const ::armarx::Matrix_6_4_byte& b)
    {
        ::Eigen::Matrix< ::Ice::Byte, 6, 4> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2, b.e0.e3, b.e1.e0, b.e1.e1, b.e1.e2, b.e1.e3, b.e2.e0, b.e2.e1, b.e2.e2, b.e2.e3, b.e3.e0, b.e3.e1, b.e3.e2, b.e3.e3, b.e4.e0, b.e4.e1, b.e4.e2, b.e4.e3, b.e5.e0, b.e5.e1, b.e5.e2, b.e5.e3;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Byte, 1, 5> ToEigen(const ::armarx::Matrix_1_5_byte& b)
    {
        ::Eigen::Matrix< ::Ice::Byte, 1, 5> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2, b.e0.e3, b.e0.e4;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Byte, 2, 5> ToEigen(const ::armarx::Matrix_2_5_byte& b)
    {
        ::Eigen::Matrix< ::Ice::Byte, 2, 5> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2, b.e0.e3, b.e0.e4, b.e1.e0, b.e1.e1, b.e1.e2, b.e1.e3, b.e1.e4;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Byte, 3, 5> ToEigen(const ::armarx::Matrix_3_5_byte& b)
    {
        ::Eigen::Matrix< ::Ice::Byte, 3, 5> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2, b.e0.e3, b.e0.e4, b.e1.e0, b.e1.e1, b.e1.e2, b.e1.e3, b.e1.e4, b.e2.e0, b.e2.e1, b.e2.e2, b.e2.e3, b.e2.e4;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Byte, 4, 5> ToEigen(const ::armarx::Matrix_4_5_byte& b)
    {
        ::Eigen::Matrix< ::Ice::Byte, 4, 5> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2, b.e0.e3, b.e0.e4, b.e1.e0, b.e1.e1, b.e1.e2, b.e1.e3, b.e1.e4, b.e2.e0, b.e2.e1, b.e2.e2, b.e2.e3, b.e2.e4, b.e3.e0, b.e3.e1, b.e3.e2, b.e3.e3, b.e3.e4;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Byte, 5, 5> ToEigen(const ::armarx::Matrix_5_5_byte& b)
    {
        ::Eigen::Matrix< ::Ice::Byte, 5, 5> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2, b.e0.e3, b.e0.e4, b.e1.e0, b.e1.e1, b.e1.e2, b.e1.e3, b.e1.e4, b.e2.e0, b.e2.e1, b.e2.e2, b.e2.e3, b.e2.e4, b.e3.e0, b.e3.e1, b.e3.e2, b.e3.e3, b.e3.e4, b.e4.e0, b.e4.e1, b.e4.e2, b.e4.e3, b.e4.e4;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Byte, 6, 5> ToEigen(const ::armarx::Matrix_6_5_byte& b)
    {
        ::Eigen::Matrix< ::Ice::Byte, 6, 5> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2, b.e0.e3, b.e0.e4, b.e1.e0, b.e1.e1, b.e1.e2, b.e1.e3, b.e1.e4, b.e2.e0, b.e2.e1, b.e2.e2, b.e2.e3, b.e2.e4, b.e3.e0, b.e3.e1, b.e3.e2, b.e3.e3, b.e3.e4, b.e4.e0, b.e4.e1, b.e4.e2, b.e4.e3, b.e4.e4, b.e5.e0, b.e5.e1, b.e5.e2, b.e5.e3, b.e5.e4;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Byte, 1, 6> ToEigen(const ::armarx::Matrix_1_6_byte& b)
    {
        ::Eigen::Matrix< ::Ice::Byte, 1, 6> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2, b.e0.e3, b.e0.e4, b.e0.e5;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Byte, 2, 6> ToEigen(const ::armarx::Matrix_2_6_byte& b)
    {
        ::Eigen::Matrix< ::Ice::Byte, 2, 6> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2, b.e0.e3, b.e0.e4, b.e0.e5, b.e1.e0, b.e1.e1, b.e1.e2, b.e1.e3, b.e1.e4, b.e1.e5;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Byte, 3, 6> ToEigen(const ::armarx::Matrix_3_6_byte& b)
    {
        ::Eigen::Matrix< ::Ice::Byte, 3, 6> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2, b.e0.e3, b.e0.e4, b.e0.e5, b.e1.e0, b.e1.e1, b.e1.e2, b.e1.e3, b.e1.e4, b.e1.e5, b.e2.e0, b.e2.e1, b.e2.e2, b.e2.e3, b.e2.e4, b.e2.e5;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Byte, 4, 6> ToEigen(const ::armarx::Matrix_4_6_byte& b)
    {
        ::Eigen::Matrix< ::Ice::Byte, 4, 6> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2, b.e0.e3, b.e0.e4, b.e0.e5, b.e1.e0, b.e1.e1, b.e1.e2, b.e1.e3, b.e1.e4, b.e1.e5, b.e2.e0, b.e2.e1, b.e2.e2, b.e2.e3, b.e2.e4, b.e2.e5, b.e3.e0, b.e3.e1, b.e3.e2, b.e3.e3, b.e3.e4, b.e3.e5;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Byte, 5, 6> ToEigen(const ::armarx::Matrix_5_6_byte& b)
    {
        ::Eigen::Matrix< ::Ice::Byte, 5, 6> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2, b.e0.e3, b.e0.e4, b.e0.e5, b.e1.e0, b.e1.e1, b.e1.e2, b.e1.e3, b.e1.e4, b.e1.e5, b.e2.e0, b.e2.e1, b.e2.e2, b.e2.e3, b.e2.e4, b.e2.e5, b.e3.e0, b.e3.e1, b.e3.e2, b.e3.e3, b.e3.e4, b.e3.e5, b.e4.e0, b.e4.e1, b.e4.e2, b.e4.e3, b.e4.e4, b.e4.e5;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Byte, 6, 6> ToEigen(const ::armarx::Matrix_6_6_byte& b)
    {
        ::Eigen::Matrix< ::Ice::Byte, 6, 6> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2, b.e0.e3, b.e0.e4, b.e0.e5, b.e1.e0, b.e1.e1, b.e1.e2, b.e1.e3, b.e1.e4, b.e1.e5, b.e2.e0, b.e2.e1, b.e2.e2, b.e2.e3, b.e2.e4, b.e2.e5, b.e3.e0, b.e3.e1, b.e3.e2, b.e3.e3, b.e3.e4, b.e3.e5, b.e4.e0, b.e4.e1, b.e4.e2, b.e4.e3, b.e4.e4, b.e4.e5, b.e5.e0, b.e5.e1, b.e5.e2, b.e5.e3, b.e5.e4, b.e5.e5;
        return m;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Vector1byte& r)
    {
        detail::bvt::AllOut(l, '\t', r.e0);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Vector2byte& r)
    {
        detail::bvt::AllOut(l, '\t', r.e0, r.e1);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Vector3byte& r)
    {
        detail::bvt::AllOut(l, '\t', r.e0, r.e1, r.e2);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Vector4byte& r)
    {
        detail::bvt::AllOut(l, '\t', r.e0, r.e1, r.e2, r.e3);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Vector5byte& r)
    {
        detail::bvt::AllOut(l, '\t', r.e0, r.e1, r.e2, r.e3, r.e4);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Vector6byte& r)
    {
        detail::bvt::AllOut(l, '\t', r.e0, r.e1, r.e2, r.e3, r.e4, r.e5);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_1_1_byte& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_2_1_byte& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_3_1_byte& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_4_1_byte& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2, r.e3);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_5_1_byte& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2, r.e3, r.e4);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_6_1_byte& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2, r.e3, r.e4, r.e5);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_1_2_byte& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_2_2_byte& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_3_2_byte& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_4_2_byte& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2, r.e3);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_5_2_byte& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2, r.e3, r.e4);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_6_2_byte& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2, r.e3, r.e4, r.e5);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_1_3_byte& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_2_3_byte& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_3_3_byte& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_4_3_byte& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2, r.e3);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_5_3_byte& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2, r.e3, r.e4);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_6_3_byte& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2, r.e3, r.e4, r.e5);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_1_4_byte& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_2_4_byte& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_3_4_byte& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_4_4_byte& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2, r.e3);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_5_4_byte& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2, r.e3, r.e4);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_6_4_byte& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2, r.e3, r.e4, r.e5);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_1_5_byte& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_2_5_byte& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_3_5_byte& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_4_5_byte& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2, r.e3);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_5_5_byte& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2, r.e3, r.e4);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_6_5_byte& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2, r.e3, r.e4, r.e5);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_1_6_byte& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_2_6_byte& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_3_6_byte& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_4_6_byte& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2, r.e3);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_5_6_byte& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2, r.e3, r.e4);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_6_6_byte& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2, r.e3, r.e4, r.e5);
        return l;
    }
    template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Short, 1, 1>, Derived, ::armarx::Vector1s> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {e(0)};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Short, 2, 1>, Derived, ::armarx::Vector2s> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {e(0), e(1)};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Short, 3, 1>, Derived, ::armarx::Vector3s> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {e(0), e(1), e(2)};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Short, 4, 1>, Derived, ::armarx::Vector4s> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {e(0), e(1), e(2), e(3)};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Short, 5, 1>, Derived, ::armarx::Vector5s> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {e(0), e(1), e(2), e(3), e(4)};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Short, 6, 1>, Derived, ::armarx::Vector6s> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {e(0), e(1), e(2), e(3), e(4), e(5)};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Short, 1, 2>, Derived, ::armarx::Matrix_1_2_s> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Short, 2, 2>, Derived, ::armarx::Matrix_2_2_s> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1)}, {e(1, 0), e(1, 1)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Short, 3, 2>, Derived, ::armarx::Matrix_3_2_s> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1)}, {e(1, 0), e(1, 1)}, {e(2, 0), e(2, 1)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Short, 4, 2>, Derived, ::armarx::Matrix_4_2_s> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1)}, {e(1, 0), e(1, 1)}, {e(2, 0), e(2, 1)}, {e(3, 0), e(3, 1)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Short, 5, 2>, Derived, ::armarx::Matrix_5_2_s> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1)}, {e(1, 0), e(1, 1)}, {e(2, 0), e(2, 1)}, {e(3, 0), e(3, 1)}, {e(4, 0), e(4, 1)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Short, 6, 2>, Derived, ::armarx::Matrix_6_2_s> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1)}, {e(1, 0), e(1, 1)}, {e(2, 0), e(2, 1)}, {e(3, 0), e(3, 1)}, {e(4, 0), e(4, 1)}, {e(5, 0), e(5, 1)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Short, 1, 3>, Derived, ::armarx::Matrix_1_3_s> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Short, 2, 3>, Derived, ::armarx::Matrix_2_3_s> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2)}, {e(1, 0), e(1, 1), e(1, 2)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Short, 3, 3>, Derived, ::armarx::Matrix_3_3_s> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2)}, {e(1, 0), e(1, 1), e(1, 2)}, {e(2, 0), e(2, 1), e(2, 2)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Short, 4, 3>, Derived, ::armarx::Matrix_4_3_s> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2)}, {e(1, 0), e(1, 1), e(1, 2)}, {e(2, 0), e(2, 1), e(2, 2)}, {e(3, 0), e(3, 1), e(3, 2)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Short, 5, 3>, Derived, ::armarx::Matrix_5_3_s> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2)}, {e(1, 0), e(1, 1), e(1, 2)}, {e(2, 0), e(2, 1), e(2, 2)}, {e(3, 0), e(3, 1), e(3, 2)}, {e(4, 0), e(4, 1), e(4, 2)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Short, 6, 3>, Derived, ::armarx::Matrix_6_3_s> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2)}, {e(1, 0), e(1, 1), e(1, 2)}, {e(2, 0), e(2, 1), e(2, 2)}, {e(3, 0), e(3, 1), e(3, 2)}, {e(4, 0), e(4, 1), e(4, 2)}, {e(5, 0), e(5, 1), e(5, 2)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Short, 1, 4>, Derived, ::armarx::Matrix_1_4_s> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2), e(0, 3)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Short, 2, 4>, Derived, ::armarx::Matrix_2_4_s> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2), e(0, 3)}, {e(1, 0), e(1, 1), e(1, 2), e(1, 3)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Short, 3, 4>, Derived, ::armarx::Matrix_3_4_s> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2), e(0, 3)}, {e(1, 0), e(1, 1), e(1, 2), e(1, 3)}, {e(2, 0), e(2, 1), e(2, 2), e(2, 3)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Short, 4, 4>, Derived, ::armarx::Matrix_4_4_s> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2), e(0, 3)}, {e(1, 0), e(1, 1), e(1, 2), e(1, 3)}, {e(2, 0), e(2, 1), e(2, 2), e(2, 3)}, {e(3, 0), e(3, 1), e(3, 2), e(3, 3)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Short, 5, 4>, Derived, ::armarx::Matrix_5_4_s> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2), e(0, 3)}, {e(1, 0), e(1, 1), e(1, 2), e(1, 3)}, {e(2, 0), e(2, 1), e(2, 2), e(2, 3)}, {e(3, 0), e(3, 1), e(3, 2), e(3, 3)}, {e(4, 0), e(4, 1), e(4, 2), e(4, 3)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Short, 6, 4>, Derived, ::armarx::Matrix_6_4_s> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2), e(0, 3)}, {e(1, 0), e(1, 1), e(1, 2), e(1, 3)}, {e(2, 0), e(2, 1), e(2, 2), e(2, 3)}, {e(3, 0), e(3, 1), e(3, 2), e(3, 3)}, {e(4, 0), e(4, 1), e(4, 2), e(4, 3)}, {e(5, 0), e(5, 1), e(5, 2), e(5, 3)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Short, 1, 5>, Derived, ::armarx::Matrix_1_5_s> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2), e(0, 3), e(0, 4)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Short, 2, 5>, Derived, ::armarx::Matrix_2_5_s> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2), e(0, 3), e(0, 4)}, {e(1, 0), e(1, 1), e(1, 2), e(1, 3), e(1, 4)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Short, 3, 5>, Derived, ::armarx::Matrix_3_5_s> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2), e(0, 3), e(0, 4)}, {e(1, 0), e(1, 1), e(1, 2), e(1, 3), e(1, 4)}, {e(2, 0), e(2, 1), e(2, 2), e(2, 3), e(2, 4)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Short, 4, 5>, Derived, ::armarx::Matrix_4_5_s> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2), e(0, 3), e(0, 4)}, {e(1, 0), e(1, 1), e(1, 2), e(1, 3), e(1, 4)}, {e(2, 0), e(2, 1), e(2, 2), e(2, 3), e(2, 4)}, {e(3, 0), e(3, 1), e(3, 2), e(3, 3), e(3, 4)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Short, 5, 5>, Derived, ::armarx::Matrix_5_5_s> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2), e(0, 3), e(0, 4)}, {e(1, 0), e(1, 1), e(1, 2), e(1, 3), e(1, 4)}, {e(2, 0), e(2, 1), e(2, 2), e(2, 3), e(2, 4)}, {e(3, 0), e(3, 1), e(3, 2), e(3, 3), e(3, 4)}, {e(4, 0), e(4, 1), e(4, 2), e(4, 3), e(4, 4)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Short, 6, 5>, Derived, ::armarx::Matrix_6_5_s> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2), e(0, 3), e(0, 4)}, {e(1, 0), e(1, 1), e(1, 2), e(1, 3), e(1, 4)}, {e(2, 0), e(2, 1), e(2, 2), e(2, 3), e(2, 4)}, {e(3, 0), e(3, 1), e(3, 2), e(3, 3), e(3, 4)}, {e(4, 0), e(4, 1), e(4, 2), e(4, 3), e(4, 4)}, {e(5, 0), e(5, 1), e(5, 2), e(5, 3), e(5, 4)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Short, 1, 6>, Derived, ::armarx::Matrix_1_6_s> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2), e(0, 3), e(0, 4), e(0, 5)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Short, 2, 6>, Derived, ::armarx::Matrix_2_6_s> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2), e(0, 3), e(0, 4), e(0, 5)}, {e(1, 0), e(1, 1), e(1, 2), e(1, 3), e(1, 4), e(1, 5)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Short, 3, 6>, Derived, ::armarx::Matrix_3_6_s> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2), e(0, 3), e(0, 4), e(0, 5)}, {e(1, 0), e(1, 1), e(1, 2), e(1, 3), e(1, 4), e(1, 5)}, {e(2, 0), e(2, 1), e(2, 2), e(2, 3), e(2, 4), e(2, 5)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Short, 4, 6>, Derived, ::armarx::Matrix_4_6_s> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2), e(0, 3), e(0, 4), e(0, 5)}, {e(1, 0), e(1, 1), e(1, 2), e(1, 3), e(1, 4), e(1, 5)}, {e(2, 0), e(2, 1), e(2, 2), e(2, 3), e(2, 4), e(2, 5)}, {e(3, 0), e(3, 1), e(3, 2), e(3, 3), e(3, 4), e(3, 5)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Short, 5, 6>, Derived, ::armarx::Matrix_5_6_s> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2), e(0, 3), e(0, 4), e(0, 5)}, {e(1, 0), e(1, 1), e(1, 2), e(1, 3), e(1, 4), e(1, 5)}, {e(2, 0), e(2, 1), e(2, 2), e(2, 3), e(2, 4), e(2, 5)}, {e(3, 0), e(3, 1), e(3, 2), e(3, 3), e(3, 4), e(3, 5)}, {e(4, 0), e(4, 1), e(4, 2), e(4, 3), e(4, 4), e(4, 5)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Short, 6, 6>, Derived, ::armarx::Matrix_6_6_s> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2), e(0, 3), e(0, 4), e(0, 5)}, {e(1, 0), e(1, 1), e(1, 2), e(1, 3), e(1, 4), e(1, 5)}, {e(2, 0), e(2, 1), e(2, 2), e(2, 3), e(2, 4), e(2, 5)}, {e(3, 0), e(3, 1), e(3, 2), e(3, 3), e(3, 4), e(3, 5)}, {e(4, 0), e(4, 1), e(4, 2), e(4, 3), e(4, 4), e(4, 5)}, {e(5, 0), e(5, 1), e(5, 2), e(5, 3), e(5, 4), e(5, 5)}};
    } inline ::Eigen::Matrix< ::Ice::Short, 1, 1> ToEigen(const ::armarx::Vector1s& b)
    {
        ::Eigen::Matrix< ::Ice::Short, 1, 1> m;
        m << b.e0;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Short, 2, 1> ToEigen(const ::armarx::Vector2s& b)
    {
        ::Eigen::Matrix< ::Ice::Short, 2, 1> m;
        m << b.e0, b.e1;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Short, 3, 1> ToEigen(const ::armarx::Vector3s& b)
    {
        ::Eigen::Matrix< ::Ice::Short, 3, 1> m;
        m << b.e0, b.e1, b.e2;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Short, 4, 1> ToEigen(const ::armarx::Vector4s& b)
    {
        ::Eigen::Matrix< ::Ice::Short, 4, 1> m;
        m << b.e0, b.e1, b.e2, b.e3;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Short, 5, 1> ToEigen(const ::armarx::Vector5s& b)
    {
        ::Eigen::Matrix< ::Ice::Short, 5, 1> m;
        m << b.e0, b.e1, b.e2, b.e3, b.e4;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Short, 6, 1> ToEigen(const ::armarx::Vector6s& b)
    {
        ::Eigen::Matrix< ::Ice::Short, 6, 1> m;
        m << b.e0, b.e1, b.e2, b.e3, b.e4, b.e5;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Short, 1, 1> ToEigen(const ::armarx::Matrix_1_1_s& b)
    {
        ::Eigen::Matrix< ::Ice::Short, 1, 1> m;
        m << b.e0.e0;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Short, 2, 1> ToEigen(const ::armarx::Matrix_2_1_s& b)
    {
        ::Eigen::Matrix< ::Ice::Short, 2, 1> m;
        m << b.e0.e0, b.e1.e0;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Short, 3, 1> ToEigen(const ::armarx::Matrix_3_1_s& b)
    {
        ::Eigen::Matrix< ::Ice::Short, 3, 1> m;
        m << b.e0.e0, b.e1.e0, b.e2.e0;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Short, 4, 1> ToEigen(const ::armarx::Matrix_4_1_s& b)
    {
        ::Eigen::Matrix< ::Ice::Short, 4, 1> m;
        m << b.e0.e0, b.e1.e0, b.e2.e0, b.e3.e0;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Short, 5, 1> ToEigen(const ::armarx::Matrix_5_1_s& b)
    {
        ::Eigen::Matrix< ::Ice::Short, 5, 1> m;
        m << b.e0.e0, b.e1.e0, b.e2.e0, b.e3.e0, b.e4.e0;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Short, 6, 1> ToEigen(const ::armarx::Matrix_6_1_s& b)
    {
        ::Eigen::Matrix< ::Ice::Short, 6, 1> m;
        m << b.e0.e0, b.e1.e0, b.e2.e0, b.e3.e0, b.e4.e0, b.e5.e0;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Short, 1, 2> ToEigen(const ::armarx::Matrix_1_2_s& b)
    {
        ::Eigen::Matrix< ::Ice::Short, 1, 2> m;
        m << b.e0.e0, b.e0.e1;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Short, 2, 2> ToEigen(const ::armarx::Matrix_2_2_s& b)
    {
        ::Eigen::Matrix< ::Ice::Short, 2, 2> m;
        m << b.e0.e0, b.e0.e1, b.e1.e0, b.e1.e1;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Short, 3, 2> ToEigen(const ::armarx::Matrix_3_2_s& b)
    {
        ::Eigen::Matrix< ::Ice::Short, 3, 2> m;
        m << b.e0.e0, b.e0.e1, b.e1.e0, b.e1.e1, b.e2.e0, b.e2.e1;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Short, 4, 2> ToEigen(const ::armarx::Matrix_4_2_s& b)
    {
        ::Eigen::Matrix< ::Ice::Short, 4, 2> m;
        m << b.e0.e0, b.e0.e1, b.e1.e0, b.e1.e1, b.e2.e0, b.e2.e1, b.e3.e0, b.e3.e1;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Short, 5, 2> ToEigen(const ::armarx::Matrix_5_2_s& b)
    {
        ::Eigen::Matrix< ::Ice::Short, 5, 2> m;
        m << b.e0.e0, b.e0.e1, b.e1.e0, b.e1.e1, b.e2.e0, b.e2.e1, b.e3.e0, b.e3.e1, b.e4.e0, b.e4.e1;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Short, 6, 2> ToEigen(const ::armarx::Matrix_6_2_s& b)
    {
        ::Eigen::Matrix< ::Ice::Short, 6, 2> m;
        m << b.e0.e0, b.e0.e1, b.e1.e0, b.e1.e1, b.e2.e0, b.e2.e1, b.e3.e0, b.e3.e1, b.e4.e0, b.e4.e1, b.e5.e0, b.e5.e1;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Short, 1, 3> ToEigen(const ::armarx::Matrix_1_3_s& b)
    {
        ::Eigen::Matrix< ::Ice::Short, 1, 3> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Short, 2, 3> ToEigen(const ::armarx::Matrix_2_3_s& b)
    {
        ::Eigen::Matrix< ::Ice::Short, 2, 3> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2, b.e1.e0, b.e1.e1, b.e1.e2;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Short, 3, 3> ToEigen(const ::armarx::Matrix_3_3_s& b)
    {
        ::Eigen::Matrix< ::Ice::Short, 3, 3> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2, b.e1.e0, b.e1.e1, b.e1.e2, b.e2.e0, b.e2.e1, b.e2.e2;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Short, 4, 3> ToEigen(const ::armarx::Matrix_4_3_s& b)
    {
        ::Eigen::Matrix< ::Ice::Short, 4, 3> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2, b.e1.e0, b.e1.e1, b.e1.e2, b.e2.e0, b.e2.e1, b.e2.e2, b.e3.e0, b.e3.e1, b.e3.e2;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Short, 5, 3> ToEigen(const ::armarx::Matrix_5_3_s& b)
    {
        ::Eigen::Matrix< ::Ice::Short, 5, 3> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2, b.e1.e0, b.e1.e1, b.e1.e2, b.e2.e0, b.e2.e1, b.e2.e2, b.e3.e0, b.e3.e1, b.e3.e2, b.e4.e0, b.e4.e1, b.e4.e2;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Short, 6, 3> ToEigen(const ::armarx::Matrix_6_3_s& b)
    {
        ::Eigen::Matrix< ::Ice::Short, 6, 3> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2, b.e1.e0, b.e1.e1, b.e1.e2, b.e2.e0, b.e2.e1, b.e2.e2, b.e3.e0, b.e3.e1, b.e3.e2, b.e4.e0, b.e4.e1, b.e4.e2, b.e5.e0, b.e5.e1, b.e5.e2;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Short, 1, 4> ToEigen(const ::armarx::Matrix_1_4_s& b)
    {
        ::Eigen::Matrix< ::Ice::Short, 1, 4> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2, b.e0.e3;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Short, 2, 4> ToEigen(const ::armarx::Matrix_2_4_s& b)
    {
        ::Eigen::Matrix< ::Ice::Short, 2, 4> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2, b.e0.e3, b.e1.e0, b.e1.e1, b.e1.e2, b.e1.e3;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Short, 3, 4> ToEigen(const ::armarx::Matrix_3_4_s& b)
    {
        ::Eigen::Matrix< ::Ice::Short, 3, 4> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2, b.e0.e3, b.e1.e0, b.e1.e1, b.e1.e2, b.e1.e3, b.e2.e0, b.e2.e1, b.e2.e2, b.e2.e3;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Short, 4, 4> ToEigen(const ::armarx::Matrix_4_4_s& b)
    {
        ::Eigen::Matrix< ::Ice::Short, 4, 4> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2, b.e0.e3, b.e1.e0, b.e1.e1, b.e1.e2, b.e1.e3, b.e2.e0, b.e2.e1, b.e2.e2, b.e2.e3, b.e3.e0, b.e3.e1, b.e3.e2, b.e3.e3;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Short, 5, 4> ToEigen(const ::armarx::Matrix_5_4_s& b)
    {
        ::Eigen::Matrix< ::Ice::Short, 5, 4> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2, b.e0.e3, b.e1.e0, b.e1.e1, b.e1.e2, b.e1.e3, b.e2.e0, b.e2.e1, b.e2.e2, b.e2.e3, b.e3.e0, b.e3.e1, b.e3.e2, b.e3.e3, b.e4.e0, b.e4.e1, b.e4.e2, b.e4.e3;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Short, 6, 4> ToEigen(const ::armarx::Matrix_6_4_s& b)
    {
        ::Eigen::Matrix< ::Ice::Short, 6, 4> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2, b.e0.e3, b.e1.e0, b.e1.e1, b.e1.e2, b.e1.e3, b.e2.e0, b.e2.e1, b.e2.e2, b.e2.e3, b.e3.e0, b.e3.e1, b.e3.e2, b.e3.e3, b.e4.e0, b.e4.e1, b.e4.e2, b.e4.e3, b.e5.e0, b.e5.e1, b.e5.e2, b.e5.e3;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Short, 1, 5> ToEigen(const ::armarx::Matrix_1_5_s& b)
    {
        ::Eigen::Matrix< ::Ice::Short, 1, 5> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2, b.e0.e3, b.e0.e4;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Short, 2, 5> ToEigen(const ::armarx::Matrix_2_5_s& b)
    {
        ::Eigen::Matrix< ::Ice::Short, 2, 5> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2, b.e0.e3, b.e0.e4, b.e1.e0, b.e1.e1, b.e1.e2, b.e1.e3, b.e1.e4;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Short, 3, 5> ToEigen(const ::armarx::Matrix_3_5_s& b)
    {
        ::Eigen::Matrix< ::Ice::Short, 3, 5> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2, b.e0.e3, b.e0.e4, b.e1.e0, b.e1.e1, b.e1.e2, b.e1.e3, b.e1.e4, b.e2.e0, b.e2.e1, b.e2.e2, b.e2.e3, b.e2.e4;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Short, 4, 5> ToEigen(const ::armarx::Matrix_4_5_s& b)
    {
        ::Eigen::Matrix< ::Ice::Short, 4, 5> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2, b.e0.e3, b.e0.e4, b.e1.e0, b.e1.e1, b.e1.e2, b.e1.e3, b.e1.e4, b.e2.e0, b.e2.e1, b.e2.e2, b.e2.e3, b.e2.e4, b.e3.e0, b.e3.e1, b.e3.e2, b.e3.e3, b.e3.e4;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Short, 5, 5> ToEigen(const ::armarx::Matrix_5_5_s& b)
    {
        ::Eigen::Matrix< ::Ice::Short, 5, 5> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2, b.e0.e3, b.e0.e4, b.e1.e0, b.e1.e1, b.e1.e2, b.e1.e3, b.e1.e4, b.e2.e0, b.e2.e1, b.e2.e2, b.e2.e3, b.e2.e4, b.e3.e0, b.e3.e1, b.e3.e2, b.e3.e3, b.e3.e4, b.e4.e0, b.e4.e1, b.e4.e2, b.e4.e3, b.e4.e4;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Short, 6, 5> ToEigen(const ::armarx::Matrix_6_5_s& b)
    {
        ::Eigen::Matrix< ::Ice::Short, 6, 5> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2, b.e0.e3, b.e0.e4, b.e1.e0, b.e1.e1, b.e1.e2, b.e1.e3, b.e1.e4, b.e2.e0, b.e2.e1, b.e2.e2, b.e2.e3, b.e2.e4, b.e3.e0, b.e3.e1, b.e3.e2, b.e3.e3, b.e3.e4, b.e4.e0, b.e4.e1, b.e4.e2, b.e4.e3, b.e4.e4, b.e5.e0, b.e5.e1, b.e5.e2, b.e5.e3, b.e5.e4;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Short, 1, 6> ToEigen(const ::armarx::Matrix_1_6_s& b)
    {
        ::Eigen::Matrix< ::Ice::Short, 1, 6> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2, b.e0.e3, b.e0.e4, b.e0.e5;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Short, 2, 6> ToEigen(const ::armarx::Matrix_2_6_s& b)
    {
        ::Eigen::Matrix< ::Ice::Short, 2, 6> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2, b.e0.e3, b.e0.e4, b.e0.e5, b.e1.e0, b.e1.e1, b.e1.e2, b.e1.e3, b.e1.e4, b.e1.e5;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Short, 3, 6> ToEigen(const ::armarx::Matrix_3_6_s& b)
    {
        ::Eigen::Matrix< ::Ice::Short, 3, 6> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2, b.e0.e3, b.e0.e4, b.e0.e5, b.e1.e0, b.e1.e1, b.e1.e2, b.e1.e3, b.e1.e4, b.e1.e5, b.e2.e0, b.e2.e1, b.e2.e2, b.e2.e3, b.e2.e4, b.e2.e5;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Short, 4, 6> ToEigen(const ::armarx::Matrix_4_6_s& b)
    {
        ::Eigen::Matrix< ::Ice::Short, 4, 6> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2, b.e0.e3, b.e0.e4, b.e0.e5, b.e1.e0, b.e1.e1, b.e1.e2, b.e1.e3, b.e1.e4, b.e1.e5, b.e2.e0, b.e2.e1, b.e2.e2, b.e2.e3, b.e2.e4, b.e2.e5, b.e3.e0, b.e3.e1, b.e3.e2, b.e3.e3, b.e3.e4, b.e3.e5;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Short, 5, 6> ToEigen(const ::armarx::Matrix_5_6_s& b)
    {
        ::Eigen::Matrix< ::Ice::Short, 5, 6> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2, b.e0.e3, b.e0.e4, b.e0.e5, b.e1.e0, b.e1.e1, b.e1.e2, b.e1.e3, b.e1.e4, b.e1.e5, b.e2.e0, b.e2.e1, b.e2.e2, b.e2.e3, b.e2.e4, b.e2.e5, b.e3.e0, b.e3.e1, b.e3.e2, b.e3.e3, b.e3.e4, b.e3.e5, b.e4.e0, b.e4.e1, b.e4.e2, b.e4.e3, b.e4.e4, b.e4.e5;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Short, 6, 6> ToEigen(const ::armarx::Matrix_6_6_s& b)
    {
        ::Eigen::Matrix< ::Ice::Short, 6, 6> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2, b.e0.e3, b.e0.e4, b.e0.e5, b.e1.e0, b.e1.e1, b.e1.e2, b.e1.e3, b.e1.e4, b.e1.e5, b.e2.e0, b.e2.e1, b.e2.e2, b.e2.e3, b.e2.e4, b.e2.e5, b.e3.e0, b.e3.e1, b.e3.e2, b.e3.e3, b.e3.e4, b.e3.e5, b.e4.e0, b.e4.e1, b.e4.e2, b.e4.e3, b.e4.e4, b.e4.e5, b.e5.e0, b.e5.e1, b.e5.e2, b.e5.e3, b.e5.e4, b.e5.e5;
        return m;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Vector1s& r)
    {
        detail::bvt::AllOut(l, '\t', r.e0);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Vector2s& r)
    {
        detail::bvt::AllOut(l, '\t', r.e0, r.e1);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Vector3s& r)
    {
        detail::bvt::AllOut(l, '\t', r.e0, r.e1, r.e2);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Vector4s& r)
    {
        detail::bvt::AllOut(l, '\t', r.e0, r.e1, r.e2, r.e3);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Vector5s& r)
    {
        detail::bvt::AllOut(l, '\t', r.e0, r.e1, r.e2, r.e3, r.e4);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Vector6s& r)
    {
        detail::bvt::AllOut(l, '\t', r.e0, r.e1, r.e2, r.e3, r.e4, r.e5);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_1_1_s& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_2_1_s& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_3_1_s& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_4_1_s& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2, r.e3);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_5_1_s& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2, r.e3, r.e4);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_6_1_s& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2, r.e3, r.e4, r.e5);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_1_2_s& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_2_2_s& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_3_2_s& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_4_2_s& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2, r.e3);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_5_2_s& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2, r.e3, r.e4);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_6_2_s& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2, r.e3, r.e4, r.e5);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_1_3_s& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_2_3_s& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_3_3_s& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_4_3_s& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2, r.e3);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_5_3_s& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2, r.e3, r.e4);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_6_3_s& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2, r.e3, r.e4, r.e5);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_1_4_s& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_2_4_s& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_3_4_s& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_4_4_s& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2, r.e3);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_5_4_s& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2, r.e3, r.e4);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_6_4_s& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2, r.e3, r.e4, r.e5);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_1_5_s& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_2_5_s& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_3_5_s& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_4_5_s& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2, r.e3);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_5_5_s& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2, r.e3, r.e4);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_6_5_s& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2, r.e3, r.e4, r.e5);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_1_6_s& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_2_6_s& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_3_6_s& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_4_6_s& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2, r.e3);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_5_6_s& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2, r.e3, r.e4);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_6_6_s& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2, r.e3, r.e4, r.e5);
        return l;
    }
    template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Int, 1, 1>, Derived, ::armarx::Vector1i> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {e(0)};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Int, 2, 1>, Derived, ::armarx::Vector2i> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {e(0), e(1)};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Int, 3, 1>, Derived, ::armarx::Vector3i> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {e(0), e(1), e(2)};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Int, 4, 1>, Derived, ::armarx::Vector4i> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {e(0), e(1), e(2), e(3)};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Int, 5, 1>, Derived, ::armarx::Vector5i> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {e(0), e(1), e(2), e(3), e(4)};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Int, 6, 1>, Derived, ::armarx::Vector6i> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {e(0), e(1), e(2), e(3), e(4), e(5)};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Int, 1, 2>, Derived, ::armarx::Matrix_1_2_i> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Int, 2, 2>, Derived, ::armarx::Matrix_2_2_i> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1)}, {e(1, 0), e(1, 1)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Int, 3, 2>, Derived, ::armarx::Matrix_3_2_i> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1)}, {e(1, 0), e(1, 1)}, {e(2, 0), e(2, 1)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Int, 4, 2>, Derived, ::armarx::Matrix_4_2_i> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1)}, {e(1, 0), e(1, 1)}, {e(2, 0), e(2, 1)}, {e(3, 0), e(3, 1)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Int, 5, 2>, Derived, ::armarx::Matrix_5_2_i> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1)}, {e(1, 0), e(1, 1)}, {e(2, 0), e(2, 1)}, {e(3, 0), e(3, 1)}, {e(4, 0), e(4, 1)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Int, 6, 2>, Derived, ::armarx::Matrix_6_2_i> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1)}, {e(1, 0), e(1, 1)}, {e(2, 0), e(2, 1)}, {e(3, 0), e(3, 1)}, {e(4, 0), e(4, 1)}, {e(5, 0), e(5, 1)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Int, 1, 3>, Derived, ::armarx::Matrix_1_3_i> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Int, 2, 3>, Derived, ::armarx::Matrix_2_3_i> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2)}, {e(1, 0), e(1, 1), e(1, 2)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Int, 3, 3>, Derived, ::armarx::Matrix_3_3_i> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2)}, {e(1, 0), e(1, 1), e(1, 2)}, {e(2, 0), e(2, 1), e(2, 2)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Int, 4, 3>, Derived, ::armarx::Matrix_4_3_i> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2)}, {e(1, 0), e(1, 1), e(1, 2)}, {e(2, 0), e(2, 1), e(2, 2)}, {e(3, 0), e(3, 1), e(3, 2)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Int, 5, 3>, Derived, ::armarx::Matrix_5_3_i> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2)}, {e(1, 0), e(1, 1), e(1, 2)}, {e(2, 0), e(2, 1), e(2, 2)}, {e(3, 0), e(3, 1), e(3, 2)}, {e(4, 0), e(4, 1), e(4, 2)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Int, 6, 3>, Derived, ::armarx::Matrix_6_3_i> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2)}, {e(1, 0), e(1, 1), e(1, 2)}, {e(2, 0), e(2, 1), e(2, 2)}, {e(3, 0), e(3, 1), e(3, 2)}, {e(4, 0), e(4, 1), e(4, 2)}, {e(5, 0), e(5, 1), e(5, 2)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Int, 1, 4>, Derived, ::armarx::Matrix_1_4_i> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2), e(0, 3)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Int, 2, 4>, Derived, ::armarx::Matrix_2_4_i> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2), e(0, 3)}, {e(1, 0), e(1, 1), e(1, 2), e(1, 3)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Int, 3, 4>, Derived, ::armarx::Matrix_3_4_i> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2), e(0, 3)}, {e(1, 0), e(1, 1), e(1, 2), e(1, 3)}, {e(2, 0), e(2, 1), e(2, 2), e(2, 3)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Int, 4, 4>, Derived, ::armarx::Matrix_4_4_i> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2), e(0, 3)}, {e(1, 0), e(1, 1), e(1, 2), e(1, 3)}, {e(2, 0), e(2, 1), e(2, 2), e(2, 3)}, {e(3, 0), e(3, 1), e(3, 2), e(3, 3)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Int, 5, 4>, Derived, ::armarx::Matrix_5_4_i> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2), e(0, 3)}, {e(1, 0), e(1, 1), e(1, 2), e(1, 3)}, {e(2, 0), e(2, 1), e(2, 2), e(2, 3)}, {e(3, 0), e(3, 1), e(3, 2), e(3, 3)}, {e(4, 0), e(4, 1), e(4, 2), e(4, 3)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Int, 6, 4>, Derived, ::armarx::Matrix_6_4_i> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2), e(0, 3)}, {e(1, 0), e(1, 1), e(1, 2), e(1, 3)}, {e(2, 0), e(2, 1), e(2, 2), e(2, 3)}, {e(3, 0), e(3, 1), e(3, 2), e(3, 3)}, {e(4, 0), e(4, 1), e(4, 2), e(4, 3)}, {e(5, 0), e(5, 1), e(5, 2), e(5, 3)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Int, 1, 5>, Derived, ::armarx::Matrix_1_5_i> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2), e(0, 3), e(0, 4)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Int, 2, 5>, Derived, ::armarx::Matrix_2_5_i> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2), e(0, 3), e(0, 4)}, {e(1, 0), e(1, 1), e(1, 2), e(1, 3), e(1, 4)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Int, 3, 5>, Derived, ::armarx::Matrix_3_5_i> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2), e(0, 3), e(0, 4)}, {e(1, 0), e(1, 1), e(1, 2), e(1, 3), e(1, 4)}, {e(2, 0), e(2, 1), e(2, 2), e(2, 3), e(2, 4)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Int, 4, 5>, Derived, ::armarx::Matrix_4_5_i> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2), e(0, 3), e(0, 4)}, {e(1, 0), e(1, 1), e(1, 2), e(1, 3), e(1, 4)}, {e(2, 0), e(2, 1), e(2, 2), e(2, 3), e(2, 4)}, {e(3, 0), e(3, 1), e(3, 2), e(3, 3), e(3, 4)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Int, 5, 5>, Derived, ::armarx::Matrix_5_5_i> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2), e(0, 3), e(0, 4)}, {e(1, 0), e(1, 1), e(1, 2), e(1, 3), e(1, 4)}, {e(2, 0), e(2, 1), e(2, 2), e(2, 3), e(2, 4)}, {e(3, 0), e(3, 1), e(3, 2), e(3, 3), e(3, 4)}, {e(4, 0), e(4, 1), e(4, 2), e(4, 3), e(4, 4)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Int, 6, 5>, Derived, ::armarx::Matrix_6_5_i> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2), e(0, 3), e(0, 4)}, {e(1, 0), e(1, 1), e(1, 2), e(1, 3), e(1, 4)}, {e(2, 0), e(2, 1), e(2, 2), e(2, 3), e(2, 4)}, {e(3, 0), e(3, 1), e(3, 2), e(3, 3), e(3, 4)}, {e(4, 0), e(4, 1), e(4, 2), e(4, 3), e(4, 4)}, {e(5, 0), e(5, 1), e(5, 2), e(5, 3), e(5, 4)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Int, 1, 6>, Derived, ::armarx::Matrix_1_6_i> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2), e(0, 3), e(0, 4), e(0, 5)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Int, 2, 6>, Derived, ::armarx::Matrix_2_6_i> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2), e(0, 3), e(0, 4), e(0, 5)}, {e(1, 0), e(1, 1), e(1, 2), e(1, 3), e(1, 4), e(1, 5)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Int, 3, 6>, Derived, ::armarx::Matrix_3_6_i> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2), e(0, 3), e(0, 4), e(0, 5)}, {e(1, 0), e(1, 1), e(1, 2), e(1, 3), e(1, 4), e(1, 5)}, {e(2, 0), e(2, 1), e(2, 2), e(2, 3), e(2, 4), e(2, 5)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Int, 4, 6>, Derived, ::armarx::Matrix_4_6_i> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2), e(0, 3), e(0, 4), e(0, 5)}, {e(1, 0), e(1, 1), e(1, 2), e(1, 3), e(1, 4), e(1, 5)}, {e(2, 0), e(2, 1), e(2, 2), e(2, 3), e(2, 4), e(2, 5)}, {e(3, 0), e(3, 1), e(3, 2), e(3, 3), e(3, 4), e(3, 5)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Int, 5, 6>, Derived, ::armarx::Matrix_5_6_i> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2), e(0, 3), e(0, 4), e(0, 5)}, {e(1, 0), e(1, 1), e(1, 2), e(1, 3), e(1, 4), e(1, 5)}, {e(2, 0), e(2, 1), e(2, 2), e(2, 3), e(2, 4), e(2, 5)}, {e(3, 0), e(3, 1), e(3, 2), e(3, 3), e(3, 4), e(3, 5)}, {e(4, 0), e(4, 1), e(4, 2), e(4, 3), e(4, 4), e(4, 5)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Int, 6, 6>, Derived, ::armarx::Matrix_6_6_i> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2), e(0, 3), e(0, 4), e(0, 5)}, {e(1, 0), e(1, 1), e(1, 2), e(1, 3), e(1, 4), e(1, 5)}, {e(2, 0), e(2, 1), e(2, 2), e(2, 3), e(2, 4), e(2, 5)}, {e(3, 0), e(3, 1), e(3, 2), e(3, 3), e(3, 4), e(3, 5)}, {e(4, 0), e(4, 1), e(4, 2), e(4, 3), e(4, 4), e(4, 5)}, {e(5, 0), e(5, 1), e(5, 2), e(5, 3), e(5, 4), e(5, 5)}};
    } inline ::Eigen::Matrix< ::Ice::Int, 1, 1> ToEigen(const ::armarx::Vector1i& b)
    {
        ::Eigen::Matrix< ::Ice::Int, 1, 1> m;
        m << b.e0;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Int, 2, 1> ToEigen(const ::armarx::Vector2i& b)
    {
        ::Eigen::Matrix< ::Ice::Int, 2, 1> m;
        m << b.e0, b.e1;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Int, 3, 1> ToEigen(const ::armarx::Vector3i& b)
    {
        ::Eigen::Matrix< ::Ice::Int, 3, 1> m;
        m << b.e0, b.e1, b.e2;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Int, 4, 1> ToEigen(const ::armarx::Vector4i& b)
    {
        ::Eigen::Matrix< ::Ice::Int, 4, 1> m;
        m << b.e0, b.e1, b.e2, b.e3;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Int, 5, 1> ToEigen(const ::armarx::Vector5i& b)
    {
        ::Eigen::Matrix< ::Ice::Int, 5, 1> m;
        m << b.e0, b.e1, b.e2, b.e3, b.e4;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Int, 6, 1> ToEigen(const ::armarx::Vector6i& b)
    {
        ::Eigen::Matrix< ::Ice::Int, 6, 1> m;
        m << b.e0, b.e1, b.e2, b.e3, b.e4, b.e5;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Int, 1, 1> ToEigen(const ::armarx::Matrix_1_1_i& b)
    {
        ::Eigen::Matrix< ::Ice::Int, 1, 1> m;
        m << b.e0.e0;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Int, 2, 1> ToEigen(const ::armarx::Matrix_2_1_i& b)
    {
        ::Eigen::Matrix< ::Ice::Int, 2, 1> m;
        m << b.e0.e0, b.e1.e0;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Int, 3, 1> ToEigen(const ::armarx::Matrix_3_1_i& b)
    {
        ::Eigen::Matrix< ::Ice::Int, 3, 1> m;
        m << b.e0.e0, b.e1.e0, b.e2.e0;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Int, 4, 1> ToEigen(const ::armarx::Matrix_4_1_i& b)
    {
        ::Eigen::Matrix< ::Ice::Int, 4, 1> m;
        m << b.e0.e0, b.e1.e0, b.e2.e0, b.e3.e0;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Int, 5, 1> ToEigen(const ::armarx::Matrix_5_1_i& b)
    {
        ::Eigen::Matrix< ::Ice::Int, 5, 1> m;
        m << b.e0.e0, b.e1.e0, b.e2.e0, b.e3.e0, b.e4.e0;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Int, 6, 1> ToEigen(const ::armarx::Matrix_6_1_i& b)
    {
        ::Eigen::Matrix< ::Ice::Int, 6, 1> m;
        m << b.e0.e0, b.e1.e0, b.e2.e0, b.e3.e0, b.e4.e0, b.e5.e0;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Int, 1, 2> ToEigen(const ::armarx::Matrix_1_2_i& b)
    {
        ::Eigen::Matrix< ::Ice::Int, 1, 2> m;
        m << b.e0.e0, b.e0.e1;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Int, 2, 2> ToEigen(const ::armarx::Matrix_2_2_i& b)
    {
        ::Eigen::Matrix< ::Ice::Int, 2, 2> m;
        m << b.e0.e0, b.e0.e1, b.e1.e0, b.e1.e1;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Int, 3, 2> ToEigen(const ::armarx::Matrix_3_2_i& b)
    {
        ::Eigen::Matrix< ::Ice::Int, 3, 2> m;
        m << b.e0.e0, b.e0.e1, b.e1.e0, b.e1.e1, b.e2.e0, b.e2.e1;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Int, 4, 2> ToEigen(const ::armarx::Matrix_4_2_i& b)
    {
        ::Eigen::Matrix< ::Ice::Int, 4, 2> m;
        m << b.e0.e0, b.e0.e1, b.e1.e0, b.e1.e1, b.e2.e0, b.e2.e1, b.e3.e0, b.e3.e1;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Int, 5, 2> ToEigen(const ::armarx::Matrix_5_2_i& b)
    {
        ::Eigen::Matrix< ::Ice::Int, 5, 2> m;
        m << b.e0.e0, b.e0.e1, b.e1.e0, b.e1.e1, b.e2.e0, b.e2.e1, b.e3.e0, b.e3.e1, b.e4.e0, b.e4.e1;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Int, 6, 2> ToEigen(const ::armarx::Matrix_6_2_i& b)
    {
        ::Eigen::Matrix< ::Ice::Int, 6, 2> m;
        m << b.e0.e0, b.e0.e1, b.e1.e0, b.e1.e1, b.e2.e0, b.e2.e1, b.e3.e0, b.e3.e1, b.e4.e0, b.e4.e1, b.e5.e0, b.e5.e1;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Int, 1, 3> ToEigen(const ::armarx::Matrix_1_3_i& b)
    {
        ::Eigen::Matrix< ::Ice::Int, 1, 3> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Int, 2, 3> ToEigen(const ::armarx::Matrix_2_3_i& b)
    {
        ::Eigen::Matrix< ::Ice::Int, 2, 3> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2, b.e1.e0, b.e1.e1, b.e1.e2;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Int, 3, 3> ToEigen(const ::armarx::Matrix_3_3_i& b)
    {
        ::Eigen::Matrix< ::Ice::Int, 3, 3> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2, b.e1.e0, b.e1.e1, b.e1.e2, b.e2.e0, b.e2.e1, b.e2.e2;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Int, 4, 3> ToEigen(const ::armarx::Matrix_4_3_i& b)
    {
        ::Eigen::Matrix< ::Ice::Int, 4, 3> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2, b.e1.e0, b.e1.e1, b.e1.e2, b.e2.e0, b.e2.e1, b.e2.e2, b.e3.e0, b.e3.e1, b.e3.e2;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Int, 5, 3> ToEigen(const ::armarx::Matrix_5_3_i& b)
    {
        ::Eigen::Matrix< ::Ice::Int, 5, 3> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2, b.e1.e0, b.e1.e1, b.e1.e2, b.e2.e0, b.e2.e1, b.e2.e2, b.e3.e0, b.e3.e1, b.e3.e2, b.e4.e0, b.e4.e1, b.e4.e2;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Int, 6, 3> ToEigen(const ::armarx::Matrix_6_3_i& b)
    {
        ::Eigen::Matrix< ::Ice::Int, 6, 3> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2, b.e1.e0, b.e1.e1, b.e1.e2, b.e2.e0, b.e2.e1, b.e2.e2, b.e3.e0, b.e3.e1, b.e3.e2, b.e4.e0, b.e4.e1, b.e4.e2, b.e5.e0, b.e5.e1, b.e5.e2;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Int, 1, 4> ToEigen(const ::armarx::Matrix_1_4_i& b)
    {
        ::Eigen::Matrix< ::Ice::Int, 1, 4> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2, b.e0.e3;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Int, 2, 4> ToEigen(const ::armarx::Matrix_2_4_i& b)
    {
        ::Eigen::Matrix< ::Ice::Int, 2, 4> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2, b.e0.e3, b.e1.e0, b.e1.e1, b.e1.e2, b.e1.e3;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Int, 3, 4> ToEigen(const ::armarx::Matrix_3_4_i& b)
    {
        ::Eigen::Matrix< ::Ice::Int, 3, 4> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2, b.e0.e3, b.e1.e0, b.e1.e1, b.e1.e2, b.e1.e3, b.e2.e0, b.e2.e1, b.e2.e2, b.e2.e3;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Int, 4, 4> ToEigen(const ::armarx::Matrix_4_4_i& b)
    {
        ::Eigen::Matrix< ::Ice::Int, 4, 4> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2, b.e0.e3, b.e1.e0, b.e1.e1, b.e1.e2, b.e1.e3, b.e2.e0, b.e2.e1, b.e2.e2, b.e2.e3, b.e3.e0, b.e3.e1, b.e3.e2, b.e3.e3;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Int, 5, 4> ToEigen(const ::armarx::Matrix_5_4_i& b)
    {
        ::Eigen::Matrix< ::Ice::Int, 5, 4> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2, b.e0.e3, b.e1.e0, b.e1.e1, b.e1.e2, b.e1.e3, b.e2.e0, b.e2.e1, b.e2.e2, b.e2.e3, b.e3.e0, b.e3.e1, b.e3.e2, b.e3.e3, b.e4.e0, b.e4.e1, b.e4.e2, b.e4.e3;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Int, 6, 4> ToEigen(const ::armarx::Matrix_6_4_i& b)
    {
        ::Eigen::Matrix< ::Ice::Int, 6, 4> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2, b.e0.e3, b.e1.e0, b.e1.e1, b.e1.e2, b.e1.e3, b.e2.e0, b.e2.e1, b.e2.e2, b.e2.e3, b.e3.e0, b.e3.e1, b.e3.e2, b.e3.e3, b.e4.e0, b.e4.e1, b.e4.e2, b.e4.e3, b.e5.e0, b.e5.e1, b.e5.e2, b.e5.e3;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Int, 1, 5> ToEigen(const ::armarx::Matrix_1_5_i& b)
    {
        ::Eigen::Matrix< ::Ice::Int, 1, 5> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2, b.e0.e3, b.e0.e4;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Int, 2, 5> ToEigen(const ::armarx::Matrix_2_5_i& b)
    {
        ::Eigen::Matrix< ::Ice::Int, 2, 5> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2, b.e0.e3, b.e0.e4, b.e1.e0, b.e1.e1, b.e1.e2, b.e1.e3, b.e1.e4;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Int, 3, 5> ToEigen(const ::armarx::Matrix_3_5_i& b)
    {
        ::Eigen::Matrix< ::Ice::Int, 3, 5> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2, b.e0.e3, b.e0.e4, b.e1.e0, b.e1.e1, b.e1.e2, b.e1.e3, b.e1.e4, b.e2.e0, b.e2.e1, b.e2.e2, b.e2.e3, b.e2.e4;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Int, 4, 5> ToEigen(const ::armarx::Matrix_4_5_i& b)
    {
        ::Eigen::Matrix< ::Ice::Int, 4, 5> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2, b.e0.e3, b.e0.e4, b.e1.e0, b.e1.e1, b.e1.e2, b.e1.e3, b.e1.e4, b.e2.e0, b.e2.e1, b.e2.e2, b.e2.e3, b.e2.e4, b.e3.e0, b.e3.e1, b.e3.e2, b.e3.e3, b.e3.e4;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Int, 5, 5> ToEigen(const ::armarx::Matrix_5_5_i& b)
    {
        ::Eigen::Matrix< ::Ice::Int, 5, 5> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2, b.e0.e3, b.e0.e4, b.e1.e0, b.e1.e1, b.e1.e2, b.e1.e3, b.e1.e4, b.e2.e0, b.e2.e1, b.e2.e2, b.e2.e3, b.e2.e4, b.e3.e0, b.e3.e1, b.e3.e2, b.e3.e3, b.e3.e4, b.e4.e0, b.e4.e1, b.e4.e2, b.e4.e3, b.e4.e4;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Int, 6, 5> ToEigen(const ::armarx::Matrix_6_5_i& b)
    {
        ::Eigen::Matrix< ::Ice::Int, 6, 5> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2, b.e0.e3, b.e0.e4, b.e1.e0, b.e1.e1, b.e1.e2, b.e1.e3, b.e1.e4, b.e2.e0, b.e2.e1, b.e2.e2, b.e2.e3, b.e2.e4, b.e3.e0, b.e3.e1, b.e3.e2, b.e3.e3, b.e3.e4, b.e4.e0, b.e4.e1, b.e4.e2, b.e4.e3, b.e4.e4, b.e5.e0, b.e5.e1, b.e5.e2, b.e5.e3, b.e5.e4;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Int, 1, 6> ToEigen(const ::armarx::Matrix_1_6_i& b)
    {
        ::Eigen::Matrix< ::Ice::Int, 1, 6> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2, b.e0.e3, b.e0.e4, b.e0.e5;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Int, 2, 6> ToEigen(const ::armarx::Matrix_2_6_i& b)
    {
        ::Eigen::Matrix< ::Ice::Int, 2, 6> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2, b.e0.e3, b.e0.e4, b.e0.e5, b.e1.e0, b.e1.e1, b.e1.e2, b.e1.e3, b.e1.e4, b.e1.e5;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Int, 3, 6> ToEigen(const ::armarx::Matrix_3_6_i& b)
    {
        ::Eigen::Matrix< ::Ice::Int, 3, 6> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2, b.e0.e3, b.e0.e4, b.e0.e5, b.e1.e0, b.e1.e1, b.e1.e2, b.e1.e3, b.e1.e4, b.e1.e5, b.e2.e0, b.e2.e1, b.e2.e2, b.e2.e3, b.e2.e4, b.e2.e5;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Int, 4, 6> ToEigen(const ::armarx::Matrix_4_6_i& b)
    {
        ::Eigen::Matrix< ::Ice::Int, 4, 6> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2, b.e0.e3, b.e0.e4, b.e0.e5, b.e1.e0, b.e1.e1, b.e1.e2, b.e1.e3, b.e1.e4, b.e1.e5, b.e2.e0, b.e2.e1, b.e2.e2, b.e2.e3, b.e2.e4, b.e2.e5, b.e3.e0, b.e3.e1, b.e3.e2, b.e3.e3, b.e3.e4, b.e3.e5;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Int, 5, 6> ToEigen(const ::armarx::Matrix_5_6_i& b)
    {
        ::Eigen::Matrix< ::Ice::Int, 5, 6> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2, b.e0.e3, b.e0.e4, b.e0.e5, b.e1.e0, b.e1.e1, b.e1.e2, b.e1.e3, b.e1.e4, b.e1.e5, b.e2.e0, b.e2.e1, b.e2.e2, b.e2.e3, b.e2.e4, b.e2.e5, b.e3.e0, b.e3.e1, b.e3.e2, b.e3.e3, b.e3.e4, b.e3.e5, b.e4.e0, b.e4.e1, b.e4.e2, b.e4.e3, b.e4.e4, b.e4.e5;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Int, 6, 6> ToEigen(const ::armarx::Matrix_6_6_i& b)
    {
        ::Eigen::Matrix< ::Ice::Int, 6, 6> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2, b.e0.e3, b.e0.e4, b.e0.e5, b.e1.e0, b.e1.e1, b.e1.e2, b.e1.e3, b.e1.e4, b.e1.e5, b.e2.e0, b.e2.e1, b.e2.e2, b.e2.e3, b.e2.e4, b.e2.e5, b.e3.e0, b.e3.e1, b.e3.e2, b.e3.e3, b.e3.e4, b.e3.e5, b.e4.e0, b.e4.e1, b.e4.e2, b.e4.e3, b.e4.e4, b.e4.e5, b.e5.e0, b.e5.e1, b.e5.e2, b.e5.e3, b.e5.e4, b.e5.e5;
        return m;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Vector1i& r)
    {
        detail::bvt::AllOut(l, '\t', r.e0);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Vector2i& r)
    {
        detail::bvt::AllOut(l, '\t', r.e0, r.e1);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Vector3i& r)
    {
        detail::bvt::AllOut(l, '\t', r.e0, r.e1, r.e2);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Vector4i& r)
    {
        detail::bvt::AllOut(l, '\t', r.e0, r.e1, r.e2, r.e3);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Vector5i& r)
    {
        detail::bvt::AllOut(l, '\t', r.e0, r.e1, r.e2, r.e3, r.e4);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Vector6i& r)
    {
        detail::bvt::AllOut(l, '\t', r.e0, r.e1, r.e2, r.e3, r.e4, r.e5);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_1_1_i& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_2_1_i& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_3_1_i& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_4_1_i& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2, r.e3);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_5_1_i& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2, r.e3, r.e4);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_6_1_i& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2, r.e3, r.e4, r.e5);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_1_2_i& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_2_2_i& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_3_2_i& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_4_2_i& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2, r.e3);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_5_2_i& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2, r.e3, r.e4);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_6_2_i& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2, r.e3, r.e4, r.e5);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_1_3_i& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_2_3_i& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_3_3_i& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_4_3_i& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2, r.e3);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_5_3_i& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2, r.e3, r.e4);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_6_3_i& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2, r.e3, r.e4, r.e5);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_1_4_i& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_2_4_i& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_3_4_i& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_4_4_i& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2, r.e3);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_5_4_i& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2, r.e3, r.e4);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_6_4_i& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2, r.e3, r.e4, r.e5);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_1_5_i& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_2_5_i& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_3_5_i& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_4_5_i& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2, r.e3);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_5_5_i& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2, r.e3, r.e4);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_6_5_i& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2, r.e3, r.e4, r.e5);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_1_6_i& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_2_6_i& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_3_6_i& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_4_6_i& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2, r.e3);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_5_6_i& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2, r.e3, r.e4);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_6_6_i& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2, r.e3, r.e4, r.e5);
        return l;
    }
    template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Long, 1, 1>, Derived, ::armarx::Vector1l> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {e(0)};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Long, 2, 1>, Derived, ::armarx::Vector2l> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {e(0), e(1)};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Long, 3, 1>, Derived, ::armarx::Vector3l> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {e(0), e(1), e(2)};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Long, 4, 1>, Derived, ::armarx::Vector4l> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {e(0), e(1), e(2), e(3)};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Long, 5, 1>, Derived, ::armarx::Vector5l> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {e(0), e(1), e(2), e(3), e(4)};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Long, 6, 1>, Derived, ::armarx::Vector6l> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {e(0), e(1), e(2), e(3), e(4), e(5)};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Long, 1, 2>, Derived, ::armarx::Matrix_1_2_l> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Long, 2, 2>, Derived, ::armarx::Matrix_2_2_l> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1)}, {e(1, 0), e(1, 1)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Long, 3, 2>, Derived, ::armarx::Matrix_3_2_l> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1)}, {e(1, 0), e(1, 1)}, {e(2, 0), e(2, 1)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Long, 4, 2>, Derived, ::armarx::Matrix_4_2_l> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1)}, {e(1, 0), e(1, 1)}, {e(2, 0), e(2, 1)}, {e(3, 0), e(3, 1)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Long, 5, 2>, Derived, ::armarx::Matrix_5_2_l> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1)}, {e(1, 0), e(1, 1)}, {e(2, 0), e(2, 1)}, {e(3, 0), e(3, 1)}, {e(4, 0), e(4, 1)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Long, 6, 2>, Derived, ::armarx::Matrix_6_2_l> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1)}, {e(1, 0), e(1, 1)}, {e(2, 0), e(2, 1)}, {e(3, 0), e(3, 1)}, {e(4, 0), e(4, 1)}, {e(5, 0), e(5, 1)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Long, 1, 3>, Derived, ::armarx::Matrix_1_3_l> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Long, 2, 3>, Derived, ::armarx::Matrix_2_3_l> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2)}, {e(1, 0), e(1, 1), e(1, 2)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Long, 3, 3>, Derived, ::armarx::Matrix_3_3_l> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2)}, {e(1, 0), e(1, 1), e(1, 2)}, {e(2, 0), e(2, 1), e(2, 2)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Long, 4, 3>, Derived, ::armarx::Matrix_4_3_l> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2)}, {e(1, 0), e(1, 1), e(1, 2)}, {e(2, 0), e(2, 1), e(2, 2)}, {e(3, 0), e(3, 1), e(3, 2)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Long, 5, 3>, Derived, ::armarx::Matrix_5_3_l> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2)}, {e(1, 0), e(1, 1), e(1, 2)}, {e(2, 0), e(2, 1), e(2, 2)}, {e(3, 0), e(3, 1), e(3, 2)}, {e(4, 0), e(4, 1), e(4, 2)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Long, 6, 3>, Derived, ::armarx::Matrix_6_3_l> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2)}, {e(1, 0), e(1, 1), e(1, 2)}, {e(2, 0), e(2, 1), e(2, 2)}, {e(3, 0), e(3, 1), e(3, 2)}, {e(4, 0), e(4, 1), e(4, 2)}, {e(5, 0), e(5, 1), e(5, 2)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Long, 1, 4>, Derived, ::armarx::Matrix_1_4_l> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2), e(0, 3)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Long, 2, 4>, Derived, ::armarx::Matrix_2_4_l> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2), e(0, 3)}, {e(1, 0), e(1, 1), e(1, 2), e(1, 3)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Long, 3, 4>, Derived, ::armarx::Matrix_3_4_l> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2), e(0, 3)}, {e(1, 0), e(1, 1), e(1, 2), e(1, 3)}, {e(2, 0), e(2, 1), e(2, 2), e(2, 3)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Long, 4, 4>, Derived, ::armarx::Matrix_4_4_l> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2), e(0, 3)}, {e(1, 0), e(1, 1), e(1, 2), e(1, 3)}, {e(2, 0), e(2, 1), e(2, 2), e(2, 3)}, {e(3, 0), e(3, 1), e(3, 2), e(3, 3)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Long, 5, 4>, Derived, ::armarx::Matrix_5_4_l> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2), e(0, 3)}, {e(1, 0), e(1, 1), e(1, 2), e(1, 3)}, {e(2, 0), e(2, 1), e(2, 2), e(2, 3)}, {e(3, 0), e(3, 1), e(3, 2), e(3, 3)}, {e(4, 0), e(4, 1), e(4, 2), e(4, 3)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Long, 6, 4>, Derived, ::armarx::Matrix_6_4_l> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2), e(0, 3)}, {e(1, 0), e(1, 1), e(1, 2), e(1, 3)}, {e(2, 0), e(2, 1), e(2, 2), e(2, 3)}, {e(3, 0), e(3, 1), e(3, 2), e(3, 3)}, {e(4, 0), e(4, 1), e(4, 2), e(4, 3)}, {e(5, 0), e(5, 1), e(5, 2), e(5, 3)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Long, 1, 5>, Derived, ::armarx::Matrix_1_5_l> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2), e(0, 3), e(0, 4)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Long, 2, 5>, Derived, ::armarx::Matrix_2_5_l> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2), e(0, 3), e(0, 4)}, {e(1, 0), e(1, 1), e(1, 2), e(1, 3), e(1, 4)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Long, 3, 5>, Derived, ::armarx::Matrix_3_5_l> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2), e(0, 3), e(0, 4)}, {e(1, 0), e(1, 1), e(1, 2), e(1, 3), e(1, 4)}, {e(2, 0), e(2, 1), e(2, 2), e(2, 3), e(2, 4)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Long, 4, 5>, Derived, ::armarx::Matrix_4_5_l> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2), e(0, 3), e(0, 4)}, {e(1, 0), e(1, 1), e(1, 2), e(1, 3), e(1, 4)}, {e(2, 0), e(2, 1), e(2, 2), e(2, 3), e(2, 4)}, {e(3, 0), e(3, 1), e(3, 2), e(3, 3), e(3, 4)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Long, 5, 5>, Derived, ::armarx::Matrix_5_5_l> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2), e(0, 3), e(0, 4)}, {e(1, 0), e(1, 1), e(1, 2), e(1, 3), e(1, 4)}, {e(2, 0), e(2, 1), e(2, 2), e(2, 3), e(2, 4)}, {e(3, 0), e(3, 1), e(3, 2), e(3, 3), e(3, 4)}, {e(4, 0), e(4, 1), e(4, 2), e(4, 3), e(4, 4)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Long, 6, 5>, Derived, ::armarx::Matrix_6_5_l> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2), e(0, 3), e(0, 4)}, {e(1, 0), e(1, 1), e(1, 2), e(1, 3), e(1, 4)}, {e(2, 0), e(2, 1), e(2, 2), e(2, 3), e(2, 4)}, {e(3, 0), e(3, 1), e(3, 2), e(3, 3), e(3, 4)}, {e(4, 0), e(4, 1), e(4, 2), e(4, 3), e(4, 4)}, {e(5, 0), e(5, 1), e(5, 2), e(5, 3), e(5, 4)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Long, 1, 6>, Derived, ::armarx::Matrix_1_6_l> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2), e(0, 3), e(0, 4), e(0, 5)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Long, 2, 6>, Derived, ::armarx::Matrix_2_6_l> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2), e(0, 3), e(0, 4), e(0, 5)}, {e(1, 0), e(1, 1), e(1, 2), e(1, 3), e(1, 4), e(1, 5)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Long, 3, 6>, Derived, ::armarx::Matrix_3_6_l> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2), e(0, 3), e(0, 4), e(0, 5)}, {e(1, 0), e(1, 1), e(1, 2), e(1, 3), e(1, 4), e(1, 5)}, {e(2, 0), e(2, 1), e(2, 2), e(2, 3), e(2, 4), e(2, 5)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Long, 4, 6>, Derived, ::armarx::Matrix_4_6_l> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2), e(0, 3), e(0, 4), e(0, 5)}, {e(1, 0), e(1, 1), e(1, 2), e(1, 3), e(1, 4), e(1, 5)}, {e(2, 0), e(2, 1), e(2, 2), e(2, 3), e(2, 4), e(2, 5)}, {e(3, 0), e(3, 1), e(3, 2), e(3, 3), e(3, 4), e(3, 5)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Long, 5, 6>, Derived, ::armarx::Matrix_5_6_l> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2), e(0, 3), e(0, 4), e(0, 5)}, {e(1, 0), e(1, 1), e(1, 2), e(1, 3), e(1, 4), e(1, 5)}, {e(2, 0), e(2, 1), e(2, 2), e(2, 3), e(2, 4), e(2, 5)}, {e(3, 0), e(3, 1), e(3, 2), e(3, 3), e(3, 4), e(3, 5)}, {e(4, 0), e(4, 1), e(4, 2), e(4, 3), e(4, 4), e(4, 5)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Long, 6, 6>, Derived, ::armarx::Matrix_6_6_l> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2), e(0, 3), e(0, 4), e(0, 5)}, {e(1, 0), e(1, 1), e(1, 2), e(1, 3), e(1, 4), e(1, 5)}, {e(2, 0), e(2, 1), e(2, 2), e(2, 3), e(2, 4), e(2, 5)}, {e(3, 0), e(3, 1), e(3, 2), e(3, 3), e(3, 4), e(3, 5)}, {e(4, 0), e(4, 1), e(4, 2), e(4, 3), e(4, 4), e(4, 5)}, {e(5, 0), e(5, 1), e(5, 2), e(5, 3), e(5, 4), e(5, 5)}};
    } inline ::Eigen::Matrix< ::Ice::Long, 1, 1> ToEigen(const ::armarx::Vector1l& b)
    {
        ::Eigen::Matrix< ::Ice::Long, 1, 1> m;
        m << b.e0;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Long, 2, 1> ToEigen(const ::armarx::Vector2l& b)
    {
        ::Eigen::Matrix< ::Ice::Long, 2, 1> m;
        m << b.e0, b.e1;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Long, 3, 1> ToEigen(const ::armarx::Vector3l& b)
    {
        ::Eigen::Matrix< ::Ice::Long, 3, 1> m;
        m << b.e0, b.e1, b.e2;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Long, 4, 1> ToEigen(const ::armarx::Vector4l& b)
    {
        ::Eigen::Matrix< ::Ice::Long, 4, 1> m;
        m << b.e0, b.e1, b.e2, b.e3;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Long, 5, 1> ToEigen(const ::armarx::Vector5l& b)
    {
        ::Eigen::Matrix< ::Ice::Long, 5, 1> m;
        m << b.e0, b.e1, b.e2, b.e3, b.e4;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Long, 6, 1> ToEigen(const ::armarx::Vector6l& b)
    {
        ::Eigen::Matrix< ::Ice::Long, 6, 1> m;
        m << b.e0, b.e1, b.e2, b.e3, b.e4, b.e5;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Long, 1, 1> ToEigen(const ::armarx::Matrix_1_1_l& b)
    {
        ::Eigen::Matrix< ::Ice::Long, 1, 1> m;
        m << b.e0.e0;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Long, 2, 1> ToEigen(const ::armarx::Matrix_2_1_l& b)
    {
        ::Eigen::Matrix< ::Ice::Long, 2, 1> m;
        m << b.e0.e0, b.e1.e0;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Long, 3, 1> ToEigen(const ::armarx::Matrix_3_1_l& b)
    {
        ::Eigen::Matrix< ::Ice::Long, 3, 1> m;
        m << b.e0.e0, b.e1.e0, b.e2.e0;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Long, 4, 1> ToEigen(const ::armarx::Matrix_4_1_l& b)
    {
        ::Eigen::Matrix< ::Ice::Long, 4, 1> m;
        m << b.e0.e0, b.e1.e0, b.e2.e0, b.e3.e0;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Long, 5, 1> ToEigen(const ::armarx::Matrix_5_1_l& b)
    {
        ::Eigen::Matrix< ::Ice::Long, 5, 1> m;
        m << b.e0.e0, b.e1.e0, b.e2.e0, b.e3.e0, b.e4.e0;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Long, 6, 1> ToEigen(const ::armarx::Matrix_6_1_l& b)
    {
        ::Eigen::Matrix< ::Ice::Long, 6, 1> m;
        m << b.e0.e0, b.e1.e0, b.e2.e0, b.e3.e0, b.e4.e0, b.e5.e0;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Long, 1, 2> ToEigen(const ::armarx::Matrix_1_2_l& b)
    {
        ::Eigen::Matrix< ::Ice::Long, 1, 2> m;
        m << b.e0.e0, b.e0.e1;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Long, 2, 2> ToEigen(const ::armarx::Matrix_2_2_l& b)
    {
        ::Eigen::Matrix< ::Ice::Long, 2, 2> m;
        m << b.e0.e0, b.e0.e1, b.e1.e0, b.e1.e1;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Long, 3, 2> ToEigen(const ::armarx::Matrix_3_2_l& b)
    {
        ::Eigen::Matrix< ::Ice::Long, 3, 2> m;
        m << b.e0.e0, b.e0.e1, b.e1.e0, b.e1.e1, b.e2.e0, b.e2.e1;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Long, 4, 2> ToEigen(const ::armarx::Matrix_4_2_l& b)
    {
        ::Eigen::Matrix< ::Ice::Long, 4, 2> m;
        m << b.e0.e0, b.e0.e1, b.e1.e0, b.e1.e1, b.e2.e0, b.e2.e1, b.e3.e0, b.e3.e1;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Long, 5, 2> ToEigen(const ::armarx::Matrix_5_2_l& b)
    {
        ::Eigen::Matrix< ::Ice::Long, 5, 2> m;
        m << b.e0.e0, b.e0.e1, b.e1.e0, b.e1.e1, b.e2.e0, b.e2.e1, b.e3.e0, b.e3.e1, b.e4.e0, b.e4.e1;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Long, 6, 2> ToEigen(const ::armarx::Matrix_6_2_l& b)
    {
        ::Eigen::Matrix< ::Ice::Long, 6, 2> m;
        m << b.e0.e0, b.e0.e1, b.e1.e0, b.e1.e1, b.e2.e0, b.e2.e1, b.e3.e0, b.e3.e1, b.e4.e0, b.e4.e1, b.e5.e0, b.e5.e1;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Long, 1, 3> ToEigen(const ::armarx::Matrix_1_3_l& b)
    {
        ::Eigen::Matrix< ::Ice::Long, 1, 3> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Long, 2, 3> ToEigen(const ::armarx::Matrix_2_3_l& b)
    {
        ::Eigen::Matrix< ::Ice::Long, 2, 3> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2, b.e1.e0, b.e1.e1, b.e1.e2;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Long, 3, 3> ToEigen(const ::armarx::Matrix_3_3_l& b)
    {
        ::Eigen::Matrix< ::Ice::Long, 3, 3> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2, b.e1.e0, b.e1.e1, b.e1.e2, b.e2.e0, b.e2.e1, b.e2.e2;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Long, 4, 3> ToEigen(const ::armarx::Matrix_4_3_l& b)
    {
        ::Eigen::Matrix< ::Ice::Long, 4, 3> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2, b.e1.e0, b.e1.e1, b.e1.e2, b.e2.e0, b.e2.e1, b.e2.e2, b.e3.e0, b.e3.e1, b.e3.e2;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Long, 5, 3> ToEigen(const ::armarx::Matrix_5_3_l& b)
    {
        ::Eigen::Matrix< ::Ice::Long, 5, 3> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2, b.e1.e0, b.e1.e1, b.e1.e2, b.e2.e0, b.e2.e1, b.e2.e2, b.e3.e0, b.e3.e1, b.e3.e2, b.e4.e0, b.e4.e1, b.e4.e2;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Long, 6, 3> ToEigen(const ::armarx::Matrix_6_3_l& b)
    {
        ::Eigen::Matrix< ::Ice::Long, 6, 3> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2, b.e1.e0, b.e1.e1, b.e1.e2, b.e2.e0, b.e2.e1, b.e2.e2, b.e3.e0, b.e3.e1, b.e3.e2, b.e4.e0, b.e4.e1, b.e4.e2, b.e5.e0, b.e5.e1, b.e5.e2;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Long, 1, 4> ToEigen(const ::armarx::Matrix_1_4_l& b)
    {
        ::Eigen::Matrix< ::Ice::Long, 1, 4> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2, b.e0.e3;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Long, 2, 4> ToEigen(const ::armarx::Matrix_2_4_l& b)
    {
        ::Eigen::Matrix< ::Ice::Long, 2, 4> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2, b.e0.e3, b.e1.e0, b.e1.e1, b.e1.e2, b.e1.e3;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Long, 3, 4> ToEigen(const ::armarx::Matrix_3_4_l& b)
    {
        ::Eigen::Matrix< ::Ice::Long, 3, 4> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2, b.e0.e3, b.e1.e0, b.e1.e1, b.e1.e2, b.e1.e3, b.e2.e0, b.e2.e1, b.e2.e2, b.e2.e3;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Long, 4, 4> ToEigen(const ::armarx::Matrix_4_4_l& b)
    {
        ::Eigen::Matrix< ::Ice::Long, 4, 4> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2, b.e0.e3, b.e1.e0, b.e1.e1, b.e1.e2, b.e1.e3, b.e2.e0, b.e2.e1, b.e2.e2, b.e2.e3, b.e3.e0, b.e3.e1, b.e3.e2, b.e3.e3;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Long, 5, 4> ToEigen(const ::armarx::Matrix_5_4_l& b)
    {
        ::Eigen::Matrix< ::Ice::Long, 5, 4> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2, b.e0.e3, b.e1.e0, b.e1.e1, b.e1.e2, b.e1.e3, b.e2.e0, b.e2.e1, b.e2.e2, b.e2.e3, b.e3.e0, b.e3.e1, b.e3.e2, b.e3.e3, b.e4.e0, b.e4.e1, b.e4.e2, b.e4.e3;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Long, 6, 4> ToEigen(const ::armarx::Matrix_6_4_l& b)
    {
        ::Eigen::Matrix< ::Ice::Long, 6, 4> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2, b.e0.e3, b.e1.e0, b.e1.e1, b.e1.e2, b.e1.e3, b.e2.e0, b.e2.e1, b.e2.e2, b.e2.e3, b.e3.e0, b.e3.e1, b.e3.e2, b.e3.e3, b.e4.e0, b.e4.e1, b.e4.e2, b.e4.e3, b.e5.e0, b.e5.e1, b.e5.e2, b.e5.e3;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Long, 1, 5> ToEigen(const ::armarx::Matrix_1_5_l& b)
    {
        ::Eigen::Matrix< ::Ice::Long, 1, 5> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2, b.e0.e3, b.e0.e4;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Long, 2, 5> ToEigen(const ::armarx::Matrix_2_5_l& b)
    {
        ::Eigen::Matrix< ::Ice::Long, 2, 5> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2, b.e0.e3, b.e0.e4, b.e1.e0, b.e1.e1, b.e1.e2, b.e1.e3, b.e1.e4;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Long, 3, 5> ToEigen(const ::armarx::Matrix_3_5_l& b)
    {
        ::Eigen::Matrix< ::Ice::Long, 3, 5> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2, b.e0.e3, b.e0.e4, b.e1.e0, b.e1.e1, b.e1.e2, b.e1.e3, b.e1.e4, b.e2.e0, b.e2.e1, b.e2.e2, b.e2.e3, b.e2.e4;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Long, 4, 5> ToEigen(const ::armarx::Matrix_4_5_l& b)
    {
        ::Eigen::Matrix< ::Ice::Long, 4, 5> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2, b.e0.e3, b.e0.e4, b.e1.e0, b.e1.e1, b.e1.e2, b.e1.e3, b.e1.e4, b.e2.e0, b.e2.e1, b.e2.e2, b.e2.e3, b.e2.e4, b.e3.e0, b.e3.e1, b.e3.e2, b.e3.e3, b.e3.e4;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Long, 5, 5> ToEigen(const ::armarx::Matrix_5_5_l& b)
    {
        ::Eigen::Matrix< ::Ice::Long, 5, 5> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2, b.e0.e3, b.e0.e4, b.e1.e0, b.e1.e1, b.e1.e2, b.e1.e3, b.e1.e4, b.e2.e0, b.e2.e1, b.e2.e2, b.e2.e3, b.e2.e4, b.e3.e0, b.e3.e1, b.e3.e2, b.e3.e3, b.e3.e4, b.e4.e0, b.e4.e1, b.e4.e2, b.e4.e3, b.e4.e4;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Long, 6, 5> ToEigen(const ::armarx::Matrix_6_5_l& b)
    {
        ::Eigen::Matrix< ::Ice::Long, 6, 5> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2, b.e0.e3, b.e0.e4, b.e1.e0, b.e1.e1, b.e1.e2, b.e1.e3, b.e1.e4, b.e2.e0, b.e2.e1, b.e2.e2, b.e2.e3, b.e2.e4, b.e3.e0, b.e3.e1, b.e3.e2, b.e3.e3, b.e3.e4, b.e4.e0, b.e4.e1, b.e4.e2, b.e4.e3, b.e4.e4, b.e5.e0, b.e5.e1, b.e5.e2, b.e5.e3, b.e5.e4;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Long, 1, 6> ToEigen(const ::armarx::Matrix_1_6_l& b)
    {
        ::Eigen::Matrix< ::Ice::Long, 1, 6> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2, b.e0.e3, b.e0.e4, b.e0.e5;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Long, 2, 6> ToEigen(const ::armarx::Matrix_2_6_l& b)
    {
        ::Eigen::Matrix< ::Ice::Long, 2, 6> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2, b.e0.e3, b.e0.e4, b.e0.e5, b.e1.e0, b.e1.e1, b.e1.e2, b.e1.e3, b.e1.e4, b.e1.e5;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Long, 3, 6> ToEigen(const ::armarx::Matrix_3_6_l& b)
    {
        ::Eigen::Matrix< ::Ice::Long, 3, 6> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2, b.e0.e3, b.e0.e4, b.e0.e5, b.e1.e0, b.e1.e1, b.e1.e2, b.e1.e3, b.e1.e4, b.e1.e5, b.e2.e0, b.e2.e1, b.e2.e2, b.e2.e3, b.e2.e4, b.e2.e5;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Long, 4, 6> ToEigen(const ::armarx::Matrix_4_6_l& b)
    {
        ::Eigen::Matrix< ::Ice::Long, 4, 6> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2, b.e0.e3, b.e0.e4, b.e0.e5, b.e1.e0, b.e1.e1, b.e1.e2, b.e1.e3, b.e1.e4, b.e1.e5, b.e2.e0, b.e2.e1, b.e2.e2, b.e2.e3, b.e2.e4, b.e2.e5, b.e3.e0, b.e3.e1, b.e3.e2, b.e3.e3, b.e3.e4, b.e3.e5;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Long, 5, 6> ToEigen(const ::armarx::Matrix_5_6_l& b)
    {
        ::Eigen::Matrix< ::Ice::Long, 5, 6> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2, b.e0.e3, b.e0.e4, b.e0.e5, b.e1.e0, b.e1.e1, b.e1.e2, b.e1.e3, b.e1.e4, b.e1.e5, b.e2.e0, b.e2.e1, b.e2.e2, b.e2.e3, b.e2.e4, b.e2.e5, b.e3.e0, b.e3.e1, b.e3.e2, b.e3.e3, b.e3.e4, b.e3.e5, b.e4.e0, b.e4.e1, b.e4.e2, b.e4.e3, b.e4.e4, b.e4.e5;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Long, 6, 6> ToEigen(const ::armarx::Matrix_6_6_l& b)
    {
        ::Eigen::Matrix< ::Ice::Long, 6, 6> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2, b.e0.e3, b.e0.e4, b.e0.e5, b.e1.e0, b.e1.e1, b.e1.e2, b.e1.e3, b.e1.e4, b.e1.e5, b.e2.e0, b.e2.e1, b.e2.e2, b.e2.e3, b.e2.e4, b.e2.e5, b.e3.e0, b.e3.e1, b.e3.e2, b.e3.e3, b.e3.e4, b.e3.e5, b.e4.e0, b.e4.e1, b.e4.e2, b.e4.e3, b.e4.e4, b.e4.e5, b.e5.e0, b.e5.e1, b.e5.e2, b.e5.e3, b.e5.e4, b.e5.e5;
        return m;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Vector1l& r)
    {
        detail::bvt::AllOut(l, '\t', r.e0);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Vector2l& r)
    {
        detail::bvt::AllOut(l, '\t', r.e0, r.e1);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Vector3l& r)
    {
        detail::bvt::AllOut(l, '\t', r.e0, r.e1, r.e2);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Vector4l& r)
    {
        detail::bvt::AllOut(l, '\t', r.e0, r.e1, r.e2, r.e3);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Vector5l& r)
    {
        detail::bvt::AllOut(l, '\t', r.e0, r.e1, r.e2, r.e3, r.e4);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Vector6l& r)
    {
        detail::bvt::AllOut(l, '\t', r.e0, r.e1, r.e2, r.e3, r.e4, r.e5);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_1_1_l& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_2_1_l& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_3_1_l& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_4_1_l& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2, r.e3);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_5_1_l& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2, r.e3, r.e4);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_6_1_l& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2, r.e3, r.e4, r.e5);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_1_2_l& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_2_2_l& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_3_2_l& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_4_2_l& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2, r.e3);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_5_2_l& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2, r.e3, r.e4);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_6_2_l& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2, r.e3, r.e4, r.e5);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_1_3_l& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_2_3_l& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_3_3_l& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_4_3_l& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2, r.e3);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_5_3_l& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2, r.e3, r.e4);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_6_3_l& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2, r.e3, r.e4, r.e5);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_1_4_l& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_2_4_l& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_3_4_l& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_4_4_l& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2, r.e3);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_5_4_l& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2, r.e3, r.e4);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_6_4_l& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2, r.e3, r.e4, r.e5);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_1_5_l& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_2_5_l& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_3_5_l& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_4_5_l& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2, r.e3);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_5_5_l& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2, r.e3, r.e4);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_6_5_l& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2, r.e3, r.e4, r.e5);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_1_6_l& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_2_6_l& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_3_6_l& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_4_6_l& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2, r.e3);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_5_6_l& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2, r.e3, r.e4);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_6_6_l& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2, r.e3, r.e4, r.e5);
        return l;
    }
    template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Float, 1, 1>, Derived, ::armarx::Vector1f> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {e(0)};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Float, 2, 1>, Derived, ::armarx::Vector2f> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {e(0), e(1)};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Float, 3, 1>, Derived, ::armarx::Vector3f> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {e(0), e(1), e(2)};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Float, 4, 1>, Derived, ::armarx::Vector4f> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {e(0), e(1), e(2), e(3)};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Float, 5, 1>, Derived, ::armarx::Vector5f> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {e(0), e(1), e(2), e(3), e(4)};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Float, 6, 1>, Derived, ::armarx::Vector6f> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {e(0), e(1), e(2), e(3), e(4), e(5)};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Float, 1, 2>, Derived, ::armarx::Matrix_1_2_f> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Float, 2, 2>, Derived, ::armarx::Matrix_2_2_f> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1)}, {e(1, 0), e(1, 1)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Float, 3, 2>, Derived, ::armarx::Matrix_3_2_f> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1)}, {e(1, 0), e(1, 1)}, {e(2, 0), e(2, 1)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Float, 4, 2>, Derived, ::armarx::Matrix_4_2_f> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1)}, {e(1, 0), e(1, 1)}, {e(2, 0), e(2, 1)}, {e(3, 0), e(3, 1)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Float, 5, 2>, Derived, ::armarx::Matrix_5_2_f> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1)}, {e(1, 0), e(1, 1)}, {e(2, 0), e(2, 1)}, {e(3, 0), e(3, 1)}, {e(4, 0), e(4, 1)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Float, 6, 2>, Derived, ::armarx::Matrix_6_2_f> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1)}, {e(1, 0), e(1, 1)}, {e(2, 0), e(2, 1)}, {e(3, 0), e(3, 1)}, {e(4, 0), e(4, 1)}, {e(5, 0), e(5, 1)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Float, 1, 3>, Derived, ::armarx::Matrix_1_3_f> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Float, 2, 3>, Derived, ::armarx::Matrix_2_3_f> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2)}, {e(1, 0), e(1, 1), e(1, 2)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Float, 3, 3>, Derived, ::armarx::Matrix_3_3_f> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2)}, {e(1, 0), e(1, 1), e(1, 2)}, {e(2, 0), e(2, 1), e(2, 2)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Float, 4, 3>, Derived, ::armarx::Matrix_4_3_f> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2)}, {e(1, 0), e(1, 1), e(1, 2)}, {e(2, 0), e(2, 1), e(2, 2)}, {e(3, 0), e(3, 1), e(3, 2)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Float, 5, 3>, Derived, ::armarx::Matrix_5_3_f> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2)}, {e(1, 0), e(1, 1), e(1, 2)}, {e(2, 0), e(2, 1), e(2, 2)}, {e(3, 0), e(3, 1), e(3, 2)}, {e(4, 0), e(4, 1), e(4, 2)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Float, 6, 3>, Derived, ::armarx::Matrix_6_3_f> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2)}, {e(1, 0), e(1, 1), e(1, 2)}, {e(2, 0), e(2, 1), e(2, 2)}, {e(3, 0), e(3, 1), e(3, 2)}, {e(4, 0), e(4, 1), e(4, 2)}, {e(5, 0), e(5, 1), e(5, 2)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Float, 1, 4>, Derived, ::armarx::Matrix_1_4_f> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2), e(0, 3)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Float, 2, 4>, Derived, ::armarx::Matrix_2_4_f> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2), e(0, 3)}, {e(1, 0), e(1, 1), e(1, 2), e(1, 3)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Float, 3, 4>, Derived, ::armarx::Matrix_3_4_f> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2), e(0, 3)}, {e(1, 0), e(1, 1), e(1, 2), e(1, 3)}, {e(2, 0), e(2, 1), e(2, 2), e(2, 3)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Float, 4, 4>, Derived, ::armarx::Matrix_4_4_f> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2), e(0, 3)}, {e(1, 0), e(1, 1), e(1, 2), e(1, 3)}, {e(2, 0), e(2, 1), e(2, 2), e(2, 3)}, {e(3, 0), e(3, 1), e(3, 2), e(3, 3)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Float, 5, 4>, Derived, ::armarx::Matrix_5_4_f> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2), e(0, 3)}, {e(1, 0), e(1, 1), e(1, 2), e(1, 3)}, {e(2, 0), e(2, 1), e(2, 2), e(2, 3)}, {e(3, 0), e(3, 1), e(3, 2), e(3, 3)}, {e(4, 0), e(4, 1), e(4, 2), e(4, 3)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Float, 6, 4>, Derived, ::armarx::Matrix_6_4_f> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2), e(0, 3)}, {e(1, 0), e(1, 1), e(1, 2), e(1, 3)}, {e(2, 0), e(2, 1), e(2, 2), e(2, 3)}, {e(3, 0), e(3, 1), e(3, 2), e(3, 3)}, {e(4, 0), e(4, 1), e(4, 2), e(4, 3)}, {e(5, 0), e(5, 1), e(5, 2), e(5, 3)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Float, 1, 5>, Derived, ::armarx::Matrix_1_5_f> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2), e(0, 3), e(0, 4)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Float, 2, 5>, Derived, ::armarx::Matrix_2_5_f> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2), e(0, 3), e(0, 4)}, {e(1, 0), e(1, 1), e(1, 2), e(1, 3), e(1, 4)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Float, 3, 5>, Derived, ::armarx::Matrix_3_5_f> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2), e(0, 3), e(0, 4)}, {e(1, 0), e(1, 1), e(1, 2), e(1, 3), e(1, 4)}, {e(2, 0), e(2, 1), e(2, 2), e(2, 3), e(2, 4)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Float, 4, 5>, Derived, ::armarx::Matrix_4_5_f> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2), e(0, 3), e(0, 4)}, {e(1, 0), e(1, 1), e(1, 2), e(1, 3), e(1, 4)}, {e(2, 0), e(2, 1), e(2, 2), e(2, 3), e(2, 4)}, {e(3, 0), e(3, 1), e(3, 2), e(3, 3), e(3, 4)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Float, 5, 5>, Derived, ::armarx::Matrix_5_5_f> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2), e(0, 3), e(0, 4)}, {e(1, 0), e(1, 1), e(1, 2), e(1, 3), e(1, 4)}, {e(2, 0), e(2, 1), e(2, 2), e(2, 3), e(2, 4)}, {e(3, 0), e(3, 1), e(3, 2), e(3, 3), e(3, 4)}, {e(4, 0), e(4, 1), e(4, 2), e(4, 3), e(4, 4)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Float, 6, 5>, Derived, ::armarx::Matrix_6_5_f> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2), e(0, 3), e(0, 4)}, {e(1, 0), e(1, 1), e(1, 2), e(1, 3), e(1, 4)}, {e(2, 0), e(2, 1), e(2, 2), e(2, 3), e(2, 4)}, {e(3, 0), e(3, 1), e(3, 2), e(3, 3), e(3, 4)}, {e(4, 0), e(4, 1), e(4, 2), e(4, 3), e(4, 4)}, {e(5, 0), e(5, 1), e(5, 2), e(5, 3), e(5, 4)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Float, 1, 6>, Derived, ::armarx::Matrix_1_6_f> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2), e(0, 3), e(0, 4), e(0, 5)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Float, 2, 6>, Derived, ::armarx::Matrix_2_6_f> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2), e(0, 3), e(0, 4), e(0, 5)}, {e(1, 0), e(1, 1), e(1, 2), e(1, 3), e(1, 4), e(1, 5)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Float, 3, 6>, Derived, ::armarx::Matrix_3_6_f> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2), e(0, 3), e(0, 4), e(0, 5)}, {e(1, 0), e(1, 1), e(1, 2), e(1, 3), e(1, 4), e(1, 5)}, {e(2, 0), e(2, 1), e(2, 2), e(2, 3), e(2, 4), e(2, 5)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Float, 4, 6>, Derived, ::armarx::Matrix_4_6_f> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2), e(0, 3), e(0, 4), e(0, 5)}, {e(1, 0), e(1, 1), e(1, 2), e(1, 3), e(1, 4), e(1, 5)}, {e(2, 0), e(2, 1), e(2, 2), e(2, 3), e(2, 4), e(2, 5)}, {e(3, 0), e(3, 1), e(3, 2), e(3, 3), e(3, 4), e(3, 5)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Float, 5, 6>, Derived, ::armarx::Matrix_5_6_f> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2), e(0, 3), e(0, 4), e(0, 5)}, {e(1, 0), e(1, 1), e(1, 2), e(1, 3), e(1, 4), e(1, 5)}, {e(2, 0), e(2, 1), e(2, 2), e(2, 3), e(2, 4), e(2, 5)}, {e(3, 0), e(3, 1), e(3, 2), e(3, 3), e(3, 4), e(3, 5)}, {e(4, 0), e(4, 1), e(4, 2), e(4, 3), e(4, 4), e(4, 5)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Float, 6, 6>, Derived, ::armarx::Matrix_6_6_f> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2), e(0, 3), e(0, 4), e(0, 5)}, {e(1, 0), e(1, 1), e(1, 2), e(1, 3), e(1, 4), e(1, 5)}, {e(2, 0), e(2, 1), e(2, 2), e(2, 3), e(2, 4), e(2, 5)}, {e(3, 0), e(3, 1), e(3, 2), e(3, 3), e(3, 4), e(3, 5)}, {e(4, 0), e(4, 1), e(4, 2), e(4, 3), e(4, 4), e(4, 5)}, {e(5, 0), e(5, 1), e(5, 2), e(5, 3), e(5, 4), e(5, 5)}};
    } inline ::Eigen::Matrix< ::Ice::Float, 1, 1> ToEigen(const ::armarx::Vector1f& b)
    {
        ::Eigen::Matrix< ::Ice::Float, 1, 1> m;
        m << b.e0;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Float, 2, 1> ToEigen(const ::armarx::Vector2f& b)
    {
        ::Eigen::Matrix< ::Ice::Float, 2, 1> m;
        m << b.e0, b.e1;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Float, 3, 1> ToEigen(const ::armarx::Vector3f& b)
    {
        ::Eigen::Matrix< ::Ice::Float, 3, 1> m;
        m << b.e0, b.e1, b.e2;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Float, 4, 1> ToEigen(const ::armarx::Vector4f& b)
    {
        ::Eigen::Matrix< ::Ice::Float, 4, 1> m;
        m << b.e0, b.e1, b.e2, b.e3;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Float, 5, 1> ToEigen(const ::armarx::Vector5f& b)
    {
        ::Eigen::Matrix< ::Ice::Float, 5, 1> m;
        m << b.e0, b.e1, b.e2, b.e3, b.e4;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Float, 6, 1> ToEigen(const ::armarx::Vector6f& b)
    {
        ::Eigen::Matrix< ::Ice::Float, 6, 1> m;
        m << b.e0, b.e1, b.e2, b.e3, b.e4, b.e5;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Float, 1, 1> ToEigen(const ::armarx::Matrix_1_1_f& b)
    {
        ::Eigen::Matrix< ::Ice::Float, 1, 1> m;
        m << b.e0.e0;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Float, 2, 1> ToEigen(const ::armarx::Matrix_2_1_f& b)
    {
        ::Eigen::Matrix< ::Ice::Float, 2, 1> m;
        m << b.e0.e0, b.e1.e0;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Float, 3, 1> ToEigen(const ::armarx::Matrix_3_1_f& b)
    {
        ::Eigen::Matrix< ::Ice::Float, 3, 1> m;
        m << b.e0.e0, b.e1.e0, b.e2.e0;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Float, 4, 1> ToEigen(const ::armarx::Matrix_4_1_f& b)
    {
        ::Eigen::Matrix< ::Ice::Float, 4, 1> m;
        m << b.e0.e0, b.e1.e0, b.e2.e0, b.e3.e0;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Float, 5, 1> ToEigen(const ::armarx::Matrix_5_1_f& b)
    {
        ::Eigen::Matrix< ::Ice::Float, 5, 1> m;
        m << b.e0.e0, b.e1.e0, b.e2.e0, b.e3.e0, b.e4.e0;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Float, 6, 1> ToEigen(const ::armarx::Matrix_6_1_f& b)
    {
        ::Eigen::Matrix< ::Ice::Float, 6, 1> m;
        m << b.e0.e0, b.e1.e0, b.e2.e0, b.e3.e0, b.e4.e0, b.e5.e0;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Float, 1, 2> ToEigen(const ::armarx::Matrix_1_2_f& b)
    {
        ::Eigen::Matrix< ::Ice::Float, 1, 2> m;
        m << b.e0.e0, b.e0.e1;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Float, 2, 2> ToEigen(const ::armarx::Matrix_2_2_f& b)
    {
        ::Eigen::Matrix< ::Ice::Float, 2, 2> m;
        m << b.e0.e0, b.e0.e1, b.e1.e0, b.e1.e1;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Float, 3, 2> ToEigen(const ::armarx::Matrix_3_2_f& b)
    {
        ::Eigen::Matrix< ::Ice::Float, 3, 2> m;
        m << b.e0.e0, b.e0.e1, b.e1.e0, b.e1.e1, b.e2.e0, b.e2.e1;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Float, 4, 2> ToEigen(const ::armarx::Matrix_4_2_f& b)
    {
        ::Eigen::Matrix< ::Ice::Float, 4, 2> m;
        m << b.e0.e0, b.e0.e1, b.e1.e0, b.e1.e1, b.e2.e0, b.e2.e1, b.e3.e0, b.e3.e1;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Float, 5, 2> ToEigen(const ::armarx::Matrix_5_2_f& b)
    {
        ::Eigen::Matrix< ::Ice::Float, 5, 2> m;
        m << b.e0.e0, b.e0.e1, b.e1.e0, b.e1.e1, b.e2.e0, b.e2.e1, b.e3.e0, b.e3.e1, b.e4.e0, b.e4.e1;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Float, 6, 2> ToEigen(const ::armarx::Matrix_6_2_f& b)
    {
        ::Eigen::Matrix< ::Ice::Float, 6, 2> m;
        m << b.e0.e0, b.e0.e1, b.e1.e0, b.e1.e1, b.e2.e0, b.e2.e1, b.e3.e0, b.e3.e1, b.e4.e0, b.e4.e1, b.e5.e0, b.e5.e1;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Float, 1, 3> ToEigen(const ::armarx::Matrix_1_3_f& b)
    {
        ::Eigen::Matrix< ::Ice::Float, 1, 3> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Float, 2, 3> ToEigen(const ::armarx::Matrix_2_3_f& b)
    {
        ::Eigen::Matrix< ::Ice::Float, 2, 3> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2, b.e1.e0, b.e1.e1, b.e1.e2;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Float, 3, 3> ToEigen(const ::armarx::Matrix_3_3_f& b)
    {
        ::Eigen::Matrix< ::Ice::Float, 3, 3> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2, b.e1.e0, b.e1.e1, b.e1.e2, b.e2.e0, b.e2.e1, b.e2.e2;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Float, 4, 3> ToEigen(const ::armarx::Matrix_4_3_f& b)
    {
        ::Eigen::Matrix< ::Ice::Float, 4, 3> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2, b.e1.e0, b.e1.e1, b.e1.e2, b.e2.e0, b.e2.e1, b.e2.e2, b.e3.e0, b.e3.e1, b.e3.e2;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Float, 5, 3> ToEigen(const ::armarx::Matrix_5_3_f& b)
    {
        ::Eigen::Matrix< ::Ice::Float, 5, 3> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2, b.e1.e0, b.e1.e1, b.e1.e2, b.e2.e0, b.e2.e1, b.e2.e2, b.e3.e0, b.e3.e1, b.e3.e2, b.e4.e0, b.e4.e1, b.e4.e2;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Float, 6, 3> ToEigen(const ::armarx::Matrix_6_3_f& b)
    {
        ::Eigen::Matrix< ::Ice::Float, 6, 3> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2, b.e1.e0, b.e1.e1, b.e1.e2, b.e2.e0, b.e2.e1, b.e2.e2, b.e3.e0, b.e3.e1, b.e3.e2, b.e4.e0, b.e4.e1, b.e4.e2, b.e5.e0, b.e5.e1, b.e5.e2;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Float, 1, 4> ToEigen(const ::armarx::Matrix_1_4_f& b)
    {
        ::Eigen::Matrix< ::Ice::Float, 1, 4> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2, b.e0.e3;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Float, 2, 4> ToEigen(const ::armarx::Matrix_2_4_f& b)
    {
        ::Eigen::Matrix< ::Ice::Float, 2, 4> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2, b.e0.e3, b.e1.e0, b.e1.e1, b.e1.e2, b.e1.e3;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Float, 3, 4> ToEigen(const ::armarx::Matrix_3_4_f& b)
    {
        ::Eigen::Matrix< ::Ice::Float, 3, 4> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2, b.e0.e3, b.e1.e0, b.e1.e1, b.e1.e2, b.e1.e3, b.e2.e0, b.e2.e1, b.e2.e2, b.e2.e3;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Float, 4, 4> ToEigen(const ::armarx::Matrix_4_4_f& b)
    {
        ::Eigen::Matrix< ::Ice::Float, 4, 4> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2, b.e0.e3, b.e1.e0, b.e1.e1, b.e1.e2, b.e1.e3, b.e2.e0, b.e2.e1, b.e2.e2, b.e2.e3, b.e3.e0, b.e3.e1, b.e3.e2, b.e3.e3;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Float, 5, 4> ToEigen(const ::armarx::Matrix_5_4_f& b)
    {
        ::Eigen::Matrix< ::Ice::Float, 5, 4> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2, b.e0.e3, b.e1.e0, b.e1.e1, b.e1.e2, b.e1.e3, b.e2.e0, b.e2.e1, b.e2.e2, b.e2.e3, b.e3.e0, b.e3.e1, b.e3.e2, b.e3.e3, b.e4.e0, b.e4.e1, b.e4.e2, b.e4.e3;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Float, 6, 4> ToEigen(const ::armarx::Matrix_6_4_f& b)
    {
        ::Eigen::Matrix< ::Ice::Float, 6, 4> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2, b.e0.e3, b.e1.e0, b.e1.e1, b.e1.e2, b.e1.e3, b.e2.e0, b.e2.e1, b.e2.e2, b.e2.e3, b.e3.e0, b.e3.e1, b.e3.e2, b.e3.e3, b.e4.e0, b.e4.e1, b.e4.e2, b.e4.e3, b.e5.e0, b.e5.e1, b.e5.e2, b.e5.e3;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Float, 1, 5> ToEigen(const ::armarx::Matrix_1_5_f& b)
    {
        ::Eigen::Matrix< ::Ice::Float, 1, 5> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2, b.e0.e3, b.e0.e4;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Float, 2, 5> ToEigen(const ::armarx::Matrix_2_5_f& b)
    {
        ::Eigen::Matrix< ::Ice::Float, 2, 5> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2, b.e0.e3, b.e0.e4, b.e1.e0, b.e1.e1, b.e1.e2, b.e1.e3, b.e1.e4;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Float, 3, 5> ToEigen(const ::armarx::Matrix_3_5_f& b)
    {
        ::Eigen::Matrix< ::Ice::Float, 3, 5> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2, b.e0.e3, b.e0.e4, b.e1.e0, b.e1.e1, b.e1.e2, b.e1.e3, b.e1.e4, b.e2.e0, b.e2.e1, b.e2.e2, b.e2.e3, b.e2.e4;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Float, 4, 5> ToEigen(const ::armarx::Matrix_4_5_f& b)
    {
        ::Eigen::Matrix< ::Ice::Float, 4, 5> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2, b.e0.e3, b.e0.e4, b.e1.e0, b.e1.e1, b.e1.e2, b.e1.e3, b.e1.e4, b.e2.e0, b.e2.e1, b.e2.e2, b.e2.e3, b.e2.e4, b.e3.e0, b.e3.e1, b.e3.e2, b.e3.e3, b.e3.e4;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Float, 5, 5> ToEigen(const ::armarx::Matrix_5_5_f& b)
    {
        ::Eigen::Matrix< ::Ice::Float, 5, 5> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2, b.e0.e3, b.e0.e4, b.e1.e0, b.e1.e1, b.e1.e2, b.e1.e3, b.e1.e4, b.e2.e0, b.e2.e1, b.e2.e2, b.e2.e3, b.e2.e4, b.e3.e0, b.e3.e1, b.e3.e2, b.e3.e3, b.e3.e4, b.e4.e0, b.e4.e1, b.e4.e2, b.e4.e3, b.e4.e4;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Float, 6, 5> ToEigen(const ::armarx::Matrix_6_5_f& b)
    {
        ::Eigen::Matrix< ::Ice::Float, 6, 5> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2, b.e0.e3, b.e0.e4, b.e1.e0, b.e1.e1, b.e1.e2, b.e1.e3, b.e1.e4, b.e2.e0, b.e2.e1, b.e2.e2, b.e2.e3, b.e2.e4, b.e3.e0, b.e3.e1, b.e3.e2, b.e3.e3, b.e3.e4, b.e4.e0, b.e4.e1, b.e4.e2, b.e4.e3, b.e4.e4, b.e5.e0, b.e5.e1, b.e5.e2, b.e5.e3, b.e5.e4;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Float, 1, 6> ToEigen(const ::armarx::Matrix_1_6_f& b)
    {
        ::Eigen::Matrix< ::Ice::Float, 1, 6> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2, b.e0.e3, b.e0.e4, b.e0.e5;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Float, 2, 6> ToEigen(const ::armarx::Matrix_2_6_f& b)
    {
        ::Eigen::Matrix< ::Ice::Float, 2, 6> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2, b.e0.e3, b.e0.e4, b.e0.e5, b.e1.e0, b.e1.e1, b.e1.e2, b.e1.e3, b.e1.e4, b.e1.e5;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Float, 3, 6> ToEigen(const ::armarx::Matrix_3_6_f& b)
    {
        ::Eigen::Matrix< ::Ice::Float, 3, 6> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2, b.e0.e3, b.e0.e4, b.e0.e5, b.e1.e0, b.e1.e1, b.e1.e2, b.e1.e3, b.e1.e4, b.e1.e5, b.e2.e0, b.e2.e1, b.e2.e2, b.e2.e3, b.e2.e4, b.e2.e5;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Float, 4, 6> ToEigen(const ::armarx::Matrix_4_6_f& b)
    {
        ::Eigen::Matrix< ::Ice::Float, 4, 6> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2, b.e0.e3, b.e0.e4, b.e0.e5, b.e1.e0, b.e1.e1, b.e1.e2, b.e1.e3, b.e1.e4, b.e1.e5, b.e2.e0, b.e2.e1, b.e2.e2, b.e2.e3, b.e2.e4, b.e2.e5, b.e3.e0, b.e3.e1, b.e3.e2, b.e3.e3, b.e3.e4, b.e3.e5;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Float, 5, 6> ToEigen(const ::armarx::Matrix_5_6_f& b)
    {
        ::Eigen::Matrix< ::Ice::Float, 5, 6> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2, b.e0.e3, b.e0.e4, b.e0.e5, b.e1.e0, b.e1.e1, b.e1.e2, b.e1.e3, b.e1.e4, b.e1.e5, b.e2.e0, b.e2.e1, b.e2.e2, b.e2.e3, b.e2.e4, b.e2.e5, b.e3.e0, b.e3.e1, b.e3.e2, b.e3.e3, b.e3.e4, b.e3.e5, b.e4.e0, b.e4.e1, b.e4.e2, b.e4.e3, b.e4.e4, b.e4.e5;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Float, 6, 6> ToEigen(const ::armarx::Matrix_6_6_f& b)
    {
        ::Eigen::Matrix< ::Ice::Float, 6, 6> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2, b.e0.e3, b.e0.e4, b.e0.e5, b.e1.e0, b.e1.e1, b.e1.e2, b.e1.e3, b.e1.e4, b.e1.e5, b.e2.e0, b.e2.e1, b.e2.e2, b.e2.e3, b.e2.e4, b.e2.e5, b.e3.e0, b.e3.e1, b.e3.e2, b.e3.e3, b.e3.e4, b.e3.e5, b.e4.e0, b.e4.e1, b.e4.e2, b.e4.e3, b.e4.e4, b.e4.e5, b.e5.e0, b.e5.e1, b.e5.e2, b.e5.e3, b.e5.e4, b.e5.e5;
        return m;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Vector1f& r)
    {
        detail::bvt::AllOut(l, '\t', r.e0);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Vector2f& r)
    {
        detail::bvt::AllOut(l, '\t', r.e0, r.e1);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Vector3f& r)
    {
        detail::bvt::AllOut(l, '\t', r.e0, r.e1, r.e2);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Vector4f& r)
    {
        detail::bvt::AllOut(l, '\t', r.e0, r.e1, r.e2, r.e3);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Vector5f& r)
    {
        detail::bvt::AllOut(l, '\t', r.e0, r.e1, r.e2, r.e3, r.e4);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Vector6f& r)
    {
        detail::bvt::AllOut(l, '\t', r.e0, r.e1, r.e2, r.e3, r.e4, r.e5);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_1_1_f& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_2_1_f& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_3_1_f& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_4_1_f& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2, r.e3);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_5_1_f& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2, r.e3, r.e4);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_6_1_f& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2, r.e3, r.e4, r.e5);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_1_2_f& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_2_2_f& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_3_2_f& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_4_2_f& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2, r.e3);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_5_2_f& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2, r.e3, r.e4);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_6_2_f& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2, r.e3, r.e4, r.e5);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_1_3_f& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_2_3_f& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_3_3_f& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_4_3_f& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2, r.e3);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_5_3_f& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2, r.e3, r.e4);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_6_3_f& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2, r.e3, r.e4, r.e5);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_1_4_f& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_2_4_f& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_3_4_f& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_4_4_f& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2, r.e3);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_5_4_f& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2, r.e3, r.e4);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_6_4_f& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2, r.e3, r.e4, r.e5);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_1_5_f& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_2_5_f& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_3_5_f& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_4_5_f& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2, r.e3);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_5_5_f& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2, r.e3, r.e4);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_6_5_f& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2, r.e3, r.e4, r.e5);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_1_6_f& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_2_6_f& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_3_6_f& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_4_6_f& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2, r.e3);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_5_6_f& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2, r.e3, r.e4);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_6_6_f& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2, r.e3, r.e4, r.e5);
        return l;
    }
    template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Double, 1, 1>, Derived, ::armarx::Vector1d> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {e(0)};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Double, 2, 1>, Derived, ::armarx::Vector2d> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {e(0), e(1)};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Double, 3, 1>, Derived, ::armarx::Vector3d> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {e(0), e(1), e(2)};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Double, 4, 1>, Derived, ::armarx::Vector4d> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {e(0), e(1), e(2), e(3)};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Double, 5, 1>, Derived, ::armarx::Vector5d> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {e(0), e(1), e(2), e(3), e(4)};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Double, 6, 1>, Derived, ::armarx::Vector6d> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {e(0), e(1), e(2), e(3), e(4), e(5)};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Double, 1, 2>, Derived, ::armarx::Matrix_1_2_d> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Double, 2, 2>, Derived, ::armarx::Matrix_2_2_d> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1)}, {e(1, 0), e(1, 1)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Double, 3, 2>, Derived, ::armarx::Matrix_3_2_d> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1)}, {e(1, 0), e(1, 1)}, {e(2, 0), e(2, 1)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Double, 4, 2>, Derived, ::armarx::Matrix_4_2_d> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1)}, {e(1, 0), e(1, 1)}, {e(2, 0), e(2, 1)}, {e(3, 0), e(3, 1)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Double, 5, 2>, Derived, ::armarx::Matrix_5_2_d> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1)}, {e(1, 0), e(1, 1)}, {e(2, 0), e(2, 1)}, {e(3, 0), e(3, 1)}, {e(4, 0), e(4, 1)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Double, 6, 2>, Derived, ::armarx::Matrix_6_2_d> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1)}, {e(1, 0), e(1, 1)}, {e(2, 0), e(2, 1)}, {e(3, 0), e(3, 1)}, {e(4, 0), e(4, 1)}, {e(5, 0), e(5, 1)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Double, 1, 3>, Derived, ::armarx::Matrix_1_3_d> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Double, 2, 3>, Derived, ::armarx::Matrix_2_3_d> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2)}, {e(1, 0), e(1, 1), e(1, 2)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Double, 3, 3>, Derived, ::armarx::Matrix_3_3_d> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2)}, {e(1, 0), e(1, 1), e(1, 2)}, {e(2, 0), e(2, 1), e(2, 2)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Double, 4, 3>, Derived, ::armarx::Matrix_4_3_d> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2)}, {e(1, 0), e(1, 1), e(1, 2)}, {e(2, 0), e(2, 1), e(2, 2)}, {e(3, 0), e(3, 1), e(3, 2)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Double, 5, 3>, Derived, ::armarx::Matrix_5_3_d> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2)}, {e(1, 0), e(1, 1), e(1, 2)}, {e(2, 0), e(2, 1), e(2, 2)}, {e(3, 0), e(3, 1), e(3, 2)}, {e(4, 0), e(4, 1), e(4, 2)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Double, 6, 3>, Derived, ::armarx::Matrix_6_3_d> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2)}, {e(1, 0), e(1, 1), e(1, 2)}, {e(2, 0), e(2, 1), e(2, 2)}, {e(3, 0), e(3, 1), e(3, 2)}, {e(4, 0), e(4, 1), e(4, 2)}, {e(5, 0), e(5, 1), e(5, 2)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Double, 1, 4>, Derived, ::armarx::Matrix_1_4_d> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2), e(0, 3)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Double, 2, 4>, Derived, ::armarx::Matrix_2_4_d> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2), e(0, 3)}, {e(1, 0), e(1, 1), e(1, 2), e(1, 3)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Double, 3, 4>, Derived, ::armarx::Matrix_3_4_d> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2), e(0, 3)}, {e(1, 0), e(1, 1), e(1, 2), e(1, 3)}, {e(2, 0), e(2, 1), e(2, 2), e(2, 3)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Double, 4, 4>, Derived, ::armarx::Matrix_4_4_d> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2), e(0, 3)}, {e(1, 0), e(1, 1), e(1, 2), e(1, 3)}, {e(2, 0), e(2, 1), e(2, 2), e(2, 3)}, {e(3, 0), e(3, 1), e(3, 2), e(3, 3)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Double, 5, 4>, Derived, ::armarx::Matrix_5_4_d> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2), e(0, 3)}, {e(1, 0), e(1, 1), e(1, 2), e(1, 3)}, {e(2, 0), e(2, 1), e(2, 2), e(2, 3)}, {e(3, 0), e(3, 1), e(3, 2), e(3, 3)}, {e(4, 0), e(4, 1), e(4, 2), e(4, 3)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Double, 6, 4>, Derived, ::armarx::Matrix_6_4_d> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2), e(0, 3)}, {e(1, 0), e(1, 1), e(1, 2), e(1, 3)}, {e(2, 0), e(2, 1), e(2, 2), e(2, 3)}, {e(3, 0), e(3, 1), e(3, 2), e(3, 3)}, {e(4, 0), e(4, 1), e(4, 2), e(4, 3)}, {e(5, 0), e(5, 1), e(5, 2), e(5, 3)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Double, 1, 5>, Derived, ::armarx::Matrix_1_5_d> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2), e(0, 3), e(0, 4)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Double, 2, 5>, Derived, ::armarx::Matrix_2_5_d> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2), e(0, 3), e(0, 4)}, {e(1, 0), e(1, 1), e(1, 2), e(1, 3), e(1, 4)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Double, 3, 5>, Derived, ::armarx::Matrix_3_5_d> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2), e(0, 3), e(0, 4)}, {e(1, 0), e(1, 1), e(1, 2), e(1, 3), e(1, 4)}, {e(2, 0), e(2, 1), e(2, 2), e(2, 3), e(2, 4)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Double, 4, 5>, Derived, ::armarx::Matrix_4_5_d> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2), e(0, 3), e(0, 4)}, {e(1, 0), e(1, 1), e(1, 2), e(1, 3), e(1, 4)}, {e(2, 0), e(2, 1), e(2, 2), e(2, 3), e(2, 4)}, {e(3, 0), e(3, 1), e(3, 2), e(3, 3), e(3, 4)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Double, 5, 5>, Derived, ::armarx::Matrix_5_5_d> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2), e(0, 3), e(0, 4)}, {e(1, 0), e(1, 1), e(1, 2), e(1, 3), e(1, 4)}, {e(2, 0), e(2, 1), e(2, 2), e(2, 3), e(2, 4)}, {e(3, 0), e(3, 1), e(3, 2), e(3, 3), e(3, 4)}, {e(4, 0), e(4, 1), e(4, 2), e(4, 3), e(4, 4)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Double, 6, 5>, Derived, ::armarx::Matrix_6_5_d> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2), e(0, 3), e(0, 4)}, {e(1, 0), e(1, 1), e(1, 2), e(1, 3), e(1, 4)}, {e(2, 0), e(2, 1), e(2, 2), e(2, 3), e(2, 4)}, {e(3, 0), e(3, 1), e(3, 2), e(3, 3), e(3, 4)}, {e(4, 0), e(4, 1), e(4, 2), e(4, 3), e(4, 4)}, {e(5, 0), e(5, 1), e(5, 2), e(5, 3), e(5, 4)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Double, 1, 6>, Derived, ::armarx::Matrix_1_6_d> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2), e(0, 3), e(0, 4), e(0, 5)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Double, 2, 6>, Derived, ::armarx::Matrix_2_6_d> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2), e(0, 3), e(0, 4), e(0, 5)}, {e(1, 0), e(1, 1), e(1, 2), e(1, 3), e(1, 4), e(1, 5)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Double, 3, 6>, Derived, ::armarx::Matrix_3_6_d> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2), e(0, 3), e(0, 4), e(0, 5)}, {e(1, 0), e(1, 1), e(1, 2), e(1, 3), e(1, 4), e(1, 5)}, {e(2, 0), e(2, 1), e(2, 2), e(2, 3), e(2, 4), e(2, 5)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Double, 4, 6>, Derived, ::armarx::Matrix_4_6_d> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2), e(0, 3), e(0, 4), e(0, 5)}, {e(1, 0), e(1, 1), e(1, 2), e(1, 3), e(1, 4), e(1, 5)}, {e(2, 0), e(2, 1), e(2, 2), e(2, 3), e(2, 4), e(2, 5)}, {e(3, 0), e(3, 1), e(3, 2), e(3, 3), e(3, 4), e(3, 5)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Double, 5, 6>, Derived, ::armarx::Matrix_5_6_d> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2), e(0, 3), e(0, 4), e(0, 5)}, {e(1, 0), e(1, 1), e(1, 2), e(1, 3), e(1, 4), e(1, 5)}, {e(2, 0), e(2, 1), e(2, 2), e(2, 3), e(2, 4), e(2, 5)}, {e(3, 0), e(3, 1), e(3, 2), e(3, 3), e(3, 4), e(3, 5)}, {e(4, 0), e(4, 1), e(4, 2), e(4, 3), e(4, 4), e(4, 5)}};
    } template<class Derived> inline detail::bvt::enable_if_assignable_t< ::Eigen::Matrix< ::Ice::Double, 6, 6>, Derived, ::armarx::Matrix_6_6_d> ToBasicVectorType(const Eigen::MatrixBase<Derived>& e)
    {
        return {{e(0, 0), e(0, 1), e(0, 2), e(0, 3), e(0, 4), e(0, 5)}, {e(1, 0), e(1, 1), e(1, 2), e(1, 3), e(1, 4), e(1, 5)}, {e(2, 0), e(2, 1), e(2, 2), e(2, 3), e(2, 4), e(2, 5)}, {e(3, 0), e(3, 1), e(3, 2), e(3, 3), e(3, 4), e(3, 5)}, {e(4, 0), e(4, 1), e(4, 2), e(4, 3), e(4, 4), e(4, 5)}, {e(5, 0), e(5, 1), e(5, 2), e(5, 3), e(5, 4), e(5, 5)}};
    } inline ::Eigen::Matrix< ::Ice::Double, 1, 1> ToEigen(const ::armarx::Vector1d& b)
    {
        ::Eigen::Matrix< ::Ice::Double, 1, 1> m;
        m << b.e0;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Double, 2, 1> ToEigen(const ::armarx::Vector2d& b)
    {
        ::Eigen::Matrix< ::Ice::Double, 2, 1> m;
        m << b.e0, b.e1;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Double, 3, 1> ToEigen(const ::armarx::Vector3d& b)
    {
        ::Eigen::Matrix< ::Ice::Double, 3, 1> m;
        m << b.e0, b.e1, b.e2;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Double, 4, 1> ToEigen(const ::armarx::Vector4d& b)
    {
        ::Eigen::Matrix< ::Ice::Double, 4, 1> m;
        m << b.e0, b.e1, b.e2, b.e3;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Double, 5, 1> ToEigen(const ::armarx::Vector5d& b)
    {
        ::Eigen::Matrix< ::Ice::Double, 5, 1> m;
        m << b.e0, b.e1, b.e2, b.e3, b.e4;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Double, 6, 1> ToEigen(const ::armarx::Vector6d& b)
    {
        ::Eigen::Matrix< ::Ice::Double, 6, 1> m;
        m << b.e0, b.e1, b.e2, b.e3, b.e4, b.e5;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Double, 1, 1> ToEigen(const ::armarx::Matrix_1_1_d& b)
    {
        ::Eigen::Matrix< ::Ice::Double, 1, 1> m;
        m << b.e0.e0;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Double, 2, 1> ToEigen(const ::armarx::Matrix_2_1_d& b)
    {
        ::Eigen::Matrix< ::Ice::Double, 2, 1> m;
        m << b.e0.e0, b.e1.e0;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Double, 3, 1> ToEigen(const ::armarx::Matrix_3_1_d& b)
    {
        ::Eigen::Matrix< ::Ice::Double, 3, 1> m;
        m << b.e0.e0, b.e1.e0, b.e2.e0;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Double, 4, 1> ToEigen(const ::armarx::Matrix_4_1_d& b)
    {
        ::Eigen::Matrix< ::Ice::Double, 4, 1> m;
        m << b.e0.e0, b.e1.e0, b.e2.e0, b.e3.e0;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Double, 5, 1> ToEigen(const ::armarx::Matrix_5_1_d& b)
    {
        ::Eigen::Matrix< ::Ice::Double, 5, 1> m;
        m << b.e0.e0, b.e1.e0, b.e2.e0, b.e3.e0, b.e4.e0;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Double, 6, 1> ToEigen(const ::armarx::Matrix_6_1_d& b)
    {
        ::Eigen::Matrix< ::Ice::Double, 6, 1> m;
        m << b.e0.e0, b.e1.e0, b.e2.e0, b.e3.e0, b.e4.e0, b.e5.e0;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Double, 1, 2> ToEigen(const ::armarx::Matrix_1_2_d& b)
    {
        ::Eigen::Matrix< ::Ice::Double, 1, 2> m;
        m << b.e0.e0, b.e0.e1;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Double, 2, 2> ToEigen(const ::armarx::Matrix_2_2_d& b)
    {
        ::Eigen::Matrix< ::Ice::Double, 2, 2> m;
        m << b.e0.e0, b.e0.e1, b.e1.e0, b.e1.e1;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Double, 3, 2> ToEigen(const ::armarx::Matrix_3_2_d& b)
    {
        ::Eigen::Matrix< ::Ice::Double, 3, 2> m;
        m << b.e0.e0, b.e0.e1, b.e1.e0, b.e1.e1, b.e2.e0, b.e2.e1;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Double, 4, 2> ToEigen(const ::armarx::Matrix_4_2_d& b)
    {
        ::Eigen::Matrix< ::Ice::Double, 4, 2> m;
        m << b.e0.e0, b.e0.e1, b.e1.e0, b.e1.e1, b.e2.e0, b.e2.e1, b.e3.e0, b.e3.e1;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Double, 5, 2> ToEigen(const ::armarx::Matrix_5_2_d& b)
    {
        ::Eigen::Matrix< ::Ice::Double, 5, 2> m;
        m << b.e0.e0, b.e0.e1, b.e1.e0, b.e1.e1, b.e2.e0, b.e2.e1, b.e3.e0, b.e3.e1, b.e4.e0, b.e4.e1;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Double, 6, 2> ToEigen(const ::armarx::Matrix_6_2_d& b)
    {
        ::Eigen::Matrix< ::Ice::Double, 6, 2> m;
        m << b.e0.e0, b.e0.e1, b.e1.e0, b.e1.e1, b.e2.e0, b.e2.e1, b.e3.e0, b.e3.e1, b.e4.e0, b.e4.e1, b.e5.e0, b.e5.e1;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Double, 1, 3> ToEigen(const ::armarx::Matrix_1_3_d& b)
    {
        ::Eigen::Matrix< ::Ice::Double, 1, 3> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Double, 2, 3> ToEigen(const ::armarx::Matrix_2_3_d& b)
    {
        ::Eigen::Matrix< ::Ice::Double, 2, 3> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2, b.e1.e0, b.e1.e1, b.e1.e2;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Double, 3, 3> ToEigen(const ::armarx::Matrix_3_3_d& b)
    {
        ::Eigen::Matrix< ::Ice::Double, 3, 3> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2, b.e1.e0, b.e1.e1, b.e1.e2, b.e2.e0, b.e2.e1, b.e2.e2;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Double, 4, 3> ToEigen(const ::armarx::Matrix_4_3_d& b)
    {
        ::Eigen::Matrix< ::Ice::Double, 4, 3> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2, b.e1.e0, b.e1.e1, b.e1.e2, b.e2.e0, b.e2.e1, b.e2.e2, b.e3.e0, b.e3.e1, b.e3.e2;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Double, 5, 3> ToEigen(const ::armarx::Matrix_5_3_d& b)
    {
        ::Eigen::Matrix< ::Ice::Double, 5, 3> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2, b.e1.e0, b.e1.e1, b.e1.e2, b.e2.e0, b.e2.e1, b.e2.e2, b.e3.e0, b.e3.e1, b.e3.e2, b.e4.e0, b.e4.e1, b.e4.e2;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Double, 6, 3> ToEigen(const ::armarx::Matrix_6_3_d& b)
    {
        ::Eigen::Matrix< ::Ice::Double, 6, 3> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2, b.e1.e0, b.e1.e1, b.e1.e2, b.e2.e0, b.e2.e1, b.e2.e2, b.e3.e0, b.e3.e1, b.e3.e2, b.e4.e0, b.e4.e1, b.e4.e2, b.e5.e0, b.e5.e1, b.e5.e2;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Double, 1, 4> ToEigen(const ::armarx::Matrix_1_4_d& b)
    {
        ::Eigen::Matrix< ::Ice::Double, 1, 4> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2, b.e0.e3;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Double, 2, 4> ToEigen(const ::armarx::Matrix_2_4_d& b)
    {
        ::Eigen::Matrix< ::Ice::Double, 2, 4> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2, b.e0.e3, b.e1.e0, b.e1.e1, b.e1.e2, b.e1.e3;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Double, 3, 4> ToEigen(const ::armarx::Matrix_3_4_d& b)
    {
        ::Eigen::Matrix< ::Ice::Double, 3, 4> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2, b.e0.e3, b.e1.e0, b.e1.e1, b.e1.e2, b.e1.e3, b.e2.e0, b.e2.e1, b.e2.e2, b.e2.e3;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Double, 4, 4> ToEigen(const ::armarx::Matrix_4_4_d& b)
    {
        ::Eigen::Matrix< ::Ice::Double, 4, 4> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2, b.e0.e3, b.e1.e0, b.e1.e1, b.e1.e2, b.e1.e3, b.e2.e0, b.e2.e1, b.e2.e2, b.e2.e3, b.e3.e0, b.e3.e1, b.e3.e2, b.e3.e3;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Double, 5, 4> ToEigen(const ::armarx::Matrix_5_4_d& b)
    {
        ::Eigen::Matrix< ::Ice::Double, 5, 4> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2, b.e0.e3, b.e1.e0, b.e1.e1, b.e1.e2, b.e1.e3, b.e2.e0, b.e2.e1, b.e2.e2, b.e2.e3, b.e3.e0, b.e3.e1, b.e3.e2, b.e3.e3, b.e4.e0, b.e4.e1, b.e4.e2, b.e4.e3;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Double, 6, 4> ToEigen(const ::armarx::Matrix_6_4_d& b)
    {
        ::Eigen::Matrix< ::Ice::Double, 6, 4> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2, b.e0.e3, b.e1.e0, b.e1.e1, b.e1.e2, b.e1.e3, b.e2.e0, b.e2.e1, b.e2.e2, b.e2.e3, b.e3.e0, b.e3.e1, b.e3.e2, b.e3.e3, b.e4.e0, b.e4.e1, b.e4.e2, b.e4.e3, b.e5.e0, b.e5.e1, b.e5.e2, b.e5.e3;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Double, 1, 5> ToEigen(const ::armarx::Matrix_1_5_d& b)
    {
        ::Eigen::Matrix< ::Ice::Double, 1, 5> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2, b.e0.e3, b.e0.e4;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Double, 2, 5> ToEigen(const ::armarx::Matrix_2_5_d& b)
    {
        ::Eigen::Matrix< ::Ice::Double, 2, 5> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2, b.e0.e3, b.e0.e4, b.e1.e0, b.e1.e1, b.e1.e2, b.e1.e3, b.e1.e4;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Double, 3, 5> ToEigen(const ::armarx::Matrix_3_5_d& b)
    {
        ::Eigen::Matrix< ::Ice::Double, 3, 5> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2, b.e0.e3, b.e0.e4, b.e1.e0, b.e1.e1, b.e1.e2, b.e1.e3, b.e1.e4, b.e2.e0, b.e2.e1, b.e2.e2, b.e2.e3, b.e2.e4;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Double, 4, 5> ToEigen(const ::armarx::Matrix_4_5_d& b)
    {
        ::Eigen::Matrix< ::Ice::Double, 4, 5> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2, b.e0.e3, b.e0.e4, b.e1.e0, b.e1.e1, b.e1.e2, b.e1.e3, b.e1.e4, b.e2.e0, b.e2.e1, b.e2.e2, b.e2.e3, b.e2.e4, b.e3.e0, b.e3.e1, b.e3.e2, b.e3.e3, b.e3.e4;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Double, 5, 5> ToEigen(const ::armarx::Matrix_5_5_d& b)
    {
        ::Eigen::Matrix< ::Ice::Double, 5, 5> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2, b.e0.e3, b.e0.e4, b.e1.e0, b.e1.e1, b.e1.e2, b.e1.e3, b.e1.e4, b.e2.e0, b.e2.e1, b.e2.e2, b.e2.e3, b.e2.e4, b.e3.e0, b.e3.e1, b.e3.e2, b.e3.e3, b.e3.e4, b.e4.e0, b.e4.e1, b.e4.e2, b.e4.e3, b.e4.e4;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Double, 6, 5> ToEigen(const ::armarx::Matrix_6_5_d& b)
    {
        ::Eigen::Matrix< ::Ice::Double, 6, 5> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2, b.e0.e3, b.e0.e4, b.e1.e0, b.e1.e1, b.e1.e2, b.e1.e3, b.e1.e4, b.e2.e0, b.e2.e1, b.e2.e2, b.e2.e3, b.e2.e4, b.e3.e0, b.e3.e1, b.e3.e2, b.e3.e3, b.e3.e4, b.e4.e0, b.e4.e1, b.e4.e2, b.e4.e3, b.e4.e4, b.e5.e0, b.e5.e1, b.e5.e2, b.e5.e3, b.e5.e4;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Double, 1, 6> ToEigen(const ::armarx::Matrix_1_6_d& b)
    {
        ::Eigen::Matrix< ::Ice::Double, 1, 6> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2, b.e0.e3, b.e0.e4, b.e0.e5;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Double, 2, 6> ToEigen(const ::armarx::Matrix_2_6_d& b)
    {
        ::Eigen::Matrix< ::Ice::Double, 2, 6> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2, b.e0.e3, b.e0.e4, b.e0.e5, b.e1.e0, b.e1.e1, b.e1.e2, b.e1.e3, b.e1.e4, b.e1.e5;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Double, 3, 6> ToEigen(const ::armarx::Matrix_3_6_d& b)
    {
        ::Eigen::Matrix< ::Ice::Double, 3, 6> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2, b.e0.e3, b.e0.e4, b.e0.e5, b.e1.e0, b.e1.e1, b.e1.e2, b.e1.e3, b.e1.e4, b.e1.e5, b.e2.e0, b.e2.e1, b.e2.e2, b.e2.e3, b.e2.e4, b.e2.e5;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Double, 4, 6> ToEigen(const ::armarx::Matrix_4_6_d& b)
    {
        ::Eigen::Matrix< ::Ice::Double, 4, 6> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2, b.e0.e3, b.e0.e4, b.e0.e5, b.e1.e0, b.e1.e1, b.e1.e2, b.e1.e3, b.e1.e4, b.e1.e5, b.e2.e0, b.e2.e1, b.e2.e2, b.e2.e3, b.e2.e4, b.e2.e5, b.e3.e0, b.e3.e1, b.e3.e2, b.e3.e3, b.e3.e4, b.e3.e5;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Double, 5, 6> ToEigen(const ::armarx::Matrix_5_6_d& b)
    {
        ::Eigen::Matrix< ::Ice::Double, 5, 6> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2, b.e0.e3, b.e0.e4, b.e0.e5, b.e1.e0, b.e1.e1, b.e1.e2, b.e1.e3, b.e1.e4, b.e1.e5, b.e2.e0, b.e2.e1, b.e2.e2, b.e2.e3, b.e2.e4, b.e2.e5, b.e3.e0, b.e3.e1, b.e3.e2, b.e3.e3, b.e3.e4, b.e3.e5, b.e4.e0, b.e4.e1, b.e4.e2, b.e4.e3, b.e4.e4, b.e4.e5;
        return m;
    } inline ::Eigen::Matrix< ::Ice::Double, 6, 6> ToEigen(const ::armarx::Matrix_6_6_d& b)
    {
        ::Eigen::Matrix< ::Ice::Double, 6, 6> m;
        m << b.e0.e0, b.e0.e1, b.e0.e2, b.e0.e3, b.e0.e4, b.e0.e5, b.e1.e0, b.e1.e1, b.e1.e2, b.e1.e3, b.e1.e4, b.e1.e5, b.e2.e0, b.e2.e1, b.e2.e2, b.e2.e3, b.e2.e4, b.e2.e5, b.e3.e0, b.e3.e1, b.e3.e2, b.e3.e3, b.e3.e4, b.e3.e5, b.e4.e0, b.e4.e1, b.e4.e2, b.e4.e3, b.e4.e4, b.e4.e5, b.e5.e0, b.e5.e1, b.e5.e2, b.e5.e3, b.e5.e4, b.e5.e5;
        return m;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Vector1d& r)
    {
        detail::bvt::AllOut(l, '\t', r.e0);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Vector2d& r)
    {
        detail::bvt::AllOut(l, '\t', r.e0, r.e1);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Vector3d& r)
    {
        detail::bvt::AllOut(l, '\t', r.e0, r.e1, r.e2);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Vector4d& r)
    {
        detail::bvt::AllOut(l, '\t', r.e0, r.e1, r.e2, r.e3);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Vector5d& r)
    {
        detail::bvt::AllOut(l, '\t', r.e0, r.e1, r.e2, r.e3, r.e4);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Vector6d& r)
    {
        detail::bvt::AllOut(l, '\t', r.e0, r.e1, r.e2, r.e3, r.e4, r.e5);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_1_1_d& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_2_1_d& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_3_1_d& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_4_1_d& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2, r.e3);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_5_1_d& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2, r.e3, r.e4);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_6_1_d& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2, r.e3, r.e4, r.e5);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_1_2_d& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_2_2_d& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_3_2_d& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_4_2_d& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2, r.e3);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_5_2_d& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2, r.e3, r.e4);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_6_2_d& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2, r.e3, r.e4, r.e5);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_1_3_d& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_2_3_d& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_3_3_d& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_4_3_d& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2, r.e3);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_5_3_d& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2, r.e3, r.e4);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_6_3_d& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2, r.e3, r.e4, r.e5);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_1_4_d& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_2_4_d& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_3_4_d& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_4_4_d& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2, r.e3);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_5_4_d& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2, r.e3, r.e4);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_6_4_d& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2, r.e3, r.e4, r.e5);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_1_5_d& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_2_5_d& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_3_5_d& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_4_5_d& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2, r.e3);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_5_5_d& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2, r.e3, r.e4);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_6_5_d& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2, r.e3, r.e4, r.e5);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_1_6_d& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_2_6_d& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_3_6_d& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_4_6_d& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2, r.e3);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_5_6_d& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2, r.e3, r.e4);
        return l;
    } inline std::ostream& operator<<(std::ostream& l, const ::armarx::Matrix_6_6_d& r)
    {
        detail::bvt::AllOut(l, '\n', r.e0, r.e1, r.e2, r.e3, r.e4, r.e5);
        return l;
    }


    inline bool operator <(const ::armarx::Vector1f& l, const ::armarx::Vector1f& r)
    {
        return std::tie(l.e0) < std::tie(r.e0);
    } inline bool operator >(const ::armarx::Vector1f& l, const ::armarx::Vector1f& r)
    {
        return std::tie(l.e0) > std::tie(r.e0);
    } inline bool operator ==(const ::armarx::Vector1f& l, const ::armarx::Vector1f& r)
    {
        return std::tie(l.e0) == std::tie(r.e0);
    } inline bool operator !=(const ::armarx::Vector1f& l, const ::armarx::Vector1f& r)
    {
        return std::tie(l.e0) != std::tie(r.e0);
    } inline bool operator >=(const ::armarx::Vector1f& l, const ::armarx::Vector1f& r)
    {
        return std::tie(l.e0) >= std::tie(r.e0);
    } inline bool operator <=(const ::armarx::Vector1f& l, const ::armarx::Vector1f& r)
    {
        return std::tie(l.e0) <= std::tie(r.e0);
    } inline bool operator <(const ::armarx::Vector2f& l, const ::armarx::Vector2f& r)
    {
        return std::tie(l.e0, l.e1) < std::tie(r.e0, r.e1);
    } inline bool operator >(const ::armarx::Vector2f& l, const ::armarx::Vector2f& r)
    {
        return std::tie(l.e0, l.e1) > std::tie(r.e0, r.e1);
    } inline bool operator ==(const ::armarx::Vector2f& l, const ::armarx::Vector2f& r)
    {
        return std::tie(l.e0, l.e1) == std::tie(r.e0, r.e1);
    } inline bool operator !=(const ::armarx::Vector2f& l, const ::armarx::Vector2f& r)
    {
        return std::tie(l.e0, l.e1) != std::tie(r.e0, r.e1);
    } inline bool operator >=(const ::armarx::Vector2f& l, const ::armarx::Vector2f& r)
    {
        return std::tie(l.e0, l.e1) >= std::tie(r.e0, r.e1);
    } inline bool operator <=(const ::armarx::Vector2f& l, const ::armarx::Vector2f& r)
    {
        return std::tie(l.e0, l.e1) <= std::tie(r.e0, r.e1);
    } inline bool operator <(const ::armarx::Vector3f& l, const ::armarx::Vector3f& r)
    {
        return std::tie(l.e0, l.e1, l.e2) < std::tie(r.e0, r.e1, r.e2);
    } inline bool operator >(const ::armarx::Vector3f& l, const ::armarx::Vector3f& r)
    {
        return std::tie(l.e0, l.e1, l.e2) > std::tie(r.e0, r.e1, r.e2);
    } inline bool operator ==(const ::armarx::Vector3f& l, const ::armarx::Vector3f& r)
    {
        return std::tie(l.e0, l.e1, l.e2) == std::tie(r.e0, r.e1, r.e2);
    } inline bool operator !=(const ::armarx::Vector3f& l, const ::armarx::Vector3f& r)
    {
        return std::tie(l.e0, l.e1, l.e2) != std::tie(r.e0, r.e1, r.e2);
    } inline bool operator >=(const ::armarx::Vector3f& l, const ::armarx::Vector3f& r)
    {
        return std::tie(l.e0, l.e1, l.e2) >= std::tie(r.e0, r.e1, r.e2);
    } inline bool operator <=(const ::armarx::Vector3f& l, const ::armarx::Vector3f& r)
    {
        return std::tie(l.e0, l.e1, l.e2) <= std::tie(r.e0, r.e1, r.e2);
    } inline bool operator <(const ::armarx::Vector4f& l, const ::armarx::Vector4f& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3) < std::tie(r.e0, r.e1, r.e2, r.e3);
    } inline bool operator >(const ::armarx::Vector4f& l, const ::armarx::Vector4f& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3) > std::tie(r.e0, r.e1, r.e2, r.e3);
    } inline bool operator ==(const ::armarx::Vector4f& l, const ::armarx::Vector4f& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3) == std::tie(r.e0, r.e1, r.e2, r.e3);
    } inline bool operator !=(const ::armarx::Vector4f& l, const ::armarx::Vector4f& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3) != std::tie(r.e0, r.e1, r.e2, r.e3);
    } inline bool operator >=(const ::armarx::Vector4f& l, const ::armarx::Vector4f& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3) >= std::tie(r.e0, r.e1, r.e2, r.e3);
    } inline bool operator <=(const ::armarx::Vector4f& l, const ::armarx::Vector4f& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3) <= std::tie(r.e0, r.e1, r.e2, r.e3);
    } inline bool operator <(const ::armarx::Vector5f& l, const ::armarx::Vector5f& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4) < std::tie(r.e0, r.e1, r.e2, r.e3, r.e4);
    } inline bool operator >(const ::armarx::Vector5f& l, const ::armarx::Vector5f& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4) > std::tie(r.e0, r.e1, r.e2, r.e3, r.e4);
    } inline bool operator ==(const ::armarx::Vector5f& l, const ::armarx::Vector5f& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4) == std::tie(r.e0, r.e1, r.e2, r.e3, r.e4);
    } inline bool operator !=(const ::armarx::Vector5f& l, const ::armarx::Vector5f& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4) != std::tie(r.e0, r.e1, r.e2, r.e3, r.e4);
    } inline bool operator >=(const ::armarx::Vector5f& l, const ::armarx::Vector5f& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4) >= std::tie(r.e0, r.e1, r.e2, r.e3, r.e4);
    } inline bool operator <=(const ::armarx::Vector5f& l, const ::armarx::Vector5f& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4) <= std::tie(r.e0, r.e1, r.e2, r.e3, r.e4);
    } inline bool operator <(const ::armarx::Vector6f& l, const ::armarx::Vector6f& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4, l.e5) < std::tie(r.e0, r.e1, r.e2, r.e3, r.e4, r.e5);
    } inline bool operator >(const ::armarx::Vector6f& l, const ::armarx::Vector6f& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4, l.e5) > std::tie(r.e0, r.e1, r.e2, r.e3, r.e4, r.e5);
    } inline bool operator ==(const ::armarx::Vector6f& l, const ::armarx::Vector6f& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4, l.e5) == std::tie(r.e0, r.e1, r.e2, r.e3, r.e4, r.e5);
    } inline bool operator !=(const ::armarx::Vector6f& l, const ::armarx::Vector6f& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4, l.e5) != std::tie(r.e0, r.e1, r.e2, r.e3, r.e4, r.e5);
    } inline bool operator >=(const ::armarx::Vector6f& l, const ::armarx::Vector6f& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4, l.e5) >= std::tie(r.e0, r.e1, r.e2, r.e3, r.e4, r.e5);
    } inline bool operator <=(const ::armarx::Vector6f& l, const ::armarx::Vector6f& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4, l.e5) <= std::tie(r.e0, r.e1, r.e2, r.e3, r.e4, r.e5);
    }
    inline bool operator <(const ::armarx::Matrix_1_1_f& l, const ::armarx::Matrix_1_1_f& r)
    {
        return std::tie(l.e0) < std::tie(r.e0);
    } inline bool operator >(const ::armarx::Matrix_1_1_f& l, const ::armarx::Matrix_1_1_f& r)
    {
        return std::tie(l.e0) > std::tie(r.e0);
    } inline bool operator ==(const ::armarx::Matrix_1_1_f& l, const ::armarx::Matrix_1_1_f& r)
    {
        return std::tie(l.e0) == std::tie(r.e0);
    } inline bool operator !=(const ::armarx::Matrix_1_1_f& l, const ::armarx::Matrix_1_1_f& r)
    {
        return std::tie(l.e0) != std::tie(r.e0);
    } inline bool operator >=(const ::armarx::Matrix_1_1_f& l, const ::armarx::Matrix_1_1_f& r)
    {
        return std::tie(l.e0) >= std::tie(r.e0);
    } inline bool operator <=(const ::armarx::Matrix_1_1_f& l, const ::armarx::Matrix_1_1_f& r)
    {
        return std::tie(l.e0) <= std::tie(r.e0);
    } inline bool operator <(const ::armarx::Matrix_2_1_f& l, const ::armarx::Matrix_2_1_f& r)
    {
        return std::tie(l.e0, l.e1) < std::tie(r.e0, r.e1);
    } inline bool operator >(const ::armarx::Matrix_2_1_f& l, const ::armarx::Matrix_2_1_f& r)
    {
        return std::tie(l.e0, l.e1) > std::tie(r.e0, r.e1);
    } inline bool operator ==(const ::armarx::Matrix_2_1_f& l, const ::armarx::Matrix_2_1_f& r)
    {
        return std::tie(l.e0, l.e1) == std::tie(r.e0, r.e1);
    } inline bool operator !=(const ::armarx::Matrix_2_1_f& l, const ::armarx::Matrix_2_1_f& r)
    {
        return std::tie(l.e0, l.e1) != std::tie(r.e0, r.e1);
    } inline bool operator >=(const ::armarx::Matrix_2_1_f& l, const ::armarx::Matrix_2_1_f& r)
    {
        return std::tie(l.e0, l.e1) >= std::tie(r.e0, r.e1);
    } inline bool operator <=(const ::armarx::Matrix_2_1_f& l, const ::armarx::Matrix_2_1_f& r)
    {
        return std::tie(l.e0, l.e1) <= std::tie(r.e0, r.e1);
    } inline bool operator <(const ::armarx::Matrix_3_1_f& l, const ::armarx::Matrix_3_1_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2) < std::tie(r.e0, r.e1, r.e2);
    } inline bool operator >(const ::armarx::Matrix_3_1_f& l, const ::armarx::Matrix_3_1_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2) > std::tie(r.e0, r.e1, r.e2);
    } inline bool operator ==(const ::armarx::Matrix_3_1_f& l, const ::armarx::Matrix_3_1_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2) == std::tie(r.e0, r.e1, r.e2);
    } inline bool operator !=(const ::armarx::Matrix_3_1_f& l, const ::armarx::Matrix_3_1_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2) != std::tie(r.e0, r.e1, r.e2);
    } inline bool operator >=(const ::armarx::Matrix_3_1_f& l, const ::armarx::Matrix_3_1_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2) >= std::tie(r.e0, r.e1, r.e2);
    } inline bool operator <=(const ::armarx::Matrix_3_1_f& l, const ::armarx::Matrix_3_1_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2) <= std::tie(r.e0, r.e1, r.e2);
    } inline bool operator <(const ::armarx::Matrix_4_1_f& l, const ::armarx::Matrix_4_1_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3) < std::tie(r.e0, r.e1, r.e2, r.e3);
    } inline bool operator >(const ::armarx::Matrix_4_1_f& l, const ::armarx::Matrix_4_1_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3) > std::tie(r.e0, r.e1, r.e2, r.e3);
    } inline bool operator ==(const ::armarx::Matrix_4_1_f& l, const ::armarx::Matrix_4_1_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3) == std::tie(r.e0, r.e1, r.e2, r.e3);
    } inline bool operator !=(const ::armarx::Matrix_4_1_f& l, const ::armarx::Matrix_4_1_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3) != std::tie(r.e0, r.e1, r.e2, r.e3);
    } inline bool operator >=(const ::armarx::Matrix_4_1_f& l, const ::armarx::Matrix_4_1_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3) >= std::tie(r.e0, r.e1, r.e2, r.e3);
    } inline bool operator <=(const ::armarx::Matrix_4_1_f& l, const ::armarx::Matrix_4_1_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3) <= std::tie(r.e0, r.e1, r.e2, r.e3);
    } inline bool operator <(const ::armarx::Matrix_5_1_f& l, const ::armarx::Matrix_5_1_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4) < std::tie(r.e0, r.e1, r.e2, r.e3, r.e4);
    } inline bool operator >(const ::armarx::Matrix_5_1_f& l, const ::armarx::Matrix_5_1_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4) > std::tie(r.e0, r.e1, r.e2, r.e3, r.e4);
    } inline bool operator ==(const ::armarx::Matrix_5_1_f& l, const ::armarx::Matrix_5_1_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4) == std::tie(r.e0, r.e1, r.e2, r.e3, r.e4);
    } inline bool operator !=(const ::armarx::Matrix_5_1_f& l, const ::armarx::Matrix_5_1_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4) != std::tie(r.e0, r.e1, r.e2, r.e3, r.e4);
    } inline bool operator >=(const ::armarx::Matrix_5_1_f& l, const ::armarx::Matrix_5_1_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4) >= std::tie(r.e0, r.e1, r.e2, r.e3, r.e4);
    } inline bool operator <=(const ::armarx::Matrix_5_1_f& l, const ::armarx::Matrix_5_1_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4) <= std::tie(r.e0, r.e1, r.e2, r.e3, r.e4);
    } inline bool operator <(const ::armarx::Matrix_6_1_f& l, const ::armarx::Matrix_6_1_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4, l.e5) < std::tie(r.e0, r.e1, r.e2, r.e3, r.e4, r.e5);
    } inline bool operator >(const ::armarx::Matrix_6_1_f& l, const ::armarx::Matrix_6_1_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4, l.e5) > std::tie(r.e0, r.e1, r.e2, r.e3, r.e4, r.e5);
    } inline bool operator ==(const ::armarx::Matrix_6_1_f& l, const ::armarx::Matrix_6_1_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4, l.e5) == std::tie(r.e0, r.e1, r.e2, r.e3, r.e4, r.e5);
    } inline bool operator !=(const ::armarx::Matrix_6_1_f& l, const ::armarx::Matrix_6_1_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4, l.e5) != std::tie(r.e0, r.e1, r.e2, r.e3, r.e4, r.e5);
    } inline bool operator >=(const ::armarx::Matrix_6_1_f& l, const ::armarx::Matrix_6_1_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4, l.e5) >= std::tie(r.e0, r.e1, r.e2, r.e3, r.e4, r.e5);
    } inline bool operator <=(const ::armarx::Matrix_6_1_f& l, const ::armarx::Matrix_6_1_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4, l.e5) <= std::tie(r.e0, r.e1, r.e2, r.e3, r.e4, r.e5);
    }
    inline bool operator <(const ::armarx::Matrix_1_2_f& l, const ::armarx::Matrix_1_2_f& r)
    {
        return std::tie(l.e0) < std::tie(r.e0);
    } inline bool operator >(const ::armarx::Matrix_1_2_f& l, const ::armarx::Matrix_1_2_f& r)
    {
        return std::tie(l.e0) > std::tie(r.e0);
    } inline bool operator ==(const ::armarx::Matrix_1_2_f& l, const ::armarx::Matrix_1_2_f& r)
    {
        return std::tie(l.e0) == std::tie(r.e0);
    } inline bool operator !=(const ::armarx::Matrix_1_2_f& l, const ::armarx::Matrix_1_2_f& r)
    {
        return std::tie(l.e0) != std::tie(r.e0);
    } inline bool operator >=(const ::armarx::Matrix_1_2_f& l, const ::armarx::Matrix_1_2_f& r)
    {
        return std::tie(l.e0) >= std::tie(r.e0);
    } inline bool operator <=(const ::armarx::Matrix_1_2_f& l, const ::armarx::Matrix_1_2_f& r)
    {
        return std::tie(l.e0) <= std::tie(r.e0);
    } inline bool operator <(const ::armarx::Matrix_2_2_f& l, const ::armarx::Matrix_2_2_f& r)
    {
        return std::tie(l.e0, l.e1) < std::tie(r.e0, r.e1);
    } inline bool operator >(const ::armarx::Matrix_2_2_f& l, const ::armarx::Matrix_2_2_f& r)
    {
        return std::tie(l.e0, l.e1) > std::tie(r.e0, r.e1);
    } inline bool operator ==(const ::armarx::Matrix_2_2_f& l, const ::armarx::Matrix_2_2_f& r)
    {
        return std::tie(l.e0, l.e1) == std::tie(r.e0, r.e1);
    } inline bool operator !=(const ::armarx::Matrix_2_2_f& l, const ::armarx::Matrix_2_2_f& r)
    {
        return std::tie(l.e0, l.e1) != std::tie(r.e0, r.e1);
    } inline bool operator >=(const ::armarx::Matrix_2_2_f& l, const ::armarx::Matrix_2_2_f& r)
    {
        return std::tie(l.e0, l.e1) >= std::tie(r.e0, r.e1);
    } inline bool operator <=(const ::armarx::Matrix_2_2_f& l, const ::armarx::Matrix_2_2_f& r)
    {
        return std::tie(l.e0, l.e1) <= std::tie(r.e0, r.e1);
    } inline bool operator <(const ::armarx::Matrix_3_2_f& l, const ::armarx::Matrix_3_2_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2) < std::tie(r.e0, r.e1, r.e2);
    } inline bool operator >(const ::armarx::Matrix_3_2_f& l, const ::armarx::Matrix_3_2_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2) > std::tie(r.e0, r.e1, r.e2);
    } inline bool operator ==(const ::armarx::Matrix_3_2_f& l, const ::armarx::Matrix_3_2_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2) == std::tie(r.e0, r.e1, r.e2);
    } inline bool operator !=(const ::armarx::Matrix_3_2_f& l, const ::armarx::Matrix_3_2_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2) != std::tie(r.e0, r.e1, r.e2);
    } inline bool operator >=(const ::armarx::Matrix_3_2_f& l, const ::armarx::Matrix_3_2_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2) >= std::tie(r.e0, r.e1, r.e2);
    } inline bool operator <=(const ::armarx::Matrix_3_2_f& l, const ::armarx::Matrix_3_2_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2) <= std::tie(r.e0, r.e1, r.e2);
    } inline bool operator <(const ::armarx::Matrix_4_2_f& l, const ::armarx::Matrix_4_2_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3) < std::tie(r.e0, r.e1, r.e2, r.e3);
    } inline bool operator >(const ::armarx::Matrix_4_2_f& l, const ::armarx::Matrix_4_2_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3) > std::tie(r.e0, r.e1, r.e2, r.e3);
    } inline bool operator ==(const ::armarx::Matrix_4_2_f& l, const ::armarx::Matrix_4_2_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3) == std::tie(r.e0, r.e1, r.e2, r.e3);
    } inline bool operator !=(const ::armarx::Matrix_4_2_f& l, const ::armarx::Matrix_4_2_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3) != std::tie(r.e0, r.e1, r.e2, r.e3);
    } inline bool operator >=(const ::armarx::Matrix_4_2_f& l, const ::armarx::Matrix_4_2_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3) >= std::tie(r.e0, r.e1, r.e2, r.e3);
    } inline bool operator <=(const ::armarx::Matrix_4_2_f& l, const ::armarx::Matrix_4_2_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3) <= std::tie(r.e0, r.e1, r.e2, r.e3);
    } inline bool operator <(const ::armarx::Matrix_5_2_f& l, const ::armarx::Matrix_5_2_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4) < std::tie(r.e0, r.e1, r.e2, r.e3, r.e4);
    } inline bool operator >(const ::armarx::Matrix_5_2_f& l, const ::armarx::Matrix_5_2_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4) > std::tie(r.e0, r.e1, r.e2, r.e3, r.e4);
    } inline bool operator ==(const ::armarx::Matrix_5_2_f& l, const ::armarx::Matrix_5_2_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4) == std::tie(r.e0, r.e1, r.e2, r.e3, r.e4);
    } inline bool operator !=(const ::armarx::Matrix_5_2_f& l, const ::armarx::Matrix_5_2_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4) != std::tie(r.e0, r.e1, r.e2, r.e3, r.e4);
    } inline bool operator >=(const ::armarx::Matrix_5_2_f& l, const ::armarx::Matrix_5_2_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4) >= std::tie(r.e0, r.e1, r.e2, r.e3, r.e4);
    } inline bool operator <=(const ::armarx::Matrix_5_2_f& l, const ::armarx::Matrix_5_2_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4) <= std::tie(r.e0, r.e1, r.e2, r.e3, r.e4);
    } inline bool operator <(const ::armarx::Matrix_6_2_f& l, const ::armarx::Matrix_6_2_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4, l.e5) < std::tie(r.e0, r.e1, r.e2, r.e3, r.e4, r.e5);
    } inline bool operator >(const ::armarx::Matrix_6_2_f& l, const ::armarx::Matrix_6_2_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4, l.e5) > std::tie(r.e0, r.e1, r.e2, r.e3, r.e4, r.e5);
    } inline bool operator ==(const ::armarx::Matrix_6_2_f& l, const ::armarx::Matrix_6_2_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4, l.e5) == std::tie(r.e0, r.e1, r.e2, r.e3, r.e4, r.e5);
    } inline bool operator !=(const ::armarx::Matrix_6_2_f& l, const ::armarx::Matrix_6_2_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4, l.e5) != std::tie(r.e0, r.e1, r.e2, r.e3, r.e4, r.e5);
    } inline bool operator >=(const ::armarx::Matrix_6_2_f& l, const ::armarx::Matrix_6_2_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4, l.e5) >= std::tie(r.e0, r.e1, r.e2, r.e3, r.e4, r.e5);
    } inline bool operator <=(const ::armarx::Matrix_6_2_f& l, const ::armarx::Matrix_6_2_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4, l.e5) <= std::tie(r.e0, r.e1, r.e2, r.e3, r.e4, r.e5);
    }
    inline bool operator <(const ::armarx::Matrix_1_3_f& l, const ::armarx::Matrix_1_3_f& r)
    {
        return std::tie(l.e0) < std::tie(r.e0);
    } inline bool operator >(const ::armarx::Matrix_1_3_f& l, const ::armarx::Matrix_1_3_f& r)
    {
        return std::tie(l.e0) > std::tie(r.e0);
    } inline bool operator ==(const ::armarx::Matrix_1_3_f& l, const ::armarx::Matrix_1_3_f& r)
    {
        return std::tie(l.e0) == std::tie(r.e0);
    } inline bool operator !=(const ::armarx::Matrix_1_3_f& l, const ::armarx::Matrix_1_3_f& r)
    {
        return std::tie(l.e0) != std::tie(r.e0);
    } inline bool operator >=(const ::armarx::Matrix_1_3_f& l, const ::armarx::Matrix_1_3_f& r)
    {
        return std::tie(l.e0) >= std::tie(r.e0);
    } inline bool operator <=(const ::armarx::Matrix_1_3_f& l, const ::armarx::Matrix_1_3_f& r)
    {
        return std::tie(l.e0) <= std::tie(r.e0);
    } inline bool operator <(const ::armarx::Matrix_2_3_f& l, const ::armarx::Matrix_2_3_f& r)
    {
        return std::tie(l.e0, l.e1) < std::tie(r.e0, r.e1);
    } inline bool operator >(const ::armarx::Matrix_2_3_f& l, const ::armarx::Matrix_2_3_f& r)
    {
        return std::tie(l.e0, l.e1) > std::tie(r.e0, r.e1);
    } inline bool operator ==(const ::armarx::Matrix_2_3_f& l, const ::armarx::Matrix_2_3_f& r)
    {
        return std::tie(l.e0, l.e1) == std::tie(r.e0, r.e1);
    } inline bool operator !=(const ::armarx::Matrix_2_3_f& l, const ::armarx::Matrix_2_3_f& r)
    {
        return std::tie(l.e0, l.e1) != std::tie(r.e0, r.e1);
    } inline bool operator >=(const ::armarx::Matrix_2_3_f& l, const ::armarx::Matrix_2_3_f& r)
    {
        return std::tie(l.e0, l.e1) >= std::tie(r.e0, r.e1);
    } inline bool operator <=(const ::armarx::Matrix_2_3_f& l, const ::armarx::Matrix_2_3_f& r)
    {
        return std::tie(l.e0, l.e1) <= std::tie(r.e0, r.e1);
    } inline bool operator <(const ::armarx::Matrix_3_3_f& l, const ::armarx::Matrix_3_3_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2) < std::tie(r.e0, r.e1, r.e2);
    } inline bool operator >(const ::armarx::Matrix_3_3_f& l, const ::armarx::Matrix_3_3_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2) > std::tie(r.e0, r.e1, r.e2);
    } inline bool operator ==(const ::armarx::Matrix_3_3_f& l, const ::armarx::Matrix_3_3_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2) == std::tie(r.e0, r.e1, r.e2);
    } inline bool operator !=(const ::armarx::Matrix_3_3_f& l, const ::armarx::Matrix_3_3_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2) != std::tie(r.e0, r.e1, r.e2);
    } inline bool operator >=(const ::armarx::Matrix_3_3_f& l, const ::armarx::Matrix_3_3_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2) >= std::tie(r.e0, r.e1, r.e2);
    } inline bool operator <=(const ::armarx::Matrix_3_3_f& l, const ::armarx::Matrix_3_3_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2) <= std::tie(r.e0, r.e1, r.e2);
    } inline bool operator <(const ::armarx::Matrix_4_3_f& l, const ::armarx::Matrix_4_3_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3) < std::tie(r.e0, r.e1, r.e2, r.e3);
    } inline bool operator >(const ::armarx::Matrix_4_3_f& l, const ::armarx::Matrix_4_3_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3) > std::tie(r.e0, r.e1, r.e2, r.e3);
    } inline bool operator ==(const ::armarx::Matrix_4_3_f& l, const ::armarx::Matrix_4_3_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3) == std::tie(r.e0, r.e1, r.e2, r.e3);
    } inline bool operator !=(const ::armarx::Matrix_4_3_f& l, const ::armarx::Matrix_4_3_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3) != std::tie(r.e0, r.e1, r.e2, r.e3);
    } inline bool operator >=(const ::armarx::Matrix_4_3_f& l, const ::armarx::Matrix_4_3_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3) >= std::tie(r.e0, r.e1, r.e2, r.e3);
    } inline bool operator <=(const ::armarx::Matrix_4_3_f& l, const ::armarx::Matrix_4_3_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3) <= std::tie(r.e0, r.e1, r.e2, r.e3);
    } inline bool operator <(const ::armarx::Matrix_5_3_f& l, const ::armarx::Matrix_5_3_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4) < std::tie(r.e0, r.e1, r.e2, r.e3, r.e4);
    } inline bool operator >(const ::armarx::Matrix_5_3_f& l, const ::armarx::Matrix_5_3_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4) > std::tie(r.e0, r.e1, r.e2, r.e3, r.e4);
    } inline bool operator ==(const ::armarx::Matrix_5_3_f& l, const ::armarx::Matrix_5_3_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4) == std::tie(r.e0, r.e1, r.e2, r.e3, r.e4);
    } inline bool operator !=(const ::armarx::Matrix_5_3_f& l, const ::armarx::Matrix_5_3_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4) != std::tie(r.e0, r.e1, r.e2, r.e3, r.e4);
    } inline bool operator >=(const ::armarx::Matrix_5_3_f& l, const ::armarx::Matrix_5_3_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4) >= std::tie(r.e0, r.e1, r.e2, r.e3, r.e4);
    } inline bool operator <=(const ::armarx::Matrix_5_3_f& l, const ::armarx::Matrix_5_3_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4) <= std::tie(r.e0, r.e1, r.e2, r.e3, r.e4);
    } inline bool operator <(const ::armarx::Matrix_6_3_f& l, const ::armarx::Matrix_6_3_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4, l.e5) < std::tie(r.e0, r.e1, r.e2, r.e3, r.e4, r.e5);
    } inline bool operator >(const ::armarx::Matrix_6_3_f& l, const ::armarx::Matrix_6_3_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4, l.e5) > std::tie(r.e0, r.e1, r.e2, r.e3, r.e4, r.e5);
    } inline bool operator ==(const ::armarx::Matrix_6_3_f& l, const ::armarx::Matrix_6_3_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4, l.e5) == std::tie(r.e0, r.e1, r.e2, r.e3, r.e4, r.e5);
    } inline bool operator !=(const ::armarx::Matrix_6_3_f& l, const ::armarx::Matrix_6_3_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4, l.e5) != std::tie(r.e0, r.e1, r.e2, r.e3, r.e4, r.e5);
    } inline bool operator >=(const ::armarx::Matrix_6_3_f& l, const ::armarx::Matrix_6_3_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4, l.e5) >= std::tie(r.e0, r.e1, r.e2, r.e3, r.e4, r.e5);
    } inline bool operator <=(const ::armarx::Matrix_6_3_f& l, const ::armarx::Matrix_6_3_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4, l.e5) <= std::tie(r.e0, r.e1, r.e2, r.e3, r.e4, r.e5);
    }
    inline bool operator <(const ::armarx::Matrix_1_4_f& l, const ::armarx::Matrix_1_4_f& r)
    {
        return std::tie(l.e0) < std::tie(r.e0);
    } inline bool operator >(const ::armarx::Matrix_1_4_f& l, const ::armarx::Matrix_1_4_f& r)
    {
        return std::tie(l.e0) > std::tie(r.e0);
    } inline bool operator ==(const ::armarx::Matrix_1_4_f& l, const ::armarx::Matrix_1_4_f& r)
    {
        return std::tie(l.e0) == std::tie(r.e0);
    } inline bool operator !=(const ::armarx::Matrix_1_4_f& l, const ::armarx::Matrix_1_4_f& r)
    {
        return std::tie(l.e0) != std::tie(r.e0);
    } inline bool operator >=(const ::armarx::Matrix_1_4_f& l, const ::armarx::Matrix_1_4_f& r)
    {
        return std::tie(l.e0) >= std::tie(r.e0);
    } inline bool operator <=(const ::armarx::Matrix_1_4_f& l, const ::armarx::Matrix_1_4_f& r)
    {
        return std::tie(l.e0) <= std::tie(r.e0);
    } inline bool operator <(const ::armarx::Matrix_2_4_f& l, const ::armarx::Matrix_2_4_f& r)
    {
        return std::tie(l.e0, l.e1) < std::tie(r.e0, r.e1);
    } inline bool operator >(const ::armarx::Matrix_2_4_f& l, const ::armarx::Matrix_2_4_f& r)
    {
        return std::tie(l.e0, l.e1) > std::tie(r.e0, r.e1);
    } inline bool operator ==(const ::armarx::Matrix_2_4_f& l, const ::armarx::Matrix_2_4_f& r)
    {
        return std::tie(l.e0, l.e1) == std::tie(r.e0, r.e1);
    } inline bool operator !=(const ::armarx::Matrix_2_4_f& l, const ::armarx::Matrix_2_4_f& r)
    {
        return std::tie(l.e0, l.e1) != std::tie(r.e0, r.e1);
    } inline bool operator >=(const ::armarx::Matrix_2_4_f& l, const ::armarx::Matrix_2_4_f& r)
    {
        return std::tie(l.e0, l.e1) >= std::tie(r.e0, r.e1);
    } inline bool operator <=(const ::armarx::Matrix_2_4_f& l, const ::armarx::Matrix_2_4_f& r)
    {
        return std::tie(l.e0, l.e1) <= std::tie(r.e0, r.e1);
    } inline bool operator <(const ::armarx::Matrix_3_4_f& l, const ::armarx::Matrix_3_4_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2) < std::tie(r.e0, r.e1, r.e2);
    } inline bool operator >(const ::armarx::Matrix_3_4_f& l, const ::armarx::Matrix_3_4_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2) > std::tie(r.e0, r.e1, r.e2);
    } inline bool operator ==(const ::armarx::Matrix_3_4_f& l, const ::armarx::Matrix_3_4_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2) == std::tie(r.e0, r.e1, r.e2);
    } inline bool operator !=(const ::armarx::Matrix_3_4_f& l, const ::armarx::Matrix_3_4_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2) != std::tie(r.e0, r.e1, r.e2);
    } inline bool operator >=(const ::armarx::Matrix_3_4_f& l, const ::armarx::Matrix_3_4_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2) >= std::tie(r.e0, r.e1, r.e2);
    } inline bool operator <=(const ::armarx::Matrix_3_4_f& l, const ::armarx::Matrix_3_4_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2) <= std::tie(r.e0, r.e1, r.e2);
    } inline bool operator <(const ::armarx::Matrix_4_4_f& l, const ::armarx::Matrix_4_4_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3) < std::tie(r.e0, r.e1, r.e2, r.e3);
    } inline bool operator >(const ::armarx::Matrix_4_4_f& l, const ::armarx::Matrix_4_4_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3) > std::tie(r.e0, r.e1, r.e2, r.e3);
    } inline bool operator ==(const ::armarx::Matrix_4_4_f& l, const ::armarx::Matrix_4_4_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3) == std::tie(r.e0, r.e1, r.e2, r.e3);
    } inline bool operator !=(const ::armarx::Matrix_4_4_f& l, const ::armarx::Matrix_4_4_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3) != std::tie(r.e0, r.e1, r.e2, r.e3);
    } inline bool operator >=(const ::armarx::Matrix_4_4_f& l, const ::armarx::Matrix_4_4_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3) >= std::tie(r.e0, r.e1, r.e2, r.e3);
    } inline bool operator <=(const ::armarx::Matrix_4_4_f& l, const ::armarx::Matrix_4_4_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3) <= std::tie(r.e0, r.e1, r.e2, r.e3);
    } inline bool operator <(const ::armarx::Matrix_5_4_f& l, const ::armarx::Matrix_5_4_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4) < std::tie(r.e0, r.e1, r.e2, r.e3, r.e4);
    } inline bool operator >(const ::armarx::Matrix_5_4_f& l, const ::armarx::Matrix_5_4_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4) > std::tie(r.e0, r.e1, r.e2, r.e3, r.e4);
    } inline bool operator ==(const ::armarx::Matrix_5_4_f& l, const ::armarx::Matrix_5_4_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4) == std::tie(r.e0, r.e1, r.e2, r.e3, r.e4);
    } inline bool operator !=(const ::armarx::Matrix_5_4_f& l, const ::armarx::Matrix_5_4_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4) != std::tie(r.e0, r.e1, r.e2, r.e3, r.e4);
    } inline bool operator >=(const ::armarx::Matrix_5_4_f& l, const ::armarx::Matrix_5_4_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4) >= std::tie(r.e0, r.e1, r.e2, r.e3, r.e4);
    } inline bool operator <=(const ::armarx::Matrix_5_4_f& l, const ::armarx::Matrix_5_4_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4) <= std::tie(r.e0, r.e1, r.e2, r.e3, r.e4);
    } inline bool operator <(const ::armarx::Matrix_6_4_f& l, const ::armarx::Matrix_6_4_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4, l.e5) < std::tie(r.e0, r.e1, r.e2, r.e3, r.e4, r.e5);
    } inline bool operator >(const ::armarx::Matrix_6_4_f& l, const ::armarx::Matrix_6_4_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4, l.e5) > std::tie(r.e0, r.e1, r.e2, r.e3, r.e4, r.e5);
    } inline bool operator ==(const ::armarx::Matrix_6_4_f& l, const ::armarx::Matrix_6_4_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4, l.e5) == std::tie(r.e0, r.e1, r.e2, r.e3, r.e4, r.e5);
    } inline bool operator !=(const ::armarx::Matrix_6_4_f& l, const ::armarx::Matrix_6_4_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4, l.e5) != std::tie(r.e0, r.e1, r.e2, r.e3, r.e4, r.e5);
    } inline bool operator >=(const ::armarx::Matrix_6_4_f& l, const ::armarx::Matrix_6_4_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4, l.e5) >= std::tie(r.e0, r.e1, r.e2, r.e3, r.e4, r.e5);
    } inline bool operator <=(const ::armarx::Matrix_6_4_f& l, const ::armarx::Matrix_6_4_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4, l.e5) <= std::tie(r.e0, r.e1, r.e2, r.e3, r.e4, r.e5);
    }
    inline bool operator <(const ::armarx::Matrix_1_5_f& l, const ::armarx::Matrix_1_5_f& r)
    {
        return std::tie(l.e0) < std::tie(r.e0);
    } inline bool operator >(const ::armarx::Matrix_1_5_f& l, const ::armarx::Matrix_1_5_f& r)
    {
        return std::tie(l.e0) > std::tie(r.e0);
    } inline bool operator ==(const ::armarx::Matrix_1_5_f& l, const ::armarx::Matrix_1_5_f& r)
    {
        return std::tie(l.e0) == std::tie(r.e0);
    } inline bool operator !=(const ::armarx::Matrix_1_5_f& l, const ::armarx::Matrix_1_5_f& r)
    {
        return std::tie(l.e0) != std::tie(r.e0);
    } inline bool operator >=(const ::armarx::Matrix_1_5_f& l, const ::armarx::Matrix_1_5_f& r)
    {
        return std::tie(l.e0) >= std::tie(r.e0);
    } inline bool operator <=(const ::armarx::Matrix_1_5_f& l, const ::armarx::Matrix_1_5_f& r)
    {
        return std::tie(l.e0) <= std::tie(r.e0);
    } inline bool operator <(const ::armarx::Matrix_2_5_f& l, const ::armarx::Matrix_2_5_f& r)
    {
        return std::tie(l.e0, l.e1) < std::tie(r.e0, r.e1);
    } inline bool operator >(const ::armarx::Matrix_2_5_f& l, const ::armarx::Matrix_2_5_f& r)
    {
        return std::tie(l.e0, l.e1) > std::tie(r.e0, r.e1);
    } inline bool operator ==(const ::armarx::Matrix_2_5_f& l, const ::armarx::Matrix_2_5_f& r)
    {
        return std::tie(l.e0, l.e1) == std::tie(r.e0, r.e1);
    } inline bool operator !=(const ::armarx::Matrix_2_5_f& l, const ::armarx::Matrix_2_5_f& r)
    {
        return std::tie(l.e0, l.e1) != std::tie(r.e0, r.e1);
    } inline bool operator >=(const ::armarx::Matrix_2_5_f& l, const ::armarx::Matrix_2_5_f& r)
    {
        return std::tie(l.e0, l.e1) >= std::tie(r.e0, r.e1);
    } inline bool operator <=(const ::armarx::Matrix_2_5_f& l, const ::armarx::Matrix_2_5_f& r)
    {
        return std::tie(l.e0, l.e1) <= std::tie(r.e0, r.e1);
    } inline bool operator <(const ::armarx::Matrix_3_5_f& l, const ::armarx::Matrix_3_5_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2) < std::tie(r.e0, r.e1, r.e2);
    } inline bool operator >(const ::armarx::Matrix_3_5_f& l, const ::armarx::Matrix_3_5_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2) > std::tie(r.e0, r.e1, r.e2);
    } inline bool operator ==(const ::armarx::Matrix_3_5_f& l, const ::armarx::Matrix_3_5_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2) == std::tie(r.e0, r.e1, r.e2);
    } inline bool operator !=(const ::armarx::Matrix_3_5_f& l, const ::armarx::Matrix_3_5_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2) != std::tie(r.e0, r.e1, r.e2);
    } inline bool operator >=(const ::armarx::Matrix_3_5_f& l, const ::armarx::Matrix_3_5_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2) >= std::tie(r.e0, r.e1, r.e2);
    } inline bool operator <=(const ::armarx::Matrix_3_5_f& l, const ::armarx::Matrix_3_5_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2) <= std::tie(r.e0, r.e1, r.e2);
    } inline bool operator <(const ::armarx::Matrix_4_5_f& l, const ::armarx::Matrix_4_5_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3) < std::tie(r.e0, r.e1, r.e2, r.e3);
    } inline bool operator >(const ::armarx::Matrix_4_5_f& l, const ::armarx::Matrix_4_5_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3) > std::tie(r.e0, r.e1, r.e2, r.e3);
    } inline bool operator ==(const ::armarx::Matrix_4_5_f& l, const ::armarx::Matrix_4_5_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3) == std::tie(r.e0, r.e1, r.e2, r.e3);
    } inline bool operator !=(const ::armarx::Matrix_4_5_f& l, const ::armarx::Matrix_4_5_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3) != std::tie(r.e0, r.e1, r.e2, r.e3);
    } inline bool operator >=(const ::armarx::Matrix_4_5_f& l, const ::armarx::Matrix_4_5_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3) >= std::tie(r.e0, r.e1, r.e2, r.e3);
    } inline bool operator <=(const ::armarx::Matrix_4_5_f& l, const ::armarx::Matrix_4_5_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3) <= std::tie(r.e0, r.e1, r.e2, r.e3);
    } inline bool operator <(const ::armarx::Matrix_5_5_f& l, const ::armarx::Matrix_5_5_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4) < std::tie(r.e0, r.e1, r.e2, r.e3, r.e4);
    } inline bool operator >(const ::armarx::Matrix_5_5_f& l, const ::armarx::Matrix_5_5_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4) > std::tie(r.e0, r.e1, r.e2, r.e3, r.e4);
    } inline bool operator ==(const ::armarx::Matrix_5_5_f& l, const ::armarx::Matrix_5_5_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4) == std::tie(r.e0, r.e1, r.e2, r.e3, r.e4);
    } inline bool operator !=(const ::armarx::Matrix_5_5_f& l, const ::armarx::Matrix_5_5_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4) != std::tie(r.e0, r.e1, r.e2, r.e3, r.e4);
    } inline bool operator >=(const ::armarx::Matrix_5_5_f& l, const ::armarx::Matrix_5_5_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4) >= std::tie(r.e0, r.e1, r.e2, r.e3, r.e4);
    } inline bool operator <=(const ::armarx::Matrix_5_5_f& l, const ::armarx::Matrix_5_5_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4) <= std::tie(r.e0, r.e1, r.e2, r.e3, r.e4);
    } inline bool operator <(const ::armarx::Matrix_6_5_f& l, const ::armarx::Matrix_6_5_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4, l.e5) < std::tie(r.e0, r.e1, r.e2, r.e3, r.e4, r.e5);
    } inline bool operator >(const ::armarx::Matrix_6_5_f& l, const ::armarx::Matrix_6_5_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4, l.e5) > std::tie(r.e0, r.e1, r.e2, r.e3, r.e4, r.e5);
    } inline bool operator ==(const ::armarx::Matrix_6_5_f& l, const ::armarx::Matrix_6_5_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4, l.e5) == std::tie(r.e0, r.e1, r.e2, r.e3, r.e4, r.e5);
    } inline bool operator !=(const ::armarx::Matrix_6_5_f& l, const ::armarx::Matrix_6_5_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4, l.e5) != std::tie(r.e0, r.e1, r.e2, r.e3, r.e4, r.e5);
    } inline bool operator >=(const ::armarx::Matrix_6_5_f& l, const ::armarx::Matrix_6_5_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4, l.e5) >= std::tie(r.e0, r.e1, r.e2, r.e3, r.e4, r.e5);
    } inline bool operator <=(const ::armarx::Matrix_6_5_f& l, const ::armarx::Matrix_6_5_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4, l.e5) <= std::tie(r.e0, r.e1, r.e2, r.e3, r.e4, r.e5);
    }
    inline bool operator <(const ::armarx::Matrix_1_6_f& l, const ::armarx::Matrix_1_6_f& r)
    {
        return std::tie(l.e0) < std::tie(r.e0);
    } inline bool operator >(const ::armarx::Matrix_1_6_f& l, const ::armarx::Matrix_1_6_f& r)
    {
        return std::tie(l.e0) > std::tie(r.e0);
    } inline bool operator ==(const ::armarx::Matrix_1_6_f& l, const ::armarx::Matrix_1_6_f& r)
    {
        return std::tie(l.e0) == std::tie(r.e0);
    } inline bool operator !=(const ::armarx::Matrix_1_6_f& l, const ::armarx::Matrix_1_6_f& r)
    {
        return std::tie(l.e0) != std::tie(r.e0);
    } inline bool operator >=(const ::armarx::Matrix_1_6_f& l, const ::armarx::Matrix_1_6_f& r)
    {
        return std::tie(l.e0) >= std::tie(r.e0);
    } inline bool operator <=(const ::armarx::Matrix_1_6_f& l, const ::armarx::Matrix_1_6_f& r)
    {
        return std::tie(l.e0) <= std::tie(r.e0);
    } inline bool operator <(const ::armarx::Matrix_2_6_f& l, const ::armarx::Matrix_2_6_f& r)
    {
        return std::tie(l.e0, l.e1) < std::tie(r.e0, r.e1);
    } inline bool operator >(const ::armarx::Matrix_2_6_f& l, const ::armarx::Matrix_2_6_f& r)
    {
        return std::tie(l.e0, l.e1) > std::tie(r.e0, r.e1);
    } inline bool operator ==(const ::armarx::Matrix_2_6_f& l, const ::armarx::Matrix_2_6_f& r)
    {
        return std::tie(l.e0, l.e1) == std::tie(r.e0, r.e1);
    } inline bool operator !=(const ::armarx::Matrix_2_6_f& l, const ::armarx::Matrix_2_6_f& r)
    {
        return std::tie(l.e0, l.e1) != std::tie(r.e0, r.e1);
    } inline bool operator >=(const ::armarx::Matrix_2_6_f& l, const ::armarx::Matrix_2_6_f& r)
    {
        return std::tie(l.e0, l.e1) >= std::tie(r.e0, r.e1);
    } inline bool operator <=(const ::armarx::Matrix_2_6_f& l, const ::armarx::Matrix_2_6_f& r)
    {
        return std::tie(l.e0, l.e1) <= std::tie(r.e0, r.e1);
    } inline bool operator <(const ::armarx::Matrix_3_6_f& l, const ::armarx::Matrix_3_6_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2) < std::tie(r.e0, r.e1, r.e2);
    } inline bool operator >(const ::armarx::Matrix_3_6_f& l, const ::armarx::Matrix_3_6_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2) > std::tie(r.e0, r.e1, r.e2);
    } inline bool operator ==(const ::armarx::Matrix_3_6_f& l, const ::armarx::Matrix_3_6_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2) == std::tie(r.e0, r.e1, r.e2);
    } inline bool operator !=(const ::armarx::Matrix_3_6_f& l, const ::armarx::Matrix_3_6_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2) != std::tie(r.e0, r.e1, r.e2);
    } inline bool operator >=(const ::armarx::Matrix_3_6_f& l, const ::armarx::Matrix_3_6_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2) >= std::tie(r.e0, r.e1, r.e2);
    } inline bool operator <=(const ::armarx::Matrix_3_6_f& l, const ::armarx::Matrix_3_6_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2) <= std::tie(r.e0, r.e1, r.e2);
    } inline bool operator <(const ::armarx::Matrix_4_6_f& l, const ::armarx::Matrix_4_6_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3) < std::tie(r.e0, r.e1, r.e2, r.e3);
    } inline bool operator >(const ::armarx::Matrix_4_6_f& l, const ::armarx::Matrix_4_6_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3) > std::tie(r.e0, r.e1, r.e2, r.e3);
    } inline bool operator ==(const ::armarx::Matrix_4_6_f& l, const ::armarx::Matrix_4_6_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3) == std::tie(r.e0, r.e1, r.e2, r.e3);
    } inline bool operator !=(const ::armarx::Matrix_4_6_f& l, const ::armarx::Matrix_4_6_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3) != std::tie(r.e0, r.e1, r.e2, r.e3);
    } inline bool operator >=(const ::armarx::Matrix_4_6_f& l, const ::armarx::Matrix_4_6_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3) >= std::tie(r.e0, r.e1, r.e2, r.e3);
    } inline bool operator <=(const ::armarx::Matrix_4_6_f& l, const ::armarx::Matrix_4_6_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3) <= std::tie(r.e0, r.e1, r.e2, r.e3);
    } inline bool operator <(const ::armarx::Matrix_5_6_f& l, const ::armarx::Matrix_5_6_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4) < std::tie(r.e0, r.e1, r.e2, r.e3, r.e4);
    } inline bool operator >(const ::armarx::Matrix_5_6_f& l, const ::armarx::Matrix_5_6_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4) > std::tie(r.e0, r.e1, r.e2, r.e3, r.e4);
    } inline bool operator ==(const ::armarx::Matrix_5_6_f& l, const ::armarx::Matrix_5_6_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4) == std::tie(r.e0, r.e1, r.e2, r.e3, r.e4);
    } inline bool operator !=(const ::armarx::Matrix_5_6_f& l, const ::armarx::Matrix_5_6_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4) != std::tie(r.e0, r.e1, r.e2, r.e3, r.e4);
    } inline bool operator >=(const ::armarx::Matrix_5_6_f& l, const ::armarx::Matrix_5_6_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4) >= std::tie(r.e0, r.e1, r.e2, r.e3, r.e4);
    } inline bool operator <=(const ::armarx::Matrix_5_6_f& l, const ::armarx::Matrix_5_6_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4) <= std::tie(r.e0, r.e1, r.e2, r.e3, r.e4);
    } inline bool operator <(const ::armarx::Matrix_6_6_f& l, const ::armarx::Matrix_6_6_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4, l.e5) < std::tie(r.e0, r.e1, r.e2, r.e3, r.e4, r.e5);
    } inline bool operator >(const ::armarx::Matrix_6_6_f& l, const ::armarx::Matrix_6_6_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4, l.e5) > std::tie(r.e0, r.e1, r.e2, r.e3, r.e4, r.e5);
    } inline bool operator ==(const ::armarx::Matrix_6_6_f& l, const ::armarx::Matrix_6_6_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4, l.e5) == std::tie(r.e0, r.e1, r.e2, r.e3, r.e4, r.e5);
    } inline bool operator !=(const ::armarx::Matrix_6_6_f& l, const ::armarx::Matrix_6_6_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4, l.e5) != std::tie(r.e0, r.e1, r.e2, r.e3, r.e4, r.e5);
    } inline bool operator >=(const ::armarx::Matrix_6_6_f& l, const ::armarx::Matrix_6_6_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4, l.e5) >= std::tie(r.e0, r.e1, r.e2, r.e3, r.e4, r.e5);
    } inline bool operator <=(const ::armarx::Matrix_6_6_f& l, const ::armarx::Matrix_6_6_f& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4, l.e5) <= std::tie(r.e0, r.e1, r.e2, r.e3, r.e4, r.e5);
    }
    inline bool operator <(const ::armarx::Vector1d& l, const ::armarx::Vector1d& r)
    {
        return std::tie(l.e0) < std::tie(r.e0);
    } inline bool operator >(const ::armarx::Vector1d& l, const ::armarx::Vector1d& r)
    {
        return std::tie(l.e0) > std::tie(r.e0);
    } inline bool operator ==(const ::armarx::Vector1d& l, const ::armarx::Vector1d& r)
    {
        return std::tie(l.e0) == std::tie(r.e0);
    } inline bool operator !=(const ::armarx::Vector1d& l, const ::armarx::Vector1d& r)
    {
        return std::tie(l.e0) != std::tie(r.e0);
    } inline bool operator >=(const ::armarx::Vector1d& l, const ::armarx::Vector1d& r)
    {
        return std::tie(l.e0) >= std::tie(r.e0);
    } inline bool operator <=(const ::armarx::Vector1d& l, const ::armarx::Vector1d& r)
    {
        return std::tie(l.e0) <= std::tie(r.e0);
    } inline bool operator <(const ::armarx::Vector2d& l, const ::armarx::Vector2d& r)
    {
        return std::tie(l.e0, l.e1) < std::tie(r.e0, r.e1);
    } inline bool operator >(const ::armarx::Vector2d& l, const ::armarx::Vector2d& r)
    {
        return std::tie(l.e0, l.e1) > std::tie(r.e0, r.e1);
    } inline bool operator ==(const ::armarx::Vector2d& l, const ::armarx::Vector2d& r)
    {
        return std::tie(l.e0, l.e1) == std::tie(r.e0, r.e1);
    } inline bool operator !=(const ::armarx::Vector2d& l, const ::armarx::Vector2d& r)
    {
        return std::tie(l.e0, l.e1) != std::tie(r.e0, r.e1);
    } inline bool operator >=(const ::armarx::Vector2d& l, const ::armarx::Vector2d& r)
    {
        return std::tie(l.e0, l.e1) >= std::tie(r.e0, r.e1);
    } inline bool operator <=(const ::armarx::Vector2d& l, const ::armarx::Vector2d& r)
    {
        return std::tie(l.e0, l.e1) <= std::tie(r.e0, r.e1);
    } inline bool operator <(const ::armarx::Vector3d& l, const ::armarx::Vector3d& r)
    {
        return std::tie(l.e0, l.e1, l.e2) < std::tie(r.e0, r.e1, r.e2);
    } inline bool operator >(const ::armarx::Vector3d& l, const ::armarx::Vector3d& r)
    {
        return std::tie(l.e0, l.e1, l.e2) > std::tie(r.e0, r.e1, r.e2);
    } inline bool operator ==(const ::armarx::Vector3d& l, const ::armarx::Vector3d& r)
    {
        return std::tie(l.e0, l.e1, l.e2) == std::tie(r.e0, r.e1, r.e2);
    } inline bool operator !=(const ::armarx::Vector3d& l, const ::armarx::Vector3d& r)
    {
        return std::tie(l.e0, l.e1, l.e2) != std::tie(r.e0, r.e1, r.e2);
    } inline bool operator >=(const ::armarx::Vector3d& l, const ::armarx::Vector3d& r)
    {
        return std::tie(l.e0, l.e1, l.e2) >= std::tie(r.e0, r.e1, r.e2);
    } inline bool operator <=(const ::armarx::Vector3d& l, const ::armarx::Vector3d& r)
    {
        return std::tie(l.e0, l.e1, l.e2) <= std::tie(r.e0, r.e1, r.e2);
    } inline bool operator <(const ::armarx::Vector4d& l, const ::armarx::Vector4d& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3) < std::tie(r.e0, r.e1, r.e2, r.e3);
    } inline bool operator >(const ::armarx::Vector4d& l, const ::armarx::Vector4d& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3) > std::tie(r.e0, r.e1, r.e2, r.e3);
    } inline bool operator ==(const ::armarx::Vector4d& l, const ::armarx::Vector4d& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3) == std::tie(r.e0, r.e1, r.e2, r.e3);
    } inline bool operator !=(const ::armarx::Vector4d& l, const ::armarx::Vector4d& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3) != std::tie(r.e0, r.e1, r.e2, r.e3);
    } inline bool operator >=(const ::armarx::Vector4d& l, const ::armarx::Vector4d& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3) >= std::tie(r.e0, r.e1, r.e2, r.e3);
    } inline bool operator <=(const ::armarx::Vector4d& l, const ::armarx::Vector4d& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3) <= std::tie(r.e0, r.e1, r.e2, r.e3);
    } inline bool operator <(const ::armarx::Vector5d& l, const ::armarx::Vector5d& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4) < std::tie(r.e0, r.e1, r.e2, r.e3, r.e4);
    } inline bool operator >(const ::armarx::Vector5d& l, const ::armarx::Vector5d& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4) > std::tie(r.e0, r.e1, r.e2, r.e3, r.e4);
    } inline bool operator ==(const ::armarx::Vector5d& l, const ::armarx::Vector5d& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4) == std::tie(r.e0, r.e1, r.e2, r.e3, r.e4);
    } inline bool operator !=(const ::armarx::Vector5d& l, const ::armarx::Vector5d& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4) != std::tie(r.e0, r.e1, r.e2, r.e3, r.e4);
    } inline bool operator >=(const ::armarx::Vector5d& l, const ::armarx::Vector5d& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4) >= std::tie(r.e0, r.e1, r.e2, r.e3, r.e4);
    } inline bool operator <=(const ::armarx::Vector5d& l, const ::armarx::Vector5d& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4) <= std::tie(r.e0, r.e1, r.e2, r.e3, r.e4);
    } inline bool operator <(const ::armarx::Vector6d& l, const ::armarx::Vector6d& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4, l.e5) < std::tie(r.e0, r.e1, r.e2, r.e3, r.e4, r.e5);
    } inline bool operator >(const ::armarx::Vector6d& l, const ::armarx::Vector6d& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4, l.e5) > std::tie(r.e0, r.e1, r.e2, r.e3, r.e4, r.e5);
    } inline bool operator ==(const ::armarx::Vector6d& l, const ::armarx::Vector6d& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4, l.e5) == std::tie(r.e0, r.e1, r.e2, r.e3, r.e4, r.e5);
    } inline bool operator !=(const ::armarx::Vector6d& l, const ::armarx::Vector6d& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4, l.e5) != std::tie(r.e0, r.e1, r.e2, r.e3, r.e4, r.e5);
    } inline bool operator >=(const ::armarx::Vector6d& l, const ::armarx::Vector6d& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4, l.e5) >= std::tie(r.e0, r.e1, r.e2, r.e3, r.e4, r.e5);
    } inline bool operator <=(const ::armarx::Vector6d& l, const ::armarx::Vector6d& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4, l.e5) <= std::tie(r.e0, r.e1, r.e2, r.e3, r.e4, r.e5);
    }
    inline bool operator <(const ::armarx::Matrix_1_1_d& l, const ::armarx::Matrix_1_1_d& r)
    {
        return std::tie(l.e0) < std::tie(r.e0);
    } inline bool operator >(const ::armarx::Matrix_1_1_d& l, const ::armarx::Matrix_1_1_d& r)
    {
        return std::tie(l.e0) > std::tie(r.e0);
    } inline bool operator ==(const ::armarx::Matrix_1_1_d& l, const ::armarx::Matrix_1_1_d& r)
    {
        return std::tie(l.e0) == std::tie(r.e0);
    } inline bool operator !=(const ::armarx::Matrix_1_1_d& l, const ::armarx::Matrix_1_1_d& r)
    {
        return std::tie(l.e0) != std::tie(r.e0);
    } inline bool operator >=(const ::armarx::Matrix_1_1_d& l, const ::armarx::Matrix_1_1_d& r)
    {
        return std::tie(l.e0) >= std::tie(r.e0);
    } inline bool operator <=(const ::armarx::Matrix_1_1_d& l, const ::armarx::Matrix_1_1_d& r)
    {
        return std::tie(l.e0) <= std::tie(r.e0);
    } inline bool operator <(const ::armarx::Matrix_2_1_d& l, const ::armarx::Matrix_2_1_d& r)
    {
        return std::tie(l.e0, l.e1) < std::tie(r.e0, r.e1);
    } inline bool operator >(const ::armarx::Matrix_2_1_d& l, const ::armarx::Matrix_2_1_d& r)
    {
        return std::tie(l.e0, l.e1) > std::tie(r.e0, r.e1);
    } inline bool operator ==(const ::armarx::Matrix_2_1_d& l, const ::armarx::Matrix_2_1_d& r)
    {
        return std::tie(l.e0, l.e1) == std::tie(r.e0, r.e1);
    } inline bool operator !=(const ::armarx::Matrix_2_1_d& l, const ::armarx::Matrix_2_1_d& r)
    {
        return std::tie(l.e0, l.e1) != std::tie(r.e0, r.e1);
    } inline bool operator >=(const ::armarx::Matrix_2_1_d& l, const ::armarx::Matrix_2_1_d& r)
    {
        return std::tie(l.e0, l.e1) >= std::tie(r.e0, r.e1);
    } inline bool operator <=(const ::armarx::Matrix_2_1_d& l, const ::armarx::Matrix_2_1_d& r)
    {
        return std::tie(l.e0, l.e1) <= std::tie(r.e0, r.e1);
    } inline bool operator <(const ::armarx::Matrix_3_1_d& l, const ::armarx::Matrix_3_1_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2) < std::tie(r.e0, r.e1, r.e2);
    } inline bool operator >(const ::armarx::Matrix_3_1_d& l, const ::armarx::Matrix_3_1_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2) > std::tie(r.e0, r.e1, r.e2);
    } inline bool operator ==(const ::armarx::Matrix_3_1_d& l, const ::armarx::Matrix_3_1_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2) == std::tie(r.e0, r.e1, r.e2);
    } inline bool operator !=(const ::armarx::Matrix_3_1_d& l, const ::armarx::Matrix_3_1_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2) != std::tie(r.e0, r.e1, r.e2);
    } inline bool operator >=(const ::armarx::Matrix_3_1_d& l, const ::armarx::Matrix_3_1_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2) >= std::tie(r.e0, r.e1, r.e2);
    } inline bool operator <=(const ::armarx::Matrix_3_1_d& l, const ::armarx::Matrix_3_1_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2) <= std::tie(r.e0, r.e1, r.e2);
    } inline bool operator <(const ::armarx::Matrix_4_1_d& l, const ::armarx::Matrix_4_1_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3) < std::tie(r.e0, r.e1, r.e2, r.e3);
    } inline bool operator >(const ::armarx::Matrix_4_1_d& l, const ::armarx::Matrix_4_1_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3) > std::tie(r.e0, r.e1, r.e2, r.e3);
    } inline bool operator ==(const ::armarx::Matrix_4_1_d& l, const ::armarx::Matrix_4_1_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3) == std::tie(r.e0, r.e1, r.e2, r.e3);
    } inline bool operator !=(const ::armarx::Matrix_4_1_d& l, const ::armarx::Matrix_4_1_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3) != std::tie(r.e0, r.e1, r.e2, r.e3);
    } inline bool operator >=(const ::armarx::Matrix_4_1_d& l, const ::armarx::Matrix_4_1_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3) >= std::tie(r.e0, r.e1, r.e2, r.e3);
    } inline bool operator <=(const ::armarx::Matrix_4_1_d& l, const ::armarx::Matrix_4_1_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3) <= std::tie(r.e0, r.e1, r.e2, r.e3);
    } inline bool operator <(const ::armarx::Matrix_5_1_d& l, const ::armarx::Matrix_5_1_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4) < std::tie(r.e0, r.e1, r.e2, r.e3, r.e4);
    } inline bool operator >(const ::armarx::Matrix_5_1_d& l, const ::armarx::Matrix_5_1_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4) > std::tie(r.e0, r.e1, r.e2, r.e3, r.e4);
    } inline bool operator ==(const ::armarx::Matrix_5_1_d& l, const ::armarx::Matrix_5_1_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4) == std::tie(r.e0, r.e1, r.e2, r.e3, r.e4);
    } inline bool operator !=(const ::armarx::Matrix_5_1_d& l, const ::armarx::Matrix_5_1_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4) != std::tie(r.e0, r.e1, r.e2, r.e3, r.e4);
    } inline bool operator >=(const ::armarx::Matrix_5_1_d& l, const ::armarx::Matrix_5_1_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4) >= std::tie(r.e0, r.e1, r.e2, r.e3, r.e4);
    } inline bool operator <=(const ::armarx::Matrix_5_1_d& l, const ::armarx::Matrix_5_1_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4) <= std::tie(r.e0, r.e1, r.e2, r.e3, r.e4);
    } inline bool operator <(const ::armarx::Matrix_6_1_d& l, const ::armarx::Matrix_6_1_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4, l.e5) < std::tie(r.e0, r.e1, r.e2, r.e3, r.e4, r.e5);
    } inline bool operator >(const ::armarx::Matrix_6_1_d& l, const ::armarx::Matrix_6_1_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4, l.e5) > std::tie(r.e0, r.e1, r.e2, r.e3, r.e4, r.e5);
    } inline bool operator ==(const ::armarx::Matrix_6_1_d& l, const ::armarx::Matrix_6_1_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4, l.e5) == std::tie(r.e0, r.e1, r.e2, r.e3, r.e4, r.e5);
    } inline bool operator !=(const ::armarx::Matrix_6_1_d& l, const ::armarx::Matrix_6_1_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4, l.e5) != std::tie(r.e0, r.e1, r.e2, r.e3, r.e4, r.e5);
    } inline bool operator >=(const ::armarx::Matrix_6_1_d& l, const ::armarx::Matrix_6_1_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4, l.e5) >= std::tie(r.e0, r.e1, r.e2, r.e3, r.e4, r.e5);
    } inline bool operator <=(const ::armarx::Matrix_6_1_d& l, const ::armarx::Matrix_6_1_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4, l.e5) <= std::tie(r.e0, r.e1, r.e2, r.e3, r.e4, r.e5);
    }
    inline bool operator <(const ::armarx::Matrix_1_2_d& l, const ::armarx::Matrix_1_2_d& r)
    {
        return std::tie(l.e0) < std::tie(r.e0);
    } inline bool operator >(const ::armarx::Matrix_1_2_d& l, const ::armarx::Matrix_1_2_d& r)
    {
        return std::tie(l.e0) > std::tie(r.e0);
    } inline bool operator ==(const ::armarx::Matrix_1_2_d& l, const ::armarx::Matrix_1_2_d& r)
    {
        return std::tie(l.e0) == std::tie(r.e0);
    } inline bool operator !=(const ::armarx::Matrix_1_2_d& l, const ::armarx::Matrix_1_2_d& r)
    {
        return std::tie(l.e0) != std::tie(r.e0);
    } inline bool operator >=(const ::armarx::Matrix_1_2_d& l, const ::armarx::Matrix_1_2_d& r)
    {
        return std::tie(l.e0) >= std::tie(r.e0);
    } inline bool operator <=(const ::armarx::Matrix_1_2_d& l, const ::armarx::Matrix_1_2_d& r)
    {
        return std::tie(l.e0) <= std::tie(r.e0);
    } inline bool operator <(const ::armarx::Matrix_2_2_d& l, const ::armarx::Matrix_2_2_d& r)
    {
        return std::tie(l.e0, l.e1) < std::tie(r.e0, r.e1);
    } inline bool operator >(const ::armarx::Matrix_2_2_d& l, const ::armarx::Matrix_2_2_d& r)
    {
        return std::tie(l.e0, l.e1) > std::tie(r.e0, r.e1);
    } inline bool operator ==(const ::armarx::Matrix_2_2_d& l, const ::armarx::Matrix_2_2_d& r)
    {
        return std::tie(l.e0, l.e1) == std::tie(r.e0, r.e1);
    } inline bool operator !=(const ::armarx::Matrix_2_2_d& l, const ::armarx::Matrix_2_2_d& r)
    {
        return std::tie(l.e0, l.e1) != std::tie(r.e0, r.e1);
    } inline bool operator >=(const ::armarx::Matrix_2_2_d& l, const ::armarx::Matrix_2_2_d& r)
    {
        return std::tie(l.e0, l.e1) >= std::tie(r.e0, r.e1);
    } inline bool operator <=(const ::armarx::Matrix_2_2_d& l, const ::armarx::Matrix_2_2_d& r)
    {
        return std::tie(l.e0, l.e1) <= std::tie(r.e0, r.e1);
    } inline bool operator <(const ::armarx::Matrix_3_2_d& l, const ::armarx::Matrix_3_2_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2) < std::tie(r.e0, r.e1, r.e2);
    } inline bool operator >(const ::armarx::Matrix_3_2_d& l, const ::armarx::Matrix_3_2_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2) > std::tie(r.e0, r.e1, r.e2);
    } inline bool operator ==(const ::armarx::Matrix_3_2_d& l, const ::armarx::Matrix_3_2_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2) == std::tie(r.e0, r.e1, r.e2);
    } inline bool operator !=(const ::armarx::Matrix_3_2_d& l, const ::armarx::Matrix_3_2_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2) != std::tie(r.e0, r.e1, r.e2);
    } inline bool operator >=(const ::armarx::Matrix_3_2_d& l, const ::armarx::Matrix_3_2_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2) >= std::tie(r.e0, r.e1, r.e2);
    } inline bool operator <=(const ::armarx::Matrix_3_2_d& l, const ::armarx::Matrix_3_2_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2) <= std::tie(r.e0, r.e1, r.e2);
    } inline bool operator <(const ::armarx::Matrix_4_2_d& l, const ::armarx::Matrix_4_2_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3) < std::tie(r.e0, r.e1, r.e2, r.e3);
    } inline bool operator >(const ::armarx::Matrix_4_2_d& l, const ::armarx::Matrix_4_2_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3) > std::tie(r.e0, r.e1, r.e2, r.e3);
    } inline bool operator ==(const ::armarx::Matrix_4_2_d& l, const ::armarx::Matrix_4_2_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3) == std::tie(r.e0, r.e1, r.e2, r.e3);
    } inline bool operator !=(const ::armarx::Matrix_4_2_d& l, const ::armarx::Matrix_4_2_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3) != std::tie(r.e0, r.e1, r.e2, r.e3);
    } inline bool operator >=(const ::armarx::Matrix_4_2_d& l, const ::armarx::Matrix_4_2_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3) >= std::tie(r.e0, r.e1, r.e2, r.e3);
    } inline bool operator <=(const ::armarx::Matrix_4_2_d& l, const ::armarx::Matrix_4_2_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3) <= std::tie(r.e0, r.e1, r.e2, r.e3);
    } inline bool operator <(const ::armarx::Matrix_5_2_d& l, const ::armarx::Matrix_5_2_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4) < std::tie(r.e0, r.e1, r.e2, r.e3, r.e4);
    } inline bool operator >(const ::armarx::Matrix_5_2_d& l, const ::armarx::Matrix_5_2_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4) > std::tie(r.e0, r.e1, r.e2, r.e3, r.e4);
    } inline bool operator ==(const ::armarx::Matrix_5_2_d& l, const ::armarx::Matrix_5_2_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4) == std::tie(r.e0, r.e1, r.e2, r.e3, r.e4);
    } inline bool operator !=(const ::armarx::Matrix_5_2_d& l, const ::armarx::Matrix_5_2_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4) != std::tie(r.e0, r.e1, r.e2, r.e3, r.e4);
    } inline bool operator >=(const ::armarx::Matrix_5_2_d& l, const ::armarx::Matrix_5_2_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4) >= std::tie(r.e0, r.e1, r.e2, r.e3, r.e4);
    } inline bool operator <=(const ::armarx::Matrix_5_2_d& l, const ::armarx::Matrix_5_2_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4) <= std::tie(r.e0, r.e1, r.e2, r.e3, r.e4);
    } inline bool operator <(const ::armarx::Matrix_6_2_d& l, const ::armarx::Matrix_6_2_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4, l.e5) < std::tie(r.e0, r.e1, r.e2, r.e3, r.e4, r.e5);
    } inline bool operator >(const ::armarx::Matrix_6_2_d& l, const ::armarx::Matrix_6_2_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4, l.e5) > std::tie(r.e0, r.e1, r.e2, r.e3, r.e4, r.e5);
    } inline bool operator ==(const ::armarx::Matrix_6_2_d& l, const ::armarx::Matrix_6_2_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4, l.e5) == std::tie(r.e0, r.e1, r.e2, r.e3, r.e4, r.e5);
    } inline bool operator !=(const ::armarx::Matrix_6_2_d& l, const ::armarx::Matrix_6_2_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4, l.e5) != std::tie(r.e0, r.e1, r.e2, r.e3, r.e4, r.e5);
    } inline bool operator >=(const ::armarx::Matrix_6_2_d& l, const ::armarx::Matrix_6_2_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4, l.e5) >= std::tie(r.e0, r.e1, r.e2, r.e3, r.e4, r.e5);
    } inline bool operator <=(const ::armarx::Matrix_6_2_d& l, const ::armarx::Matrix_6_2_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4, l.e5) <= std::tie(r.e0, r.e1, r.e2, r.e3, r.e4, r.e5);
    }
    inline bool operator <(const ::armarx::Matrix_1_3_d& l, const ::armarx::Matrix_1_3_d& r)
    {
        return std::tie(l.e0) < std::tie(r.e0);
    } inline bool operator >(const ::armarx::Matrix_1_3_d& l, const ::armarx::Matrix_1_3_d& r)
    {
        return std::tie(l.e0) > std::tie(r.e0);
    } inline bool operator ==(const ::armarx::Matrix_1_3_d& l, const ::armarx::Matrix_1_3_d& r)
    {
        return std::tie(l.e0) == std::tie(r.e0);
    } inline bool operator !=(const ::armarx::Matrix_1_3_d& l, const ::armarx::Matrix_1_3_d& r)
    {
        return std::tie(l.e0) != std::tie(r.e0);
    } inline bool operator >=(const ::armarx::Matrix_1_3_d& l, const ::armarx::Matrix_1_3_d& r)
    {
        return std::tie(l.e0) >= std::tie(r.e0);
    } inline bool operator <=(const ::armarx::Matrix_1_3_d& l, const ::armarx::Matrix_1_3_d& r)
    {
        return std::tie(l.e0) <= std::tie(r.e0);
    } inline bool operator <(const ::armarx::Matrix_2_3_d& l, const ::armarx::Matrix_2_3_d& r)
    {
        return std::tie(l.e0, l.e1) < std::tie(r.e0, r.e1);
    } inline bool operator >(const ::armarx::Matrix_2_3_d& l, const ::armarx::Matrix_2_3_d& r)
    {
        return std::tie(l.e0, l.e1) > std::tie(r.e0, r.e1);
    } inline bool operator ==(const ::armarx::Matrix_2_3_d& l, const ::armarx::Matrix_2_3_d& r)
    {
        return std::tie(l.e0, l.e1) == std::tie(r.e0, r.e1);
    } inline bool operator !=(const ::armarx::Matrix_2_3_d& l, const ::armarx::Matrix_2_3_d& r)
    {
        return std::tie(l.e0, l.e1) != std::tie(r.e0, r.e1);
    } inline bool operator >=(const ::armarx::Matrix_2_3_d& l, const ::armarx::Matrix_2_3_d& r)
    {
        return std::tie(l.e0, l.e1) >= std::tie(r.e0, r.e1);
    } inline bool operator <=(const ::armarx::Matrix_2_3_d& l, const ::armarx::Matrix_2_3_d& r)
    {
        return std::tie(l.e0, l.e1) <= std::tie(r.e0, r.e1);
    } inline bool operator <(const ::armarx::Matrix_3_3_d& l, const ::armarx::Matrix_3_3_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2) < std::tie(r.e0, r.e1, r.e2);
    } inline bool operator >(const ::armarx::Matrix_3_3_d& l, const ::armarx::Matrix_3_3_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2) > std::tie(r.e0, r.e1, r.e2);
    } inline bool operator ==(const ::armarx::Matrix_3_3_d& l, const ::armarx::Matrix_3_3_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2) == std::tie(r.e0, r.e1, r.e2);
    } inline bool operator !=(const ::armarx::Matrix_3_3_d& l, const ::armarx::Matrix_3_3_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2) != std::tie(r.e0, r.e1, r.e2);
    } inline bool operator >=(const ::armarx::Matrix_3_3_d& l, const ::armarx::Matrix_3_3_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2) >= std::tie(r.e0, r.e1, r.e2);
    } inline bool operator <=(const ::armarx::Matrix_3_3_d& l, const ::armarx::Matrix_3_3_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2) <= std::tie(r.e0, r.e1, r.e2);
    } inline bool operator <(const ::armarx::Matrix_4_3_d& l, const ::armarx::Matrix_4_3_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3) < std::tie(r.e0, r.e1, r.e2, r.e3);
    } inline bool operator >(const ::armarx::Matrix_4_3_d& l, const ::armarx::Matrix_4_3_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3) > std::tie(r.e0, r.e1, r.e2, r.e3);
    } inline bool operator ==(const ::armarx::Matrix_4_3_d& l, const ::armarx::Matrix_4_3_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3) == std::tie(r.e0, r.e1, r.e2, r.e3);
    } inline bool operator !=(const ::armarx::Matrix_4_3_d& l, const ::armarx::Matrix_4_3_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3) != std::tie(r.e0, r.e1, r.e2, r.e3);
    } inline bool operator >=(const ::armarx::Matrix_4_3_d& l, const ::armarx::Matrix_4_3_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3) >= std::tie(r.e0, r.e1, r.e2, r.e3);
    } inline bool operator <=(const ::armarx::Matrix_4_3_d& l, const ::armarx::Matrix_4_3_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3) <= std::tie(r.e0, r.e1, r.e2, r.e3);
    } inline bool operator <(const ::armarx::Matrix_5_3_d& l, const ::armarx::Matrix_5_3_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4) < std::tie(r.e0, r.e1, r.e2, r.e3, r.e4);
    } inline bool operator >(const ::armarx::Matrix_5_3_d& l, const ::armarx::Matrix_5_3_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4) > std::tie(r.e0, r.e1, r.e2, r.e3, r.e4);
    } inline bool operator ==(const ::armarx::Matrix_5_3_d& l, const ::armarx::Matrix_5_3_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4) == std::tie(r.e0, r.e1, r.e2, r.e3, r.e4);
    } inline bool operator !=(const ::armarx::Matrix_5_3_d& l, const ::armarx::Matrix_5_3_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4) != std::tie(r.e0, r.e1, r.e2, r.e3, r.e4);
    } inline bool operator >=(const ::armarx::Matrix_5_3_d& l, const ::armarx::Matrix_5_3_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4) >= std::tie(r.e0, r.e1, r.e2, r.e3, r.e4);
    } inline bool operator <=(const ::armarx::Matrix_5_3_d& l, const ::armarx::Matrix_5_3_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4) <= std::tie(r.e0, r.e1, r.e2, r.e3, r.e4);
    } inline bool operator <(const ::armarx::Matrix_6_3_d& l, const ::armarx::Matrix_6_3_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4, l.e5) < std::tie(r.e0, r.e1, r.e2, r.e3, r.e4, r.e5);
    } inline bool operator >(const ::armarx::Matrix_6_3_d& l, const ::armarx::Matrix_6_3_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4, l.e5) > std::tie(r.e0, r.e1, r.e2, r.e3, r.e4, r.e5);
    } inline bool operator ==(const ::armarx::Matrix_6_3_d& l, const ::armarx::Matrix_6_3_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4, l.e5) == std::tie(r.e0, r.e1, r.e2, r.e3, r.e4, r.e5);
    } inline bool operator !=(const ::armarx::Matrix_6_3_d& l, const ::armarx::Matrix_6_3_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4, l.e5) != std::tie(r.e0, r.e1, r.e2, r.e3, r.e4, r.e5);
    } inline bool operator >=(const ::armarx::Matrix_6_3_d& l, const ::armarx::Matrix_6_3_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4, l.e5) >= std::tie(r.e0, r.e1, r.e2, r.e3, r.e4, r.e5);
    } inline bool operator <=(const ::armarx::Matrix_6_3_d& l, const ::armarx::Matrix_6_3_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4, l.e5) <= std::tie(r.e0, r.e1, r.e2, r.e3, r.e4, r.e5);
    }
    inline bool operator <(const ::armarx::Matrix_1_4_d& l, const ::armarx::Matrix_1_4_d& r)
    {
        return std::tie(l.e0) < std::tie(r.e0);
    } inline bool operator >(const ::armarx::Matrix_1_4_d& l, const ::armarx::Matrix_1_4_d& r)
    {
        return std::tie(l.e0) > std::tie(r.e0);
    } inline bool operator ==(const ::armarx::Matrix_1_4_d& l, const ::armarx::Matrix_1_4_d& r)
    {
        return std::tie(l.e0) == std::tie(r.e0);
    } inline bool operator !=(const ::armarx::Matrix_1_4_d& l, const ::armarx::Matrix_1_4_d& r)
    {
        return std::tie(l.e0) != std::tie(r.e0);
    } inline bool operator >=(const ::armarx::Matrix_1_4_d& l, const ::armarx::Matrix_1_4_d& r)
    {
        return std::tie(l.e0) >= std::tie(r.e0);
    } inline bool operator <=(const ::armarx::Matrix_1_4_d& l, const ::armarx::Matrix_1_4_d& r)
    {
        return std::tie(l.e0) <= std::tie(r.e0);
    } inline bool operator <(const ::armarx::Matrix_2_4_d& l, const ::armarx::Matrix_2_4_d& r)
    {
        return std::tie(l.e0, l.e1) < std::tie(r.e0, r.e1);
    } inline bool operator >(const ::armarx::Matrix_2_4_d& l, const ::armarx::Matrix_2_4_d& r)
    {
        return std::tie(l.e0, l.e1) > std::tie(r.e0, r.e1);
    } inline bool operator ==(const ::armarx::Matrix_2_4_d& l, const ::armarx::Matrix_2_4_d& r)
    {
        return std::tie(l.e0, l.e1) == std::tie(r.e0, r.e1);
    } inline bool operator !=(const ::armarx::Matrix_2_4_d& l, const ::armarx::Matrix_2_4_d& r)
    {
        return std::tie(l.e0, l.e1) != std::tie(r.e0, r.e1);
    } inline bool operator >=(const ::armarx::Matrix_2_4_d& l, const ::armarx::Matrix_2_4_d& r)
    {
        return std::tie(l.e0, l.e1) >= std::tie(r.e0, r.e1);
    } inline bool operator <=(const ::armarx::Matrix_2_4_d& l, const ::armarx::Matrix_2_4_d& r)
    {
        return std::tie(l.e0, l.e1) <= std::tie(r.e0, r.e1);
    } inline bool operator <(const ::armarx::Matrix_3_4_d& l, const ::armarx::Matrix_3_4_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2) < std::tie(r.e0, r.e1, r.e2);
    } inline bool operator >(const ::armarx::Matrix_3_4_d& l, const ::armarx::Matrix_3_4_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2) > std::tie(r.e0, r.e1, r.e2);
    } inline bool operator ==(const ::armarx::Matrix_3_4_d& l, const ::armarx::Matrix_3_4_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2) == std::tie(r.e0, r.e1, r.e2);
    } inline bool operator !=(const ::armarx::Matrix_3_4_d& l, const ::armarx::Matrix_3_4_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2) != std::tie(r.e0, r.e1, r.e2);
    } inline bool operator >=(const ::armarx::Matrix_3_4_d& l, const ::armarx::Matrix_3_4_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2) >= std::tie(r.e0, r.e1, r.e2);
    } inline bool operator <=(const ::armarx::Matrix_3_4_d& l, const ::armarx::Matrix_3_4_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2) <= std::tie(r.e0, r.e1, r.e2);
    } inline bool operator <(const ::armarx::Matrix_4_4_d& l, const ::armarx::Matrix_4_4_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3) < std::tie(r.e0, r.e1, r.e2, r.e3);
    } inline bool operator >(const ::armarx::Matrix_4_4_d& l, const ::armarx::Matrix_4_4_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3) > std::tie(r.e0, r.e1, r.e2, r.e3);
    } inline bool operator ==(const ::armarx::Matrix_4_4_d& l, const ::armarx::Matrix_4_4_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3) == std::tie(r.e0, r.e1, r.e2, r.e3);
    } inline bool operator !=(const ::armarx::Matrix_4_4_d& l, const ::armarx::Matrix_4_4_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3) != std::tie(r.e0, r.e1, r.e2, r.e3);
    } inline bool operator >=(const ::armarx::Matrix_4_4_d& l, const ::armarx::Matrix_4_4_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3) >= std::tie(r.e0, r.e1, r.e2, r.e3);
    } inline bool operator <=(const ::armarx::Matrix_4_4_d& l, const ::armarx::Matrix_4_4_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3) <= std::tie(r.e0, r.e1, r.e2, r.e3);
    } inline bool operator <(const ::armarx::Matrix_5_4_d& l, const ::armarx::Matrix_5_4_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4) < std::tie(r.e0, r.e1, r.e2, r.e3, r.e4);
    } inline bool operator >(const ::armarx::Matrix_5_4_d& l, const ::armarx::Matrix_5_4_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4) > std::tie(r.e0, r.e1, r.e2, r.e3, r.e4);
    } inline bool operator ==(const ::armarx::Matrix_5_4_d& l, const ::armarx::Matrix_5_4_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4) == std::tie(r.e0, r.e1, r.e2, r.e3, r.e4);
    } inline bool operator !=(const ::armarx::Matrix_5_4_d& l, const ::armarx::Matrix_5_4_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4) != std::tie(r.e0, r.e1, r.e2, r.e3, r.e4);
    } inline bool operator >=(const ::armarx::Matrix_5_4_d& l, const ::armarx::Matrix_5_4_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4) >= std::tie(r.e0, r.e1, r.e2, r.e3, r.e4);
    } inline bool operator <=(const ::armarx::Matrix_5_4_d& l, const ::armarx::Matrix_5_4_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4) <= std::tie(r.e0, r.e1, r.e2, r.e3, r.e4);
    } inline bool operator <(const ::armarx::Matrix_6_4_d& l, const ::armarx::Matrix_6_4_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4, l.e5) < std::tie(r.e0, r.e1, r.e2, r.e3, r.e4, r.e5);
    } inline bool operator >(const ::armarx::Matrix_6_4_d& l, const ::armarx::Matrix_6_4_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4, l.e5) > std::tie(r.e0, r.e1, r.e2, r.e3, r.e4, r.e5);
    } inline bool operator ==(const ::armarx::Matrix_6_4_d& l, const ::armarx::Matrix_6_4_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4, l.e5) == std::tie(r.e0, r.e1, r.e2, r.e3, r.e4, r.e5);
    } inline bool operator !=(const ::armarx::Matrix_6_4_d& l, const ::armarx::Matrix_6_4_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4, l.e5) != std::tie(r.e0, r.e1, r.e2, r.e3, r.e4, r.e5);
    } inline bool operator >=(const ::armarx::Matrix_6_4_d& l, const ::armarx::Matrix_6_4_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4, l.e5) >= std::tie(r.e0, r.e1, r.e2, r.e3, r.e4, r.e5);
    } inline bool operator <=(const ::armarx::Matrix_6_4_d& l, const ::armarx::Matrix_6_4_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4, l.e5) <= std::tie(r.e0, r.e1, r.e2, r.e3, r.e4, r.e5);
    }
    inline bool operator <(const ::armarx::Matrix_1_5_d& l, const ::armarx::Matrix_1_5_d& r)
    {
        return std::tie(l.e0) < std::tie(r.e0);
    } inline bool operator >(const ::armarx::Matrix_1_5_d& l, const ::armarx::Matrix_1_5_d& r)
    {
        return std::tie(l.e0) > std::tie(r.e0);
    } inline bool operator ==(const ::armarx::Matrix_1_5_d& l, const ::armarx::Matrix_1_5_d& r)
    {
        return std::tie(l.e0) == std::tie(r.e0);
    } inline bool operator !=(const ::armarx::Matrix_1_5_d& l, const ::armarx::Matrix_1_5_d& r)
    {
        return std::tie(l.e0) != std::tie(r.e0);
    } inline bool operator >=(const ::armarx::Matrix_1_5_d& l, const ::armarx::Matrix_1_5_d& r)
    {
        return std::tie(l.e0) >= std::tie(r.e0);
    } inline bool operator <=(const ::armarx::Matrix_1_5_d& l, const ::armarx::Matrix_1_5_d& r)
    {
        return std::tie(l.e0) <= std::tie(r.e0);
    } inline bool operator <(const ::armarx::Matrix_2_5_d& l, const ::armarx::Matrix_2_5_d& r)
    {
        return std::tie(l.e0, l.e1) < std::tie(r.e0, r.e1);
    } inline bool operator >(const ::armarx::Matrix_2_5_d& l, const ::armarx::Matrix_2_5_d& r)
    {
        return std::tie(l.e0, l.e1) > std::tie(r.e0, r.e1);
    } inline bool operator ==(const ::armarx::Matrix_2_5_d& l, const ::armarx::Matrix_2_5_d& r)
    {
        return std::tie(l.e0, l.e1) == std::tie(r.e0, r.e1);
    } inline bool operator !=(const ::armarx::Matrix_2_5_d& l, const ::armarx::Matrix_2_5_d& r)
    {
        return std::tie(l.e0, l.e1) != std::tie(r.e0, r.e1);
    } inline bool operator >=(const ::armarx::Matrix_2_5_d& l, const ::armarx::Matrix_2_5_d& r)
    {
        return std::tie(l.e0, l.e1) >= std::tie(r.e0, r.e1);
    } inline bool operator <=(const ::armarx::Matrix_2_5_d& l, const ::armarx::Matrix_2_5_d& r)
    {
        return std::tie(l.e0, l.e1) <= std::tie(r.e0, r.e1);
    } inline bool operator <(const ::armarx::Matrix_3_5_d& l, const ::armarx::Matrix_3_5_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2) < std::tie(r.e0, r.e1, r.e2);
    } inline bool operator >(const ::armarx::Matrix_3_5_d& l, const ::armarx::Matrix_3_5_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2) > std::tie(r.e0, r.e1, r.e2);
    } inline bool operator ==(const ::armarx::Matrix_3_5_d& l, const ::armarx::Matrix_3_5_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2) == std::tie(r.e0, r.e1, r.e2);
    } inline bool operator !=(const ::armarx::Matrix_3_5_d& l, const ::armarx::Matrix_3_5_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2) != std::tie(r.e0, r.e1, r.e2);
    } inline bool operator >=(const ::armarx::Matrix_3_5_d& l, const ::armarx::Matrix_3_5_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2) >= std::tie(r.e0, r.e1, r.e2);
    } inline bool operator <=(const ::armarx::Matrix_3_5_d& l, const ::armarx::Matrix_3_5_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2) <= std::tie(r.e0, r.e1, r.e2);
    } inline bool operator <(const ::armarx::Matrix_4_5_d& l, const ::armarx::Matrix_4_5_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3) < std::tie(r.e0, r.e1, r.e2, r.e3);
    } inline bool operator >(const ::armarx::Matrix_4_5_d& l, const ::armarx::Matrix_4_5_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3) > std::tie(r.e0, r.e1, r.e2, r.e3);
    } inline bool operator ==(const ::armarx::Matrix_4_5_d& l, const ::armarx::Matrix_4_5_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3) == std::tie(r.e0, r.e1, r.e2, r.e3);
    } inline bool operator !=(const ::armarx::Matrix_4_5_d& l, const ::armarx::Matrix_4_5_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3) != std::tie(r.e0, r.e1, r.e2, r.e3);
    } inline bool operator >=(const ::armarx::Matrix_4_5_d& l, const ::armarx::Matrix_4_5_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3) >= std::tie(r.e0, r.e1, r.e2, r.e3);
    } inline bool operator <=(const ::armarx::Matrix_4_5_d& l, const ::armarx::Matrix_4_5_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3) <= std::tie(r.e0, r.e1, r.e2, r.e3);
    } inline bool operator <(const ::armarx::Matrix_5_5_d& l, const ::armarx::Matrix_5_5_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4) < std::tie(r.e0, r.e1, r.e2, r.e3, r.e4);
    } inline bool operator >(const ::armarx::Matrix_5_5_d& l, const ::armarx::Matrix_5_5_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4) > std::tie(r.e0, r.e1, r.e2, r.e3, r.e4);
    } inline bool operator ==(const ::armarx::Matrix_5_5_d& l, const ::armarx::Matrix_5_5_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4) == std::tie(r.e0, r.e1, r.e2, r.e3, r.e4);
    } inline bool operator !=(const ::armarx::Matrix_5_5_d& l, const ::armarx::Matrix_5_5_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4) != std::tie(r.e0, r.e1, r.e2, r.e3, r.e4);
    } inline bool operator >=(const ::armarx::Matrix_5_5_d& l, const ::armarx::Matrix_5_5_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4) >= std::tie(r.e0, r.e1, r.e2, r.e3, r.e4);
    } inline bool operator <=(const ::armarx::Matrix_5_5_d& l, const ::armarx::Matrix_5_5_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4) <= std::tie(r.e0, r.e1, r.e2, r.e3, r.e4);
    } inline bool operator <(const ::armarx::Matrix_6_5_d& l, const ::armarx::Matrix_6_5_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4, l.e5) < std::tie(r.e0, r.e1, r.e2, r.e3, r.e4, r.e5);
    } inline bool operator >(const ::armarx::Matrix_6_5_d& l, const ::armarx::Matrix_6_5_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4, l.e5) > std::tie(r.e0, r.e1, r.e2, r.e3, r.e4, r.e5);
    } inline bool operator ==(const ::armarx::Matrix_6_5_d& l, const ::armarx::Matrix_6_5_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4, l.e5) == std::tie(r.e0, r.e1, r.e2, r.e3, r.e4, r.e5);
    } inline bool operator !=(const ::armarx::Matrix_6_5_d& l, const ::armarx::Matrix_6_5_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4, l.e5) != std::tie(r.e0, r.e1, r.e2, r.e3, r.e4, r.e5);
    } inline bool operator >=(const ::armarx::Matrix_6_5_d& l, const ::armarx::Matrix_6_5_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4, l.e5) >= std::tie(r.e0, r.e1, r.e2, r.e3, r.e4, r.e5);
    } inline bool operator <=(const ::armarx::Matrix_6_5_d& l, const ::armarx::Matrix_6_5_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4, l.e5) <= std::tie(r.e0, r.e1, r.e2, r.e3, r.e4, r.e5);
    }
    inline bool operator <(const ::armarx::Matrix_1_6_d& l, const ::armarx::Matrix_1_6_d& r)
    {
        return std::tie(l.e0) < std::tie(r.e0);
    } inline bool operator >(const ::armarx::Matrix_1_6_d& l, const ::armarx::Matrix_1_6_d& r)
    {
        return std::tie(l.e0) > std::tie(r.e0);
    } inline bool operator ==(const ::armarx::Matrix_1_6_d& l, const ::armarx::Matrix_1_6_d& r)
    {
        return std::tie(l.e0) == std::tie(r.e0);
    } inline bool operator !=(const ::armarx::Matrix_1_6_d& l, const ::armarx::Matrix_1_6_d& r)
    {
        return std::tie(l.e0) != std::tie(r.e0);
    } inline bool operator >=(const ::armarx::Matrix_1_6_d& l, const ::armarx::Matrix_1_6_d& r)
    {
        return std::tie(l.e0) >= std::tie(r.e0);
    } inline bool operator <=(const ::armarx::Matrix_1_6_d& l, const ::armarx::Matrix_1_6_d& r)
    {
        return std::tie(l.e0) <= std::tie(r.e0);
    } inline bool operator <(const ::armarx::Matrix_2_6_d& l, const ::armarx::Matrix_2_6_d& r)
    {
        return std::tie(l.e0, l.e1) < std::tie(r.e0, r.e1);
    } inline bool operator >(const ::armarx::Matrix_2_6_d& l, const ::armarx::Matrix_2_6_d& r)
    {
        return std::tie(l.e0, l.e1) > std::tie(r.e0, r.e1);
    } inline bool operator ==(const ::armarx::Matrix_2_6_d& l, const ::armarx::Matrix_2_6_d& r)
    {
        return std::tie(l.e0, l.e1) == std::tie(r.e0, r.e1);
    } inline bool operator !=(const ::armarx::Matrix_2_6_d& l, const ::armarx::Matrix_2_6_d& r)
    {
        return std::tie(l.e0, l.e1) != std::tie(r.e0, r.e1);
    } inline bool operator >=(const ::armarx::Matrix_2_6_d& l, const ::armarx::Matrix_2_6_d& r)
    {
        return std::tie(l.e0, l.e1) >= std::tie(r.e0, r.e1);
    } inline bool operator <=(const ::armarx::Matrix_2_6_d& l, const ::armarx::Matrix_2_6_d& r)
    {
        return std::tie(l.e0, l.e1) <= std::tie(r.e0, r.e1);
    } inline bool operator <(const ::armarx::Matrix_3_6_d& l, const ::armarx::Matrix_3_6_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2) < std::tie(r.e0, r.e1, r.e2);
    } inline bool operator >(const ::armarx::Matrix_3_6_d& l, const ::armarx::Matrix_3_6_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2) > std::tie(r.e0, r.e1, r.e2);
    } inline bool operator ==(const ::armarx::Matrix_3_6_d& l, const ::armarx::Matrix_3_6_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2) == std::tie(r.e0, r.e1, r.e2);
    } inline bool operator !=(const ::armarx::Matrix_3_6_d& l, const ::armarx::Matrix_3_6_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2) != std::tie(r.e0, r.e1, r.e2);
    } inline bool operator >=(const ::armarx::Matrix_3_6_d& l, const ::armarx::Matrix_3_6_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2) >= std::tie(r.e0, r.e1, r.e2);
    } inline bool operator <=(const ::armarx::Matrix_3_6_d& l, const ::armarx::Matrix_3_6_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2) <= std::tie(r.e0, r.e1, r.e2);
    } inline bool operator <(const ::armarx::Matrix_4_6_d& l, const ::armarx::Matrix_4_6_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3) < std::tie(r.e0, r.e1, r.e2, r.e3);
    } inline bool operator >(const ::armarx::Matrix_4_6_d& l, const ::armarx::Matrix_4_6_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3) > std::tie(r.e0, r.e1, r.e2, r.e3);
    } inline bool operator ==(const ::armarx::Matrix_4_6_d& l, const ::armarx::Matrix_4_6_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3) == std::tie(r.e0, r.e1, r.e2, r.e3);
    } inline bool operator !=(const ::armarx::Matrix_4_6_d& l, const ::armarx::Matrix_4_6_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3) != std::tie(r.e0, r.e1, r.e2, r.e3);
    } inline bool operator >=(const ::armarx::Matrix_4_6_d& l, const ::armarx::Matrix_4_6_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3) >= std::tie(r.e0, r.e1, r.e2, r.e3);
    } inline bool operator <=(const ::armarx::Matrix_4_6_d& l, const ::armarx::Matrix_4_6_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3) <= std::tie(r.e0, r.e1, r.e2, r.e3);
    } inline bool operator <(const ::armarx::Matrix_5_6_d& l, const ::armarx::Matrix_5_6_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4) < std::tie(r.e0, r.e1, r.e2, r.e3, r.e4);
    } inline bool operator >(const ::armarx::Matrix_5_6_d& l, const ::armarx::Matrix_5_6_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4) > std::tie(r.e0, r.e1, r.e2, r.e3, r.e4);
    } inline bool operator ==(const ::armarx::Matrix_5_6_d& l, const ::armarx::Matrix_5_6_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4) == std::tie(r.e0, r.e1, r.e2, r.e3, r.e4);
    } inline bool operator !=(const ::armarx::Matrix_5_6_d& l, const ::armarx::Matrix_5_6_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4) != std::tie(r.e0, r.e1, r.e2, r.e3, r.e4);
    } inline bool operator >=(const ::armarx::Matrix_5_6_d& l, const ::armarx::Matrix_5_6_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4) >= std::tie(r.e0, r.e1, r.e2, r.e3, r.e4);
    } inline bool operator <=(const ::armarx::Matrix_5_6_d& l, const ::armarx::Matrix_5_6_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4) <= std::tie(r.e0, r.e1, r.e2, r.e3, r.e4);
    } inline bool operator <(const ::armarx::Matrix_6_6_d& l, const ::armarx::Matrix_6_6_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4, l.e5) < std::tie(r.e0, r.e1, r.e2, r.e3, r.e4, r.e5);
    } inline bool operator >(const ::armarx::Matrix_6_6_d& l, const ::armarx::Matrix_6_6_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4, l.e5) > std::tie(r.e0, r.e1, r.e2, r.e3, r.e4, r.e5);
    } inline bool operator ==(const ::armarx::Matrix_6_6_d& l, const ::armarx::Matrix_6_6_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4, l.e5) == std::tie(r.e0, r.e1, r.e2, r.e3, r.e4, r.e5);
    } inline bool operator !=(const ::armarx::Matrix_6_6_d& l, const ::armarx::Matrix_6_6_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4, l.e5) != std::tie(r.e0, r.e1, r.e2, r.e3, r.e4, r.e5);
    } inline bool operator >=(const ::armarx::Matrix_6_6_d& l, const ::armarx::Matrix_6_6_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4, l.e5) >= std::tie(r.e0, r.e1, r.e2, r.e3, r.e4, r.e5);
    } inline bool operator <=(const ::armarx::Matrix_6_6_d& l, const ::armarx::Matrix_6_6_d& r)
    {
        return std::tie(l.e0, l.e1, l.e2, l.e3, l.e4, l.e5) <= std::tie(r.e0, r.e1, r.e2, r.e3, r.e4, r.e5);
    }

    template<class T, class R = decltype(ToEigen(std::declval<T>()))>
    inline std::vector<R> ToEigen(const std::vector<T>& v)
    {
        std::vector<R> r;
        r.reserve(v.size());
        for (const auto& e : v)
        {
            r.emplace_back(ToEigen(e));
        }
        return r;
    }

    template <
        class S,
        int Row,
        int Col,
        class R = decltype(ToBasicVectorType(std::declval<Eigen::Matrix<S, Row, Col>>()))
        >
    inline std::vector<R>  ToBasicVectorType(const std::vector<Eigen::Matrix<S, Row, Col>>& v)
    {
        std::vector<R> r;
        r.reserve(v.size());
        for (const auto& e : v)
        {
            r.emplace_back(ToBasicVectorType(e));
        }
        return r;
    }
}
