/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore
 * @author     Raphael Grimm ( ufdrv at student dot kit dot edu )
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */

#pragma once

module armarx
{
    struct ColorRGB24
    {
        byte r = 0;
        byte g = 0;
        byte b = 0;
    };
    struct ColorARGB32
    {
        byte r = 0;
        byte g = 0;
        byte b = 0;
        byte a = 0;
    };
    struct ColorRGBf
    {
        float r = 0;
        float g = 0;
        float b = 0;
    };
    struct ColorARGBf
    {
        float r = 0;
        float g = 0;
        float b = 0;
        float a = 0;
    };
};
