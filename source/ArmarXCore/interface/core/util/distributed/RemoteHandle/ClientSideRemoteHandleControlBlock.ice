/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore
 * @author     Raphael Grimm ( ufdrv at student dot kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */
#pragma once

#include <ArmarXCore/interface/core/util/distributed/RemoteHandle/RemoteHandleControlBlock.ice>

module armarx
{
    /**
     * @brief Used to notice the remote object when the reference count is in/decreased (per ctor/dtor)
     */
    class ClientSideRemoteHandleControlBlockBase
    {
        /**
         * @return The managed object proxy.
         */
        Object* getManagedObjectProxy();

        /**
         * @brief The remot object reference counter.
         */
        ["protected"] RemoteHandleControlBlockInterface* remoteHandleControlBlockProxy;
        /**
         * @brief The managed object proxy.
         */
        ["protected"] Object* managedObjectProxy;
    };
};
