/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Core
* @author     Kai Welke <welke@kit.edu>
* @copyright  2012 Humanoids Group, HIS, KIT
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <ArmarXCore/interface/core/ManagedIceObjectDependencyBase.ice>
#include <ArmarXCore/interface/core/BasicTypes.ice>

module armarx
{
    enum ManagedIceObjectState
    {
        eManagedIceObjectCreated,			// component has been constructed
        eManagedIceObjectInitializing,		// initialization started
        eManagedIceObjectInitializationFailed,		// initialization failed (usually exception)
        eManagedIceObjectInitialized,		// initialization done and onInitComponent call finished
        eManagedIceObjectStarting,		// dependencies resolved calling onStartComponent
        eManagedIceObjectStartingFailed,		// starting of component failed (usually exception)
        eManagedIceObjectStarted,		// call to onStartComponent done
        eManagedIceObjectExiting,		// component interrupted (interrupt())
        eManagedIceObjectExited
    };

    struct ManagedIceObjectConnectivity
    {
        DependencyMap dependencies;
        Ice::StringSeq usedTopics;
        Ice::StringSeq offeredTopics;
    };
};

