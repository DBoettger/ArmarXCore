/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Core
* @author     Manfred Kroehnert <manfred dot kroehnert at kit dot edu>
* @copyright  2015
* @license    http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <ArmarXCore/interface/statechart/StatechartIce.ice>

module armarx
{
    module Profiler
    {
        const string PROFILER_TOPIC_NAME = "ArmarXProfilerListenerTopic";
    };

    struct ProfilerEvent
    {
        int processId;
        long timestamp;
        string timestampUnit;
        string executableName;
        string eventName;
        string parentName;
        string functionName;
    };

    sequence<ProfilerEvent> ProfilerEventList;

    struct ProfilerStatechartTransition
    {
        int processId;
        long timestamp;
        string parentStateIdentifier;
        string sourceStateIdentifier;
        string targetStateIdentifier;
        string eventName;
        eStateType targetStateType;
    };

    sequence<ProfilerStatechartTransition> ProfilerStatechartTransitionList;

    struct ProfilerStatechartParameters
    {
        int processId;
        long timestamp;
        string stateIdentifier;
        StateParameterMap parameterMap;
    };

    sequence<ProfilerStatechartParameters> ProfilerStatechartParametersList;


    struct ProfilerProcessCpuUsage
    {
        int processId;
        long timestamp;
        string processName;
        float cpuUsage;
    };

    sequence<ProfilerProcessCpuUsage> ProfilerProcessCpuUsageList;

    struct ProfilerProcessMemoryUsage
    {
        int processId;
        long timestamp;
        string processName;
        int memoryUsage;
    };

    sequence<ProfilerProcessMemoryUsage> ProfilerProcessMemoryUsageList;

    interface ProfilerListener
    {
        void reportNetworkTraffic(string id, string protocol, int inBytes, int outBytes);
        void reportEvent(ProfilerEvent e);
        void reportStatechartTransition(ProfilerStatechartTransition transition);
        void reportStatechartInputParameters(ProfilerStatechartParameters inputParameters);
        void reportStatechartLocalParameters(ProfilerStatechartParameters localParameters);
        void reportStatechartOutputParameters(ProfilerStatechartParameters outputParameters);
        void reportProcessCpuUsage(ProfilerProcessCpuUsage process);
        void reportProcessMemoryUsage(ProfilerProcessMemoryUsage memoryUsage);

        void reportEventList(ProfilerEventList events);
        void reportStatechartTransitionList(ProfilerStatechartTransitionList transitions);
        void reportStatechartInputParametersList(ProfilerStatechartParametersList inputParametersList);
        void reportStatechartLocalParametersList(ProfilerStatechartParametersList localParametersList);
        void reportStatechartOutputParametersList(ProfilerStatechartParametersList outputParametersList);
        void reportProcessCpuUsageList(ProfilerProcessCpuUsageList processes);
        void reportProcessMemoryUsageList(ProfilerProcessMemoryUsageList memoryUsages);
    };
};

