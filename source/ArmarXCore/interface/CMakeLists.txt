###
### CMakeLists.txt file for ArmarX Interfaces
###

set(SLICE_FILES
core/ArmarXManagerInterface.ice
core/BasicTypes.ice
core/BasicVectorTypes.ice
core/BasicColorTypes.ice
core/Log.ice
core/ManagedIceObjectDefinitions.ice
core/ManagedIceObjectDependencyBase.ice
core/Profiler.ice
core/SharedMemory.ice
core/TextInterface.ice
core/ThreadingIceBase.ice
core/TimeServerInterface.ice
core/UserException.ice
core/RemoteObjectNode.ice
core/RemoteReferenceCount.ice

core/util/distributed/RemoteHandle/RemoteHandleControlBlock.ice
core/util/distributed/RemoteHandle/ClientSideRemoteHandleControlBlock.ice

observers/ChannelRefBase.ice
observers/Complex.ice
observers/ConditionCheckBase.ice
observers/ConditionHandlerInterface.ice
observers/DataFieldIdentifierBase.ice
observers/Event.ice
observers/Matrix.ice
observers/ObserverInterface.ice
observers/ParameterBase.ice
observers/Serialization.ice
observers/SystemObserverInterface.ice
observers/TermImplBase.ice
observers/Timestamp.ice
observers/VariantBase.ice
observers/VariantBaseTypes.ice
observers/VariantContainers.ice
observers/Filters.ice
observers/ExampleUnitInterface.ice
observers/ExampleUnitObserverInterface.ice
observers/ProfilerObserverInterface.ice

operations/Operation.ice
operations/RobotControlIceBase.ice

statechart/RemoteStateIce.ice
statechart/RemoteStateOffererIce.ice
statechart/StatechartIce.ice

serialization/JSONSerialization.ice
serialization/BoostFusionAdaptor.ice

components/ExternalApplicationManagerInterface.ice
components/TopicRecorderInterface.ice
components/EmergencyStopInterface.ice
)
set(SLICE_FILES_ADDITIONAL_HEADERS
    core/BasicVectorTypesHelpers.h
)

set(SLICE_FILES_ADDITIONAL_HEADERS
    serialization/BoostFusionAdaptor/Meta.h
    serialization/BoostFusionAdaptor/StreamReader.h
    serialization/BoostFusionAdaptor/StreamWriter.h
    serialization/BoostFusionAdaptor/StreamableTraits.h
    serialization/BoostFusionAdaptor/GenerateIceSerialization.h
)

find_package(Eigen3 QUIET)
if(Eigen3_FOUND)
    list(APPEND SLICE_FILES_ADDITIONAL_HEADERS
        serialization/Eigen/StreamReader.h
        serialization/Eigen/StreamWriter.h
        serialization/Eigen/StreamableTraits.h
    )
    list(APPEND SLICE_FILES
        serialization/Eigen.ice
    )
    include_directories(SYSTEM ${Eigen3_INCLUDE_DIR})
endif()

# generate the interface library
armarx_interfaces_generate_library(ArmarXCore)
