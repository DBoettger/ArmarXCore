/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2017, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarX
 * @author     Mirko Waechter( mirko.waechter at kit dot edu)
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <ArmarXCore/core/logging/Logging.h>
#include "ArmarXObjectScheduler.h"


namespace armarx
{

    template <class T>
    class RunningTask;

    class ArmarXMultipleObjectsScheduler :
        public IceUtil::Shared,
        virtual public Logging
    {
    public:
        ArmarXMultipleObjectsScheduler();
        ~ArmarXMultipleObjectsScheduler() override;

        bool addObjectScheduler(const ArmarXObjectSchedulerPtr& scheduler);
    private:
        void schedule();

        IceUtil::Handle<RunningTask<ArmarXMultipleObjectsScheduler> > scheduleObjectsTask;
        std::vector<ArmarXObjectSchedulerPtr> schedulers;
        Mutex dataMutex;
        Mutex interruptMutex;
        boost::shared_ptr<bool> interruptConditionVariable;
        boost::shared_ptr<boost::condition_variable> interruptCondition;
    };
    typedef IceUtil::Handle<ArmarXMultipleObjectsScheduler> ArmarXMultipleObjectsSchedulerPtr;

} // namespace armarx

