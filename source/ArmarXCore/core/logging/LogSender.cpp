/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarXCore::core
* @author     Nils Adermann (naderman at naderman dot de)
* @date       2010
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "LogSender.h"
#include <ArmarXCore/core/util/StringHelpers.h>

#include <execinfo.h>
#include <cxxabi.h>
#include <dlfcn.h>
#include <cstdarg>
//#include <Ice/Ice.h>

// boost
#include <boost/utility/enable_if.hpp>
#include <boost/type_traits/is_base_of.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/enable_shared_from_this.hpp>



#ifdef WIN32

#else
#ifdef __APPLE__
#include <pthread.h>
#endif

#include <sys/syscall.h>
#include <sys/types.h>
#endif

#include <stdio.h>

#include <boost/lexical_cast.hpp>
#include <boost/algorithm/string.hpp>

#include <IceUtil/Time.h>


using namespace armarx;

LogPrx LogSender::logProxy;
std::string LogSender::componentName;
std::vector<LogMessage> LogSender::buffer;
Mutex LogSender::mutex;
bool LogSender::LoggingActivated = true;
MessageType LogSender::GlobalMinimumLoggingLevel = eINFO;
bool LogSender::ColoringActivated = true;
bool LogSender::SendLogging = true;
std::ostream LogSender::outbuf(std::cout.rdbuf());
std::ostream LogSender::errbuf(std::cerr.rdbuf());







LogSenderPtr LogSender::createLogSender()
{
    return LogSenderPtr(new LogSender());
}



LogSender::LogSender() :
    minimumLoggingLevel(eUNDEFINED),
    cancelNextMessage(false),
    currentSeverity(eINFO),
    printBackTrace(boost::logic::indeterminate)
{
    initConsole();
    resetLocation();
}



LogSender::LogSender(const std::string& componentName, LogPrx logProxy) :
    minimumLoggingLevel(eUNDEFINED),
    cancelNextMessage(false),
    currentSeverity(eINFO),
    printBackTrace(true)
{
    initConsole();
    setProxy(componentName, logProxy);
    resetLocation();
}

LogSender::~LogSender()
{
    if (!currentMessage.str().empty())
    {
        flush();
    }
}


void LogSender::setProxy(const std::string& componentName, LogPrx logProxy)
{

    ScopedLock lock(mutex);
    LogSender::logProxy = logProxy;
    LogSender::componentName = componentName;

    if (!logProxy)
    {
        return;
    }

    for (unsigned int i = 0; i < buffer.size(); ++i)
    {
        LogSender::logProxy->writeLog(buffer[i]);
    }

    buffer.clear();
}

void LogSender::SetComponentName(const std::string& componentName)
{
    ScopedLock lock(mutex);
    LogSender::componentName = componentName;
}

LogSender::ConsoleColor LogSender::GetColorCode(MessageType type)
{
    switch (type)
    {
        case eVERBOSE:
            return eBlue;
            break;

        case eIMPORTANT:
            return eGreen;

        case eWARN:
            return eYellow;
            break;

        case eERROR:
        case eFATAL:
            return eRed;
            break;

        default:
            return eReset;
            break;
    }
}

std::string LogSender::GetColorCodeString(MessageType verbosityLevel)
{

    ConsoleColor colorCode = GetColorCode(verbosityLevel);
    return GetColorCodeString(colorCode);
}

std::string LogSender::GetColorCodeString(ConsoleColor colorCode)
{
    std::stringstream stream;

    if (colorCode == LogSender::eReset)
    {
        stream << "\033[0" << "m";
    }
    else
    {
        stream << "\033[3" << colorCode << "m";
    }

    return stream.str();
}

void LogSender::resetLocation()
{
    currentFile = "";
    currentLine = -1;
    currentFunction = "";
}

void LogSender::log(MessageType severity, std::string message)
{
    boost::trim(message);
    if (cancelNextMessage)
    {
        cancelNextMessage = false;
        return;
    }

    if (!LoggingActivated)
    {
        return;
    }

    if (severity < minimumLoggingLevel)
    {
        return;
    }

    if (minimumLoggingLevel == eUNDEFINED
        &&  severity < GlobalMinimumLoggingLevel
       )
    {
        return;
    }

    IceUtil::Time time = IceUtil::Time::now();

    std::string catStr;

    if (!currentTag.tagName.empty())
    {
        catStr = "[" + currentTag.tagName + "]: ";
    }
    else
    {
        catStr = "[" + CropFunctionName(currentFunction) + "]: ";
    }

    std::string outputStr;

    if (ColoringActivated)
    {
        outputStr += GetColorCodeString(severity);

        if (severity >= eFATAL)
        {
            outputStr += "\033[1m";    // Bold text
        }
    }

    outputStr += "[" + time.toDateTime().substr(time.toDateTime().find(' ') + 1) + "]" + "[" + componentName + "]" + catStr;

    if (ColoringActivated)
    {
        outputStr += GetColorCodeString(eReset);
    }

    outputStr += message;

    if (ColoringActivated)
    {
        outputStr += LogSender::GetColorCodeString(eReset);
    }

    LogMessage msg;
    msg.who  = componentName;
    msg.time = time.toMicroSeconds();
    msg.tag = currentTag.tagName;
    msg.type = severity;
    msg.what = message;
    msg.file = currentFile;
    msg.line = currentLine;
    msg.function = currentFunction;
    msg.threadId = threadId.is_initialized() ? threadId.get() : getThreadId();

    if ((severity >= eWARN && boost::logic::indeterminate(printBackTrace))
        || printBackTrace)
    {
        msg.backtrace = CreateBackTrace();
    }

    if (SendLogging && logProxy)
    {
        logProxy->begin_writeLog(msg);    // use async function, so that it returns faster
    }



    try
    {
        ScopedLock lock(mutex); // std::cout is not threadsafe and also lock for inserting into buffer

        if (severity >= eERROR)
        {
            errbuf << "[" << msg.threadId << "]" << outputStr << std::endl;
        }
        else
        {
            outbuf << "[" << msg.threadId << "]" << outputStr << std::endl;
        }

        if (!logProxy)
        {
            buffer.push_back(msg);
        }

        resetLocation();
    }
    catch (boost::thread_exception& e)
    {
        std::cout << outputStr << std::endl;
        return; // mutex may be deleted already if the system shuts down at this moment
    }
}

long LogSender::getThreadId()
{
#ifdef WIN32
    return 0;
#else
#ifdef __APPLE__
    mach_port_t tid = pthread_mach_thread_np(pthread_self());
    return (long)tid;
#else
    return syscall(SYS_gettid);
#endif
#endif
}

long LogSender::getProcessId()
{
#ifdef WIN32
    return 0;
#else
    return getpid();
#endif
}


void LogSender::flush()
{

    log(currentSeverity, currentMessage.str());
    currentMessage.str(""); // reset
}

std::string LogSender::CropFunctionName(std::string const& originalFunctionName)
{
    unsigned int maxLength = 40;
    std::string result;

    if (originalFunctionName.length() <= maxLength)
    {
        return originalFunctionName;
    }

    std::string beforeFunctionName = originalFunctionName.substr(0, originalFunctionName.rfind("::"));
    std::string::size_type namespaceDotsPos = beforeFunctionName.find("::", beforeFunctionName.find(' '));

    if (namespaceDotsPos != std::string::npos)
    {
        std::string afterClassName = originalFunctionName.substr(originalFunctionName.rfind("::"));
        result = beforeFunctionName.substr(namespaceDotsPos + 2) + afterClassName;
    }
    else
    {
        result = originalFunctionName;
    }

    if (result.length() <= maxLength)
    {
        return result;
    }


    result = result.substr(0, result.find("("));
    result += "(...)";

    return result;



}


std::string LogSender::CreateBackTrace(int linesToSkip)
{
    void* callstack[128];
    const int nMaxFrames = sizeof(callstack) / sizeof(callstack[0]);
    char buf[1024];

    int nFrames = ::backtrace(callstack, nMaxFrames);
    char** symbols = backtrace_symbols(callstack, nFrames);

    std::ostringstream trace_buf;

    for (int i = linesToSkip; i < nFrames; i++)
    {
        Dl_info info;

        if (dladdr(callstack[i], &info) && info.dli_sname)
        {
            char* demangled = nullptr;
            int status = -1;

            if (info.dli_sname[0] == '_')
            {
                demangled = abi::__cxa_demangle(info.dli_sname, nullptr, nullptr, &status);
            }

            snprintf(buf, sizeof(buf), "%-3d %*p %s + %zd\n",
                     i - linesToSkip + 1, int(2 + sizeof(void*) * 2), callstack[i],
                     status == 0 ? demangled :
                     info.dli_sname == nullptr ? symbols[i] : info.dli_sname,
                     (char*)callstack[i] - (char*)info.dli_saddr);
            free(demangled);
        }
        else
        {
            snprintf(buf, sizeof(buf), "%-3d %*p %s\n",
                     i - linesToSkip + 1, int(2 + sizeof(void*) * 2), callstack[i], symbols[i]);
        }

        trace_buf << buf;
    }

    free(symbols);

    if (nFrames == nMaxFrames)
    {
        trace_buf << "[truncated]\n";
    }

    return trace_buf.str();
}

MessageType LogSender::getSeverity()
{
    return currentSeverity;
}

LogSenderPtr LogSender::setFile(const std::string& file)
{
    currentFile = file;
    return shared_from_this();
}

LogSenderPtr LogSender::setLine(int line)
{
    currentLine = line;
    return shared_from_this();
}

LogSenderPtr LogSender::setFunction(const std::string& function)
{
    currentFunction = function;
    return shared_from_this();
}

LogSenderPtr LogSender::setLocalMinimumLoggingLevel(MessageType level)
{
    minimumLoggingLevel = level;
    return shared_from_this();
}

LogSenderPtr LogSender::setBacktrace(bool printBackTrace)
{
    this->printBackTrace = printBackTrace;
    return shared_from_this();
}
LogSenderPtr LogSender::setThreadId(Ice::Int tid)
{
    threadId = tid;
    return shared_from_this();
}

LogSenderPtr LogSender::setTag(const LogTag& tag)
{
    currentTag = tag;
    return shared_from_this();
}

//LogSenderPtr LogSender::setSpamFilter(SpamFilterMapPtr spamFilter)
//{
//    this->spamFilter = spamFilter;
//    return shared_from_this();
//}

std::string LogSender::levelToString(MessageType type)
{
    switch (type)
    {
        case eDEBUG:
            return "Debug";

        case eVERBOSE:
            return "Verbose";

        case  eINFO:
            return "Info";

        case eIMPORTANT:
            return "Important";

        case eWARN:
            return "Warning";

        case eERROR:
            return "Error";

        case eFATAL:
            return "Fatal";

        default:
            return "Undefined";
    }
}

MessageType LogSender::StringToLevel(const std::string& typeStr)
{
    if (boost::iequals(typeStr, "Debug"))
    {
        return eDEBUG;
    }

    if (boost::iequals(typeStr, "Verbose"))
    {
        return eVERBOSE;
    }

    if (boost::iequals(typeStr, "Important"))
    {
        return eIMPORTANT;
    }

    if (boost::iequals(typeStr, "Warning"))
    {
        return eWARN;
    }

    if (boost::iequals(typeStr, "Error"))
    {
        return eERROR;
    }

    if (boost::iequals(typeStr, "Fatal"))
    {
        return eFATAL;
    }

    return eUNDEFINED;

}

void LogSender::SetGlobalMinimumLoggingLevel(MessageType level)
{
    if (level == GlobalMinimumLoggingLevel)
    {
        return;
    }

    GlobalMinimumLoggingLevel = level;
    std::cout << "setting global logging level to "  << LogSender::levelToString(level) << std::endl;
}

MessageType LogSender::GetGlobalMinimumLoggingLevel()
{
    return GlobalMinimumLoggingLevel;
}


void LogSender::SetLoggingActivated(bool activated, bool showMessage)
{
    if (LoggingActivated && showMessage && !activated)
    {
        std::cout << "logging is now DEACTIVATED" << std::endl;
    }

    if (activated && LoggingActivated != activated && showMessage)
    {
        std::cout << "logging is now ACTIVATED" << std::endl;
    }
    LoggingActivated = activated;
}

void LogSender::SetSendLoggingActivated(bool activated)
{
    SendLogging = activated;
}

void LogSender::SetColoredConsoleActivated(bool activated)
{
    ColoringActivated = activated;
    //    if(activated)
    //        std::cout << "setting colored console to activated" << std::endl;
    //    else
    //        std::cout << "setting colored console to deactivated" << std::endl;
}

void LogSender::initConsole()
{
    static bool initialized = false;

    if (!initialized)
    {
        initialized = true;
        char* termType = getenv("TERM");

        if (termType)
        {
            std::string termTypeStr(termType);

            if (termTypeStr != "xterm"
                && termTypeStr != "emacs")
            {
                LogSender::SetColoredConsoleActivated(false);
            }
        }
        else
        {
            LogSender::SetColoredConsoleActivated(false);
        }

    }
}

template<>
ARMARXCORE_IMPORT_EXPORT LogSender& LogSender::operator<< <LogSender::ConsoleColor>(const LogSender::ConsoleColor& colorCode)
{
    currentMessage << LogSender::GetColorCodeString(colorCode);

    return *this;
}




template<>
LogSender& LogSender::operator<< <MessageType>(const MessageType& severity)
{
    currentSeverity = severity;

    return *this;
}

template<>
LogSender& LogSender::operator<< <LogTag>(const LogTag& tag)
{
    currentTag = tag;

    return *this;
}

template<>
LogSender& LogSender::operator<< <LogSender::manipulator>(const manipulator& manipulator)
{
    (this->*manipulator)();

    return *this;
}

LogSender& LogSender::operator<< (const StandardEndLine& manipulator)
{
    flush();

    return *this;
}

template<>
LogSender& LogSender::operator<< <bool>(const bool& duality)
{
    if (duality)
    {
        currentMessage << "true";
    }
    else
    {
        currentMessage << "false";
    }

    return *this;
}

template<>
LogSender& LogSender::operator<< <SpamFilterDataPtr>(const SpamFilterDataPtr& spamFilterData)
{
    if (!spamFilterData)
    {
        return *this;
    }

    ScopedLock lock(*spamFilterData->mutex);
    float deactivationDurationSec = spamFilterData->durationSec;
    SpamFilterMapPtr spamFilter = spamFilterData->filterMap;
    std::string id = currentFile + ":" + to_string(currentLine);
    auto it = spamFilter->find(spamFilterData->identifier);

    if (it != spamFilter->end())
    {
        auto itSub = it->second.find(id);
        IceUtil::Time durationEnd = IceUtil::Time::now() + IceUtil::Time::milliSecondsDouble(deactivationDurationSec * 1000);

        if (itSub == it->second.end())
        {
            itSub = it->second.insert(
                        std::make_pair(
                            id,
                            durationEnd)).first;
            cancelNextMessage = false;
        }
        else if (IceUtil::Time::now() < itSub->second)
        {
            cancelNextMessage = true;
        }
        else
        {
            cancelNextMessage = false;
            itSub->second  = durationEnd;
        }
    }

    return *this;
}


