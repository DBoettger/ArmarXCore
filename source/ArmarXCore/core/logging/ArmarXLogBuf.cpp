/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "ArmarXLogBuf.h"

#include <boost/algorithm/string.hpp>
#include "../system/Synchronization.h"

armarx::ArmarXLogBuf::ArmarXLogBuf(const std::string& tagName, MessageType severity, bool flushOnlyOnNewLine) :
    _buffer(1024),
    flushOnlyOnNewLine(flushOnlyOnNewLine)
{
    _outBuffer.reserve(1024);
    char* base = &_buffer.front();
    setp(base, base + _buffer.size() - 1);
    log = armarx::LogSender::createLogSender()->setTag(LogTag(tagName));
    *log << severity;
}

int armarx::ArmarXLogBuf::overflow(int c)
{
    armarx::ScopedLock lock(mutex);
    if (c == EOF)
    {
        return EOF;
    }

    if (epptr() == pptr())
    {
        write();
    }

    *pptr() = c;
    pbump(1);
    return c;
}

int armarx::ArmarXLogBuf::sync()
{
    armarx::ScopedLock lock(mutex);
    flush();
    return 0;
}

void armarx::ArmarXLogBuf::write()
{
    for (char* p = pbase(), *e = pptr(); p != e; ++p)
    {
        _outBuffer += (*p);
    }
    std::ptrdiff_t n = pptr() - pbase();
    pbump(-n);
}

void armarx::ArmarXLogBuf::flush()
{
    write();
    if (!flushOnlyOnNewLine)
    {
        boost::trim(_outBuffer);
    }
    if (_outBuffer.size() == 0)
    {
        return;
    }

    try
    {
        *log << _outBuffer;
        if (!flushOnlyOnNewLine || _outBuffer.find('\n') != std::string::npos)
        {
            log->flush();
        }
        _outBuffer.clear();
    }
    catch (...)
    {

    }
}
