/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarXCore::core
* @author     Kai Welke (welke at kit dot edu)
* @author     Mirko Wächter 2012 (mirko dot waechter @ kit dot edu)
* @date       2010
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/
#pragma once

#include <ArmarXCore/core/logging/LogSender.h>
#include <boost/current_function.hpp>

/**
  *  \page LoggingDoc ArmarX Logging
  *
  *  Every output in ArmarX should be done with the built-in logging features.
  *  The ArmarX logging facilities are
  *  available anytime and anywhere in the code and should be used
  *  instead of std::cout or printf().<br/>
  *  ArmarX Logging provides the following main features:
  *  \li usable like std::cout
  *  \li stores meta data like file, line, time, who etc.
  *  \li offers several different log levels (from debug to fatal)
  *  \li can log backtraces
  *  \li provides logs over network (Ice)
  *  \li LogViewer (GUI) which works through Ice
  *  \li thread safety
  *  \li console coloring
  *  \li redirection of std::cout/cerr to ArmarX log
  *
  *  \section LoggingDoc-Usage How to use the ArmarX logging facilities
  *
  *  For logging there are a few macros available (e.g. "ARMARX_INFO"). They all
  *  begin with "ARMARX_". Followed by the log level (e.g. "ARMARX_INFO").
  *  <br/>
  *
  *  An example looks like this:<br/>
  *  \code
  *     ARMARX_INFO << "cycle time dropped below " << 1 << " fps in Capturer";
  *  \endcode
  *
  *  After the c++-line has ended (i.e. after the semi-colon), the data is written
  *  as <b>one entry</b> into the log.
  *
  *  To be able use the logging facilities, one has to include the file <ArmarXCore/core/logging/Logging.h>.<p/>
  *
  *  If you want to use all logging features, you need to let your class inherit from armarx::Logging.
  *  Then features like tagging and spam-deactivation are available.
  *
  *  \note To log inside a static function of a class inheriting from Logging, you have to
  *  use the logging macros ending on _S, e.g. ARMARX_INFO_S.
  *
  *  \note If the current minimum log-level is below the used log level, the output string calculation is completly skipped.
  *  This means no code with effect on any variable should be put into the logging-code-line.
  *  This also means it is performance-wise safe to have a lot debug messages in the code.
  *
  *  The main features in detail:
  *  \li <b>usable like std::cout</b><br/>
  *      The usage of ArmarX logging works like std::cout. That means after the macro (e.g.
  *      "ARMARX_INFO") follows the streaming operator (<<). There are conversions
  *      for all of the standard types available like with std::cout and some
  *      additional types (e.g. std::vector, QString in Gui). These can be easily
  *      extended by overloading/specializing the armarx::LogSender::operator<< <T>().
  *  \li <b>stores meta data like file, line, time, who etc.</b><br/>
  *      At the moment the logging stores the following meta data: time, tag, log-level,
  *      Ice-component, file, line, function, threadId and backtrace (for log-level
  *      eWARN and above).
  *  \li <b>offers several different log levels (from debug to fatal)</b><br>
  *      There are 5 log levels available. In order of increasing severity:
  *      debug, verbose, info, warning, error, fatal (The corresponding macros are:
  *      ARMARX_DEBUG, ARMARX_VERBOSE, ARMARX_INFO, ARMARX_WARNING, ARMARX_ERROR, ARMARX_FATAL).
  *  \li <b>can log backtraces</b><br>
  *      For the log levels warning and above a backtrace is stored in the meta data.
  *      For lower levels the backtrace is empty to reduce the network traffic.<br/>
  *      The backtrace is not shown in the console, only in the LogViewer.
  *  \li <b>provides logs over network (Ice)</b><br/>
  *      All log output is automatically streamed via Ice, if an Ice connection is available.<br/>
  *      If the connection is not established yet, all log outputs are buffered locally
  *      until the connection is ready.<br/>
  *      The logging facilties are using the publish-subscribe-methodology of Ice for this,
  *      so potential log-viewers can just subscribe to the topic "Log"(Type: LogPrx).
  *  \li <b>LogViewer (GUI)</b><br/>
  *      The LogViewer is an ArmarX Gui Widget, that provides a comfortable
  *      access to the logs. It offers the possibility to define custom filters, to split
  *      the data in several logs, to search quickly through the log file and much more.
  *  \li <b>thread safety</b><br/>
  *      As opposed to std::cout the ArmarX logging facilities are thread safe.
  *      So there is no mixing of log entries of different threads.
  *  \li <b>console coloring</b><br/>
  *      If activated (default) the output on the console of different log levels
  *      is colorized. Additionally, the user can just use the streaming operator \<\<
  *      and the LogSender color enum (e.g. LogSender::eGreen) to set the color
  *      for the current output message.
  *
  *
  *
  *
  *
 */


#define ARMARX_FUNCTION BOOST_CURRENT_FUNCTION
//! Dummy instance for faster skipped logging (if verbosity level is lower than selected level) - DO NOT USE THIS VARIABLE
const armarx::LogSender _GlobalDummyLogSender;

/**
 * \defgroup Logging
 * \ingroup core-utility
 * \copydoc LoggingDoc

 * \def ARMARX_LOG_S
 * \ingroup Logging
 * This macro creates a new temporary instance which
 * can then be used to log data using the << operator.
 * This macro is dedicated to the special case logging in a static function of a class that inherits Logging.
 * For all other cases the non static version should be used (without _S).
 * S stands for static.

 * example:
 * \code
 * ARMARX_LOG_S << eWARN << "cycle time dropped below " << 1 << "fps in " << "Capturer" << endl;
 * \endcode
 *
 */
#define ARMARX_LOG_S (*(armarx::LogSender::createLogSender()->setFile(__FILE__)->setLine(__LINE__)->setFunction(ARMARX_FUNCTION)))

/**
 * \def ARMARX_LOG
 * \deprecated
 * \ingroup Logging
 * This macro retrieves the armarx::LogSender instance of this class which
 * can then be used to log data using the << operator .
 * This macro can be used by classes that inherit from Logging.
 *
 * \note This is deprecated. Use the macros with attached logging level instead like ARMARX_INFO.
 *
 * example:
 * \code
 * ARMARX_LOG << eWARN << "cycle time dropped below " << 1 << "fps in " << "Capturer" << endl;
 * \endcode
 *
 */
#define ARMARX_LOG (*ARMARX_LOG_S.setLocalMinimumLoggingLevel(this->Logging::minimumLoggingLevel)) << this->armarx::Logging::tag


#define _ARMARX_LOG_INTERNAL_S(level) (level < armarx::LogSender::GetGlobalMinimumLoggingLevel()) ? _GlobalDummyLogSender : ARMARX_LOG_S << level

#define _ARMARX_LOG_INTERNAL_(level) (checkLogLevel(level)) ? _GlobalDummyLogSender : (*loghelper(__FILE__, __LINE__, ARMARX_FUNCTION)) << level



//! \ingroup Logging
//! The normal logging level
#define ARMARX_INFO _ARMARX_LOG_INTERNAL_(armarx::eINFO)
//! \ingroup Logging
//! The logging level for output that is only interesting while debugging
#define ARMARX_DEBUG _ARMARX_LOG_INTERNAL_(armarx::eDEBUG)
//! \ingroup Logging
//! The logging level for verbose information
#define ARMARX_VERBOSE _ARMARX_LOG_INTERNAL_(armarx::eVERBOSE)
//! \ingroup Logging
//! The logging level for always important information, but expected behaviour (in contrast to ARMARX_WARNING)
#define ARMARX_IMPORTANT _ARMARX_LOG_INTERNAL_(armarx::eIMPORTANT)
//! \ingroup Logging
//! The logging level for unexpected behaviour, but not a serious problem
#define ARMARX_WARNING _ARMARX_LOG_INTERNAL_(armarx::eWARN)
//! \ingroup Logging
//! The logging level for unexpected behaviour, that must be fixed
#define ARMARX_ERROR _ARMARX_LOG_INTERNAL_(armarx::eERROR)
//! \ingroup Logging
//! The logging level for unexpected behaviour, that will lead to a seriously malfunctioning program and probably to program exit
#define ARMARX_FATAL _ARMARX_LOG_INTERNAL_(armarx::eFATAL)

//! \ingroup Logging
#define ARMARX_INFO_S _ARMARX_LOG_INTERNAL_S(armarx::eINFO)
//! \ingroup Logging
//! The logging level for output that is only interesting while debugging
#define ARMARX_DEBUG_S _ARMARX_LOG_INTERNAL_S(armarx::eDEBUG)
//! \ingroup Logging
#define ARMARX_VERBOSE_S _ARMARX_LOG_INTERNAL_S(armarx::eVERBOSE)
//! \ingroup Logging
//! The logging level for always important information, but expected behaviour (in contrast to ARMARX_WARNING)
#define ARMARX_IMPORTANT_S _ARMARX_LOG_INTERNAL_S(armarx::eIMPORTANT)
//! \ingroup Logging
//! The logging level for unexpected behaviour, but not a serious problem
#define ARMARX_WARNING_S _ARMARX_LOG_INTERNAL_S(armarx::eWARN)
//! \ingroup Logging
//! The logging level for unexpected behaviour, that must be fixed
#define ARMARX_ERROR_S _ARMARX_LOG_INTERNAL_S(armarx::eERROR)
//! \ingroup Logging
//! The logging level for unexpected behaviour, that will lead to a seriously malfunctioning program and probably to program exit
#define ARMARX_FATAL_S _ARMARX_LOG_INTERNAL_S(armarx::eFATAL)

namespace armarx
{
    //! const Ice::Current instance to use as default parameter for all Ice interface implementations (better performance than constructing it on every call)
    const Ice::Current GlobalIceCurrent = Ice::Current();



    class LogSender;
    typedef boost::shared_ptr<LogSender> LogSenderPtr;

    struct SpamFilterData;
    typedef boost::shared_ptr<SpamFilterData> SpamFilterDataPtr;

    /**
      \class Logging
      \brief Base Class for all Logging classes.
      \ingroup Logging

      Inherit from this class in order to use ArmarX Logging facitities.

      You should call setTag() in the derived class's constructor,
      otherwise the category string will be empty.
    */
    class ARMARXCORE_IMPORT_EXPORT Logging
    {
    public:


        Logging();
        virtual ~Logging();
        void setTag(const LogTag& tag);
        void setTag(const std::string& tagName);
        /*!
         * \brief With setLocalMinimumLoggingLevel the minimum verbosity-level of
         * log-messages can be set.
         * \param level The minimum logging level
         */
        void setLocalMinimumLoggingLevel(MessageType level);
        MessageType getEffectiveLoggingLevel() const;

        /**
         * @brief disables the logging for the current line for the given amount of seconds.
         * Usage:
         * @verbatim
           ARMARX_INFO << deactivateSpam(1) << "My Controller value" << x;
           @endverbatim
         * @note Needs to be called like above. Just calling it has no effect.
         * @param deactivationDurationSec Duration for which the logging is deactivated.
         * @param identifier Additional log entry identifier. Useful, when having a loop and each iteration should be printed.
         * @param deactivate If true the logging is disabled. If false the logging is immediately enabled.
         * @return Internal datastruct for the logging framework.
         */
        SpamFilterDataPtr deactivateSpam(float deactivationDurationSec = 10.0f, const std::string& identifier = "", bool deactivate = true) const;

    protected:
        /** Retrieve log sender.
         *
         * This method is usually called by using one of the loggin macro ARMARX_LOG.
         *
         * @return pointer to the logSender
         */
        const LogSenderPtr& getLogSender() const;
        LogTag tag;
        MessageType minimumLoggingLevel;
        mutable SpamFilterDataPtr spamFilter;
        LogSenderPtr loghelper(const char* file, int line, const char* function) const;
        bool checkLogLevel(MessageType level) const;

    private:
        LogSenderPtr logSender;

    };





    /**
     * @brief use this macro to write output code that is executed when printed
     * and thus not executed if the debug level prevents printing.
     * In the code you should write to the stream out to output messages.
     *
     * Example:
     * \code{.cpp}
     * ARMARX_DEBUG << ARMARX_STREAM_PRINTER
        {
            for (const std::string& elem : someSetOfStrings)
            {
                if(elem.size() == 5)
                {
                    out << "    " << elem << "\n";
                }
            }
        };
     * \endcode
     * This code will only run if the effective debug level is Debug and only
     * entries are printed which satisfy the if condition
     */
#define ARMARX_STREAM_PRINTER ::armarx::detail::StreamPrinterTag::tag * [&](std::ostream& out)
}

armarx::LogSenderPtr loghelper(const char* file, int line, const char* function);
bool checkLogLevel(armarx::MessageType level);
armarx::SpamFilterDataPtr deactivateSpam(float deactivationDurationSec = 10.0f, const std::string& identifier = "", bool deactivate = true);
inline armarx::SpamFilterDataPtr deactivateSpam(const std::string& identifier, float deactivationDurationSec = 10.0f, bool deactivate = true)
{
    return deactivateSpam(deactivationDurationSec, identifier, deactivate);
}

