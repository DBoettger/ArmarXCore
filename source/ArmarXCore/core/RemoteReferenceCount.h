/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2017, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarX
 * @author     Raphael Grimm( raphael dor grimm at kit dot edu)
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <set>
#include <queue>
#include <map>

#include <atomic>
#include <functional>
#include <string>
#include <mutex>
#include <thread>

#include <IceUtil/UUID.h>

#include <ArmarXCore/interface/core/RemoteReferenceCount.h>

#include "logging/Logging.h"

//internal base impl
namespace armarx
{
    class ArmarXManager;
    class RemoteReferenceCountControlBlockManager;
    namespace detail
    {
        class RemoteReferenceCountControlBlockManagementInterface : virtual public Ice::Object
        {
        public:
            using ArmarXManagerPtr = IceUtil::Handle<ArmarXManager>;
            void activateCounting()
            {
                countingActivated_ = true;
            }
            bool isCountingActivated() const
            {
                return countingActivated_;
            }
            bool hasCountReachedZero() const
            {
                return countReachedZero_;
            }
            Ice::ObjectPrx getProxy()  const
            {
                return selfProxy;
            }

            RemoteReferenceCountControlBlockManagementInterface(const ArmarXManagerPtr& manager, const std::string& id);

            ~RemoteReferenceCountControlBlockManagementInterface() override;

        protected:
            friend class ::armarx::RemoteReferenceCountControlBlockManager;
            /**
             * @brief Returns the next timepoint when this ControlBlock should be checked.
             * If the returned time is in the past and counting was activated, this Block
             * is removed from counting and onCountReachedZero is called.
             * @return
             */
            virtual IceUtil::Time nextCheckTimePoint() = 0;
            virtual void onCountReachedZero() = 0;

            void countReachedZero();

            ArmarXManagerPtr armarXManager;
            Ice::ObjectPrx selfProxy;

            std::mutex mtx;
            IceUtil::Time lastTimeReachedZero;

            const std::string id;

        private:
            std::atomic_bool countingActivated_ {false};
            std::atomic_bool countReachedZero_ {false};
        };
        using RemoteReferenceCountControlBlockManagementInterfacePtr = IceUtil::Handle<detail::RemoteReferenceCountControlBlockManagementInterface>;
    }
}

//internal base impl AbstractRemoteReferenceCountControlBlock
namespace armarx
{
    namespace detail
    {
        class AbstractRemoteReferenceCountControlBlock
            : virtual public RemoteReferenceCountControlBlockInterface,
              virtual public RemoteReferenceCountControlBlockManagementInterface
        {
        public:
            using ArmarXManagerPtr = IceUtil::Handle<ArmarXManager>;
            RemoteReferenceCounterBasePtr getReferenceCounter();

            void heartbeat(const std::string& counterId, const Ice::Current& = GlobalIceCurrent) final override;
            void addCounter(const std::string& counterId, const Ice::Current& = GlobalIceCurrent) final override;
            void removeCounter(const std::string& counterId, const Ice::Current& = GlobalIceCurrent) final override;

        protected:
            AbstractRemoteReferenceCountControlBlock(const ArmarXManagerPtr& manager, const std::string& id,
                    IceUtil::Time deletionDelay, IceUtil::Time orphantDeletionDelay, long heartBeatMs);

        private:
            IceUtil::Time nextCheckTimePoint() final override;

            std::map<std::string, IceUtil::Time> counterIds;
            const IceUtil::Time deletionDelay;
            const IceUtil::Time orphantDeletionDelay;
            const long heartBeatMs;
        };
    }
}
//internal base impl AbstractSimpleRemoteReferenceCountControlBlock
namespace armarx
{
    namespace detail
    {
        class AbstractSimpleRemoteReferenceCountControlBlock
            : virtual public SimpleRemoteReferenceCountControlBlockInterface,
              virtual public RemoteReferenceCountControlBlockManagementInterface
        {
        public:
            using ArmarXManagerPtr = IceUtil::Handle<ArmarXManager>;
            SimpleRemoteReferenceCounterBasePtr getReferenceCounter();

            void addCounter(const std::string& counterId, const Ice::Current&) final override;
            void removeCounter(const std::string& counterId, const Ice::Current&) final override;

        protected:
            AbstractSimpleRemoteReferenceCountControlBlock(const ArmarXManagerPtr& manager, const std::string& id, IceUtil::Time deletionDelay);

        private:
            IceUtil::Time nextCheckTimePoint() final override;

            std::set<std::string> counterIds;
            const IceUtil::Time deletionDelay;
        };
    }
}
//impl RemoteReferenceCountControlBlock
namespace armarx
{
    template<class FunctionType = std::function<void(void)>, class DataType = void>
    class RemoteReferenceCountControlBlock : virtual public detail::AbstractRemoteReferenceCountControlBlock
    {
        RemoteReferenceCountControlBlock(
            const ArmarXManagerPtr& manager, const std::string& id, FunctionType f, DataType  d,
            IceUtil::Time deletionDelay, IceUtil::Time orphantDeletionDelay, long heartBeatMs
        )
            : detail::RemoteReferenceCountControlBlockManagementInterface(manager, id),
              detail::AbstractRemoteReferenceCountControlBlock(manager, id, deletionDelay, orphantDeletionDelay, heartBeatMs),
              data {std::move(d)},
              function {std::move(f)}
        {}

        DataType data;
        FunctionType function;
        void onCountReachedZero() final override
        {
            function(data);
        }
        friend class ArmarXManager;
        using ArmarXManagerPtr = IceUtil::Handle<ArmarXManager>;
    };
    template<class FunctionType>
    class RemoteReferenceCountControlBlock<FunctionType, void> : virtual public detail::AbstractRemoteReferenceCountControlBlock
    {
        RemoteReferenceCountControlBlock(
            const ArmarXManagerPtr& manager, const std::string& id, FunctionType f,
            IceUtil::Time deletionDelay, IceUtil::Time orphantDeletionDelay, long heartBeatMs
        )
            : detail::RemoteReferenceCountControlBlockManagementInterface(manager, id),
              detail::AbstractRemoteReferenceCountControlBlock(manager, id, deletionDelay, orphantDeletionDelay, heartBeatMs),
              function {std::move(f)}
        {}
        FunctionType function;
        void onCountReachedZero() final override
        {
            function();
        }
        friend class ArmarXManager;
        using ArmarXManagerPtr = IceUtil::Handle<ArmarXManager>;
    };

    template<class FunctionType, class DataType = void>
    using RemoteReferenceCountControlBlockPtr = IceUtil::Handle<RemoteReferenceCountControlBlock<FunctionType, DataType>>;
}
//impl SimpleRemoteReferenceCountControlBlock
namespace armarx
{
    template<class FunctionType = std::function<void(void)>, class DataType = void>
    class SimpleRemoteReferenceCountControlBlock : virtual public detail::AbstractSimpleRemoteReferenceCountControlBlock
    {
        SimpleRemoteReferenceCountControlBlock(
            const ArmarXManagerPtr& manager, const std::string& id, FunctionType f, DataType  d, IceUtil::Time deletionDelay)
            : detail::RemoteReferenceCountControlBlockManagementInterface(manager, id),
              detail::AbstractSimpleRemoteReferenceCountControlBlock(manager, id, deletionDelay),
              data {std::move(d)}, function {std::move(f)}
        {}
        DataType data;
        FunctionType function;
        void onCountReachedZero() final override
        {
            function(data);
        }
        friend class ArmarXManager;
        using ArmarXManagerPtr = IceUtil::Handle<ArmarXManager>;
    };
    template<class FunctionType>
    class SimpleRemoteReferenceCountControlBlock<FunctionType, void> : virtual public detail::AbstractSimpleRemoteReferenceCountControlBlock
    {
        SimpleRemoteReferenceCountControlBlock(
            const ArmarXManagerPtr& manager, const std::string& id, FunctionType f, IceUtil::Time deletionDelay)
            : detail::RemoteReferenceCountControlBlockManagementInterface(manager, id),
              detail::AbstractSimpleRemoteReferenceCountControlBlock(manager, id, deletionDelay), function {std::move(f)}
        {}
        FunctionType function;
        void onCountReachedZero() final override
        {
            function();
        }
        friend class ArmarXManager;
        using ArmarXManagerPtr = IceUtil::Handle<ArmarXManager>;
    };

    template<class FunctionType, class DataType = void>
    using SimpleRemoteReferenceCountControlBlockPtr = IceUtil::Handle<SimpleRemoteReferenceCountControlBlock<FunctionType, DataType>>;
}
//impl manager
namespace armarx
{
    class RemoteReferenceCountControlBlockManager
    {
    public:
        RemoteReferenceCountControlBlockManager(IceUtil::Time period): period {period} {}
        ~RemoteReferenceCountControlBlockManager()
        {
            stop();
        }
        static const Ice::Long DefaultDeletionDelayMs;
        static const Ice::Long DefaultOrphantDeletionDelayMs;

        void add(detail::RemoteReferenceCountControlBlockManagementInterfacePtr ptr);
        void stop();

    private:
        void manage();

        const IceUtil::Time period;
        using PqEntry = std::pair<IceUtil::Time, detail::RemoteReferenceCountControlBlockManagementInterfacePtr>;
        struct PqEntryCompare
        {
            bool operator()(const PqEntry& lhs, const PqEntry& rhs) const
            {
                return std::make_pair(lhs.first, lhs.second.get()) < std::make_pair(rhs.first, rhs.second.get());
            }
        };
        struct RRCBMIPtr
        {
            bool operator()(
                const detail::RemoteReferenceCountControlBlockManagementInterfacePtr& lhs,
                const detail::RemoteReferenceCountControlBlockManagementInterfacePtr& rhs) const
            {
                return lhs.get() < rhs.get();
            }
        };
        std::mutex stateMutex;
        std::set<detail::RemoteReferenceCountControlBlockManagementInterfacePtr, RRCBMIPtr> pendingForActivation;
        std::priority_queue<PqEntry, std::vector<PqEntry>, PqEntryCompare> rrccbs;
        std::thread thread;
        std::atomic_bool shutdown {false};
    };
}

