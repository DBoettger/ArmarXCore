/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarXCore::core
* @author     Kai Welke (welke at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "ArmarXCore/core/application/properties/Property.h"
#include "ArmarXCore/core/application/properties/PropertyUser.h"
#include "Component.h"

using namespace armarx;

std::string Component::getConfigDomain()
{
    return configDomain;
}

std::string Component::getConfigName()
{
    return configName;
}

std::string Component::getConfigIdentifier()
{
    return configDomain + "." + configName;
}

void Component::initializeProperties(const std::string& configName, Ice::PropertiesPtr properties, const std::string& configDomain)
{
    this->configDomain = configDomain;
    this->configName = configName;

    // set default object name to configName
    setName(this->configName);

    // set properties
    setIceProperties(properties);
    icePropertiesInitialized();
}


void Component::addPropertyUser(const PropertyUserPtr& subPropertyUser)
{
    additionalPropertyUsers.push_back(subPropertyUser);
}

std::vector<PropertyUserPtr> Component::getAdditionalPropertyUsers() const
{
    return additionalPropertyUsers;
}


void Component::forceComponentCreatedByComponentCreateFunc()
{
    this->createdByComponentCreate = true;
}

PropertyDefinitionsPtr Component::createPropertyDefinitions()
{
    return PropertyDefinitionsPtr(new ComponentPropertyDefinitions(getConfigDomain() + "." + getConfigName()));
}

void Component::icePropertiesUpdated(const std::set<std::string>& changedProperties)
{
    // check if properties have been initialized
    if (getConfigDomain().empty() || getConfigName().empty())
    {
        return;
    }

    // set the logging behavior for this component
    if (changedProperties.count("MinimumLoggingLevel") && getProperty<MessageType>("MinimumLoggingLevel").isSet())
    {
        setLocalMinimumLoggingLevel(
            getProperty<MessageType>("MinimumLoggingLevel").getValue());
    }

    // get well-known name of this component instance
    // (default is the ManagedIceObject name itself)
    if (changedProperties.count("ObjectName") && getProperty<std::string>("ObjectName").isSet())
    {
        setName(getProperty<std::string>("ObjectName").getValue());
    }

    // set logging tag, may be overwritten by implementer
    setTag(getName());

    // enable Profiling
    // enable via global ArmarX.Profiling property and disable with specific one?
    if (changedProperties.count("EnableProfiling"))
    {
        enableProfiler(getProperty<bool>("EnableProfiling").getValue());
    }

    //only update user properties after the object was initialized
    if (getState() > eManagedIceObjectInitialized)
    {
        componentPropertiesUpdated(changedProperties);
    }
}

void Component::componentPropertiesUpdated(const std::set<std::string>& changedProperties)
{
}
