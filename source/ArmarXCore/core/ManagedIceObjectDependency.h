/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarXCore::core
* @author     Kai Welke (welke at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

// ArmarXCore
#include <ArmarXCore/core/IceManager.h>
#include <ArmarXCore/core/system/ImportExport.h>

// interface
#include <ArmarXCore/interface/core/ManagedIceObjectDependencyBase.h>

// stl
#include <string>

namespace armarx
{
    /**
     * ManagedIceObjectDependency shared pointer for convenience
     */
    class ManagedIceObjectDependency;
    typedef IceUtil::Handle<ManagedIceObjectDependency> ManagedIceObjectDependencyPtr;

    /**
     * Map of dependency names and dependecies.
     */
    typedef std::map<std::string, ManagedIceObjectDependencyPtr> DependencyList;


    /**
     * @class ManagedIceObjectDependency
     * @brief The ManagedIceObjectDependency class is part of the ManagedIceObjectConnectivity.
     * @ingroup DistributedProcessingSub
     *
     * For each dependency on the distributed application, a ManagedIceObjectDependency is added.
     * The dependencies are resolved by the ArmarXObjectScheduler.
     *
     * The checkDependency method needs to be implemented in each dependency.
     */
    class ARMARXCORE_IMPORT_EXPORT ManagedIceObjectDependency :
        public ManagedIceObjectDependencyBase
    {
    public:
        /**
         * Required by factory functions
         */
        ManagedIceObjectDependency() { }

        /**
         * Constructs a ManagedIceObjectDependency
         *
         * @param iceManager pointer to the ice manager
         * @param name name of dependant object
         * @param type type of dependency
         */
        ManagedIceObjectDependency(IceManagerPtr iceManager, std::string name, std::string type)
        {
            this->name = name;
            this->type = type;
            this->iceManager = iceManager;

            resolved = false;
            stateChanged = true;
        }

        /**
         * Retrieve name of dependency. Part of the Ice interface.
         *
         * @param c Ice context
         * @return name of dependant object
         */
        std::string getName(const Ice::Current& c = ::Ice::Current()) override
        {
            return name;
        }

        /**
         * Retrieve type of dependency. Part of the Ice interface.
         *
         * @param c Ice context
         * @return type of the dependency
         */
        std::string getType(const Ice::Current& c = ::Ice::Current()) override
        {
            return type;
        }

        /**
         * Retrieve whether dependency is resolved. Part of the Ice interface.
         *
         * @param c Ice context
         * @return dependency resolved
         */
        bool getResolved(const Ice::Current& c = ::Ice::Current()) override
        {
            return resolved;
        }

        /**
         * Retrieve whether state has changed since the last call to the method.
         *
         * @return state change condition
         */
        bool getStateChanged()
        {
            bool result = stateChanged;
            stateChanged = false;
            return result;
        }

        /**
         * This method is called to check the dependency and update the resolved and statechanged members.
         * Internally calls checkDependency of subclass.
         */
        void check()
        {
            bool oldResolved = resolved;
            resolved = checkDependency();

            if (resolved != oldResolved)
            {
                stateChanged = true;
            }
        };

    protected:
        // implement in subclass
        virtual bool checkDependency() = 0;

        // pointer to iceManager
        IceManagerPtr iceManager;
    private:
        // state changed
        bool stateChanged;
    };

    /**
     * @class ProxyDependency
     * @brief The ProxyDependency class is part of the ManagedIceObjectConnectivity.
     * @ingroup DistributedProcessingSub
     *
     * For each proxy used by a ManagedIceObject by calling ManagedIceObject::usingProxy, a
     * ProxyDependency is inserted in the ManagedIceObjectConnectivity.
     *
     * The dependency check is realized by pinging the well-known object.
     */
    class ARMARXCORE_IMPORT_EXPORT ProxyDependency :
        public ManagedIceObjectDependency
    {
    public:
        /**
         * Required by the ProxyDependencyFactory
         */
        ProxyDependency() {}

        /**
         * Constructs a ProxyDependency
         *
         * @param iceManager pointer to the ice manager
         * @param proxyName name of dependant proxy
         */
        ProxyDependency(IceManagerPtr iceManager, std::string proxyName)
            : ManagedIceObjectDependency(iceManager, proxyName, "Proxy")
        {
            // create proxy
            proxy = iceManager->getCommunicator()->stringToProxy(getName());
        }

        /**
         * Implementation of the dependency check
         *
         * @return whether dependency is resolved
         */
        bool checkDependency() override
        {
            // try to ping
            try
            {
                // object needs to be pingable
                proxy->ice_timeout(1000)->ice_ping();
                return true;
            }
            catch (...)
            {
                return false;
            }
        }
    private:
        // proxy of the dependency
        Ice::ObjectPrx proxy;
    };
}
