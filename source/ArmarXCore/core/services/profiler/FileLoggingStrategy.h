#pragma once

/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core::services::profiler
 * @author     Manfred Kroehnert ( manfred dot kroehnert at dot kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include "LoggingStrategy.h"

#include <vector>
#include <mutex>

namespace armarx
{
    namespace Profiler
    {
        class FileLoggingStrategy;
        typedef boost::shared_ptr<FileLoggingStrategy> FileLoggingStrategyPtr;

        /**
         * @class FileLoggingStrategy
         * @ingroup Profiling
         * @brief A brief description
         *
         * Detailed Description
         */
        class FileLoggingStrategy :
            virtual public LoggingStrategy
        {
        public:
            FileLoggingStrategy();

            ~FileLoggingStrategy() override;

            void logEvent(pid_t processId, uint64_t timestamp, const std::string& executableName, const std::string& timestampUnit, const std::string& eventName, const std::string& parentName, const std::string& functionName) override;
        private:
            void writeFile();
            void writeHeader(std::ostream& outputStream);

            std::mutex mutex;
            std::vector<std::string> eventList;
        };
    }
}
