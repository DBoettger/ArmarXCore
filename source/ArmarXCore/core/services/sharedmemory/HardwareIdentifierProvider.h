/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::Core
 * @author     Kai Welke (kai dot welke at kit dot edu)
 * @date       2012
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <ArmarXCore/interface/core/SharedMemory.h>
#include <ArmarXCore/core/services/sharedmemory/HardwareId.h>

namespace armarx
{
    /**
    * \class HardwareIdentifierProvider is used to retreive a unique Hardware identifier
    * \ingroup SharedMemory
    */
    class HardwareIdentifierProvider :
        virtual public HardwareIdentifierProviderInterface
    {
    protected:
        /**
        * Retrieve machine's hardware Id string.
        * The string is derived from the MAC address of the first network adapter.
        */
        std::string getHardwareId(const Ice::Current& c = ::Ice::Current()) override
        {
            return armarx::tools::getHardwareId();
        }
    };
}

