/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarXCore::core
* @author     Kai Welke (kai dot welke at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include "ArmarXCore/core/exceptions/Exception.h"

#include <string>

namespace armarx
{
    namespace exceptions
    {
        namespace local
        {
            class SharedMemoryException : public armarx::LocalException
            {
            public:
                SharedMemoryException(std::string memoryName, std::string reason) : armarx::LocalException("")
                {
                    setReason("Error in shared memory '" + memoryName + "':  " + reason);
                }

                ~SharedMemoryException() noexcept override { }

                std::string name() const override
                {
                    return "armarx::exceptions::local::SharedMemoryException";
                }
            };

            class SharedMemoryConnectionFailure : public armarx::LocalException
            {
            public:
                SharedMemoryConnectionFailure(std::string providerName, std::string reason) : armarx::LocalException("")
                {
                    setReason("Error connecting to shared memory provider '" + providerName + "': " + reason);
                }

                ~SharedMemoryConnectionFailure() noexcept override { }


                std::string name() const override
                {
                    return "armarx::exceptions::local::SharedMemoryConnectionFailure";
                }
            };
        }
    }
}

