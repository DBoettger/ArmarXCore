/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    VisionX::Tools
 * @author     Kai Welke (kai dot welke at kit dot edu)
 * @date       2011
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "HardwareId.h"


// *******************************************************
// linux implementation
// *******************************************************
#ifdef __linux__
#include <unistd.h>
#include <errno.h>
#include <netinet/in.h>
#include <net/if.h>
#include <sys/ioctl.h>
#include <cstdio>
#include <cstring>

std::string armarx::tools::getHardwareId()
{
#ifndef SIOCGIFADDR
    return "00:00:00:00:00:00";
#endif

    int fd;
    struct ifreq ifr;
    std::string mac;

    // create socket
    fd = socket(AF_INET, SOCK_DGRAM, 0);
    ifr.ifr_addr.sa_family = AF_INET;
    strncpy(ifr.ifr_name, "eth0", IFNAMSIZ - 1);

    // retrieve hardware address
    ioctl(fd, SIOCGIFHWADDR, &ifr);

    int nError = errno;

    if (nError == 19)
    {
        printf("No ethernet adapter found. Using dummy MAC\n");
        mac = "00:00:00:00:00:00";
    }
    else
    {
        char temp[1024];
        sprintf(temp, "%.2x:%.2x:%.2x:%.2x:%.2x:%.2x",
                (unsigned char)ifr.ifr_hwaddr.sa_data[0],
                (unsigned char)ifr.ifr_hwaddr.sa_data[1],
                (unsigned char)ifr.ifr_hwaddr.sa_data[2],
                (unsigned char)ifr.ifr_hwaddr.sa_data[3],
                (unsigned char)ifr.ifr_hwaddr.sa_data[4],
                (unsigned char)ifr.ifr_hwaddr.sa_data[5]);

        mac = temp;
    }

    // close socket
    close(fd);

    return mac;
}
#endif /* __linux__ */

// *******************************************************
// windows implementation
// *******************************************************
#ifdef __WINDOWS__
#include <winsock2.h>
#include <iphlpapi.h>
#include <Winerror.h>

std::string armarx::tools::getHardwareId()
{

    std::string mac;
    IP_ADAPTER_INFO AdapterInfo[128];
    DWORD dwBufLen = sizeof(AdapterInfo);
    DWORD dwStatus = GetAdaptersInfo(AdapterInfo, &dwBufLen);

    // network card not found
    if (dwStatus != ERROR_SUCCESS)
    {
        return "00:00:00:00:00:00";
    }

    PIP_ADAPTER_INFO pAdapterInfo = AdapterInfo;
    char szBuffer[512];

    while (pAdapterInfo)
    {
        if (pAdapterInfo->Type == MIB_IF_TYPE_ETHERNET)
        {
            sprintf_s(szBuffer, sizeof(szBuffer), "%.2x:%.2x:%.2x:%.2x:%.2x:%.2x",
                      pAdapterInfo->Address[0],
                      pAdapterInfo->Address[1],
                      pAdapterInfo->Address[2],
                      pAdapterInfo->Address[3],
                      pAdapterInfo->Address[4],
                      pAdapterInfo->Address[5]);

            mac = szBuffer;
            return mac;
        }

        pAdapterInfo = pAdapterInfo->Next;
    }

    return "00:00:00:00:00:00";
}
#endif /* __WINDOWS__ */

// *******************************************************
// Apple implementation
// *******************************************************
#ifdef __APPLE__

std::string armarx::tools::getHardwareId()
{
    std::string mac = "00:00:00:00:00:00";
    return mac;
}
#endif /* __APPLE__ */
