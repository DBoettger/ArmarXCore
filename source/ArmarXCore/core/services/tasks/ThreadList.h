/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <ArmarXCore/interface/core/ThreadingIceBase.h>

#include <IceUtil/Thread.h>
#include <IceUtil/Time.h>
#include <ArmarXCore/core/ManagedIceObject.h>
#include <ArmarXCore/core/system/Synchronization.h>

#include <ostream>
#include <map>

namespace armarx
{

    class ThreadList;
    typedef IceInternal::Handle<ThreadList> ThreadListPtr;

    class ProcessWatcher;

    class ThreadList :
        virtual public ThreadListInterface,
        virtual public ManagedIceObject
    {
    public:
        ThreadList();
        Ice::StringSeq getRunningTaskNames(const Ice::Current& c = ::Ice::Current()) override;
        Ice::StringSeq getPeriodicTaskNames(const Ice::Current& c = ::Ice::Current()) override;

        double getCpuUsage(const Ice::Current& c = ::Ice::Current()) override;

        RunningTaskList getRunningTasks(const Ice::Current& c = ::Ice::Current()) override;
        PeriodicTaskList getPeriodicTasks(const Ice::Current& c = ::Ice::Current()) override;

        /**
         * @brief add RunningTask instance to this thread list
         * @param threadPtr pointer to the RunningTask instance to add
         */
        void addRunningTask(RunningTaskIceBase* threadPtr);

        /**
         * @brief remove RunningTask instance from this thread list
         * @param threadPtr pointer to the RunningTask instance to remove
         */
        bool removeRunningTask(RunningTaskIceBase* threadPtr);

        /**
         * @brief add PeriodicTask instance to this thread list
         * @param threadPtr pointer to the PeriodicTask instance to add
         */
        void addPeriodicTask(PeriodicTaskIceBase* threadPtr);

        /**
         * @brief remove PeriodicTask instance from this thread list
         * @param threadPtr pointer to the PeriodicTask instance to remove
         */
        bool removePeriodicTask(PeriodicTaskIceBase* threadPtr);

        /**
         * @brief getApplicationThreadList retrieves the ThreadList, that
         * contains all TimerTasks and PeriodicTasks in this Application.
         *
         * @return
         */

        static ThreadListPtr getApplicationThreadList();
        static int GetThreadJiffies(int threadId);
        static int GetHertz();
    protected:
        void setApplicationThreadListName(const std::string& threadListName);
        friend class ArmarXManager;
        //inherited from ManagedIceObject
        void onInitComponent() override;
        void onConnectComponent() override;
        void onDisconnectComponent() override;
        std::string getDefaultName() const override
        {
            return "ThreadList";
        }

        void updateCPUUsage();
    private:
        std::set<RunningTaskIceBase*> activeRunningTaskList;
        std::set<PeriodicTaskIceBase*> activePeriodicTaskList;
        Mutex runListMutex;
        Mutex periodicListMutex;
        Mutex procTotalTimeMutex;

        boost::shared_ptr<ProcessWatcher> processWatcher;
    };
}

namespace std
{
    ARMARXCORE_IMPORT_EXPORT ostream& operator<< (ostream& stream, const armarx::RunningTaskIceBase& task);
}


