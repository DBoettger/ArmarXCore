/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarXCore::core
* @author     Kai Welke (welke at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once


// Core includes
#include <ArmarXCore/core/system/ImportExport.h>
#include <ArmarXCore/core/exceptions/Exception.h>
#include "ThreadList.h"
#include <functional>

#include <IceUtil/Thread.h>
#include <IceUtil/ThreadException.h>

//#include <boost/thread.hpp>

#include <cxxabi.h>

#include <ArmarXCore/core/application/Application.h>

namespace armarx
{

    /**
      \page ThreadsDoc ArmarX Threads
      ArmarX provides utility methods in order to include different types of threads. In order to support the inclusion
      of multiple threads in a single objects, without the neccessity to write additional thread classes the mechanism
      uses functionoids of class methods.
      Two types of threads are provided by the ArmarX core: the RunningTask and the PeriodicTask. These can be
      extended in projects which use the ArmarX core.

      The API documentation can be found here: \ref Threads

      \section RunningTasks Using running tasks
      The running task executes one thread method once. The task can be started only once.
      \include RunningTask.dox

      \section PeriodicTasks Using periodic tasks
      The periodic task executes one thread method repeatedly using the time period specified in the constructor.
      \include PeriodicTask.dox



      \defgroup Threads
      \ingroup core-utility

    * \class RunningTask
    * \ingroup Threads
    * The running task executes one thread method once. The task can be started multiple times.
    */
    template <class T>
    class ARMARXCORE_IMPORT_EXPORT RunningTask :
        virtual public IceUtil::Thread,
        virtual protected RunningTaskIceBase
    {
    public:
        /**
         * Typedef for the thread method. Thread methods need to follow the template
         * void methodName(void). Parameter passing is not used since methods are members
         * of the class.
         */
        typedef void (T::*method_type)(void);

        /**
         * Shared pointer type for convenience.
         */
        typedef IceUtil::Handle<RunningTask<T> > pointer_type;

        /**
         * Constructs a running task within the class parent which calls the \p runningFn in
         * a new thread.
         *
         * @param name of this thread, that describes what it is doing. If string
         *        is empty, it will use the name of the class with RTTI
         */
        RunningTask(T* parent, method_type runningFn, const std::string& name = "")

        {
            threadJoined = false;
            running = false;
            stopped = false;
            finished = false;
            workload = 0.0f;

            if (name.empty())
            {
                char* demangled = nullptr;
                int status = -1;
                demangled = abi::__cxa_demangle(typeid(T).name(), nullptr, nullptr, &status);
                setName(demangled);
                free(demangled);
            }
            else
            {
                setName(name);
            }

            this->runningFn = runningFn;
            this->parent = parent;
        }



        /**
         * Destructor stops the thread and waits for completion.
         */
        ~RunningTask() override
        {
            stop();

        }

        void setName(const std::string& name)
        {
            RunningTaskIceBase::name = name;

            if (isRunning())
            {
#ifdef WIN32
#else
                //                    pthread_setname_np(_thread, name.c_str()); removed due to compatibility issues
#endif
            }

        }

        void setThreadList(ThreadListPtr threadList)
        {
            this->customThreadList = threadList;

            if (isRunning())
            {
                customThreadList->addRunningTask(this);
            }
        }

        /**
         * Starts the thread. Only has effect if the task has not been started. After completion of the task, it cannot be started again.
         */
        void start()
        {
            auto app = Application::getInstance();
            if (app && app->getForbidThreadCreation())
            {
                throw LocalException() << "Thread creation is now allowed in this application at the point in time!  Use Application::getInstance()->getThreadPool() instead.";
            }

            if (!running && !finished)
            {
                threadJoined = false;
                startTime = IceUtil::Time::now().toMicroSeconds();
                IceUtil::Thread::start();
#ifdef WIN32
#else
                //                    pthread_setname_np(_thread, name.c_str()); removed due to compatibility issues
#endif
                running = true;
            }

            if (!running && finished)
            {
                throw LocalException("Running Task '" +  RunningTaskIceBase::name  + "' is already finished and cannot be started again.");
            }
        }

        /**
         * Stops the thread. Only has effect if the task has been started.
         *
         * @param waitForJoin wait for the thread to terminate and join with the process.
         */
        void stop(bool waitForJoin = true)
        {
            if (waitForJoin)
            {
                stoppingMutex.lock();    // thread is beeing stopped at the moment
            }
            else if (!stoppingMutex.try_lock())
            {
                return;    // thread is already being stopped
            }

            try
            {
                if (running)
                {
                    if (!stopped)
                    {
                        {
                            ScopedLock lock(stopMutex);
                            stopped = true;
                            stopCondition.notify_all();
                        }
                    }
                }

                if (waitForJoin && !threadJoined)
                {
                    threadJoined = true;
                    getThreadControl().join();
                }

                stoppingMutex.unlock();

            }
            catch (IceUtil::ThreadSyscallException& e) // happens for example when the thread is already erased
            {
                stoppingMutex.unlock();
            }
            catch (IceUtil::ThreadNotStartedException& e)
            {
                stoppingMutex.unlock();
            }
            catch (...) // make sure that the mutex is unlocked on all exceptions
            {
                ARMARX_INFO_S << "Got exception in RunningTask::stop()";
                stoppingMutex.unlock();
                throw;
            }

        }

        /**
         * @brief Wait for the RunningTask to finish *without* telling it to finish.
         */
        void join()
        {
            ScopedLock lock(stoppingMutex);
            if (!threadJoined)
            {
                threadJoined = true;
                getThreadControl().join();
            }
        }

        /**
         * Retrieve running state of the thread. The task is running when it is started until the thread method joints.
         *
         * @return running state
         */
        bool isRunning() const
        {
            return running;
        }

        /**
         * Retrieve finished state of the thread. The task is finished once the thread has joined.
         *
         * @return finished state
         */
        bool isFinished() const
        {
            return finished;
        }

        /**
         * wait blocking for thread to be finished. The task is finished once the thread has joined.
         * @param timeoutMS timeout in milliseconds. If -1 there is no timeout.
         * @return returns true if the task finished in the given time interval
         */
        bool waitForFinished(int timeoutMS = -1)
        {
            ScopedLock lock(finishedMutex);
            if (!running && !finished) // the task never started
            {
                return true;
            }
            while (!isFinished())
            {
                if (timeoutMS == -1)
                {
                    finishedCondition.wait(lock);
                }
                else if (!finishedCondition.timed_wait(lock, boost::posix_time::milliseconds(timeoutMS)))
                {
                    return false;
                }

            }
            return true;
        }

        /**
         * Retrieve whether stop() has been called. Has to be handled in thread function implementation for proper exit.
         *
         * @return stopped state
         */
        bool isStopped()
        {
            lastFeedbackTime = IceUtil::Time::now().toMicroSeconds();
            return stopped;
        }

        /**
         * Wait blocking for thread until stop() has been called. Can be used for blocking wait for stop in thread function implementation
         */
        void waitForStop()
        {
            ScopedLock lock(stopMutex);

            while (!isStopped())
            {
                stopCondition.wait(lock);
            }

        }

        std::string getName() const
        {
            return RunningTaskIceBase::name;
        }
    private:
        void run() override
        {
            threadId = LogSender::getThreadId();
            ThreadList::getApplicationThreadList()->addRunningTask(this);

            if (customThreadList)
            {
                customThreadList->addRunningTask(this);
            }

            try
            {
                (parent->*runningFn)();
            }
            catch (...)
            {
                handleExceptions();
            }

            ScopedLock lock(finishedMutex);
            finished = true;
            running = false;
            ThreadList::getApplicationThreadList()->removeRunningTask(this);

            if (customThreadList)
            {
                customThreadList->removeRunningTask(this);
            }

            stopCondition.notify_all();
            finishedCondition.notify_all();
        }

        // thread method
        T*                      parent;
        method_type             runningFn;


        // sync
        //! mutex for the stop function
        Mutex                   stoppingMutex;
        //! mutex for the stopped status
        Mutex                   stopMutex;
        boost::condition_variable stopCondition;
        //! mutex for the finished status
        Mutex                   finishedMutex;
        boost::condition_variable finishedCondition;

        ThreadListPtr customThreadList;
        bool threadJoined;
    };



}


