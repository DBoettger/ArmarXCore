/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2017, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarX
 * @author     Mirko Waechter( mirko.waechter at kit dot edu)
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include "PeriodicTask.h"
#include "RunningTask.h"
#include <functional>

namespace armarx
{

    /**
     * Usage:
     \verbatim
        SimplePeriodicTask<>::pointer_type task = new SimplePeriodicTask<>([&]
        {
           ARMARX_INFO << "Hello world from another thread";
        }, 100);
        task->start();
     \endverbatim
     */
    template<class Functor = std::function<void(void)> >
    class SimplePeriodicTask : public PeriodicTask<SimplePeriodicTask<Functor> >
    {
    public:
        SimplePeriodicTask(Functor f, int periodMs, bool assureMeanInterval = false, std::string name = "", bool forceSystemTime = true):
            PeriodicTask<SimplePeriodicTask<Functor> >(this, &SimplePeriodicTask::runningFn, periodMs, assureMeanInterval, name, forceSystemTime),
            f(f)
        {}
        void runningFn()
        {
            f();
        }
        Functor f;
    };

    /**
     * Usage:
     \verbatim
        SimpleRunningTask<>::pointer_type task = new SimpleRunningTask<>([&]
        {
           ARMARX_INFO << "Hello world from another thread";
        });
        task->start();
     \endverbatim
     */
    template<class Functor = std::function<void(void)>>
    class SimpleRunningTask : public RunningTask<SimpleRunningTask<Functor> >
    {
    public:
        SimpleRunningTask(Functor f, std::string name = ""):
            RunningTask<SimpleRunningTask<Functor> >(this, &SimpleRunningTask::runningFn, name = ""),
            f(f)
        {}
        void runningFn()
        {
            f();
        }
        Functor f;
    };

}

