/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2017, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarX
 * @author     Mirko Waechter( mirko.waechter at kit dot edu)
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once
#include <boost/asio.hpp>
#include <boost/thread.hpp>
#include <mutex>
#include <future>
//#include <ArmarXCore/core/system/Synchronization.h>

namespace armarx
{

    /**
     * @brief The ThreadPool class
     *
     * Usage:
     * \code{.cpp}
     * ThreadPool pool(5);
     * auto handle = pool.runTask([]{
     *   ...
     * });
     * handle.join();
     * // or handle.detach();
     * \endcode
     */
    class ThreadPool
    {
    private:
        boost::asio::io_service io_service;
        boost::asio::io_service::work work;
        boost::thread_group threads;
        std::size_t available;
        std::mutex mutex;
        const bool queueTasks;
    public:

        class Handle
        {
        public:
            Handle();
            Handle(Handle&&) = default;
            Handle(std::shared_future<void> functionFinished);
            Handle& operator=(Handle &&) = default;
            ~Handle() noexcept(false);
            operator bool() const
            {
                return isValid();
            }
            bool isValid() const;
            void join();
            void detach();
            const std::shared_future<void>& getFuture() const;

            bool isDetached() const;
            bool isJoined() const;

        private:
            std::shared_ptr<std::mutex> mutex;
            std::shared_future<void> functionFinished;
            bool joined = false;
            bool detached = false;
        };

        /// @brief Constructor.
        ThreadPool(std::size_t pool_size, bool queueTasks = false);

        /// @brief Destructor.
        ~ThreadPool();

        /// @brief Adds a task to the thread pool if a thread is currently available.
        /// @return returns true if a thread is available, otherwise returns false.
        template < typename Task >
        Handle runTask(Task&& task)
        {
            std::unique_lock< std::mutex > lock(mutex);

            // If no threads are available, then return.
            if (!queueTasks && available == 0)
            {
                return {};
            }

            // Decrement count, indicating thread is no longer available.
            if (!queueTasks)
            {
                --available;
            }
            auto promise = std::make_shared<std::promise<void>>();
            // Post a wrapped task into the queue.
            io_service.post([this, task, promise]
            {
                wrap_task(task);
                promise->set_value();
            });
            return Handle {promise->get_future()};
        }

        /**
         * @brief If queing is disabled, returns the number of available threads. Otherwise returns -1;
         */
        int getAvailableTaskCount() const;

    private:
        /// @brief Wrap a task so that the available count can be increased once
        ///        the user provided task has completed.
        void wrap_task(std::function< void() > task);
    };


}
