/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarXCore::core
* @author     Nikolaus Vahrenkamp (vahrenkamp at kit dot edu)
* @date       2011
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <ArmarXCore/core/logging/Logging.h>  // for Logging
#include <ArmarXCore/core/system/ImportExport.h>
#include <Ice/CommunicatorF.h>          // for CommunicatorPtr
#include <Ice/Current.h>                // for Current
#include <Ice/Identity.h>               // for Identity
#include <Ice/ObjectAdapterF.h>         // for ObjectAdapterPtr
#include <Ice/ObjectF.h>                // for ObjectPtr
#include <Ice/Proxy.h>                  // for Object
#include <Ice/ProxyF.h>                 // for ObjectPrx
#include <IceGrid/Admin.h>              // for ObjectInfoSeq, AdminPrx, etc
#include <IceGrid/IceGrid.h>
#include <IceGrid/Observer.h>
#include <IceGrid/Registry.h>           // for RegistryPrx
#include <IceUtil/Handle.h>             // for HandleBase, Handle
#include <IceUtil/Time.h>               // for Time
#include <IceUtil/Timer.h>              // for TimerPtr, TimerTask
#include <boost/thread/pthread/mutex.hpp>  // for mutex
#include <map>                          // for map
#include <string>                       // for string
#include <vector>                       // for vector, vector<>::iterator

namespace boost
{
    template <class Y> class shared_ptr;
}  // namespace boost

namespace armarx
{
    class Component;
    class IceGridAdmin;

    /**
    * Typedef of IceUtil::Handle for convenience.
    */
    typedef IceUtil::Handle<IceGridAdmin> IceGridAdminPtr;

    /**
     * @class IceGridAdmin
     * @brief Provides simplified access to commonly used IceGrid features.
     * @ingroup DistributedProcessingSub
     *
     * It is possible to create an IceGridAdmin instance in your code in addition to the
     * default one used for dependency checking.
     * You will want to do this for registering your own IceGrid observers.
     * However, the following steps must be taken care of if you want your custom IceGridAdmin
     * instance to be destroyed.
     *
     * \li stop updating the IceGrid::AdminSession
    @code
    iceGridAdmin->stop();
    @endcode
     * \li remove installed observers so the IceGridAmin does not hold any more references to them
    @code
    iceGridAdmin->adminSession()->removeObservers((NULL, NULL, NULL, NULL, NULL)));
    @endcode
     * \li destroy the object adapters returned by IceGridManager::registerObjectWithNewAdapter() to remove the observer objects
    @code
    observerAdapter->destroy();
    @encode
     */
    class ARMARXCORE_IMPORT_EXPORT IceGridAdmin :
        virtual public IceUtil::TimerTask,
        virtual public IceGrid::NodeObserver,
        virtual public Logging
    {
    protected:
        /**
        * Create an IceGrid::AdminSession and starts a
        * keepAlive thread.
        *
        * @param c An Ice communicator.
        * @param name A unique name which is used to create IceGridObservers which must have unique names.
        * In case the name is not unique no state changed messages can be processed with this IceGridAdmin session.
        */
        IceGridAdmin(Ice::CommunicatorPtr c, std::string name);
        void init();
    public:
        static IceGridAdminPtr Create(Ice::CommunicatorPtr c, std::string name);
        ~IceGridAdmin() override;

        /**
        * Provides access to the IceGrid::AdminSession.
        *
        * @return A proxy of the IceGrid::AdminSession.
        */
        IceGrid::AdminSessionPrx adminSession();

        /**
        * Provides access to the IceGrid::Admin.
        *
        * @return A proxy of the IceGrid::Admin.
        */
        IceGrid::AdminPrx getAdmin();

        /**
         * Adds an application based on a path to an XML file
         *
         * @param xmlPath Path to XML configuration file for the application
         */
        void addApplication(const std::string& xmlPath);

        /**
         * Provides access to the IceGrid::Registry.
         * A proxy is created if necessary.
         *
         * @return A proxy of the IceGrid::Registy.
         */
        IceGrid::RegistryPrx registry();

        /**
         * @brief setObjectObserver changes the ObjectObserver to the instance passed as parameter
         * @param objObserverPrx
         */
        void setObjectObserver(IceGrid::ObjectObserverPrx objObserverPrx);

        /**
         * @brief removeObservers removes all observers set on this IceGridAdmin instance
         */
        void removeObservers();

        /**
        * Register an object with Ice for being accessed through IceGrid.
        * First, an Ice::ObjectAdapter is created, the \p object added to
        * it and then it is activated.
        * Afterwards a proxy to the \p object is created and returned
        *
        * @param object     The object to be registered, implementing an Ice
        *                   interface.
        * @param objectName The name this object should be available as.
        * @param objectAdapter instance of the newly created Ice::ObjectAdapter
        *
        * @return The registered object proxy.
        */
        Ice::ObjectPrx registerObjectWithNewAdapter(
            Ice::ObjectPtr object,
            const std::string& objectName,
            Ice::ObjectAdapterPtr& objectAdapter);

        /**
         * @brief stops the timertask which calls the keepAlive() method on the admin session
         */
        void stop();

        /**
        * Retrieve all well-known objects with name matching the regExp and of
        * proxy type given as template parameter
        *
        * @param regExp regular expression to use for filtering
        * @return list of object infos corresponding to all well-known objects registred in Ice Grid which match the ObjectType and the regExp
        */
        template <class ObjectType>
        IceGrid::ObjectInfoSeq getRegisteredObjects(std::string regExp = "*")
        {
            IceGrid::AdminPrx admin = getAdmin();
            IceGrid::ObjectInfoSeq objects = admin->getAllObjectInfos(regExp);
            IceGrid::ObjectInfoSeq::iterator iter = objects.begin();
            IceGrid::ObjectInfoSeq result;

            while (iter != objects.end())
            {
                Ice::ObjectPrx current = iter->proxy;

                ObjectType object;

                // if objects are hangig we might get connection refused
                try
                {
                    object = ObjectType::checkedCast(current->ice_timeout(60));
                }
                catch (...)
                {
                }

                if (object)
                {
                    result.push_back(*iter);
                }

                ++iter;
            }

            return result;
        }

        /**
        * Retrieve names of all well-known objects with name matching the regExp and of
        * proxy type given as template parameter
        *
        * @param regExp regular expression to use for filtering
        * @return list of object names corresponding to all well-known objects registred in Ice Grid which match the ObjectType and the regExp
        */
        template <class ObjectType>
        std::vector<std::string> getRegisteredObjectNames(std::string regExp = "*")
        {
            IceGrid::ObjectInfoSeq objects = getRegisteredObjects<ObjectType>(regExp);

            IceGrid::ObjectInfoSeq::iterator iter = objects.begin();
            std::vector<std::string> result;

            while (iter != objects.end())
            {
                result.push_back(iter->proxy->ice_getIdentity().name);
                ++iter;
            }

            return result;
        }

        /**
         * @brief Removes all dead objects from the registry.
         */
        void cleanUpDeadObjects();

        /**
        * Node Observer callbacks.
        */
        void nodeInit(const IceGrid::NodeDynamicInfoSeq& nodes, const Ice::Current& c = ::Ice::Current()) override;
        void nodeUp(const IceGrid::NodeDynamicInfo& node, const Ice::Current& c = ::Ice::Current()) override;
        void nodeDown(const std::string& name, const Ice::Current& c = ::Ice::Current()) override;
        void updateServer(const std::string& node, IceGrid::ServerDynamicInfo updatedInfo, const Ice::Current& c = ::Ice::Current());
        void updateAdapter(const std::string& node, IceGrid::AdapterDynamicInfo updatedInfo, const Ice::Current& c = ::Ice::Current());
        void updateServer(const ::std::string& node, const ::IceGrid::ServerDynamicInfo& updatedInfo, const ::Ice::Current& c = ::Ice::Current()) override;
        void updateAdapter(const ::std::string& node, const ::IceGrid::AdapterDynamicInfo& updatedInfo, const ::Ice::Current& c = ::Ice::Current()) override;

        enum ComponentState
        {
            eUnknown,
            eActivated,     // running
            eDeactivated    // stopped, crashed or not yet started
        };

        /**
        * Checks the state of a remote component.
        * Don't use this method in your control loop, since it may introduce some network overhead.
        * It's better to use callbacks for state updates (@see BaseComponent::requestComponentStateChangeNotification())
        *
        * @return The state of the component: If the component is running eActivated will be returned.
        */
        ComponentState getComponentState(std::string id);

    protected:
        /**
        * This method is executed periodically by IceGridAdmin::timer in
        * the frequency specified by IceGridAdmin::interval.
        */
        void runTimerTask() override;

        void printServerInfo(const IceGrid::ServerDynamicInfo& updatedInfo);
        void reportRemoteComponentStateChange(const IceGrid::ServerDynamicInfo& updatedInfo);
        //! All components that have registered for notification with this id are notified
        void notifyComponentChanged(ComponentState state, std::string id);
        void setObservers();
    private:
        Ice::CommunicatorPtr communicator;
        IceGrid::RegistryPrx registryProxy;
        IceGrid::AdminSessionPrx adminSessionProxy;
        Ice::ObjectAdapterPtr iceGridAdminAdapter;

        IceGrid::NodeObserverPrx iceGridAdminProxy;
        IceGrid::ObjectObserverPrx objObserverPrx;

        std::map<std::string, std::vector< boost::shared_ptr<Component> > > stateChangeNotifications;
        std::map<std::string, ComponentState> remoteComponentsState;

        std::string name;

        IceUtil::TimerPtr timer;
        IceUtil::Time interval;

        boost::mutex mutexComponentStateUpdate;

    };
}

