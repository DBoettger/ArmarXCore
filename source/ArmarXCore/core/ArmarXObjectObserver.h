/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2013
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <IceGrid/Observer.h>

namespace armarx
{
    class ArmarXManager;
    typedef IceUtil::Handle<ArmarXManager> ArmarXManagerPtr;

    /**
     * @brief The ArmarXObjectObserver inherits from IceGrid::ObjectObserver and signals its associated ArmarXManager instance when Ice Objects are added, updated, or removed.
     */
    class ArmarXObjectObserver :
        virtual public IceGrid::ObjectObserver
    {
    public:
        ArmarXObjectObserver(ArmarXManagerPtr armarxManager);

        // inherited from ObjectObserver
        void objectInit(const ::IceGrid::ObjectInfoSeq& objSeq, const ::Ice::Current& = ::Ice::Current()) override ;
        void objectAdded(const ::IceGrid::ObjectInfo& objInfo, const ::Ice::Current& = ::Ice::Current()) override ;
        void objectUpdated(const ::IceGrid::ObjectInfo& objInfo, const ::Ice::Current& = ::Ice::Current()) override ;
        void objectRemoved(const ::Ice::Identity& objIdentity, const ::Ice::Current& = ::Ice::Current()) override ;

    private:
        ArmarXManagerPtr armarxManager;
    };

    typedef IceInternal::Handle<ArmarXObjectObserver> ArmarXObjectObserverPtr;
}

