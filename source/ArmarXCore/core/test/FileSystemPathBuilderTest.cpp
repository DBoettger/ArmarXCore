/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core::Test
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#define BOOST_TEST_MODULE ArmarX::Core::FileSystemPathBuilder
#define ARMARX_BOOST_TEST
#include <ArmarXCore/Test.h>
#include <ArmarXCore/core/test/IceTestHelper.h>

#include<ArmarXCore/core/util/FileSystemPathBuilder.h>

using namespace armarx;

#include <iostream>
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>

BOOST_AUTO_TEST_CASE(testFileSystemPathBuilderTest)
{

    std::string rawPath = "{LEFT CURLY}{SourceDir:ArmarXCore}/core/test/FileSystemPathBuilderTest.cpp{RIGHT CURLY}";

    FileSystemPathBuilder b {rawPath};
    std::string str = "{" + boost::filesystem::canonical(__FILE__).string() + "}";

    BOOST_CHECK_EQUAL(str, b.getPath());
}


