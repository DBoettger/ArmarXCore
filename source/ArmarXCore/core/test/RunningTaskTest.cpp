/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarXCore::core::Test
* @author     Nils Adermann (naderman at naderman dot de)
* @date       2010
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#define BOOST_TEST_MODULE ArmarX::Core::RunningTask
#define ARMARX_BOOST_TEST
#include <ArmarXCore/Test.h>
#include <ArmarXCore/core/services/tasks/ThreadList.h>
#include <ArmarXCore/core/test/IceTestHelper.h>
#include <ArmarXCore/core/application/Application.h>
#include <ArmarXCore/core/services/tasks/RunningTask.h>
#include <ArmarXCore/core/Component.h>

#include <iostream>

using namespace armarx;


class TestComponent
{
public:
    TestComponent()
    {
        runningTask = new RunningTask<TestComponent>(this, &TestComponent::runTimerTask, "TestRunningTask");
        runningTask2 = new RunningTask<TestComponent>(this, &TestComponent::runTimerTask2, "TestRunningTask2");
        runningTask->start();
        runningTask2->start();
    }

    void runTimerTask()
    {
        runningTask2->stop();
    }

    void runTimerTask2()
    {
        ARMARX_INFO_S << "runTimerTask2 running";
        usleep(300000);
    }

    RunningTask<TestComponent>::pointer_type runningTask;
    RunningTask<TestComponent>::pointer_type runningTask2;
};

BOOST_AUTO_TEST_CASE(testStopRunningTask)
{
    //    //RunningTask<TestComponent>::pointer_type runningTask = new RunningTask<TestComponent>(new TestComponent(), &TestComponent::runTimerTask, "TestRunningTask" );
    //    TestComponent comp;
    //    IceUtil::Time start = IceUtil::Time::now();
    //    while(!comp.runningTask2->isRunning() && (IceUtil::Time::now() - start).toSeconds() < 2)
    //    {
    //        usleep(100);
    //    }
    //    BOOST_CHECK((IceUtil::Time::now() - start).toSeconds() < 2);
    //    comp.runningTask2->stop();


}

class TestComponent2 : public ManagedIceObject
{
public:
    TestComponent2()
    {
        number = 3;
        runningTask = new RunningTask<TestComponent2>(this, &TestComponent2::runTimerTask, "TestRunningTask");


    }
    ~TestComponent2()
    {
        std::cout << "destr" << std::endl;
        runningTask->stop();
        std::cout << "destr DONE" << std::endl;
    }
    std::string getDefaultName() const
    {
        return "TestComponent2";
    }
    void onInitComponent()
    {
    }

    void onConnectComponent()
    {
        std::cout << "connect" << std::endl;
        runningTask->start();

    }

    void runTimerTask()
    {
        usleep(1000000);
        std::cout << number  << tag.tagName << std::endl;

    }
    int number;


    RunningTask<TestComponent2>::pointer_type runningTask;

};

class TestApp : public Application
{
public:
    void setup(const ManagedIceObjectRegistryInterfacePtr& registry, Ice::PropertiesPtr properties)
    {
        registry->addObject(ManagedIceObject::create<TestComponent2>());
    }

};


BOOST_AUTO_TEST_CASE(testStopRunningTask2)
{
    //RunningTask<TestComponent>::pointer_type runningTask = new RunningTask<TestComponent>(new TestComponent(), &TestComponent::runTimerTask, "TestRunningTask" );
    ApplicationPtr app =  Application::createInstance<TestApp>();
    app->run(0, NULL);

    IceUtil::Time start = IceUtil::Time::now();
    usleep(100000);
    BOOST_CHECK((IceUtil::Time::now() - start).toSeconds() < 2);



}

