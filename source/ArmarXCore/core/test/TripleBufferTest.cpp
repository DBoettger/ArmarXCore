/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core::Test
 * @author     Raphael ( ufdrv at student dot kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#define BOOST_TEST_MODULE ArmarX::Core::TripleBuffer

#define ARMARX_BOOST_TEST

#include <thread>
#include <iostream>

#include <ArmarXCore/Test.h>
#include <ArmarXCore/core/util/TripleBuffer.h>

class IntrospectTripleBuffer: public armarx::TripleBuffer<int>
{
public:
    IntrospectTripleBuffer() = default;
    ~IntrospectTripleBuffer() = default;

    uint_fast8_t r()   const
    {
        return TripleBuffer<int>::getReadBufferIndex();
    }
    uint_fast8_t w()  const
    {
        return TripleBuffer<int>::getWriteBufferIndex();
    }
    uint_fast8_t h() const
    {
        return TripleBuffer<int>::getHiddenBufferIndex();
    }
    std::set<uint_fast8_t> indexset()
    {
        return std::set<uint_fast8_t> {r(), w(), h()};
    }
};

static constexpr std::size_t THREAD_NUM = 8;
static constexpr std::size_t LOOP_NUM   = 100000;
static constexpr std::size_t CHECK_NUM  = 10;

static const std::set<uint_fast8_t> indices {0, 1, 2};

BOOST_AUTO_TEST_CASE(TripleBufferIndexTest)
{
    std::thread t[THREAD_NUM];
    IntrospectTripleBuffer instance;

    BOOST_CHECK(indices == instance.indexset());

    auto checks = CHECK_NUM;
    while (checks--)
    {
        for (std::size_t i = 0; i < THREAD_NUM; ++ i)
        {
            t[i] = std::thread {[&]{
                    auto loops = LOOP_NUM;
                    while (loops--)
                    {
                        instance.commitWrite();
                        instance.updateReadBuffer();
                    }
                }
            };
        }
        for (std::size_t i = 0; i < THREAD_NUM; ++ i)
        {
            t[i].join();
        }
        BOOST_CHECK(indices == instance.indexset());
    }
}


static constexpr std::size_t INSERT_NUM   = 10000;

BOOST_AUTO_TEST_CASE(WriteBufferedTripleBufferTest)
{
    armarx::WriteBufferedTripleBuffer<std::vector<int>> instance;

    for (std::size_t i = 0; i < INSERT_NUM; ++ i)
    {
        instance.getWriteBuffer().emplace_back(i);
        instance.commitWrite();
    }
    BOOST_CHECK(instance.updateReadBuffer());
    BOOST_CHECK(instance.getReadBuffer().size() == INSERT_NUM);
}

