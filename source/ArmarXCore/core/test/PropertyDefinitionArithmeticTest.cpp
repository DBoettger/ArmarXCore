
/**
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarXCore::core::Test
* @author     Jan Issac (jan dot issac at gmx dot de)
* @date       2011
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#define BOOST_TEST_MODULE ArmarX::Core::PropertyDefinitionArithmetic
#define ARMARX_BOOST_TEST
#include <ArmarXCore/Test.h>

#include "PropertyFixtures.h"

// ArmarXCore
#include <ArmarXCore/core/application/properties/Properties.h>
#include <ArmarXCore/core/Component.h>

// Ice
#include <Ice/Ice.h>

#include <string>
#include <iostream>
#include <limits>

using namespace armarx;

class MyPropertyDefinitions: public ComponentPropertyDefinitions
{
public:
    MyPropertyDefinitions(std::string identifier):
        ComponentPropertyDefinitions(identifier)
    {
        defineOptionalProperty<char>("i-am-optional:char", 42, "Desc: ...").setMin(0).setMax(60);
        defineOptionalProperty<unsigned char>("i-am-optional:uchar", 42, "Desc: ...").setMin(0).setMax(60);
        defineOptionalProperty<int>("i-am-optional:int", 42, "Desc: ...").setMin(0).setMax(60);
        defineOptionalProperty<unsigned int>("i-am-optional:uint", 42, "Desc: ...").setMin(0u).setMax(60u);
        defineOptionalProperty<float>("i-am-optional:float", 42, "Desc: ...").setMin(0).setMax(60);
        defineOptionalProperty<double>("i-am-optional:double", 42, "Desc: ...").setMin(0).setMax(60);

        defineRequiredProperty<char>("i-am-required:char", "Desc: ...").setMin(0).setMax(60);
        defineRequiredProperty<unsigned char>("i-am-required:uchar", "Desc: ...").setMin(0u).setMax(60u);
        defineRequiredProperty<int>("i-am-required:int", "Desc: ...").setMin(0).setMax(60);
        defineRequiredProperty<unsigned int>("i-am-required:uint", "Desc: ...").setMin(0u).setMax(60u);
        defineRequiredProperty<float>("i-am-required:float", "Desc: ...").setMin(0).setMax(60);
        defineRequiredProperty<double>("i-am-required:double", "Desc: ...").setMin(0).setMax(60);
    }
};


PropertyDefinitionsPtr ComponentPrototype::createPropertyDefinition()
{
    return PropertyDefinitionsPtr(new MyPropertyDefinitions("ArmarX"));
}


struct DefinitionFixture
{
    DefinitionFixture()
    {
        defs = component.getPropertyDefinitions();
    }

    ComponentPrototype component;
    PropertyDefinitionsPtr defs;
};


BOOST_AUTO_TEST_SUITE(PropertyDefinitions)


BOOST_FIXTURE_TEST_CASE(DefinitionMinMaxTest,  DefinitionFixture)
{
    BOOST_CHECK_EQUAL(defs->getDefintion<char>("i-am-optional:char").getMin(), 0);
    BOOST_CHECK_EQUAL(defs->getDefintion<char>("i-am-optional:char").getMax(), 60);
    BOOST_CHECK_EQUAL(defs->getDefintion<unsigned char>("i-am-optional:uchar").getMin(), 0);
    BOOST_CHECK_EQUAL(defs->getDefintion<unsigned char>("i-am-optional:uchar").getMax(), 60);
    BOOST_CHECK_EQUAL(defs->getDefintion<int>("i-am-optional:int").getMin(), 0);
    BOOST_CHECK_EQUAL(defs->getDefintion<int>("i-am-optional:int").getMax(), 60);
    BOOST_CHECK_EQUAL(defs->getDefintion<unsigned int>("i-am-optional:uint").getMin(), 0u);
    BOOST_CHECK_EQUAL(defs->getDefintion<unsigned int>("i-am-optional:uint").getMax(), 60u);
    BOOST_CHECK_EQUAL(defs->getDefintion<float>("i-am-optional:float").getMin(), 0);
    BOOST_CHECK_EQUAL(defs->getDefintion<float>("i-am-optional:float").getMax(), 60);
    BOOST_CHECK_EQUAL(defs->getDefintion<double>("i-am-optional:double").getMin(), 0);
    BOOST_CHECK_EQUAL(defs->getDefintion<double>("i-am-optional:double").getMax(), 60);

    BOOST_CHECK_EQUAL(defs->getDefintion<char>("i-am-required:char").getMin(), 0);
    BOOST_CHECK_EQUAL(defs->getDefintion<char>("i-am-required:char").getMax(), 60);
    BOOST_CHECK_EQUAL(defs->getDefintion<unsigned char>("i-am-required:uchar").getMin(), 0u);
    BOOST_CHECK_EQUAL(defs->getDefintion<unsigned char>("i-am-required:uchar").getMax(), 60u);
    BOOST_CHECK_EQUAL(defs->getDefintion<int>("i-am-required:int").getMin(), 0);
    BOOST_CHECK_EQUAL(defs->getDefintion<int>("i-am-required:int").getMax(), 60);
    BOOST_CHECK_EQUAL(defs->getDefintion<unsigned int>("i-am-required:uint").getMin(), 0u);
    BOOST_CHECK_EQUAL(defs->getDefintion<unsigned int>("i-am-required:uint").getMax(), 60u);
    BOOST_CHECK_EQUAL(defs->getDefintion<float>("i-am-required:float").getMin(), 0);
    BOOST_CHECK_EQUAL(defs->getDefintion<float>("i-am-required:float").getMax(), 60);
    BOOST_CHECK_EQUAL(defs->getDefintion<double>("i-am-required:double").getMin(), 0);
    BOOST_CHECK_EQUAL(defs->getDefintion<double>("i-am-required:double").getMax(), 60);
}

BOOST_FIXTURE_TEST_CASE(DefinitionMinNegaiveOverflowTest,  DefinitionFixture)
{
    BOOST_CHECK_THROW
    (
        defs->defineRequiredProperty<char>("i-am-required:char").setMin(-129),
        boost::numeric::negative_overflow
    );

    BOOST_CHECK_THROW
    (
        defs->defineRequiredProperty<unsigned char>("i-am-required:uchar").setMin(-1),
        boost::numeric::negative_overflow
    );

    BOOST_CHECK_THROW
    (
        defs->defineRequiredProperty<int>("i-am-required:int").setMin(std::numeric_limits<long long>::min()),
        boost::numeric::negative_overflow
    );

    BOOST_CHECK_THROW
    (
        defs->defineRequiredProperty<unsigned int>("i-am-required:uint").setMin(-1),
        boost::numeric::negative_overflow
    );
}

BOOST_FIXTURE_TEST_CASE(DefinitionMinPositiveOverflowTest,  DefinitionFixture)
{
    BOOST_CHECK_THROW
    (
        defs->defineRequiredProperty<char>("i-am-required:char").setMin(129),
        boost::numeric::positive_overflow
    );

    BOOST_CHECK_THROW
    (
        defs->defineRequiredProperty<unsigned char>("i-am-required:uchar").setMin(256),
        boost::numeric::positive_overflow
    );

    BOOST_CHECK_THROW
    (
        defs->defineRequiredProperty<int>("i-am-required:int").setMin(std::numeric_limits<long long>::max()),
        boost::numeric::positive_overflow
    );

    BOOST_CHECK_THROW
    (
        defs->defineRequiredProperty<unsigned int>("i-am-required:uint").setMin(std::numeric_limits<long long>::max()),
        boost::numeric::positive_overflow
    );
}


BOOST_FIXTURE_TEST_CASE(DefinitionMaxNegaiveOverflowTest,  DefinitionFixture)
{
    BOOST_CHECK_THROW
    (
        defs->defineRequiredProperty<char>("i-am-required:char").setMax(-129),
        boost::numeric::negative_overflow
    );

    BOOST_CHECK_THROW
    (
        defs->defineRequiredProperty<unsigned char>("i-am-required:uchar").setMax(-1),
        boost::numeric::negative_overflow
    );

    BOOST_CHECK_THROW
    (
        defs->defineRequiredProperty<int>("i-am-required:int").setMax(std::numeric_limits<long long>::min()),
        boost::numeric::negative_overflow
    );

    BOOST_CHECK_THROW
    (
        defs->defineRequiredProperty<unsigned int>("i-am-required:uint").setMax(-1),
        boost::numeric::negative_overflow
    );
}


BOOST_FIXTURE_TEST_CASE(DefinitionMaxPositiveOverflowTest,  DefinitionFixture)
{
    BOOST_CHECK_THROW
    (
        defs->defineRequiredProperty<char>("i-am-required:char").setMax(129),
        boost::numeric::positive_overflow
    );

    BOOST_CHECK_THROW
    (
        defs->defineRequiredProperty<unsigned char>("i-am-required:uchar").setMax(256),
        boost::numeric::positive_overflow
    );

    BOOST_CHECK_THROW
    (
        defs->defineRequiredProperty<int>("i-am-required:int").setMax(std::numeric_limits<long long>::max()),
        boost::numeric::positive_overflow
    );

    BOOST_CHECK_THROW
    (
        defs->defineRequiredProperty<unsigned int>("i-am-required:uint").setMax(std::numeric_limits<long long>::max()),
        boost::numeric::positive_overflow
    );
}


BOOST_AUTO_TEST_SUITE_END(/* PropertyDefinitions */)


