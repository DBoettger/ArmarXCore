/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarXCore::core::Test
* @author     Jan Issac (jan dot issac at gmx dot de)
* @date       2011
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#define BOOST_TEST_MODULE ArmarX::Core::PropertyDefinitionMapping
#define ARMARX_BOOST_TEST
#include <ArmarXCore/Test.h>

#include "PropertyFixtures.h"

// ArmarXCore
#include <ArmarXCore/core/application/properties/Properties.h>
#include <ArmarXCore/core/Component.h>

// Ice
#include <Ice/Ice.h>

#include <string>
#include <iostream>
#include <limits>

using namespace armarx;

class MyPropertyDefinitions: public ComponentPropertyDefinitions
{
public:
    MyPropertyDefinitions(std::string identifier):
        ComponentPropertyDefinitions(identifier)
    {
        defineRequiredProperty<std::string>("prop:mapped:string", "Desc: ...")
        .map("one", "1")
        .map("two", "2")
        .map("three", "3");

        defineRequiredProperty<int>("prop:mapped:int", "Desc: ...")
        .map("one", 1)
        .map("two", 2)
        .map("three", 3);

        defineRequiredProperty<float>("prop:mapped:float", "Desc: ...")
        .map("one", 1.f)
        .map("two", 2.f)
        .map("three", 3.f);

        defineRequiredProperty<TestTypes::EnumType>("prop:mapped:EnumType", "Desc: ...")
        .map("one", TestTypes::One)
        .map("two", TestTypes::Two)
        .map("three", TestTypes::Three);

        defineRequiredProperty<TestTypes::StructType>("prop:mapped:StructType", "Desc: ...")
        .map("one", TestTypes::StructType(1, 1, "1"))
        .map("two", TestTypes::StructType(2, 2, "2"))
        .map("three", TestTypes::StructType(3, 3, "3"));

        defineRequiredProperty<TestTypes::ClassType>("prop:mapped:ClassType", "Desc: ...")
        .map("one", TestTypes::ClassType(1, 1, "1"))
        .map("two", TestTypes::ClassType(2, 2, "2"))
        .map("three", TestTypes::ClassType(3, 3, "3"));
    }
};


PropertyDefinitionsPtr ComponentPrototype::createPropertyDefinition()
{
    return PropertyDefinitionsPtr(new MyPropertyDefinitions("ArmarX"));
}


struct DefinitionFixture
{
    DefinitionFixture()
    {
        defs = component.getPropertyDefinitions();

        keyValues = new std::string[3];
        keyValues[0] = "one";
        keyValues[1] = "two";
        keyValues[2] = "three";
    }

    ~DefinitionFixture()
    {
        delete [] keyValues;
    }

    ComponentPrototype component;
    PropertyDefinitionsPtr defs;
    std::string* keyValues;
};


BOOST_AUTO_TEST_SUITE(PropertyDefinitions)


BOOST_FIXTURE_TEST_CASE(DefinitionMappingSearchTest,  DefinitionFixture)
{
    for (int i = 0; i < 3; i++)
    {
        {
            PropertyDefinition<std::string>::PropertyValuesMap valueMap
                = defs->getDefintion<std::string>(
                      "prop:mapped:string").getValueMap();
            BOOST_CHECK(valueMap.find(keyValues[i]) != valueMap.end());
        }

        {
            PropertyDefinition<int>::PropertyValuesMap valueMap
                = defs->getDefintion<int>(
                      "prop:mapped:int").getValueMap();
            BOOST_CHECK(valueMap.find(keyValues[i]) != valueMap.end());
        }

        {
            PropertyDefinition<float>::PropertyValuesMap valueMap
                = defs->getDefintion<float>(
                      "prop:mapped:float").getValueMap();
            BOOST_CHECK(valueMap.find(keyValues[i]) != valueMap.end());
        }

        {
            PropertyDefinition<TestTypes::EnumType>::PropertyValuesMap valueMap
                = defs->getDefintion<TestTypes::EnumType>(
                      "prop:mapped:EnumType").getValueMap();
            BOOST_CHECK(valueMap.find(keyValues[i]) != valueMap.end());
        }

        {
            PropertyDefinition<TestTypes::ClassType>::PropertyValuesMap valueMap
                = defs->getDefintion<TestTypes::ClassType>(
                      "prop:mapped:ClassType").getValueMap();
            BOOST_CHECK(valueMap.find(keyValues[i]) != valueMap.end());
        }
    }
}


BOOST_AUTO_TEST_SUITE_END(/* PropertyDefinitions */)



