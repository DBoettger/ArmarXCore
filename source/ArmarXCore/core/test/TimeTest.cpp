/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarXCore::core::Test
* @author     Nils Adermann (naderman at naderman dot de)
* @date       2010
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#define BOOST_TEST_MODULE ArmarX::Core::Time
#define ARMARX_BOOST_TEST
#include <ArmarXCore/Test.h>

#include <ArmarXCore/core/time/CycleUtil.h>
#include <ArmarXCore/core/logging/Logging.h>

BOOST_AUTO_TEST_CASE(CycleUtilTest)
{

    armarx::CycleUtil c(10);
    for (int i = 0; i < 100; ++i)
    {
        ARMARX_INFO << i << " waitTime: " << c.waitForCycleDuration().toMilliSecondsDouble() << std::endl;

    }
    auto  duration = (c.getLastCycleTime() - c.getStartTime()).toMilliSeconds();
    std::cout << VAROUT(duration) << std::endl;
    BOOST_CHECK(duration >= 10 * 100);
    BOOST_CHECK(duration <= 10 * 100 * 1.1);
    BOOST_CHECK_EQUAL(100, c.getCycleCount());
}





