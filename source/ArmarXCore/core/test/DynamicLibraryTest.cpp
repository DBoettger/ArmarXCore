/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarXCore::core::Test
* @author     Mirko Waechter (waechter @ kit . edu)
* @date       2010
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#define BOOST_TEST_MODULE ArmarX::Core::DynamicLibrary
#define ARMARX_BOOST_TEST
#include <ArmarXCore/Test.h>
#include <ArmarXCore/core/system/ArmarXDataPath.h>
#include <ArmarXCore/core/system/DynamicLibrary.h>
#include <ArmarXCore/core/system/cmake/CMakePackageFinder.h>

#include <iostream>

using namespace armarx;


typedef bool (*boolFunc)();
typedef std::string(*FunctionType)(int);
BOOST_AUTO_TEST_CASE(testDynamicLibrary)
{
    try
    {
        std::string relPath = "libArmarXCore.so";
        std::string path;
        CMakePackageFinder package("ArmarXCore");

        if (!ArmarXDataPath::getAbsolutePath(relPath, path, {package.getLibraryPaths()}))
            return;
        DynamicLibrary lib;
        BOOST_CHECK(!lib.isLibraryLoaded());
        lib.load(path);
        BOOST_CHECK(lib.isLibraryLoaded());

        BOOST_CHECK_THROW(lib.getSymbol<FunctionType>("armarx::LogSender::levelToString"), exceptions::local::DynamicLibraryException);

        lib.unload();
        BOOST_CHECK(!lib.isLibraryLoaded());
        BOOST_CHECK_THROW(lib.load(path + "xxx"), exceptions::local::DynamicLibraryException);
        BOOST_CHECK(!lib.isLibraryLoaded());
    }
    catch (...)
    {

    }

}

BOOST_AUTO_TEST_CASE(DynamicLibraryExample)
{
    try
    {
        //! [DynamicLibrary DynamicLibraryUsage]
        std::string relPath = "libArmarXCore.so";
        std::string path;
        CMakePackageFinder package("ArmarXCore");

        if (!ArmarXDataPath::getAbsolutePath(relPath, path, {package.getLibraryPaths()}))
            return;
        ARMARX_INFO_S << "Loading lib from " << path;
        DynamicLibrary lib;
        // Load the library
        lib.load(path);

        if (!lib.isLibraryLoaded())
        {
            throw LocalException("Could not load library ") << path;
        }

        // create typedef for the function
        typedef void (*FunctionType)();

        // retrieve function from library
        FunctionType handleExceptionsFunc = lib.getSymbol<FunctionType>("handleExceptions");

        // the function can be called now
        try
        {
            throw LocalException("Test Exception");
        }
        catch (...)
        {
            handleExceptionsFunc();
        }

        // Unload lib (also done in destructor of DynamicLibrary)
        lib.unload();
        //! [DynamicLibrary DynamicLibraryUsage]
    }
    catch (...)
    {

    }

}

