/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarXCore::core::Test
* @author     Jan Issac (jan dot issac at gmx dot de)
* @date       2011
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#define BOOST_TEST_MODULE ArmarX::Core::Property
#define ARMARX_BOOST_TEST
#include <ArmarXCore/Test.h>

// ArmarXCore
#include <ArmarXCore/core/Property.h>

// Ice
#include <Ice/Ice.h>

#include <string>
#include <iostream>

struct PropertiesFixture
{
    PropertiesFixture()
    {
        properties = Ice::createProperties();
    }

    ~PropertiesFixture()
    {
    }

    Ice::PropertiesPtr properties;
};

/* ========================================================================= */
/* == */ BOOST_AUTO_TEST_SUITE(StringsValueMapping)   /* =================== */
/* ========================================================================= */
BOOST_FIXTURE_TEST_CASE(StringValueMapping, PropertiesFixture)
{
    properties->setProperty("permutation", "B");
    std::string permutation = armarx::Property<std::string>("permutation", properties, "A")
                              .map("A", "B")
                              .map("B", "C")
                              .map("C", "A")
                              .getValue();
    BOOST_CHECK(permutation == "C");
}

BOOST_FIXTURE_TEST_CASE(RawStringRetrieval, PropertiesFixture)
{
    properties->setProperty("awesomeproperty", "awesomevalue");
    std::string awesomeproperty = armarx::Property<std::string>("awesomeproperty", properties, "myvalue")
                                  .map("awesomevalue-altered", "awesomevalue-extended")
                                  .setBypassMapping(true)
                                  .getValue();
    BOOST_CHECK_EQUAL(awesomeproperty, "awesomevalue");
}
BOOST_AUTO_TEST_SUITE_END(/* StringValueMapping */)

/* ========================================================================= */
/* == */ BOOST_AUTO_TEST_SUITE(MappingStatus)   /* =================== */
/* ========================================================================= */
BOOST_FIXTURE_TEST_CASE(MappingDoneIndicator, PropertiesFixture)
{
    armarx::Property<std::string> awesomemapper =  armarx::Property<std::string>("awesomeproperty", properties, "myvalue")
            .map("superb", "awesome")
            .doneMapping();
    BOOST_CHECK(awesomemapper.isMappingDone());
}

BOOST_FIXTURE_TEST_CASE(MappingNotDoneIndicator, PropertiesFixture)
{
    armarx::Property<std::string> awesomemapper =  armarx::Property<std::string>("awesomeproperty", properties, "myvalue")
            .map("superb", "awesome");
    BOOST_CHECK(!awesomemapper.isMappingDone());
}
BOOST_AUTO_TEST_SUITE_END(/* MappingStatus */)


/* ========================================================================= */
/* == */ BOOST_AUTO_TEST_SUITE(PropertyRequirement)   /* =================== */
/* ========================================================================= */
BOOST_FIXTURE_TEST_CASE(RequirementSetIndicator, PropertiesFixture)
{
    armarx::Property<std::string> awesomemapper =  armarx::Property<std::string>("awesomeproperty", properties, "myvalue")
            .setRequired(true);
    BOOST_CHECK(awesomemapper.isRequired());
}

BOOST_FIXTURE_TEST_CASE(RequirementNotSetIndicator, PropertiesFixture)
{
    armarx::Property<std::string> awesomemapper =  armarx::Property<std::string>("awesomeproperty", properties, "myvalue")
            .setRequired(false);
    BOOST_CHECK(!awesomemapper.isRequired());
}

BOOST_FIXTURE_TEST_CASE(RequiredPropertyNoThrow, PropertiesFixture)
{
    properties->setProperty("i_am_required", "10");

    BOOST_CHECK_NO_THROW
    (
        armarx::Property<std::string>("i_am_required", properties, "myvalue")
        .setRequired(true)
        .getValue()
    );

    BOOST_CHECK_NO_THROW
    (
        armarx::Property<int>("i_am_required", properties, 0)
        .map("10", 0)
        .setRequired(true)
        .getValue()
    );

    BOOST_CHECK_NO_THROW
    (
        armarx::Property<int>("i_am_required", properties, 0)
        .setRequired(true)
        .getValue()
    );
}

BOOST_FIXTURE_TEST_CASE(NotRequiredPropertyNoThrow, PropertiesFixture)
{
    // Note: "i_am_required" is not set in properties

    BOOST_CHECK_NO_THROW
    (
        armarx::Property<std::string>("i_am_not_required", properties, "myvalue")
        .setRequired(false)
        .getValue()
    );
}

BOOST_FIXTURE_TEST_CASE(MissingRequiredPropertyThrow, PropertiesFixture)
{
    // Note: "i_am_required" is not set in properties

    BOOST_CHECK_THROW
    (
        armarx::Property<std::string>("i_am_required", properties, "myvalue")
        .setRequired(true)
        .getValue(),
        armarx::exceptions::local::MissingRequiredPropertyException
    );

    BOOST_CHECK_THROW
    (
        armarx::Property<int>("i_am_required", properties, 0)
        .map("10", 0)
        .setRequired(true)
        .getValue(),
        armarx::exceptions::local::MissingRequiredPropertyException
    );

    BOOST_CHECK_THROW
    (
        armarx::Property<int>("i_am_required", properties, 0)
        .setRequired(true)
        .getValue(),
        armarx::exceptions::local::MissingRequiredPropertyException
    );
}
BOOST_AUTO_TEST_SUITE_END(/* PropertyRequirement */)




/* ========================================================================= */
/* == */ BOOST_AUTO_TEST_SUITE(DefaultValueFallback)   /* ================== */
/* ========================================================================= */
BOOST_FIXTURE_TEST_CASE(DefaultFallbackOnUndefined, PropertiesFixture)
{
    std::string notsetPropStr;
    int notsetPropInt;

    BOOST_CHECK_NO_THROW
    (
        notsetPropStr = armarx::Property<std::string>("im_not_set", properties, "my_default_value")
                        .map("one", "two")
                        .getValueOrDefault();
    );
    BOOST_CHECK_EQUAL(notsetPropStr, "my_default_value");

    BOOST_CHECK_NO_THROW
    (
        notsetPropStr = armarx::Property<std::string>("im_not_set", properties, "my_default_value")
                        .map("one", "two")
                        .getValueOrDefault();
    );
    BOOST_CHECK_EQUAL(notsetPropStr, "my_default_value");

    BOOST_CHECK_NO_THROW
    (
        notsetPropInt = armarx::Property<int>("im_not_set", properties, -500)
                        .map("1", 1)
                        .map("2", 2)
                        .getValueOrDefault();
    );
    BOOST_CHECK_EQUAL(notsetPropInt, -500);
}

BOOST_FIXTURE_TEST_CASE(DefaultFallbackOnUnmapped, PropertiesFixture)
{
    properties->setProperty("im_set", "10");

    std::string notsetPropStr;
    int notsetPropInt;

    BOOST_CHECK_NO_THROW
    (
        notsetPropStr = armarx::Property<std::string>("im_set", properties, "my_default_value")
                        .map("one", "two")
                        .getValueOrDefault();
    );
    BOOST_CHECK_EQUAL(notsetPropStr, "my_default_value");

    BOOST_CHECK_NO_THROW
    (
        notsetPropInt = armarx::Property<int>("im_not_set", properties, -500)
                        .map("1", 1)
                        .map("2", 2)
                        .getValueOrDefault();
    );
    BOOST_CHECK_EQUAL(notsetPropInt, -500);


    // TODO: move or create a bypass mapping test case
    BOOST_CHECK_NO_THROW
    (
        notsetPropInt = armarx::Property<int>("im_set", properties, -500)
                        .map("1", 1)
                        .map("2", 2)
                        .setBypassMapping(true)
                        .getValue();
    );
    BOOST_CHECK_EQUAL(notsetPropInt, 10);
}
BOOST_AUTO_TEST_SUITE_END(/* DefaultValueFallback */)




/* ========================================================================= */
/* == */ BOOST_AUTO_TEST_SUITE(MappingCheck)   /* ================== */
/* ========================================================================= */
BOOST_FIXTURE_TEST_CASE(NoThrowOnRetrievalOfMappedValues, PropertiesFixture)
{
    properties->setProperty("myProperty", "10");

    std::string myPropertyStr;

    BOOST_CHECK_NO_THROW
    (
        myPropertyStr = armarx::Property<std::string>("myProperty", properties, "my_default_value")
                        .map("10", "ten")
                        .getValue();
    );
    BOOST_CHECK_EQUAL(myPropertyStr, "ten");
}

BOOST_FIXTURE_TEST_CASE(ThrowOnRetrievalOfUnmappedValues, PropertiesFixture)
{
    properties->setProperty("myProperty", "10");

    BOOST_CHECK_THROW
    (
        armarx::Property<std::string>("myProperty", properties, "my_default_value")
        .map("1", "one")
        .getValue(),
        armarx::exceptions::local::UnmappedValueException
    );
}
BOOST_AUTO_TEST_SUITE_END(/* MappingCheck */)




/* ========================================================================= */
/* == */ BOOST_AUTO_TEST_SUITE(RegularExpressionCheck)   /* ================== */
/* ========================================================================= */
BOOST_FIXTURE_TEST_CASE(NoThrowOnValidValue, PropertiesFixture)
{
    properties->setProperty("device-mac", "0a:b1:2c:d3:4e:f5");
    properties->setProperty("app-socket", "192.168.0.1:3221");

    std::string mac;
    int socket;

    BOOST_CHECK_NO_THROW
    (
        mac = armarx::Property<std::string>("device-mac", properties, "00:00:00:00:00:00")
              .setMatchRegex("([0-9a-fA-F]{2})(:[0-9a-fA-F]{2}){5}")
              .map("0a:b1:2c:d3:4e:f5", "0a:b1:2c:d3:4e:f5:00:00:00:00:00:00:00:00:00:00:00:00")
              .getValue();
    );
    BOOST_CHECK_EQUAL(mac, "0a:b1:2c:d3:4e:f5:00:00:00:00:00:00:00:00:00:00:00:00");


    BOOST_CHECK_NO_THROW
    (
        socket = armarx::Property<int>("app-socket", properties, 0)
                 .setMatchRegex("([0-9]{1,3})(.[0-9]{1,3}){3}:([0-9]{1,5})")
                 .map("192.168.0.1:3221", 13221)
                 .getValue();
    );
    BOOST_CHECK_EQUAL(socket, 13221);
}

BOOST_FIXTURE_TEST_CASE(ThrowOnValidValue, PropertiesFixture)
{
    properties->setProperty("device-mac", "0a;b1:2c:d3:4e:f5");
    properties->setProperty("app-socket", "1920.168.0.1:3221");

    std::string mac = "unset";
    int socket = -1;

    BOOST_CHECK_THROW
    (
        mac = armarx::Property<std::string>("device-mac", properties, "00:00:00:00:00:00")
              .setMatchRegex("([0-9a-fA-F]{2})(:[0-9a-fA-F]{2}){5}")
              .map("0a:b1:2c:d3:4e:f5", "0a:b1:2c:d3:4e:f5:00:00:00:00:00:00:00:00:00:00:00:00")
              .getValue(),
        armarx::exceptions::local::InvalidPropertyValueException
    );
    BOOST_CHECK_EQUAL(mac, "unset");


    BOOST_CHECK_THROW
    (
        socket = armarx::Property<int>("app-socket", properties, 0)
                 .setMatchRegex("([0-9]{1,3})(.[0-9]{1,3}){3}:([0-9]{1,5})")
                 .map("192.168.0.1:3221", 13221)
                 .getValue(),
        armarx::exceptions::local::InvalidPropertyValueException
    );
    BOOST_CHECK_EQUAL(socket, -1);
}
BOOST_AUTO_TEST_SUITE_END(/* RegularExpressionCheck */)




/* ========================================================================= */
/* == */ BOOST_AUTO_TEST_SUITE(NumericLimitsCheck)   /* ================== */
/* ========================================================================= */
BOOST_FIXTURE_TEST_CASE(NoThrowOnRetrievalOfInBoundShortValue, PropertiesFixture)
{
    short shortValue;
    properties->setProperty("shortValue", "50");
    BOOST_CHECK_NO_THROW
    (
        shortValue = armarx::Property<short>("shortValue", properties, 0)
                     .setMin(-100)
                     .setMax(100)
                     .getValue();
    )
    BOOST_CHECK_EQUAL(shortValue, 50);
}

BOOST_FIXTURE_TEST_CASE(NoThrowOnRetrievalOfInBoundUnsignedShortValue, PropertiesFixture)
{
    unsigned short unsignedShortValue;
    properties->setProperty("unsignedShortValue", "50");
    BOOST_CHECK_NO_THROW
    (
        unsignedShortValue = armarx::Property<unsigned short>("unsignedShortValue", properties, 0)
                             .setMin(10)
                             .setMax(100)
                             .getValue();
    )
    BOOST_CHECK_EQUAL(unsignedShortValue, 50);
}

BOOST_FIXTURE_TEST_CASE(NoThrowOnRetrievalOfInBoundIntValue, PropertiesFixture)
{
    int intValue;
    properties->setProperty("intValue", "-50");
    BOOST_CHECK_NO_THROW
    (
        intValue = armarx::Property<int>("intValue", properties, 0)
                   .setMin(-100)
                   .setMax(100)
                   .getValue();
    )
    BOOST_CHECK_EQUAL(intValue, -50);
}

BOOST_FIXTURE_TEST_CASE(NoThrowOnRetrievalOfInBoundUnsignedIntValue, PropertiesFixture)
{
    unsigned int unsignedIntValue;
    properties->setProperty("unsignedIntValue", "50");
    BOOST_CHECK_NO_THROW
    (
        unsignedIntValue = armarx::Property<unsigned int>("unsignedIntValue", properties, 0)
                           .setMin(10)
                           .setMax(100)
                           .getValue();
    )
    BOOST_CHECK_EQUAL(unsignedIntValue, 50u);
}

BOOST_FIXTURE_TEST_CASE(NoThrowOnRetrievalOfInBoundLongValue, PropertiesFixture)
{
    long longValue;
    properties->setProperty("longValue", "-50");
    BOOST_CHECK_NO_THROW
    (
        longValue = armarx::Property<long>("longValue", properties, 0)
                    .setMin(-100)
                    .setMax(100)
                    .getValue();
    )
    BOOST_CHECK_EQUAL(longValue, -50l);
}

BOOST_FIXTURE_TEST_CASE(NoThrowOnRetrievalOfInBoundUnsignedLongValue, PropertiesFixture)
{
    unsigned long unsignedLongValue;
    properties->setProperty("unsignedLongValue", "60");
    BOOST_CHECK_NO_THROW
    (
        unsignedLongValue = armarx::Property<unsigned long>("unsignedLongValue", properties, 50)
                            .setMin(50)
                            .setMax(100000)
                            .getValue();
    )
    BOOST_CHECK_EQUAL(unsignedLongValue, 60ul);
}

BOOST_FIXTURE_TEST_CASE(NoThrowOnRetrievalOfInBoundFloatValue, PropertiesFixture)
{
    float floatValue;
    properties->setProperty("floatValue", "60.0");
    BOOST_CHECK_NO_THROW
    (
        floatValue = armarx::Property<float>("floatValue", properties, 50.f)
                     .setMin(50.f)
                     .setMax(100000.f)
                     .getValue();
    )
    BOOST_CHECK_EQUAL(floatValue, 60.f);
}

BOOST_FIXTURE_TEST_CASE(NoThrowOnRetrievalOfInBoundUnsignedDoubleValue, PropertiesFixture)
{
    double doubleValue;
    properties->setProperty("doubleValue", "60.0");
    BOOST_CHECK_NO_THROW
    (
        doubleValue = armarx::Property<double>("doubleValue", properties, 50.0)
                      .setMin(50.)
                      .setMax(100000.)
                      .getValue();
    )
    BOOST_CHECK_EQUAL(doubleValue, 60.);
}





BOOST_FIXTURE_TEST_CASE(ThrowOnRetrievalOfInBoundShortValue, PropertiesFixture)
{
    short shortValue;
    properties->setProperty("shortValue", "500");
    BOOST_CHECK_THROW
    (
        shortValue = armarx::Property<short>("shortValue", properties, 0)
                     .setMin(-100)
                     .setMax(100)
                     .getValue(),
        armarx::exceptions::local::ValueRangeExceededException
    )
}

BOOST_FIXTURE_TEST_CASE(ThrowOnRetrievalOfInBoundUnsignedShortValue, PropertiesFixture)
{
    unsigned short unsignedShortValue;
    properties->setProperty("unsignedShortValue", "500");
    BOOST_CHECK_THROW
    (
        unsignedShortValue = armarx::Property<unsigned short>("unsignedShortValue", properties, 0)
                             .setMin(10)
                             .setMax(100)
                             .getValue(),
        armarx::exceptions::local::ValueRangeExceededException
    )
}

BOOST_FIXTURE_TEST_CASE(ThrowOnRetrievalOfInBoundIntValue, PropertiesFixture)
{
    int intValue;
    properties->setProperty("intValue", "-500");
    BOOST_CHECK_THROW
    (
        intValue = armarx::Property<int>("intValue", properties, 0)
                   .setMin(-100)
                   .setMax(100)
                   .getValue(),
        armarx::exceptions::local::ValueRangeExceededException
    )
}

BOOST_FIXTURE_TEST_CASE(ThrowOnRetrievalOfInBoundUnsignedIntValue, PropertiesFixture)
{
    unsigned int unsignedIntValue;
    properties->setProperty("unsignedIntValue", "500");
    BOOST_CHECK_THROW
    (
        unsignedIntValue = armarx::Property<unsigned int>("unsignedIntValue", properties, 0)
                           .setMin(10)
                           .setMax(100)
                           .getValue(),
        armarx::exceptions::local::ValueRangeExceededException
    )
}

BOOST_FIXTURE_TEST_CASE(ThrowOnRetrievalOfInBoundLongValue, PropertiesFixture)
{
    long longValue;
    properties->setProperty("longValue", "-500");
    BOOST_CHECK_THROW
    (
        longValue = armarx::Property<long>("longValue", properties, 0)
                    .setMin(-100)
                    .setMax(100)
                    .getValue(),
        armarx::exceptions::local::ValueRangeExceededException
    )
}

BOOST_FIXTURE_TEST_CASE(ThrowOnRetrievalOfInBoundUnsignedLongValue, PropertiesFixture)
{
    unsigned long unsignedLongValue;
    properties->setProperty("unsignedLongValue", "40");
    BOOST_CHECK_THROW
    (
        unsignedLongValue = armarx::Property<unsigned long>("unsignedLongValue", properties, 50)
                            .setMin(50)
                            .setMax(100000)
                            .getValue(),
        armarx::exceptions::local::ValueRangeExceededException
    )
}

BOOST_FIXTURE_TEST_CASE(ThrowOnRetrievalOfInBoundFloatValue, PropertiesFixture)
{
    float floatValue;
    properties->setProperty("floatValue", "40.0");
    BOOST_CHECK_THROW
    (
        floatValue = armarx::Property<float>("floatValue", properties, 50.f)
                     .setMin(50.f)
                     .setMax(100000.f)
                     .getValue(),
        armarx::exceptions::local::ValueRangeExceededException
    )
}

BOOST_FIXTURE_TEST_CASE(ThrowOnRetrievalOfInBoundUnsignedDoubleValue, PropertiesFixture)
{
    double doubleValue;
    properties->setProperty("doubleValue", "100001.0");
    BOOST_CHECK_THROW
    (
        doubleValue = armarx::Property<double>("doubleValue", properties, 50.0)
                      .setMin(50.)
                      .setMax(100000.)
                      .getValue(),
        armarx::exceptions::local::ValueRangeExceededException
    )
}
BOOST_AUTO_TEST_SUITE_END(/* NumericLimitsCheck */)




/* ========================================================================= */
/* == */ BOOST_AUTO_TEST_SUITE(CaseSensitivity)   /* ================== */
/* ========================================================================= */
BOOST_FIXTURE_TEST_CASE(NoThrowMatchingCaseSetBeforeMapping, PropertiesFixture)
{
    properties->setProperty("device-mac", "0A:b1:2C:d3:4E:f5");

    std::string mac;

    BOOST_CHECK_NO_THROW
    (
        mac = armarx::Property<std::string>("device-mac", properties, "00:00:00:00:00:00")
              .setCaseInsensitive(false)
              .map("0A:b1:2C:d3:4E:f5", "0a:b1:2c:d3:4e:f5:00:00:00:00:00:00:00:00:00:00:00:00")
              .getValue();
    );
    BOOST_CHECK_EQUAL(mac, "0a:b1:2c:d3:4e:f5:00:00:00:00:00:00:00:00:00:00:00:00");
}
BOOST_FIXTURE_TEST_CASE(NoThrowMatchingCaseSetAfterMapping, PropertiesFixture)
{
    properties->setProperty("device-mac", "0A:b1:2C:d3:4E:f5");

    std::string mac;

    BOOST_CHECK_NO_THROW
    (
        mac = armarx::Property<std::string>("device-mac", properties, "00:00:00:00:00:00")
              .map("0A:b1:2C:d3:4E:f5", "0a:b1:2c:d3:4e:f5:00:00:00:00:00:00:00:00:00:00:00:00")
              .setCaseInsensitive(false)
              .getValue();
    );
    BOOST_CHECK_EQUAL(mac, "0a:b1:2c:d3:4e:f5:00:00:00:00:00:00:00:00:00:00:00:00");
}

BOOST_FIXTURE_TEST_CASE(ThrowOnNotMatchingCaseSetBeforeMapping, PropertiesFixture)
{
    properties->setProperty("device-mac", "0A:b1:2C:d3:4E:f5");

    std::string mac;

    BOOST_CHECK_THROW
    (
        armarx::Property<std::string>("device-mac", properties, "00:00:00:00:00:00")
        .setCaseInsensitive(false)
        .map("0a:b1:2c:d3:4e:f5", "0a:b1:2c:d3:4e:f5:00:00:00:00:00:00:00:00:00:00:00:00")
        .getValue(),
        armarx::exceptions::local::UnmappedValueException
    )
}
BOOST_FIXTURE_TEST_CASE(ThrowOnNotMatchingCaseSetAfterMapping, PropertiesFixture)
{
    properties->setProperty("device-mac", "0A:b1:2C:d3:4E:f5");

    std::string mac;

    BOOST_CHECK_THROW
    (
        armarx::Property<std::string>("device-mac", properties, "00:00:00:00:00:00")
        .map("0a:b1:2c:d3:4e:f5", "0a:b1:2c:d3:4e:f5:00:00:00:00:00:00:00:00:00:00:00:00")
        .setCaseInsensitive(false)
        .getValue(),
        armarx::exceptions::local::UnmappedValueException
    )
}
BOOST_AUTO_TEST_SUITE_END(/* NumericLimitsCheck */)



/* ========================================================================= */
/* == */ BOOST_AUTO_TEST_SUITE(PrimitiveDataTypeValueMapping)   /* ========= */
/* ========================================================================= */
BOOST_FIXTURE_TEST_CASE(BoolPropertyValueMapping, PropertiesFixture)
{
    properties->setProperty("booleanValue", "true");
    bool booleanValue = armarx::Property<bool>("booleanValue", properties, false)
                        .map("true", true)
                        .map("false", false)
                        .getValue();
    BOOST_CHECK(booleanValue == true);
}

BOOST_FIXTURE_TEST_CASE(CharPropertyValueMapping, PropertiesFixture)
{
    properties->setProperty("charValue", "127");
    char charValue = armarx::Property<char>("charValue", properties, 0)
                     .map("127", 127)
                     .map("0", 0)
                     .map("-128", char(-127))
                     .getValue();
    BOOST_CHECK(charValue == 127);
}

BOOST_FIXTURE_TEST_CASE(UnsignedCharPropertyValueMapping, PropertiesFixture)
{
    properties->setProperty("unsignedCharValue", "255");
    unsigned char unsignedCharValue = armarx::Property<unsigned char>("unsignedCharValue", properties, 0)
                                      .map("255", 255)
                                      .map("0", 0)
                                      .getValue();
    BOOST_CHECK(unsignedCharValue == 255);
}

BOOST_FIXTURE_TEST_CASE(ShortPropertyValueMapping, PropertiesFixture)
{
    properties->setProperty("shortValue", "32767");
    short shortValue = armarx::Property<short>("shortValue", properties, 0)
                       .map("32767", 32767)
                       .map("0", 0)
                       .map("-32767", 32767)
                       .getValue();
    BOOST_CHECK(shortValue == 32767);
}

BOOST_FIXTURE_TEST_CASE(UnsignedShortPropertyValueMapping, PropertiesFixture)
{
    properties->setProperty("unsignedShortValue", "65535");
    unsigned short unsignedShortValue = armarx::Property<unsigned short>("unsignedShortValue", properties, 0)
                                        .map("65535", 65535)
                                        .map("0", 0)
                                        .getValue();
    BOOST_CHECK(unsignedShortValue == 65535);
}

BOOST_FIXTURE_TEST_CASE(IntPropertyValueMapping, PropertiesFixture)
{
    properties->setProperty("intValue", "-32767");
    int intValue = armarx::Property<int>("intValue", properties, 0)
                   .map("32767", 32767)
                   .map("0", 0)
                   .map("-32767", -32767)
                   .getValue();
    BOOST_CHECK(intValue == -32767);
}

BOOST_FIXTURE_TEST_CASE(UnsignedIntPropertyValueMapping, PropertiesFixture)
{
    properties->setProperty("unsignedIntValue", "65535");
    unsigned int unsignedIntValue = armarx::Property<unsigned int>("unsignedIntValue", properties, 0)
                                    .map("65535", 65535)
                                    .map("0", 0)
                                    .getValue();
    BOOST_CHECK(unsignedIntValue == 65535);
}

BOOST_FIXTURE_TEST_CASE(LongPropertyValueMapping, PropertiesFixture)
{
    properties->setProperty("longValue", "-2147483647");
    long longValue = armarx::Property<long>("longValue", properties, 0)
                     .map("2147483647", 2147483647)
                     .map("0", 0)
                     .map("-2147483647", -2147483647)
                     .getValue();
    BOOST_CHECK(longValue == -2147483647);
}

BOOST_FIXTURE_TEST_CASE(UnsignedLongPropertyValueMapping, PropertiesFixture)
{
    properties->setProperty("unsignedLongValue", "2147483647");
    unsigned long unsignedLongValue = armarx::Property<unsigned long>("unsignedLongValue", properties, 0)
                                      .map("2147483647", 2147483647)
                                      .map("0", 0)
                                      .getValue();
    BOOST_CHECK(unsignedLongValue == 2147483647);
}

BOOST_FIXTURE_TEST_CASE(FloatPropertyValueMapping, PropertiesFixture)
{
    properties->setProperty("floatValue", "123456.789");
    float floatValue = armarx::Property<float>("floatValue", properties, 0.f)
                       .map("123456.789", 123456.789f)
                       .map("0", 0.f)
                       .map("-123456.789", -123456.789f)
                       .getValue();
    BOOST_CHECK(floatValue == 123456.789f);
}

BOOST_FIXTURE_TEST_CASE(DoublePropertyValueMapping, PropertiesFixture)
{
    properties->setProperty("doubleValue", "123456.789");
    double doubleValue = armarx::Property<double>("doubleValue", properties, 0.0)
                         .map("123456.789", 123456.789)
                         .map("0", 0.f)
                         .map("-123456.789", -123456.789)
                         .getValue();
    BOOST_CHECK(doubleValue == 123456.789);
}
BOOST_AUTO_TEST_SUITE_END(/* PrimitiveDataTypeValueMapping */)
