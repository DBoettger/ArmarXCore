/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarXCore::core::Test
* @author     Nils Adermann (naderman at naderman dot de)
* @date       2010
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#define BOOST_TEST_MODULE ArmarX::Core::PeriodicTask
#define ARMARX_BOOST_TEST
#include <ArmarXCore/Test.h>
#include <ArmarXCore/core/services/tasks/ThreadList.h>
#include <ArmarXCore/core/test/IceTestHelper.h>
#include <ArmarXCore/core/application/Application.h>
#include <ArmarXCore/core/services/tasks/PeriodicTask.h>
#include <ArmarXCore/core/services/tasks/TaskUtil.h>
#include <ArmarXCore/core/Component.h>

#include <iostream>

using namespace armarx;

class TestApp : public Application
{
public:
    void setup(const ManagedIceObjectRegistryInterfacePtr& registry, Ice::PropertiesPtr properties) override {}

};

class TestComponent : public Component
{
public:
    void onInitComponent() override {}
    void onConnectComponent() override {}
    std::string getDefaultName() const override
    {
        return "TestComponent";
    }
    void runTimerTask()
    {
        ARMARX_INFO << "Test";

    }

};

BOOST_AUTO_TEST_CASE(testChangePeriodicTaskInterval)
{
    PeriodicTask<TestComponent>::pointer_type periodicTask = new PeriodicTask<TestComponent>(new TestComponent(), &TestComponent::runTimerTask, 10, false, "TestPeriodicTask");

    periodicTask->start();
    usleep(30000);

    BOOST_CHECK_EQUAL(10, periodicTask->getInterval());
    periodicTask->changeInterval(20);
    BOOST_CHECK_EQUAL(20, periodicTask->getInterval());
    usleep(50000);
    //    BOOST_CHECK_EQUAL(true,periodicTask->lastCycleDuration > 10);


    periodicTask->stop();


}


BOOST_AUTO_TEST_CASE(testReducePeriodicTaskInterval)
{

    PeriodicTask<TestComponent>::pointer_type periodicTask = new PeriodicTask<TestComponent>(new TestComponent(), &TestComponent::runTimerTask, 1000, false, "TestPeriodicTask");

    periodicTask->start();
    usleep(300000);

    BOOST_CHECK_EQUAL(1000, periodicTask->getInterval());
    periodicTask->changeInterval(20);
    BOOST_CHECK_EQUAL(20, periodicTask->getInterval());
    usleep(50000);
    const PeriodicTaskIceBase taskBase = periodicTask->getStatistics();
    int timeSinceLastExecStart = (IceUtil::Time::now() - IceUtil::Time::microSeconds(taskBase.lastCycleStartTime)).toMilliSeconds();
    ARMARX_VERBOSE_S << "timeSinceLastExecStart: " << timeSinceLastExecStart;
    BOOST_CHECK_EQUAL(true, timeSinceLastExecStart < 100);


    periodicTask->stop();
}

BOOST_AUTO_TEST_CASE(testLambdaPeriodicTask)
{

    SimplePeriodicTask<>::pointer_type periodicTask = new SimplePeriodicTask<>([&] { ARMARX_INFO << "Async output";}, 10, false, "TestPeriodicTask");

    periodicTask->start();
    usleep(300000);

    const PeriodicTaskIceBase taskBase = periodicTask->getStatistics();
    int timeSinceLastExecStart = (IceUtil::Time::now() - IceUtil::Time::microSeconds(taskBase.lastCycleStartTime)).toMilliSeconds();
    ARMARX_VERBOSE_S << "timeSinceLastExecStart: " << timeSinceLastExecStart;
    BOOST_CHECK_EQUAL(true, timeSinceLastExecStart < 100);


    periodicTask->stop();


}

