/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Nils Adermann (naderman at naderman dot de)
 * @date       2010
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

namespace armarx
{
#ifdef WIN32
#    pragma warning ( disable : 4251 )
#    if defined(ArmarXComponent_EXPORTS)
#        define ARMARXCOMPONENT_IMPORT_EXPORT __declspec(dllexport)
#  else
#    define ARMARXCOMPONENT_IMPORT_EXPORT __declspec(dllimport)
#  endif
#else
#  define ARMARXCOMPONENT_IMPORT_EXPORT
#endif
}


