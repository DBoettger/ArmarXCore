/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarXCore::core
* @author     Nikolaus Vahrenkamp
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include <ArmarXCore/core/application/Application.h>
#include <ArmarXCore/core/logging/Logging.h>  // for ARMARX_WARNING_S, etc
#include <ArmarXCore/core/system/cmake/CMakePackageFinder.h>
#include <ArmarXCore/core/util/StringHelpers.h>  // for Contains, VAROUT
#include <IceUtil/Handle.h>             // for HandleBase
#include <boost/algorithm/string/classification.hpp>  // for is_any_of
#include <boost/algorithm/string/detail/classification.hpp>
#include <boost/algorithm/string/replace.hpp>  // for replace_all
#include <boost/algorithm/string/split.hpp>  // for split
#include <boost/filesystem/operations.hpp>  // for exists, is_symlink, etc
#include <boost/filesystem/path.hpp>    // for path, path::iterator, etc
#include <boost/filesystem/path_traits.hpp>  // for filesystem
#include <boost/iterator/iterator_facade.hpp>  // for iterator_facade, etc
#include <boost/range/iterator_range_core.hpp>  // for operator==
#include <boost/regex/config.hpp>       // for get_mem_block, etc
#include <boost/regex/v4/basic_regex.hpp>  // for basic_regex
#include <boost/regex/v4/cpp_regex_traits.hpp>
#include <boost/regex/v4/match_flags.hpp>  // for operator|=, operator&
#include <boost/regex/v4/match_results.hpp>  // for match_results
#include <boost/regex/v4/perl_matcher_common.hpp>
#include <boost/regex/v4/perl_matcher_non_recursive.hpp>
#include <boost/regex/v4/regex_format.hpp>
#include <boost/regex/v4/regex_fwd.hpp>  // for regex
#include <boost/regex/v4/regex_replace.hpp>  // for regex_replace
#include <boost/regex/v4/regex_search.hpp>  // for regex_search
#include <boost/regex/v4/sub_match.hpp>  // for sub_match
#include <boost/smart_ptr/shared_ptr.hpp>  // for shared_ptr
#include <stddef.h>                     // for size_t
#include <algorithm>                    // for min, fill, find
#include <cstdlib>                      // for getenv
#include <sstream>                      // for operator<<, char_traits, etc
#include <string>                       // for basic_string, string, etc
#include <vector>                       // for vector

#include "ArmarXCore/core/application/properties/Property.h"
#include "ArmarXCore/core/exceptions/Exception.h"  // for LocalException
#include "ArmarXCore/core/logging/LogSender.h"  // for LogSender
#include "ArmarXDataPath.h"             // for ArmarXDataPath

using namespace armarx;


namespace fs = boost::filesystem;
std::vector<std::string> armarx::ArmarXDataPath::dataPaths;
bool armarx::ArmarXDataPath::initialized = false;


ArmarXDataPath::ArmarXDataPath()
{}


bool ArmarXDataPath::getAbsolutePath(
        const std::string& relativeFilename,
        std::string& storeAbsoluteFilename,
        const std::vector<std::string>& additionalSearchPaths,
        bool verbose)
{
    init();

    boost::filesystem::path filename(relativeFilename);

    // first: check current path
    if (boost::filesystem::exists(filename))
    {
        storeAbsoluteFilename = filename.string();
        return true;
    }

    std::vector<std::string> paths(additionalSearchPaths.begin(), additionalSearchPaths.end());
    paths.insert(paths.begin(), dataPaths.rbegin(), dataPaths.rend());

    for (const auto& currentPath : paths)
    {
        boost::filesystem::path path(currentPath);

        boost::filesystem::path filenameComplete = path / filename;

        if (boost::filesystem::exists(filenameComplete))
        {
            storeAbsoluteFilename = filenameComplete.string();
            return true;
        }
    }

    if (verbose)
    {
        ARMARX_WARNING_S << "Could not find file '" << relativeFilename
                         << "' in the following paths: " << paths;
    }

    return false;
}

std::string ArmarXDataPath::getAbsolutePath(
        const std::string& relativeFilename,
        const std::vector<std::string>& additionalSearchPaths,
        bool verbose)
{
    std::string storeAbsoluteFilename;
    getAbsolutePath(relativeFilename, storeAbsoluteFilename, additionalSearchPaths, verbose);
    return storeAbsoluteFilename;
}

std::string ArmarXDataPath::getProject(const std::vector<std::string>& projects, const std::string& relativeFilename)
{
    boost::filesystem::path fn(relativeFilename);
    for (auto currentProject : projects)
    {
        armarx::CMakePackageFinder finder(currentProject);

        if (!finder.packageFound())
        {
            ARMARX_WARNING_S << "ArmarX Package " << currentProject << " has not been found!";
            continue;
        }

        //ARMARX_INFO_S << "Checking datapath: " << finder.getDataDir();
        boost::filesystem::path p(finder.getDataDir());
        boost::filesystem::path fnComplete = boost::filesystem::operator/(p, fn);

        if (boost::filesystem::exists(fnComplete))
        {
            //ARMARX_INFO_S << "Found in project " << currentProject;
            return currentProject;
        }
    }

    return std::string();
}


std::string ArmarXDataPath::cleanPath(const std::string& filepathStr)
{
    boost::filesystem::path p(filepathStr);
    boost::filesystem::path result;

    for (boost::filesystem::path::iterator it = p.begin();
         it != p.end();
         ++it)
    {
        if (*it == ".." && it != p.begin())
        {
            // /a/b/.. is not necessarily /a if b is a symbolic link
            bool isSymLink = false;

            try
            {
                isSymLink = boost::filesystem::is_symlink(result);
            }
            catch (boost::filesystem::filesystem_error&) {}

            if (isSymLink)
            {
                result /= *it;
            }
            // /a/b/../.. is not /a/b/.. under most circumstances
            // We can end up with ..s in our result because of symbolic links
            else if (result.filename() == "..")
            {
                result /= *it;
            }
            // Otherwise it should be safe to resolve the parent
            else
            {
                result = result.parent_path();
            }
        }
        else if (*it == ".")
        {
            // Ignore
        }
        else
        {
            // Just concatenate other path entries
            result /= *it;
        }
    }

    if (*filepathStr.rbegin() == '/')
    {
        return result.string() + "/";
    }
    else if (*filepathStr.rbegin() == '\\')
    {
        return result.string() + "\\";
    }

    return result.string();
}

std::string ArmarXDataPath::getRelativeArmarXPath(const std::string& absolutePathString)
{
    boost::filesystem::path absolutePath(absolutePathString);
    boost::filesystem::path absolutePathPart(absolutePathString);
    boost::filesystem::path relativePath;

    boost::filesystem::path::iterator it = --absolutePath.end();

    while (it != absolutePath.begin())
    {
        for (size_t i = 0; i < dataPaths.size(); i ++)
        {
            boost::filesystem::path p(dataPaths[i]);

            if (boost::filesystem::equivalent(p, absolutePathPart))
            {
                return relativePath.string();
            }
        }

        relativePath = *it / relativePath;
        absolutePathPart = absolutePathPart.parent_path();
        it--;

    }

    throw LocalException("Could not make path relative to any Armarx data path");
    return absolutePathString;
}

std::string ArmarXDataPath::relativeTo(const std::string& fromStr, const std::string& toStr)
{
    // Start at the root path. While they are the same, do nothing.
    // When they first diverge, take the remainder of the two paths
    // and replace the entire from path with ".." segments.
    fs::path from(fromStr);
    fs::path to(toStr);
    fs::path::const_iterator fromIter = (from).begin();
    fs::path::const_iterator toIter = (to).begin();
    if (from.empty())
    {
        throw LocalException("From path is empty");
    }
    if (to.empty())
    {
        throw LocalException("To path is empty");
    }
    if (*fromIter != *toIter)
    {
        throw LocalException("From and to path do not have the same toplevel dir: ") << VAROUT(fromStr) << VAROUT(toStr);
    }
    // Loop through both
    while (fromIter != from.end() && toIter != to.end() && (*toIter) == (*fromIter))
    {
        ++toIter;
        ++fromIter;
    }

    fs::path finalPath;
    while (fromIter != from.end())
    {
        finalPath /= "..";
        ++fromIter;
    }

    while (toIter != to.end())
    {
        finalPath /= *toIter;
        ++toIter;
    }

    return finalPath.string();
}

bool ArmarXDataPath::mergePaths(std::string pathStr, std::string subPathStr, std::string& result)
{
    fs::path subPath(subPathStr);
    pathStr = cleanPath(pathStr);
    subPathStr = cleanPath(subPathStr);
    fs::path strippedPath;
    while (!subPath.empty())
    {
        if (Contains(pathStr, subPath.string()))
        {
            result = (fs::path(pathStr) / strippedPath).string();
            return true;
        }
        strippedPath = subPath.leaf() / strippedPath;
        subPath = subPath.parent_path();
    }
    result.clear();
    return false;
}

void ArmarXDataPath::initDataPaths(const std::string& dataPathList)
{
    addDataPaths(dataPathList);

    // check for standard armarx data path
    if (getHomePath() != "")
    {
        boost::filesystem::path homePath(getHomePath());
        homePath /= "data";
        boost::filesystem::path fn(homePath);

        if (boost::filesystem::exists(fn))
        {
            __addPath(homePath.string());
        }
    }
}

bool ArmarXDataPath::ReplaceEnvVars(std::string& string)
{
    const boost::regex e("\\$([a-zA-Z0-9_]+)");
    const boost::regex e2("\\$\\{([a-zA-Z0-9_]+)\\}");
    boost::match_results<std::string::const_iterator> what;
    bool found = false;
    auto replaceVars = [&](const boost::regex & e)
    {
        bool found_match = boost::regex_search(string, what, e);
        if (found_match)
        {
            for (size_t i = 1; i < what.size(); i += 2)
            {
                std::string var = what[i];

                auto envVar = getenv(var.c_str());
                if (envVar)
                {
                    string = boost::regex_replace(string, e, std::string(envVar));
                    found = true;
                }
            }
        }
    };
    replaceVars(e);
    replaceVars(e2);
    return found;
}

void ArmarXDataPath::ReplaceVar(std::string& string, const std::string varName, const std::string& varValue)
{
    boost::replace_all(string, std::string("${") + varName + "}", varValue);
}

void ArmarXDataPath::ResolveHomePath(std::string& path)
{
    ReplaceEnvVars(path);
    if (!path.empty() && path[0] == '~')
    {
        path = path.erase(0, 1);
        auto envVar = getenv("HOME");
        if (envVar)
        {
            path = std::string(envVar) + "/" + path;
        }
        else
        {
            ARMARX_WARNING << "$HOME var is not set!";
        }
    }
    path = cleanPath(path);
}

void ArmarXDataPath::addDataPaths(const std::string& dataPathList)
{
    init();
    __addPaths(dataPathList);
}

std::string ArmarXDataPath::getHomePath()
{
    char* home_path = getenv("ArmarXHome_DIR");

    if (!home_path)
    {
        return std::string();
    }

    return cleanPath(std::string(home_path));
}

void ArmarXDataPath::init()
{
    if (initialized)
    {
        return;
    }

    std::string homePath = getHomePath();

    if (!homePath.empty())
    {
        __addPaths(homePath);
    }

    char* data_path = getenv("ArmarXData_DIRS");

    if (data_path)
    {
        std::string pathStr(data_path);
        __addPaths(pathStr);
    }

    //check the documented variable
    char* data_path_dir = getenv("ArmarXData_DIR");

    if (data_path_dir)
    {
        std::string pathStr(data_path_dir);
        __addPaths(pathStr);
    }

    initialized = true;
}

std::vector<std::string> ArmarXDataPath::getDataPaths()
{
    return dataPaths;
}

bool ArmarXDataPath::__addPaths(const std::string& pathList)
{
    if (pathList == "")
    {
        return false;
    }

    std::vector<std::string> separatedPaths = __separatePaths(pathList);

    if (separatedPaths.size() == 0)
    {
        return false;
    }

    bool ok = true;

    for (int i = separatedPaths.size() - 1; i >= 0 ; i--)
    {
        ArmarXDataPath::ReplaceEnvVars(separatedPaths[i]);
        ok = ok & __addPath(separatedPaths[i]);
    }

    return ok;
}

std::vector<std::string> ArmarXDataPath::__separatePaths(const std::string& pathList)
{
    std::string delimiters = ";";
    std::vector<std::string> tokens;
    boost::split(tokens, pathList, boost::is_any_of(delimiters));
    return tokens;
}


bool ArmarXDataPath::__addPath(const std::string& path)
{
    if (path.empty())
    {
        return false;
    }

    boost::filesystem::path p(path);

    if (!boost::filesystem::is_directory(p) && !boost::filesystem::is_symlink(p))
    {
        ARMARX_WARNING_S << "Not a valid data path:" << path << std::endl;
        return false;
    }
    else if (std::find(dataPaths.begin(), dataPaths.end(), path) == dataPaths.end())
    {
        ARMARX_DEBUG_S << "Adding data path:" << path << std::endl;
        dataPaths.push_back(path);
    }

    return true;
}

std::string ArmarXDataPath::GetCachePath()
{
    try
    {
        ApplicationPtr application = armarx::Application::getInstance();
        if (application.get() != nullptr)
        {

            std::string cachePathStr;

            cachePathStr = application->getProperty<std::string>("CachePath").getValue();
            if (boost::filesystem::path(cachePathStr).is_relative())
            {
                std::string pathPrefix;
                if (getenv(Application::ArmarXUserConfigDirEnvVar.c_str()))
                {
                    pathPrefix = std::string(getenv(Application::ArmarXUserConfigDirEnvVar.c_str()));
                }
                else
                {
                    pathPrefix = Application::GetArmarXConfigDefaultPath();
                }
                //                ARMARX_INFO << VAROUT(pathPrefix);
                cachePathStr = (boost::filesystem::path(pathPrefix) / boost::filesystem::path(cachePathStr)).string();
            }
            else
            {
                //                ARMARX_INFO << "Cache path is absolute: " << cachePathStr;
            }

            ReplaceEnvVars(cachePathStr);
            return cachePathStr;
        }
        else
        {
            return "";
        }
    }
    catch (LocalException& error)
    {
        return "";
    }
}
