/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore
 * @author     Kai Welke (welke at kit dot edu)
 * @date       2013
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <boost/thread.hpp>
#include <boost/thread/recursive_mutex.hpp>

namespace armarx
{

    /**
     * @class ConditionSynchronization
     * @ingroup Threads
     * Encapsules the synchronization members for multiple purposes.
     * The synchronization uses a single condition, protected by a mutex.
     * Typical usage is to transfer data from one thread to another
     * with a signal (producer / consumer).
     *
     * Example usage in producer / consumer:
     * @code
     *   ConditionSynchronization sync;
     *   int sharedData;
     *
     *   int consumer_thread()
     *   {
     *     while(true)
     *     {
     *       sync.lockNotify();
     *       sharedData = rand();
     *       sync.notify();
     *       sync.unlockNotify();
     *     }
     *
     *     consumerSync.abort();
     *   }
     *
     *   int producer_thread()
     *   {
     *     bool abort = false;
     *     while(!abort)
     *     {
     *       consumerSync.lockWait();
     *       abort = consumerSync.wait();
     *       std::cout << "Got " << sharedData << std::endl
     *       consumerSync.unlockWait();
     *     }
     *   }
     * @endcode
     **/
    class ConditionSynchronization
    {
    public:
        /**
         * Initializes all synchronization objects
         **/
        ConditionSynchronization()
        {
            reset();

            // create two locks, do not lock
            condLockWait = boost::unique_lock<boost::mutex>(condMutex, boost::defer_lock_t());
            condLockNotify = boost::unique_lock<boost::mutex>(condMutex, boost::defer_lock_t());
        }

        void reset()
        {
            // work around spurious wakeups
            condition = false;
            abortRequested = false;
        }

        /**
         * Locks the mutex for waiting. Use this before calling ConditionSynchronization::wait().
         **/
        void lockWait()
        {
            condLockWait.lock();
        }

        /**
         * Unlocks the mutex after waiting. Use this after calling ConditionSynchronization::wait().
         **/
        void unlockWait()
        {
            condLockWait.unlock();
        }

        /**
         * Locks the mutex for notifying. Use this before calling ConditionSynchronization::notify().
         **/
        void lockNotify()
        {
            condLockNotify.lock();
        }

        /**
         * Unlocks the mutex after notifying. Use this after calling ConditionSynchronization::notify().
         **/
        void unlockNotify()
        {
            condLockNotify.unlock();
        }

        /**
         * Notifies the condition
         **/
        void notify()
        {
            condition = true;
            condVar.notify_one();
        }

        /**
         * Waits for condition condition
         *
         * @return false if abort has been requested, true otherwise
         **/
        bool wait()
        {
            while (!condition)
            {
                condVar.wait(condLockWait);
            }

            condition = false;

            return !abortRequested;
        }

        /**
         * Aborts the synchronization. Results in notifying the condition, thus waking up a waiting thread. The abort is indicated via the return
         * value of wait.
         **/
        void abort()
        {
            lockNotify();
            abortRequested = true;
            notify();
            unlockNotify();
        }

    private:
        boost::mutex condMutex;
        boost::unique_lock<boost::mutex> condLockWait;
        boost::unique_lock<boost::mutex> condLockNotify;
        boost::condition_variable condVar;
        bool condition;
        bool abortRequested;
    };

}

