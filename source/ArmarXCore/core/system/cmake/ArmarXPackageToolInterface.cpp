/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2014
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "ArmarXPackageToolInterface.h"
#include <boost/algorithm/string.hpp>
#include <ArmarXCore/core/logging/Logging.h>
#include <boost/filesystem.hpp>
#include <cstdlib>

#define ARMARX_PACKAGE_TOOL "armarx-package"

namespace armarx
{

    ArmarXPackageToolInterface::ArmarXPackageToolInterface() :
        finder("ArmarXCore")
    {
        packageToolPath = finder.getBinaryDir() + "/" + ARMARX_PACKAGE_TOOL;

        if (!boost::filesystem::exists(packageToolPath))
        {
            if (system((std::string(ARMARX_PACKAGE_TOOL) + " -h").c_str()) == 0)
            {
                packageToolPath = ARMARX_PACKAGE_TOOL;
            }
            else
            {
                ARMARX_ERROR_S << "unable to find armarx package tool!";
            }
        }
    }

    bool ArmarXPackageToolInterface::checkPackagePath(const std::string& pathToPackageRoot, std::string& output) const
    {
        int result;
        if (!boost::filesystem::exists(pathToPackageRoot))
        {
            return false;
        }
        std::string cmd = packageToolPath + " status --dir \"" + pathToPackageRoot + "\"";
        output = CMakePackageFinder::ExecCommand(cmd, result);

        if (result == 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    bool ArmarXPackageToolInterface::checkPackagePath(const std::string& pathToPackageRoot) const
    {
        std::string output;
        return checkPackagePath(pathToPackageRoot, output);
    }

    bool ArmarXPackageToolInterface::addStatechart(const std::string& statechartGroupName, const std::string& statePath, const std::string& packagePath) const
    {
        int result = 0;
        std::string escapedStatePath = boost::replace_all_copy(statePath, " ", "\\ ");
        std::string escapedStatechartGroupName = boost::replace_all_copy(statechartGroupName, " ", "\\ ");
        std::string cmd = packageToolPath + " add statechart " + escapedStatechartGroupName + "/" +  escapedStatePath + " -l " + escapedStatechartGroupName;

        if (!packagePath.empty())
        {
            if (!boost::filesystem::exists(packagePath))
            {
                return false;
            }
            cmd += " --dir " + packagePath;
        }

        ARMARX_INFO_S << CMakePackageFinder::ExecCommand(cmd, result);
        return result == 0;

    }

    bool ArmarXPackageToolInterface::addFullXmlStatechart(const std::string& statechartGroupName, const std::string& statePath, const std::string& packagePath) const
    {
        return addXmlStatechart("xmlstate", "default", statechartGroupName, statePath, packagePath);
    }

    bool ArmarXPackageToolInterface::addXmlOnlyXmlStatechart(const std::string& statechartGroupName, const std::string& statePath, const std::string& packagePath) const
    {
        return addXmlStatechart("xmlstate-xmlonly", "xmlonly", statechartGroupName, statePath, packagePath);
    }

    bool ArmarXPackageToolInterface::addCppOnlyXmlStatechart(const std::string& statechartGroupName, const std::string& statePath, const std::string& packagePath) const
    {
        return addXmlStatechart("xmlstate", "cpponly", statechartGroupName, statePath, packagePath);
    }

    std::string ArmarXPackageToolInterface::getLastOutput() const
    {
        return lastOutput;
    }

    bool ArmarXPackageToolInterface::addXmlStatechart(const std::string& componentType, const std::string& replacementStrategyType, const std::string& statechartGroupName, const std::string& statePath, const std::string& packagePath) const
    {
        int result = 0;
        std::string escapedStatePath = boost::replace_all_copy(statePath, " ", "\\ ");
        std::string escapedStatechartGroupName = boost::replace_all_copy(statechartGroupName, " ", "\\ ");
        std::string cmd = packageToolPath + " add " + componentType + " " + escapedStatechartGroupName + "/" +  escapedStatePath + " -l " + escapedStatechartGroupName + " -s " + replacementStrategyType;

        if (!packagePath.empty())
        {
            cmd += " --dir " + packagePath;
        }

        lastOutput = CMakePackageFinder::ExecCommand(cmd, result);
        return result == 0;
    }
}
