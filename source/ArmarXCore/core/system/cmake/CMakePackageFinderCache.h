#pragma once

#include "CMakePackageFinder.h"
#include "../Synchronization.h"
#include <IceUtil/Time.h>

namespace armarx
{
    class CMakePackageFinderCache;
    class CMakePackageFinderCache
    {
    public:
        CMakePackageFinderCache(const CMakePackageFinderCache& s);
        CMakePackageFinderCache(IceUtil::Time timeout = IceUtil::Time::seconds(10));
        const CMakePackageFinder& findPackage(const std::string& packageName);
        static CMakePackageFinderCache GlobalCache;
    private:
        Mutex mutex;
        std::map < std::string, std::pair<IceUtil::Time, CMakePackageFinder> > packageFinders;
        IceUtil::Time timeout;
    };

}

