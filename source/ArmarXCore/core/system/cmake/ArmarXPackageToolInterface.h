/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2014
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/


#pragma once

#include <boost/shared_ptr.hpp>
#include "CMakePackageFinder.h"
namespace armarx
{

    /**
     * @class ArmarXPackageToolInterface
     * @ingroup core-utility
     * @brief The ArmarXPackageToolInterface class
     */
    class ArmarXPackageToolInterface
    {
    public:
        ArmarXPackageToolInterface();
        bool checkPackagePath(const std::string& pathToPackageRoot, std::string& output) const;
        bool checkPackagePath(const std::string& pathToPackageRoot) const;
        bool addStatechart(const std::string& statechartGroupName, const std::string& statePath, const std::string& packagePath = "") const;
        bool addFullXmlStatechart(const std::string& statechartGroupName, const std::string& statePath, const std::string& packagePath) const;
        bool addXmlOnlyXmlStatechart(const std::string& statechartGroupName, const std::string& statePath, const std::string& packagePath) const;
        bool addCppOnlyXmlStatechart(const std::string& statechartGroupName, const std::string& statePath, const std::string& packagePath) const;
        std::string getLastOutput() const;
    private:
        bool addXmlStatechart(const std::string& componentType, const std::string& replacementStrategyType, const std::string& statechartGroupName, const std::string& statePath, const std::string& packagePath) const;
        mutable std::string lastOutput;
        CMakePackageFinder finder;
        std::string packageToolPath;
    };
    typedef boost::shared_ptr<ArmarXPackageToolInterface> ArmarXPackageToolInterfacePtr;

}

