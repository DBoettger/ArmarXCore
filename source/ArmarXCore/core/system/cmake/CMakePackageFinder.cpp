/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2014
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "CMakePackageFinder.h"
#include <ArmarXCore/core/logging/Logging.h>
#include <ArmarXCore/core/system/ArmarXDataPath.h>
#include <ctime>
#include <stdio.h>
#include <sstream>
#include <boost/algorithm/string.hpp>
#include <boost/algorithm/string/regex.hpp>
#include <boost/regex.hpp>
#include <boost/interprocess/sync/file_lock.hpp>
#include <boost/interprocess/sync/scoped_lock.hpp>
#include <boost/interprocess/sync/sharable_lock.hpp>
#include <boost/filesystem.hpp>
#include "../../rapidxml/wrapper/RapidXmlReader.h"
#include "CMakePackageFinderCache.h"
#include <boost/shared_ptr.hpp>
#ifndef Q_MOC_RUN
#include <boost/interprocess/managed_shared_memory.hpp>
#include <boost/interprocess/sync/interprocess_upgradable_mutex.hpp>
#include <boost/interprocess/sync/interprocess_condition.hpp>
#include <ArmarXCore/core/services/sharedmemory/exceptions/SharedMemoryExceptions.h>
#endif

#define SCRIPT_PATH "ArmarXCore/core/system/cmake/FindPackageX.cmake"
#define CACHE_PATH "/ArmarXCMakeCache_" + (getenv("USER")?getenv("USER"):"DUMMY_USER") + "/"

namespace armarx
{
    std::string getHomeDir()
    {
        char* homePathC = getenv("HOME");
        std::string homePath;
        if (homePathC)
        {
            homePath = homePathC;
        }
        return homePath;
    }

    boost::shared_ptr<boost::interprocess::file_lock> getFileLock(std::string lockName)
    {
        std::string path = boost::filesystem::temp_directory_path().string() + CACHE_PATH;
        if (!boost::filesystem::exists(path))
        {
            if (!boost::filesystem::create_directories(path))
            {
                return boost::shared_ptr<boost::interprocess::file_lock>();
            }
        }
        path += lockName;
        if (!boost::filesystem::exists(path))
        {
            //touch file
            std::ofstream file(path);
            file.close();
        }
        return boost::shared_ptr<boost::interprocess::file_lock>(new boost::interprocess::file_lock(path.c_str()));
    }

    boost::shared_ptr<boost::interprocess::file_lock> CacheFileLock(getFileLock(".cachelock"));
    Mutex CacheMutex; // filelocks should NOT be accessed twice in the same process, so lock the filelock with a normal lock...
    boost::shared_ptr<boost::interprocess::file_lock> CMakeFileLock(getFileLock(".cmakelock"));
    Mutex CMakeMutex;

    typedef boost::shared_ptr<boost::interprocess::scoped_lock<boost::interprocess::file_lock>> ScopedFileLockPtr;

    ScopedFileLockPtr lockCMake()
    {
        ScopedLock lock(CMakeMutex);

        ScopedFileLockPtr lockPtr(new boost::interprocess::scoped_lock<boost::interprocess::file_lock>(*CMakeFileLock,
                                  boost::get_system_time() + boost::posix_time::milliseconds(50)));
        while (!lockPtr->owns())
        {
            lockPtr->timed_lock(boost::get_system_time() + boost::posix_time::milliseconds(50));
        }
        return lockPtr;
    }

    bool readCMakeCache(const std::string& packageName, std::string& packageContent)
    {
        auto start = IceUtil::Time::now();
        std::string path = boost::filesystem::temp_directory_path().string() + CACHE_PATH;
        path += packageName;
        if (!boost::filesystem::exists(path))
        {
            return false;
        }
        else
        {

            std::time_t writeTime = boost::filesystem::last_write_time(path);
            std::time_t result = std::time(nullptr);
            std::localtime(&result);
            long age = result - writeTime;
            //            ARMARX_INFO_S << VAROUT(packageName) << VAROUT(age) << VAROUT(std::asctime(std::localtime(&writeTime)));
            boost::interprocess::sharable_lock<boost::interprocess::file_lock> e_lock(*CacheFileLock);
            packageContent = RapidXmlReader::ReadFileContents(path);
            auto dura = (IceUtil::Time::now() - start).toMilliSecondsDouble();
            if (dura > 10000)
            {
                ARMARX_INFO_S << packageName << " from cache locked for " << dura;
            }
            if (age < 10)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }

    bool updateCMakeCache(const std::string& packageName, const std::string& packageContent)
    {
        auto start = IceUtil::Time::now();
        std::string path = boost::filesystem::temp_directory_path().string() + CACHE_PATH;
        if (!boost::filesystem::exists(path))
        {
            if (!boost::filesystem::create_directories(path))
            {
                return false;
            }
        }
        path = path + packageName;
        boost::interprocess::scoped_lock<boost::interprocess::file_lock> e_lock(*CacheFileLock);
        //        std::string existingPackageContent = RapidXmlReader::ReadFileContents(path);
        //        if (existingPackageContent != packageContent)
        {
            std::ofstream file(path);
            file << packageContent;
        }
        //        ARMARX_IMPORTANT_S << "Updateing cmakecache " << VAROUT(packageName) << VAROUT(path);
        auto dura = (IceUtil::Time::now() - start).toMilliSecondsDouble();
        if (dura > 10000)
        {
            ARMARX_INFO_S << packageName << " update cache locked for " << dura;
        }
        return true;
    }

    boost::interprocess::interprocess_upgradable_mutex* memoryMutex = nullptr;
    boost::shared_ptr<boost::interprocess::managed_shared_memory> sharedMemorySegment;
    Mutex cmakePackageMutex;



    CMakePackageFinder::CMakePackageFinder(const std::string& packageName, const boost::filesystem::path& packagePath, bool suppressStdErr,  bool usePackagePathOnlyAsHint) :
        found(false),
        packageName(boost::trim_copy(packageName))
    {
        if (this->packageName.empty())
        {
            ARMARX_WARNING << "CMakePackageFinder: Package name must not be empty";
        }
        static std::string scriptPath;
        {

            ScopedLock lock(cmakePackageMutex);

            if (scriptPath.empty())
            {
                // Somehow this call fails sometimes if many components are started seperatly
                // So try on fail again with another path
                auto start = IceUtil::Time::now();
                auto list  = FindPackageIncludePathList("ArmarXCore");
                auto dura = (IceUtil::Time::now() - start);

                if (dura.toMilliSeconds() > 10000)
                {
                    ARMARX_INFO_S << "Cmakefinder for initial core search took long  - Duration: " << dura.toMilliSeconds();
                }

                if (!ArmarXDataPath::getAbsolutePath(SCRIPT_PATH, scriptPath, list, false))
                {
                    ARMARX_WARNING_S << "Finding FindPackageX.cmake failed - trying again with different path";

                    if (!ArmarXDataPath::getAbsolutePath(std::string("../source/") + SCRIPT_PATH, scriptPath))
                    {
                        return;
                    }
                }
            }
        }
        //        ARMARX_INFO_S << scriptPath;
        int result;

        auto start = IceUtil::Time::now();
        std::string resultStr;
        std::string tmpDir = getArmarXCMakeTempDir();
        try
        {
            if (!packagePath.empty())
            {
                resultStr = ExecCommand("cd " + tmpDir + "; cmake -DPACKAGE=" + this->packageName + " -DPACKAGEBUILDPATH" + (usePackagePathOnlyAsHint ? "Hint" : "") + "=" + packagePath.string() + " -P " + scriptPath, result, suppressStdErr);
            }
            else if (!readCMakeCache(this->packageName, resultStr))
            {
                resultStr = ExecCommand("cd " + tmpDir + "; cmake -DPACKAGE=" + this->packageName + " -P " + scriptPath, result, suppressStdErr);
                updateCMakeCache(this->packageName, resultStr);

            }
        }
        catch (...)
        {


        }


        auto dura = (IceUtil::Time::now() - start);

        if (dura.toMilliSeconds() > 10000)
        {
            ARMARX_INFO_S << "Cmakefinder took long for package " << packagePath << " - Duration: " << dura.toMilliSeconds();
        }

        std::vector<std::string> resultList;
        resultList = extractVariables(resultStr);

        //        ARMARX_INFO_S << resultList;
    }

    std::vector<std::string> CMakePackageFinder::FindPackageIncludePathList(const std::string& packageName)
    {
        //        _CreateSharedMutex();

        std::string output = FindPackageIncludePaths(packageName);
        std::vector<std::string> result;
        boost::split_regex(result,
                           output,
                           boost::regex("-I")
                          );

        for (size_t i = 0; i < result.size(); i++)
        {
            boost::algorithm::trim(result[i]);

            if (result[i].empty())
            {
                result.erase(result.begin() + i);
                i--;
            }
        }

        return result;
    }

    std::string CMakePackageFinder::FindPackageLibs(const std::string& packageName)
    {
        //        boost::interprocess::scoped_lock<boost::interprocess::file_lock> e_lock(*CMakeFileLock);
        auto lock = lockCMake();
        auto start = IceUtil::Time::now();
        try
        {

            std::stringstream str;
            str << "cmake --find-package -DNAME=" << packageName << " -DLANGUAGE=CXX -DCOMPILER_ID=GNU -DMODE=LINK";
            int result;
            std::string output = ExecCommand(str.str(), result);

            if (result != 0)
            {
                return "";
            }


            auto dura = (IceUtil::Time::now() - start).toMilliSecondsDouble();
            if (dura > 10)
            {
                //                ARMARX_INFO_S << packageName << " locked for " << dura;
            }
            return output;

        }
        catch (...)
        {
            return "";
        }

    }


    std::string CMakePackageFinder::FindPackageIncludePaths(const std::string& packageName)
    {
        auto start = IceUtil::Time::now();
        //        std::cout << start.toDateTime() << " Waiting for lock;" << std::endl;
        auto lock = lockCMake();
        //        auto dura = (IceUtil::Time::now() - start).toMilliSecondsDouble();
        //        if(dura > 1000)
        //            ARMARX_IMPORTANT_S << " waited for " << dura;
        start = IceUtil::Time::now();
        try
        {
            std::string tmpDir = getArmarXCMakeTempDir();
            std::stringstream str;
            str << "cd " + tmpDir + ";cmake --find-package -DNAME=" << packageName << " -DLANGUAGE=CXX -DCOMPILER_ID=GNU -DMODE=COMPILE";
            int result;
            std::string output = ExecCommand(str.str(), result);

            if (result != 0)
            {
                ARMARX_IMPORTANT_S << packageName << " search failed - output: " << output;
                return "";
            }


            auto dura = (IceUtil::Time::now() - start).toMilliSecondsDouble();
            if (dura > 10)
            {
                //                ARMARX_INFO_S << packageName << " locked for " << dura;
            }
            return output;
        }
        catch (...)
        {


            return "";
        }

    }

    std::string CMakePackageFinder::ExecCommand(std::string command, int& result, bool suppressStdErr)
    {
        auto start = IceUtil::Time::now();
        if (suppressStdErr)
        {
            command += " 2>/dev/null";
        }
        ARMARX_DEBUG << "Cmd: " << command;
        FILE* fp = popen(command.c_str(), "r");
        char line [50];
        std::string output;

        while (fgets(line, sizeof line, fp))
        {
            output += line;
        }

        result = pclose(fp) / 256;
        auto dura = (IceUtil::Time::now() - start).toMilliSecondsDouble();
        if (dura > 1000)
        {
            ARMARX_INFO_S << "ExecCommand took " << dura << " \n command: " << command;
        }
        return output;
    }

    Ice::StringSeq CMakePackageFinder::FindAllArmarXSourcePackages()
    {
        Ice::StringSeq result;
        using namespace boost::filesystem;
        auto home = getHomeDir();
        if (!home.empty())
        {
            path p = path {home} / ".cmake" / "packages";
            if (is_directory(p))
            {
                for (const path& entry : boost::make_iterator_range(directory_iterator(p), {}))
                {
                    const std::string pkg = entry.filename().string();
                    auto pckFinder = CMakePackageFinder {pkg, "", true};
                    if (pckFinder.packageFound() && !pckFinder.getBuildDir().empty())
                    {
                        result.push_back(pkg);
                    }
                }
            }
        }
        return result;
    }

    std::string CMakePackageFinder::getName() const
    {
        return packageName;
    }

    bool CMakePackageFinder::packageFound() const
    {
        return found;
    }

    const std::map<std::string, std::string>& CMakePackageFinder::getVars() const
    {
        return vars;
    }

    std::string CMakePackageFinder::getVar(const std::string& varName) const
    {
        std::map<std::string, std::string>::const_iterator it =  vars.find(varName);

        if (it != vars.end())
        {
            return it->second;
        }

        return "";
    }

    std::vector<std::string> CMakePackageFinder::getDependencies() const
    {
        auto depListString = getVar("DEPENDENCIES");
        std::vector<std::string> resultList = armarx::Split(depListString, ";", true, true);

        return resultList;
    }

    std::map<std::string, std::string> CMakePackageFinder::getDependencyPaths() const
    {
        // is of type "package1:package1Path;package2:packagePath2"
        auto depListString = getVar("PACKAGE_DEPENDENCY_PATHS");
        std::vector<std::string> resultList;
        boost::split(resultList,
                     depListString,
                     boost::is_any_of(";"),
                     boost::token_compress_on);
        std::map<std::string, std::string> resultMap;

        for (auto& depPairString : resultList)
        {
            std::vector<std::string> depPair;
            boost::split(depPair,
                         depPairString,
                         boost::is_any_of(":"),
                         boost::token_compress_on);

            if (depPair.size() < 2)
            {
                continue;
            }

            resultMap[depPair.at(0)] = depPair.at(1);
        }

        return resultMap;
    }

    bool CMakePackageFinder::_ParseString(const std::string& input, std::string& varName, std::string& content)
    {
        //        ARMARX_INFO_S << "input: " << input;
        const boost::regex e("\\-\\- ([a-zA-Z0-9_]+):(.+)");
        boost::match_results<std::string::const_iterator> what;

        bool found = boost::regex_search(input, what, e);

        for (size_t i = 1; i < what.size(); i++)
        {

            if (i == 1)
            {
                varName =  what[i];
            }
            else if (i == 2)
            {
                content = what[i];
            }

            //            ARMARX_INFO_S << VAROUT(varName);
        }

        boost::algorithm::trim(varName);
        boost::algorithm::trim(content);
        return found;

    }

    void CMakePackageFinder::_CreateSharedMutex()
    {

    }


    std::vector<std::string> CMakePackageFinder::extractVariables(const std::string& cmakeVarString)
    {
        std::vector<std::string> resultList;
        boost::split(resultList,
                     cmakeVarString,
                     boost::is_any_of("\n"),
                     boost::token_compress_on);

        for (size_t i = 0; i < resultList.size(); i++)
        {
            boost::algorithm::trim(resultList[i]);

            if (resultList[i].empty())
            {
                resultList.erase(resultList.begin() + i);
                i--;
            }
            else
            {
                std::string var;
                std::string content;

                if (_ParseString(resultList[i], var, content))
                {
                    found = true;
                    vars[var] = content;
                }
            }
        }

        return resultList;
    }

    std::vector<std::string> armarx::CMakePackageFinder::getIncludePathList() const
    {
        auto depListString = getIncludePaths();
        std::vector<std::string> resultList;
        boost::split(resultList,
                     depListString,
                     boost::is_any_of(";"),
                     boost::token_compress_on);
        return resultList;
    }


    std::string CMakePackageFinder::getArmarXCMakeTempDir()
    {
        const std::string tmpDir = "/tmp";
        std::string result = tmpDir;
        char* username = getenv("USER");
        if (username)
        {
            std::string usernameString = std::string(username);
            boost::algorithm::trim(usernameString);
            result += "/armarxcmake-" + usernameString;
            if (!boost::filesystem::exists(result))
            {
                if (!boost::filesystem::create_directories(result))
                {
                    result = tmpDir;
                }
            }
        }
        return result;
    }

    bool CMakePackageFinder::ReplaceCMakePackageFinderVars(std::string& string)
    {
        const boost::regex e("\\$C\\{([a-zA-Z0-9_\\-]+):([a-zA-Z0-9_\\-]+)\\}");
        boost::match_results<std::string::const_iterator> what;
        bool found = boost::regex_search(string, what, e);
        std::map<std::string, CMakePackageFinder> finders;
        if (found)
        {
            for (size_t i = 1; i < what.size(); i += 3)
            {
                std::string package = what[i];
                auto it = finders.find(package);
                if (it == finders.end())
                {
                    it = finders.insert(
                             std::make_pair(package, CMakePackageFinderCache::GlobalCache.findPackage(package))).first;
                    //                it = finders.find(package);
                }
                std::string var = what[i + 1];


                auto envVar = it->second.getVar(var);
                string = boost::regex_replace(string, e, std::string(envVar));
                ARMARX_DEBUG << "Replacing '" << var << "' with '" << std::string(envVar) << "'";
            }
        }
        return found;

    }
}
