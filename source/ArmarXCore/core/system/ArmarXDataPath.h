/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarXCore::core
* @author     Nikolaus Vahrenkamp
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <string>
#include <vector>


namespace armarx
{
    /**
    * \class ArmarXDataPath
    * \ingroup core-utility
    * The ArmarXDataPath class Provides static methods to handle ArmarX data directories.
    *
    * Data paths can be defined via the environment variable ArmarXData_DIRS (semicolon separated list).
    * Additionally ArmarX Properties (ArmarX.DataPath) can be used either by command line or by config files.
    *
    * The search order is as follows
    * 1. Environment variable
    * 2. Properties
    * 3. Standard armax data path (ArmarXHome_DIR/data)
    *
    * Usually, the paths are initialized by the framework automatically (see Application).
    *
    * To convert a filename given relatively to one of the data directories use the following syntax
    * @code
    * std::string relativeFilename("robot/ArmarIII/ArmarIII.xml");
    * std::string absoluteFilename;
    * bool fileFound = ArmarXDataPath::getAbsolutePath(relativeFilename, absoluteFilename);
    * if (fileFound)
    *     load(absoluteFilename);
    * @endcode
    *
    */
    class ArmarXDataPath
    {
    public:
        /*!
         * This method searches all data paths for a valid file.
         * The precedence of the datapaths is in the insertion order.
         * The datapaths given to this function have the highest precedence.
         * \param relativeFilename The file relatively to one of the data paths
         * \param storeAbsoluteFilename In case a valid absolute filename can be found, it is stored here.
         * \param additionalSearchPaths Additional search paths that are not in the globally registered variable.
         * \param verbose If set to true, the function will print all available paths and the requested file to the console on failure.
         * \return true on success.
         */
        static bool getAbsolutePath(const std::string& relativeFilename,
                                    std::string& storeAbsoluteFilename,
                                    const std::vector<std::string>& additionalSearchPaths = {},
                                    bool verbose = true);

        /*!
         * This method searches all data paths for a valid file.
         * The precedence of the datapaths is in the insertion order.
         * The datapaths given to this function have the highest precedence.
         * \param relativeFilename The file relatively to one of the data paths
         * \param additionalSearchPaths Additional search paths that are not in the globally registered variable.
         * \param verbose If set to true, the function will print all available paths and the requested file to the console on failure.
         * \return In case a valid absolute filename can be found, it is returned (otherwise an empty string is returned).
         */
        static std::string getAbsolutePath(const std::string& relativeFilename,
                                           const std::vector<std::string>& additionalSearchPaths = {},
                                           bool verbose = true);

        /*!
         * Search all project data paths and reports within which project the file can be located.
         * First hit is reported.
         * If none of the project's data paths contains the file, an empty string is returned.
         * \param projects List of ArmarXProjects
         * \param relativeFilename Filename relative to a project's data directory.
         * \return The armarx project that contains the given file in its data directory.
         */
        static std::string getProject(const std::vector<std::string>& projects,
                                      const std::string& relativeFilename);

        static std::string cleanPath(const std::string& filepathStr);


        /**
         * @brief This method tries to morph a given absolute path into a
         * relative path to a ArmarXDataPath.
         * @param absolutePathString String of an absolute (file)path
         * @return The relative path to the given absolute (file)path, if possible.
         * Otherwise the given absolute path.
         * @see addDataPaths()
         */
        static std::string getRelativeArmarXPath(const std::string& absolutePathString);

        static std::string relativeTo(const std::string& from, const std::string& to);
        static bool mergePaths(std::string path, std::string subPath, std::string& result);

        /*!
         * Add data paths to the internal search list.
         * Multiple paths are separated by a semicolon ";" and environment
         * variables specified with "${}" inside the paths are expanded.
         * The latest additions have the highest precedence, when solving relative paths.
         *  \code
         *  addDataPaths("/root;/usr");
         *  addDataPaths("/home;/var");
         *  \endcode
         * This would result in the following precedence order: home, var, root, usr.
         * \param dataPathList A semicolon separated list of absolute paths.
         * Each path is checked for existance and in case it is not present it is silently ignored.
         */
        static void addDataPaths(const std::string& dataPathList);

        /*!
         * The base directory of ArmarX.
         * Checks the environment variables for ArmarXHome_DIR and returns the entry.
         */
        static std::string getHomePath();

        /*!
         * Returns all absolute data paths.
         */
        static std::vector<std::string> getDataPaths();

        /*!
         * Initializes data paths. Automatically called by an Application.
         * Additionally checks for environment variables (ArmarXData_DIR) and
         * standard data path (ArmarXHome_DIR/data)
         */
        static void initDataPaths(const std::string& dataPathList);

        /**
         * @brief ReplaceEnvVars replaces environment variables in a string with
         * their values, if the env. variable is set. E.g. "${HOME}/.armarx/default.cfg"
         * @param string String in which the environment variables are to be replaced
         * @return returns true if atleast one replacement has been done.
         */
        static bool ReplaceEnvVars(std::string& string);

        /**
         * @brief Replaces all occurences of variables in bash notation,
         * e.g. ${HOME}, with the given value in the string.
         * @param string String to be modified
         * @param varName Name of the variable
         * @param varValue Value to be inserted in place of varName occurrences
         */
        static void ReplaceVar(std::string& string, const std::string varName, const std::string& varValue);

        /**
         * @brief Resolves a path like ~/myfile.txt or $HOME/myfile.txt to /home/user/myfile.txt.
         * @param string
         */
        static void ResolveHomePath(std::string& path);

        /**
         * @brief The base Cache directory of ArmarX
         * @return returns the cache path of ArmarX or an empty String
         * if there is no valid Application instance
         */
        static std::string GetCachePath();

    private:
        //! Initializes and searches the environment variable ArmarXData_DIR for data directories.
        static void init();

        static bool __addPath(const std::string& path);
        static bool __addPaths(const std::string& pathList);
        static std::vector<std::string> __separatePaths(const std::string& pathList);

        static std::vector<std::string> dataPaths;

        static bool initialized;
        ArmarXDataPath(); // no instantiation allowed
    };
}

