/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2013
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <ArmarXCore/core/services/tasks/PeriodicTask.h>
#include <IceUtil/Time.h>               // for Time
#include <boost/functional/hash/hash.hpp>  // for hash_combine
#include <boost/optional/optional.hpp>  // for optional
#include <boost/unordered/unordered_set.hpp>  // for unordered_set
#include <map>                          // for map
#include <ostream>                      // for operator<<, size_t
#include <string>                       // for string, operator<<
#include <vector>                       // for vector

#include "../services/profiler/Profiler.h"  // for ProfilerPtr
#include "ArmarXCore/core/system/Synchronization.h"  // for Mutex

namespace boost
{
    template <class T> struct hash;
}  // namespace boost

namespace armarx
{
    struct ThreadUsage
    {
        int threadId;
        int processId;
        IceUtil::Time lastUpdate;
        int lastJiffies;
        double load;
        bool operator <(const ThreadUsage& rhs) const;
        bool operator ==(const ThreadUsage& rhs) const;
    };

    struct CpuUsage
    {
        long processId;
        int lastUtime;
        int lastStime;
        int lastCUtime;
        int lastCStime;
        IceUtil::Time lastUpdate;
        double proc_total_time;
    };

    struct MemoryUsage
    {
        int fastbinBlocks; // fsmblocks: space in freed fastbin blocks (bytes)
        int totalAllocatedSpace; // uordblks: total allocated space (bytes)
        int totalFreeSpace; // fordblks: total free space (bytes)
    };
}

namespace boost
{
    template <>
    struct hash<armarx::ThreadUsage>
    {
        typedef armarx::ThreadUsage      argument_type;
        typedef std::size_t  result_type;

        result_type operator()(const armarx::ThreadUsage& t) const
        {
            std::size_t val = {0};

            boost::hash_combine(val, t.processId);
            boost::hash_combine(val, t.threadId);
            return val;
        }
    };
}

namespace armarx
{
    /**
     * @brief The ProcessWatcher class is instantiated once in each armarx::Application an monitors thread, CPU, and memory utilization
     */
    class ProcessWatcher
    {
    public:
        ProcessWatcher(Profiler::ProfilerPtr profiler);
        void start();
        void stop();


        void addThread(int processId, int threadId);
        void addThread(int threadId);
        void addAllChildThreads(int processId, int threadId);


        void removeAllThreads();
        void removeThread(int processId, int threadId);
        void removeThread(int threadId);

        void reportCpuUsage();
        void reportMemoryUsage();

        double getThreadLoad(int processId, int threadId);
        double getThreadLoad(int threadId);

        armarx::CpuUsage getProcessCpuUsage();

        static int GetThreadJiffies(int processId, int threadId);
        static std::map<int, int> GetThreadListJiffies(int processId, std::vector<int> threadIds);
        static int GetHertz();
    private:
        enum PROC_INDEX
        {
            eUTime = 13,
            eSTime = 14,
            eCUTIME = 15,
            eCSTIME = 16
        };

        void watch();
        void runThreadWatchList();
        void cpuUsageFromProcFile();
        void getMemoryUsage();

        long Hertz;
        std::string processCpuDataFilename;

        PeriodicTask<ProcessWatcher>::pointer_type watcherTask;

        Mutex processCpuUsageMutex;
        armarx::CpuUsage processCpuUsage;

        Mutex processMemoryUsageMutex;
        armarx::MemoryUsage memoryUsage;

        Mutex threadWatchListMutex;
        boost::unordered_set<ThreadUsage> threadWatchList;

        /**
         * Holds an instance of armarx::Profiler wich is set in the constructor of CPULoadWatcher.
         */
        Profiler::ProfilerPtr profiler;

    };
}



