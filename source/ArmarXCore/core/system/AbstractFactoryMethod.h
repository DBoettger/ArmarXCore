/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarXCore::core
* @author     Nils Adermann (naderman at naderman dot de)
* @date       2009
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <string>
#include <map>
#include <vector>
#include <boost/shared_ptr.hpp>
#include <ArmarXCore/core/logging/Logging.h>

namespace armarx
{
    /**
    * A template that can be used as a superclass of a class hierarchy that
    * wants to provide a factory method which allows instantiation of objects
    * based on a string identifier.
    *
    * The first template argument is the base class of your class hierarchy.
    * The second argument is the parameter type for the initialisation function
    * each subclass has to provide. If you need multiple constructor arguments
    * it is recommended to use a boost::tuple.
    */
    template <typename Base, typename constructorArg, typename SharedPointer = boost::shared_ptr<Base> >
    class AbstractFactoryMethod
    {
        /**
        * Classname of this instance, so that one can save in a string which classtype this instance is and recreate it.
        */
        std::string className;

    public:
        using BaseFactory = Base;
        using ConstructorArg = constructorArg;
        using SharedPointerType = SharedPointer;
        /**
         * @brief getClassName returns the name that this instance was generated
         * with.
         * @return name of class
         */
        std::string getClassName()
        {
            return className;
        }

        /**
        * The function pointer type of subclass initialisation functions.
        * This matches the createInstance method.
        */
        typedef SharedPointerType(*initialisationFunction)(constructorArg);

        /**
        * Function which can be used to retrieve an object specified by string name.
        */
        static SharedPointerType fromName(const std::string& name, constructorArg params)
        {
            if (subTypes()->find(name) == subTypes()->end())
            {
                return SharedPointerType();
            }

            SharedPointerType result = (*subTypes())[name](params);
            if (result)
            {
                result->className = name;
            }
            return result;
        }


        /**
         * @brief getAvailableClasses retrieves a list of all registered classes
         * as their string-representation.
         * @return list of available classes by their name.
         */
        static std::vector<std::string> getAvailableClasses()
        {
            std::vector<std::string> result;
            typename std::map<std::string, initialisationFunction>::iterator it = subTypes()->begin();

            for (; it != subTypes()->end(); ++it)
            {
                result.push_back(it->first);
            }

            return result;

        }

        /**
        * Returns the class's name. This is used to identify the class which the
        * user requests an instance of.
        */
        static std::string getName()
        {
            return "AbstractFactoryMethod";
        }

        /**
        * Initialisation function which needs to be provided by every subclass.
        * It calls the constructor and returns a shared_ptr to the resulting
        * object.
        */
        static SharedPointerType createInstance(constructorArg)
        {
            return SharedPointerType();
        }



        /**
        * Statically called by subclasses to register their name and initialisation
        * function so they can be found by {@link fromName fromName}.
        */
        static void registerClass(const std::string& name, initialisationFunction init)
        {
            int status = -1;
            char* demangled = abi::__cxa_demangle(typeid(init).name(), nullptr, nullptr, &status);;
            ARMARX_VERBOSE << typeid(Base).name() << "Registering factory '" << name << "' for function of type: " << demangled;
            free(demangled);
            (*subTypes())[name] = init;
        }

        /**
        * A helper struct to allow static initialisation of the subclass lookup table.
        */
        struct SubClassRegistry
        {
            SubClassRegistry(const std::string& name, initialisationFunction init)
            {
                Base::registerClass(name, init);
            }
        };

    private:
        /**
        * Static wrapper method for accessing subTypes map.
        * This method is necessary to make certain that the map is initialised
        * before use. This can only be guaranteed through a static local variable
        * in a function.
        */
        static boost::shared_ptr<std::map<std::string, initialisationFunction> > subTypes()
        {
            static boost::shared_ptr<std::map<std::string, initialisationFunction> > subTypes(new std::map<std::string, initialisationFunction>);

            return subTypes;
        }
    };
}

