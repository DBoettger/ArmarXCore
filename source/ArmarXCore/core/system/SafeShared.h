/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <ArmarXCore/core/exceptions/Exception.h>

// Ice
#include <IceUtil/Shared.h>
#include <Ice/ProxyHandle.h>

// boost
#include <boost/utility/enable_if.hpp>
#include <boost/type_traits/is_base_of.hpp>
#include <boost/static_assert.hpp>



namespace armarx
{
    /**
     * @class SafeShared
     * @ingroup core-utility
     * This template class ensures, that the derived class cannot be transformed
     * from a normal variable to a sharedPtr, which could lead to early deletion
     * of the object.
     */
    template <class Derived>
    class SafeShared :
        virtual public IceUtil::Shared
    {
    public:
        SafeShared()
        {
            BOOST_STATIC_ASSERT_MSG((boost::is_base_of<SafeShared, Derived>::value), "The template parameter of SafeShared, must be a class that derives from SafeShared");
            BOOST_STATIC_ASSERT_MSG((boost::is_base_of<IceUtil::Shared, Derived>::value), "The template parameter of SafeShared, must be a class that derives from IceUtil::Shared");
        }
        ~SafeShared() override
        {

        }
    protected:
        // &-operator is protected and cannot be called
        IceInternal::Handle<Derived> operator&()
        {
        }
    };
}

