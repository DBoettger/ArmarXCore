/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package
* @author     Mirko Waechter( waechter at kit dot edu)
* @date       2013
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <vector>
#include <map>
#include <string>
#include <boost/shared_ptr.hpp>

#include <ArmarXCore/core/system/Synchronization.h>
#include <ArmarXCore/core/logging/Logging.h>

#include <Ice/ObjectFactory.h>

namespace armarx
{
    class FactoryCollectionBase;
    typedef IceUtil::Handle<FactoryCollectionBase> FactoryCollectionBasePtr;
    class FactoryCollectionBaseCleanUp
    {
        FactoryCollectionBaseCleanUp(FactoryCollectionBasePtr factoryToRemoveOnDesctruction);
        FactoryCollectionBasePtr factoryCollection;

    public:
        FactoryCollectionBaseCleanUp(FactoryCollectionBaseCleanUp&&) = default;
        ~FactoryCollectionBaseCleanUp();
        friend class FactoryCollectionBase;

    };

    template <class IceBaseClass, class DerivedClass>
    class GenericFactory : public Ice::ObjectFactory
    {
    public:
        Ice::ObjectPtr create(const std::string& type) override
        {
            assert(type == IceBaseClass::ice_staticId());
            return new DerivedClass();
        }
        void destroy() override {}
    };

    typedef std::map<std::string, Ice::ObjectFactoryPtr> ObjectFactoryMap;

    class FactoryCollectionBase : virtual public IceUtil::Shared
    {
    public:
        FactoryCollectionBase();

        virtual ObjectFactoryMap getFactories() = 0;


        static FactoryCollectionBaseCleanUp addToPreregistration(FactoryCollectionBasePtr factoryCollection);

        static const std::vector<FactoryCollectionBasePtr>& GetPreregistratedFactories()
        {
            return PreregistrationList();
        }

        template <class IceBaseClass, class DerivedClass>
        void add(ObjectFactoryMap& map)
        {
            map.insert(std::make_pair(IceBaseClass::ice_staticId(), new GenericFactory<IceBaseClass, DerivedClass>()));
        }

        template <class DerivedClass>
        void add(ObjectFactoryMap& map)
        {
            map.insert(std::make_pair(DerivedClass::ice_staticId(), new GenericFactory<DerivedClass, DerivedClass>()));
        }

    protected:
        static std::vector<FactoryCollectionBasePtr>& PreregistrationList();
        static Mutex& RegistrationListMutex();

        friend class FactoryCollectionBaseCleanUp;
        friend class ArmarXManager;
    };



}
