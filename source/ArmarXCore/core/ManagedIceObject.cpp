/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarXCore::core
* @author     Kai Welke (welke at kit dot edu)
* @author     Jan Issac (jan dot issac at gmx dot de)
* @date       2011
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include <ArmarXCore/interface/core/Profiler.h>
#include <Ice/BuiltinSequences.h>       // for StringSeq
#include <Ice/Identity.h>               // for Identity
#include <Ice/LocalException.h>
#include <Ice/ObjectAdapter.h>          // for ObjectAdapterPtr, etc
#include <IceStorm/IceStorm.h>          // for TopicPrx
#include <IceUtil/Shared.h>             // for Shared
#include <IceUtil/UUID.h>               // for generateUUID
#include <boost/smart_ptr/shared_ptr.hpp>  // for shared_ptr
#include <boost/thread/pthread/condition_variable.hpp>
#include <boost/thread/pthread/mutex.hpp>  // for mutex, etc
#include <algorithm>                    // for find
#include <map>                          // for _Rb_tree_iterator, etc
#include <ostream>                      // for operator<<, basic_ostream, etc
#include <utility>                      // for pair, make_pair

#include "ArmarXCore/core/ArmarXFwd.h"  // for ArmarXObjectSchedulerPtr, etc
#include "ArmarXCore/core/exceptions/local/ExpressionException.h"
#include "ArmarXCore/core/logging/LogSender.h"  // for LogSender
#include "ArmarXCore/core/logging/Logging.h"  // for ARMARX_VERBOSE, etc
#include "ArmarXCore/core/services/profiler/Profiler.h"  // for Profiler, etc
#include "ArmarXCore/interface/core/ManagedIceObjectDefinitions.h"
#include "ArmarXCore/interface/core/ManagedIceObjectDependencyBase.h"
#include "ArmarXManager.h"              // for ArmarXObjectSchedulerPtr, etc
#include "ArmarXObjectScheduler.h"      // for ArmarXObjectScheduler
#include "IceManager.h"                 // for IceManager, IceManagerPtr
#include "ManagedIceObject.h"
#include "ManagedIceObjectDependency.h"
#include "services/profiler/IceLoggingStrategy.h"
#include "services/profiler/LoggingStrategy.h"  // for LoggingStrategy, etc

namespace Ice
{
    class NotRegisteredException;
    class ObjectAdapterDeactivatedException;
}  // namespace Ice


using namespace armarx;

// *******************************************************
// static data
// *******************************************************
const ManagedIceObjectPtr ManagedIceObject::NullPtr(nullptr);

// *******************************************************
// construction
// *******************************************************
ManagedIceObject::ManagedIceObject() :
    name(""),
    objectState(eManagedIceObjectCreated),
    profiler(new Profiler::Profiler())
{
    enableProfilerFunction = &ManagedIceObject::Noop;
}

ManagedIceObject::ManagedIceObject(ManagedIceObject const& other) :
    IceUtil::Shared(),
    name(other.name),
    armarXManager(other.armarXManager),
    iceManager(nullptr),
    objectScheduler(nullptr),
    objectState(eManagedIceObjectCreated),
    proxy(nullptr),
    objectAdapter(nullptr),
    connectivity(other.connectivity),
    profiler(other.profiler)
{
    enableProfilerFunction = &ManagedIceObject::Noop;
}

std::string ManagedIceObject::getName() const
{
    if (name.empty())
    {
        return getDefaultName();
    }

    return name;
}

std::string ManagedIceObject::generateSubObjectName(const std::string& superObjectName, const std::string& subObjectName)
{
    return superObjectName + "." + subObjectName;
}

std::string ManagedIceObject::generateSubObjectName(const std::string& subObjectName)
{
    return generateSubObjectName(getName(), subObjectName);
}


ManagedIceObject::~ManagedIceObject()
{
}

Ice::ObjectAdapterPtr ManagedIceObject::getObjectAdapter() const
{
    return objectAdapter;
}



// *******************************************************
// establish dependencies
// *******************************************************

bool  ManagedIceObject::usingProxy(const std::string& name, const std::string& endpoints)
{
    if (name.empty())
    {
        throw LocalException("proxyName must not be empty.");
    }

    boost::mutex::scoped_lock lock(connectivityMutex);

    // check if proxy already exists
    if (connectivity.dependencies.find(name) != connectivity.dependencies.end())
    {
        return false;
    }

    // add dependency
    ManagedIceObjectDependencyPtr proxyDependency = new ProxyDependency(getIceManager(), name);
    connectivity.dependencies.insert(std::make_pair(name, proxyDependency));

    return true;
}

std::vector<std::string> ManagedIceObject::getUnresolvedDependencies() const
{
    ManagedIceObjectConnectivity con = getConnectivity();
    std::vector<std::string> dependencies;

    for (DependencyMap::iterator i = con.dependencies.begin(); i != con.dependencies.end(); i++)
    {
        ManagedIceObjectDependencyBasePtr& dep = i->second;

        if (!dep->getResolved())
        {
            dependencies.push_back(dep->getName());
        }
    }

    return dependencies;
}

std::string ManagedIceObject::GetObjectStateAsString(ManagedIceObjectState state)
{
    switch (state)
    {
        case eManagedIceObjectCreated:
            return "Created";

        case eManagedIceObjectInitializing:
            return "Initializing";

        case eManagedIceObjectInitialized:
            return "Initialized";
        case eManagedIceObjectInitializationFailed:
            return "InitializationFailed";

        case eManagedIceObjectStarting:
            return "Starting";

        case eManagedIceObjectStartingFailed:
            return "StartingFailed";

        case eManagedIceObjectStarted:
            return "Started";

        case eManagedIceObjectExiting:
            return "Exiting";

        case eManagedIceObjectExited:
            return "Exited";
    }

    return "Unknown";
}


void ManagedIceObject::usingTopic(const std::string& name, bool orderedPublishing)
{
    boost::mutex::scoped_lock lock(connectivityMutex);

    // check if topic is already used
    if (std::find(connectivity.usedTopics.begin(), connectivity.usedTopics.end(), name) != connectivity.usedTopics.end())
    {
        return;
    }

    // add dependency
    connectivity.usedTopics.push_back(name);
    orderedTopicPublishing[name] = orderedPublishing;
    ARMARX_VERBOSE << "Using topic with name: '" << name << "'";

    // only subscribe direcly if component has been initialized. Otherwise the dependency resolution will take care
    if (getState() >= eManagedIceObjectStarting)
    {
        iceManager->subscribeTopic(proxy, name, orderedPublishing);
    }
}

bool ManagedIceObject::unsubscribeFromTopic(const std::string& name)
{
    boost::mutex::scoped_lock lock(connectivityMutex);

    ARMARX_CHECK_EXPRESSION(proxy);
    iceManager->unsubscribeTopic(proxy, name);
    // check if topic is already used
    auto it = std::find(connectivity.usedTopics.begin(), connectivity.usedTopics.end(), name);
    if (it != connectivity.usedTopics.end())
    {
        connectivity.usedTopics.erase(it);
        ARMARX_VERBOSE << "Removing " << name << " from used topic list";
        return true;
    }
    return false;
}


void ManagedIceObject::offeringTopic(const std::string& name)
{
    boost::mutex::scoped_lock lock(connectivityMutex);
    auto topicName = name + iceManager->getTopicSuffix();
    // check if topic is already used
    if (std::find(connectivity.offeredTopics.begin(), connectivity.offeredTopics.end(), topicName) != connectivity.offeredTopics.end())
    {
        return;
    }

    // add dependency
    connectivity.offeredTopics.push_back(topicName);
    ARMARX_INFO << "Offering topic with name: '" << topicName << "'";

    // only retrieve topic direcly if component has been initialized. Otherwise the dependency resolution will take care
    if (getState() >= eManagedIceObjectStarting)
    {
        iceManager->getTopic<IceStorm::TopicPrx>(name);
    }
}




bool ManagedIceObject::removeProxyDependency(const std::string& name)
{
    boost::mutex::scoped_lock lock(connectivityMutex);

    // check if proxy already exists
    DependencyMap::iterator it = connectivity.dependencies.find(name);

    if (it == connectivity.dependencies.end())
    {
        return false;
    }

    ARMARX_VERBOSE << "Removing proxy '" << name << "' from dependency list.";
    connectivity.dependencies.erase(it);
    getObjectScheduler()->wakeupDependencyCheck();
    return true;
}

// *******************************************************
// getters
// *******************************************************
ArmarXManagerPtr ManagedIceObject::getArmarXManager() const
{
    return armarXManager;
}

IceManagerPtr ManagedIceObject::getIceManager() const
{
    return iceManager;
}


Profiler::ProfilerPtr ManagedIceObject::getProfiler() const
{
    return profiler;
}

void ManagedIceObject::enableProfiler(bool enable)
{
    Profiler::LoggingStrategyPtr strategy(new Profiler::LoggingStrategy);
    // set to empty log strategy if enable == false
    enableProfilerFunction = &ManagedIceObject::Noop;

    if (!enable)
    {
        profiler->setLoggingStrategy(strategy);
        return;
    }

    // enable the IceLoggingStrategy if IceManager is available
    // otherwise defer the creation until ManagedIceObject::init() is called
    if (getIceManager())
    {
        // create the IceLoggingStrategy immediately if Ice is available
        //enableProfilerFunction(this);
        ManagedIceObject::EnableProfilerOn(this);
    }
    else
    {
        // if Ice is not yet available, the creation of IceLoggingStrategy is delayed via the enableProfilerFunction
        enableProfilerFunction = &ManagedIceObject::EnableProfilerOn;
    }
}


Ice::ObjectPrx ManagedIceObject::getProxy(long timeoutMs, bool waitForScheduler) const
{
    ArmarXObjectSchedulerPtr oSched = getObjectScheduler();
    IceUtil::Time startTime = TimeUtil::GetTime(true);
    if (waitForScheduler)
    {
        while (!oSched && ((TimeUtil::GetTime(true) - startTime).toMilliSecondsDouble() < timeoutMs || timeoutMs < 0))
        {
            oSched = getObjectScheduler();
            TimeUtil::USleep(10000);
        }
    }
    if (oSched)
    {
        oSched->waitForObjectStateMinimum(eManagedIceObjectStarting, timeoutMs);
    }
    else
    {
        ARMARX_WARNING_S << "called on an object without a object scheduler. To fix this add the object to an ArmarXManager prior to calling this function.";
    }
    return proxy;
}

// *******************************************************
// termination
// *******************************************************
void ManagedIceObject::terminate()
{
    stateCondition.notify_all();
    objectScheduler->terminate();
}

void ManagedIceObject::setName(std::string name)
{
    this->name = name;
}

Ice::CommunicatorPtr ManagedIceObject::getCommunicator() const
{
    return getIceManager()->getCommunicator();
}

// *******************************************************
// phase handling
// *******************************************************
void ManagedIceObject::init(IceManagerPtr iceManager)
{
    // set state
    setObjectState(eManagedIceObjectInitializing);

    // set ice manager
    this->iceManager = iceManager;

    // evaluate function created by enableProfiling();
    enableProfilerFunction(this);

    // call framwork hook
    onInitComponent();

    // set state
    setObjectState(eManagedIceObjectInitialized);
}

void ManagedIceObject::start(Ice::ObjectPrx& proxy, const Ice::ObjectAdapterPtr& objectAdapter)
{
    // set members
    this->proxy = proxy;
    this->objectAdapter = objectAdapter;

    // set state
    setObjectState(eManagedIceObjectStarting);

    // call framwork hook
    onConnectComponent();

    // set state
    setObjectState(eManagedIceObjectStarted);
}

void ManagedIceObject::disconnect()
{
    // set state

    try
    {
        // call framwork hook
        onDisconnectComponent();
        // set state
    }
    catch (...) // dispatch and handle exception
    {
        handleExceptions();
    }
    setObjectState(eManagedIceObjectInitialized);

}

void ManagedIceObject::exit()
{
    // set state
    setObjectState(eManagedIceObjectExiting);

    Ice::Identity id;
    id.name = getName();

    // remove self so no new calls are coming in
    try
    {
        if (getObjectAdapter())
        {
            getObjectAdapter()->remove(id);
        }
    }
    catch (Ice::ObjectAdapterDeactivatedException& e)
    {
    }
    catch (Ice::NotRegisteredException& e)
    {
    }

    try
    {
        // call framwork hook
        onExitComponent();
    }
    catch (...) // dispatch and handle exception
    {
        handleExceptions();
    }
    // set state
    setObjectState(eManagedIceObjectExited);
    armarXManager = nullptr;
    iceManager = nullptr;
    objectScheduler = nullptr;
    objectAdapter = nullptr;
    proxy = nullptr;
    ARMARX_VERBOSE << "Object '" << this->getName() << "' is finished";
}

void ManagedIceObject::setObjectState(ManagedIceObjectState newState)
{
    ScopedLock lock(objectStateMutex);

    if (newState == objectState)
    {
        return;
    }

    objectState = newState;
    stateCondition.notify_all();
}

void ManagedIceObject::Noop(ManagedIceObject*)
{
    // NOOP
}

void ManagedIceObject::EnableProfilerOn(ManagedIceObject* object)
{
    object->offeringTopic(armarx::Profiler::PROFILER_TOPIC_NAME);
    ProfilerListenerPrx profilerTopic = object->getIceManager()->getTopic<ProfilerListenerPrx>(armarx::Profiler::PROFILER_TOPIC_NAME);
    Profiler::LoggingStrategyPtr strategy(new Profiler::IceLoggingStrategy(profilerTopic));
    object->profiler->setLoggingStrategy(strategy);
    ARMARX_INFO_S << "Profiler enabled for " << object->getName();
}

ManagedIceObjectState ManagedIceObject::getState() const
{
    ScopedLock lock(objectStateMutex);
    return objectState;
}

ArmarXObjectSchedulerPtr ManagedIceObject::getObjectScheduler() const
{
    return objectScheduler;
}


ManagedIceObjectConnectivity ManagedIceObject::getConnectivity() const
{
    boost::mutex::scoped_lock lock(connectivityMutex);
    return connectivity;
}


void armarx::ManagedIceObject::waitForObjectScheduler()
{
    getObjectScheduler()->waitForDependencies();
}

void ManagedIceObject::setMetaInfo(const std::string& id, const VariantBasePtr& value)
{
    ScopedLock lock(metaInfoMapMutex);
    metaInfoMap[id] = value;
}

VariantBasePtr ManagedIceObject::getMetaInfo(const std::string& id)
{
    ScopedLock lock(metaInfoMapMutex);
    return metaInfoMap[id];
}

StringVariantBaseMap ManagedIceObject::getMetaInfoMap() const
{
    ScopedLock lock(metaInfoMapMutex);
    return metaInfoMap;
}
