/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::application::ArmarXTimeserver
 * @author     Clemens Wallrath ( uagzs at student dot kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */

#pragma once

#include <IceUtil/Time.h>

namespace armarx
{
    /**
     * @class TimeKeeper
     * @brief The TimeKeeper class tracks the passing of time and allows to stop it, restart it, and adjust its speed (only used by ArmarXTimeServer).
     * @ingroup VirtualTime
     *
     * Internally the current time is represented as a combination of start time, speed and an offset.
     * The current time (i.e. the output of getTime()) is always `(IceUtil::Time::now() - startTime) * speed + offset`.
     * When the timer is paused and unpaused or the speed changes, `offset` gets increased by `(IceUtil::Time::now() - starttime) * speed`
     * and `starttime` gets reset to the current time.
     */
    class TimeKeeper
    {
    public:
        TimeKeeper();
        /**
         * @brief starts the clock
         */
        void start();
        /**
         * @brief stops the clock. This does not reset the clock.
         */
        void stop();
        /**
         * @brief resets the clock
         */
        void reset();
        /**
         * @brief get the current time of this clock
         * @return the current time. In most cases this should be interpreted as a timedelta, not a datetime.
         */
        IceUtil::Time getTime() const;
        /**
         * @brief sets the speed factor of the clock
         * @param newSpeed new speed factor
         *
         * Sets how fast the clock is running.
         * e.g. a value of 0.5 means that the clock only ticks with half the speed of the system clock.
         */
        void setSpeed(float newSpeed);
        float getSpeed();
        /**
         * @brief adds a timedelta to the current time
         * @param stepSize how much to add
         */
        void step(IceUtil::Time stepSize);

    private:
        /**
         * @brief offset that is added to the current time
         */
        IceUtil::Time offset;
        /**
         * @brief the time the clock was started (in local system time)
         */
        IceUtil::Time startTime;
        /**
         * @brief speed factor for the clock
         */
        float speed;
        /**
         * @brief if the clock is running
         */
        bool running;
    };
}

