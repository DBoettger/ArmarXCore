/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Clemens Wallrath ( uagzs at student dot kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */

#include <Ice/PropertiesF.h>            // for upCast
#include <boost/thread/lock_guard.hpp>  // for lock_guard
#include <boost/thread/lock_types.hpp>  // for unique_lock
#include <boost/thread/pthread/condition_variable.hpp>

#include "../application/Application.h"  // for Application, etc
#include "ArmarXCore/core/time/../application/properties/Property.h"
#include "ArmarXCore/core/time/LocalTimeServer.h"
#include "ArmarXCore/core/time/TimeUtil.h"  // for TimeUtil
#include "ArmarXCore/interface/core/UserException.h"
#include "Timer.h"

namespace armarx
{

    Timer::Timer(bool forceSystemTime):
        called(false)
    {

        useSystemTime = forceSystemTime || !Application::getInstance() || !Application::getInstance()->getProperty<bool>("UseTimeServer").getValue();

        if (useSystemTime)
        {
            iceTimer = new IceUtil::Timer;
        }
        else
        {
            running = true;
            start();
        }
    }

    void Timer::schedule(const IceUtil::TimerTaskPtr& task, const IceUtil::Time& interval)
    {

        if (useSystemTime)
        {
            iceTimer->schedule(task, interval);
        }
        else
        {
            IceUtil::Time endTime = TimeUtil::GetTime() + interval;
            {
                boost::mutex::scoped_lock lock(scheduledTasksMutex);
                scheduledTasks.push_back(ScheduledTask {task, endTime});
            }
            TimeUtil::GetTimeServer()->registerTimer(endTime, static_cast<CallbackReceiver*>(this));
        }
    }

    void Timer::scheduleRepeated(const IceUtil::TimerTaskPtr& task, const IceUtil::Time& interval)
    {
        throw NotImplementedYetException("scheduleReleated: function not yet implemented.");
    }

    bool Timer::cancel(const IceUtil::TimerTaskPtr& task)
    {
        if (useSystemTime)
        {
            iceTimer->cancel(task);
        }

        bool found = false;
        {
            boost::mutex::scoped_lock lock(scheduledTasksMutex);

            for (std::vector<ScheduledTask>::iterator it = scheduledTasks.begin(); it != scheduledTasks.end();)
            {
                if (it->task.get() == task.get())
                {
                    it = scheduledTasks.erase(it);
                    found = true;
                }
                else
                {
                    ++it;
                }
            }
        }

        return found;
    }

    void Timer::run()
    {

        boost::unique_lock<boost::mutex> lockWait(callbackWaitMutex);

        while (running)
        {
            std::vector<IceUtil::TimerTaskPtr> tasksToRun;
            {
                boost::mutex::scoped_lock lock(scheduledTasksMutex);

                for (std::vector<ScheduledTask>::iterator it = scheduledTasks.begin(); it != scheduledTasks.end();)
                {
                    if (it->endTime < TimeUtil::GetTime())
                    {
                        tasksToRun.emplace_back(it->task);
                        it = scheduledTasks.erase(it);
                    }
                    else
                    {
                        ++it;
                    }
                }
            }

            for (std::vector<IceUtil::TimerTaskPtr>::iterator it = tasksToRun.begin(); it != tasksToRun.end(); ++it)
            {
                (*it)->runTimerTask();
            }

            while (!called)
            {
                condWait.wait(lockWait); // wait for unlock via call() from LocalTimeServer
            }
            called = false;
        }
    }

    void Timer::call()
    {
        {
            boost::lock_guard<boost::mutex> lock(callbackWaitMutex);
            called = true;
        }
        condWait.notify_all();
    }

    bool Timer::getUseSystemTime() const
    {
        return useSystemTime;
    }

    void Timer::destroy()
    {
        //TODO: no schedule() after destroy()

        if (useSystemTime)
        {
            iceTimer->destroy();
            return;
        }
        else
        {
            TimeUtil::GetTimeServer()->unregisterTimer(static_cast<CallbackReceiver*>(this));
        }

        if (!isAlive())
        {
            return;
        }

        running = false;
        call(); //unlock the timer thread

        if (IceUtil::ThreadControl().id() == getThreadControl().id()) // called from Timer thread
        {
            getThreadControl().detach();
        }
        else // called from another thread
        {
            getThreadControl().join();
        }
    }

    Timer::~Timer()
    {
        destroy();

        if (scheduledTasksMutex.try_lock())
        {
            scheduledTasksMutex.unlock();
        }
        else
        {
        }

        if (callbackWaitMutex.try_lock())
        {
            callbackWaitMutex.unlock();
        }
        else
        {
        }

    }
}
