/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Clemens Wallrath ( uagzs at student dot kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */

#pragma once

#include <IceUtil/Handle.h>             // for Handle
#include <IceUtil/Shared.h>             // for Shared
#include <IceUtil/Thread.h>             // for Thread
#include <IceUtil/Time.h>               // for Time
#include <IceUtil/Timer.h>              // for TimerTaskPtr, TimerPtr
#include <boost/thread/pthread/condition_variable_fwd.hpp>
#include <boost/thread/pthread/mutex.hpp>  // for mutex
#include <vector>                       // for vector

#include "LocalTimeServer.h"            // for CallbackReceiver

namespace armarx
{
    typedef struct
    {
        IceUtil::TimerTaskPtr task;
        IceUtil::Time endTime;
    } ScheduledTask;

    /**
       @class Timer
     * @brief Timer implementation with TimeServer support
     * @ingroup VirtualTime
     *
     * The Timer class provides a timer following the same interface as
     * IceUtil::Timer, but adding the option to use time from a TimeServer.
     *
     * Using system time for scheduling is still possible using forceSystemTime=true
     * in the constructor.
     */
    class Timer :
        public virtual IceUtil::Shared,
        private virtual IceUtil::Thread,
        public CallbackReceiver
    {
    public:
        /**
         * @brief constructs a new Timer and starts its execution thread.
         * @param forceSystemTime if set to true, system time will be used even if a TimeServer is available
         */
        Timer(bool forceSystemTime = false);

        ~Timer() override;

        /**
         * @brief destroys the Timer and detaches the exection thread if
         * the calling thread is the timer thread, joins the thread otherwise
         *
         * The Timer must not be used to schedule anything new after a call to `destroy()`!
         */
        void destroy();

        /**
         * @brief schedules a task for execution
         * @param task the task to execute (an object extending IceUtil::TimerTask
         * @param interval how long to wait before the task is run
         */
        void schedule(const IceUtil::TimerTaskPtr& task, const IceUtil::Time& interval);

        /**
         * @brief schedules a task for repeated execution
         * @param task the task to execute (an object extending IceUtil::TimerTask
         * @param interval the interval in which the task is executed
         */
        void scheduleRepeated(const IceUtil::TimerTaskPtr& task, const IceUtil::Time& interval);

        /**
         * @brief cancels a task, returns true if the task was successfully
         * canceled (i.e. was found in the queue), false otherwise
         * @param task task to cancel
         * @return if cancelling was successful
         */
        bool cancel(const IceUtil::TimerTaskPtr& task);

        /**
         * @brief wakes up the execution thread to check if a task has to run
         */
        void call() override;

        /**
         * @return true if system time is used, false if VirtualTime is used.
         */
        bool getUseSystemTime() const;

    protected:
        /**
         * @brief if we are using the system time (or the TimeServer time)
         */
        bool useSystemTime;

        /**
         * @brief used for waiting for a callback from the LocalTimeServer
         */
        boost::mutex callbackWaitMutex;

        /**
         * @brief used for waiting for a callback from the LocalTimeServer
         */
        boost::condition_variable condWait;

        /**
         * @brief used for locking scheduledTasks
         */
        boost::mutex scheduledTasksMutex;

        /**
         * @brief set to false to stop the execution thread
         */
        bool running;

        /**
         * @brief if call() has been called. Used together with condWait.
         */
        bool called;

        /**
         * @brief timer for use in system time mode
         */
        IceUtil::TimerPtr iceTimer;

        /**
         * @brief list of scheduled tasks
         */
        std::vector<ScheduledTask> scheduledTasks;

        /**
         * @brief the execution thread main method
         */
        void run() override;
    };
    /**
     * @brief smart pointer for armarx::Timer
     */
    typedef IceUtil::Handle<Timer> TimerPtr;
}

