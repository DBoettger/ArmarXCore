/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Clemens Wallrath ( uagzs at student dot kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */

#pragma once


#include "../ManagedIceObject.h"
#include <ArmarXCore/interface/core/TimeServerInterface.h>

namespace armarx
{
    /**
     * @class CallbackReceiver
     * @brief Used by CallbackWaitLock
     * @ingroup VirtualTime
     */
    class CallbackReceiver
    {
    public:
        virtual ~CallbackReceiver() {}

        virtual void call() = 0;
    };
    typedef boost::shared_ptr<CallbackReceiver> CallbackReceiverPtr;

    typedef struct
    {
        IceUtil::Time endTime;
        CallbackReceiver* callback;
    } RegisteredTimer;

    class LocalTimeServer;
    typedef IceInternal::Handle<LocalTimeServer> LocalTimeServerPtr;

    /**
     * @class LocalTimeServer
     * @brief A local time server that gets its time from the MasterTimeServer
     * @ingroup VirtualTime
     *
     * The LocalTimeServer runs as a component in every application and offers the time it gets from the MasterTimeServer to the other compnents.
     */
    class LocalTimeServer :
        virtual public TimeServerRelay,
        virtual public ManagedIceObject
    {
    public:
        /**
         * @see armarx::ManagedIceObject::getDefaultName()
         */
        std::string getDefaultName() const override;

        void reportTime(::Ice::Long, const ::Ice::Current& = ::Ice::Current()) override;

        Ice::Long getTime(const ::Ice::Current& = ::Ice::Current()) override;
        void stop(const ::Ice::Current& = ::Ice::Current()) override;
        void start(const ::Ice::Current& = ::Ice::Current()) override;
        void step(const ::Ice::Current& = ::Ice::Current()) override;
        void setSpeed(Ice::Float newSpeed, const ::Ice::Current& = ::Ice::Current()) override;
        Ice::Float getSpeed(const ::Ice::Current& = ::Ice::Current()) override;
        Ice::Int getStepTimeMS(const ::Ice::Current& = ::Ice::Current()) override;

        /**
         * @brief register a callack to call at endTime
         * @param endTime when to call
         * @param callback whom to call (has to implement CallbackReceiver)
         *
         * Register a callback that is called when the endTime is reached. Always takes at least one timeserver tick when virtual time is active
         *
         * A registered timer _must_ be unregistered if it is destroyed before the endTime is reached!
         */
        void registerTimer(IceUtil::Time endTime, CallbackReceiver* callback);

        /**
         * @brief unregister a timer
         * @param the CallbackReceiver to unregister
         *
         * Cancels all scheduled callbacks registered by the given CallbackReceiver
         */
        void unregisterTimer(CallbackReceiver* callback);

        /**
         * @brief Get the applications LocalTimeServer instance
         * @return The applications LocalTimeServer instance
         */
        static LocalTimeServerPtr getApplicationTimeServer();

    protected:
        /**
         * Please use LocalTimeServer::getApplicationtimeserver() to access your local timeserver.
         */
        LocalTimeServer();

        /**
         * @brief the current time
         */
        IceUtil::Time time;
        SharedMutex timeMutex;

        /**
         * @brief a handle for the MasterTimeServer
         */
        TimeServerInterfacePrx timeServerPrx;

        /**
         * @brief a handle for the topic "Time"
         */
        TimeServerListenerPrx timeTopicPrx;

        /**
         * @see armarx::ManagedIceObject::onInitComponent()
         */
        void onInitComponent() override;

        /**
         * @see armarx::ManagedIceObject::onConnectComponent()
         */
        void onConnectComponent() override;
        void onDisconnectComponent() override;

        static void setApplicationTimeServerName(const std::string& name);

        /**
         * @brief used for locking scheduledTasks
         */
        boost::mutex scheduledTasksMutex;
        std::vector<RegisteredTimer> scheduledTasks;

        friend class ArmarXManager;
    };
}

