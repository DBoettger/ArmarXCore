/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Clemens Wallrath ( uagzs at student dot kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */
#include <algorithm>

#include "LocalTimeServer.h"


using namespace armarx;

LocalTimeServer::LocalTimeServer()
{
    time = IceUtil::Time::milliSeconds(0);
}

std::string LocalTimeServer::getDefaultName() const
{
    return "LocalTimeServer";
}

void LocalTimeServer::onInitComponent()
{
    usingProxy(GLOBAL_TIMESERVER_NAME);
    usingTopic(TIME_TOPIC_NAME);
}


void LocalTimeServer::onConnectComponent()
{
    timeServerPrx = getProxy<TimeServerInterfacePrx>(GLOBAL_TIMESERVER_NAME);
    timeTopicPrx = armarx::ManagedIceObject::getTopic<TimeServerListenerPrx>(TIME_TOPIC_NAME);
}

void LocalTimeServer::onDisconnectComponent()
{
    timeServerPrx = nullptr;
}


void LocalTimeServer::reportTime(::Ice::Long timestamp, const Ice::Current&)
{
    IceUtil::Time tempTime = IceUtil::Time::milliSeconds(timestamp);
    // if the new time is before the current time we have to subtract the delta from all threads sleeping
    // to prevent them from waiting too long.
    // if we don't do this, the following can happen:
    // global time = 10min
    // thread sleeps for 10ms (-> until time is 10min + 10ms)
    // reset of timeserver -> global time = 0
    // thread wakes up after 10min + 10ms
    IceUtil::Time timeDeduction;
    {
        boost::unique_lock<SharedMutex> lock(timeMutex);
        timeDeduction = tempTime - this->time;
        this->time = tempTime;
    }
    if (timeDeduction.toMicroSeconds() > 0)
    {
        //the time progresses monotonically
        timeDeduction = IceUtil::Time::microSeconds(0);
    }

    if (scheduledTasksMutex.try_lock())
    {
        for (std::vector<RegisteredTimer>::iterator it = scheduledTasks.begin(); it != scheduledTasks.end();)
        {
            it->endTime += timeDeduction;
            if (it->endTime < tempTime)
            {
                CallbackReceiver* callback = it->callback;
                it = scheduledTasks.erase(it);
                callback->call();
            }
            else
            {
                ++it;
            }
        }

        scheduledTasksMutex.unlock();
    }
}

Ice::Long LocalTimeServer::getTime(const ::Ice::Current&)
{
    boost::shared_lock<SharedMutex> lock(timeMutex);
    return static_cast<Ice::Long>(time.toMilliSeconds());
}

LocalTimeServerPtr LocalTimeServer::getApplicationTimeServer()
{
    static LocalTimeServerPtr applicationTimeServer(new LocalTimeServer());
    return applicationTimeServer;
}

void LocalTimeServer::setApplicationTimeServerName(const std::string& name)
{
    getApplicationTimeServer()->setName(name);
}

void LocalTimeServer::stop(const ::Ice::Current&)
{
    timeServerPrx->stop();
}

void LocalTimeServer::start(const ::Ice::Current&)
{
    timeServerPrx->start();
}

void LocalTimeServer::step(const ::Ice::Current&)
{
    timeServerPrx->step();
}

void LocalTimeServer::setSpeed(Ice::Float newSpeed, const ::Ice::Current&)
{
    timeServerPrx->setSpeed(newSpeed);
}

Ice::Float LocalTimeServer::getSpeed(const Ice::Current&)
{
    return timeServerPrx->getSpeed();
}

void LocalTimeServer::registerTimer(IceUtil::Time endTime, CallbackReceiver* callback)
{
    boost::lock_guard<boost::mutex> lock(scheduledTasksMutex);
    scheduledTasks.push_back(RegisteredTimer {endTime, callback});
}

void LocalTimeServer::unregisterTimer(CallbackReceiver* callback)
{
    boost::lock_guard<boost::mutex> lock(scheduledTasksMutex);
    scheduledTasks.erase(std::remove_if(scheduledTasks.begin(), scheduledTasks.end(), [callback](RegisteredTimer t)
    {
        return t.callback == callback;
    }), scheduledTasks.end());
}

Ice::Int LocalTimeServer::getStepTimeMS(const ::Ice::Current&)
{
    return timeServerPrx->getStepTimeMS();
}
