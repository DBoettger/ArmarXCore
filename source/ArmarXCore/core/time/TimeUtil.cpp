/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarXCore::core
* @author     Clemens Wallrath (uagzs at student dot kit dot edu)
* @date       2015
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "TimeUtil.h"

#include "CallbackWaitLock.h"
#include "LocalTimeServer.h"
#include <time.h>


#define USECS_PER_SEC     1000000
#define NANOSECS_PER_SEC  1000000000



using namespace armarx;

LocalTimeServerPtr TimeUtil::timeServerPtr = nullptr;

IceUtil::Time TimeUtil::GetTime(bool forceSystemTime)
{
    if (!forceSystemTime && timeServerPtr)
    {
        return IceUtil::Time::milliSeconds(timeServerPtr->getTime());
    }
    else
    {
        return IceUtil::Time::now();
    }
}

void TimeUtil::Sleep(IceUtil::Time duration)
{
    if (duration.toMicroSeconds() <= 0)
    {
        return;
    }
    if (timeServerPtr)
    {
        CallbackWaitLock waitLock;
        timeServerPtr->registerTimer(GetTime() + duration, static_cast<CallbackReceiver*>(&waitLock));
        waitLock.wait();
    }
    else
    {
        USleep(duration.toMicroSeconds());
    }
}

void TimeUtil::MSSleep(int durationMS)
{
    Sleep(IceUtil::Time::milliSeconds(durationMS));
}

void TimeUtil::SetTimeServer(LocalTimeServerPtr ts)
{
    timeServerPtr = ts;
}

void TimeUtil::WaitForNextTick()
{
    MSSleep(0); // even duration=0 will block until next tick, if timeserver active
}

LocalTimeServerPtr TimeUtil::GetTimeServer()
{
    return timeServerPtr;
}

bool TimeUtil::HasTimeServer()
{
    return timeServerPtr._ptr != nullptr;
}

bool TimeUtil::TimedWait(boost::condition_variable& cond, boost::unique_lock<boost::mutex>& lock, IceUtil::Time duration, IceUtil::Time granularity)
{
    if (timeServerPtr)
    {
        auto endTime = GetTime() + duration;
        boost::posix_time::time_duration td;
        if (granularity.toMicroSeconds() == 0)
        {
            td = boost::posix_time::microseconds((duration / 10).toMicroSeconds()); // assumption: granularity of a tenth of the total duration is ok-ish
        }
        else
        {
            td = boost::posix_time::microseconds(granularity.toMicroSeconds());
        }
        while (!cond.timed_wait(lock, td))
        {
            if (GetTime() > endTime)
            {
                return false;
            }
        }
        return true;
    }
    else
    {
        return cond.timed_wait(lock, boost::posix_time::microseconds(duration.toMicroSeconds()));
    }
}

int TimeUtil::USleep(long usec)
{
    return NanoSleep(usec * 1000);
}

int TimeUtil::NanoSleep(long nanosec)
{
    struct timespec ts;
    ts.tv_sec = nanosec / NANOSECS_PER_SEC;
    ts.tv_nsec = (nanosec % NANOSECS_PER_SEC);
    return nanosleep(&ts, nullptr);
}

