/*
 * This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Clemens Wallrath ( uagzs at student dot kit dot edu )
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */

#pragma once

#include "LocalTimeServer.h"

#include <boost/thread/mutex.hpp>
#include <boost/thread/condition_variable.hpp>

namespace armarx
{
    /**
     * @class CallbackWaitLock
     * @brief CallbackWaitLock is used to block a thread and unblock it from another thread via callback
     * @ingroup VirtualTime
     *
     * When wait() is called, the calling thread is blocked until call() is called from another thread.
     * This is mainly intended as helper for TimeUtil::Sleep(,,,).
     */
    class CallbackWaitLock :
        public CallbackReceiver
    {
    public:
        CallbackWaitLock();

        void call() override;

        /**
         * @brief wait blocks the calling thread until CallbackWaitLock::call() is called.
         */
        void wait();

    protected:
        boost::condition_variable condWait;
        boost::mutex waitMutex;
        /**
         * whether call() was called (to prevent race conditions)
         */
        bool called;
    };
}
