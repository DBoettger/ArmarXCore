/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::application::ArmarXTimeserver
 * @author     Clemens Wallrath ( uagzs at student dot kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */

#include "TimeKeeper.h"

using namespace armarx;

TimeKeeper::TimeKeeper()
{
    reset();
    running = false;
}

IceUtil::Time TimeKeeper::getTime() const
{

    if (running)
    {
        return offset + (IceUtil::Time::now() - startTime) * speed;
    }
    else
    {
        return offset;
    }
}

void TimeKeeper::reset()
{
    offset = IceUtil::Time::microSeconds(0);
    startTime = IceUtil::Time::now();
    speed = 1;
}

void TimeKeeper::setSpeed(float newSpeed)
{
    offset = getTime();
    startTime = IceUtil::Time::now();
    speed = newSpeed;
}

float TimeKeeper::getSpeed()
{
    return speed;
}

void TimeKeeper::start()
{
    if (!running)
    {
        startTime = IceUtil::Time::now();
        running = true;
    }
}

void TimeKeeper::stop()
{
    if (running)
    {
        offset = getTime();
        running = false;
    }
}

void TimeKeeper::step(IceUtil::Time stepSize)
{
    offset += stepSize;
}
