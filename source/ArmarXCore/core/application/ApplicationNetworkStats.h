/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarXCore::core::application
* @author     Manfred Kroehnert (manfred dot kroehnert at kit dot edu)
* @date       2015
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <ArmarXCore/interface/core/Profiler.h>

namespace armarx
{
    template<class Functor>
    class SimplePeriodicTask;
    namespace detail
    {
        struct ArmarXConnectionObserver;
    }

    /**
     * \brief The ApplicationNetworkStats class implements the Ice::Instrumentation::CommunicatorObserver interface to meassure network traffic.
     *
     * It accumulates the amount of bytes received and sent over network and periodically publishes them
     * over the IceStorm topic set via ApplicationNetworkStats::setProfilerTopic().
     *
     * The Stats object must be added to the Ice::InitializationData object used during Ice initialization.
     * This is done in armarx::Application::doMain()
     */
    class ApplicationNetworkStats : public Ice::Instrumentation::CommunicatorObserver
    {
    public:

        ApplicationNetworkStats();
        ~ApplicationNetworkStats() override;

        void setProfilerTopic();
        void setApplicationName();
        void start(ProfilerListenerPrx profiler, const std::string& applicationName);
        void stopTask();

        Ice::Instrumentation::ConnectionObserverPtr
        getConnectionObserver(const Ice::ConnectionInfoPtr& c, const Ice::EndpointPtr& e,
                              Ice::Instrumentation::ConnectionState s,
                              const Ice::Instrumentation::ConnectionObserverPtr& previous) override;

        Ice::Instrumentation::ObserverPtr getConnectionEstablishmentObserver(const Ice::EndpointPtr&, const std::string&) override;
        Ice::Instrumentation::ObserverPtr getEndpointLookupObserver(const Ice::EndpointPtr&) override;
        Ice::Instrumentation::ThreadObserverPtr getThreadObserver(
            const std::string&,
            const std::string&,
            Ice::Instrumentation::ThreadState,
            const Ice::Instrumentation::ThreadObserverPtr&) override;
        Ice::Instrumentation::InvocationObserverPtr getInvocationObserver(const Ice::ObjectPrx&, const std::string&, const Ice::Context&) override;
        Ice::Instrumentation::DispatchObserverPtr getDispatchObserver(const Ice::Current&, Ice::Int) override;
        void setObserverUpdater(const Ice::Instrumentation::ObserverUpdaterPtr& upd) override;
    private:
        IceUtil::Handle<SimplePeriodicTask<std::function<void(void)>> > reportNetworkTrafficTask;
        const int REPORT_TIME_MS
        {
            1000
        };

        //we need to manually list every protocol since ice only tells us an id as protocol type and no string
        IceInternal::Handle<detail::ArmarXConnectionObserver> observerTCP;
        IceInternal::Handle<detail::ArmarXConnectionObserver> observerUDP;
        IceInternal::Handle<detail::ArmarXConnectionObserver> observerSSL;
        IceInternal::Handle<detail::ArmarXConnectionObserver> observerUnknown;
    };
    using ApplicationNetworkStatsPtr = ::IceUtil::Handle<ApplicationNetworkStats>;
}

