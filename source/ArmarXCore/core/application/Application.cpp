/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Nils Adermann (naderman at naderman dot de)
 * @author     Kai Welke (welke at kit dot edu)
 * @author     Jan Issac (jan dot issac at gmail dot com)
 * @author     Mirko Waechter (waechter at kit dot edu)
 * @author     Manfred Kroehnert (manfred dot kroehnert at kit dot edu)
 * @date       2010
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include <Ice/Communicator.h>           // for Communicator, etc
#include <Ice/Initialize.h>             // for InitializationData
#include <Ice/LocalException.h>         // for FileException, etc
#include <Ice/ObjectF.h>                // for upCast
#include <Ice/Process.h>                // for ProcessPtr, Process
#include <Ice/Properties.h>             // for Properties, PropertiesPtr
#include <Ice/PropertiesAdmin.h>        // for PropertyDict
#include <IceUtil/Time.h>               // for Time
#include <boost/algorithm/string/case_conv.hpp>  // for to_lower_copy, etc
#include <boost/algorithm/string/classification.hpp>  // for is_any_of
#include <boost/algorithm/string/constants.hpp>
#include <boost/algorithm/string/detail/classification.hpp>
#include <boost/algorithm/string/replace.hpp>  // for replace_all
#include <boost/algorithm/string/split.hpp>  // for split
#include <boost/filesystem/operations.hpp>  // for exists
#include <boost/filesystem/path.hpp>    // for path, operator/
#include <stddef.h>                     // for size_t
#include <stdlib.h>                     // for NULL, getenv, system, exit, etc
#include <unistd.h>                     // for getpid
#include <iostream>                     // for operator<<, ostream, etc
#include <sstream>                      // for basic_stringbuf<>::int_type, etc
#include <thread>                       // for thread
#include <utility>                      // for pair, move
#include <fstream>

#include "../logging/ArmarXLogBuf.h"    // for ArmarXLogBuf
#include "Application.h"
#include "ApplicationOptions.h"         // for Options, showHelp, etc
#include "ApplicationProcessFacet.h"    // for ArmarXManagerPtr, etc
#include "ApplicationNetworkStats.h"    // for ApplicationNetworkStatsPtr
#include "ArmarXCore/core/IceManager.h"  // for IceManager
#include "ArmarXCore/core/ManagedIceObject.h"
#include "ArmarXCore/core/application/../ArmarXDummyManager.h"
#include "ArmarXCore/core/application/../ArmarXManager.h"
#include "ArmarXCore/core/application/../system/ArmarXDataPath.h"
#include "ArmarXCore/core/application/../system/cmake/CMakePackageFinder.h"
#include "ArmarXCore/core/application/ApplicationNetworkStats.h"
#include "ArmarXCore/core/application/properties/Property.h"
#include "ArmarXCore/core/application/properties/PropertyDefinition.h"
#include "ArmarXCore/core/application/properties/PropertyDefinition.hpp"
#include "ArmarXCore/core/application/properties/PropertyUser.h"
#include "ArmarXCore/core/logging/LogSender.h"  // for LogSender
#include "ArmarXCore/core/logging/Logging.h"  // for ARMARX_INFO_S, etc
#include "ArmarXCore/interface/core/Log.h"  // for MessageType, etc
#include "ArmarXCore/interface/core/Profiler.h"
#include "properties/IceProperties.h"   // for IceProperties

#ifndef WIN32
#include <ArmarXCore/core/util/OnScopeExit.h>  // for ARMARX_ON_SCOPE_EXIT
#include <signal.h>                     // for signal, SIGABRT, SIGSEGV, etc
#include <execinfo.h>
#include <cstdio>
#endif
#include <ArmarXCore/core/services/tasks/ThreadPool.h>
#include <ArmarXCore/core/system/DynamicLibrary.h>
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>


using namespace armarx;

// static members for the one application instance (maybe not required anymore???)
Mutex Application::instanceMutex;
ApplicationPtr Application::instance;
Ice::StringSeq Application::ProjectDependendencies;
std::string Application::ProjectName;
const std::string Application::ArmarXUserConfigDirEnvVar = "ARMARX_USER_CONFIG_DIR";
std::string errormsg = "Segmentation fault - Backtrace: \n";
bool crashed = false;
#include <Ice/Properties.h>
#include <Ice/PropertiesAdmin.h>

namespace armarx
{
    class IcePropertyChangeCallback :
        public Ice::PropertiesAdminUpdateCallback
    {
    public:
        IcePropertyChangeCallback(armarx::ApplicationPtr application) :
            application(application)
        {
        }

        void updated(const Ice::PropertyDict& changes) override
        {
            //            ARMARX_INFO << "Properties were updated: " << changes;
            application->updateIceProperties(changes);
        }
    private:
        armarx::ApplicationPtr application;
    };
    typedef IceUtil::Handle<IcePropertyChangeCallback> IcePropertyChangeCallbackPtr;
}



void Application::HandlerFault(int sig)
{
    if (crashed)
    {
        return;
    }
    crashed = true;
#ifndef WIN32
    // dont allocate memory in segfault
    if (sig == SIGSEGV)
    {
        std::array<void*, 20> array;
        size_t size;
        size = backtrace(array.data(), 20);

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-result"
        write(STDERR_FILENO, errormsg.data(), errormsg.size());
#pragma GCC diagnostic pop
        backtrace_symbols_fd(array.data(), size, STDERR_FILENO);
        exit(EXIT_FAILURE);
    }
    // print out all the frames to stderr
    std::stringstream str;

    if (sig == SIGABRT)
    {
        str << "Error: Abort\nBacktrace:\n";
    }
    else
    {
        str << "Error: signal " << sig;
    }

    ARMARX_FATAL_S << str.str() << "\nBacktrace:\n" << LogSender::CreateBackTrace();
    if ((sig == SIGSEGV || sig == SIGABRT) && Application::getInstance() && Application::getInstance()->getProperty<bool>("StartDebuggerOnCrash").getValue())
    {
        std::stringstream s;
        int res;
        //        res = system("which qtcreator");
        //        res = WEXITSTATUS(res);
        //        if (res == EXIT_SUCCESS)
        {
            s << "qtcreator -debug " << getpid();
            ARMARX_IMPORTANT << s.str();
            res = system(s.str().c_str());
            res++; // suppress warning
        }
        //        else
        //        {
        //            ARMARX_INFO_S << "Could not find qtcreator";
        //        }
    }
    exit(EXIT_FAILURE);
#endif
}

void Application::loadLibrariesFromProperties()
{
    auto loadLibString = getProperty<std::string>("LoadLibraries").getValue();
    if (!loadLibString.empty())
    {
        if (!libLoadingEnabled)
        {
            throw LocalException("Loading of dynamic libraries is not enabled in this application. The Application needs to call enableLibLoading()");
        }
        auto entries = armarx::Split(loadLibString, ";", true, true);
        for (auto& entry : entries)
        {
            try
            {
                boost::filesystem::path path;
                if (armarx::Contains(entry, "/"))
                {
                    path = entry;
                }
                else
                {
                    auto elements = Split(entry, ":");
                    ARMARX_CHECK_EQUAL(elements.size(), 2);
                    std::string libFileName = "lib" + elements.at(1) + "." + DynamicLibrary::GetSharedLibraryFileExtension();
                    CMakePackageFinder p(elements.at(0));
                    for (auto& libDir : armarx::Split(p.getLibraryPaths(), ";"))
                    {
                        boost::filesystem::path testPath(libDir);
                        testPath /= libFileName;
                        if (boost::filesystem::exists(testPath))
                        {
                            path = testPath;
                            break;
                        }

                    }
                    if (path.empty())
                    {
                        ARMARX_ERROR << "Could find library '" << libFileName << "' in any of the following paths: " << p.getLibraryPaths();
                        continue;
                    }
                }
                ARMARX_CHECK_EXPRESSION(!path.empty());
                DynamicLibrary lib;
                lib.setUnloadOnDestruct(false);
                ARMARX_VERBOSE << "Loading library " << path.string();
                lib.load(path);
            }
            catch (...)
            {
                handleExceptions();
            }
        }
    }
}

bool Application::getForbidThreadCreation() const
{
    return forbidThreadCreation;
}

void Application::setForbidThreadCreation(bool value)
{
    forbidThreadCreation = value;
    if (forbidThreadCreation)
    {
        ARMARX_INFO << "Thread creation with RunningTask and PeriodicTask is now forbidden in this process.";
    }
    else
    {
        ARMARX_INFO << "Thread creation with RunningTask and PeriodicTask is now allowed again in this process.";
    }
}

void Application::enableLibLoading(bool enable)
{
    this->libLoadingEnabled = enable;
}


void Application::HandlerInterrupt(int sig)
{
#ifndef WIN32
    ARMARX_DEBUG_S << "Caught interrupt: " << sig;

    if (Application::getInstance())
    {
        Application::getInstance()->interruptCallback(sig);
    }

#endif
}


// main entry point of Ice::Application
Application::Application()
#ifndef WIN32
    : Ice::Application(Ice::NoSignalHandling)
#endif
{
}


ApplicationPtr Application::getInstance()
{
    ScopedLock lock(instanceMutex);

    return instance;
}


void Application::setIceProperties(Ice::PropertiesPtr properties)
{
    // call base class method
    PropertyUser::setIceProperties(properties);
    // set the properties of all components
    if (armarXManager)
    {
        armarXManager->setComponentIceProperties(properties);
    }
}


void Application::updateIceProperties(const Ice::PropertyDict& properties)
{
    // call base class method
    PropertyUser::updateIceProperties(properties);
    // update the properties of all components
    if (armarXManager)
    {
        armarXManager->updateComponentIceProperties(properties);
    }
}


void Application::icePropertiesUpdated(const std::set<std::string>& changedProperties)
{
    if (armarXManager)
    {
        if (changedProperties.count("DisableLogging") && getProperty<bool>("DisableLogging").isSet())
        {
            //            ARMARX_INFO << "Logging disabled: " << getProperty<bool>("DisableLogging").getValue();
            armarXManager->enableLogging(!getProperty<bool>("DisableLogging").getValue());
        }

        if (changedProperties.count("EnableProfiling"))
        {
            armarXManager->enableProfiling(getProperty<bool>("EnableProfiling").getValue());
        }
        if (changedProperties.count("Verbosity"))
        {
            armarXManager->setGlobalMinimumLoggingLevel(getProperty<MessageType>("Verbosity").getValue());
        }
    }
}

const ThreadPoolPtr& Application::getThreadPool() const
{
    return threadPool;
}


int Application::run(int argc, char* argv[])
{
#ifndef WIN32
    // register signal handler
    signal(SIGILL, Application::HandlerFault);
    signal(SIGSEGV, Application::HandlerFault);
    signal(SIGABRT, Application::HandlerFault);

    signal(SIGHUP, Application::HandlerInterrupt);
    signal(SIGINT, Application::HandlerInterrupt);
    signal(SIGTERM, Application::HandlerInterrupt);
#endif


    // parse options and merge these with properties passed via Ice.Config
    Ice::PropertiesPtr properties = parseOptionsMergeProperties(argc, argv);

    // parse help options
    ApplicationOptions::Options options = ApplicationOptions::parseHelpOptions(properties, argc, argv);

    if (options.error)
    {
        // error in options
        return 0;
    }
    else if (options.showHelp) // display help
    {
        showHelp(options);
        return 0;
    }
    else if (options.showVersion)
    {
        std::cout << "Version: " << GetVersion() << std::endl;
        return 0;
    }
    setIceProperties(properties);

    // extract application name
    if (getProperty<std::string>("ApplicationName").isSet())
    {
        applicationName = getProperty<std::string>("ApplicationName").getValue();
    }

    // Redirect std::cout and std::cerr
    const bool redirectStdout = getProperty<bool>("RedirectStdout").getValue();
    ArmarXLogBuf buf("std::cout", eINFO, false);
    ArmarXLogBuf errbuf("std::cerr", eWARN, true);
    std::streambuf* cout_sbuf = nullptr;
    std::streambuf* cerr_sbuf = nullptr;
    if (redirectStdout)
    {
        cout_sbuf = std::cout.rdbuf();
        cerr_sbuf = std::cerr.rdbuf();
        std::cout.rdbuf(&buf);
        std::cerr.rdbuf(&errbuf);
    }
    ARMARX_ON_SCOPE_EXIT
    {
        //reset std::cout and std::cerr to original stream
        if (redirectStdout)
        {
            std::cout.rdbuf(cout_sbuf);
            std::cerr.rdbuf(cerr_sbuf);
        }
    };

    int result = 0;

    threadPool.reset(new ThreadPool(getProperty<unsigned int>("ThreadPoolSize").getValue()));
    // create the ArmarXManager and set its properties
    try
    {
        armarXManager = new ArmarXManager(applicationName, communicator());
    }
    catch (Ice::ConnectFailedException&)
    {
        return 1;
    }

    armarXManager->setDataPaths(getProperty<std::string>("DataPath").getValue());

    // set the properties again, since the armarx manager is now available
    setIceProperties(properties);

    loadDependentProjectDatapaths();

#ifdef WIN32
    // register interrupt handler
    callbackOnInterrupt();
#endif


    installProcessFacet(armarXManager);


    if (applicationNetworkStats)
    {
        ProfilerListenerPrx profilerTopic = armarXManager->getIceManager()->getTopic<ProfilerListenerPrx>(armarx::Profiler::PROFILER_TOPIC_NAME);
        applicationNetworkStats->start(profilerTopic, applicationName);
    }

    loadLibrariesFromProperties();

    try
    {
        // calls virtual setup in order to allow subclass to add components to the application
        setup(armarXManager, properties);
    }
    catch (...)
    {
        handleExceptions();
        armarXManager->shutdown();
    }

    // calls exec implementation
    result = exec(armarXManager);
    LogSender::SetSendLoggingActivated(false);

    if (shutdownThread)
    {
        shutdownThread->join();
    }

    return result;
}

/*
 * the default exec implementation. Exec is always blocking and waits for
 * shutdown
 */
int Application::exec(const ArmarXManagerPtr& armarXManager)
{
    armarXManager->waitForShutdown();

    return 0;
}


Ice::PropertiesPtr Application::parseOptionsMergeProperties(int argc, char* argv[])
{
    // parse options and merge these with properties passed via Ice.Config
    // Here, use armarx::IceProperties instead of Ice::Properties
    // to support property inheritance.
    Ice::PropertiesPtr properties = IceProperties::create(ApplicationOptions::mergeProperties(communicator()->getProperties()->clone(), argc, argv));

    // Load config files passed via ArmarX.Config
    std::string configFiles = communicator()->getProperties()->getProperty("ArmarX.Config");

    Property<std::string>::removeQuotes(configFiles, configFiles);

    // if not set, size is zero
    // if set with out value (== "1" as TRUE), size is one
    if (!configFiles.empty() && configFiles.compare("1") != 0)
    {
        std::vector<std::string> configFileList;
        boost::split(configFileList,
                     configFiles,
                     boost::is_any_of(","),
                     boost::token_compress_on);

        for (std::string configFile : configFileList)
        {
            if (!configFile.empty())
            {
                properties->load(configFile);
            }
        }
    }
    return properties;
}


void Application::showHelp(ApplicationOptions::Options& options)
{
    // perform dummy setup to register ManagedIceObjects
    ArmarXDummyManagerPtr dummyManager = new ArmarXDummyManager();

    LogSender::SetLoggingActivated(false, false);

    setup(dummyManager, getIceProperties());

    if (options.outfile.empty())
    {
        ApplicationOptions::showHelp(this, dummyManager, options, nullptr);
    }
    else
    {
        std::ofstream out(options.outfile);
        if (out.is_open())
        {
            ApplicationOptions::showHelp(this, dummyManager, options, nullptr, out);
        }
        else
        {
            options.format = ApplicationOptions::OptionsFormat::eHelpBrief;
            ApplicationOptions::showHelp(this, dummyManager, options, nullptr);

            std::cout << "Could not write to file " << options.outfile << std::endl;
        }
    }
}


std::string Application::getDomainName()
{
    return "ArmarX";
}

void Application::setName(const std::string& name)
{
    this->applicationName = name;
}

std::string Application::getName() const
{
    return applicationName;
}

void Application::interruptCallback(int signal)
{
    ARMARX_INFO_S << "Interrupt received: " << signal;
    if (applicationNetworkStats)
    {
        applicationNetworkStats->stopTask();
    }
    if (!shutdownThread)
        shutdownThread.reset(new std::thread {[this]{
            if (armarXManager)
                this->armarXManager->shutdown();
        }
    });

}

void Application::registerDataPathsFromDependencies(std::string dependencies)
{
    IceUtil::Time start = IceUtil::Time::now();
    Ice::StringSeq resultList;
    ARMARX_INFO_S << "Deps: " << dependencies;
    boost::replace_all(dependencies, "\"", "");
    boost::split(resultList,
                 dependencies,
                 boost::is_any_of("/"),
                 boost::token_compress_on);

    for (size_t i = 0; i < resultList.size(); i++)
    {
        CMakePackageFinder pack(resultList[i]);

        if (pack.packageFound())
        {
            ArmarXDataPath::addDataPaths(pack.getIncludePaths());
        }
    }

    ARMARX_INFO_S << "loading took " << (IceUtil::Time::now() - start).toMilliSeconds();
}

std::vector<std::string> Application::getDefaultPackageNames()
{
    Ice::StringSeq result = getProperty<Ice::StringSeq>("DefaultPackages").getValue();
    Ice::StringSeq additional = getProperty<Ice::StringSeq>("AdditionalPackages").getValue();
    result.insert(result.end(), additional.begin(), additional.end());

    return result;
}

std::string Application::GetArmarXConfigDefaultPath(bool envVarExpanded)
{
    std::string defaultConfigDirName = ".armarx";
    if (envVarExpanded)
    {
        char* home = getenv("HOME");
        boost::filesystem::path  absBasePath;

        if (home)
        {
            absBasePath = boost::filesystem::path(home) / defaultConfigDirName;
        }
        else
        {
            absBasePath = "~/" + defaultConfigDirName;
        }
        return absBasePath.string();
    }
    else
    {
        return "${HOME}/" + defaultConfigDirName;
    }
}

ArmarXManagerPtr Application::getArmarXManager()
{
    return armarXManager;
}


void Application::installProcessFacet(const ArmarXManagerPtr& armarXManager)
{
    Ice::CommunicatorPtr applicationCommunicator = communicator();
    // remove default Ice::Process facet
    applicationCommunicator->removeAdminFacet("Process");
    // create and register new Ice::Process facet
    Ice::ProcessPtr applicationProcessFacet = new ApplicationProcessFacet(armarXManager);
    applicationCommunicator->addAdminFacet(applicationProcessFacet, "Process");

    IcePropertyChangeCallbackPtr propertyChangeCallback = new IcePropertyChangeCallback(this);

    Ice::ObjectPtr obj = applicationCommunicator->findAdminFacet("Properties");
    Ice::NativePropertiesAdminPtr admin = Ice::NativePropertiesAdminPtr::dynamicCast(obj);
    admin->addUpdateCallback(propertyChangeCallback);

    // activate new Ice::Process facet and PropertyChangeCallback
    applicationCommunicator->getAdmin();
}


int Application::doMain(int argc, char* argv[], const Ice::InitializationData& initData)
{
    // create copy of initData so the stats object can be added
    // using const_cast leads to unexpected errors
    Ice::InitializationData id(initData);
    loadDefaultConfig(argc, argv, id);

    // enable the PropertyAdmin ObjectAdapter
    // https://doc.zeroc.com/display/Ice35/The+Administrative+Object+Adapter
    if (id.properties->getProperty("Ice.Admin.InstanceName").empty())
    {
        id.properties->setProperty("Ice.Admin.InstanceName", this->getName());
    }
    if (id.properties->getProperty("Ice.Admin.Endpoints").empty())
    {
        id.properties->setProperty("Ice.Admin.Endpoints", "tcp -h 127.0.0.1");
    }

    if (initData.properties->getProperty("ArmarX.NetworkStats") == "1")
    {
        applicationNetworkStats = new ApplicationNetworkStats();
        id.observer = applicationNetworkStats;

        const auto oldFacets = initData.properties->getPropertyAsList("Ice.Admin.Facets");
        if (oldFacets.empty())
        {
            initData.properties->setProperty("Ice.Admin.Facets", "Metrics");
        }
        else if (std::find(oldFacets.begin(), oldFacets.end(), "Metrics") == oldFacets.end())
        {
            std::stringstream str;
            str << "Metrics";
            for (const auto& facet : oldFacets)
            {
                str << "," << facet;
            }
            initData.properties->setProperty("Ice.Admin.Facets", str.str());
        }
        initData.properties->setProperty("IceMX.Metrics.NetworkStats.GroupBy", "none");
    }

    return Ice::Application::doMain(argc, argv, id);
}



Ice::StringSeq Application::GetDefaultsPaths()
{
    const std::string configFileName = "default.cfg";
    const std::string generatedConfigFileName = "default.generated.cfg";
    Ice::StringSeq defaultsPaths;
    char* defaultsPathVar = getenv(ArmarXUserConfigDirEnvVar.c_str());
    boost::filesystem::path absBasePath;
    if (defaultsPathVar)
    {
        absBasePath = defaultsPathVar;
    }
    else
    {
        absBasePath = boost::filesystem::path(GetArmarXConfigDefaultPath());
    }
    defaultsPaths.push_back((absBasePath / configFileName).string());
    defaultsPaths.push_back((absBasePath / generatedConfigFileName).string());
    return defaultsPaths;
}


void Application::loadDefaultConfig(int argc, char* argv[], const Ice::InitializationData& initData)
{
    LoadDefaultConfig(initData.properties);
}

void Application::LoadDefaultConfig(Ice::PropertiesPtr properties)
{
    const Ice::StringSeq defaultsPath = GetDefaultsPaths();
    Ice::PropertiesPtr p = IceProperties::create();

    for (std::string path : defaultsPath)
    {
        try
        {
            p->load(path);

            Ice::PropertyDict defaultCfg = p->getPropertiesForPrefix("");

            // copy default config into current config (if values are not already there)
            for (auto e : defaultCfg)
            {
                if (properties->getProperty(e.first).empty())
                {
                    properties->setProperty(e.first, e.second);
                }
            }

        }
        catch (Ice::FileException& e)
        {
            ARMARX_WARNING_S << "Loading default config failed: " << e.what();
        }
    }
}


const std::string& Application::GetProjectName()
{
    return ProjectName;
}


const Ice::StringSeq& Application::GetProjectDependencies()
{
    return ProjectDependendencies;
}


PropertyDefinitionsPtr Application::createPropertyDefinitions()
{
    return PropertyDefinitionsPtr(new ApplicationPropertyDefinitions(getDomainName()));
}


void Application::loadDependentProjectDatapaths()
{
    std::string dependenciesConfig = getIceProperties()->getProperty("ArmarX.DependenciesConfig");

    if (!dependenciesConfig.empty())
    {
        if (boost::filesystem::exists(dependenciesConfig))
        {
            Ice::PropertiesPtr prop = IceProperties::create();
            prop->load(dependenciesConfig);
            ArmarXDataPath::addDataPaths(prop->getProperty("ArmarX.ProjectDatapath"));
            ProjectName = prop->getProperty("ArmarX.ProjectName");
            std::string dependencies = prop->getProperty("ArmarX.ProjectDependencies");
            boost::split(ProjectDependendencies, dependencies, boost::is_any_of(";"), boost::token_compress_on);
        }
        else
        {
            /*
            ARMARX_WARNING_S << "The given project datapath config file '" << datapathConfig << "', that this app depends on, could not be found. \
                                Relative paths to subprojects are not available. Set the property ArmarX.ProjectDatapath to empty, if you do not need the paths.";
            */
        }
    }
}


ApplicationPropertyDefinitions::ApplicationPropertyDefinitions(std::string prefix):
    PropertyDefinitionContainer(prefix)
{
    defineOptionalProperty<std::string>("Config", "", "Comma-separated list of configuration files ");
    defineOptionalProperty<std::string>("DependenciesConfig", "./config/dependencies.cfg", "Path to the (usually generated) config file containing all data paths of all dependent projects. This property usually does not need to be edited.");
    defineOptionalProperty<Ice::StringSeq>("DefaultPackages", {"ArmarXCore", "ArmarXGui", "MemoryX", "RobotAPI", "RobotComponents", "RobotSkillTemplates", "ArmarXSimulation", "VisionX", "SpeechX", "Armar3", "Armar4"},
                                           "List of ArmarX packages which are accessible by default. Comma separated List. If you want to add your own packages and use all default ArmarX packages, use the property 'AdditionalPackages'.");
    defineOptionalProperty<Ice::StringSeq>("AdditionalPackages", {},
                                           "List of additional ArmarX packages which should be in the list of default packages. If you have custom packages, which should be found by the gui or other apps, specify them here. Comma separated List.");

    defineOptionalProperty<std::string>("ApplicationName", "", "Application name");

    defineOptionalProperty<bool>("DisableLogging", false, "Turn logging off in whole application", PropertyDefinitionBase::eModifiable);

    defineOptionalProperty<MessageType>("Verbosity", eINFO, "Global logging level for whole application", PropertyDefinitionBase::eModifiable)
    .setCaseInsensitive(true)
    .map("Debug", eDEBUG)
    .map("Verbose", eVERBOSE)
    .map("Info", eINFO)
    .map("Important", eIMPORTANT)
    .map("Warning", eWARN)
    .map("Error", eERROR)
    .map("Fatal", eFATAL)
    .map("Undefined", eUNDEFINED);

    defineOptionalProperty<std::string>("DataPath", "", "Semicolon-separated search list for data files");

    defineOptionalProperty<std::string>("CachePath", "mongo/.cache", std::string("Path for cache files. If relative path AND env. variable ") +
                                        Application::ArmarXUserConfigDirEnvVar + " is set, the cache path will be made relative to " +
                                        Application::ArmarXUserConfigDirEnvVar + ". Otherwise if relative it will be relative to the default ArmarX config dir (" +
                                        Application::GetArmarXConfigDefaultPath(false) + ")");

    defineOptionalProperty<bool>("EnableProfiling", false, "Enable profiling of CPU load produced by this application", PropertyDefinitionBase::eModifiable);

    defineOptionalProperty<bool>("RedirectStdout", true, "Redirect std::cout and std::cerr to ArmarXLog");

    defineOptionalProperty<std::string>("TopicSuffix", "", "Suffix appended to all topic names for outgoing topics. This is mainly used to direct all topics to another name for TopicReplaying purposes.");

    defineOptionalProperty<bool>("UseTimeServer", false, "Enable using a global Timeserver (e.g. from ArmarXSimulator)");

    defineOptionalProperty<unsigned int>("RemoteHandlesDeletionTimeout", 3000, "The timeout (in ms) before a remote handle deletes the managed object after the use count reached 0. This time can be used by a client to increment the count again (may be required when transmitting remote handles)");
    defineOptionalProperty<bool>("StartDebuggerOnCrash", false, "If this application crashes (segmentation fault) qtcreator will attach to this process and start the debugger.");
    defineOptionalProperty<unsigned int>("ThreadPoolSize", 1, "Size of the ArmarX ThreadPool that is always running.");
    defineOptionalProperty<std::string>("LoadLibraries", "", "Libraries to load at start up of the application. Must be enabled by the Application with enableLibLoading(). Format: PackageName:LibraryName;... or /absolute/path/to/library;...");
}
