/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Manfred Kroehnert (manfred dot kroehnert at kit dot edu)
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "ApplicationProcessFacet.h"

#include "../ArmarXManager.h"

armarx::ApplicationProcessFacet::ApplicationProcessFacet(const ArmarXManagerPtr& armarXManager) :
    armarXManager(armarXManager)
{
}

void armarx::ApplicationProcessFacet::shutdown(const Ice::Current&)
{
    armarXManager->shutdown();
}

void armarx::ApplicationProcessFacet::writeMessage(const std::string& msg, Ice::Int fd, const Ice::Current&)
{
    if (fd == 1)
    {
        std::cout << msg << std::endl;
    }
    else if (fd == 2)
    {
        std::cerr << msg << std::endl;
    }
}
