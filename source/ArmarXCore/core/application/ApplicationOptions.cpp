/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Jan Issac (jan dot issac at gmail dot com)
 * @date       2012
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include <Ice/BuiltinSequences.h>       // for StringSeq
#include <Ice/Handle.h>                 // for Handle
#include <Ice/Initialize.h>             // for argsToStringSeq
#include <Ice/ObjectF.h>                // for upCast
#include <Ice/PropertiesF.h>            // for PropertiesPtr
#include <boost/algorithm/string/case_conv.hpp>  // for to_lower_copy
#include <boost/iterator/iterator_facade.hpp>  // for operator!=, etc
//#include <boost/program_options/options_description.hpp>
//#include <boost/program_options/parsers.hpp>
#include <boost/program_options/value_semantic.hpp>  // for typed_value
#include <boost/program_options/variables_map.hpp>  // for variables_map, etc
#include <stddef.h>                     // for NULL
#include <map>                          // for map, _Rb_tree_iterator
#include <ostream>                      // for char_traits, ostream, etc
#include <vector>                       // for vector, vector<>::iterator

#include "Application.h"                // for Application, ApplicationPtr
#include "ApplicationOptions.h"
#include "ArmarXCore/core/ManagedIceObject.h"  // for ManagedIceObjectPtr, etc
#include "ArmarXCore/core/application/../ArmarXDummyManager.h"
#include "ArmarXCore/core/application/properties/PropertyDefinitionBriefHelpFormatter.h"
#include "ArmarXCore/core/application/properties/PropertyDefinitionConfigFormatter.h"
#include "ArmarXCore/core/application/properties/PropertyDefinitionContainerBriefHelpFormatter.h"
#include "ArmarXCore/core/application/properties/PropertyDefinitionContainerFormatter.h"
#include "ArmarXCore/core/application/properties/PropertyDefinitionDoxygenComponentPagesFormatter.h"
#include "ArmarXCore/core/application/properties/PropertyDefinitionDoxygenFormatter.h"
#include "ArmarXCore/core/application/properties/PropertyDefinitionFormatter.h"
#include "ArmarXCore/core/application/properties/PropertyDefinitionHelpFormatter.h"
#include "ArmarXCore/core/application/properties/PropertyDefinitionXmlFormatter.h"
#include "ArmarXCore/core/application/properties/PropertyUser.h"
#include "ArmarXCore/core/logging/LogSender.h"  // for LogSender
#include "ArmarXCore/core/logging/Logging.h"  // for ARMARX_WARNING

using namespace armarx;
using namespace boost;
using namespace boost::program_options;


Ice::PropertiesPtr ApplicationOptions::mergeProperties(Ice::PropertiesPtr properties, int argc, char* argv[])
{
    // override and merge new properties given by the command line
    Ice::StringSeq args = Ice::argsToStringSeq(argc, argv);
    args = properties->parseCommandLineOptions("", args);

    return properties;
}

ApplicationOptions::Options ApplicationOptions::parseHelpOptions(Ice::PropertiesPtr properties, int argc, char* argv[])
{
    Options options;
    options.error = false;
    options.showHelp = false;
    options.showVersion = false;
    options.outfile = "";

    // create program options
    options.description.reset(new options_description(std::string(
                                  "This executable is an application within the ArmarX framework.\n")
                              + "==================================================================\n"
                              + " Starting an ArmarX application\n"
                              + "==================================================================\n"
                              + "In order to start the application, execute it normally and pass optional config"
                              + " parameters to it:\n\n"

                              + "  > " + argv[0] + " [options]\n\n"

                              + "Make sure that Ice is started using the ice-start.sh script.\n"
                              + "Options for the component can be provided either in the command line\n"
                              + "or in a separate config file. In order to provide config files use \n\n"

                              + "  > " + argv[0] + " --Ice.Config=cfg1,cfg2\n\n"

                              + "where multiple config files are allowed.\n\n"

                              + "Help options"));

    options.description->add_options()("help,h", "Help option");
    options.description->add_options()("version,v", "Shows version of this application, which is usually the version of the package.");

    options.description->add_options()("print-options,p", "Print detailed application options description");

    options.description->add_options()("options-format,f", value<std::string>(), "Options print format: { help, config, doxygen, doxygen_component_pages, xml }");

    options.description->add_options()("output-file,o", value <std::string>(), "Optional output file (outherwise gets printed to std::cout)");

    variables_map vm;
    store(command_line_parser(argc, argv).options(*options.description).allow_unregistered().run(), vm);
    notify(vm);

    if (vm.count("print-options"))
    {
        std::map<std::string, OptionsFormat> optionsFormats;
        optionsFormats["help"] = eOptionsDetailed;
        optionsFormats["doxygen"] = eDoxygen;
        optionsFormats["doxygen_component_pages"] = eDoxygenComponentPages;
        optionsFormats["config"] = eConfig;
        optionsFormats["xml"] = eXml;

        if (vm.count("options-format"))
        {
            std::string optionsFormatName = vm["options-format"].as<std::string>();
            optionsFormatName = boost::algorithm::to_lower_copy(optionsFormatName);

            if (optionsFormats.find(optionsFormatName) != optionsFormats.end())
            {
                options.format = optionsFormats[optionsFormatName];
            }
            else
            {
                std::cout << "Unknown options-format:" << optionsFormatName << std::endl;

                options.showHelp = false;
                options.error = true;
                return options;
            }
        }
        else
        {
            options.format = eOptionsDetailed;
        }
    }
    else if (vm.count("help") || properties->getProperty("Ice.Default.Locator").empty())
    {
        options.format = eHelpBrief;
    }
    else if (vm.count("version"))
    {
        options.showVersion = true;
        return options;
    }
    else
    {
        return options;
    }

    if (vm.count("output-file"))
    {
        std::string outputFilePath = vm["output-file"].as<std::string>();
        options.outfile = outputFilePath;
    }

    options.showHelp = true;

    return options;
}


void ApplicationOptions::showHelp(ApplicationPtr application, ArmarXDummyManagerPtr dummyManager, ApplicationOptions::Options options, Ice::PropertiesPtr properties, std::ostream& out)
{
    OptionsFormat optionsFormat = options.format;
    program_options::options_description& desc = *options.description;

    PropertyDefinitionFormatter* defFormatter = nullptr;
    PropertyDefinitionContainerFormatter* pdcFormatter = nullptr;
    PropertyUserList puList;

    switch (optionsFormat)
    {
        case eHelpBrief:
        {
            PropertyDefinitionBriefHelpFormatter* definitionFormatter;
            PropertyDefinitionContainerBriefHelpFormatter* containerFormatter;

            // help options
            out << desc << std::endl;

            out << "By default the following config files are loaded:\n ";
            Ice::StringSeq paths = application->GetDefaultsPaths();

            for (auto p : paths)
            {
                out << "\t" << p << std::endl;
            }

            out << std::endl;
            // application options
            options_description applicationDesc("Application properties");

            definitionFormatter = new PropertyDefinitionBriefHelpFormatter(&applicationDesc);
            containerFormatter = new PropertyDefinitionContainerBriefHelpFormatter(*definitionFormatter, &applicationDesc);

            puList.push_front(application);
            out << containerFormatter->formatPropertyUsers(puList) << std::endl;

            // component options
            options_description componentDesc("Component properties");

            delete definitionFormatter;
            delete containerFormatter;

            definitionFormatter = new PropertyDefinitionBriefHelpFormatter(&componentDesc);
            containerFormatter = new PropertyDefinitionContainerBriefHelpFormatter(*definitionFormatter, &componentDesc);

            puList = getPropertyUsers(dummyManager);
            out << containerFormatter->formatPropertyUsers(puList) << std::endl;

            delete definitionFormatter;
            delete containerFormatter;
        }

        return;

        case eOptionsDetailed:
            defFormatter = new PropertyDefinitionHelpFormatter();
            pdcFormatter = new PropertyDefinitionContainerFormatter(*defFormatter);
            break;

        case eXml:
            defFormatter = new PropertyDefinitionXmlFormatter();
            pdcFormatter = new PropertyDefinitionContainerFormatter(*defFormatter);
            break;

        case eDoxygen:
            defFormatter = new PropertyDefinitionDoxygenFormatter();
            pdcFormatter = new PropertyDefinitionContainerFormatter(*defFormatter);
            break;

        case eDoxygenComponentPages:
            defFormatter = new PropertyDefinitionDoxygenComponentPagesFormatter();
            pdcFormatter = new PropertyDefinitionContainerFormatter(*defFormatter);
            break;

        case eConfig:
            defFormatter = new PropertyDefinitionConfigFormatter();
            pdcFormatter = new PropertyDefinitionContainerFormatter(*defFormatter);
            break;
        default:
            ARMARX_WARNING << "ConfigFormatter with id " << (int)optionsFormat << " not available";
            return;
    }

    pdcFormatter->setProperties(properties);
    // add this application to the property user list
    puList = getPropertyUsers(dummyManager);
    puList.push_front(application);

    out << pdcFormatter->formatPropertyUsers(puList) << std::endl;

    delete defFormatter;
    delete pdcFormatter;
}


PropertyUserList ApplicationOptions::getPropertyUsers(ArmarXDummyManagerPtr dummyManager)
{
    PropertyUserList propertyUsers;

    std::vector<ManagedIceObjectPtr> objects = dummyManager->getManagedObjects();
    std::vector<ManagedIceObjectPtr>::iterator it = objects.begin();

    while (it != objects.end())
    {
        PropertyUserPtr user = PropertyUserPtr::dynamicCast(*it);

        if (user)
        {
            propertyUsers.push_back(user);
        }

        ComponentPtr comp = ComponentPtr::dynamicCast(*it);
        if (comp)
        {
            auto additionalPropertyUsers = comp->getAdditionalPropertyUsers();
            propertyUsers.insert(propertyUsers.end(), additionalPropertyUsers.begin(), additionalPropertyUsers.end());
        }


        ++it;
    }

    return propertyUsers;
}
