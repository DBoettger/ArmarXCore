/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Christian Mandery (mandery@kit.edu)
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include "PropertyDefinitionDoxygenFormatter.h"

namespace armarx
{
    /**
     * @ingroup properties
     *
     * @class PropertyDefinitionDoxygenComponentPagesFormatter
     * @brief PropertyDefinitionDoxygenComponentPagesFormatter creates doxygen output with an own group for every component
     */
    class PropertyDefinitionDoxygenComponentPagesFormatter:
        public PropertyDefinitionDoxygenFormatter
    {
    public:
        PropertyDefinitionDoxygenComponentPagesFormatter() {}

        std::string formatDefinition(std::string name,
                                     std::string description,
                                     std::string min,
                                     std::string max,
                                     std::string default_,
                                     std::string casesensitivity,
                                     std::string requirement,
                                     std::string reged,
                                     std::vector<std::string> values, std::string value) override;

        std::string formatValues(std::vector<std::string> values) override;
        std::string formatAttribute(std::string name, std::string details) override;

        std::string getFormat() override;

    private:
        std::string escapeMarkdown(std::string s);

        std::string lastPrefix;
    };
}

