/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2015
* @copyright  http://www.gnu.org/licenses/gpl.txt
*             GNU General Public License
*/

#pragma once
#include "PropertyDefinition.h"

#include <boost/any.hpp>
#include <boost/unordered/unordered_map.hpp>

#include <limits>


#include <ArmarXCore/core/exceptions/Exception.h>
#include <ArmarXCore/core/exceptions/local/InvalidPropertyValueException.h>
#include <ArmarXCore/core/exceptions/local/MissingRequiredPropertyException.h>
#include <ArmarXCore/core/exceptions/local/UnmappedValueException.h>
#include <ArmarXCore/core/exceptions/local/ValueRangeExceededException.h>

#include <boost/regex.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/numeric/conversion/cast.hpp>
#include <boost/numeric/conversion/converter.hpp>

//#include <boost/type_traits/is_floating_point.hpp>
//#include <boost/type_traits/is_integral.hpp>

// Workaround for QTBUG-22829
#ifndef Q_MOC_RUN
#include <boost/lexical_cast.hpp>
#endif

/* ====================================================================== */
/* === Property Definition Implementation =============================== */
/* ====================================================================== */
namespace armarx
{


    template<typename PropertyType>
    PropertyDefinition<PropertyType>::PropertyDefinition(
        const std::string& propertyName,
        const std::string& description,
        PropertyDefinitionBase::PropertyConstness constness) :
        PropertyDefinitionBase(true, constness),
        propertyName(propertyName),
        description(description),
        factory(nullptr),
        regex(""),
        caseInsensitive(true),
        expandEnvVars(true),
        stringRemoveQuotes(true),
        max(std::numeric_limits<double>::max()),
        min(-std::numeric_limits<double>::max())
    {
    }


    template<typename PropertyType>
    PropertyDefinition<PropertyType>::PropertyDefinition(
        const std::string& propertyName,
        PropertyType defaultValue,
        const std::string& description,
        PropertyDefinitionBase::PropertyConstness constness) :
        PropertyDefinitionBase(false, constness),
        propertyName(propertyName),
        description(description),
        defaultValue(defaultValue),
        factory(nullptr),
        regex(""),
        caseInsensitive(true),
        expandEnvVars(true),
        stringRemoveQuotes(true),
        max(std::numeric_limits<double>::max()),
        min(-std::numeric_limits<double>::max())
    {
    }


    template <typename PropertyType>
    PropertyDefinition<PropertyType>&
    PropertyDefinition<PropertyType>::map(
        const std::string& valueString,
        PropertyType value)
    {
        if (caseInsensitive)
        {
            std::string lowerCaseValueString =
                boost::algorithm::to_lower_copy(valueString);

            propertyValuesMap[lowerCaseValueString] = ValueEntry(valueString,
                    value);
        }
        else
        {
            propertyValuesMap[valueString] = ValueEntry(valueString, value);
        }

        return *this;
    }


    template <typename PropertyType>
    PropertyDefinition<PropertyType>&
    PropertyDefinition<PropertyType>::setFactory(const PropertyFactoryFunction& func)
    {
        this->factory = func;

        return *this;
    }


    template <typename PropertyType>
    PropertyDefinition<PropertyType>&
    PropertyDefinition<PropertyType>::setCaseInsensitive(bool caseInsensitive)
    {
        if (this->caseInsensitive != caseInsensitive)
        {
            // rebuild value map
            PropertyValuesMap newPropertyValuesMap;

            typename PropertyValuesMap::iterator valueIter =
                propertyValuesMap.begin();

            std::string key;

            while (valueIter != propertyValuesMap.end())
            {
                key = valueIter->second.first;

                if (caseInsensitive)
                {
                    key = boost::algorithm::to_lower_copy(key);
                }

                newPropertyValuesMap[key] = valueIter->second;
                valueIter++;
            }

            // set the new map
            propertyValuesMap = newPropertyValuesMap;
            this->caseInsensitive = caseInsensitive;
        }

        return *this;
    }

    template <typename PropertyType>
    PropertyDefinition<PropertyType>&
    PropertyDefinition<PropertyType>::setExpandEnvironmentVariables(bool expand)
    {
        this->expandEnvVars = expand;
        return *this;
    }

    template <typename PropertyType>
    PropertyDefinition<PropertyType>&
    PropertyDefinition<PropertyType>::setRemoveQuotes(bool removeQuotes)
    {
        this->stringRemoveQuotes = removeQuotes;
        return *this;
    }


    template <typename PropertyType>
    PropertyDefinition<PropertyType>&
    PropertyDefinition<PropertyType>::setMatchRegex(
        const std::string& regex)
    {
        this->regex = regex;

        return *this;
    }


    template <typename PropertyType>
    PropertyDefinition<PropertyType>&
    PropertyDefinition<PropertyType>::setMin(double min)
    {
        // positive & negative overflow check
        boost::numeric_cast<PropertyType, double>(min);

        //this->min = boost::numeric_cast<double, PropertyType>(min);
        this->min = min;

        return *this;
    }


    template <typename PropertyType>
    PropertyDefinition<PropertyType>&
    PropertyDefinition<PropertyType>::setMax(double max)
    {
        // positive & negative overflow check
        boost::numeric_cast<PropertyType, double>(max);

        //this->max = boost::numeric_cast<double, PropertyType>(max);
        this->max = max;

        return *this;
    }

    template <typename PropertyType>
    bool
    PropertyDefinition<PropertyType>::isCaseInsensitive() const
    {
        return caseInsensitive;
    }

    template <typename PropertyType>
    bool
    PropertyDefinition<PropertyType>::expandEnvironmentVariables() const
    {
        return expandEnvVars;
    }

    template <typename PropertyType>
    bool
    PropertyDefinition<PropertyType>::removeQuotes() const
    {
        return stringRemoveQuotes;
    }


    template <typename PropertyType>
    std::string PropertyDefinition<PropertyType>::getMatchRegex() const
    {
        return regex;
    }


    template <typename PropertyType>
    double
    PropertyDefinition<PropertyType>::getMin() const
    {
        return min;
    }


    template <typename PropertyType>
    double
    PropertyDefinition<PropertyType>::getMax() const
    {
        return max;
    }


    template <typename PropertyType>
    std::string
    PropertyDefinition<PropertyType>::getPropertyName() const
    {
        return propertyName;
    }

    template <typename PropertyType>
    std::string
    PropertyDefinition<PropertyType>::getDescription() const
    {
        return description;
    }

    template <typename PropertyType>
    typename PropertyDefinition<PropertyType>::PropertyValuesMap&
    PropertyDefinition<PropertyType>::getValueMap()
    {
        return propertyValuesMap;
    }


    template <typename PropertyType>
    PropertyType
    PropertyDefinition<PropertyType>::getDefaultValue()
    {
        // throw exception if no default exists

        if (isRequired())
        {
            throw armarx::LocalException("Required property '" + getPropertyName() + "': default doesn't exist.");
        }

        return defaultValue;
    }


    template <typename PropertyType>
    typename PropertyDefinition<PropertyType>::PropertyFactoryFunction PropertyDefinition<PropertyType>::getFactory() const
    {
        return factory;
    }

    template <typename PropertyType>
    std::string
    PropertyDefinition<PropertyType>::toString(PropertyDefinitionFormatter& formatter, const std::string& value)
    {
        std::vector<std::string> mapValues;

        typename PropertyValuesMap::iterator it = propertyValuesMap.begin();

        while (it != propertyValuesMap.end())
        {
            mapValues.push_back(it->second.first);

            ++it;
        }

        return formatter.formatDefinition(
                   propertyName,
                   description,
                   (min != -std::numeric_limits<double>::max() ? boost::lexical_cast<std::string>(min) : ""),
                   (max != std::numeric_limits<double>::max() ? boost::lexical_cast<std::string>(max) : ""),
                   getDefaultAsString(),
                   (!isCaseInsensitive() ? "yes" : "no"),
                   (isRequired() ? "yes" : "no"),
                   regex,
                   mapValues,
                   value);
    }


    template <typename PropertyType>
    std::string PropertyDefinition<PropertyType>::getDefaultAsString()
    {
        if (!isRequired())
        {
            return getDefaultAsStringImpl<PropertyType>();
        }

        // no default available
        return std::string();
    }


    template <typename PropertyType>
    template <typename T>
    typename std::enable_if<std::is_same<T, bool>::value, std::string>::type
    PropertyDefinition<PropertyType>::getDefaultAsStringImpl(Qualifier<3>)
    {
        if (!isRequired())
        {
            return getDefaultValue() ? std::string("true") : std::string("false");
        }
        return std::string();
    }

    template <typename PropertyType>
    template <typename T>
    typename std::enable_if < !std::is_same<T, bool>::value && std::is_arithmetic<T>::value, std::string >::type
    PropertyDefinition<PropertyType>::getDefaultAsStringImpl(Qualifier<0>)
    {
        return boost::lexical_cast<std::string>(getDefaultValue());
    }


    template <typename PropertyType>
    template <typename T>
    typename std::enable_if<std::is_same<T, std::string>::value, std::string>::type
    PropertyDefinition<PropertyType>::getDefaultAsStringImpl(Qualifier<1>)
    {
        if (getDefaultValue().empty())
        {
            return "\"\"";
        }

        return getDefaultValue();
    }


    template <typename PropertyType>
    template <typename T>
    typename std::enable_if<
    !(std::is_same<T, std::string>::value || std::is_arithmetic<T>::value), std::string >::type
    PropertyDefinition<PropertyType>::getDefaultAsStringImpl(Qualifier<2>)
    {
        typename PropertyValuesMap::iterator it = propertyValuesMap.begin();

        while (it != propertyValuesMap.end())
        {
            if (it->second.second == getDefaultValue())
            {
                return it->second.first;
            }

            ++it;
        }

        return "Default value not mapped.";
    }
}
