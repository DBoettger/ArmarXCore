/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarXCore::core
* @author     Jan Issac (jan dot issac at gmx dot de)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "ArmarXCore/core/application/properties/PropertyDefinitionContainer.h"
#include "ArmarXCore/core/exceptions/Exception.h"  // for LocalException
#include "ArmarXCore/core/util/StringHelpers.h"  // for Contains
#include "PropertyDefinitionConfigFormatter.h"
#include "PropertyUser.h"
#include "../../logging/Logging.h"

#include <Ice/Initialize.h>

#include <sstream>

using namespace armarx;

PropertyUser::~PropertyUser()
{
}

PropertyDefinitionsPtr PropertyUser::getPropertyDefinitions()
{
    if (propertyDefinitions.get() == nullptr)
    {
        propertyDefinitions = createPropertyDefinitions();
        if (properties)
        {
            propertyDefinitions->setProperties(properties);
        }
        if (armarx::Contains(propertyDefinitions->getPrefix(), ".."))
        {
            throw LocalException("Property prefix contains '..'. Did you call 'getProperty' in a Component constructor?");
        }
    }

    return propertyDefinitions;
}


void PropertyUser::setIceProperties(Ice::PropertiesPtr properties)
{
    if (properties)
    {
        std::set<std::string> changedPropertyNames;
        for (auto& prop : properties->getPropertiesForPrefix(""))
        {
            auto propName = prop.first;
            auto index = propName.find_last_of('.');
            std::string name = propName.substr(index + 1);
            changedPropertyNames.insert(name);
        }
        this->properties = properties->clone();
        if (propertyDefinitions)
        {
            propertyDefinitions->setProperties(this->properties);
        }
        //        if (signalUpdates)
        {
            icePropertiesUpdated(changedPropertyNames);
        }
    }
}

void PropertyUser::updateIceProperties(const Ice::PropertyDict& changes)
{
    ScopedLock lock(mutex);
    PropertyDefinitionsPtr definitions = getPropertyDefinitions();
    std::set<std::string> changedPropertyNames;
    for (auto& prop : changes)
    {
        std::string propName = prop.first;
        try
        {
            auto index = propName.find_last_of('.');
            std::string name = propName.substr(index + 1);
            std::string prefix = propName.substr(0, index + 1);
            if (prefix != definitions->getPrefix())
            {
                continue;
            }
            if (!definitions->getDefinitionBase(name)->isConstant())
            {
                this->properties->setProperty(propName, prop.second);
                changedPropertyNames.insert(name);
                ARMARX_VERBOSE << "Updating MUTABLE Property: " << prop.first << " with value '" << prop.second << "'";
            }
            else
            {
                ARMARX_WARNING << "Can not set CONST Property: " << prop.first;
            }
        }
        catch (const armarx::LocalException& e)
        {
            ARMARX_IMPORTANT << "MISSING Property: " << e.what();
        }
    }

    icePropertiesUpdated(changedPropertyNames);

}

bool PropertyUser::tryAddProperty(const std::string& propertyName, const std::string& value)
{
    if (properties->getProperty(propertyName).empty())
    {
        properties->setProperty(propertyName, value);
        return true;
    }
    return false;
}


void PropertyUser::icePropertiesUpdated(const std::set<std::string>& changedProperties)
{
    // intentionally empty, since subclasses are supposed to overwrite this method
}


Ice::PropertiesPtr PropertyUser::getIceProperties() const
{
    if (properties)
    {
        return properties->clone();// cloning is necessary here
    }
    else
    {
        return Ice::createProperties();
    }
}

bool armarx::PropertyUser::hasProperty(const std::string& name)
{
    return getPropertyDefinitions()->hasDefinition(name);
}
