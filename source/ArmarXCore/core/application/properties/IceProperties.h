/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Jan Issac (jan dot issac at mail dot com)
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <Ice/Properties.h>

#include <vector>
#include <map>
#include <string>

#define INHERITANCE_KEYWORD "inheritFrom"

namespace armarx
{
    class IceProperties;

    /**
     * @ingroup properties
     *
     * @class IceProperties
     * @brief IceProperties stores ice properties and resolves property inheritance.
     *
     * This Ice property container supports inheritance. That is, a namespace can inherit all
     * properties defined within another namespace. Inherited properties can also be overridden
     * if redefined.
     *
     * @code <namespace>.inheritFrom = <namespace>@endcode
     *
     * Example config:
     *
     * @code
     *
     *     ArmarX.CommonProperties.FrameRate = 30
     *     ArmarX.CommonProperties.ColorMode = HSV
     *     ArmarX.CommonProperties.ShutterSpeed = 0.003
     *     ArmarX.CommonProperties.Aperture = 1.2
     *     ArmarX.CommonProperties.WhiteBalance = Auto
     *     ArmarX.CommonProperties.Metering = CenteredMean
     *     ArmarX.CommonProperties.Resolution = 640x480
     *     ArmarX.CommonProperties.CropFactor = 1.5
     *
     *     VisionX.MyCapturer1.inheritFrom = ArmarX.CommonProperties
     *     VisionX.MyCapturer1.ColorMode = RGB
     *
     *     VisionX.MyCapturer2.inheritFrom = ArmarX.CommonProperties
     *     VisionX.MyCapturer2.ShutterSpeed = 0.001
     *
     *     VisionX.MyCapturer3.FrameRate = 60
     *     VisionX.MyCapturer3.inheritFrom = ArmarX.CommonProperties
     * @endcode
     *
     * The result is:
     *
     * @code
     *     ArmarX.CommonProperties.FrameRate = 30
     *     ...
     *     ...
     *     ArmarX.CommonProperties.CropFactor = 1.5
     *
     *     VisionX.MyCapturer1.FrameRate = 30
     *     VisionX.MyCapturer1.ColorMode = RGB
     *     VisionX.MyCapturer1.ShutterSpeed = 0.003
     *     VisionX.MyCapturer1.Aperture = 1.2
     *     VisionX.MyCapturer1.WhiteBalance = Auto
     *     VisionX.MyCapturer1.Metering = CenteredMean
     *     VisionX.MyCapturer1.Resolution = 640x480
     *     VisionX.MyCapturer1.CropFactor = 1.5
     *
     *     VisionX.MyCapturer2.FrameRate = 30
     *     VisionX.MyCapturer2.ColorMode = HSV
     *     VisionX.MyCapturer2.ShutterSpeed = 0.001
     *     VisionX.MyCapturer2.Aperture = 1.2
     *     VisionX.MyCapturer2.WhiteBalance = Auto
     *     VisionX.MyCapturer2.Metering = CenteredMean
     *     VisionX.MyCapturer2.Resolution = 640x480
     *     VisionX.MyCapturer2.CropFactor = 1.5
     *
     *     VisionX.MyCapturer3.FrameRate = 60
     *     VisionX.MyCapturer3.ColorMode = HSV
     *     VisionX.MyCapturer3.ShutterSpeed = 0.003
     *     VisionX.MyCapturer3.Aperture = 1.2
     *     VisionX.MyCapturer3.WhiteBalance = Auto
     *     VisionX.MyCapturer3.Metering = CenteredMean
     *     VisionX.MyCapturer3.Resolution = 640x480
     *     VisionX.MyCapturer3.CropFactor = 1.5
     * @endcode
     */
    class IceProperties:
        public Ice::Properties
    {
    public:
        static Ice::PropertiesPtr create(const Ice::PropertiesPtr& iceProperties = nullptr);

        static Ice::PropertiesPtr create(Ice::StringSeq& propertySeq,
                                         const Ice::PropertiesPtr& iceProperties = nullptr);

        static Ice::PropertiesPtr create(int& argn,
                                         char* argv[],
                                         const Ice::PropertiesPtr& iceProperties = nullptr);
    public:
        /**
         * Ice property container constructor
         */
        explicit IceProperties(const Ice::PropertiesPtr iceProperties);
        IceProperties(const IceProperties& source);

        /**
         * Ice property container desctructor
         */
        ~IceProperties() override;

        /**
         * @see Ice::Properties::getProperty()
         */
        ::std::string getProperty(const ::std::string& name) override;

        /**
         * @see Ice::Properties::getPropertyWithDefault()
         */
        ::std::string getPropertyWithDefault(const ::std::string& name,
                                             const ::std::string& defaultValue) override;
        /**
         * @see Ice::Properties::getPropertyAsInt()
         */
        ::Ice::Int getPropertyAsInt(const ::std::string& name) override;

        /**
         * @see Ice::Properties::getPropertyAsIntWithDefault()
         */
        ::Ice::Int getPropertyAsIntWithDefault(const ::std::string& name,
                                               ::Ice::Int defaultValue) override;
        /**
         * @see Ice::Properties::getPropertyAsList()
         */
        ::Ice::StringSeq getPropertyAsList(const ::std::string& name) override;

        /**
         * @see Ice::Properties::getPropertyAsListWithDefault()
         */
        ::Ice::StringSeq getPropertyAsListWithDefault(const ::std::string& name,
                const ::Ice::StringSeq& defaultValue) override;
        /**
         * @see Ice::Properties::getPropertiesForPrefix()
         */
        ::Ice::PropertyDict getPropertiesForPrefix(const ::std::string& prefix) override;

        /**
         * @see Ice::Properties::setProperty()
         */
        void setProperty(const ::std::string& name, const ::std::string& value) override;

        /**
         * @see Ice::Properties::getCommandLineOptions()
         */
        ::Ice::StringSeq getCommandLineOptions() override;

        /**
         * @see Ice::Properties::parseCommandLineOptions()
         */
        ::Ice::StringSeq parseCommandLineOptions(const ::std::string& prefix,
                const ::Ice::StringSeq& options) override;
        /**
         * @see Ice::Properties::parseIceCommandLineOptions()
         */
        ::Ice::StringSeq parseIceCommandLineOptions(const ::Ice::StringSeq& options) override;

        /**
         * @see Ice::Properties::load()
         */
        void load(const ::std::string& fileName) override;

        /**
         * @see Ice::Properties::clone()
         */
        ::Ice::PropertiesPtr clone() override;

    public:
        class InheritanceSolver:
            public IceUtil::Shared
        {
        public:
            typedef std::map<std::string, bool> NamespaceMap;

        public:
            ~InheritanceSolver() override;

            /**
             * Resolves all inheritance specifications made using the config notation
             *  "<namespace>.inheritFrom = <namespace>" while <namespace> is <domain>.<object-name>.
             *
             * @param properties    The properties with inheritance specifications. The resolved
             *                      inheritance is done within this property contain as well.
             */
            void resolveInheritance(Ice::PropertiesPtr& properties);

            /**
             * Extracts all existing namespace within the passed property container. More simply
             * it extracts all existing prefixes.
             *
             * @param [in]  properties    Input property container
             * @param [out] namespaces    Extracted properties
             */
            void extractNamespaces(const Ice::PropertiesPtr& properties,
                                   NamespaceMap& namespaces);

            /**
             * Resolves namespace inheritance for a specific namespace. This will resolve all
             * inheritance specifications of it's parent namespace.
             *
             * @param [in]  childNamespace      Namespace requiring inheritance
             * @param       properties          Input and output properties
             */
            void resolveNamespaceInheritance(const std::string& childNamespace,
                                             Ice::PropertiesPtr& properties);

            /**
             * Performs the inheritance of properties from parentNamespace to childNamespace.
             *
             * @param [in]  childNamespace      Namespace requiring inheritance
             * @param [in]  parentNamespace     Parent namespace to inherit from.
             * @param       properties          Input and output properties
             * @param       heritageLine        Heritage line vector. This is used to detect
             *                                  inheritance cycles.
             */
            void inherit(const std::string& childNamespace,
                         const std::string& parentNamespace,
                         Ice::PropertiesPtr& properties,
                         std::vector<std::string> heritageLine);

            /**
             * Checks whether the specified namespace has a parent which it inherits from.
             * The inheritance is specified via a property as follows:
             *   <childNamespace>.inheritFrom = <parentNamespace>.
             *
             * <childNamespace> can be anything. Usually it consists of the <domain> and
             * <object-name>.
             *
             * @param [in] childNamespace   Child namespace.
             * @param [in] properties       Input properties containing inheritance specifications.
             */
            bool hasParent(const std::string& childNamespace, const Ice::PropertiesPtr& properties);

            /**
             * Returns the parent namespace of a child namespace
             *
             * @param [in] childNamespace   Child namespace.
             * @param [in] properties       Input properties containing inheritance specifications.
             *
             * @return parent namespace, otherwise an empty string
             */
            std::string getParent(const std::string& childNamespace,
                                  const Ice::PropertiesPtr& properties);

            /**
             * Checks whether the specified namespace exists already in the heritageLine
             *
             * @param [in] heritageLine  Heritage line vector
             * @param [in] namespace     The namespace to look for
             *
             * @return true if namespace is in heritageLine, otherwise false
             */
            bool isInHeritageLine(const std::vector<std::string>& heritageLine,
                                  const std::string& namespace_);

            /**
             * Removes the namespace of the property name.
             *
             * "<namespace>.<property-name> = <propertyValue>" becomes "<property-name> =
             * <property-value>".
             *
             * @return property name without the namespace.
             */
            std::string stripNamespace(const std::string& propertyName,
                                       const std::string& namespace_);
        };

    public:
        typedef IceUtil::Handle<InheritanceSolver> InheritanceSolverPtr;

    public:
        /**
         * Returns the currently used inheritance solver
         *
         * @return inheritance solver pointer
         */
        virtual InheritanceSolverPtr getInheritanceSolver();

        /**
         * Sets an inheritance solver in case of testing or using a non-default solver.
         */
        virtual void setInheritanceSolver(const InheritanceSolverPtr& inheritanceSolver);

    private:
        /**
         * Flags the property contain that it has been changed. This is required to resolve
         * possible inheritance.
         */
        void setModified(bool modified);

        /**
         * Checks whether the container has been modified.
         *
         * @return true if at least one property has been set ot altered, otherwise false
         */
        bool isModified();

        /**
         * Updates the container by resolving potential inheritance specifications.
         */
        void update();

    private:
        /**
         * Use internal composite to manage and store properties instead of calling directly
         * the parent functions. This is due to provide the create factory methods in the
         * same fashion as provided by Ice::createProperties.
         */
        Ice::PropertiesPtr internalProperties;

        /**
         * Modification flag
         */
        bool modified;

        /**
         * Property inheritance solver tool.
         */
        InheritanceSolverPtr inheritanceSolver;
    };
}

