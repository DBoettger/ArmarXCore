/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Jan Issac (jan dot issac at gmx dot de)
 * @date       2012
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <string>                       // for string
#include <vector>

#include "PropertyDefinitionContainerFormatter.h"
#include "PropertyUser.h"               // for PropertyUserList

namespace armarx
{
    class PropertyDefinitionFormatter;
}  // namespace armarx
namespace boost
{
    namespace program_options
    {
        class options_description;
    }  // namespace program_options
}  // namespace boost

// Workaround for QTBUG-22829
#ifndef Q_MOC_RUN
#include <boost/program_options.hpp>
#endif

namespace armarx
{
    /**
     * @ingroup properties
     *
     * @class PropertyDefinitionContainerBriefHelpFormatter
     * @brief PropertyDefinitionContainerBriefHelpFormatter
     */
    class PropertyDefinitionContainerBriefHelpFormatter:
        public PropertyDefinitionContainerFormatter
    {
    public:
        PropertyDefinitionContainerBriefHelpFormatter(
            PropertyDefinitionFormatter& defFormatter,
            boost::program_options::options_description* optionsDescription);

        std::string formatPropertyUsers(
            const PropertyUserList& propertyUserList) override;

    protected:
        boost::program_options::options_description* optionsDescription;
    };
}

