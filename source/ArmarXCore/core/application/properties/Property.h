/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Jan Issac (jan dot issac at gmx dot de)
 * @date       2011
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <Ice/Properties.h>             // for PropertiesPtr, Properties
#include <IceUtil/Handle.h>             // for HandleBase
//#include <boost/algorithm/string.hpp>
#include <boost/algorithm/string/case_conv.hpp>  // for to_lower
#include <boost/numeric/conversion/cast.hpp>
#include <boost/regex.hpp>
#include <boost/type_traits/is_arithmetic.hpp>  // for is_arithmetic
#include <boost/type_traits/is_same.hpp>  // for is_same
#include <boost/utility/enable_if.hpp>  // for enable_if_c, disable_if_c
#include <stddef.h>                     // for size_t
#include <stdlib.h>                     // for getenv

#include "ArmarXCore/core/application/properties/PropertyDefinition.h"
#include "PropertyDefinition.hpp"


#include "ArmarXCore/core/exceptions/local/InvalidPropertyValueException.h"
#include "ArmarXCore/core/exceptions/local/MissingRequiredPropertyException.h"
#include "ArmarXCore/core/exceptions/local/UnmappedValueException.h"
#include "ArmarXCore/core/exceptions/local/ValueRangeExceededException.h"

// Workaround for QTBUG-22829
#ifndef Q_MOC_RUN
#include <boost/lexical_cast.hpp>       // for lexical_cast
#endif

#include <algorithm>                    // for min, fill
#include <iostream>                     // for basic_ostream, operator<<, etc
#include <limits>
#include <map>
#include <string>                       // for basic_string, string, etc
#include <typeinfo>                     // for bad_cast
#include <vector>                       // for vector

namespace armarx
{
    /* ====================================================================== */
    /* === Property Declaration ============================================= */
    /* ====================================================================== */

    /**
     * @ingroup properties
     *
     * @class Property
     * @brief Provides access to Ice properties with extended capabilities.
     *
     * The Property template class provides access to Ice config properties
     * and command line options. Its main capability is to map a string value on
     * any type that is required and put constrains on the value as well as
     * on the property itself at the same time. For instance you can specify
     * that a certain property is required for the component to proceed or a
     * property may require a specific value syntax or even numeric bounds which
     * may not be exceeded. These features and few more are supported by the
     * Property combined with the PropertyDefinition.
     *
     * \section properties-outline Outline
     *
     * @code
     *  template<typename PropertyType> class Property
     *  {
     *      bool            iseSet();
     *      bool            isRequired();
     *      PropertyType    getValue();
     *  }
     * @endcode
     */
    template <typename PropertyType>
    class Property
    {
    public:
        /**
         * Property value map type definition
         */
        typedef typename PropertyDefinition<PropertyType>::PropertyValuesMap
        PropertyValuesMap;

        /**
         * Property constructor
         *
         * @param definition    Property specific definition
         * @param prefix        Property prefix such as domain or component name
         * @param properties    Ice properties set which contains all property
         *                      strings.
         *
         */
        Property(PropertyDefinition<PropertyType> definition,
                 std::string prefix,
                 Ice::PropertiesPtr properties);

        /**
         * Checks whether the property is set
         *
         * @return true if property is set, otheriwse false
         */
        bool isSet() const;

        /**
         * Checks if this property is required
         *
         * @return true if set required, otherwise false
         */
        bool isRequired() const;

        /**
         * Checks if this property is constant or if it can be changed at runtime.
         *
         * @return true if property is constant, otherwise false
         */
        bool isConstant() const;

        /**
         * Returns the property value set in a config file or passed as a
         * command-line option. If property is not set, the default value is
         * returned unless the property is required. In the latter case an
         * exception is thrown indicating that the required property is not set.
         *
         * @return Property value of the type <b>PropertyType</b>
         *
         * @throw armarx::exceptions::local::ValueRangeExceededException
         * @throw armarx::exceptions::local::InvalidPropertyValueException
         */
        PropertyType getValue();

        /**
         * @brief Convenience overload of getValue().
         * Usage:
         * \code
         * std::string val = getProperty<std::string>("myProp");
         * \endcode
         */
        operator PropertyType()
        {
            return getValue();
        }

        //! Checks first and last character of input and in case both are quotes, they are removed and the remaining result is stored in output
        static void removeQuotes(const std::string& input, std::string& output);

    private:
        /* = Implementation details ========================================= */

        template <int> struct Qualifier
        {
            Qualifier(int) {}
        };

        /**
         * getValue implementation for arithmetic types which involves no
         * mapping and only casting.
         */
        template <typename T>
        typename boost::enable_if_c<boost::is_arithmetic<T>::value, T>::type
        getValueRequiredImpl(Qualifier<0> = 0);

        /**
         * getValueOrDefault implementation for arithmetic types which involves
         * no mapping and only casting.
         */
        template <typename T>
        typename boost::enable_if_c<boost::is_arithmetic<T>::value, T>::type
        getValueOptionalImpl(Qualifier<0> = 0);

        /**
         * getValue implementation for strings.
         */
        template <typename T>
        typename boost::enable_if_c<boost::is_same<T, std::string>::value, T>::type
        getValueRequiredImpl(Qualifier<1> = 0);

        /**
         * getValueOrDefault implementation for strings.
         */
        template <typename T>
        typename boost::enable_if_c<boost::is_same<T, std::string>::value, T>::type
        getValueOptionalImpl(Qualifier<1> = 0);

        /**
         * getValue implementation which is restricted on mapped values
         */
        template <typename T>
        typename boost::disable_if_c <
        boost::is_same<T, std::string>::value || boost::is_arithmetic<T>::value,
              T >::type
              getValueRequiredImpl(Qualifier<2> = 0);

        /**
         * getValueOrDefault implementation which is restricted on mapped values
         */
        template <typename T>
        typename boost::disable_if_c <
        boost::is_same<T, std::string>::value || boost::is_arithmetic<T>::value,
              T >::type
              getValueOptionalImpl(Qualifier<2> = 0);

        /**
         * Verifies that the passed numericValue is within the defined
         * limits.
         *
         * @param numericValue  The numeric value of the property
         *
         * @throw armarx::exceptions::local::ValueRangeExceededException
         */
        void checkLimits(PropertyType numericValue) const;

        /**
         * Checks if this property is required and throws an exception if the
         * property is not specified.
         *
         * @throw armarx::exceptions::local::MissingRequiredPropertyException
         */
        void checkRequirement();

        /**
         * Returns the property string value. This function adapts the value
         * case according to the case sensitivity state. If no case sensitivity
         * is required, the return value will always be transformed to lower
         * case. This is mainly used to generate the value keys used in mapping.
         *
         * @return lower or mixed case eof th raw string value
         */
        std::string getPropertyValue() const;

        /**
         * Returns the raw string value of this property.
         *
         * @return raw string value.
         */
        std::string getRawPropertyValue() const;

        /**
         * Checks whether the a passed string matches a specified regular
         * expression.
         *
         * @param value     The value to check
         *
         * @return true if the string matches, otherwise false
         */
        bool matchRegex(const std::string& value) const;

        static bool expandEnvironmentVariables(const std::string& input, std::string& output);


        /**
         * Property definition
         */
        PropertyDefinition<PropertyType> definition;

        /**
         * Property prefix such as domain name
         */
        std::string prefix;

        /**
         * Ice property container
         */
        Ice::PropertiesPtr properties;
    };


    /* ====================================================================== */
    /* === Property Implementation ========================================== */
    /* ====================================================================== */
    template <typename PropertyType>
    Property<PropertyType>::Property(
        PropertyDefinition<PropertyType> definition,
        std::string prefix,
        Ice::PropertiesPtr properties):
        definition(definition),
        prefix(prefix),
        properties(properties)
    {
    }


    template <typename PropertyType>
    PropertyType
    Property<PropertyType>::getValue()
    {
        if (isRequired())
        {
            return getValueRequiredImpl<PropertyType>();
        }

        return getValueOptionalImpl<PropertyType>();
    }


    template <typename PropertyType>
    bool
    Property<PropertyType>::isRequired() const
    {
        return definition.isRequired();
    }


    template <typename PropertyType>
    bool
    Property<PropertyType>::isConstant() const
    {
        return definition.isConstant();
    }


    template <typename PropertyType>
    template <typename T>
    typename boost::enable_if_c<boost::is_arithmetic<T>::value, T>::type
    Property<PropertyType>::getValueRequiredImpl(Qualifier<0>)
    {
        using namespace armarx::exceptions;

        checkRequirement();

        std::string keyValue = getPropertyValue();
        std::string value = getRawPropertyValue();

        if (matchRegex(value))
        {
            if (definition.getFactory())
            {
                try
                {
                    return definition.getFactory()(value);
                }
                catch (std::bad_cast&)
                {
                    throw local::InvalidPropertyValueException(
                        prefix + definition.getPropertyName(), value);
                }
            }

            PropertyValuesMap& propertyValuesMap = definition.getValueMap();

            if (propertyValuesMap.size() != 0)
            {
                typename PropertyValuesMap::iterator valueIter =
                    propertyValuesMap.find(keyValue);

                if (valueIter != propertyValuesMap.end())
                {
                    return valueIter->second.second;
                }

                // mapping exist but value not found
                throw local::UnmappedValueException(
                    prefix + definition.getPropertyName(), value);
            }

            try
            {
                PropertyType numericValue =
                    boost::lexical_cast<PropertyType>(value);

                checkLimits(numericValue);

                return numericValue;
            }
            catch (std::bad_cast& e)
            {
                // catch std::bad_cast to throw an InvalidPropertyValueException
            }
        }

        throw local::InvalidPropertyValueException(
            prefix + definition.getPropertyName(), value);
    }


    template <typename PropertyType>
    template <typename T>
    typename boost::enable_if_c<boost::is_arithmetic<T>::value, T>::type
    Property<PropertyType>::getValueOptionalImpl(Qualifier<0>)
    {
        using namespace armarx::exceptions;


        if (isSet())
        {
            std::string keyValue = getPropertyValue();
            std::string value = getRawPropertyValue();

            if (matchRegex(value))
            {
                if (definition.getFactory())
                {
                    try
                    {
                        return definition.getFactory()(value);
                    }
                    catch (std::bad_cast&)
                    {
                        throw local::InvalidPropertyValueException(
                            prefix + definition.getPropertyName(), value);
                    }
                }

                PropertyValuesMap& propertyValuesMap = definition.getValueMap();

                if (propertyValuesMap.size() != 0)
                {
                    typename PropertyValuesMap::iterator valueIter =
                        propertyValuesMap.find(keyValue);

                    if (valueIter != propertyValuesMap.end())
                    {
                        return valueIter->second.second;
                    }

                    // mapping exist but value not found
                    throw local::UnmappedValueException(
                        prefix + definition.getPropertyName(), value);
                }

                try
                {
                    PropertyType numericValue =
                        boost::lexical_cast<PropertyType>(value);

                    checkLimits(numericValue);

                    return numericValue;
                }
                catch (std::bad_cast& e)
                {
                    // invalid numeric value

                }
            }
        }
        else
        {
            return definition.getDefaultValue();
        }

        throw local::InvalidPropertyValueException(
            prefix + definition.getPropertyName(), getRawPropertyValue());
    }


    template <typename PropertyType>
    template <typename T>
    typename boost::enable_if_c<boost::is_same<T, std::string>::value, T>::type
    Property<PropertyType>::getValueRequiredImpl(Qualifier<1>)
    {
        using namespace armarx::exceptions;

        checkRequirement();

        std::string keyValue = getPropertyValue();
        std::string value = getRawPropertyValue();

        if (matchRegex(value))
        {

            if (definition.getFactory())
            {
                return definition.getFactory()(value);
            }

            PropertyValuesMap& propertyValuesMap = definition.getValueMap();

            if (propertyValuesMap.size() != 0)
            {
                typename PropertyValuesMap::iterator valueIter =
                    propertyValuesMap.find(keyValue);

                if (valueIter != propertyValuesMap.end())
                {
                    std::string res = valueIter->second.second;

                    if (definition.expandEnvironmentVariables())
                    {
                        expandEnvironmentVariables(res, res);
                    }

                    if (definition.removeQuotes())
                    {
                        removeQuotes(res, res);
                    }

                    return res;
                }

                // mapping exist but value not found
                throw local::UnmappedValueException(
                    prefix + definition.getPropertyName(), value);
            }

            std::string res = value;

            if (definition.expandEnvironmentVariables())
            {
                expandEnvironmentVariables(res, res);
            }

            if (definition.removeQuotes())
            {
                removeQuotes(res, res);
            }

            return res;
        }

        throw local::InvalidPropertyValueException(
            prefix + definition.getPropertyName(), value);
    }


    template <typename PropertyType>
    template <typename T>
    typename boost::enable_if_c<boost::is_same<T, std::string>::value, T>::type
    Property<PropertyType>::getValueOptionalImpl(Qualifier<1>)
    {
        using namespace armarx::exceptions;


        if (isSet())
        {
            std::string keyValue = getPropertyValue();
            std::string value = getRawPropertyValue();

            if (matchRegex(value))
            {

                if (definition.getFactory())
                {
                    return definition.getFactory()(value);
                }

                PropertyValuesMap& propertyValuesMap = definition.getValueMap();

                if (propertyValuesMap.size() != 0)
                {
                    typename PropertyValuesMap::iterator valueIter =
                        propertyValuesMap.find(keyValue);

                    if (valueIter != propertyValuesMap.end())
                    {
                        std::string res = valueIter->second.second;

                        if (definition.expandEnvironmentVariables())
                        {
                            expandEnvironmentVariables(res, res);
                        }

                        if (definition.removeQuotes())
                        {
                            removeQuotes(res, res);
                        }

                        return res;
                    }

                    // mapping exist but value not found
                    throw local::UnmappedValueException(
                        prefix + definition.getPropertyName(), value);
                }

                std::string res = value;

                if (definition.expandEnvironmentVariables())
                {
                    expandEnvironmentVariables(res, res);
                }

                if (definition.removeQuotes())
                {
                    removeQuotes(res, res);
                }

                return res;
            }
        }
        else
        {
            return definition.getDefaultValue();
        }

        throw local::InvalidPropertyValueException(
            prefix + definition.getPropertyName(), getRawPropertyValue());
    }


    template <typename PropertyType>
    template <typename T>
    typename boost::disable_if_c < boost::is_same<T, std::string>::value || boost::is_arithmetic<T>::value, T >::type
    Property<PropertyType>::getValueRequiredImpl(Qualifier<2>)
    {
        using namespace armarx::exceptions;

        checkRequirement();

        std::string keyValue = getPropertyValue();
        std::string value = getRawPropertyValue();

        if (matchRegex(value))
        {

            if (definition.getFactory())
            {
                return definition.getFactory()(value);
            }

            PropertyValuesMap& propertyValuesMap = definition.getValueMap();

            if (propertyValuesMap.size() != 0)
            {
                typename PropertyValuesMap::iterator valueIter =
                    propertyValuesMap.find(keyValue);

                if (valueIter != propertyValuesMap.end())
                {
                    return valueIter->second.second;
                }

                // mapping exist but value not found
                throw local::UnmappedValueException(
                    prefix + definition.getPropertyName(), value);
            }
            else
            {
                // Todo: throw a mapping required exception!
            }
        }

        throw local::InvalidPropertyValueException(
            prefix + definition.getPropertyName(), value);
    }


    template <typename PropertyType>
    template <typename T>
    typename boost::disable_if_c < boost::is_same<T, std::string>::value || boost::is_arithmetic<T>::value, T >::type
    Property<PropertyType>::getValueOptionalImpl(Qualifier<2>)
    {
        using namespace armarx::exceptions;

        if (isSet())
        {
            std::string keyValue = getPropertyValue();
            std::string value = getRawPropertyValue();

            if (matchRegex(value))
            {

                if (definition.getFactory())
                {
                    return definition.getFactory()(value);
                }

                PropertyValuesMap& propertyValuesMap = definition.getValueMap();

                if (propertyValuesMap.size() != 0)
                {
                    typename PropertyValuesMap::iterator valueIter =
                        propertyValuesMap.find(keyValue);

                    if (valueIter != propertyValuesMap.end())
                    {
                        return valueIter->second.second;
                    }

                    // mapping exist but value not found
                    throw local::UnmappedValueException(
                        prefix + definition.getPropertyName(), value);
                }
                else
                {
                    // Todo: throw a mapping required exception!
                }
            }
        }
        else
        {
            return definition.getDefaultValue();
        }

        throw local::InvalidPropertyValueException(
            prefix + definition.getPropertyName(), getRawPropertyValue());
    }


    template <typename PropertyType>
    std::string
    Property<PropertyType>::getPropertyValue() const
    {
        std::string value = properties->getProperty(prefix + definition.getPropertyName());

        if (definition.isCaseInsensitive())
        {
            boost::algorithm::to_lower(value);
        }

        return value;
    }


    template <typename PropertyType>
    std::string
    Property<PropertyType>::getRawPropertyValue() const
    {
        return properties->getProperty(prefix + definition.getPropertyName());
    }


    template <typename PropertyType>
    bool
    Property<PropertyType>::isSet() const
    {
        if (!properties)
        {
            return false;
        }

        return properties->getPropertyWithDefault(prefix + definition.getPropertyName(), "::NOT-SET::")
               .compare("::NOT-SET::") != 0;
    }


    template <typename PropertyType>
    bool
    Property<PropertyType>::matchRegex(const std::string& value) const
    {
        if (!definition.getMatchRegex().empty())
        {
            boost::regex expr(definition.getMatchRegex());
            return boost::regex_match(value, expr);
        }

        return true;
    }


    template <typename PropertyType>
    void
    Property<PropertyType>::checkLimits(PropertyType numericValue) const
    {
        using namespace armarx::exceptions;

        double dirtyValue = boost::lexical_cast<double>(numericValue);

        if ((dirtyValue < definition.getMin()) || (dirtyValue > definition.getMax()))
        {
            throw local::ValueRangeExceededException(
                prefix + definition.getPropertyName(),
                definition.getMin(),
                definition.getMax(),
                dirtyValue);
        }
    }


    template <typename PropertyType>
    void
    Property<PropertyType>::checkRequirement()
    {
        using namespace armarx::exceptions;

        if (definition.isRequired())
        {
            if (!isSet())
            {
                throw local::MissingRequiredPropertyException(
                    prefix + definition.getPropertyName(),
                    properties);
            }
        }
    }

    template <typename PropertyType>
    bool
    Property<PropertyType>::expandEnvironmentVariables(const std::string& input, std::string& output)
    {
        output = input;

        if (input == "")
        {
            return false;
        }

        bool res = true;
        bool goOn = false;

        do
        {
            size_t foundStart = output.find("${");

            if (foundStart != std::string::npos)
            {
                goOn = true;
                size_t foundEnd = output.find("}", foundStart);

                if (foundEnd != std::string::npos)
                {
                    std::string envVar = output.substr(foundStart + 2, foundEnd - foundStart - 2);
                    std::string replacement;
                    char* envVarENV = getenv(envVar.c_str());

                    if (envVarENV)
                    {
                        replacement = envVarENV;
                    }
                    else
                    {
                        std::cout << "WARNING: Could not expand environment variable: " << envVar << std::endl;
                    }

                    output.replace(foundStart, foundEnd - foundStart + 1, replacement);
                }
                else
                {
                    std::cout << "ERROR: Found '${' but missing '}' in " << input << std::endl;
                    goOn = false;
                    res = false;
                }

            }
            else
            {
                goOn = false;
            }

        }
        while (goOn);

        return res;
    }


    template <typename PropertyType>
    void
    Property<PropertyType>::removeQuotes(const std::string& input, std::string& output)
    {
        output = input;

        if (input.size() < 2)
        {
            return;
        }

        if ((input[0] == '"' && input[input.size() - 1] == '"')
            || (input[0] == '\'' && input[input.size() - 1] == '\'')
           )
        {
            output = input.substr(1, input.size() - 2);
        }
    }

    extern template class Property <int>;
    extern template class Property <float>;
    extern template class Property <double>;
    extern template class Property <std::string>;
    extern template class Property <bool>;

}


