/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Jan Issac (jan dot issac at gmx dot de)
 * @date       2011
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include "PropertyDefinitionInterface.h"
#include "PropertyDefinitionFormatter.h"
#include <boost/shared_ptr.hpp>

#include <Ice/Properties.h>

#include <functional>
#include <type_traits>


namespace armarx
{
    /* ====================================================================== */
    /* === Property Definition Declaration ================================== */
    /* ====================================================================== */

    /**
     * @ingroup properties
     *
     * @class PropertyDefinition
     * @brief PropertyDefinition defines a property that will be available
     *        within the PropertyUser
     *
     * PropertyDefinition provides the definition and description of a
     * property that may be definined in a config file or passed via command
     * line as an option. Depending which constructor is used a property
     * is either required or optional. In the latter case a default value has
     * to be specified.
     *
     * The property specific value type (@em PropertyType) can be arbitrary, yet
     * it is devided into these three groups which are handled internally
     * differently:
     *  - String values
     *  - Arithmetic values (integral & floating point types)
     *  - Custom objects (Classes, Structs, Enums)
     *
     * @section fluentinterface Fluent Interface
     *
     * All setters are self referential and therefor can be chained to
     * simplify the implementation and increase the readability.
     *
     * @subsection fluentinterface_example Example
     * @code
     * PropertyDefinition<float> frameRateDef("MyFrameRate", "Capture frame rate");
     *
     * frameRateDef
     *     .setMatchRegex("\\d+(.\\d*)?")
     *     .setMin(0.0f)
     *     .setMax(60.0f);
     * @endcode
     */
    template<typename PropertyType>
    class PropertyDefinition :
        public PropertyDefinitionBase
    {
    public:
        template <int> struct Qualifier
        {
            Qualifier(int) {}
        };

        using PropertyTypePtr = boost::shared_ptr<PropertyType>;
        using ValueEntry = std::pair<std::string, PropertyType>;
        using PropertyValuesMap = std::map<std::string, ValueEntry>;
        using PropertyFactoryFunction = std::function<PropertyType(std::string)>;

        /**
         * Constructs a property definition of a required property
         *
         * @param propertyName  Name of the property used in config files
         * @param description   Mandatory property description
         */
        PropertyDefinition(const std::string& propertyName, const std::string& description, PropertyDefinitionBase::PropertyConstness constness = PropertyDefinitionBase::eConstant);

        /**
         * Constructs a property definition of an optional property
         *
         * @param propertyName  Name of the property used in config files
         * @param defaultValue  Property default value of the type
         *                      @em PropertyType
         * @param description   Mandatory property description
         */
        PropertyDefinition(const std::string& propertyName,
                           PropertyType defaultValue,
                           const std::string& description,
                           PropertyDefinitionBase::PropertyConstness constness = PropertyDefinitionBase::eConstant);

        /**
         * Maps a string value onto a value of the specified template type
         *
         * @param valueString   Property string value key
         * @param value         Property value
         *
         * @return self reference
         */
        PropertyDefinition<PropertyType>& map(const std::string& valueString,
                                              PropertyType value);


        /**
         * Sets the factory function that creates the specified template type from the actual string value
         *
         * @param func         the factory function
         *
         * @return self reference
         */
        PropertyDefinition<PropertyType>& setFactory(const PropertyFactoryFunction& func);

        /**
         * Sets whether the property value matching is case insensitive.
         *
         * @param caseInsensitive   Case sensitivity state
         *
         * @return self reference
         */
        PropertyDefinition<PropertyType>& setCaseInsensitive(bool caseInsensitive);

        /**
         * Sets whether for string values environment varbiale expanding should be considered.
         *
         * @param expand   Expand entries like '${ENV_VAR}' to the according values of the environment.
         *
         * @return self reference
         */
        PropertyDefinition<PropertyType>& setExpandEnvironmentVariables(bool expand);

        /**
         * Sets whether for string values leading and trailing quotes should be removed.
         *
         * @param removeQuotes The indicator.
         *
         * @return self reference
         */
        PropertyDefinition<PropertyType>& setRemoveQuotes(bool removeQuotes);

        /**
        * Sets the regular expression which the value has to be matched with.
        *
        * @param expr              Value regular expression
        *
        * @return self reference
        */
        PropertyDefinition<PropertyType>& setMatchRegex(const std::string& expr);

        /**
         * Sets the min allowed numeric value
         *
         * @param min   Min. allowed value
         *
         * @return self reference
         */
        PropertyDefinition<PropertyType>& setMin(double min);

        /**
         * Sets the max allowed numeric value
         *
         * @param max   Max. allowed value
         *
         * @return self reference
         */
        PropertyDefinition<PropertyType>& setMax(double max);

        bool isCaseInsensitive() const;
        bool expandEnvironmentVariables() const;
        bool removeQuotes() const;
        std::string getMatchRegex() const;
        double getMin() const;
        double getMax() const;
        std::string getPropertyName() const;
        PropertyValuesMap& getValueMap();
        PropertyType getDefaultValue();
        PropertyFactoryFunction getFactory() const;
        std::string getDefaultAsString() override;

        /**
         * @see PropertyDefinitionBase::toString()
         */
        std::string toString(PropertyDefinitionFormatter& formatter, const std::string& value) override;

        std::string getDescription() const;

    protected:
        /**
         * Main property map
         */
        PropertyValuesMap propertyValuesMap;

        /**
         * Property name
         */
        std::string propertyName;

        /**
         * Property description
         */
        std::string description;

        /**
         * Fallback/Default property value
         */
        PropertyType defaultValue;


        /**
          * Builder function
          */
        PropertyFactoryFunction factory;

        /**
         * Regular expression to approve a required value pattern
         */
        std::string regex;

        /**
         * Case sensitivity indicator
         */
        bool caseInsensitive;

        /**
         * Exand environments variables indicator (standard: true)
         */
        bool expandEnvVars;

        /**
         * Remove leading and trailing quotes indicator (standard: true)
         * First and last character of a string property value are checked for quotes.
         * E.g.
         *     "test" -> test
         *     'test' -> test
         */
        bool stringRemoveQuotes;

        /**
         * Upper bound of numeric values (used for numeric value retrieval
         * without mapping)
         */
        double max;

        /**
         * Lower bound of numeric values (used for numeric value retrieval
         * without mapping)
         */
        double min;

    private:

        /// Specialization for bool to return "false"/"true" instead of "0"/"1".
        template <typename T>
        typename std::enable_if<std::is_same<T, bool>::value, std::string>::type
        getDefaultAsStringImpl(Qualifier<3> = 0);

        template <typename T>
        typename std::enable_if < !std::is_same<T, bool>::value&&  std::is_arithmetic<T>::value, std::string >::type
        getDefaultAsStringImpl(Qualifier<0> = 0);

        template <typename T>
        typename std::enable_if<std::is_same<T, std::string>::value, std::string>::type
        getDefaultAsStringImpl(Qualifier<1> = 0);

        template <typename T>
        typename std::enable_if < !(std::is_same<T, std::string>::value || std::is_arithmetic<T>::value), std::string >::type
        getDefaultAsStringImpl(Qualifier<2> = 0);

        /*
        template <typename T>
        typename std::enable_if<std::is_floating_point<T>::value, std::string>::type
        getMinImpl(Qualifier<0> = 0) const;

        template <typename T>
        typename std::enable_if<std::is_integral<T>::value, std::string>::type
        getMinImpl(Qualifier<1> = 0) const;

        template <typename T>
        typename std::enable_if<!(std::is_same<T, std::string>::value || std::is_arithmetic<T>::value), std::string>::type
        getMinImpl(Qualifier<2> = 0);

        template <typename T>
        typename std::enable_if<std::is_floating_point<T>::value, std::string>::type
        getMaxImpl(Qualifier<0> = 0) const;

        template <typename T>
        typename std::enable_if<std::is_integral<T>::value, std::string>::type
        getMaxImpl(Qualifier<1> = 0) const;

        template <typename T>
        typename std::enable_if<!(std::is_same<T, std::string>::value || std::is_arithmetic<T>::value), std::string>::type
        getMaxImpl(Qualifier<2> = 0);
        */
    };



    template <>
    inline PropertyDefinition<std::string>&
    PropertyDefinition<std::string>::setMin(double min)
    {
        (void) min; // unused
        return *this;
    }

    template <>
    inline PropertyDefinition<std::string>&
    PropertyDefinition<std::string>::setMax(double max)
    {
        (void) max; // unused
        return *this;
    }


    extern template class PropertyDefinition<int>;
    extern template class PropertyDefinition<float>;
    extern template class PropertyDefinition<double>;
    extern template class PropertyDefinition<std::string>;
    extern template class PropertyDefinition<bool>;
}

namespace std
{
    extern template class map<string, float>;
    extern template class map<string, pair<string, int> >;
    extern template class map<string, pair<string, float> >;
    extern template class map<string, pair<string, double> >;
    extern template class map<string, pair<string, string> >;
    extern template class map<string, pair<string, bool> >;
}

