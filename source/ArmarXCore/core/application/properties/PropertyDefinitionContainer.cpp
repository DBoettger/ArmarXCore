/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Mirko Waechter (waechter at kit dot edu)
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "PropertyDefinitionContainer.h"
#include "PropertyDefinition.hpp"
#include "Property.h"
#include "ArmarXCore/core/exceptions/Exception.h"
#include "ArmarXCore/core/exceptions/local/InvalidPropertyValueException.h"
#include "ArmarXCore/core/exceptions/local/MissingRequiredPropertyException.h"
#include "ArmarXCore/core/exceptions/local/UnmappedValueException.h"
#include "ArmarXCore/core/exceptions/local/ValueRangeExceededException.h"
#include <ArmarXCore/core/logging/Logging.h>
#include <map>

#include <Ice/Properties.h>

#include <string>

// Workaround for QTBUG-22829
#ifndef Q_MOC_RUN
//#include <boost/lexical_cast.hpp>
#endif
#include <boost/algorithm/string/trim.hpp>

using namespace armarx;

template <>
PropertyDefinition<bool>&
PropertyDefinitionContainer::defineOptionalProperty(
    const std::string& name,
    bool defaultValue,
    const std::string& description,
    PropertyDefinitionBase::PropertyConstness constness)
{
    PropertyDefinition<bool>* def =
        new PropertyDefinition<bool>(name,
                                     defaultValue,
                                     description,
                                     constness);

    def->setTypeIdName(typeid(bool).name());

    definitions[name] = static_cast<PropertyDefinitionBase*>(def);

    def->setCaseInsensitive(true)
    .map("true",    true)
    .map("yes",     true)
    .map("1",       true)
    .map("false",   false)
    .map("no",      false)
    .map("0",       false);

    return *def;
}


template <>
PropertyDefinition<bool>&
PropertyDefinitionContainer::defineRequiredProperty(
    const std::string& name,
    const std::string& description,
    PropertyDefinitionBase::PropertyConstness constness)
{
    PropertyDefinition<bool>* def =
        new PropertyDefinition<bool>(name, description, constness);

    def->setTypeIdName(typeid(bool).name());

    definitions[name] = static_cast<PropertyDefinitionBase*>(def);

    def->setCaseInsensitive(true)
    .map("true",    true)
    .map("yes",     true)
    .map("1",       true)
    .map("false",   false)
    .map("no",      false)
    .map("0",       false);


    return *def;
}

std::map<std::string, std::string> parseMap(std::string input)
{
    std::map<std::string, std::string> result;
    auto splits = armarx::Split(input, ",");
    for (auto& elem : splits)
    {
        boost::trim(elem);
        auto split = armarx::Split(elem, ":");
        if (split.size() == 2)
        {
            result[boost::trim_copy(split[0])] = boost::trim_copy(split[1]);
        }
    }
    return result;
}

template <>
PropertyDefinition<std::map<std::string, std::string>>&
        PropertyDefinitionContainer::defineOptionalProperty(
            const std::string& name,
            std::map<std::string, std::string> defaultValue,
            const std::string& description,
            PropertyDefinitionBase::PropertyConstness constness)
{
    PropertyDefinition<std::map<std::string, std::string>>* def =
                new PropertyDefinition<std::map<std::string, std::string>>(name,
                        defaultValue,
                        description,
                        constness);

    def->setTypeIdName(typeid(std::map<std::string, std::string>).name());

    definitions[name] = static_cast<PropertyDefinitionBase*>(def);
    PropertyDefinition<std::map<std::string, std::string>>::PropertyFactoryFunction f = &parseMap;
    def->setFactory(f);
    return *def;
}

template <>
PropertyDefinition<std::map<std::string, std::string>>&
        PropertyDefinitionContainer::defineRequiredProperty(
            const std::string& name,
            const std::string& description,
            PropertyDefinitionBase::PropertyConstness constness)
{
    PropertyDefinition<std::map<std::string, std::string>>* def =
                new PropertyDefinition<std::map<std::string, std::string>>(name, description, constness);

    def->setTypeIdName(typeid(std::map<std::string, std::string>).name());

    definitions[name] = static_cast<PropertyDefinitionBase*>(def);

    PropertyDefinition<std::map<std::string, std::string>>::PropertyFactoryFunction f = &parseMap;
    def->setFactory(f);

    return *def;
}

Ice::StringSeq split(std::string input)
{
    return Split(input, ",", true);
}

Ice::StringSeq parseEscapedCommaList(std::string input)
{
    Ice::StringSeq args;

    const boost::regex re(",(?=(?:[^\\\"]*\\\"[^\\\"]*\\\")*[^\\\"]*$)");

    boost::match_results<std::string::const_iterator> what;
    boost::match_flag_type flags = boost::match_default;
    std::string::const_iterator s = input.begin();
    std::string::const_iterator e = input.end();
    size_t begin = 0;
    while (boost::regex_search(s, e, what, re, flags))
    {
        int pos = what.position();
        auto arg = input.substr(begin, pos);
        boost::trim(arg);
        if (!arg.empty())
        {
            args.push_back(arg);
        }
        std::string::difference_type l = what.length();
        std::string::difference_type p = what.position();
        begin += l + p;
        s += p + l;
    }
    std::string lastArg = input.substr(begin);
    boost::trim(lastArg);
    if (!lastArg.empty())
    {
        args.push_back(lastArg);
    }
    return args;
}

template <>
PropertyDefinition<Ice::StringSeq>&
PropertyDefinitionContainer::defineOptionalProperty(
    const std::string& name,
    Ice::StringSeq defaultValue,
    const std::string& description,
    PropertyDefinitionBase::PropertyConstness constness)
{
    PropertyDefinition<Ice::StringSeq>* def =
        new PropertyDefinition<Ice::StringSeq>(name,
                defaultValue,
                description,
                constness);

    def->setTypeIdName(typeid(Ice::StringSeq).name());

    definitions[name] = static_cast<PropertyDefinitionBase*>(def);
    PropertyDefinition<Ice::StringSeq>::PropertyFactoryFunction f = &parseEscapedCommaList;
    def->setFactory(f);
    return *def;
}

template <>
PropertyDefinition<Ice::StringSeq>&
PropertyDefinitionContainer::defineRequiredProperty(
    const std::string& name,
    const std::string& description,
    PropertyDefinitionBase::PropertyConstness constness)
{
    typedef Ice::StringSeq ResultType;
    PropertyDefinition<ResultType>* def =
        new PropertyDefinition<ResultType>(name, description, constness);

    def->setTypeIdName(typeid(ResultType).name());

    definitions[name] = static_cast<PropertyDefinitionBase*>(def);

    PropertyDefinition<ResultType>::PropertyFactoryFunction f = &parseEscapedCommaList;
    def->setFactory(f);

    return *def;
}

PropertyDefinitionContainer::PropertyDefinitionContainer(const std::string& prefix)
{
    setDescription(prefix + " properties");

    this->prefix = prefix + ".";
}

PropertyDefinitionContainer::~PropertyDefinitionContainer()
{
    DefinitionContainer::iterator it = definitions.begin();

    while (it != definitions.end())
    {
        delete it->second;

        it++;
    }

    definitions.clear();
}

Ice::PropertiesPtr PropertyDefinitionContainer::getProperties()
{
    return properties;
}


void PropertyDefinitionContainer::setProperties(Ice::PropertiesPtr properties)
{
    this->properties  = properties;
}


PropertyDefinitionBase*
PropertyDefinitionContainer::getDefinitionBase(const std::string& name)
{
    // check definition existance
    if (definitions.find(name) == definitions.end())
    {
        throw armarx::LocalException(name + " property is not defined.");
    }

    PropertyDefinitionBase* def = definitions[name];
    return def;
}


std::string PropertyDefinitionContainer::toString(PropertyDefinitionFormatter& formatter)
{
    formatter.setPrefix(prefix);
    std::string definitionStr;

    DefinitionContainer::iterator it = definitions.begin();
    Ice::PropertyDict propertiesMap;

    if (properties)
    {
        propertiesMap = properties->getPropertiesForPrefix(getPrefix());
    }
    while (it != definitions.end())
    {
        definitionStr += it->second->toString(formatter, getValue(it->first));
        propertiesMap.erase(it->first);
        it++;
    }

    if (properties)
    {
        formatter.setPrefix("");
    }

    Ice::StringSeq values;
    for (Ice::PropertyDict::iterator property = propertiesMap.begin(); property != propertiesMap.end(); property++)
    {
        definitionStr += formatter.formatDefinition(property->first,
                         "",
                         "",
                         "",
                         "",
                         "",
                         "",
                         "",
                         values,
                         property->second);
    }

    return definitionStr;
}

std::string PropertyDefinitionContainer::getDescription() const
{
    return description;
}

void PropertyDefinitionContainer::setDescription(const std::string& description)
{
    this->description = description;
}

void PropertyDefinitionContainer::setPrefix(std::string prefix)
{
    this->prefix = prefix;
}

std::string PropertyDefinitionContainer::getPrefix()
{
    return prefix;
}

bool PropertyDefinitionContainer::isPropertySet(const std::string& name)
{
    Ice::PropertyDict selectedProperties = properties->getPropertiesForPrefix(getPrefix());
    return (selectedProperties.count(name) > 0);
}

std::string PropertyDefinitionContainer::getValue(const std::string& name)
{
    if (properties)
    {
        return properties->getPropertiesForPrefix(getPrefix())[name];
    }
    else
    {
        return "";
    }
}

Ice::PropertyDict PropertyDefinitionContainer::getPropertyValues(const std::string& prefix) const
{
    Ice::PropertyDict result;
    for (const auto& elem : definitions)
    {
        std::string value;
        if (properties)
        {
            value = properties->getProperty(this->prefix + elem.first);
        }
        else
        {
            ARMARX_INFO << " properties NULL";
        }
        if (value.empty())
        {
            PropertyDefinitionBase* def = elem.second;
            value = def->getDefaultAsString();
            //            PropertyDefinition<std::string>* def = dynamic_cast<PropertyDefinition<std::string>*>(elem.second);
            //        result[elem.first] = Property<std::string>(
            //                                 *def,
            //                                 prefix,
            //                                 properties).getValue();
        }

        result[prefix + elem.first] = value;

    }
    return result;
}

std::string PropertyDefinitionContainer::eigenVectorPropertyDescription(
    const std::string& description, long size, char delim)
{
    std::stringstream ss;
    ss << description << "\n";
    ss << "(Format: Sequence of ";
    if (size < 0)
    {
        ss << "n";
    }
    else
    {
        ss << size;
    }
    ss << " numbers delimited by '" << delim << "'.)";

    return ss.str();
}
