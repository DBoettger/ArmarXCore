/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Christian Mandery (mandery@kit.edu)
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include "PropertyDefinitionDoxygenComponentPagesFormatter.h"

#include <boost/algorithm/string.hpp>
#include <boost/regex.hpp>

using namespace armarx;

std::string PropertyDefinitionDoxygenComponentPagesFormatter::formatDefinition(
    std::string name,
    std::string description,
    std::string min,
    std::string max,
    std::string default_,
    std::string casesensitivity,
    std::string requirement,
    std::string regex,
    std::vector<std::string> values,
    std::string value)
{
    std::string pageHeader;

    if (prefix != lastPrefix)
    {
        const boost::regex rx("[a-zA-Z0-9_\\-]+\\.([a-zA-Z0-9_\\-]+)\\.");
        boost::match_results<std::string::const_iterator> results;

        pageHeader = "\n<!-- Prefix: " + prefix + " -->\n";

        if (boost::regex_match(prefix, results, rx))  // Skip application properties (no component set) for now
        {
            pageHeader += "\\defgroup " + results[1] + "_properties " + results[1] + " Properties\n";
            pageHeader += "\\ingroup Component-" + results[1] + " componentproperties\n";
            pageHeader += "\\brief This page shows the properties for the component \\ref Component-" + results[1] + " \"" + results[1] + "\".\n\n";
        }
        else if (!lastPrefix.empty())
        {
            throw std::runtime_error("Non-component (application-specific) properties not at beginning of iteration! (this should never happen)");
        }

        // pageHeader += "<th><td>Name</td><td>Description</td><td>Default</td><td>Bounds</td><td>Case-sensitive?</td><td>Required</td><td>Regex</td><td>Values</td></th>\n";
        pageHeader += "Name & Description |  Default | Bounds | Case-sensitive? | Required | Regex | Values\n";
        pageHeader += "---- |  ------- | ------ | --------------- | -------- | ----- | ------\n";

        lastPrefix = prefix;
    }

    std::string output = getFormat();

    boost::replace_first(output, "%name%",          escapeMarkdown(formatName(getPrefix() + name)));
    boost::replace_first(output, "%description%",   escapeMarkdown(formatDescription(description)));
    boost::replace_first(output, "%bounds%",        escapeMarkdown(formatBounds(min, max)));
    boost::replace_first(output, "%default%",       escapeMarkdown(formatDefault(default_)));
    boost::replace_first(output, "%casesensitive%", escapeMarkdown(formatCaseSensitivity(casesensitivity)));
    boost::replace_first(output, "%required%",      escapeMarkdown(formatRequirement(requirement)));
    boost::replace_first(output, "%regex%",         escapeMarkdown(formatRegex(regex)));
    boost::replace_first(output, "%values%",        escapeMarkdown(formatValues(values)));

    return pageHeader + output;
}

std::string PropertyDefinitionDoxygenComponentPagesFormatter::formatValues(std::vector<std::string> mapValues)
{
    // Largely copied from PropertyDefinitionDoxygenFormatter
    std::string valueStrings;

    if (mapValues.size() > 0)
    {
        valueStrings += "{";

        std::vector<std::string>::iterator it = mapValues.begin();

        while (it != mapValues.end())
        {
            if (!it->empty())
            {
                valueStrings += formatValue(*it);
            }

            ++it;

            if (it != mapValues.end())
            {
                valueStrings += ", ";
            }
        }

        valueStrings += "}";
    }

    return valueStrings;
}

std::string PropertyDefinitionDoxygenComponentPagesFormatter::formatAttribute(std::string name, std::string details)
{
    return details;
}

std::string PropertyDefinitionDoxygenComponentPagesFormatter::getFormat()
{
    // return std::string("<tr><td>%name%</td><td>%description%</td><td>%default%</td><td>%bounds%</td><td>%casesensitive%</td><td>%required%</td><td>%regex%</td><td>%values%</td></tr>\n");
    return std::string("<b>%name%</b> <br/> %description% | <b>%default%</b> | %bounds% | %casesensitive% | %required% | %regex% | %values%\n");
}

std::string PropertyDefinitionDoxygenComponentPagesFormatter::escapeMarkdown(std::string s)
{
    boost::replace_all(s, "\"", "\\\"");
    boost::replace_all(s, "*", "\\*");
    boost::replace_all(s, "#", "\\#");
    boost::replace_all(s, "(", "\\(");
    boost::replace_all(s, ")", "\\)");
    boost::replace_all(s, "[", "\\[");
    boost::replace_all(s, "]", "\\]");
    boost::replace_all(s, "<", "\\<");
    boost::replace_all(s, ">", "\\>");
    // boost::replace_all(s, "/", "\\/");
    // boost::replace_all(s, "{", "\\{");
    // boost::replace_all(s, "}", "\\}");

    if (s.empty())
    {
        s = "-";
    }

    return s;
}
