/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Jan Issac (jan dot issac at mail dot com)
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "IceProperties.h"
#include <Ice/Initialize.h>
#include <ArmarXCore/core/logging/Logging.h>
#include "ArmarXCore/core/exceptions/local/PropertyInheritanceCycleException.h"

using namespace armarx;
using namespace std;

Ice::PropertiesPtr IceProperties::create(const Ice::PropertiesPtr& iceProperties)
{
    Ice::StringSeq strSeq;

    return new IceProperties(Ice::createProperties(strSeq, iceProperties));
}

Ice::PropertiesPtr IceProperties::create(Ice::StringSeq& propertySeq,
        const Ice::PropertiesPtr& iceProperties)
{
    Ice::PropertiesPtr armarxIceProperties =
        new IceProperties(Ice::createProperties(propertySeq, iceProperties));

    // adds all options and properties
    armarxIceProperties->parseCommandLineOptions(string(), propertySeq);

    return armarxIceProperties;
}

Ice::PropertiesPtr IceProperties::create(int& argn,
        char* argv[],
        const Ice::PropertiesPtr& iceProperties)
{
    Ice::PropertiesPtr armarxIceProperties =
        new IceProperties(Ice::createProperties(argn, argv, iceProperties));

    Ice::StringSeq propertySeq = Ice::argsToStringSeq(argn, argv);

    // adds all options and properties
    armarxIceProperties->parseCommandLineOptions(string(), propertySeq);

    return armarxIceProperties;
}


IceProperties::IceProperties(const Ice::PropertiesPtr iceProperties):
    internalProperties(iceProperties),
    modified(true)
{
    inheritanceSolver = new InheritanceSolver();
}

IceProperties::IceProperties(const IceProperties& source):
    Shared(source)
{
    internalProperties = source.internalProperties->clone();
    modified = source.modified;
    if (source.inheritanceSolver)
    {
        inheritanceSolver = new InheritanceSolver(*source.inheritanceSolver);
    }
}

IceProperties::~IceProperties()
{

}

std::string IceProperties::getProperty(const ::std::string& name)
{
    update();

    return internalProperties->getProperty(name);
}

std::string IceProperties::getPropertyWithDefault(const ::std::string& name,
        const ::std::string& defaultValue)
{
    update();

    return internalProperties->getPropertyWithDefault(name, defaultValue);
}

Ice::Int IceProperties::getPropertyAsInt(const ::std::string& name)
{
    update();

    return internalProperties->getPropertyAsInt(name);
}

Ice::Int IceProperties::getPropertyAsIntWithDefault(const ::std::string& name,
        ::Ice::Int defaultValue)
{
    update();

    return internalProperties->getPropertyAsIntWithDefault(name, defaultValue);
}

Ice::StringSeq IceProperties::getPropertyAsList(const ::std::string& name)
{
    update();

    return internalProperties->getPropertyAsList(name);
}

Ice::StringSeq IceProperties::getPropertyAsListWithDefault(
    const ::std::string& name, const ::Ice::StringSeq& defaultValue)
{
    update();

    return internalProperties->getPropertyAsListWithDefault(name, defaultValue);
}

Ice::PropertyDict IceProperties::getPropertiesForPrefix(const ::std::string& prefix)
{
    update();

    return internalProperties->getPropertiesForPrefix(prefix);
}

void IceProperties::setProperty(const ::std::string& name, const ::std::string& value)
{
    internalProperties->setProperty(name, value);
    setModified(true);
}

Ice::StringSeq IceProperties::getCommandLineOptions()
{
    update();

    return internalProperties->getCommandLineOptions();
}

Ice::StringSeq IceProperties::parseCommandLineOptions(const ::std::string& prefix,
        const ::Ice::StringSeq& options)
{
    Ice::StringSeq optionSeq = internalProperties->parseCommandLineOptions(prefix, options);

    setModified(true);
    update();

    return optionSeq;
}

Ice::StringSeq IceProperties::parseIceCommandLineOptions(const ::Ice::StringSeq& options)
{
    Ice::StringSeq optionSeq = internalProperties->parseIceCommandLineOptions(options);

    setModified(true);
    update();

    return optionSeq;
}

void IceProperties::load(const ::std::string& fileName)
{
    internalProperties->load(fileName);

    setModified(true);
}

Ice::PropertiesPtr IceProperties::clone()
{
    //    return new IceProperties(*this);
    return internalProperties->clone();
}

void IceProperties::setModified(bool modified)
{
    this->modified = modified;
}

bool IceProperties::isModified()
{
    return modified;
}

void IceProperties::update()
{
    if (isModified())
    {
        if (inheritanceSolver.get() != nullptr)
        {
            inheritanceSolver->resolveInheritance(internalProperties);
        }
    }

    setModified(false);
}

IceProperties::InheritanceSolverPtr IceProperties::getInheritanceSolver()
{
    return inheritanceSolver;
}

void IceProperties::setInheritanceSolver(
    const IceProperties::InheritanceSolverPtr& inheritanceSolver)
{
    this->inheritanceSolver = inheritanceSolver;
}

IceProperties::InheritanceSolver::~InheritanceSolver()
{

}

void IceProperties::InheritanceSolver::resolveInheritance(Ice::PropertiesPtr& properties)
{
    NamespaceMap namespaces;

    extractNamespaces(properties, namespaces);

    NamespaceMap::const_iterator namespacesIter = namespaces.begin();

    while (namespacesIter != namespaces.end())
    {
        resolveNamespaceInheritance(namespacesIter->first, properties);

        ++namespacesIter;
    }
}

void IceProperties::InheritanceSolver::extractNamespaces(const Ice::PropertiesPtr& properties,
        NamespaceMap& namespaces)
{
    namespaces.clear();

    Ice::PropertyDict propertyDict = properties->getPropertiesForPrefix(string());

    // extract namespaces
    Ice::PropertyDict::iterator dictIter = propertyDict.begin();

    while (dictIter != propertyDict.end())
    {
        string propertyFullName = dictIter->first;

        unsigned nameDelimiter = propertyFullName.find_last_of(".");
        string propertyNamespace = propertyFullName.substr(0, nameDelimiter);
        namespaces[propertyNamespace] = true;

        ++dictIter;
    }
}

bool IceProperties::InheritanceSolver::hasParent(const string& childNamespace,
        const Ice::PropertiesPtr& properties)
{
    std::string inheritanceProperty = childNamespace + "." + INHERITANCE_KEYWORD;

    if (properties->getPropertyWithDefault(
            inheritanceProperty, "::NOT-SET::").compare("::NOT-SET::") == 0)
    {
        return false;
    }

    return true;
}

string IceProperties::InheritanceSolver::getParent(const string& childNamespace,
        const Ice::PropertiesPtr& properties)
{
    const std::string inheritanceProperty = childNamespace + "." + INHERITANCE_KEYWORD;

    return properties->getProperty(inheritanceProperty);
}

bool IceProperties::InheritanceSolver::isInHeritageLine(const vector<string>& heritageLine,
        const string& namespace_)
{
    vector<string>::const_iterator heritageLineIter = heritageLine.begin();

    while (heritageLineIter != heritageLine.end())
    {
        if ((*heritageLineIter).compare(namespace_) == 0)
        {
            return true;
        }

        ++heritageLineIter;
    }

    return false;
}

string IceProperties::InheritanceSolver::stripNamespace(const string& propertyName,
        const string& namespace_)
{
    return propertyName.substr(namespace_.length() + 1);
}

void IceProperties::InheritanceSolver::resolveNamespaceInheritance(const string& childNamespace,
        Ice::PropertiesPtr& properties)
{
    vector<string> heritageLine;

    if (hasParent(childNamespace, properties))
    {
        inherit(childNamespace, getParent(childNamespace, properties), properties, heritageLine);
    }
}

void IceProperties::InheritanceSolver::inherit(const string& childNamespace,
        const string& parentNamespace,
        Ice::PropertiesPtr& properties,
        vector<string> heritageLine)
{
    heritageLine.push_back(childNamespace);

    // check parent inheritance
    if (hasParent(parentNamespace, properties))
    {
        if (isInHeritageLine(heritageLine, parentNamespace))
        {
            heritageLine.push_back(parentNamespace);
            throw exceptions::local::PropertyInheritanceCycleException(heritageLine);
        }

        inherit(parentNamespace, getParent(parentNamespace, properties), properties, heritageLine);
    }

    // perform actual inheritance
    Ice::PropertyDict parentProperties = properties->getPropertiesForPrefix(parentNamespace);

    Ice::PropertyDict::iterator parentPropIter = parentProperties.begin();

    while (parentPropIter != parentProperties.end())
    {
        string newPropertyName = childNamespace + "." +
                                 stripNamespace(parentPropIter->first, parentNamespace);

        // Check whether it has been already set. Defined properties override inherited ones.
        if (properties->getProperty(newPropertyName).empty())
        {
            properties->setProperty(newPropertyName, parentPropIter->second);
        }

        ++parentPropIter;
    }

    // remove inheritance tag
    properties->setProperty(childNamespace + "." + INHERITANCE_KEYWORD, "");
}
