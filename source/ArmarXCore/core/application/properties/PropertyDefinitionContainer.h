/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Jan Issac (jan dot issac at gmx dot de)
 * @date       2012
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once


#include "PropertyDefinitionInterface.h"
#include "PropertyDefinition.h"
#include "PropertyDefinitionFormatter.h"

#include <ArmarXCore/core/exceptions/Exception.h>

#include <boost/lexical_cast.hpp>


namespace armarx
{
    class PropertyDefinitionBase;

    /**
     * @ingroup properties
     *
     * @class PropertyDefinitionContainer
     * @brief PropertyDefinitionContainer
     */
    class PropertyDefinitionContainer : public virtual IceUtil::Shared
    {
    public:

        typedef std::map<std::string, PropertyDefinitionBase*>
        DefinitionContainer;

        PropertyDefinitionContainer(const std::string& prefix);

        ~PropertyDefinitionContainer() override;

        Ice::PropertiesPtr getProperties();

        void setProperties(Ice::PropertiesPtr properties);

        template <typename PropertyType>
        PropertyDefinition<PropertyType>& defineRequiredProperty(
            const std::string& name,
            const std::string& description = "",
            PropertyDefinitionBase::PropertyConstness constness = PropertyDefinitionBase::eConstant);

        template <typename PropertyType>
        PropertyDefinition<PropertyType>& defineOptionalProperty(
            const std::string& name,
            PropertyType defaultValue,
            const std::string& description = "",
            PropertyDefinitionBase::PropertyConstness constness = PropertyDefinitionBase::eConstant);

        /**
         * @brief Define a required property for an Eigen vector type.
         * The EigenVectorType can be any Eigen::Vector type, (Vector3f, VectorXd, ...).
         *
         * @param delimiter the delimiter between vector coefficients in the string representation
         *
         * Usage example:
         * @code
         * // Definition
         *  // accepts e.g. "1.0 2.0 3.0" and "1.0 -.3 .5"
         *  defineRequiredPropertyVector<Eigen::Vector3f>("Position", "The position.");
         *  // accepts e.g. "640x480" and "1920x1080"
         *  defineRequiredPropertyVector<Eigen::Vector2i>("Resolution", "The resolution.", 'x');
         *  // accepts e.g. "0,1,2,3,4,5,6,7,8,9" and "1"
         *  defineRequiredPropertyVector<Eigen::VectorXd>("Values", "The values.", ',');
         * 
         * // Usage:
         *  Eigen::Vector3f position = getProperty<Eigen::Vector3f>("Position");
         *  Eigen::Vector2i resolution = getProperty<Eigen::Vector2i>("Resolution");
         *  Eigen::VectorXd values = getProperty<Eigen::VectorXd>("Values");
         *  // .getValue() can trigger Eigen's implicit conversion
         *  Eigen::VectorXf positionX = getProperty<Eigen::VectorXf>("Position").getValue();
         * 
         * // Specification on command line:
         *  ./<binary> --ArmarX.<ComponentName>.Position="1 2 3"
         * @endcode
         */
        template <typename EigenVectorType>
        PropertyDefinition<EigenVectorType>& defineRequiredPropertyVector(
            const std::string& name,
            const std::string& description = "",
            char delimiter = ' ',
            PropertyDefinitionBase::PropertyConstness constness = PropertyDefinitionBase::eConstant);

        /**
         * @brief Define a required property for an Eigen vector type.
         * The EigenVectorType can be any Eigen::Vector type, (Vector3f, VectorXd, ...).
         * @param delimiter the delimiter between vector coefficients in the string representation
         * 
         * Usage example:
         * @code
         * // Definition
         *  // defaults to "0 0 0", accepts e.g. "1.0 2.0 3.0" and "1.0 -.3 .5"
         *  defineOptionalPropertyVector<Eigen::Vector3f>(
         *      "Position", Eigen::Vector3f::Zero(), "The position.");
         *  // defaults to "640x480", accepts e.g. "1920x1080"
         *  defineOptionalPropertyVector<Eigen::Vector2i>(
         *      "Resolution", Eigen::Vector2f(640, 480), "The resolution.", 'x');
         *  // defaults to "0,1,2,3,4,5,6,7,8,9", accepts e.g. "1" or "1,2,3"
         *  Eigen::VectorXd valuesDefault(10);
         *  valuesDefault << 0, 1, 2, 3, 4, 5, 6, 7, 8, 9;
         *  defineOptionalPropertyVector<Eigen::VectorXd>(
         *      "Values", valuesDefault, "The values.", ',');
         * 
         * // Usage:
         *  Eigen::Vector3f position = getProperty<Eigen::Vector3f>("Position");
         *  Eigen::Vector2i resolution = getProperty<Eigen::Vector2i>("Resolution");
         *  Eigen::VectorXd values = getProperty<Eigen::VectorXd>("Values");
         *  // .getValue() can trigger Eigen's implicit conversion
         *  Eigen::VectorXf positionX = getProperty<Eigen::VectorXf>("Position").getValue();
         * 
         * // Specification on command line:
         *  ./<binary> --ArmarX.<ComponentName>.Position="1 2 3"
         * @endcode
         */
        template <typename EigenVectorType>
        PropertyDefinition<EigenVectorType>& defineOptionalPropertyVector(
            const std::string& name,
            EigenVectorType defaultValue,
            const std::string& description = "",
            char delimiter = ' ',
            PropertyDefinitionBase::PropertyConstness constness = PropertyDefinitionBase::eConstant);


        PropertyDefinitionBase* getDefinitionBase(const std::string& name);

        template <typename PropertyType>
        PropertyDefinition<PropertyType>& getDefintion(const std::string& name);
        bool hasDefinition(const std::string& name)
        {
            return (definitions.find(name) != definitions.end());
        }

        std::string toString(PropertyDefinitionFormatter& formatter);

        /**
         * Returns the detailed description of the property user
         *
         * @return detailed description text
         */
        std::string getDescription() const;

        /**
         * Sets the detailed description of the property user
         *
         * @param description detailed description text
         */
        void setDescription(const std::string& description);

        void setPrefix(std::string prefix);

        std::string getPrefix();

        bool isPropertySet(const std::string& name);

        std::string getValue(const std::string& name);

        Ice::PropertyDict getPropertyValues(const std::string& prefix = "") const;


        template <typename EigenVectorType>
        static EigenVectorType eigenVectorFactoryFunction(std::string string, char delim);
        template <typename EigenVectorType>
        static std::string eigenVectorToString(const EigenVectorType& vector, char delim);

        static std::string eigenVectorPropertyDescription(
                const std::string& description, long size, char delim);


    protected:
        /**
         * Property definitions container
         */
        DefinitionContainer definitions;

        /**
         * Prefix of the properties such as namespace, domain, component name,
         * etc.
         */
        std::string prefix;

        /**
         * Property User description
         */
        std::string description;

        /**
         * PropertyUser brief description
         */
        std::string briefDescription;

        Ice::PropertiesPtr properties;
    };


    /**
     * PropertyDefinitions smart pointer type
     */
    typedef IceUtil::Handle<PropertyDefinitionContainer> PropertyDefinitionsPtr;

    /* ====================================================================== */
    /* === Property Definition Container Implementation ===================== */
    /* ====================================================================== */

    template <typename PropertyType>
    PropertyDefinition<PropertyType>&
    PropertyDefinitionContainer::defineRequiredProperty(const std::string& name,
            const std::string& description,
            PropertyDefinitionBase::PropertyConstness constness)
    {
        PropertyDefinition<PropertyType>* def =
            new PropertyDefinition<PropertyType>(name, description, constness);

        def->setTypeIdName(typeid(PropertyType).name());

        definitions[name] = static_cast<PropertyDefinitionBase*>(def);

        return *def;
    }


    template <typename PropertyType>
    PropertyDefinition<PropertyType>&
    PropertyDefinitionContainer::defineOptionalProperty(
        const std::string& name,
        PropertyType defaultValue,
        const std::string& description,
        PropertyDefinitionBase::PropertyConstness constness)
    {
        PropertyDefinition<PropertyType>* def =
            new PropertyDefinition<PropertyType>(name,
                    defaultValue,
                    description,
                    constness);

        def->setTypeIdName(typeid(PropertyType).name());

        definitions[name] = static_cast<PropertyDefinitionBase*>(def);

        return *def;
    }


    template <typename EigenVectorType>
    PropertyDefinition<EigenVectorType>&
    PropertyDefinitionContainer::defineRequiredPropertyVector(
        const std::string& name,
        const std::string& description,
        char delimiter,
        PropertyDefinitionBase::PropertyConstness constness)
    {
        std::string appendedDescription =
            eigenVectorPropertyDescription(description, EigenVectorType::SizeAtCompileTime, 
                                           delimiter);

        PropertyDefinition<EigenVectorType>& def = defineRequiredProperty<EigenVectorType>(
                    name, appendedDescription, constness);

        // set a factory
        def.setFactory([delimiter](std::string string) -> EigenVectorType
        {
            return eigenVectorFactoryFunction<EigenVectorType>(string, delimiter);
        });
        def.setCaseInsensitive(true);

        return def;
    }

    template<typename EigenVectorType>
    PropertyDefinition<EigenVectorType>&
    PropertyDefinitionContainer::defineOptionalPropertyVector(
        const std::string& name,
        EigenVectorType defaultValue,
        const std::string& description,
        char delimiter,
        PropertyDefinitionBase::PropertyConstness constness)
    {
        std::string appendedDescription =
            eigenVectorPropertyDescription(description, EigenVectorType::SizeAtCompileTime, 
                                           delimiter);

        PropertyDefinition<EigenVectorType>& def = defineOptionalProperty<EigenVectorType>(
                    name, defaultValue, appendedDescription, constness);

        // set a factory
        def.setFactory([delimiter](std::string string) -> EigenVectorType
        {
            return eigenVectorFactoryFunction<EigenVectorType>(string, delimiter);
        });
        // map the default value
        def.map(eigenVectorToString<EigenVectorType>(defaultValue, delimiter), defaultValue);
        def.setCaseInsensitive(true);

        return def;
    }




    template <typename PropertyType>
    PropertyDefinition<PropertyType>&
    PropertyDefinitionContainer::getDefintion(const std::string& name)
    {
        // check definition existance
        if (definitions.find(name) == definitions.end())
        {
            std::stringstream ss;
            ss << name + " property is not defined. Defined are ";
            for (typename DefinitionContainer::iterator it = definitions.begin(); it != definitions.end(); ++it)
            {
                ss << "\n" << it->first;
            }
            throw armarx::LocalException(ss.str());
        }

        PropertyDefinitionBase* def = definitions[name];

        // check definition type
        if (def->getTypeIdName().compare(typeid(PropertyType).name()) != 0)
        {
            throw armarx::LocalException(
                std::string("Calling getProperty<T>() for the property '")
                + name
                + "' with wrong property type [note: "
                + typeid(PropertyType).name()
                + " instead of "
                + def->getTypeIdName()
                + "]");
        }

        // retrieve and down cast the requested definition
        return *dynamic_cast< PropertyDefinition<PropertyType>* >(def);
    }


    template <>
    PropertyDefinition<bool>&
    PropertyDefinitionContainer::defineOptionalProperty(
        const std::string& name,
        bool defaultValue,
        const std::string& description,
        PropertyDefinitionBase::PropertyConstness constness);
    template <>
    PropertyDefinition<bool>&
    PropertyDefinitionContainer::defineRequiredProperty(const std::string& name,
            const std::string& description,
            PropertyDefinitionBase::PropertyConstness constness);

    /**
     * Parses strings like "key1:value1,key2:value2" to a string-string map. Escaping of , and : is *not* supported. Keys and values are trimmed of whitespaces.
     */
    template <>
    PropertyDefinition<std::map<std::string, std::string> >&
    PropertyDefinitionContainer::defineOptionalProperty(
        const std::string& name,
        std::map<std::string, std::string> defaultValue,
        const std::string& description,
        PropertyDefinitionBase::PropertyConstness constness);
    /**
     * Parses strings like "key1:value1,key2:value2" to a string-string map. Escaping of , and : is *not* supported. Keys and values are trimmed of whitespaces.
     */
    template <>
    PropertyDefinition<std::map<std::string, std::string> >&
    PropertyDefinitionContainer::defineRequiredProperty(const std::string& name,
            const std::string& description,
            PropertyDefinitionBase::PropertyConstness constness);

    /**
     * Parses strings like "value1,value2" to a string-vector. Commas in double-quoted strings are ignored. The values are trimmed of whitespaces.
     */
    template <>
    PropertyDefinition<Ice::StringSeq>&
    PropertyDefinitionContainer::defineOptionalProperty(
        const std::string& name,
        Ice::StringSeq defaultValue,
        const std::string& description,
        PropertyDefinitionBase::PropertyConstness constness);
    /**
     * Parses strings like "value1,value2" to a string-vector. Commas in double-quoted strings are ignored. The values are trimmed of whitespaces.
     */
    template <>
    PropertyDefinition<Ice::StringSeq>&
    PropertyDefinitionContainer::defineRequiredProperty(const std::string& name,
            const std::string& description,
            PropertyDefinitionBase::PropertyConstness constness);



    template<typename EigenVectorType>
    EigenVectorType PropertyDefinitionContainer::eigenVectorFactoryFunction(
            std::string string, char delim)
    {
        using Scalar = typename EigenVectorType::Scalar;

        long size = EigenVectorType::SizeAtCompileTime;
        bool isFixedSize = size >= 0; // Eigen::Dynamic is -1.

        std::vector<Scalar> scalars;
        if (isFixedSize)
        {
            scalars.reserve(size);
        }

        std::stringstream ss(string);
        for (std::string valueStr; std::getline(ss, valueStr, delim);)
        {
            // may throw boost::bad_lexical_cast (derives from std::bad_cast)
            Scalar value = boost::lexical_cast<Scalar>(valueStr);
            scalars.push_back(value);
        }

        EigenVectorType vector;
        if (!isFixedSize)
        {
            vector.resize(scalars.size());
        }
        // write values
        for (std::size_t i = 0; i < scalars.size(); ++i)
        {
            vector(i) = scalars[i];
        }
        return vector;
    }

    template<typename EigenVectorType>
    std::string PropertyDefinitionContainer::eigenVectorToString(
            const EigenVectorType& vector, char delim)
    {
        std::stringstream ss;
        long size = vector.size();

        ss << vector(0);
        for (int i = 1; i < size; ++i)
        {
            ss << delim << vector(i);
        }

        return ss.str();
    }


}

