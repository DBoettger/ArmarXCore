/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2015
* @copyright  http://www.gnu.org/licenses/gpl.txt
*             GNU General Public License
*/


#include "PropertyDefinition.hpp"

namespace armarx
{



    template class PropertyDefinition<int>;
    template class PropertyDefinition<float>;
    template class PropertyDefinition<double>;
    template class PropertyDefinition<std::string>;
    template class PropertyDefinition<bool>;



}

namespace std
{
    template class map<string, float>;


    template class map<string, pair<string, int>>;
    template class map<string, pair<string, float>>;
    template class map<string, pair<string, double>>;
    template class map<string, pair<string, string>>;
    template class map<string, pair<string, bool>>;

}
