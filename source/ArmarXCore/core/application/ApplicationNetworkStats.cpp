/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Manfred Kroehnert (manfred dot kroehnert at kit dot edu)
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "ApplicationNetworkStats.h"
#include "../exceptions/local/ExpressionException.h"
#include "../services/tasks/TaskUtil.h"

#include <atomic>
#include <Ice/Connection.h>
#include <Ice/Endpoint.h>
#include <IceSSL/EndpointInfo.h>

namespace armarx
{
    namespace detail
    {
        struct ArmarXConnectionObserver : public Ice::Instrumentation::ConnectionObserver
        {
            ArmarXConnectionObserver(std::string protocol): protocol {std::move(protocol)} {}
            void attach() override {}
            void detach() override {}
            void failed(const std::string&) override {}
            void sentBytes(int num) override
            {
                sent += num;
            }
            void receivedBytes(int num) override
            {
                received += num;
            }

            void report(ProfilerListenerPrx& profiler, const std::string& applicationName)
            {
                profiler->reportNetworkTraffic(
                    applicationName,
                    protocol,
                    received.exchange(0),
                    sent.exchange(0));
            }
            const std::string protocol;
            std::atomic<Ice::Int> sent {0};
            std::atomic<Ice::Int> received {0};
        };
    }
}

namespace armarx
{
    ApplicationNetworkStats::ApplicationNetworkStats()
    {
        observerTCP = new detail::ArmarXConnectionObserver {"tcp"};
        observerUDP = new detail::ArmarXConnectionObserver {"udp"};
        observerSSL = new detail::ArmarXConnectionObserver {"ssl"};
        observerUnknown = new detail::ArmarXConnectionObserver {"Unknown"};
    }

    void ApplicationNetworkStats::start(ProfilerListenerPrx profiler, const std::string& applicationName)
    {
        ARMARX_CHECK_EXPRESSION(!applicationName.empty());
        ARMARX_CHECK_EXPRESSION(profiler);
        reportNetworkTrafficTask = new SimplePeriodicTask<>
        {
            [this, profiler, applicationName]() mutable
            {
                observerTCP->report(profiler, applicationName);
                observerUDP->report(profiler, applicationName);
                observerSSL->report(profiler, applicationName);
                observerUnknown->report(profiler, applicationName);
            },
            REPORT_TIME_MS
        };
        reportNetworkTrafficTask->start();
    }

    void ApplicationNetworkStats::stopTask()
    {
        reportNetworkTrafficTask->stop();
    }

    ApplicationNetworkStats::~ApplicationNetworkStats()
    {
        stopTask();
    }
    Ice::Instrumentation::ConnectionObserverPtr ApplicationNetworkStats::getConnectionObserver(
        const Ice::ConnectionInfoPtr& /*c*/,
        const Ice::EndpointPtr& e,
        Ice::Instrumentation::ConnectionState /*s*/,
        const Ice::Instrumentation::ConnectionObserverPtr& /*previous*/)
    {
        const auto protocol = e->getInfo()->type();
        switch (protocol)
        {
            case Ice::TCPEndpointType:
                return observerTCP;
            case Ice::UDPEndpointType:
                return observerUDP;
            case IceSSL::EndpointType:
                return observerSSL;
            default:
                ARMARX_WARNING << deactivateSpam(10, to_string(protocol)) << "Unknown protocol with type " << protocol;
                return observerUnknown;
        }
    }

    Ice::Instrumentation::ObserverPtr ApplicationNetworkStats::getConnectionEstablishmentObserver(const Ice::EndpointPtr&, const std::string&)
    {
        return {};
    }

    Ice::Instrumentation::ObserverPtr ApplicationNetworkStats::getEndpointLookupObserver(const Ice::EndpointPtr&)
    {
        return {};
    }

    Ice::Instrumentation::ThreadObserverPtr ApplicationNetworkStats::getThreadObserver(const std::string&, const std::string&, Ice::Instrumentation::ThreadState, const Ice::Instrumentation::ThreadObserverPtr&)
    {
        return {};
    }

    Ice::Instrumentation::InvocationObserverPtr ApplicationNetworkStats::getInvocationObserver(const Ice::ObjectPrx&, const std::string&, const Ice::Context&)
    {
        return {};
    }

    Ice::Instrumentation::DispatchObserverPtr ApplicationNetworkStats::getDispatchObserver(const Ice::Current&, Ice::Int)
    {
        return {};
    }

    void ApplicationNetworkStats::setObserverUpdater(const Ice::Instrumentation::ObserverUpdaterPtr& upd)
    {
    }
}
