#include "UnexpectedEnumValueException.h"


namespace armarx
{
    namespace exceptions
    {
        namespace local
        {

            UnexpectedEnumValueException::UnexpectedEnumValueException(
                const std::string& enumName, int value) :
                armarx::LocalException("Unexpected value of enum " + enumName + ": "
                                       + std::to_string(value)),
                enumName(enumName), value(value)
            {}

            const std::string& UnexpectedEnumValueException::getEnumName() const
            {
                return enumName;
            }

            int UnexpectedEnumValueException::getValue() const
            {
                return value;
            }

        }
    }
}

