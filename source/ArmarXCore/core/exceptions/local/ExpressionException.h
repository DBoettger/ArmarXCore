/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2013
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include "../Exception.h"
#include <ArmarXCore/core/logging/Logging.h>
#include <string>

namespace armarx
{
    namespace exceptions
    {
        namespace local
        {
            /**
              \class ExpressionException
              \brief This exception is thrown if the macro \ref ARMARX_CHECK_EXPRESSION
              is used.
              \ingroup Exceptions
             */
            class ExpressionException : public armarx::LocalException
            {
            public:
                ExpressionException(std::string expression) noexcept :
                    armarx::LocalException("The following expression must be true, but is not: '" + expression + "'"),
                    expression(expression)
                {}
                
                virtual ~ExpressionException() noexcept override = default;

                virtual std::string name() const override
                {
                    return "armarx::exceptions::local::ExpressionException";
                }
                
                std::string getExpression() const
                {
                    return expression;
                }
                
            private:
                std::string expression;
            };
            
        }
    }
}

/**
\ingroup Exceptions
\brief This macro evaluates the expression and if it turns out to be
false it will throw an ExpressionException with the expression converted
into a string.
  **/
#define ARMARX_CHECK_EXPRESSION(expression) do { \
        if( !(expression) ) {\
            ::std::stringstream s; \
            s << #expression <<"\n(line " << __LINE__ << " in " << __FUNCTION__ << " in " << __FILE__ ")"; \
            throw ::armarx::exceptions::local::ExpressionException(s.str());  \
        }\
    } while(0);


/**
\ingroup Exceptions
\brief Shortcut for ARMARX_CHECK_EXPRESSION
  **/
#define ARMARX_CHECK(expression) ARMARX_CHECK_EXPRESSION(expression)

/**
\ingroup Exceptions
\brief Shortcut for ARMARX_CHECK_EXPRESSION_W_HINT
  **/
#define ARMARX_CHECK_W_HINT(expression, hint) ARMARX_CHECK_EXPRESSION_W_HINT(expression, hint)


/**
\ingroup Exceptions
\brief This macro evaluates the expression and if it turns out to be
false it will throw an ExpressionException with the expression converted
into a string and a hint for the user.
  **/
#define ARMARX_CHECK_EXPRESSION_W_HINT(expression, hint) do { \
        if( !(expression) ) \
            throw ::armarx::exceptions::local::ExpressionException(#expression) << " Error hint: " << hint \
                    << "\n(line " << __LINE__ << " in " << __FUNCTION__ << " in " << __FILE__ ")"; \
    } while(0)


/**
\ingroup Exceptions
\brief This macro evaluates the expression (lhs cmp rhs) and if it turns out to be
false it will throw an ExpressionException with the expression converted
into a string.
  **/
#define ARMARX_CHECK_COMPARISON(lhs, rhs, cmp)                                                          \
    do {                                                                                                \
        if( !(lhs cmp rhs) )                                                                            \
        {                                                                                               \
            throw ::armarx::exceptions::local::ExpressionException(#lhs " " #cmp " " #rhs)                \
                    << "\n(lhs = " << lhs << ", rhs = " << rhs << ")"                                   \
                    << "\n(line " << __LINE__ << " in " << __FUNCTION__ << " in " << __FILE__ ")";      \
        }                                                                                               \
    } while(0)

/**
\ingroup Exceptions
\brief This macro evaluates the expression (lhs cmp rhs) and if it turns out to be
false it will throw an ExpressionException with the expression converted
into a string and a hint for the user.
  **/
#define ARMARX_CHECK_COMPARISON_W_HINT(lhs, rhs, cmp, hint)                                             \
    do {                                                                                                \
        if( !(lhs cmp rhs) )                                                                            \
        {                                                                                               \
            throw ::armarx::exceptions::local::ExpressionException(#lhs " " #cmp " " #rhs)                \
                    << "\n(lhs = " << lhs << ", rhs = " << rhs << ")"                                   \
                    << "\n Error hint: " << hint                                                        \
                    << "\n(line " << __LINE__ << " in " << __FUNCTION__ << " in " << __FILE__ ")";      \
        }                                                                                               \
    } while(0)

/**
\ingroup Exceptions
\brief This macro evaluates whether lhs is less (<) than rhs and if it turns out to be
false it will throw an ExpressionException with the expression converted
into a string.
  **/
#define ARMARX_CHECK_LESS(lhs, rhs)          ARMARX_CHECK_COMPARISON(lhs, rhs, <)
/**
\ingroup Exceptions
\brief This macro evaluates whether lhs is less or equal (<=) rhs and if it turns out to be
false it will throw an ExpressionException with the expression converted
into a string.
  **/
#define ARMARX_CHECK_LESS_EQUAL(lhs, rhs)    ARMARX_CHECK_COMPARISON(lhs, rhs, <=)
/**
\ingroup Exceptions
\brief This macro evaluates whether lhs is greater (>) than rhs and if it turns out to be
false it will throw an ExpressionException with the expression converted
into a string.
  **/
#define ARMARX_CHECK_GREATER(lhs, rhs)       ARMARX_CHECK_COMPARISON(lhs, rhs, >)
/**
\ingroup Exceptions
\brief This macro evaluates whether lhs is greater or equal (>=) rhs and if it turns out to be
false it will throw an ExpressionException with the expression converted
into a string.
  **/
#define ARMARX_CHECK_GREATER_EQUAL(lhs, rhs) ARMARX_CHECK_COMPARISON(lhs, rhs, >=)
/**
\ingroup Exceptions
\brief This macro evaluates whether lhs is equal (==) rhs and if it turns out to be
false it will throw an ExpressionException with the expression converted
into a string.
  **/
#define ARMARX_CHECK_EQUAL(lhs, rhs)         ARMARX_CHECK_COMPARISON(lhs, rhs, ==)
/**
\ingroup Exceptions
\brief This macro evaluates whether lhs is inequal (!=) rhs and if it turns out to be
false it will throw an ExpressionException with the expression converted
into a string.
  **/
#define ARMARX_CHECK_NOT_EQUAL(lhs, rhs)     ARMARX_CHECK_COMPARISON(lhs, rhs, !=)

/**
\ingroup Exceptions
\brief This macro evaluates whether lhs is less (<) than rhs and if it turns out to be
false it will throw an ExpressionException with the expression converted
into a string and a hint for the user.
  **/
#define ARMARX_CHECK_LESS_W_HINT(lhs, rhs, hint)          ARMARX_CHECK_COMPARISON_W_HINT(lhs, rhs, < , hint)
/**
\ingroup Exceptions
\brief This macro evaluates whether lhs is less or equal (<=) rhs and if it turns out to be
false it will throw an ExpressionException with the expression converted
into a string and a hint for the user.
  **/
#define ARMARX_CHECK_LESS_EQUAL_W_HINT(lhs, rhs, hint)    ARMARX_CHECK_COMPARISON_W_HINT(lhs, rhs, <=, hint)
/**
\ingroup Exceptions
\brief This macro evaluates whether lhs is greater (>) than rhs and if it turns out to be
false it will throw an ExpressionException with the expression converted
into a string and a hint for the user.
  **/
#define ARMARX_CHECK_GREATER_W_HINT(lhs, rhs, hint)       ARMARX_CHECK_COMPARISON_W_HINT(lhs, rhs, > , hint)
/**
\ingroup Exceptions
\brief This macro evaluates whether lhs is greater or equal (>=) rhs and if it turns out to be
false it will throw an ExpressionException with the expression converted
into a string and a hint for the user.
  **/
#define ARMARX_CHECK_GREATER_EQUAL_W_HINT(lhs, rhs, hint) ARMARX_CHECK_COMPARISON_W_HINT(lhs, rhs, >=, hint)
/**
\ingroup Exceptions
\brief This macro evaluates whether lhs is equal (==) rhs and if it turns out to be
false it will throw an ExpressionException with the expression converted
into a string and a hint for the user.
  **/
#define ARMARX_CHECK_EQUAL_W_HINT(lhs, rhs, hint)         ARMARX_CHECK_COMPARISON_W_HINT(lhs, rhs, ==, hint)
/**
\ingroup Exceptions
\brief This macro evaluates whether lhs is inequal (!=) rhs and if it turns out to be
false it will throw an ExpressionException with the expression converted
into a string and a hint for the user.
  **/
#define ARMARX_CHECK_NOT_EQUAL_W_HINT(lhs, rhs, hint)     ARMARX_CHECK_COMPARISON_W_HINT(lhs, rhs, !=, hint)


/**
\ingroup Exceptions
\brief This macro evaluates whether number is positive (> 0) and if it turns out to be
false it will throw an ExpressionException with the expression converted
into a string.
  **/
#define ARMARX_CHECK_POSITIVE(number)                   ARMARX_CHECK_GREATER(number, 0)

/**
\ingroup Exceptions
\brief This macro evaluates whether number is positive (> 0) and if it turns out to be
false it will throw an ExpressionException with the expression converted
into a string and a hint for the user.
  **/
#define ARMARX_CHECK_POSITIVE_W_HINT(number, hint)      ARMARX_CHECK_GREATER_W_HINT(number, 0, hint)


/**
\ingroup Exceptions
\brief Check whether number is nonnegative (>= 0). If it is not, throw an
ExpressionException with the expression converted into a string.
  **/
#define ARMARX_CHECK_NONNEGATIVE(number)                ARMARX_CHECK_GREATER_EQUAL(number, 0)

/**
\ingroup Exceptions
\brief Check whether number is nonnegative (>= 0). If it is not, throw an
ExpressionException with the expression converted into a string and a hint for the user.
  **/
#define ARMARX_CHECK_NONNEGATIVE_W_HINT(number, hint)   ARMARX_CHECK_GREATER_EQUAL_W_HINT(number, 0, hint)



/**
\ingroup Exceptions
\brief Check whether number is nonnegative (>= 0) and less than size. If it is not,
throw an ExpressionException with the expression converted into a string.
  **/
#define ARMARX_CHECK_FITS_SIZE(number, size)    \
    ARMARX_CHECK_NONNEGATIVE(number);           \
    ARMARX_CHECK_LESS(number, size)

/**
\ingroup Exceptions
\brief Check whether number is nonnegative (>= 0) and less than size. If it is not,
throw an ExpressionException with the expression converted into a string and a hint for the user.
  **/
#define ARMARX_CHECK_FITS_SIZE_W_HINT(number, size)    \
    ARMARX_CHECK_NONNEGATIVE_W_HINT(number);           \
    ARMARX_CHECK_LESS_W_HINT(number, size)


/**
\ingroup Exceptions
\brief This macro evaluates whether number is finite (not nan or inf) and if it turns out to be
false it will throw an ExpressionException with the expression converted
into a string.
  **/
#define ARMARX_CHECK_FINITE(number)                                                                     \
    do {                                                                                                \
        if( !(std::isfinite(number)) )                                                                 \
        {                                                                                               \
            throw ::armarx::exceptions::local::ExpressionException("std::isfinite(" #number ")")         \
                    << "\n(number = " << number << ")"                                                  \
                    << "\n(line " << __LINE__ << " in " << __FUNCTION__ << " in " << __FILE__ ")";      \
        }                                                                                               \
    } while(0)

/**
\ingroup Exceptions
\brief This macro evaluates whether number is finite (not nan or inf) and if it turns out to be
false it will throw an ExpressionException with the expression converted
into a string and a hint for the user.
  **/
#define ARMARX_CHECK_FINITE_W_HINT(number, hint)                                                        \
    do {                                                                                                \
        if( !(std::isfinite(number)) )                                                                 \
        {                                                                                               \
            throw ::armarx::exceptions::local::ExpressionException("std::isfinite(" #number ")")         \
                    << "\n(number = " << number << ")"                                                  \
                    << "\n Error hint: " << hint                                                        \
                    << "\n(line " << __LINE__ << " in " << __FUNCTION__ << " in " << __FILE__ ")";      \
        }                                                                                               \
    } while(0)

/**
\ingroup Exceptions
\brief This macro evaluates whether ptr is null and if it turns out to be
false it will throw an ExpressionException with the expression converted
into a string.
  **/
#define ARMARX_CHECK_IS_NULL(ptr)                                                                       \
    do {                                                                                                \
        if( !(ptr == nullptr) )                                                                         \
        {                                                                                               \
            throw ::armarx::exceptions::local::ExpressionException("ptr == nullptr")                      \
                    << "\n(ptr = " << ptr << ")"                                                        \
                    << "\n(line " << __LINE__ << " in " << __FUNCTION__ << " in " << __FILE__ ")";      \
        }                                                                                               \
    } while(0)

/**
\ingroup Exceptions
\brief This macro evaluates whether ptr is null and if it turns out to be
false it will throw an ExpressionException with the expression converted
into a string and a hint for the user.
  **/
#define ARMARX_CHECK_IS_NULL_W_HINT(ptr, hint)                                                          \
    do {                                                                                                \
        if( !(ptr == nullptr) )                                                                         \
        {                                                                                               \
            throw ::armarx::exceptions::local::ExpressionException("ptr == nullptr")                      \
                    << "\n(ptr = " << ptr << ")"                                                        \
                    << "\n Error hint: " << hint                                                        \
                    << "\n(line " << __LINE__ << " in " << __FUNCTION__ << " in " << __FILE__ ")";      \
        }                                                                                               \
    } while(0)

/**
\ingroup Exceptions
\brief This macro evaluates whether ptr is not null and if it turns out to be
false it will throw an ExpressionException with the expression converted
into a string.
  **/
#define ARMARX_CHECK_NOT_NULL(ptr)                                                                      \
    do {                                                                                                \
        if( !(ptr != 0) )                                                                         \
        {                                                                                               \
            throw ::armarx::exceptions::local::ExpressionException("ptr != nullptr")                      \
                    << "\n(ptr = nullptr)"                                                              \
                    << "\n(line " << __LINE__ << " in " << __FUNCTION__ << " in " << __FILE__ ")";      \
        }                                                                                               \
    } while(0)

/**
\ingroup Exceptions
\brief This macro evaluates whether ptr is not null and if it turns out to be
false it will throw an ExpressionException with the expression converted
into a string and a hint for the user.
  **/
#define ARMARX_CHECK_NOT_NULL_W_HINT(ptr, hint)                                                         \
    do {                                                                                                \
        if( !(ptr != 0) )                                                                         \
        {                                                                                               \
            throw ::armarx::exceptions::local::ExpressionException("ptr != nullptr")                      \
                    << "\n(ptr = nullptr)"                                                              \
                    << "\n Error hint: " << hint                                                        \
                    << "\n(line " << __LINE__ << " in " << __FUNCTION__ << " in " << __FILE__ ")";      \
        }                                                                                               \
    } while(0)

/**
\ingroup Exceptions
\brief This macro evaluates the expression and if it turns out to be
false it will throw an exception of the given type.
  **/
#define ARMARX_CHECK_AND_THROW(expression, ExceptionType) do { \
        if( !(expression) ) \
            throw ExceptionType(#expression); \
    } while(0);

#undef eigen_assert
#define eigen_assert(expression) ARMARX_CHECK_EXPRESSION(expression)


#ifdef NDEBUG
/**
\ingroup Exceptions
\see ARMARX_CHECK_EXPRESSION
\brief Does the same as ARMARX_CHECK_EXPRESSION in debug mode, does nothing in release mode.
  **/
#define ARMARX_CHECK_EXPRESSION_DEBUG_ONLY(expression) assert(true)
/**
\ingroup Exceptions
\see ARMARX_CHECK_AND_THROW
\brief Does the same as ARMARX_CHECK_AND_THROW in debug mode, does nothing in release mode.
  **/
#define ARMARX_CHECK_AND_THROW_DEBUG_ONLY(expression, ExceptionType) assert(true)
/**
\ingroup Exceptions
\see ARMARX_CHECK_EXPRESSION_W_HINT
\brief Does the same as ARMARX_CHECK_EXPRESSION_W_HINT in debug mode, does nothing in release mode.
  **/
#define ARMARX_CHECK_EXPRESSION_W_HINT_DEBUG_ONLY(expression, hint) assert(true)
#else
/**
\ingroup Exceptions
\see ARMARX_CHECK_EXPRESSION
\brief Does the same as ARMARX_CHECK_EXPRESSION in debug mode, does nothing in release mode.
  **/
#define ARMARX_CHECK_EXPRESSION_DEBUG_ONLY(expression) ARMARX_CHECK_EXPRESSION(expression)
/**
\ingroup Exceptions
\see ARMARX_CHECK_AND_THROW
\brief Does the same as ARMARX_CHECK_AND_THROW in debug mode, does nothing in release mode.
  **/
#define ARMARX_CHECK_AND_THROW_DEBUG_ONLY(expression, ExceptionType) ARMARX_CHECK_AND_THROW(expression, ExceptionType)
/**
\ingroup Exceptions
\see ARMARX_CHECK_EXPRESSION_W_HINT
\brief Does the same as ARMARX_CHECK_EXPRESSION_W_HINT in debug mode, does nothing in release mode.
  **/
#define ARMARX_CHECK_EXPRESSION_W_HINT_DEBUG_ONLY(expression, hint) ARMARX_CHECK_EXPRESSION_W_HINT(expression, hint)
#endif

