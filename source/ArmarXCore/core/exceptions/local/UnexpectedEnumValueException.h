#pragma once


#include "../Exception.h"


namespace armarx
{
    namespace exceptions
    {
        namespace local
        {

            /**
             * @brief Indicates that an unexpected value of an enum was encountered.
             */
            class UnexpectedEnumValueException : public armarx::LocalException
            {
            public:

                /**
                 * @brief Constructor.
                 * @param enumName the name of the enum type
                 * @param value    the encountered value (cast to int if needed)
                 */
                UnexpectedEnumValueException(const std::string& enumName, int value);

                /**
                 * @brief Constructor.
                 * @param enumName the name of the enum type
                 * @param value    the encountered value (is cast to int)
                 */
                template <typename EnumT>
                UnexpectedEnumValueException(const std::string& enumName, EnumT value) :
                    UnexpectedEnumValueException(enumName, static_cast<int>(value))
                {}

                /// Virtual destructor.
                virtual ~UnexpectedEnumValueException() override = default;


                /// Get the name of the enum type.
                const std::string& getEnumName() const;

                /// Get the encountered enum value.
                int getValue() const;


            private:

                /// The name of the enum type.
                std::string enumName;

                /// The encountered value (cast to int).
                int value;

            };

        }
    }
}


/**
 * Throw an UnexpectedEnumValueException.
 * Pass EnumType as symbol (it will be converted to a string).
 */
#define ARMARX_UNEXPECTED_ENUM_VALUE(EnumType, value) \
    throw ::armarx::exceptions::local::UnexpectedEnumValueException(#EnumType, (value));
