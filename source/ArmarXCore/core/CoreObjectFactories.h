/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Core
* @author     Kai Welke (welke _at_ kit _dot_ edu)
* @date       2011
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <Ice/ObjectFactory.h>
#include <ArmarXCore/core/system/FactoryCollectionBase.h>

#include <ArmarXCore/interface/core/ManagedIceObjectDependencyBase.h>
#include <ArmarXCore/core/ManagedIceObjectDependency.h>
#include <ArmarXCore/core/system/FactoryCollectionBase.h>
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <ArmarXCore/core/util/distributed/RemoteHandle/ClientSideRemoteHandleControlBlock.h>

namespace armarx
{
    /**
      \class ProxyDependencyFactory
      \brief
      \ingroup DistributedProcessingSub
     */
    class ProxyDependencyFactory : public Ice::ObjectFactory
    {
    public:
        Ice::ObjectPtr create(const std::string& type) override
        {
            assert(type == armarx::ManagedIceObjectDependencyBase::ice_staticId());
            return new ProxyDependency();
        }
        void destroy() override {}
    };

    namespace ObjectFactories
    {
        /**
        \class CoreObjectFactories
        \brief
        \ingroup DistributedProcessingSub
        */
        class CoreObjectFactories : public FactoryCollectionBase
        {
        public:
            ObjectFactoryMap getFactories() override
            {
                ObjectFactoryMap map;

                map.insert(std::make_pair(ManagedIceObjectDependencyBase::ice_staticId(), new ProxyDependencyFactory));
                add<ClientSideRemoteHandleControlBlockBase, ClientSideRemoteHandleControlBlock>(map);
                // communicator->addObjectFactory\(new ([a-zA-Z0-9]*), ([:a-zA-Z0-9]*)::ice_staticId\(\)\)
                // map.insert(std::make_pair( \2::ice_staticId(), new \1));
                return map;
            }
        };
        const FactoryCollectionBaseCleanUp CoreObjectFactoriesVar = FactoryCollectionBase::addToPreregistration(new CoreObjectFactories());

    }
}

