/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Nils Adermann (naderman at naderman dot de)
 * @author     Jan Issac (jan dot issac at gmx dot de)
 * @date       2010
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <ArmarXCore/core/logging/Logging.h>  // for Logging
#include <ArmarXCore/core/system/ImportExport.h>
#include <Ice/Communicator.h>           // for CommunicatorPtr, etc
#include <Ice/Identity.h>               // for Identity
#include <Ice/LocalException.h>
#include <Ice/ObjectAdapter.h>          // for ObjectAdapterPtr, upCast
#include <Ice/ObjectF.h>                // for ObjectPtr
#include <Ice/Proxy.h>                  // for Object

#include <IceUtil/Handle.h>             // for Handle, HandleBase
#include <IceUtil/Shared.h>             // for Shared
#include <boost/functional/hash/hash.hpp>  // for hash
#include <boost/thread/pthread/mutex.hpp>  // for mutex, etc
#include <boost/unordered/unordered_map.hpp>  // for unordered_map
#include <stddef.h>                     // for NULL
#include <algorithm>                    // for max
#include <functional>                   // for equal_to
#include <list>                         // for list
#include <map>                          // for map
#include <ostream>                      // for basic_ostream, operator<<, etc
#include <stdexcept>                    // for out_of_range
#include <string>                       // for string, allocator, etc
#include <typeinfo>                     // for type_info
#include <utility>                      // for pair
#include <vector>                       // for vector

#include "ArmarXCore/interface/core/UserException.h"  // for UserException

namespace Ice
{
    class ConnectionRefusedException;

}  // namespace Ice

namespace IceProxy
{
    namespace IceGrid
    {
        class Registry;
    }
    namespace IceStorm
    {
        class TopicManager;
        class Topic;
    }
}

namespace IceStorm
{
    typedef ::IceInternal::ProxyHandle< ::IceProxy::IceStorm::TopicManager> TopicManagerPrx;
    typedef ::IceInternal::ProxyHandle< ::IceProxy::IceStorm::Topic> TopicPrx;
}

namespace IceGrid
{
    typedef ::IceInternal::ProxyHandle< ::IceProxy::IceGrid::Registry> RegistryPrx;
}

/**
 * \namespace armarx
 * The standard namespace for all classes and components within the ArmarXCore package
 */
namespace armarx
{
    class IceGridAdmin;
    typedef IceUtil::Handle<IceGridAdmin> IceGridAdminPtr;

    class IceManager;

    /**
     * IceManager smart pointer
     */
    typedef IceUtil::Handle<IceManager> IceManagerPtr;

    /**
     * Object handles pair which contains the object proxy and its adapter
     */
    typedef std::pair<Ice::ObjectPrx, Ice::ObjectAdapterPtr> ObjectHandles;


    /**
     * @class IceManager
     * @brief The IceManager class provides simplified access to commonly used Ice features.
     * @ingroup DistributedProcessingSub
     * @see Logging
     */
    class ARMARXCORE_IMPORT_EXPORT IceManager:
        public IceUtil::Shared,
        virtual public Logging
    {
        friend class Component;

    public:
        /**
         * Set up an instance of this class with a preexisting communicator.
         *
         * This is useful for creating an instance from within an
         * Ice::Application. If you do not have a communicator yet you can
         * use one of the static factory methods instead.
         *
         * @param communicator   An Ice communicator
         *
         * @param name           A unique name, e.g. the base component's
         *                       name. This name is used to create
         *                       IceGridObservers, which must have a unique
         *                       name.
         * @param topicSuffix    Suffix appended to all topic names.
         */
        IceManager(const Ice::CommunicatorPtr& communicator,
                   std::string name = "", const std::string topicSuffix = "");

        ~IceManager() override;

        /**
         * Register an object with Ice for being accessed through IceGrid.
         *
         * @param object     The object to be registered, implementing an
         *                   Ice interface.
         *
         * @param objectName The name this object should be available as.
         *
         * @return The registered object proxy.
         */
        ObjectHandles registerObject(const Ice::ObjectPtr& object,
                                     const std::string& objectName, const Ice::ObjectAdapterPtr& adapterToAddTo = nullptr);

        /**
         * Removes an object from the IceGrid
         *
         * @param objectName The name of the object that should be removed
         */
        void removeObject(const std::string& objectName);

        /**
         * Retrieves a proxy object.
         *
         * @param  name       Proxy name, e.g. Log
         * @param  endpoints  The endpoints, e.g. tcp ‑p 10002
         *
         * @return A proxy of the remote instance.
         */
        template <class ProxyType>
        ProxyType getProxy(const std::string& name,
                           const std::string& endpoints = std::string())
        {
            std::string proxyString = name;

            if (!endpoints.empty())
            {
                proxyString += std::string(":") + endpoints;
            }

            std::string proxyTypedId =
                proxyString
                + std::string(":")
                + std::string(typeid(ProxyType).name());

            try
            {
                boost::mutex::scoped_lock lock(proxyCacheMutex);
                return ProxyType::uncheckedCast(
                           checkedProxies.at(proxyTypedId));
            }
            catch (std::out_of_range& uncheckedProxiesException)
            {
            }

            Ice::ObjectPrx base = getCommunicator()->stringToProxy(proxyString);

            ProxyType proxy;

            try
            {
                proxy = ProxyType::checkedCast(base);
                boost::mutex::scoped_lock lock(proxyCacheMutex);
                checkedProxies[proxyTypedId] = proxy;
            }
            catch (const Ice::ConnectionRefusedException&)
            {
                std::stringstream exceptionText;
                exceptionText << "Connection refused for proxy: " << name << " ProxyString: " << proxyString << " Endpoints: " << endpoints << std::endl;
                throw armarx::UserException(exceptionText.str());
            }

            if (!proxy)
            {
                std::stringstream exceptionText;
                exceptionText << "Invalid Proxy. Searched for: " << name << " ProxyString: " << proxyString << " Endpoints: " << endpoints << std::endl;
                throw armarx::UserException(exceptionText.str());
            }

            return proxy;
        }

        /**
         * @brief This functions removes the given proxy from the proxy cache.
         * This is useful if the cached proxy became invalid.
         * @param  name       Proxy name, e.g. Log
         * @param  endpoints  The endpoints, e.g. tcp ‑p 10002
         * @return True, if the proxy was found and removed.
         */
        template <class ProxyType>
        bool removeProxyFromCache(const std::string& name,
                                  const std::string& endpoints = std::string())
        {
            std::string proxyString = name;

            if (!endpoints.empty())
            {
                proxyString += std::string(":") + endpoints;
            }

            std::string proxyTypedId =
                proxyString
                + std::string(":")
                + std::string(typeid(ProxyType).name());
            return (checkedProxies.erase(proxyTypedId) > 0);
        }

        bool removeProxyFromCache(const std::string& name,
                                  const std::string& typeName,
                                  const std::string& endpoints = std::string());

        /**
         * @brief This functions removes the given proxy from the proxy cache.
         * This is useful if the cached proxy became invalid.
         * @param  name       Proxy name, e.g. Log
         * @param  endpoints  The endpoints, e.g. tcp ‑p 10002
         * @return True, if the proxy was found and removed.
         */
        bool removeProxyFromCache(const Ice::ObjectPrx& proxy);

        /**
         * Provides access to the Ice Storm Topic Manager.
         * A proxy is created if necessary.
         *
         * @return A proxy of the IceStorm TopicManager.
         */
        IceStorm::TopicManagerPrx getTopicManager();
        static IceStorm::TopicManagerPrx GetTopicManager(Ice::CommunicatorPtr communicator);

        /**
         * Gets an Ice Storm topic for publishing messages.
         *
         * @param  topicName The name of the topic to publish on.
         *
         * @return           A proxy of the topic's remote instance.
         */
        template <class TopicProxy>
        TopicProxy getTopic(const std::string& topicName, bool useUDP = false)
        {
            Ice::ObjectPrx pub = __getTopic(topicName, useUDP);
            TopicProxy castTopic = TopicProxy::uncheckedCast(pub);

            return castTopic;
        }

        /**
         * Subscribe an object to a particular Ice Storm topic.
         * If the object is already subscribed to the topic, it will be unsubscribed before.
         * This is necessary to circumvent the usage from outdated subscriptions due to
         * unproper exits.
         *
         * @param subcriber         A proxy of the object to be subscribed,
         *                          implementing the interface for listening on this
         *                          topic
         * @param topicName         The topic's name
         * @param orderedPublishing If true, the order of topic calls is ensured. This
         *                          might decrease performance.
         */
        void subscribeTopic(Ice::ObjectPrx subscriber,
                            const std::string& topicName, bool orderedPublishing = false);

        /**
         * Unsubscribe a given subscriber from a given topic
         *
         * @param subscriberProxy of the subscriber
         * @param topicName The name of the subscribed topic.
         */
        void unsubscribeTopic(Ice::ObjectPrx subscriberProxy,
                              const std::string& topicName);

        /**
         * Registers a specified object that is required before
         * activating the endpoints.
         *
         * @param registrantName        Object that has a dependency
         * @param dependecyObjectName   Dependecy object name
         *
         *
         * @throw ObjectAlreadyActiveException
         */
        void registerObjectDependency(
            const std::string& registrantName,
            const std::string& dependencyObjectName);

        /**
         * Registers a delayed topic subscription.
         *
         * @param registrantName    Registrant object name
         * @param topicName         The topic name that should be subscribed
         *                          later on
         */
        void registerDelayedTopicSubscription(
            const std::string& registrantName,
            const std::string& topicName);

        /**
         * Registers a delayed topic retrieval.
         *
         * @param registrantName    Registrant object name
         * @param topicName     The topic name
         */
        void registerDelayedTopicRetrieval(
            const std::string& registrantName,
            const std::string& topicName);

        /**
         * @brief creates a proxy to the object specified with parameter objectName
         * and tries to ping it.
         * @param objectName Name of the object that should be checked
         * @return true, if reachable
         */
        bool isObjectReachable(std::string objectName);

        /**
         * @brief creates a proxy to the object specified with parameter objectName
         * and tries to ping it.
         * @param objectName Name of the object that should be checked
         * @return true, if reachable
         */
        template <typename ProxyType>
        bool isObjectReachable(std::string objectName);

        /**
         * Short helper method to return the Ice Communicator.
         *
         * Uses static communicator() method
         */
        const Ice::CommunicatorPtr& getCommunicator();

        /**
         * Provides access to the IceGrid Registry. A proxy is created if
         * necessary.
         *
         * @return A proxy of the IceGrid Registy.
         */
        IceGrid::RegistryPrx getIceGridRegistry();

        /**
         * Provides access to the IceGrid AdminSession via IceGridAdmin.
         *
         * @return The IceGrid AdminSession.
         */
        const armarx::IceGridAdminPtr& getIceGridSession();

        /**
         * Sets the session name
         */
        void setName(std::string name);

        /**
         * Removes all component objects and topics and shuts down the
         * communicator.
         */
        void shutdown();

        /**
         * Waits until all invoked operation has been processed
         */
        void waitForShutdown();

        /**
         * Determines whether the communicator is shutdown.
         */
        bool isShutdown();

        /**
         * Destroys the communicator
         */
        void destroy();

        /**
         * @brief Get the suffix that is appended to all topics.
         */
        std::string getTopicSuffix() const;

    private:

        Ice::ObjectPrx __getTopic(const std::string& topicName, bool useUDP = false);

        struct ObjectEntry;

        struct DependencyObjectEntry: public IceUtil::Shared
        {
            DependencyObjectEntry(std::string name, Ice::ObjectPrx proxy) :
                name(name),
                proxy(proxy),
                resolved(false)
            {

            }

            std::string name;
            Ice::ObjectPrx proxy;
            bool resolved;
        };

        typedef IceUtil::Handle<ObjectEntry>            ObjectEntryPtr;
        typedef IceUtil::Handle<DependencyObjectEntry>  DependencyObjectEntryPtr;

        typedef std::list<std::string>                  TopicList;
        typedef std::list<DependencyObjectEntryPtr>     DependencyList;

        struct ObjectEntry: public IceUtil::Shared
        {
            ObjectEntry() :
                name(std::string()),
                proxy(nullptr),
                adapter(nullptr),
                updated(true),
                dependenciesResolved(false),
                active(false),
                ownAdapter(true)
            {
            }

            std::string                 name;
            Ice::Identity               id;
            Ice::ObjectPrx              proxy;
            Ice::ObjectAdapterPtr       adapter;
            TopicList                   usedTopics;
            TopicList                   offeredTopics;
            DependencyList              dependencies;
            bool                        updated;
            bool                        dependenciesResolved;
            bool                        active;
            bool                        ownAdapter;
        };

        typedef boost::unordered_map<std::string, ObjectEntryPtr>   ObjectRegistry;
        typedef boost::unordered_map<std::string, Ice::ObjectPrx>   ProxyMap;

        /**
         * Subscribes all topics registered for a delayed subscription
         */
        void subscribeTopics(Ice::ObjectPrx subscriber,
                             const TopicList& topics, bool orderedPublishing = false);

        /**
         * gets all topics registered for a delayed retrieval
         */
        void retrieveTopics(const TopicList& topics);

        /**
         * Check the existence of required servants.
         *
         * @return True if all servant objects exist, otherise false
         */
        void resolveObjectDependencies();

        /**
         * Returns the requested object entry if exists, otherwise
         * a new entry is created and return.
         *
         * @param objectName        Object name of the entry
         *
         * @return Smart pointer of the requested ObjectEntry.
         */
        ObjectEntryPtr getOrCreateObjectEntry(const std::string& objectName);

        /**
         * Cleans up all current IceStorm connections.
         */
        void cleanUp();

        /**
         * Retrieves an Ice Storm topic from the topic manager if necessary.
         * Proxies are cached.
         *
         * @param  topicName     The name of the topic to publish on.
         *
         * @return A generic proxy instance of the topic. Has to be cast for
         *         actual messages to be sent.
         */
        IceStorm::TopicPrx retrieveTopic(const std::string& topicName);

        /**
         * Retrieves an Ice Storm topic from the topic manager if necessary.
         * Proxies are cached.
         *
         * @param  topicName     The name of the topic to publish on.
         *
         * @return A generic proxy instance of the topic. Has to be cast for
         *         actual messages to be sent.
         *
         * @deprecated Use IceManager::retrieveTopic() instead
         */
        IceStorm::TopicPrx stormTopicRetrieve(const std::string& topicName);

        /**
         * Application communicator
         */
        Ice::CommunicatorPtr communicator;

        /**
         * IceStorm Topic Manager proxy
         */
        IceStorm::TopicManagerPrx topicManagerProxy;

        /**
         * TopicManager instantiation mutex
         */
        boost::mutex topicManagerMutex;

        boost::mutex proxyCacheMutex;


        /**
         * Ice grid admin instance used administrate component objects
         * withing the IceGrid
         */
        IceGridAdminPtr iceGridAdmin;

        /**
         * IceGridAdmin instantiation mutex
         */
        boost::mutex iceGridAdminMutex;

        /**
         * Session name
         */
        std::string name;

        /**
         * Object registry which contains object dependecy and IceStorm
         * topic information.
         */
        ObjectRegistry objectRegistry;

        /**
         * Mutex for adding, activating and removing objects
         */
        boost::mutex objectRegistryMutex;

        /**
         * Retrieved and down casted checked proxies. If a requested proxy
         * resides in this map, a unchecked cast solely is performed before
         * returning the proxy.
         */
        ProxyMap checkedProxies;

        /**
          * Suffix for all topics. This is mainly used to direct all topics to another, unused topic
          * for replaying purposes.
          */
        std::string topicSuffix;
        /**
         * Retrieved topics map
         */
        std::map<std::string, IceStorm::TopicPrx> topics;

        /**
         * Subscribed IceStorm topics
         */
        std::vector<std::pair<std::string, Ice::ObjectPrx> > subscriptions;

        /**
         * Mutex for delayed topic retrieval and subscription registration
         */
        boost::mutex topicRegistrationMutex;

        /**
         * Mutex for retrieving and subscribing topics
         */
        boost::mutex topicRetrievalMutex;

        /**
         * Mutex for retrieving and subscribing topics
         */
        boost::mutex topicSubscriptionMutex;

        /**
         * Flag of forcing shutdown procedure.
         */
        bool forceShutdown;
    };


    template <typename ProxyType>
    bool IceManager::isObjectReachable(std::string objectName)
    {
        try
        {
            ProxyType prx = getProxy<ProxyType>(objectName);
            prx->ice_timeout(500)->ice_ping();
            // if ping'able, object is already registered
            return true;
        }
        catch (...)
        {

            return false;
        }
    }

}

