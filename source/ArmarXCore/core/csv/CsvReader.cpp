/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Simon Ottenhaus (simon dot ottenhaus at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "CsvReader.h"
#include <ArmarXCore/core/util/StringHelpers.h>

using namespace armarx;

CsvReader::CsvReader(const std::string& filename, bool hasHeader)
    : separator(",")
{
    file.open(filename, std::ios_base::in);

    if (hasHeader)
    {
        std::string line;
        readLine(line);
        header = Split(line, separator);
    }
}

bool CsvReader::read(std::vector<std::string>& row)
{
    std::string line;
    if (!readLine(line))
    {
        return false;
    }
    row = Split(line, separator);
    return true;
}

bool CsvReader::read(std::vector<float>& row)
{
    row.clear();
    std::string line;
    if (!readLine(line))
    {
        return false;
    }
    std::vector<std::string> rowStr = Split(line, separator);
    for (const std::string& s : rowStr)
    {
        row.push_back(toFloat(s));
    }
    return true;
}

bool CsvReader::read(std::map<std::string, float>& row)
{
    row.clear();
    std::string line;
    if (!readLine(line))
    {
        return false;
    }
    std::vector<std::string> rowStr = Split(line, separator);
    for (size_t i = 0; i < rowStr.size() && i < header.size(); i++)
    {
        row[header.at(i)] = toFloat(rowStr.at(i));
    }
    return true;
}

std::vector<std::vector<float> > CsvReader::readAllFloatList()
{
    std::vector<std::vector<float> > data;
    std::vector<float> row;
    while (read(row))
    {
        data.push_back(row);
    }
    return data;
}

std::vector<std::map<std::string, float> > CsvReader::readAllFloatMap()
{
    std::vector<std::map<std::string, float> > data;
    std::map<std::string, float> row;
    while (read(row))
    {
        data.push_back(row);
    }
    return data;
}

std::map<std::string, std::vector<float> > CsvReader::readAllColumnsFloatMap()
{
    std::map<std::string, std::vector<float> > data;
    std::vector<float> row;
    while (read(row))
    {
        for (size_t i = 0; i < row.size() && i < header.size(); i++)
        {
            data[header.at(i)].push_back(row.at(i));
        }
    }
    return data;
}

std::vector<std::string> CsvReader::getHeader()
{
    return header;
}


bool CsvReader::isEof()
{
    return file.eof();
}

bool CsvReader::readLine(std::string& line)
{
    return static_cast<bool>(std::getline(file, line));
}
