/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Simon Ottenhaus (simon dot ottenhaus at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <boost/shared_ptr.hpp>

#include <sstream>
#include <string>
#include <map>
#include <iostream>
#include <fstream>
#include <vector>

namespace armarx
{
    class CsvReader;
    typedef boost::shared_ptr<CsvReader> CsvReaderPtr;

    class CsvReader
    {
    public:
        CsvReader(const std::string& filename, bool hasHeader);
        bool read(std::vector<std::string>& row);
        bool read(std::vector<float>& row);
        bool read(std::map<std::string, float>& row);
        std::vector<std::vector<float>> readAllFloatList();
        std::vector<std::map<std::string, float>> readAllFloatMap();
        std::map<std::string, std::vector<float>> readAllColumnsFloatMap();
        std::vector<std::string> getHeader();
        bool isEof();

    private:
        bool readLine(std::string& line);
        std::ifstream file;
        std::vector<std::string> header;
        std::string separator;
    };
}

