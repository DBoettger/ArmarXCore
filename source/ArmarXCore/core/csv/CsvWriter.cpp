/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Simon Ottenhaus (simon dot ottenhaus at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "CsvWriter.h"

using namespace armarx;

CsvWriter::CsvWriter(const std::string& filename, const std::vector<std::string>& header, bool append)
    : autoflush(true), separator(",")
{
    file.open(filename.c_str(), append ? std::ios_base::app : std::ios_base::out | std::ios_base::trunc);
    file.imbue(std::locale::classic());
    if (header.size() > 0)
    {
        write(header);
    }
}

void CsvWriter::write(const std::vector<std::string>& row)
{
    bool first = true;
    for (const std::string& s : row)
    {
        if (!first)
        {
            file << separator;
        }
        first = false;
        file << s;
    }
    file << "\n";
    if (autoflush)
    {
        file.flush();
    }
}

void CsvWriter::write(const std::vector<float>& row)
{
    bool first = true;
    for (float f : row)
    {
        if (!first)
        {
            file << separator;
        }
        first = false;
        file << f;
    }
    file << "\n";
    if (autoflush)
    {
        file.flush();
    }
}

void CsvWriter::close()
{
    file.flush();
    file.close();
}

CsvWriter::~CsvWriter()
{
    if (file.is_open())
    {
        file.close();
    }
}
