/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author    Kai Welke (kai dot welke at kit dot edu)
 * @date       2012
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <ArmarXCore/core/ArmarXObjectObserver.h>
#include <ArmarXCore/core/ManagedIceObjectRegistryInterface.h>
// superclasses
#include <ArmarXCore/core/logging/Logging.h>  // for Logging
#include <ArmarXCore/core/util/StringHelpers.h>
#include <ArmarXCore/interface/core/ArmarXManagerInterface.h>
#include <Ice/BuiltinSequences.h>       // for StringSeq
#include <Ice/CommunicatorF.h>          // for CommunicatorPtr
#include <Ice/Current.h>                // for Current
#include <Ice/ObjectAdapterF.h>         // for ObjectAdapterPtr
#include <Ice/Properties.h>
#include <IceUtil/Handle.h>             // for Handle
#include <boost/smart_ptr/shared_ptr.hpp>  // for shared_ptr
#include <boost/thread/pthread/condition_variable_fwd.hpp>
#include <boost/thread/pthread/mutex.hpp>  // for mutex
#include <cstddef>                      // for size_t
#include <map>                          // for map, map<>::value_compare


// stl
#include <string>                       // for string
#include <vector>                       // for vector

#include "ArmarXCore/core/ArmarXObjectScheduler.h"
#include "ArmarXCore/core/IceManager.h"  // for IceManager
#include "ArmarXCore/core/system/Synchronization.h"  // for RecursiveMutex, etc
#include "ArmarXCore/interface/core/BasicTypes.h"
#include "ArmarXCore/interface/core/Log.h"  // for MessageType
#include "ArmarXCore/interface/core/ManagedIceObjectDefinitions.h"
#include "ArmarXCore/core/RemoteReferenceCount.h"

namespace armarx
{
    // forward declaration
    class ArmarXObjectScheduler;

    typedef IceUtil::Handle<ArmarXObjectScheduler> ArmarXObjectSchedulerPtr;

    class IceManager;

    typedef IceUtil::Handle<IceManager> IceManagerPtr;

    class ArmarXMultipleObjectsScheduler;
    typedef IceUtil::Handle<ArmarXMultipleObjectsScheduler> ArmarXMultipleObjectsSchedulerPtr;

    /**
     * IceObjectManager smart pointer type
     */
    class ArmarXManager;
    template <class T>
    class PeriodicTask;

    typedef IceUtil::Handle<ArmarXManager> ArmarXManagerPtr;


    struct SharedRemoteHandleState;

    /**
     * @class ArmarXManager
     * @brief Main class of an ArmarX process.
     * @ingroup DistributedProcessingGrp
     *
     * The ArmarXManager is the main class for writing distributed
     * applications in the ArmarX framework.
     * The ArmarXManager takes care of handling ManagedIceObjects.
     * ManagedIceObjects are the core elements of ArmarX distributed applications.
     *
     * Usually, each process creates one ArmarXManager and adds the
     * required ManagedIceObjects to this manager.
     *
     * The ArmarXManager provides and Ice interface which allows
     * retrieving the state and network connectivity of all
     * ManagedIceObject within the Manager.
     * To provide this functionality it holds one instance of Ice::Communicator
     *
     * Finally it also does shutdown handling
     */
    class ArmarXManager:
        virtual public Logging,
        virtual public ArmarXManagerInterface,
        public ManagedIceObjectRegistryInterface
    {
        friend class PeriodicTask<ArmarXManager>;
        friend class ArmarXObjectObserver;

        enum ArmarXManagerState
        {
            eCreated,
            eRunning,
            eShutdownInProgress,
            eShutdown
        };

    public:
        /**
         * ArmarXManager constructor. With this constructor, the
         * ArmarXManager builds its own communicator using the specified port, host and locatorName
         * (default: --Ice.Default.Locator= IceGrid/Locator:tcp -p 4061 -h localhost)
         *
         * @param applicationName name of the application used for registering the manager to Ice and for logging
         * @param port the port of the Ice locator service
         * @param host the host of the Ice locator service
         * @param locatorName the name of the Ice locator service
         * @param args list of arguments for Ice (e.g. "--Ice.ThreadPool.Server.SizeMax=10")
         */
        ArmarXManager(std::string applicationName, int port = 4061, std::string host = "localhost", std::string locatorName = "IceGrid/Locator", Ice::StringSeq args = Ice::StringSeq());

        /**
         * Constructs an ArmarXManager by passing a communicator. This can be used if communicator has already
         * been constructed e.g in Ice::Application.
         *
         * @param applicationName name of the application used for registering the manager to Ice and for logging
         * @param communicator configured Ice::Communicator
         */
        ArmarXManager(std::string applicationName, const Ice::CommunicatorPtr& communicator);

        ~ArmarXManager() override;

        bool checkIceConnection(bool printHint = true) const;
        static bool CheckIceConnection(const Ice::CommunicatorPtr& communicator, bool printHint);

        /**
         * Enable or disable logging
         *
         * @param enable whether to enable logging
         */
        void enableLogging(bool enable);

        /**
          * Enable or disable profiling of CPU Usage
          */
        void enableProfiling(bool enable);

        /**
         * Set minimum logging level to output in log stream.
         *
         * @param applicationName name of the application used for registering the manager to Ice and for logging
         * @param communicator configured Ice::Communicator
         */
        void setGlobalMinimumLoggingLevel(MessageType minimumLoggingLevel);

        /**
         * Set data paths used to search for datafiles
         *
         * @param dataPaths semicolon seperated list of paths
         */
        void setDataPaths(std::string dataPaths);

        /**
         * Add a ManagedIceObject to the manager. Takes care of the IceManagedObject
         * lifcycle using and ArmarXObjectScheduler. addObject is
         * thread-safe and can be called from any method in order to dynamically
         * add ManagedIceObjects (as e.g. required for GUI).
         *
         * @param object      object to add
         * @param addWithOwnAdapter      If true this object will have it's own adapter
         * @param objectName      Name that the object should have. if not empty overrides the currently set name.
         */
        void addObject(const ManagedIceObjectPtr& object, bool addWithOwnAdapter = true, const std::string& objectName = "",  bool useOwnScheduleThread = true) override;
        virtual void addObject(const ManagedIceObjectPtr& object, const std::string& objectName, bool addWithOwnAdapter = true,  bool useOwnScheduleThread = true);

        virtual void addObject(const ManagedIceObjectPtr& object, Ice::ObjectAdapterPtr objectAdapterToAddTo, const std::string& objectName = "",  bool useOwnScheduleThread = true);

        virtual void addObjectAsync(const ManagedIceObjectPtr& object, const std::string& objectName, bool addWithOwnAdapter = true,  bool useOwnScheduleThread = true);

        /**
         * Removes an object from the manager.
         *
         * This version waits until all calls to this component are finished and
         * the has been completely removed.
         *
         * @param object object to remove
         */
        void removeObjectBlocking(const ManagedIceObjectPtr& object) override;

        /**
         * Removes an object from the manager.
         *
         * This version waits until all calls to this component are finished and
         * the has been completely removed.
         *
         * @param objectName name of the object to remove
         */
        void removeObjectBlocking(const std::string& objectName) override;

        /**
         * Removes an object from the manager.
         *
         * This version returns immediatly and does <b>not</b> wait for all call to this
         * to finish.
         *
         * @param object object to remove
         */
        void removeObjectNonBlocking(const ManagedIceObjectPtr& object) override;

        /**
         * Removes an object from the manager.
         *
         * This version returns immediatly and does <b>not</b> wait for all call to this
         * to finish.
         *
         * @param objectName name of the object to remove
         */
        void removeObjectNonBlocking(const std::string& objectName) override;

        /**
         * Wait for shutdown. Shutdown is initialized once shutdown() is called on ArmarXManager e.g. from a signal handler.
         */
        void waitForShutdown();

        /**
         * Shuts down the ArmarXManager. If a thread is waiting in waitForShutdown, the thread is activated after shutdown has been completed.
         */
        void shutdown();

        /**
         * @brief Calls shutdown() after a timeout.
         * @param timeoutMs The timeout to wait. (unit ms)
         */
        void asyncShutdown(std::size_t timeoutMs = 0);

        /**
         * Whether ArmarXManager shutdown has been finished.
         *
         * @return shutdown finished
         */
        bool isShutdown();

        /**
         * Retrieve pointers to all ManagedIceObject.
         *
         * @return vector of pointers
         */
        std::vector<ManagedIceObjectPtr> getManagedObjects() override;

        /**
         * Retrieve state of a ManagedIceObject. Part of the Ice interface.
         *
         * @param objectName name of the object
         * @param c Ice context
         *
         * @return state of the ManagedIceObject
         */
        ManagedIceObjectState getObjectState(const std::string& objectName, const Ice::Current& c = ::Ice::Current()) override;

        /**
         * Retrieve connectivity of a ManagedIceObject. Part of the Ice interface.
         *
         * @param objectName name of the object
         * @param c Ice context
         *
         * @return connectivity of the ManagedIceObject
         */
        ManagedIceObjectConnectivity getObjectConnectivity(const std::string& objectName, const Ice::Current& c = ::Ice::Current()) override;

        /**
         * @brief getObjectProperties is used to retrieve the properties of an object
         * @return
         */
        ::armarx::StringStringDictionary getObjectProperties(const ::std::string& objectName, const ::Ice::Current& = ::Ice::Current()) override;
        ObjectPropertyInfos getObjectPropertyInfos(const::std::string& objectName, const::Ice::Current&) override;
        ObjectPropertyInfos getApplicationPropertyInfos(const::Ice::Current&) override;


        Ice::PropertiesAdminPrx getPropertiesAdmin(const Ice::Current& = ::Ice::Current()) override;

        /**
         * Calls Component::setIceProperties() on all components
         * assigend to this ArmarXManager Instance.
         */
        void setComponentIceProperties(const Ice::PropertiesPtr& properties);

        void updateComponentIceProperties(const Ice::PropertyDict& properties);

        /**
         * Retrieve the names of all ManagedIceObject. Part of the Ice interface.
         *
         * @param c Ice context
         *
         * @return vector of names
         */
        Ice::StringSeq getManagedObjectNames(const Ice::Current& c = ::Ice::Current()) override;

        /**
         * @brief Retrieve the instance of the icemanager
         * @return Pointer to the iceManager
         * @see ManagedIceObject::getIceManager()
         */
        const IceManagerPtr& getIceManager() const;
        const Ice::CommunicatorPtr& getCommunicator() const;

        /**
         * @brief Registers all object factories that are known with Ice.
         *
         * Object factories are automatically preregistered when their library
         * is linked. Function is idempotent.
         * This function is called in init() of ArmarXManager and needs only be called, if
         * no ArmarXManager is used or libraries are loaded at runtime.
         * @see FactoryCollectionBase
         */
        static void RegisterKnownObjectFactoriesWithIce(const Ice::CommunicatorPtr& ic);

        /**
         * @brief non static convenience version of ArmarXManager::RegisterKnownObjectFactoriesWithIce()
         * @see RegisterKnownObjectFactoriesWithIce()
         */
        void registerKnownObjectFactoriesWithIce();

        const Ice::ObjectAdapterPtr& getAdapter() const;

        const boost::shared_ptr<SharedRemoteHandleState>& getSharedRemoteHandleState() const;

        /**
         * @brief increased the number of single threaded schedulers. Decrease is not possible,
         * since the link to objects is fixed.
         * @param increasyBy Increases the number of single threaded schedulers by this amount.
         */
        void increaseSchedulers(int increaseBy);

        template<class Function, class Data>
        RemoteReferenceCountControlBlockPtr<Function, Data> createRemoteReferenceCount(
            Function f, Data d, const std::string& id = "",
            IceUtil::Time deletionDelay = IceUtil::Time::seconds(3),
            IceUtil::Time orphantDeletionDelay = IceUtil::Time::seconds(6),
            long heartBeatMs = 1000)
        {
            RemoteReferenceCountControlBlockPtr<Function, Data> ptr =
                new RemoteReferenceCountControlBlock<Function, Data>
            (
                this, id, std::forward<decltype(f)>(f), std::forward<decltype(d)>(d),
                deletionDelay, orphantDeletionDelay, heartBeatMs
            );
            remoteReferenceCountControlBlockManager->add(ptr);
            return ptr;
        }
        template<class Function>
        RemoteReferenceCountControlBlockPtr<Function> createRemoteReferenceCount(
            Function f, const std::string& id = "",
            IceUtil::Time deletionDelay = IceUtil::Time::seconds(3),
            IceUtil::Time orphantDeletionDelay = IceUtil::Time::seconds(6),
            long heartBeatMs = 1000)
        {
            RemoteReferenceCountControlBlockPtr<Function> ptr =
                new RemoteReferenceCountControlBlock<Function>
            (
                this, id, std::forward<decltype(f)>(f),
                deletionDelay, orphantDeletionDelay, heartBeatMs
            );
            remoteReferenceCountControlBlockManager->add(ptr);
            return ptr;
        }

        template<class Function, class Data>
        SimpleRemoteReferenceCountControlBlockPtr<Function, Data>
        createSimpleRemoteReferenceCount(
            Function f, Data d, const std::string& id = "", IceUtil::Time deletionDelay = IceUtil::Time::seconds(3))
        {
            SimpleRemoteReferenceCountControlBlockPtr<Function, Data> ptr =
                new SimpleRemoteReferenceCountControlBlock<Function, Data>
            (
                this, id, std::forward<decltype(f)>(f), std::forward<decltype(d)>(d), deletionDelay
            );
            remoteReferenceCountControlBlockManager->add(ptr);
            return ptr;
        }
        template<class Function>
        SimpleRemoteReferenceCountControlBlockPtr<Function>
        createSimpleRemoteReferenceCount(
            Function f, const std::string& id = "", IceUtil::Time deletionDelay = IceUtil::Time::seconds(3))
        {
            SimpleRemoteReferenceCountControlBlockPtr<Function> ptr =
                new SimpleRemoteReferenceCountControlBlock<Function>
            (
                this, id, std::forward<decltype(f)>(f), deletionDelay
            );
            remoteReferenceCountControlBlockManager->add(ptr);
            return ptr;
        }

        StringVariantBaseMap getMetaInfo(const std::string&, const Ice::Current&) override;


        bool loadLibFromPath(const std::string& path, const Ice::Current& = GlobalIceCurrent) override;
        bool loadLibFromPackage(const std::string& package, const std::string& lib, const Ice::Current& = GlobalIceCurrent) override;
    private:
        // typedefs
        typedef std::map<std::string, ArmarXObjectSchedulerPtr> ObjectSchedulerMap;
        typedef std::vector<ArmarXObjectSchedulerPtr> ObjectSchedulerList;

        // internal init used by both constructors
        void init(std::string applicationName, const Ice::CommunicatorPtr& communicator);

        // thread method to cleanup terminated schedulers
        void cleanupSchedulers();

        /**
         * @brief Disconnects all objects that dependent on the given object.
         * @param object name of the object of which all dependees should be removed
         */
        void disconnectDependees(const std::string& object);
        void disconnectAllObjects();
        std::vector<std::string> getDependendees(const std::string& removedObject);

        /**
         * @brief Schedulers which are waiting for dependencies are checking the
         * dependencies in a fix interval. Call this function to wake them up and
         * perform the check immediatly.
         */
        void wakeupWaitingSchedulers();

        // remove all managed objects
        void removeAllObjects(bool blocking);

        // remove specific managed objects
        bool removeObject(const ArmarXObjectSchedulerPtr& objectScheduler, bool blocking);

        ArmarXObjectSchedulerPtr findObjectScheduler(const std::string& objectName) const;

        void checkDependencies();

        // synchronization
        ScopedRecursiveLockPtr acquireManagedObjectsMutex();

        // name of the application
        std::string applicationName;

        // state of the manager
        ArmarXManagerState managerState;
        boost::mutex managerStateMutex;

        // ice manager
        IceManagerPtr iceManager;

        ArmarXObjectObserverPtr objObserver;

        // object scheduler registry
        ObjectSchedulerMap managedObjects;
        ObjectSchedulerList terminatingObjects;
        mutable RecursiveMutex managedObjectsMutex;
        boost::mutex terminatingObjectsMutex;

        Mutex schedulerListMutex;
        std::vector<ArmarXMultipleObjectsSchedulerPtr> singleThreadedSchedulers;

        // shutdown handling
        boost::mutex shutdownMutex;
        boost::condition_variable shutdownCondition;

        // cleanup task
        IceUtil::Handle<PeriodicTask<ArmarXManager> > cleanupSchedulersTask;

        IceUtil::Handle<PeriodicTask<ArmarXManager> > checkDependencyStatusTask;
        Ice::ObjectAdapterPtr armarxManagerAdapter;

        //remotehandles
        boost::shared_ptr<SharedRemoteHandleState> sharedRemoteHandleState;
        std::shared_ptr<RemoteReferenceCountControlBlockManager> remoteReferenceCountControlBlockManager;
    };
}

