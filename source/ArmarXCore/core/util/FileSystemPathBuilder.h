/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu)
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include <string>
#include <functional>
#include <fstream>

#include <boost/filesystem.hpp>
#include <boost/regex.hpp>

#include "Registrar.h"

namespace armarx
{
    /**
     * @brief Helps to build a path via format strings:
     * All format strings are of the form '{[^{}]+}'
     * If the resulting path should contain the characters '{' or '}', they have to be replaced with '{LEFT CURLY}' and '{RIGHT CURLY}'
     *
     * Default patterns are:
     *   * {LEFT CURLY}          -> {
     *   * {RIGHT CURLY}         -> }
     *   * {year / Year}         -> year (2/4 digits)
     *   * {month}
     *   * {day}
     *   * {hour}
     *   * {minute}
     *   * {second}
     *   * {Date}               -> YYYY-MM-DD
     *   * {Time}               -> HH-MM-SS
     *   * {DateTime}           -> {Date}_{Time}
     *   * {time-since-epoch}   -> nanoseconds since epoch (can be used as uuid)
     *   * {PackageDir:...}     ->          Directory of the given package
     *   * {ScenarioDir:...}    -> Scenario Directory of the given package
     *   * {DataDir:...}        -> Data     Directory of the given package
     *   * {BinaryeDir:...}     -> Binary   Directory of the given package
     *   * {CMakeDir:...}       -> CMake    Directory of the given package
     *   * {BuildDir:...}       -> Build    Directory of the given package
     */
    struct FileSystemPathBuilder
    {
        static const boost::regex RawPathRegex; //"([^{}]+|{[^{}]+})+"
        /**
         * @brief Replacer for a pattern.
         * The matched pattern hast to match '[^{}]+' (must not contain '{' or '}').
         * The replacement string may contain '{' and '}'
         * The patternReplacer is applied to patterns matched by patternRegex.
         */
        struct FormatStringOption
        {
            std::string name;
            std::string description;
            boost::regex patternRegex;
            std::function<std::string(const std::string&)> patternReplacer;
        };

        struct RegisterFormatStringOption
        {
            RegisterFormatStringOption(
                std::string name,
                boost::regex patternRegex,
                std::function<std::string(const std::string&)> replacer,
                std::string description = "");
        };

        /**
         * @brief FileSystemPathBuilder
         * @param rawPath must match RawPathRegex ("([^{}]+|{[^{}]+})+")
         */
        FileSystemPathBuilder(std::string rawPath) : rawPath {std::move(rawPath)}, path {ApplyFormatting(this->rawPath)}, bpath {path} {}

        const boost::filesystem::path& getBoostPath() const
        {
            return bpath;
        }
        const std::string& getRawPath() const
        {
            return rawPath;
        }
        const std::string& getPath() const
        {
            return path;
        }
        void createParentDirectories() const
        {
            boost::filesystem::create_directories(bpath.parent_path());
        }

        //broken since old gcc has no move ctor for fstreams
        //        std::ofstream getOfStream() const
        //        {
        //            boost::filesystem::create_directories(bpath.parent_path());
        //            return std::ofstream {path};
        //        }
        //        std::ifstream getIfStream() const
        //        {
        //            return std::ifstream {path};
        //        }


        static std::map<std::string, FormatStringOption> GetFormatStringOptions();

        static std::string ApplyFormatting(const std::string& rawPath);

        const std::string rawPath;
        const std::string path;
        const boost::filesystem::path bpath;

    };
    using FileSystemPathBuilderFormatStringOptionRegistrar = Registrar<FileSystemPathBuilder::FormatStringOption, std::string>;
}
