/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
* @package    ArmarXCore
* @author     Raphael Grimm ( ufdrv at student dot kit dot edu)
* @date       2016
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/
#pragma once

#include <atomic>
#include <thread>
#include <condition_variable>
#include <chrono>
#include <functional>

#include <boost/shared_ptr.hpp>
#include <boost/make_shared.hpp>

#include <Ice/ObjectAdapter.h>
#include <IceUtil/UUID.h>

#include <ArmarXCore/interface/core/util/distributed/RemoteHandle/RemoteHandleControlBlock.h>
#include "ClientSideRemoteHandleControlBlock.h"
#include "../../../services/tasks/PeriodicTask.h"
#include "../../../ArmarXManager.h"


namespace armarx
{
    class RemoteHandleControlBlock;
    using RemoteHandleControlBlockPtr = IceInternal::Handle<RemoteHandleControlBlock>;


    /**
     * @brief This holds the shared state of all RemoteHandleControlBlocks for one armarx manager.
     * It holds a thread responsible for deleting them when their usecount reaches zero and the deletion delay has passed.
     */
    struct SharedRemoteHandleState
    {
    public:
        /**
         * @brief The amount of time (in ms) required to pass after a RemoteHandleControlBlock's usecount has reacht zero, before it is deleted.
         */
        static const unsigned int DEFAULT_DELETION_DELAY;
        using PeriodicTaskType = PeriodicTask<SharedRemoteHandleState>;

        SharedRemoteHandleState(unsigned int deletionDelayMs);
        ~SharedRemoteHandleState();

        void add(RemoteHandleControlBlockPtr rh);

    private:
        void sweep();

        std::mutex stateMutex;

        //must not invalidate other iterators on erase (ok for std::set)
        std::set<RemoteHandleControlBlockPtr> rhs;
        std::chrono::milliseconds deletionDelay;
        PeriodicTaskType::pointer_type sweeper;
    };

    /**
     * @brief The RemoteHandleControlBlock is the equivalent of the std::shared_ptr's contol_block for a remote handle.
     * It does reference counting and object deletion.
     *
     * This class is created on server side and a ClientSideRemoteHandleControlBlock is send to the client.
     * Only create the class by calling RemoteHandleControlBlock::create.
     *
     * A direct pointer can be used to force the deletion of the managed object.
     * The deletion has to be done by a passed deleter (it may hold the only pointer to the managed object or execute code to remove the object from some structure).
     * The deleter should do any unregistering for the managed object (e.g. remove the object from from an ice object adapter)
     *
     * a object may leak if a connection error stops the message send when a handle is deleted (this stops the usecount from reaching zero).
     * a object may be deleted with handles still alive if:
     * 1) the object's deletion was forced
     * 2) a scenario equivalent to this:
     *   pc A has the object and sends a handle to B
     *   pc B has now the only handle
     *   pc B now sends the handle to pc C and delets the handle before C deserialized the object (this could be done via a broadcast)
     *   The handle is deserialized after the deletion timeout set via property (default 3 sec) has passed.
     * This scenario is possible but should not happen on a stable LAN if ice objects are not keept in a serialized form for a longer period.
     *
     * (for more info: \ref ArmarXCore-Tutorials-RemoteHandles "RemoteHandle tutorial")
     *
     * \ingroup Distributed
     */
    class RemoteHandleControlBlock:
        virtual public RemoteHandleControlBlockInterface
    {
    public:
        using clock = std::chrono::steady_clock;
        inline static int64_t now();
        /**
         * @brief Returned by a call to RemoteHandleControlBlock::create.
         * It holds a direct handle to the controlblock (can be used to keep the RemoteHandleControlBlock alive (but not the managed object)) (this pointer may be deleted).
         * And The ClientSideRemoteHandleControlBlock keeping the managed object alive. (send this to the client)
         */
        struct ManagementData
        {
            RemoteHandleControlBlockPtr directHandle;
            ClientSideRemoteHandleControlBlockBasePtr clientSideRemoteHandleControlBlock;
        };

        /**
         * @brief Creates a new RemoteHandleControlBlock.
         * @param objectAdapter The adapter used to register the RemoteHandleControlBlock to ice.
         * @param managedObjectPrx A Proxy to the managed object.
         * @param deleter A functor cleaning up the managed object. (has to delete it and unregister it)
         * The Functor may have the only pointer keeping the managed object alive.
         * @return A direct pointer to the created RemoteHandleControlBlock and the ClientSideRemoteHandleControlBlock keeping the managed object alive.
         */
        template<class Deleter>
        static ManagementData create(ArmarXManagerPtr manager, Ice::ObjectPrx managedObjectPrx, Deleter deleter);

        /**
         * @brief Increments the usecount.
         * DO NOT CALL IT. (if you do, you are responsible to call decrementUseCount!)
         */
        inline void incrementUseCount(const Ice::Current& = Ice::Current()) override;
        /**
         * @brief decrements the useCount.
         * DO NOT CALL IT. (except you have called incrementUseCount)
         */
        void decrementUseCount(const Ice::Current& = Ice::Current()) override;
        /**
         * @return Returns the current use count.
         */
        inline Ice::Long  getUseCount(const Ice::Current& = Ice::Current()) const override;

        /**
         * @return A new ClientSideRemoteHandleControlBlock.
         */
        ClientSideRemoteHandleControlBlockBasePtr getClientSideRemoteHandleControlBlock(const Ice::Current& = Ice::Current()) override;
        inline Ice::ObjectPrx getManagedObjectProxy(const Ice::Current& = Ice::Current()) const override;

        /**
         * @brief Call this when you want to force the deletion of the managed object.
         * It will be deleted ignoring the usecount.
         * This may be used to ensure the deletion of all objects created by yout server when shutting down.
         * This may be necessary if a client's decrementUseCount was lost by the network.
         */
        void forceDeletion()
        {
            forcedDeletion = true;
        }
    private:
        RemoteHandleControlBlock(Ice::ObjectAdapterPtr objectAdapter, Ice::ObjectPrx managedObjectPrx, std::function<void(void)> deleter);
        RemoteHandleControlBlock(const RemoteHandleControlBlock&) = delete;
        RemoteHandleControlBlock(RemoteHandleControlBlock&&) = delete;

        /**
         * @brief Does all cleanup for this object.
         */
        inline void cleanup();

        /**
         * @brief The adapter used to register this RemoteHandleControlBlock
         */
        Ice::ObjectAdapterPtr objectAdapter;
        std::function<void(void)> deleter;
        Ice::ObjectPrx managedObjectProxy;
        /**
         * @brief A proxy to this RemoteHandleControlBlock (used when creating a new ClientSideRemoteHandleControlBlock)
         */
        RemoteHandleControlBlockInterfacePrx selfProxy;

        mutable std::atomic<uint64_t> useCount {0};
        mutable std::mutex mtx;
        mutable clock::time_point lastTimeUseCountReachedZero;
        mutable std::atomic_bool forcedDeletion {false};

        friend struct SharedRemoteHandleState;
    };

    void RemoteHandleControlBlock::incrementUseCount(const Ice::Current&)
    {
        ++useCount;
    }

    Ice::Long RemoteHandleControlBlock::getUseCount(const Ice::Current&) const
    {
        return useCount;
    }

    Ice::ObjectPrx RemoteHandleControlBlock::getManagedObjectProxy(const Ice::Current&) const
    {
        return managedObjectProxy;
    }

    void RemoteHandleControlBlock::cleanup()
    {
        //delete payload
        deleter();
        deleter = []() {}; //do this to delete objects keept alive by the deleter (it may hold a unique_ptr)
        //clean up proxy
        objectAdapter->remove(selfProxy->ice_getIdentity());
    }

    template<class Deleter>
    RemoteHandleControlBlock::ManagementData RemoteHandleControlBlock::create(ArmarXManagerPtr manager, Ice::ObjectPrx managedObjectPrx, Deleter deleter)
    {
        RemoteHandleControlBlockPtr block = new RemoteHandleControlBlock {manager->getAdapter(), std::move(managedObjectPrx), std::move(deleter)};
        auto clientSideRemoteHandleControlBlock = block->getClientSideRemoteHandleControlBlock();
        manager->getSharedRemoteHandleState()->add(block);
        return {block, clientSideRemoteHandleControlBlock};
    }
}
