/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
* @package    ArmarXCore
* @author     Raphael Grimm ( ufdrv at student dot kit dot edu)
* @date       2016
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/
#pragma once

#include <Ice/ProxyHandle.h>

#include <ArmarXCore/interface/core/util/distributed/RemoteHandle/RemoteHandleControlBlock.h>
#include <ArmarXCore/interface/core/util/distributed/RemoteHandle/ClientSideRemoteHandleControlBlock.h>
#include "../../TemplateMetaProgramming.h"

/**
 * \defgroup Distributed
 * \ingroup core-utility
 */
namespace armarx
{
    /**
     * @brief The RemoteHandle class wrapps a ClientSideRemoteHandleControlBlock and can be used just as a Ice proxy.
     * It can be copied, casted, assigned, compared and dereferenced.
     * (for more info: \ref ArmarXCore-Tutorials-RemoteHandles "RemoteHandle tutorial")
     * \ingroup Distributed
     */
    template<class PrxType>
    class RemoteHandle
    {
        static_assert(meta::TypeTemplateTraits::IsInstanceOf<IceInternal::ProxyHandle, PrxType>::value, "PrxType has to be an Ice proxy");
    public:
        //constructing
        RemoteHandle() = default;
        RemoteHandle(const RemoteHandle& other) = default;
        RemoteHandle(RemoteHandle&& other) = default;
        RemoteHandle(const ClientSideRemoteHandleControlBlockBasePtr& controlBlock);
        RemoteHandle(std::nullptr_t);
        template<class PrxTypeB> RemoteHandle(const RemoteHandle<PrxTypeB>& other);
        ~RemoteHandle() = default;

        //assigning
        inline RemoteHandle<PrxType>& operator =(const RemoteHandle<PrxType>& other) = default;
        inline RemoteHandle<PrxType>& operator =(RemoteHandle<PrxType>&& other) = default;
        inline RemoteHandle<PrxType>& operator =(const ClientSideRemoteHandleControlBlockBasePtr& controlBlock);
        inline RemoteHandle<PrxType>& operator =(std::nullptr_t);
        template<class PrxTypeB>
        inline RemoteHandle<PrxType>& operator =(const RemoteHandle<PrxTypeB>& other);

        //casting
        template<class SourceType>
        inline static RemoteHandle<PrxType> uncheckedCast(const SourceType& proxyToCast);

        template<class SourceType>
        inline static RemoteHandle<PrxType> checkedCast(const SourceType& proxyToCast);

        template<class TargetPrxType> inline RemoteHandle<TargetPrxType> uncheckedCast() const;
        template<class TargetPrxType> inline RemoteHandle<TargetPrxType>   checkedCast() const;

        explicit inline operator bool() const;

        //access
        inline       PrxType operator *();
        inline const PrxType operator *() const;
        inline       PrxType operator ->();
        inline const PrxType operator ->() const;
        inline       PrxType get();
        inline const PrxType get() const;

        //compare
        template<class PrxTA, class PrxTB> friend bool operator==(const RemoteHandle<PrxTA>& fst, const RemoteHandle<PrxTB>& snd);
        template<class PrxTA, class PrxTB> friend bool operator!=(const RemoteHandle<PrxTA>& fst, const RemoteHandle<PrxTB>& snd);
        template<class PrxTA, class PrxTB> friend bool operator< (const RemoteHandle<PrxTA>& fst, const RemoteHandle<PrxTB>& snd);
        template<class PrxTA, class PrxTB> friend bool operator<=(const RemoteHandle<PrxTA>& fst, const RemoteHandle<PrxTB>& snd);
        template<class PrxTA, class PrxTB> friend bool operator >(const RemoteHandle<PrxTA>& fst, const RemoteHandle<PrxTB>& snd);
        template<class PrxTA, class PrxTB> friend bool operator>=(const RemoteHandle<PrxTA>& fst, const RemoteHandle<PrxTB>& snd);

        /**
         * @return Returns the internal ClientSideRemoteHandleControlBlock.
         * This should only be used when it has to be send per ice.
         */
        inline const IceInternal::Handle<const ClientSideRemoteHandleControlBlockBase> getclientSideControlBlock() const;

    private:
        template<class PrxTA> friend class armarx::RemoteHandle;

        PrxType objectProxy;
        ClientSideRemoteHandleControlBlockBasePtr clientSideControlBlock;
    };

    template<class PrxType>
    RemoteHandle<PrxType>::RemoteHandle(const ClientSideRemoteHandleControlBlockBasePtr& controlBlock):
        objectProxy {controlBlock ? PrxType::checkedCast(controlBlock->getManagedObjectProxy()) : nullptr},
        clientSideControlBlock {objectProxy ? controlBlock : nullptr}
    {
        assert(objectProxy ? clientSideControlBlock : !clientSideControlBlock);
    }

    template<class PrxType> template<class PrxTypeB>
    RemoteHandle<PrxType>::RemoteHandle(const RemoteHandle<PrxTypeB>& other): RemoteHandle(other.clientSideControlBlock)
    {
    }

    template<class PrxType>
    RemoteHandle<PrxType>& RemoteHandle<PrxType>::operator =(const ClientSideRemoteHandleControlBlockBasePtr& controlBlock)
    {
        objectProxy = controlBlock ? PrxType::checkedCast(controlBlock->getManagedObjectProxy()) : nullptr;
        clientSideControlBlock = objectProxy ? controlBlock : nullptr;
        return *this;
    }

    template<class PrxType>
    RemoteHandle<PrxType>& RemoteHandle<PrxType>::operator =(std::nullptr_t)
    {
        objectProxy = nullptr;
        clientSideControlBlock = nullptr;
        return *this;
    }

    template<class PrxType> template<class PrxTypeB>
    RemoteHandle<PrxType>& RemoteHandle<PrxType>::operator =(const RemoteHandle<PrxTypeB>& other)
    {
        this = other.clientSideControlBlock;
        return this;
    }

    template<class PrxType>
    RemoteHandle<PrxType>::RemoteHandle(std::nullptr_t): RemoteHandle {} {}

    template<class PrxType> template<class SourceType>
    RemoteHandle<PrxType> RemoteHandle<PrxType>::uncheckedCast(const SourceType& proxyToCast)
    {
        static_assert(meta::TypeTemplateTraits::IsInstanceOf<::armarx::RemoteHandle, SourceType>::value, "proxyToCast has to be a RemoteHandle");
        return proxyToCast.template uncheckedCast<PrxType>();
    }

    template<class PrxType> template<class SourceType>
    RemoteHandle<PrxType> RemoteHandle<PrxType>::checkedCast(const SourceType& proxyToCast)
    {
        static_assert(meta::TypeTemplateTraits::IsInstanceOf<::armarx::RemoteHandle, SourceType>::value, "proxyToCast has to be a RemoteHandle");
        return proxyToCast.template checkedCast<PrxType>();
    }

    template<class PrxType> template<class TargetPrxType>
    RemoteHandle<TargetPrxType> RemoteHandle<PrxType>::uncheckedCast() const
    {
        static_assert(meta::TypeTemplateTraits::IsInstanceOf<IceInternal::ProxyHandle, TargetPrxType>::value, "TargetPrxType has to be an Ice proxy");
        RemoteHandle<TargetPrxType> casted;
        casted.clientSideControlBlock = clientSideControlBlock;
        casted.objectProxy = TargetPrxType::uncheckedCast(objectProxy);
        return casted;
    }

    template<class PrxType> template<class TargetPrxType>
    RemoteHandle<TargetPrxType> RemoteHandle<PrxType>::checkedCast() const
    {
        static_assert(meta::TypeTemplateTraits::IsInstanceOf<IceInternal::ProxyHandle, TargetPrxType>::value, "TargetPrxType has to be an Ice proxy");
        RemoteHandle<TargetPrxType> casted;
        casted.objectProxy = TargetPrxType::checkedCast(objectProxy);
        casted.clientSideControlBlock = casted.objectProxy ? clientSideControlBlock : nullptr;
        return casted;
    }

    template<class PrxType> PrxType RemoteHandle<PrxType>::operator *()
    {
        return objectProxy;
    }
    template<class PrxType> const PrxType RemoteHandle<PrxType>::operator *() const
    {
        return objectProxy;
    }
    template<class PrxType> PrxType RemoteHandle<PrxType>::operator ->()
    {
        return objectProxy;
    }
    template<class PrxType> const PrxType RemoteHandle<PrxType>::operator ->() const
    {
        return objectProxy;
    }
    template<class PrxType> PrxType RemoteHandle<PrxType>::get()
    {
        return objectProxy;
    }
    template<class PrxType> const PrxType RemoteHandle<PrxType>::get() const
    {
        return objectProxy;
    }

    template<class PrxType> RemoteHandle<PrxType>::operator bool() const
    {
        return objectProxy;
    }

    template<class PrxType>
    const IceInternal::Handle<const ClientSideRemoteHandleControlBlockBase> RemoteHandle<PrxType>::getclientSideControlBlock() const
    {
        return clientSideControlBlock;
    }

    template<class PrxTA, class PrxTB> bool operator==(const RemoteHandle<PrxTA>& fst, const RemoteHandle<PrxTB>& snd)
    {
        return fst.clientSideControlBlock->getManagedObjectProxy() == snd.clientSideControlBlock->getManagedObjectProxy();
    }
    template<class PrxTA, class PrxTB> bool operator!=(const RemoteHandle<PrxTA>& fst, const RemoteHandle<PrxTB>& snd)
    {
        return fst.clientSideControlBlock->getManagedObjectProxy() != snd.clientSideControlBlock->getManagedObjectProxy();
    }
    template<class PrxTA, class PrxTB> bool operator<(const RemoteHandle<PrxTA>& fst, const RemoteHandle<PrxTB>& snd)
    {
        return fst.clientSideControlBlock->getManagedObjectProxy() <  snd.clientSideControlBlock->getManagedObjectProxy();
    }
    template<class PrxTA, class PrxTB> bool operator<=(const RemoteHandle<PrxTA>& fst, const RemoteHandle<PrxTB>& snd)
    {
        return fst.clientSideControlBlock->getManagedObjectProxy() <= snd.clientSideControlBlock->getManagedObjectProxy();
    }
    template<class PrxTA, class PrxTB> bool operator>(const RemoteHandle<PrxTA>& fst, const RemoteHandle<PrxTB>& snd)
    {
        return fst.clientSideControlBlock->getManagedObjectProxy() >  snd.clientSideControlBlock->getManagedObjectProxy();
    }
    template<class PrxTA, class PrxTB> bool operator>=(const RemoteHandle<PrxTA>& fst, const RemoteHandle<PrxTB>& snd)
    {
        return fst.clientSideControlBlock->getManagedObjectProxy() >= snd.clientSideControlBlock->getManagedObjectProxy();
    }
}
