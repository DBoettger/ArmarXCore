/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu)
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include <ctime>

#include "FileSystemPathBuilder.h"
#include "../system/cmake/CMakePackageFinder.h"
#include "../exceptions/local/ExpressionException.h"
#include "Preprocessor.h"

namespace armarx
{
    const boost::regex FileSystemPathBuilder::RawPathRegex
    {
        "([^{}]+|\\{[^{}]+\\})+"
    };

    std::map<std::string, FileSystemPathBuilder::FormatStringOption> FileSystemPathBuilder::GetFormatStringOptions()
    {
        std::map<std::string, FormatStringOption> descr;
        for (const auto& key : FileSystemPathBuilderFormatStringOptionRegistrar::getKeys())
        {
            descr[key] = FileSystemPathBuilderFormatStringOptionRegistrar::get(key);
        }
        return descr;
    }

    std::string FileSystemPathBuilder::ApplyFormatting(const std::string& rawPath)
    {
        ARMARX_CHECK_EXPRESSION_W_HINT(
            boost::regex_match(rawPath, RawPathRegex),
            VAROUT(rawPath));

        std::stringstream strstr;

        boost::regex expression {"([^{}]+|\\{([^{}]+)\\})"};
        std::string::const_iterator start = rawPath.begin();
        const std::string::const_iterator end = rawPath.end();
        boost::match_results<std::string::const_iterator> match;
        while (boost::regex_search(start, end, match, expression))
        {
            if (match[2].matched)
            {
                const std::string matched
                {
                    match[2].first, match[2].second
                };
                bool hit = false;
                for (const auto& key : FileSystemPathBuilderFormatStringOptionRegistrar::getKeys())
                {
                    const FormatStringOption& opt = FileSystemPathBuilderFormatStringOptionRegistrar::get(key);
                    if (boost::regex_match(matched, opt.patternRegex))
                    {
                        strstr << opt.patternReplacer(matched);
                        hit = true;
                        break;
                    }
                }
                ARMARX_CHECK_EXPRESSION_W_HINT(hit, "pattern '{" + matched + "}' did not match any known  FormatString");
            }
            else
            {
                strstr << match[0];
            }
            //advance
            start = match[0].second;
        }
        return strstr.str();
    }

    FileSystemPathBuilder::RegisterFormatStringOption::RegisterFormatStringOption(
        std::string name, boost::regex patternRegex, std::function<std::string(const std::string&)> replacer, std::string description)
    {
        FileSystemPathBuilderFormatStringOptionRegistrar::registerElement(
            name, {name, std::move(description), std::move(patternRegex), std::move(replacer)});
    }

    //chars
    FileSystemPathBuilder::RegisterFormatStringOption ARMARX_ANONYMOUS_VARIABLE
    {
        "LEFT CURLY",
        boost::regex{"LEFT CURLY"},
        [](const std::string&){return "{";},
        "prints {"
    };
    FileSystemPathBuilder::RegisterFormatStringOption ARMARX_ANONYMOUS_VARIABLE
    {
        "RIGHT CURLY",
        boost::regex{"RIGHT CURLY"},
        [](const std::string&){return "}";},
        "prints }"
    };
    //date&time
    FileSystemPathBuilder::RegisterFormatStringOption ARMARX_ANONYMOUS_VARIABLE
    {
        "year",
        boost::regex{"year"},
        [](const std::string&)
        {
            std::time_t t = std::time(nullptr);
            char buff[8];
            ARMARX_CHECK_EXPRESSION(std::strftime(buff, sizeof(buff), "%y", std::localtime(&t)));
            return std::string {buff};
        },
        "year (last two digits)"
    };
    FileSystemPathBuilder::RegisterFormatStringOption ARMARX_ANONYMOUS_VARIABLE
    {
        "Year",
        boost::regex{"Year"},
        [](const std::string&)
        {
            std::time_t t = std::time(nullptr);
            char buff[5];
            ARMARX_CHECK_EXPRESSION(std::strftime(buff, sizeof(buff), "%Y", std::localtime(&t)));
            return std::string {buff};
        },
        "Year (all four digits)"
    };
    FileSystemPathBuilder::RegisterFormatStringOption ARMARX_ANONYMOUS_VARIABLE
    {
        "month",
        boost::regex{"month"},
        [](const std::string&)
        {
            std::time_t t = std::time(nullptr);
            char buff[3];
            ARMARX_CHECK_EXPRESSION(std::strftime(buff, sizeof(buff), "%m", std::localtime(&t)));
            return std::string {buff};
        },
        "month (01-12)"
    };
    FileSystemPathBuilder::RegisterFormatStringOption ARMARX_ANONYMOUS_VARIABLE
    {
        "day",
        boost::regex{"day"},
        [](const std::string&)
        {
            std::time_t t = std::time(nullptr);
            char buff[3];
            ARMARX_CHECK_EXPRESSION(std::strftime(buff, sizeof(buff), "%d", std::localtime(&t)));
            return std::string {buff};
        },
        "day (01-31)"
    };
    FileSystemPathBuilder::RegisterFormatStringOption ARMARX_ANONYMOUS_VARIABLE
    {
        "hour",
        boost::regex{"hour"},
        [](const std::string&)
        {
            std::time_t t = std::time(nullptr);
            char buff[3];
            ARMARX_CHECK_EXPRESSION(std::strftime(buff, sizeof(buff), "%H", std::localtime(&t)));
            return std::string {buff};
        },
        "hour (00-23)"
    };
    FileSystemPathBuilder::RegisterFormatStringOption ARMARX_ANONYMOUS_VARIABLE
    {
        "minute",
        boost::regex{"minute"},
        [](const std::string&)
        {
            std::time_t t = std::time(nullptr);
            char buff[3];
            ARMARX_CHECK_EXPRESSION(std::strftime(buff, sizeof(buff), "%M", std::localtime(&t)));
            return std::string {buff};
        },
        "minute (00-59)"
    };
    FileSystemPathBuilder::RegisterFormatStringOption ARMARX_ANONYMOUS_VARIABLE
    {
        "second",
        boost::regex{"second"},
        [](const std::string&)
        {
            std::time_t t = std::time(nullptr);
            char buff[3];
            ARMARX_CHECK_EXPRESSION(std::strftime(buff, sizeof(buff), "%S", std::localtime(&t)));
            return std::string {buff};
        },
        "second (00-59)"
    };
    FileSystemPathBuilder::RegisterFormatStringOption ARMARX_ANONYMOUS_VARIABLE
    {
        "Time",
        boost::regex{"Time"},
        [](const std::string&)
        {
            std::time_t t = std::time(nullptr);
            char buff[9];
            ARMARX_CHECK_EXPRESSION(std::strftime(buff, sizeof(buff), "%H-%M-%S", std::localtime(&t)));
            return std::string {buff};
        },
        "HH-MM-SS"
    };
    FileSystemPathBuilder::RegisterFormatStringOption ARMARX_ANONYMOUS_VARIABLE
    {
        "Date",
        boost::regex{"Date"},
        [](const std::string&)
        {
            std::time_t t = std::time(nullptr);
            char buff[11];
            ARMARX_CHECK_EXPRESSION(std::strftime(buff, sizeof(buff), "%Y-%m-%d", std::localtime(&t)));
            return std::string {buff};
        },
        "YYYY-MM-DD"
    };
    FileSystemPathBuilder::RegisterFormatStringOption ARMARX_ANONYMOUS_VARIABLE
    {
        "DateTime",
        boost::regex{"DateTime"},
        [](const std::string&)
        {
            std::time_t t = std::time(nullptr);
            char buff[20];
            ARMARX_CHECK_EXPRESSION(std::strftime(buff, sizeof(buff), "%Y-%m-%d_%H-%M-%S", std::localtime(&t)));
            return std::string {buff};
        },
        "YYYY-MM-DD_HH-MM-SS"
    };
    FileSystemPathBuilder::RegisterFormatStringOption ARMARX_ANONYMOUS_VARIABLE
    {
        "time-since-epoch",
        boost::regex{"time-since-epoch"},
        [](const std::string&)
        {
            return to_string(std::chrono::high_resolution_clock::now().time_since_epoch().count());
        },
        "time since epoch in nanoseconds (can be used as UUID)"
    };
    //package paths
    FileSystemPathBuilder::RegisterFormatStringOption ARMARX_ANONYMOUS_VARIABLE
    {
        "PackageDir",
        boost::regex{"PackageDir:.+"},
        [](const std::string & s)
        {
            const auto pkg = s.substr(11);
            CMakePackageFinder pf {pkg};
            ARMARX_CHECK_EXPRESSION_W_HINT(pf.packageFound(), "package '" + pkg + "' not found!");
            return pf.getPackageDir();
        },
        "PackageDir:<PACKAGE>: Package dir of the armarx Package <PACKAGE>."
    };
    FileSystemPathBuilder::RegisterFormatStringOption ARMARX_ANONYMOUS_VARIABLE
    {
        "ScenarioDir",
        boost::regex{"ScenarioDir:.+"},
        [](const std::string & s)
        {
            const auto pkg = s.substr(12);
            CMakePackageFinder pf {pkg};
            ARMARX_CHECK_EXPRESSION_W_HINT(pf.packageFound(), "package '" + pkg + "' not found!");
            return pf.getScenariosDir();
        },
        "ScenarioDir:<PACKAGE>: Scenario dir of the armarx Package <PACKAGE>."
    };
    FileSystemPathBuilder::RegisterFormatStringOption ARMARX_ANONYMOUS_VARIABLE
    {
        "DataDir",
        boost::regex{"DataDir:.+"},
        [](const std::string & s)
        {
            const auto pkg = s.substr(8);
            CMakePackageFinder pf {pkg};
            ARMARX_CHECK_EXPRESSION_W_HINT(pf.packageFound(), "package '" + pkg + "' not found!");
            return pf.getDataDir();
        },
        "DataDir:<PACKAGE>: Data dir of the armarx Package <PACKAGE>."
    };
    FileSystemPathBuilder::RegisterFormatStringOption ARMARX_ANONYMOUS_VARIABLE
    {
        "BuildDir",
        boost::regex{"BuildDir:.+"},
        [](const std::string & s)
        {
            const auto pkg = s.substr(9);
            CMakePackageFinder pf {pkg};
            ARMARX_CHECK_EXPRESSION_W_HINT(pf.packageFound(), "package '" + pkg + "' not found!");
            return pf.getBuildDir();
        },
        "BuildDir:<PACKAGE>: Build dir of the armarx Package <PACKAGE>."
    };
    FileSystemPathBuilder::RegisterFormatStringOption ARMARX_ANONYMOUS_VARIABLE
    {
        "BinaryDir",
        boost::regex{"BinaryDir:.+"},
        [](const std::string & s)
        {
            const auto pkg = s.substr(10);
            CMakePackageFinder pf {pkg};
            ARMARX_CHECK_EXPRESSION_W_HINT(pf.packageFound(), "package '" + pkg + "' not found!");
            return pf.getBinaryDir();
        },
        "BinaryDir:<PACKAGE>: Binary dir of the armarx Package <PACKAGE>."
    };
    FileSystemPathBuilder::RegisterFormatStringOption ARMARX_ANONYMOUS_VARIABLE
    {
        "CMakeDir",
        boost::regex{"CMakeDir:.+"},
        [](const std::string & s)
        {
            const auto pkg = s.substr(9);
            CMakePackageFinder pf {pkg};
            ARMARX_CHECK_EXPRESSION_W_HINT(pf.packageFound(), "package '" + pkg + "' not found!");
            return pf.getCMakeDir();
        },
        "CMakeDir:<PACKAGE>: CMake dir of the armarx Package <PACKAGE>."
    };
    FileSystemPathBuilder::RegisterFormatStringOption ARMARX_ANONYMOUS_VARIABLE
    {
        "SourceDir",
        boost::regex{"SourceDir:.+"},
        [](const std::string & s)
        {
            const auto pkg = s.substr(10);
            CMakePackageFinder pf {pkg};
            ARMARX_CHECK_EXPRESSION_W_HINT(pf.packageFound(), "package '" + pkg + "' not found!");
            return pf.getPackageDir() + "/source/" + pkg;
        },
        "SourceDir:<PACKAGE>: CMake dir of the armarx Package <PACKAGE>."
    };

}
