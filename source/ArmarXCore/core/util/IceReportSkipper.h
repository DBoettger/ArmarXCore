/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2017, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarX
 * @author     Mirko Waechter( mirko.waechter at kit dot edu)
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <Ice/Current.h>
#include <IceUtil/Time.h>
#include <map>
#include <ArmarXCore/core/system/Synchronization.h>

#include <boost/optional.hpp>

namespace armarx
{

    class IceReportSkipper
    {
    public:
        IceReportSkipper(float maxFrequency);

        /**
         * @brief checks the frequency and returns true if the minimum required time for the frequency has passed.
         * Usage:
         \code
            if(!skipper.checkFrequency())
            {
                // skip
                return;
            }
            // do your calculations
         \endcode
         * @param c
         * @param frequency
         * @return
         */
        bool checkFrequency(const Ice::Current& c, const boost::optional<float>& frequency = boost::optional<float>());
        bool checkFrequency(const std::string& operationName, const boost::optional<float>& frequency = boost::optional<float>());

    private:
        struct SkippingData
        {
            IceUtil::Time lastTimestamp;
            float maxFrequency;
        };

        std::map<std::string, SkippingData> operationData;
        float maxFrequency;
        Mutex mutex;
    };

} // namespace armarx

