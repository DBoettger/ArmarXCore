/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarX
 * @author     Mirko Waechter( mirko.waechter at kit dot edu)
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "StringHelpers.h"
#include <boost/algorithm/string/trim.hpp>
#include <boost/algorithm/string/split.hpp>
#include <boost/algorithm/string/classification.hpp>
#include <boost/algorithm/string/case_conv.hpp>
std::vector<std::string> armarx::Split(const std::string& source, const std::string& splitBy, bool trimElements, bool removeEmptyElements)
{
    std::vector<std::string> result;

    if (source.empty())
    {
        return result;
    }

    boost::split(result, source, boost::is_any_of(splitBy));
    if (trimElements)
    {
        for (auto& e : result)
        {
            boost::trim(e);
        }
    }
    if (removeEmptyElements)
    {
        std::vector<std::string> tmp;
        tmp.reserve(result.size());
        for (auto& e : result)
        {
            if (!e.empty())
            {
                tmp.emplace_back(std::move(e));
            }
        }
        return tmp;
    }
    return result;
}

bool armarx::Contains(const std::string& haystack, const std::string& needle, bool caseInsensitive)
{
    std::string haystackToSearch = (caseInsensitive) ? boost::algorithm::to_lower_copy(haystack) : haystack;
    std::string needleToSearchFor = (caseInsensitive) ? boost::algorithm::to_lower_copy(needle) : needle;

    return haystackToSearch.find(needleToSearchFor) != std::string::npos;
}



std::string armarx::Encode(const std::string& data)
{
    std::string buffer;
    buffer.reserve(data.size());
    for (size_t pos = 0; pos != data.size(); ++pos)
    {
        switch (data[pos])
        {
            case '&':
                buffer.append("&amp;");
                break;
            case '\"':
                buffer.append("&quot;");
                break;
            case '\'':
                buffer.append("&apos;");
                break;
            case '<':
                buffer.append("&lt;");
                break;
            case '>':
                buffer.append("&gt;");
                break;
            default:
                buffer.append(&data[pos], 1);
                break;
        }
    }
    return buffer;
}


float armarx::toFloat(const std::string& input)
{
    std::stringstream ss;
    float result;
    ss.imbue(std::locale::classic());
    ss << input;
    ss >> result;
    return result;
}
int armarx::toInt(const std::string& input)
{
    std::stringstream ss;
    int result;
    ss.imbue(std::locale::classic());
    ss << input;
    ss >> result;
    return result;
}
