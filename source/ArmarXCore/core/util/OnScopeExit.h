/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore
 * @author     Raphael Grimm ( ufdrv at student dot kit dot edu)
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <utility>

#include "Preprocessor.h"

/**
 * \page OnScopeExitGuardsDoc Execution of code when a scope is left
 *
 * You can execute code when the enclosing scope is left by using the macro \ref ARMARX_ON_SCOPE_EXIT.
 * Later macro instantiations are executed first (order of c++ stack unwinding).
 *
 * \code{.cpp}
 * {
 *     ARMARX_ON_SCOPE_EXIT
 *     {
 *         std::cout<<"bar";
 *     }
 *     ARMARX_ON_SCOPE_EXIT
 *     {
 *         std::cout<<"foo";
 *     }
 *     std::cout << "baz"; //prints "baz"
 * }//prints "foo" then "bar"
 * \endcode
 * This code prints "bazfoobar" on std::cout. ("foobar" is printed when the scope is left).
 *
 * \defgroup OnScopeExitGuards
 * \ingroup core-utility
 */

namespace armarx
{
    namespace detail
    {
        /**
         * @brief Tag used by the macro ARMARX_ON_SCOPE_EXIT;
         *
         * \ingroup OnScopeExitGuards
         */
        struct ScopeGuardOnExit {};

        /**
         * @brief Executes a given function on scope exit.
         * Use with the macro ARMARX_ON_SCOPE_EXIT;
         *
         * \ingroup OnScopeExitGuards
         */
        template <typename FunctionType>
        class ScopeGuard
        {
        public:
            explicit ScopeGuard(const FunctionType& fn) : function(fn) {}

            explicit ScopeGuard(FunctionType&& fn) : function(std::move(fn)) {}

            ~ScopeGuard()
            {
                function();
            }
        private:
            FunctionType function;
        };

        template <typename Fun>
        ScopeGuard<Fun> operator+(ScopeGuardOnExit, Fun&& fn)
        {
            return ScopeGuard<Fun>(std::forward<Fun>(fn));
        }
    }
}
/**
 *
 */

/**
 * @brief Executes given code when the enclosing scope is left. (later macro instantiations are executed first (order of stack unwinding))
 *
 * \code{.cpp}
 * {
 *     ARMARX_ON_SCOPE_EXIT
 *     {
 *         std::cout<<"bar";
 *     }
 *     ARMARX_ON_SCOPE_EXIT
 *     {
 *         std::cout<<"foo";
 *     }
 * }
 * \endcode
 * prints foobar on std::cout.
 *
 * \ingroup OnScopeExitGuards
 */
#define ARMARX_ON_SCOPE_EXIT auto ARMARX_ANONYMOUS_VARIABLE_WITH_PREFIX(SCOPE_EXIT_GUARD) = ::armarx::detail::ScopeGuardOnExit() + [&]()

