/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore
 * @author     Raphael Grimm ( raphael dot grimm at student dot kit dot edu)
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <type_traits>

#include "TemplateMetaProgramming.h"

namespace armarx
{
    /**
     * @brief Wrapper for a pointer to propagate const to the pointed to value.
     * \url{http://en.cppreference.com/w/cpp/experimental/propagate_const}
     * Only difference: the armarx coding style is used (CamelCase instead of snake case) and
     * some constexpr were deleted to work with gcc4.8
     *
     * \code{cpp}
            struct T
            {
                void f()       {std::cout<<"non const\n";}
                void f() const {std::cout<<"    const\n";}
            };

            T inst;

            struct Test
            {
                T * nc{&inst};
                const T * c{&inst};
                armarx::PropagateConst<T *> pc{&inst};
            };

            int main()
            {
                Test t;
                t.nc->f(); // non const
                t. c->f(); //     const
                t.pc->f(); // non const

                const Test tc;
                tc.nc->f();// non const
                tc. c->f();//     const
                tc.pc->f();//     const
            }
     * \endcode
     */
    template <class Wrapped>
    class PropagateConst
    {
        Wrapped wrapped{nullptr};
    public:
        /* From cpp reference:
         *      T must be an object pointer type or a pointer-like class type, as specified below.
         *      The program is ill-formed if T is an array type, reference type, pointer to function
         *      type, pointer to (possibly cv-qualified) void, or if decltype(*std::declval<T&>())
         *      is not an lvalue reference type.
         */
        static_assert(
            !std::is_array<Wrapped>::value&&  //must not be array
            (
                std::is_class<Wrapped>::value || //e.g.: unique_ptr
                std::is_pointer<Wrapped>::value  //e.g.: int*
            )&&
            std::is_object<typename std::remove_pointer<Wrapped>::type>::value, //is either unique_ptr or int*, but not void*
            "PropagateConst requires Wrapped to be a ptr type"
        );

        using ElementType = typename std::remove_reference<decltype(*std::declval<Wrapped&>())>::type;

        //ctors
    public:
        constexpr PropagateConst() = default;
        constexpr PropagateConst(PropagateConst&&) = default;

        /* cppref:
         * This constructor does not participate in overload resolution unless
         * std::is_constructible<T, U&&>::value is true,
         * and is explicit if and only if std::is_convertible<U&&, T>::value is false.
         */
        template <
            class T,
            typename std::enable_if <
                std::is_constructible < Wrapped, T&& >::value&&
                std::is_convertible < T&&, Wrapped >::value,
                bool
                >::type = true
            >
        constexpr PropagateConst(PropagateConst<T>&& p) : wrapped(std::move(GetUnderlying(p))) {}
        template <
            class T,
            typename std::enable_if <
                std::is_constructible < Wrapped, T&& >::value&&
                !std::is_convertible < T&&, Wrapped >::value,
                bool
                >::type = true
            >
        constexpr explicit PropagateConst(PropagateConst<T>&& p) : wrapped(std::move(GetUnderlying(p))) {}

        /* cppref:
         * This constructor does not participate in overload resolution
         * unless std::is_constructible<T, U&&>::value is true and
         * std::decay_t<U> is not a specialization of propagate_const.
         * This constructor is explicit if and only if std::is_convertible<U&&, T>::value is false.
         */
        template <
            class T,
            typename std::enable_if <
                std::is_constructible < Wrapped, T&& >::value&&
                std::is_convertible < T&&, Wrapped >::value&&
                !meta::TypeTemplateTraits::IsInstanceOf<PropagateConst, typename std::decay<T>::type>::value,
                bool
                >::type = true
            >
        constexpr PropagateConst(T && p) : wrapped(std::forward<T>(p)) {}

        template <
            class T,
            typename std::enable_if <
                std::is_constructible < Wrapped, T&& >::value&&
                !std::is_convertible < T&&, Wrapped >::value&&
                !meta::TypeTemplateTraits::IsInstanceOf<PropagateConst, typename std::decay<T>::type>::value,
                bool
                >::type = true
            >
        constexpr explicit PropagateConst(T && p) : wrapped(std::forward<T>(p)) {}


        constexpr PropagateConst(Wrapped& p) : wrapped(p) {}

        //operator=
    public:
        PropagateConst& operator=(PropagateConst&&) = default;

        template <
            class T,
            typename std::enable_if < std::is_convertible < T&&, Wrapped >::value, bool >::type = true
            >
        constexpr PropagateConst & operator=(PropagateConst<T>&& p)
        {
            wrapped = std::move(GetUnderlying(p));
            return *this;
        }

        template <
            class T,
            typename std::enable_if <
                std::is_convertible < T&&, Wrapped >::value&&
                !meta::TypeTemplateTraits::IsInstanceOf<PropagateConst, typename std::decay<T>::type>::value,
                bool
                >::type = true
            >
        constexpr PropagateConst & operator=(T && p)
        {
            wrapped = std::forward<T>(p);
            return *this;
        }

        //swap
        constexpr void swap(PropagateConst& pt) noexcept(noexcept(swap(std::declval<Wrapped&>(), std::declval<Wrapped&>())))
        {
            using std::swap;
            swap(wrapped, GetUnderlying(pt));
        }
        //observers
        //get
    public:
        const ElementType* get() const
        {
            return Get(wrapped);
        }
        ElementType* get()
        {
            return Get(wrapped);
        }
    private:
        template<class T>    static constexpr       ElementType*    Get(T* p)
        {
            return p;
        }
        template<class T>    static constexpr       ElementType*    Get(T& p)
        {
            return p.get();
        }
        template<class T>    static constexpr const ElementType*    Get(const T* p)
        {
            return p;
        }
        template<class T>    static constexpr const ElementType*    Get(const T& p)
        {
            return p.get();
        }
        //operator bool
    public:
        explicit constexpr operator bool() const
        {
            return (bool)wrapped;
        }
        //operator*
    public:
        const ElementType& operator*() const
        {
            return *get();
        }
        ElementType& operator*()
        {
            return *get();
        }
        //operator->
    public:
        const ElementType* operator->() const
        {
            return get();
        }
        ElementType* operator->()
        {
            return get();
        }
        //opertor ElementType*
    public:
        template <
            class T = Wrapped,
            typename std::enable_if <
                std::is_pointer<T>::value ||
                std::is_convertible<T, const ElementType*>::value,
                bool
                >::type = true
            >
        constexpr operator const ElementType * () const
        {
            return Get(wrapped);
        }

        template <
            class T = Wrapped,
            typename std::enable_if <
                std::is_pointer<T>::value ||
                std::is_convertible<T, const ElementType*>::value,
                bool
                >::type = true
            >
        constexpr operator       ElementType * ()
        {
            return Get(wrapped);
        }

        //nonmember friends
    private:
        template<class wrapped> friend constexpr const wrapped& GetUnderlying(const PropagateConst<wrapped>& pt) noexcept;
        template<class wrapped> friend constexpr       wrapped& GetUnderlying(PropagateConst<wrapped>& pt) noexcept;
    };

    template<class T>
    constexpr bool operator==(const PropagateConst<T>& pt, std::nullptr_t)
    {
        return GetUnderlying(pt) == nullptr;
    }

    template<class T>
    constexpr bool operator==(nullptr_t, const PropagateConst<T>& pt)
    {
        return nullptr == GetUnderlying(pt);
    }

    template<class T>
    constexpr bool operator!=(const PropagateConst<T>& pt, std::nullptr_t)
    {
        return GetUnderlying(pt) != nullptr;
    }

    template<class T>
    constexpr bool operator!=(nullptr_t, const PropagateConst<T>& pt)
    {
        return nullptr != GetUnderlying(pt);
    }

#define make_PropagateConst_cmp(op)                                                         \
    template<class T0, typename T1>                                                         \
    constexpr bool operator op(const PropagateConst<T0>& pt0,const PropagateConst<T1>& pt1) \
    {                                                                                       \
        return GetUnderlying(pt0) op GetUnderlying(pt1);                                    \
    }                                                                                       \
    template<class Wrapped, typename T>                                                     \
    constexpr bool operator op(const PropagateConst<Wrapped>& pt, const T& rhs)             \
    {                                                                                       \
        return GetUnderlying(pt) op rhs;                                                    \
    }                                                                                       \
    template<class Wrapped, typename T>                                                     \
    constexpr bool operator op(const T& lhs,const PropagateConst<Wrapped>& pt)              \
    {                                                                                       \
        return lhs op GetUnderlying(pt);                                                    \
    }

    make_PropagateConst_cmp( ==)
    make_PropagateConst_cmp( !=)
    make_PropagateConst_cmp( <=)
    make_PropagateConst_cmp( >=)
    make_PropagateConst_cmp( <)
    make_PropagateConst_cmp( >)

#undef make_PropagateConst_cmp

    template<class T>
    constexpr void swap(PropagateConst<T>& pt1, PropagateConst<T>& pt2) noexcept(noexcept(swap(std::declval<T&>(), std::declval<T&>())))
    {
        pt1.swap(pt2);
    }

    template<class T> constexpr const T& GetUnderlying(const PropagateConst<T>& pt) noexcept
    {
        return pt.wrapped;
    }
    template<class T> constexpr       T& GetUnderlying(PropagateConst<T>& pt) noexcept
    {
        return pt.wrapped;
    }
}

namespace std
{
    template<class T>
    struct hash<armarx::PropagateConst<T>>
    {
        using result_type = size_t;
        using argument_type = armarx::PropagateConst<T>;
        size_t operator()(const armarx::PropagateConst<T>& p) const noexcept(noexcept(hash<T> {}(GetUnderlying(p))))
        {
            return hash<T> {}(GetUnderlying(p));
        }
    };

#define make_PropagateConst_cmp_function_objects(obj)                                                   \
    template<class T>                                                                                   \
    struct obj<armarx::PropagateConst<T>>                                                               \
    {                                                                                                   \
        using first_argument_type = armarx::PropagateConst<T>;                                          \
        using second_argument_type = armarx::PropagateConst<T>;                                         \
        using result_type = bool;                                                                       \
        constexpr bool operator()(const first_argument_type& l, const second_argument_type& r) const    \
        {                                                                                               \
            return obj<T>{}(GetUnderlying(l), GetUnderlying(r));                                        \
        }                                                                                               \
    }

    make_PropagateConst_cmp_function_objects(equal_to);
    make_PropagateConst_cmp_function_objects(not_equal_to);
    make_PropagateConst_cmp_function_objects(less);
    make_PropagateConst_cmp_function_objects(greater);
    make_PropagateConst_cmp_function_objects(less_equal);
    make_PropagateConst_cmp_function_objects(greater_equal);

#undef make_PropagateConst_cmp_function_objects
}

