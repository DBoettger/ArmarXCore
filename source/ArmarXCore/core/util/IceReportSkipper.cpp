/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2017, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarX
 * @author     Mirko Waechter( mirko.waechter at kit dot edu)
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "IceReportSkipper.h"

#include <ArmarXCore/core/time/TimeUtil.h>
#include <ArmarXCore/core/logging/Logging.h>

namespace armarx
{

    IceReportSkipper::IceReportSkipper(float maxFrequency)
    {
        this->maxFrequency = maxFrequency;
    }

    bool IceReportSkipper::checkFrequency(const Ice::Current& c, const boost::optional<float>& frequency)
    {
        return checkFrequency(c.operation, frequency);
    }

    bool IceReportSkipper::checkFrequency(const std::string& operationName, const boost::optional<float>& frequency)
    {
        IceUtil::Time now = TimeUtil::GetTime();
        ScopedLock lock(mutex);
        auto it = operationData.find(operationName);
        if (it == operationData.end())
        {
            float maxFrequency = frequency.is_initialized() ? frequency.get() : this->maxFrequency;
            operationData[operationName] = {now,  maxFrequency};
            return true;
        }
        else
        {
            SkippingData& data = it->second;
            float maxFrequency = frequency.is_initialized() ? frequency.get() : data.maxFrequency;
            float minInterval = 1.0 / maxFrequency;
            if (now < data.lastTimestamp + IceUtil::Time::secondsDouble(minInterval))
            {
                return false;
            }
            else
            {
                data.lastTimestamp = now;
                return true;
            }
        }
    }

} // namespace armarx
