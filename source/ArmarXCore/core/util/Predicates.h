/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore
 * @author     Raphael Grimm ( ufdrv at student dot kit dot edu)
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
 
 #pragma once

#include <type_traits>
#include <tuple>

namespace armarx
{
    template<class T>
    struct EqualPredicate
    {
        EqualPredicate(T t) : t {std::move(t)} {}
        bool operator()(const T& other)
        {
            return t == other;
        }
        const T t;
    };
    template<class T>
    EqualPredicate<T> equalPredicate(T t)
    {
        return EqualPredicate<T> {std::move(t)};
    }

    template<class T>
    struct UnequalPredicate
    {
        UnequalPredicate(T t) : t {std::move(t)} {}
        bool operator()(const T& other)
        {
            return t != other;
        }
        const T t;
    };
    template<class T>
    UnequalPredicate<T> unequalPredicate(T t)
    {
        return UnequalPredicate<T> {std::move(t)};
    }

    template<class T>
    struct LessPredicate
    {
        LessPredicate(T t) : t {std::move(t)} {}
        bool operator()(const T& other)
        {
            return other < t;
        }
        const T t;
    };
    template<class T>
    LessPredicate<T> lessPredicate(T t)
    {
        return LessPredicate<T> {std::move(t)};
    }

    template<class T>
    struct GreaterPredicate
    {
        GreaterPredicate(T t) : t {std::move(t)} {}
        bool operator()(const T& other)
        {
            return other > t;
        }
        const T t;
    };
    template<class T>
    GreaterPredicate<T> greaterPredicate(T t)
    {
        return GreaterPredicate<T> {std::move(t)};
    }

    template<class T>
    struct LessEqualPredicate
    {
        LessEqualPredicate(T t) : t {std::move(t)} {}
        bool operator()(const T& other)
        {
            return other <= t;
        }
        const T t;
    };
    template<class T>
    LessEqualPredicate<T> lessEqualPredicate(T t)
    {
        return LessEqualPredicate<T> {std::move(t)};
    }

    template<class T>
    struct GreaterEqualPredicate
    {
        GreaterEqualPredicate(T t) : t {std::move(t)} {}
        bool operator()(const T& other)
        {
            return other >= t;
        }
        const T t;
    };
    template<class T>
    GreaterEqualPredicate<T> greaterEqualPredicate(T t)
    {
        return GreaterEqualPredicate<T> {std::move(t)};
    }
}
