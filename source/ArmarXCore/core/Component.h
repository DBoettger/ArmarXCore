/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarXCore::core
* @author     Kai Welke (welke at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <ArmarXCore/core/ManagedIceObject.h>  // for ManagedIceObject
#include <ArmarXCore/core/application/properties/PropertyUser.h>
// ArmarXCore
#include <ArmarXCore/core/system/ImportExport.h>
#include <Ice/Handle.h>                 // for Handle
#include <Ice/Initialize.h>             // for createProperties
#include <Ice/Properties.h>             // for PropertiesPtr
#include <Ice/PropertiesF.h>            // for PropertiesPtr
#include <IceUtil/Handle.h>             // for HandleBase
#include <boost/algorithm/string/case_conv.hpp>  // for to_lower_copy
#include <boost/iterator/iterator_facade.hpp>  // for operator!=, etc
#include <sstream>                      // for basic_stringbuf<>::int_type, etc
#include <string>                       // for string, operator+, etc

#include "ArmarXCore/core/application/properties/PropertyDefinition.h"
#include "ArmarXCore/core/application/properties/PropertyDefinition.hpp"
#include "ArmarXCore/core/application/properties/PropertyDefinitionContainer.h"
#include "ArmarXCore/interface/core/Log.h"  // for MessageType, etc

namespace armarx
{
    /**
    \defgroup Component ArmarX Component
    \ingroup DistributedProcessingGrp
    \brief Subclass of ManagedIceObjects which contains additional properties.
    */

    class Component;

    /**
    * Component smart pointer type
    */
    typedef IceInternal::Handle<Component> ComponentPtr;

    /**
    \class ComponentPropertyDefinitions
    \brief Default component property definition container.
    \ingroup Component
    \see properties

    Inherit from this class to extend your property definitions for components!
    */
    class ComponentPropertyDefinitions:
        public PropertyDefinitionContainer
    {
    public:
        ComponentPropertyDefinitions(std::string prefix):
            PropertyDefinitionContainer(prefix)
        {
            defineOptionalProperty<std::string>("ObjectName", "", "Name of IceGrid well-known object");

            defineOptionalProperty<MessageType>("MinimumLoggingLevel", eUNDEFINED, "Local logging level only for this component", PropertyDefinitionBase::eModifiable)
            .setCaseInsensitive(true)
            .map("Debug", eDEBUG)
            .map("Verbose", eVERBOSE)
            .map("Info", eINFO)
            .map("Important", eIMPORTANT)
            .map("Warning", eWARN)
            .map("Error", eERROR)
            .map("Fatal", eFATAL)
            .map("Undefined", eUNDEFINED);
            defineOptionalProperty<bool>("EnableProfiling", false, "enable profiler which is used for logging performance events", PropertyDefinitionBase::eModifiable)
            .setCaseInsensitive(true)
            .map("false", false)
            .map("no", false)
            .map("0", false)
            .map("true", true)
            .map("yes", true)
            .map("1", true);
        }
    };

    /**
    \class Component
    \brief Baseclass for all ArmarX ManagedIceObjects requiring properties.
    \ingroup Component

    Components are the equivalents to ManagedIceObjects with additional properties.
    Usually, an ArmarX distributed application consists mainly of Component instances,
    which are configured on startup through properties specified in a config file (\see ArmarXCore-HowTos-generate-scenarios).

    It is also possible for components to be reconfigured at runtime.
    To prepare your component for this feature you need to override the Component::componentPropertiesUpdated() method, which is called every time properties change.

    The lifecycle of each Component is managed by an instance of ArmarXManager.
    */
    class ARMARXCORE_IMPORT_EXPORT Component :
        virtual public ManagedIceObject,
        virtual public PropertyUser
    {
    public:
        /**
         * Factory method for a component. Sets the specified properties.
         *
         * The properties are identified using a config domain, a config name and a property name.
         * The identifier of properties follows the rule:
         *
         * configDomain.configName.propertyName
         *
         * The configName is used as name of the well-known object in Ice.
         * @see ManagedIceObject::ManagedIceObject(std::string name)
         * This name can be changed by setting the property:
         *
         * configDomain.configName.ObjectName=<desiredname>
         *
         * \param configName name of the properties identifier
         * \param properties properties for the object
         * \param configDomain domain of the properties identifier
         */
        template <class T, class TPtr = IceInternal::Handle<T>>
        static TPtr create(Ice::PropertiesPtr properties = Ice::createProperties(), const std::string& configName = "", const std::string& configDomain = "ArmarX")
        {
            TPtr ptr = new T();
            ptr->createdByComponentCreate = true;

            ComponentPtr compPtr = ptr;

            if (configName == "")
            {
                ptr->initializeProperties(compPtr->getDefaultName(), properties, configDomain);
            }
            else
            {
                ptr->initializeProperties(configName, properties, configDomain);
            }
            compPtr->icePropertiesInitialized();
            return ptr;
        }

        /**
         * \see PropertyUser::getProperty()
         */
        using PropertyUser::getProperty;

        /**
         * @brief Implement this function if you would like to react to changes in the properties
         * @param changedProperties Set of changed properties. Retrieve the values with getProperty()
         */
        virtual void componentPropertiesUpdated(const std::set<std::string>& changedProperties);

        /**
         * @brief initializes the properties of this component.
         * Must not be called after the component was added to the ArmarXManager.
         * Use with caution!
         * @param configName
         * @param properties
         * @param configDomain
         */
        void initializeProperties(const std::string& configName, Ice::PropertiesPtr properties, const std::string& configDomain);

        /**
         * @brief forces the flag to be set to true that the object instance was created by the Component::create function
         * @note Do not call this function except you know that you are doing.
         */
        void forceComponentCreatedByComponentCreateFunc();


        std::vector<PropertyUserPtr> getAdditionalPropertyUsers() const;


    protected:
        /**
         * Protected default constructor. Used for virtual inheritance. Use createManagedIceObject() instead.
         */
        Component()
        {
            setIceProperties(Ice::createProperties());
            configName = "";
            configDomain = "ArmarX";
            createdByComponentCreate = false;
        }

        /**
         * \see PropertyUser::createPropertyDefinitions()
         */
        PropertyDefinitionsPtr createPropertyDefinitions() override;

        /**
         * Retrieve config domain for this component as set in constructor. The configdomain
         * defines which properties are used from the properties read from commandline or config file.
         * The name of the used properties follows the rule:
         *
         * configDomain.configName.propertyName
         */
        std::string getConfigDomain();

        /**
         * Retrieve config name for this component as set in constructor. The configname
         * defines which properties are used from the properties read from commandline or config file.
         * The name of the properties follows the rule:
         *
         * configDomain.configName.propertyName
         */
        std::string getConfigName();

        /**
         * Retrieve config identifier for this component as set in constructor. The config identifier
         * defines which properties are used from the properties read from commandline or config file.
         * The name of the properties follows the rule:
         *
         * configDomain.configName.propertyName
         *
         * where the configIdentifier corresponds to
         *
         * configDomain.configName
         */
        std::string getConfigIdentifier();



        virtual void icePropertiesInitialized() {}

        /**
         * @brief Add additional property users here that should show up in the application help text.
         * This function needs to be called in the icePropertiesInitialized from the component user.
         * @param subPropertyUser
         */
        void addPropertyUser(const PropertyUserPtr& subPropertyUser);


    private:
        friend class ArmarXManager;
        /**
         * This method is called when new properties are set with Component::setIceProperties().
         * It calls Component::componentPropertiesUpdated(), which can be overriden by subclasses to perform actions when properties change.
         */
        void icePropertiesUpdated(const std::set<std::string>& changedProperties) override final;

        // idenfitication of properties
        std::string configDomain;
        std::string configName;

        std::vector<PropertyUserPtr> additionalPropertyUsers;


        /**
         * @brief this flag is used by ArmarXManager::addObject to check whether a component was created using Component::create.
         */
        bool createdByComponentCreate;
    };
}

