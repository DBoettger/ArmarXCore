/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarXCore::core
* @author     Nikolaus Vahrenkamp (vahrenkamp at kit dot edu)
* @date       2011
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include <Ice/Communicator.h>           // for Communicator, etc
#include <Ice/Exception.h>              // for Exception
#include <Ice/LocalException.h>         // for ObjectNotExistException
#include <Ice/ObjectAdapter.h>          // for ObjectAdapterPtr, etc
#include <Ice/ProxyHandle.h>            // for ProxyHandle
#include <IceGrid/Descriptor.h>         // for ApplicationDescriptor
#include <IceGrid/Exception.h>
#include <IceGrid/FileParser.h>         // for FileParserPrx, FileParser, etc
#include <boost/smart_ptr/shared_ptr.hpp>  // for shared_ptr
#include <boost/thread/lock_guard.hpp>  // for lock_guard
#include <ext/alloc_traits.h>
#include <stddef.h>                     // for NULL
#include <exception>                    // for exception
#include <iostream>                     // for operator<<, endl, etc

#include "ArmarXCore/core/logging/LogSender.h"  // for LogSender
#include "ArmarXCore/core/logging/Logging.h"  // for ARMARX_DEBUG, etc
#include "IceGridAdmin.h"

using namespace armarx;
using namespace std;
using namespace IceGrid;

IceGridAdmin::IceGridAdmin(Ice::CommunicatorPtr c, std::string name):
    communicator(c),
    name(name),
    interval(IceUtil::Time::milliSeconds(500))  // call keep alive message with 2 fps
{

}

void IceGridAdmin::init()
{
    setTag("IceGridAdmin");
    std::string username = "user";
    std::string password = "password";

    if (!communicator)
    {
        throw Ice::ObjectNotExistException("Ice::Communicator needed to create IceGridAdmin, NULL not allowed here", 0);
    }

    // create the IceGrid admin session
    try
    {
        adminSessionProxy =
            registry()->createAdminSession(username, password);
    }
    catch (const IceGrid::PermissionDeniedException& e)
    {
        ARMARX_ERROR << "Permission denied:\n" << e.reason;
        throw e;
    }

    // register adapter
    std::string obsName = std::string("IceGridAdminSession_") + name;
    Ice::ObjectPrx oPrx;

    try
    {
        oPrx = registerObjectWithNewAdapter(this, obsName, iceGridAdminAdapter);
    }
    catch (const std::exception& e)
    {
        ARMARX_ERROR << "Register Adapter Exception: " << e.what() << "\n"
                     << "This might be happening because the property Ice.Default.Host is wrongly set in the config (probably ~/.armarx/default.cfg)";
        throw e;
    }

    iceGridAdminProxy = IceGrid::NodeObserverPrx::checkedCast(oPrx);

    // register observer
    setObservers();

    // start timer
    timer = new IceUtil::Timer();
    timer->scheduleRepeated(this, interval);
}

IceGridAdminPtr IceGridAdmin::Create(Ice::CommunicatorPtr c, string name)
{
    IceGridAdminPtr result = new IceGridAdmin(c, name);
    result->init();
    return result;
}

IceGridAdmin::~IceGridAdmin()
{
    ARMARX_DEBUG << "*** DESTROYING IceGridAdmin <" << name << ":" << this << "> ***";

    if (timer)
    {
        timer->cancel(this);
        timer->destroy();
    }
}

IceGrid::RegistryPrx IceGridAdmin::registry()
{
    if (!registryProxy)
    {
        Ice::ObjectPrx obj = communicator->stringToProxy("IceGrid/Registry");
        registryProxy = IceGrid::RegistryPrx::checkedCast(obj);
    }

    return registryProxy;
}

IceGrid::AdminSessionPrx IceGridAdmin::adminSession()
{
    return adminSessionProxy;
}

void IceGridAdmin::setObservers()
{
    try
    {
        if (adminSession())
        {
            adminSession()->setObservers(nullptr, nullptr, nullptr, nullptr, nullptr);
            adminSession()->setObservers(nullptr, iceGridAdminProxy, nullptr, nullptr, this->objObserverPrx);
        }
    }
    catch (const IceGrid::ObserverAlreadyRegisteredException& e)
    {
        ARMARX_INFO << " IceGrid observer with name  " << e.id.name << " already installed. State changed messages will only processed on a polling basis, which just means a small delay.\n" <<
                    "ObserverAlreadyRegisteredException:\n" << e.what();
    }
}

void IceGridAdmin::setObjectObserver(IceGrid::ObjectObserverPrx objObserverPrx)
{
    this->objObserverPrx = objObserverPrx;
    setObservers();
}

void IceGridAdmin::removeObservers()
{
    IceGrid::NodeObserverPrx adminPrx = iceGridAdminProxy;
    iceGridAdminProxy = nullptr;
    IceGrid::ObjectObserverPrx objPrx = objObserverPrx;
    objObserverPrx = nullptr;
    setObservers();

    try
    {
        if (objPrx)
        {
            getAdmin()->removeObject(objPrx->ice_getIdentity());
        }

        if (adminPrx)
        {
            getAdmin()->removeObject(adminPrx->ice_getIdentity());
            iceGridAdminAdapter->destroy();
            iceGridAdminAdapter = nullptr;
        }
    }
    catch (IceGrid::ObjectNotRegisteredException& e)
    {

    }
}


IceGrid::AdminPrx IceGridAdmin::getAdmin()
{
    return adminSessionProxy->getAdmin();
}

void IceGridAdmin::addApplication(const std::string& xmlPath)
{
    IceGrid::FileParserPrx parser;
    parser = IceGrid::FileParserPrx::checkedCast(communicator->stringToProxy("FileParser"));
    IceGrid::ApplicationDescriptor descriptor = parser->parse(xmlPath, getAdmin());
    getAdmin()->addApplication(descriptor);
}

void IceGridAdmin::runTimerTask()
{
    if (!communicator)
    {
        std::cout << "Communicator NULL" << std::endl;
        return;
    }

    try
    {

        if (communicator && communicator->isShutdown())
        {
            //            ARMARX_INFO << "*** COMMUNICATOR SHUTDOWN" << std::endl;
            return;
        }

        if (adminSessionProxy)
        {
            adminSessionProxy->keepAlive();
        }
        else
        {
            ARMARX_ERROR << "!!noAdminSessionPrx!!";
        }
    }
    catch (Ice::ObjectNotExistException& e)
    {
        ARMARX_ERROR << "*** Caught object not exist exception: " << e.what() << endl;

        if (timer)
        {
            timer->cancel(this);
        }
    }
    catch (Ice::Exception& e)
    {
        ARMARX_ERROR << "*** Caught exception: "  << e.ice_name() << ":\n" << e.what() << "\n"  << e.ice_stackTrace() << endl;

        if (timer)
        {
            timer->cancel(this);
        }
    }

    catch (std::exception& e)
    {
        ARMARX_ERROR << "*** Caught exception: "  << e.what() << endl;
    }
    catch (...)
    {
        ARMARX_ERROR << "*** Caught unknown exception in IceGridAdmin " << endl;
    }
}

void IceGridAdmin::stop()
{
    timer->cancel(this);
    timer->destroy();
}

void IceGridAdmin::cleanUpDeadObjects()
{
    IceGrid::AdminPrx admin = getAdmin();
    IceGrid::ObjectInfoSeq objects = admin->getAllObjectInfos("*");


    for (IceGrid::ObjectInfo info : objects)
    {
        auto current = info.proxy;
        try
        {
            current->ice_ping();
        }
        catch (...)
        {
            admin->removeObject(current->ice_getIdentity());
        }
    }
}

Ice::ObjectPrx IceGridAdmin::registerObjectWithNewAdapter(
    Ice::ObjectPtr object,
    const std::string& objectName,
    Ice::ObjectAdapterPtr& objectAdapter)
{
    ARMARX_VERBOSE << "Registering object with name '" << objectName << "' with new adapter in Ice";
    objectAdapter = communicator->createObjectAdapterWithEndpoints(objectName, "tcp");
    Ice::Identity id = communicator->stringToIdentity(objectName);
    objectAdapter->add(object, id);
    objectAdapter->activate();

    IceGrid::AdminPrx admin = getAdmin();

    Ice::ObjectPrx proxy = objectAdapter->createProxy(id);

    try
    {
        admin->addObjectWithType(proxy, proxy->ice_id());
    }
    catch (const IceGrid::ObjectExistsException&)
    {
        admin->updateObject(proxy);
    }

    return proxy;
}

IceGridAdmin::ComponentState IceGridAdmin::getComponentState(std::string id)
{
    Ice::ObjectPrx proxy = communicator->stringToProxy(id);

    if (!proxy)
    {
        cerr << "No proxy for id " << id << endl;
        return eUnknown;
    }

    try
    {
        proxy->ice_twoway()->ice_ping();
    }
    catch (const Ice::Exception&)
    {
        //cerr << "object " << id << " not reachable" << endl;
        return eDeactivated;
    }

    //cerr << "object " << id << " is reachable" << endl;
    return eActivated;
}

void IceGridAdmin::nodeInit(const IceGrid::NodeDynamicInfoSeq& nodes, const Ice::Current& c)
{
    ARMARX_DEBUG << __PRETTY_FUNCTION__ << endl;

    for (unsigned int i = 0; i < nodes.size(); i++)
    {
        ARMARX_DEBUG << "Node " << i << ": " << nodes[i].info.name << endl;
    }
}

void IceGridAdmin::nodeUp(const IceGrid::NodeDynamicInfo& node, const Ice::Current& c)
{
    ARMARX_DEBUG << __PRETTY_FUNCTION__ << endl;
    ARMARX_DEBUG << "Node:" << node.info.name << endl;
}

void IceGridAdmin::nodeDown(const std::string& name, const Ice::Current& c)
{
    ARMARX_DEBUG << __PRETTY_FUNCTION__ << endl;
    ARMARX_DEBUG << "Node:" << name << endl;
}


void IceGridAdmin::notifyComponentChanged(ComponentState state, std::string id)
{
}

void IceGridAdmin::reportRemoteComponentStateChange(const IceGrid::ServerDynamicInfo& updatedInfo)
{
    boost::lock_guard<boost::mutex> lock(mutexComponentStateUpdate);

    std::string idServer = updatedInfo.id;
    std::string serverString = "Server";

    // check if we have to remove the "Server" string that is added to the ID
    if (idServer.compare(idServer.size() - 6, 6, serverString) == 0)
    {
        idServer = idServer.substr(0, idServer.size() - 6);
    }

    if (remoteComponentsState.find(idServer) == remoteComponentsState.end())
    {
        // no requests for this id have been queried
        // cout << "** no observer request for " << idServer << endl;
        return;
    }

    //cout << "remoteComponentsState[idServer]:" << remoteComponentsState[idServer] << endl;
    switch (updatedInfo.state)
    {
        case IceGrid::Inactive:
        case IceGrid::Activating:
        case IceGrid::ActivationTimedOut:
        case IceGrid::Deactivating:
        case IceGrid::Destroying:
        case IceGrid::Destroyed:

            if (remoteComponentsState[idServer] == eActivated)
            {
                //cout << idServer << ": state == eActivated->notify" << endl;
                notifyComponentChanged(eDeactivated, idServer);
            }

            remoteComponentsState[idServer] = eDeactivated;
            break;

        case IceGrid::Active:

            if (remoteComponentsState[idServer] != eActivated)
            {
                //cout << idServer << ": state != eActivated->notify" << endl;
                notifyComponentChanged(eActivated, idServer);
            }

            remoteComponentsState[idServer] = eActivated;
            break;

        default:
            cerr << __PRETTY_FUNCTION__ << "Unknown state" << endl;

            if (remoteComponentsState[idServer] == eActivated)
            {
                notifyComponentChanged(eDeactivated, idServer);
            }

            remoteComponentsState[idServer] = eUnknown;
            break;
    }

}

void IceGridAdmin::printServerInfo(const IceGrid::ServerDynamicInfo& updatedInfo)
{
    cout << "Server:" << updatedInfo.id << endl;
    cout << "** enabled:";

    if (updatedInfo.enabled)
    {
        cout << "true" << endl;
    }
    else
    {
        cout << "false" << endl;
    }

    cout << "** Server state:";

    switch (updatedInfo.state)
    {
        case IceGrid::Inactive:
            cout << "Inactive" << endl;
            break;

        case IceGrid::Activating:
            cout << "Activating" << endl;
            break;

        case IceGrid::ActivationTimedOut:
            cout << "ActivationTimedOut" << endl;
            break;

        case IceGrid::Active:
            cout << "Active" << endl;
            break;

        case IceGrid::Deactivating:
            cout << "Deactivating" << endl;
            break;

        case IceGrid::Destroying:
            cout << "Destroying" << endl;
            break;

        case IceGrid::Destroyed:
            cout << "Destroyed" << endl;
            break;

        default:
            cout << "unknown" << endl;
    }
}

void IceGridAdmin::updateServer(const std::string& node, IceGrid::ServerDynamicInfo updatedInfo, const Ice::Current& c)
{
    //cout << __PRETTY_FUNCTION__ << "2" <<endl;
    //cout << "Node:" << node << endl;
    //printServerInfo(updatedInfo);
    reportRemoteComponentStateChange(updatedInfo);
}


void IceGridAdmin::updateAdapter(const std::string& node, IceGrid::AdapterDynamicInfo updatedInfo, const Ice::Current& c)
{
    //    cout << __PRETTY_FUNCTION__ << endl;
}

void IceGridAdmin::updateServer(const ::std::string& node, const ::IceGrid::ServerDynamicInfo& updateServer, const ::Ice::Current& c)
{
    //cout << __PRETTY_FUNCTION__ << "1" <<endl;
    //printServerInfo(updateServer);
    reportRemoteComponentStateChange(updateServer);
}

void IceGridAdmin::updateAdapter(const ::std::string& node, const ::IceGrid::AdapterDynamicInfo& updatedInfo, const ::Ice::Current& c)
{
    //    cout << __PRETTY_FUNCTION__ << endl;
}
