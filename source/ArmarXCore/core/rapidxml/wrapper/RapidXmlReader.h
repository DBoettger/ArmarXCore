/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <ArmarXCore/core/exceptions/Exception.h>
#include <ArmarXCore/core/rapidxml/rapidxml.hpp>
#include <ArmarXCore/core/system/ArmarXDataPath.h>

#include <boost/shared_ptr.hpp>
#include <boost/shared_array.hpp>
#include <string>
#include <fstream>
#include <streambuf>

namespace armarx
{
    namespace exceptions
    {
        namespace local
        {
            class RapidXmlReaderException: public armarx::LocalException
            {
            public:
                RapidXmlReaderException(const std::string& message) noexcept :
                    armarx::LocalException(message)
                { }

                ~RapidXmlReaderException() noexcept override
                { }

                std::string name() const override
                {
                    return "armarx::exceptions::local::RapidXmlWrapperException";
                }
            };
        }
    }

    class RapidXmlReader;
    typedef boost::shared_ptr<RapidXmlReader> RapidXmlReaderPtr;

    class RapidXmlReaderNode
    {
        friend class RapidXmlReader;

    private:
        rapidxml::xml_node<>* node;
        boost::shared_ptr<rapidxml::xml_document<> > doc;
        boost::shared_array<char> cstr;
        RapidXmlReaderNode(rapidxml::xml_node<>* node, boost::shared_ptr<rapidxml::xml_document<> > doc, boost::shared_array<char> cstr)
            : node(node), doc(doc), cstr(cstr)
        { }
        void check() const
        {
            if (!node)
            {
                throw exceptions::local::RapidXmlReaderException("RapidXmlWrapper NullPointerException");
            }
        }

        char* get_attrib_value(const char* attrName) const
        {
            check();
            rapidxml::xml_attribute<>* attrib = node->first_attribute(attrName);

            if (!attrib)
            {
                throw exceptions::local::RapidXmlReaderException(std::string("Attribute '") + attrName + "' does not exist in node " + getPath());
            }

            return attrib->value();
        }

    public:
        static RapidXmlReaderNode NullNode()
        {
            return RapidXmlReaderNode(nullptr, boost::shared_ptr<rapidxml::xml_document<> >(), boost::shared_array<char>());
        }

        /**
         * @brief get_node_ptr only for legacy code.
         * @return internal pointer
         */
        rapidxml::xml_node<>* get_node_ptr() const
        {
            return node;
        }

        RapidXmlReaderNode first_node(const char* name = nullptr) const
        {
            check();
            return RapidXmlReaderNode(node->first_node(name), doc, cstr);
        }
        RapidXmlReaderNode first_node_with_attr_value(const char* tagName, const char* attrName, const char* attrValue) const
        {
            for (RapidXmlReaderNode n = first_node(tagName); n.is_valid(); n = n.next_sibling(tagName))
            {
                if (n.has_attribute_with_value(attrName, attrValue))
                {
                    return n;
                }
            }
            return NullNode();
        }
        std::vector<RapidXmlReaderNode> nodes(const char* name = nullptr) const
        {
            std::vector<RapidXmlReaderNode> vec;
            nodes(name, vec);
            return vec;
        }
        void nodes(const char* name, std::vector<RapidXmlReaderNode>& vec) const
        {
            for (RapidXmlReaderNode n = first_node(name); n.is_valid(); n = n.next_sibling(name))
            {
                vec.push_back(n);
            }
        }
        void nodes(const char* name1, const char* name2, std::vector<RapidXmlReaderNode>& vec) const
        {
            for (RapidXmlReaderNode n = first_node(name1); n.is_valid(); n = n.next_sibling(name1))
            {
                n.nodes(name2, vec);
            }

        }
        std::vector<RapidXmlReaderNode> nodes(const char* name1, const char* name2) const
        {
            std::vector<RapidXmlReaderNode> vec;
            nodes(name1, name2, vec);
            return vec;
        }
        std::vector<RapidXmlReaderNode> nodes(const char* name1, const char* name2, const char* name3) const
        {
            std::vector<RapidXmlReaderNode> vec;

            for (RapidXmlReaderNode n = first_node(name1); n.is_valid(); n = n.next_sibling(name1))
            {
                n.nodes(name2, name3, vec);
            }

            return vec;
        }
        std::string attribute_value(const char* attrName) const
        {
            return std::string(get_attrib_value(attrName));
        }

        bool has_node(const char* nodeName) const
        {
            check();
            return node->first_node(nodeName) != nullptr;
        }

        bool has_attribute(const char* attrName) const
        {
            check();
            return node->first_attribute(attrName) != nullptr;
        }
        bool has_attribute_with_value(const char* attrName, const char* attrValue) const
        {
            check();
            rapidxml::xml_attribute<>* attrib = node->first_attribute(attrName);
            if (!attrib)
            {
                return false;
            }
            return std::string(attrib->value()) == std::string(attrValue);
        }

        std::string attribute_value_or_default(const char* attrName, const std::string& defaultValue) const
        {
            check();
            rapidxml::xml_attribute<>* attrib = node->first_attribute(attrName);

            if (!attrib)
            {
                return defaultValue;
            }

            return std::string(attrib->value());
        }

        bool attribute_as_bool(const char* attrName, const std::string& trueValue, const std::string& falseValue) const
        {
            std::string value = std::string(get_attrib_value(attrName));

            if (value == trueValue)
            {
                return true;
            }
            else if (value == falseValue)
            {
                return false;
            }
            else
            {
                throw exceptions::local::RapidXmlReaderException(std::string("Invalid value '") + value + "' for attribute '" + attrName + "'. Expecting '" + trueValue + "' or '" + falseValue + "'.");
            }
        }

        bool attribute_as_optional_bool(const char* name, const std::string& trueValue, const std::string& falseValue, bool defaultValue) const
        {
            check();
            rapidxml::xml_attribute<>* attrib = node->first_attribute(name);

            if (!attrib)
            {
                return defaultValue;
            }

            std::string value = std::string(attrib->value());

            if (value == trueValue)
            {
                return true;
            }
            else if (value == falseValue)
            {
                return false;
            }
            else
            {
                throw exceptions::local::RapidXmlReaderException(std::string("Invalid value '") + value + "' for attribute '" + name + "'. Expecting '" + trueValue + "' or '" + falseValue + "'.");
            }
        }

        float attribute_as_float(const char* attrName) const
        {
            return (float)atof(get_attrib_value(attrName));
        }

        uint32_t attribute_as_uint(const char* attrName) const
        {
            std::stringstream strValue(get_attrib_value(attrName));
            uint32_t retValue;
            strValue >> retValue;
            return retValue;
        }

        std::vector<std::pair<std::string, std::string>> get_all_attributes()
        {
            check();
            std::vector<std::pair<std::string, std::string>> attributes;

            rapidxml::xml_attribute<>* attrib = node->first_attribute();

            while (attrib)
            {
                std::string name = std::string(attrib->name());
                std::string value = std::string(attrib->value());
                attributes.push_back({name, value});
                attrib = attrib->next_attribute();
            }

            return attributes;
        }

        std::string value() const
        {
            check();
            return std::string(node->value());
        }

        float value_as_float() const
        {
            check();
            return static_cast<float>(atof(node->value()));
        }

        uint32_t value_as_uint32() const
        {
            check();
            return static_cast<uint32_t>(std::stoul(node->value()));
        }

        int32_t value_as_int32() const
        {
            check();
            return static_cast<int32_t>(std::stoul(node->value()));
        }

        int16_t value_as_int16() const
        {
            check();
            return static_cast<int16_t>(std::stoul(node->value()));
        }

        std::string name() const
        {
            check();
            return std::string(node->name());
        }

        rapidxml::node_type type()
        {
            check();
            return node->type();
        }

        std::string first_node_value(const char* nodeName = nullptr) const
        {
            check();
            rapidxml::xml_node<>* childNode = node->first_node(nodeName);

            if (!childNode)
            {
                throw exceptions::local::RapidXmlReaderException(std::string("Node '") + nodeName + "' does not exist in node " + getPath());
            }

            return std::string(childNode->value());
        }

        std::string first_node_value_or_default(const char* name, const std::string& defaultValue) const
        {
            check();
            rapidxml::xml_node<>* childNode = node->first_node(name);

            if (!childNode)
            {
                return defaultValue;
            }

            return std::string(childNode->value());
        }
        RapidXmlReaderNode next_sibling(const char* name = nullptr) const
        {
            check();
            return RapidXmlReaderNode(node->next_sibling(name), doc, cstr);
        }

        bool is_valid() const
        {
            return node != nullptr;
        }

        std::string getPath() const
        {
            check();
            std::string result = name();
            rapidxml::xml_node<>* p = node->parent();

            while (p)
            {
                result = std::string(p->name()) + "/" + result;
                p = p->parent();
            }

            return result;
        }
    };

    class RapidXmlReader
    {
    private:
        boost::shared_ptr<rapidxml::xml_document<> > doc;
        boost::shared_array<char> cstr;

        RapidXmlReader(const std::string& xml)
            : doc(new rapidxml::xml_document<>())
        {
            cstr.reset(new char[xml.size() + 1]);   // Create char buffer to store string copy
            strcpy(cstr.get(), xml.c_str());        // Copy string into char buffer
            doc->parse<0>(cstr.get());              // Pass the non-const char* to parse()
        }

    public:
        static std::string ReadFileContents(const std::string& path)
        {
            std::string absolutePath;
            bool found = ArmarXDataPath::getAbsolutePath(path, absolutePath);
            std::ifstream t(absolutePath.c_str());
            std::string str;

            if (!found || !t.is_open())
            {
                throw exceptions::local::RapidXmlReaderException("Could not open file ") << path;
            }

            t.seekg(0, std::ios::end);
            str.reserve(t.tellg());
            t.seekg(0, std::ios::beg);

            str.assign((std::istreambuf_iterator<char>(t)), std::istreambuf_iterator<char>());
            return str;
        }

        static RapidXmlReaderPtr FromXmlString(const std::string& xml)
        {
            RapidXmlReaderPtr wrapper(new RapidXmlReader(xml));
            return wrapper;
        }

        static RapidXmlReaderPtr FromFile(const std::string& path)
        {
            return FromXmlString(ReadFileContents(path));
        }

        RapidXmlReaderNode getDocument()
        {
            return RapidXmlReaderNode(doc.get(), doc, cstr);
        }

        RapidXmlReaderNode getRoot(const char* name = nullptr)
        {
            return getDocument().first_node(name);
        }

    };

}


