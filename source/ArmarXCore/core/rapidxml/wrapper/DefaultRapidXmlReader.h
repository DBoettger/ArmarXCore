/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <ArmarXCore/core/rapidxml/rapidxml.hpp>
#include <ArmarXCore/core/rapidxml/wrapper/RapidXmlReader.h>

#include <string>

namespace armarx
{
    typedef std::vector<RapidXmlReaderNode> RapidXmlReaderNodeSeq;
    class DefaultRapidXmlReaderNode
    {

    private:
        RapidXmlReaderNodeSeq nodes;

        void check() const
        {
            if (nodes.empty())
            {
                throw exceptions::local::RapidXmlReaderException("DefaultRapidXmlWrapper NullPointerException");
            }
        }

        std::string get_attrib_value(const char* attrName) const
        {
            check();
            for (RapidXmlReaderNode node : nodes)
            {
                if (node.has_attribute(attrName))
                {
                    return node.attribute_value(attrName);
                }
            }

            std::vector<std::string> paths = getPaths();
            std::string all_paths;
            for (std::string path : paths)
            {
                all_paths += path;
                all_paths += " or ";
            }
            all_paths = all_paths.substr(0, all_paths.length() - 4);
            throw exceptions::local::RapidXmlReaderException(std::string("Attribute '") + attrName + "' does not exist in node " + all_paths);
        }

        std::string get_value() const
        {
            RapidXmlReaderNode n = nodes.front();
            return n.value();
        }

    public:

        DefaultRapidXmlReaderNode(std::vector<RapidXmlReaderNode> rnodes)
        {
            for (RapidXmlReaderNode n : rnodes)
            {
                if (!n.is_valid())
                {
                    throw exceptions::local::RapidXmlReaderException("DefaultRapidXmlWrapper NullPointerException in constructor");
                }
            }
            reverse(rnodes.begin(), rnodes.end());
            nodes = rnodes;
        }



        DefaultRapidXmlReaderNode first_node(const char* name = nullptr) const
        {
            check();
            std::vector<RapidXmlReaderNode> v;
            for (RapidXmlReaderNode n : nodes)
            {
                if (n.get_node_ptr()->first_node(name) != nullptr)
                {
                    v.push_back(n.first_node(name));
                }
            }
            if (v.empty())
            {
                std::vector<std::string> paths = getPaths();
                std::string all_paths;
                for (std::string path : paths)
                {
                    all_paths = path + " or ";
                }
                all_paths = all_paths.substr(0, all_paths.length() - 4);
                throw exceptions::local::RapidXmlReaderException(std::string("Node with name '") + name + "' does not exist in node " + all_paths);
            }
            reverse(v.begin(), v.end());
            return DefaultRapidXmlReaderNode(v);
        }

        bool has_attribute(const char* attrName) const
        {
            check();
            for (RapidXmlReaderNode n : nodes)
            {
                if (n.has_attribute(attrName))
                {
                    return true;
                }
            }
            return false;
        }

        bool attribute_as_bool(const char* attrName, const std::string& trueValue, const std::string& falseValue) const
        {
            std::string value = std::string(get_attrib_value(attrName));

            if (value == trueValue)
            {
                return true;
            }
            else if (value == falseValue)
            {
                return false;
            }
            else
            {
                throw exceptions::local::RapidXmlReaderException(std::string("Invalid value '") + value + "' for attribute '" + attrName + "'. Expecting '" + trueValue + "' or '" + falseValue + "'.");
            }
        }

        bool attribute_as_optional_bool(const char* name, const std::string& trueValue, const std::string& falseValue, bool defaultValue) const
        {
            check();

            if (!has_attribute(name))
            {
                return defaultValue;
            }

            std::string value = std::string(get_attrib_value(name));

            if (value == trueValue)
            {
                return true;
            }
            else if (value == falseValue)
            {
                return false;
            }
            else
            {
                throw exceptions::local::RapidXmlReaderException(std::string("Invalid value '") + value + "' for attribute '" + name + "'. Expecting '" + trueValue + "' or '" + falseValue + "'.");
            }
        }

        std::string attribute_as_string(const char* attrName) const
        {
            return std::string(get_attrib_value(attrName));
        }

        float attribute_as_float(const char* attrName) const
        {
            std::stringstream strValue(get_attrib_value(attrName));
            float retValue;
            strValue >> retValue;
            return retValue;
        }

        uint32_t attribute_as_uint(const char* attrName) const
        {
            std::stringstream strValue(get_attrib_value(attrName));
            uint32_t retValue;
            strValue >> retValue;
            return retValue;
        }

        std::string value_as_string() const
        {
            check();
            return std::string(get_value());
        }

        float value_as_float() const
        {
            check();
            std::stringstream strValue(get_value());
            float retValue;
            strValue >> retValue;
            return retValue;
        }

        uint32_t value_as_uint32() const
        {
            check();
            std::stringstream strValue(get_value());
            uint32_t retValue;
            strValue >> retValue;
            return retValue;
        }

        int32_t value_as_int32() const
        {
            check();
            std::stringstream strValue(get_value());
            int32_t retValue;
            strValue >> retValue;
            return retValue;
        }

        bool value_as_bool(const std::string& trueValue, const std::string& falseValue) const
        {
            check();
            std::string value = std::string(get_value());
            if (value == trueValue)
            {
                return true;
            }
            else if (value == falseValue)
            {
                return false;
            }
            else
            {
                std::string s = "(";
                for (RapidXmlReaderNode node : nodes)
                {
                    s += std::string(node.name());
                    s += ", ";
                }
                s = s.substr(0, s.length() - 2);
                s += ")";
                throw exceptions::local::RapidXmlReaderException(std::string("Invalid value '") + value + "' for represented nodes " + s + ". Expecting '" + trueValue + "' or '" + falseValue + "'.");
            }
        }

        bool value_as_optional_bool(const std::string& trueValue, const std::string& falseValue, bool defaultValue) const
        {
            check();
            std::string value = std::string(get_value());
            if (value == trueValue)
            {
                return true;
            }
            else if (value == falseValue)
            {
                return false;
            }
            else
            {
                return defaultValue;
            }
        }

        bool is_valid() const
        {
            return !nodes.empty();
        }

        std::vector<std::string> getPaths() const
        {
            check();
            std::vector<std::string> results;
            for (RapidXmlReaderNode n : nodes)
            {
                results.push_back(n.getPath());
            }
            return results;
        }

        DefaultRapidXmlReaderNode add_node_at_end(RapidXmlReaderNode node)
        {
            return add_node_at(node, nodes.size());
        }

        DefaultRapidXmlReaderNode add_node_at(RapidXmlReaderNode node, size_t position)
        {
            if (position > nodes.size())
            {
                throw exceptions::local::RapidXmlReaderException("The index is out of range: given index ") << position << " size of container: " << nodes.size();
            }

            if (node.is_valid())
            {
                std::vector<RapidXmlReaderNode> v = nodes;
                reverse(v.begin(), v.end());
                v.insert(v.begin() + position, node);
                return DefaultRapidXmlReaderNode(v);
            }
            return *this;
        }

        DefaultRapidXmlReaderNode remove_node_at(size_t pos)
        {
            if (pos >= nodes.size())
            {
                pos = nodes.size() - 1;
            }
            if (is_valid())
            {
                std::vector<RapidXmlReaderNode> v = nodes;
                reverse(v.begin(), v.end());
                v.erase(v.begin() + pos);
                return DefaultRapidXmlReaderNode(v);
            }
            return *this;
        }

    };
}


