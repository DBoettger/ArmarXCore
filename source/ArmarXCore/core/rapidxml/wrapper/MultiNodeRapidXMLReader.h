/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarXCore::Core
* @author     Simon Ottenhaus( simon dot ottenhaus at kit dot edu)
* @author     Mirko Waechter( mirko dot waechter at kit dot edu)
* @date       2017
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include "RapidXmlReader.h"
#include <vector>

namespace armarx
{
    typedef std::vector<RapidXmlReaderNode> RapidXmlReaderNodeSeq;
    class MultiNodeRapidXMLReader
    {
    public:
        MultiNodeRapidXMLReader() = default;
        MultiNodeRapidXMLReader(RapidXmlReaderNodeSeq nodes) : nodeSeq(nodes) {}

        RapidXmlReaderNode first_node(const char* name) const
        {
            for (const RapidXmlReaderNode& node : nodeSeq)
            {
                auto resultNode = node.first_node(name);
                if (resultNode.is_valid())
                {
                    return resultNode;
                }
            }
            return RapidXmlReaderNode::NullNode();
        }

        RapidXmlReaderNodeSeq nodes(const char* name) const
        {
            RapidXmlReaderNodeSeq resultSeq;
            for (const RapidXmlReaderNode& node : nodeSeq)
            {
                node.nodes(name, resultSeq);
            }
            return resultSeq;
        }

        void addNode(const RapidXmlReaderNode& node)
        {
            nodeSeq.emplace_back(node);
        }

    private:
        RapidXmlReaderNodeSeq nodeSeq;
    };
}

