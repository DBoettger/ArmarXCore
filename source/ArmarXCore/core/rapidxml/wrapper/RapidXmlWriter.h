/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <ArmarXCore/core/rapidxml/rapidxml.hpp>
#include <ArmarXCore/core/rapidxml/rapidxml_print.hpp>
#include <string>
#include <fstream>

namespace armarx
{

    class RapidXmlWriterNode
    {
        friend class RapidXmlWriter;

    private:
        rapidxml::xml_document<>* document;
        rapidxml::xml_node<>* node;

        RapidXmlWriterNode(rapidxml::xml_document<>* document, rapidxml::node_type node_type)
        {
            this->document = document;
            node = document->allocate_node(node_type);
        }
        RapidXmlWriterNode(rapidxml::xml_document<>* document, rapidxml::node_type node_type, const std::string& name)
        {
            this->document = document;
            node = document->allocate_node(node_type, cloneString(name));
        }

    public:
        RapidXmlWriterNode& append_attribute(const std::string& name, const std::string& value)
        {
            node->append_attribute(document->allocate_attribute(cloneString(name), cloneString(value)));
            return *this;
        }
        RapidXmlWriterNode& append_bool_attribute(const std::string& name, const std::string& trueValue, const std::string& falseValue, bool value)
        {
            append_attribute(name, value ? trueValue : falseValue);
            return *this;
        }
        RapidXmlWriterNode& append_optional_bool_attribute(const std::string& name, const std::string& trueValue, const std::string& falseValue, bool value, bool defaultValue)
        {
            if (value != defaultValue)
            {
                append_attribute(name, value ? trueValue : falseValue);
            }
            return *this;
        }

        RapidXmlWriterNode append_node(const std::string& name)
        {
            RapidXmlWriterNode node(document, rapidxml::node_element, name);
            this->node->append_node(node.node);
            return node;
        }
        RapidXmlWriterNode& append_data_node(const std::string& value)
        {
            this->node->append_node(document->allocate_node(rapidxml::node_data, nullptr, cloneString(value)));
            return *this;
        }

        RapidXmlWriterNode& append_string_node(const std::string& name, const std::string& value)
        {
            this->node->append_node(document->allocate_node(rapidxml::node_element, cloneString(name), cloneString(value)));
            return *this;
        }

    private:
        const char* cloneString(const std::string& str)
        {
            return document->allocate_string(str.c_str());
        }

    };

    class RapidXmlWriter
    {
    private:
        rapidxml::xml_document<> document;

    public:
        RapidXmlWriter()
        {
            RapidXmlWriterNode declaration(&document, rapidxml::node_declaration);
            declaration.append_attribute("version", "1.0");
            declaration.append_attribute("encoding", "utf-8");
            document.append_node(declaration.node);
        }

        RapidXmlWriterNode createRootNode(const std::string& name)
        {
            RapidXmlWriterNode rootNode(&document, rapidxml::node_element, name);
            document.append_node(rootNode.node);
            return rootNode;
        }

        std::string print(bool indent)
        {
            std::string s;
            rapidxml::print(std::back_inserter(s), document, indent ? 0 : rapidxml::print_no_indenting);
            return s;
        }
        void saveToFile(const std::string& path, bool indent)
        {
            std::ofstream file;
            file.open(path.c_str());
            file << print(indent);
            file.close();
        }


    };
}

