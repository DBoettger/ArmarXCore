/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarXCore::Statechart
* @author     Mirko Waechter( mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/
#pragma once
#include <map>
#include <string>
#include <exception>

#include <ArmarXCore/interface/observers/VariantBase.h>
#include <ArmarXCore/interface/statechart/StatechartIce.h>

#include <ArmarXCore/observers/variant/Variant.h>
#include <ArmarXCore/observers/parameter/Parameter.h>
#include <ArmarXCore/observers/parameter/VariantParameter.h>
#include <ArmarXCore/observers/parameter/VariantListParameter.h>

#include <ArmarXCore/statechart/Exception.h>

namespace armarx
{
    class ParameterMapping;
    typedef IceInternal::Handle<ParameterMapping> ParameterMappingPtr;
    class StatechartContext;


    /**
      \class ParameterMapping
      \ingroup StatechartGrp
      \brief This class maps parameters from several source dictionaries to one input dictionary.
      The mapping depends on an instance of ParameterMappingIceBase, in which the mapping is specified.<br/>

      An example mapping looks like this:<br/>
      sourceKey: State1.timeout<br/>
      targetKey: State2.globaltimeout<br/>
      This would map the value of the outputparameter of a state named "State1.timeout" on the value of the inputparameter of a state named "State2.globaltimeout".<br/>

      Also wildcards are possible.<br/>
      sourceKey: State1.*<br/>
      targetKey: State2.*<br/>
      This mapping would try to map all parameters of a source on the inputparameters of a target state. But only those that are matching exactly on the wildcard level.<br/>
      */

    class ParameterMapping :
        virtual public ParameterMappingIceBase,
        virtual public Logging
    {

    public:
        /*!
         * \brief Creates a new instance of a ParameterMapping. Since the
         * constructors are private, this method must be used, to create a
         * new ParameterMapping.
         * \return Pointer to the new instance of a ParameterMapping.
         */
        static ParameterMappingPtr createMapping();

        //! Returns a new instance of ParameterMapping with the contents of this instance.
        ::Ice::ObjectPtr ice_clone() const override;
        //! Returns a new instance of ParameterMapping with the contents of this instance.
        virtual ParameterMappingPtr clone() const;

        ParameterMapping& operator=(const ParameterMapping& rhs);

        static std::string MappingSourceToString(MappingSource mappingSource);
        static MappingSource StringToMappingSource(const std::string& mappingSourceString);

        /*!
         *   \brief Adds a priority for a specific source dictionary to the mapping.
         *
         *   The priorities determine which source dictionary is chosen, if two
         *   entries of different source dictionaries map onto the same target
         *   parameter.
         *   Each priority level can only be used once. If the priority level
         *   already exists, an eLogicError exception is thrown.
         *   \throw eLogicError
         *   \param priorityLevel Any int value possible. High value -> Higher Priority
         *   \param mappingSrc The mapping source for which this priority level should apply.
         *   \return Shared pointer to this for fluent interface.
          */
        ParameterMappingPtr setSourcePriority(int priorityLevel, MappingSource mappingSrc, const Ice::Current& c = ::Ice::Current());

        /*!
         * \brief Adds an entry to the ParameterMapping, that maps the sourceKey's value
         *  from the <b>output</b> parameters of the last state to the targetKey's value
         * of the target dictionary.
         * \param sourceKey The key in the source dictionary.
         * \param targetKey The key in the target dictionary.
         * \return Shared pointer to this for fluent interface.
         */
        ParameterMappingPtr mapFromOutput(const std::string& sourceKey, const std::string& targetKey, const Ice::Current& c = ::Ice::Current());
        ParameterMappingPtr mapFromOutput(const std::string& bothKeys, const Ice::Current& c = ::Ice::Current());

        /*!
         * \brief Adds an entry to the ParameterMapping, that maps the sourceKey's value
         *  from the <b>parent's input</b> parameters of the current state to the
         *  targetKey's value of the target dictionary.
         * \param sourceKey The key in the source dictionary.
         * \param targetKey The key in the target dictionary.
         * \return Shared pointer to this for fluent interface.
         */
        ParameterMappingPtr mapFromParent(const std::string& parentKey, const std::string& targetKey, const Ice::Current& c = ::Ice::Current());
        ParameterMappingPtr mapFromParent(const std::string& bothKeys, const Ice::Current& c = ::Ice::Current());

        /*!
         * \brief Adds an entry to the ParameterMapping, that maps the sourceKey's value
         *  from the <b>event</b> parameters of the transition to the targetKey's value
         * of the target dictionary.
         * \param sourceKey The key in the source dictionary.
         * \param targetKey The key in the target dictionary.
         * \return Shared pointer to this for fluent interface.
         */
        ParameterMappingPtr mapFromEvent(const std::string& eventKey, const std::string& targetKey, const Ice::Current& c = ::Ice::Current());
        ParameterMappingPtr mapFromEvent(const std::string& bothKeys, const Ice::Current& c = ::Ice::Current());

        /*!
         * \brief Adds an entry to the ParameterMapping, that maps the value of the
         * datafield entry to the targetKey's value of the target dictionary.
         * \param sourceKey The key in the source dictionary.
         * \param targetKey The key in the target dictionary.
         * \return Shared pointer to this for fluent interface.
         */
        ParameterMappingPtr mapFromDataField(const DataFieldIdentifierBasePtr& dataFieldIdentifier, const std::string& targetKey, const Ice::Current& c = ::Ice::Current());

        void addMappingEntry(MappingSource mappingSource, const std::string& sourceKey, const std::string& targetKey, VariantContainerBasePtr value = nullptr);

        /*!
         * \brief Sets the behaviour of the mapping into the target dictionary
         * to greedy.
         *
         * Greedy means, that all parameters of all source dictionaries that
         * have the same key value will be mapped into the target dictionary,
         * whether or not they are specified in the mapping.
         * \param on If true, the mapping is greedy.
         * \return Shared pointer to this for fluent interface.
         */
        ParameterMappingPtr setTargetDictToGreedy(bool on = true);

        friend class StateBase;
        friend class StateController;
        template <class EventType, class StateType> friend class FinalStateBase;
        friend class StatechartContext;

    protected:
        ~ParameterMapping() override {} // protected so that nobody can delete this instance except _decRef of Ice::Shared
        static void _setStatechartContext(StatechartContext* __context);

        //! Checks wether the mapping has an entry like keyDestination that
        //!  maps onto a parameter of mapSource
        StringVariantContainerBaseMap::const_iterator _hasMappingEntry(const std::string& keyDestination,
                const StringVariantContainerBaseMap& sourceDict, MappingSource allowedMappingSource);
        StringVariantContainerBaseMap::const_iterator _findSourceEntry(const std::string sourceKey,
                const StringVariantContainerBaseMap& sourceDict, int destWildcardIndex, const Ice::StringSeq& fieldsDest);

        //! Takes a string and seperates the string by the seperator-char. Inserts all strings
        //! between the seperator-chars into a vector of strings (without the seperator-char).<br/>
        //! E.g. 'State.angle' transforms the 'State' and 'angle'
        static Ice::StringSeq _getFields(std::string source, char seperator = '.');
        //
        static void _addMissingSources(PriorityMap& priorityMap);

        /** \brief This function applies a given mapping to the given inputdictionary.
          All source-dictionary pointers can be set to NULL if the dictionary should not be considered.
          The function applyStandardMapping() is executed everytime a mapping is applied with this function.
          Afterwards the specified (if any) mapping is applied and overwrites the standard mapping.
          */
        void _applyMapping(StateParameterMap& targetDictionary);
        void _greedyMapping(StateParameterMap& targetDictionary, StringVariantContainerBaseMap& sourceDictionary);
        void _fillFromDataField(StateParameterMap& targetDictionary);
        void _fillFromValues(StateParameterMap& targetDictionary);

        ParameterMappingPtr _addSourceDictionary(MappingSource mappingSrc, const StringVariantContainerBaseMap& sourceDict, const Ice::Current& c = ::Ice::Current());

    private:
        bool __greedyInput;
        static StatechartContext* __context;
        ParameterMapping() :  __greedyInput(false) {} //Protected so that nobody can create non-shared-pointer instances.
        ParameterMapping(const ParameterMapping& source); //Protected so that nobody can create non-shared-pointer instances.

        void __fillFromMappingSource(MappingSource mappingSource, StateParameterMap& targetDictionary);
        //! copies the
        void __copyDataFromSourceDict(StringVariantContainerBaseMap::const_iterator& itSourceEntry, StateParameterMap::iterator itTargetEntry);
    };

    //! \brief Returns a new and empty instance of ParameterMapping.
    ParameterMappingPtr createMapping();

    struct eNoValidMapping : LocalException
    {
        eNoValidMapping(std::string missingKey): LocalException(std::string("armarx::eNoValidMapping: The given mapping is invalid! The key '" + missingKey + "' could not be mapped.")) {}
        ~eNoValidMapping() noexcept override {}
    };
    typedef ParameterMapping PM;
    typedef ParameterMappingPtr PMPtr;
}

namespace std
{

    ARMARXCORE_IMPORT_EXPORT ostream& operator<< (ostream& stream, const armarx::ParameterMapping& mapping);


}
