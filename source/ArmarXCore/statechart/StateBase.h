/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarXCore::Statechart
* @author     Mirko Waechter( mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/
#pragma once

// ArmarX Includes
#include <ArmarXCore/core/exceptions/Exception.h>
#include <ArmarXCore/interface/statechart/StatechartIce.h>
#include <ArmarXCore/observers/Event.h>
#include <ArmarXCore/observers/variant/Variant.h>
#include "ParameterMapping.h"
#include "Exception.h"
#include "StateUtilFunctions.h"
#include "StatechartContextInterface.h"
#include <ArmarXCore/core/logging/Logging.h>

// STL Includes
#include <vector>
#include <queue>

// boost Includes
#include <boost/thread/mutex.hpp>
#include <boost/thread/recursive_mutex.hpp>
#include <boost/preprocessor.hpp>


#define EVENTTOALL "toAll"
#define STATEINFO ARMARX_LOG << eINFO << armarx::LogTag("State: "+ stateName)


namespace armarx
{
    //TODO: Move Statecharts into own namespace
    class StateBase;
    typedef IceInternal::Handle<StateBase> StateBasePtr;
    class StatechartManager;


    /**
        * \class StateBase
        * \ingroup StatechartGrp
        * This class is the implementation of the Slice Definition of a state.
        * It is the baseclass for most statechart related classes.
        * Basic functionality like data member handling is implemented here.
        */
    class StateBase :
        virtual public StateIceBase,
        virtual public Logging
    {
    public:


        /*! \brief Function to initialize this state. Must be called in the
         * highest level of the hierarchy - and only there.
          */
        bool init(StatechartContextInterface* context, StatechartManager* manager);

        StatechartContextInterface* getContext(bool checkNULL = true) const;

        template <typename ContextType>
        ContextType*  getContext() const
        {
            ContextType* c = dynamic_cast<ContextType*>(context);

            if (!c)
            {
                throw exceptions::local::eNullPointerException("Could not cast context to requested type!");
            }

            return c;
        }
        void setContext(StatechartContextInterface* context);

        //! Not const because RemoteState implementation gets the current parameters via Ice and sets them
        virtual StateParameterMap getInputParameters() = 0;

        virtual StateParameterMap& getOutputParameters();

        /*!
         * \brief getStateName
         * \return
         */
        std::string getStateName() const;
        Ice::Int getLocalUniqueId() const;
        const std::string& getStateClassName() const;

        /*! \brief Pure virtual function to clone of the derived class type.

          Implemented function should create a new instance with new and return the StateBasePtr. The new instance should contain a reseted, but initialized version of the original.
          */
        virtual StateBasePtr clone() const = 0;
        virtual StateBasePtr createEmptyCopy() const = 0;

        //! \brief Function to get a string that contains als parent states and this state. (e.g. "Robot->Functional->Idling")
        std::string getLocalHierarchyString() const;
        std::string getGlobalHierarchyString() const;

        struct eUnexpectedEvent: LocalException
        {
            eUnexpectedEvent(const EventPtr event, StateIceBasePtr state) :
                LocalException("The state '" + state->stateName + "' does not expect the event '" + event->eventName + "' (EventReceiverState: '" + event->eventReceiverName + "')")
            {
            }
            ~eUnexpectedEvent() noexcept override
            {}
            std::string name() const override
            {
                return "eUnexpectedEvent";
            }
        };

        virtual bool waitForInitialization(int timeoutMS = -1) const;
        /*!
         * @brief Returns the status of this state. Only if a state is initialized,
         * it can be used.
         * @return Status of state.
         */
        virtual bool isInitialized() const;
        /*!
         * @brief Utility function to find a substate of this state by the name.
         *
         * If somehow multiple state with the same name exist, the first state is returned.
         * @param substateName
         * @return Pointer to the requested state, or NULL if not found.
         */
        StateBasePtr findSubstateByName(const std::string& substateName);

        StateBase();
        StateBase(const StateBase& source);
        StateBase& operator=(const StateBase& source);
    protected:
        ~StateBase() override;

        /** Function to copy the states with all it substates and transitions.
        Creates new copies of all substates and copies them as well.
         **/
        virtual void deepCopy(const StateBase& sourceState, bool reset = true);

        /**
         *Function to reset the state: clear name, clear substatesList, clear
         *transition etc.
         **/
        void reset();

        /*! \brief Virtual function, in which this state can be configured.

            Function gets automatically called in init();
          */
        virtual void defineState() {}

        /*!
         *\brief Virtual function, in which substates, transition and mappings
         *can be added.
         */
        virtual void defineSubstates() {}

        /*!
         *\brief Virtual function, in which input/local/output parameters can
         *be specified.
         */
        virtual void defineParameters() {}

        /*! \brief Virtual function, in which the behaviour of state is
        defined, when it is <b>entered</b>.<br/> Can be overridden, but it is
        optional.
          */
        virtual void onEnter();


        /**
         * @brief Virtual function, that can be reimplemented to calculate
         * complex operations. It runs in it's own thread.<br/>
         *
         * This function is called after onEnter(). This function can continue
         * to run even if the
         * state has been left (onExit() has been called), but all calculations
         * will be discarded (Output- and Localparameters will be reseted).<br/>
         * Calls to <b>external components</b> will still be executed!<br/>
         * Calls to sendEvent() will be ignored, after the state has been left.
         *
         * @note Before re-entering this state, the state waits for this
         * function from the last state-visit to complete. The implementation
         * of run should constantly check, if StateController::isRunningTaskStopped()
         * returns true and exit the run-function in that case.
         */
        virtual void run();


        /*! \brief Virtual function, in which the behaviour of state is
        defined, when it is \b exited. Can be overridden, but it is optional.
        \see onBreak()
                  */
        virtual void onExit();

        /*! \brief Virtual function, in which the behaviour of state is
        defined, when it is abnormally exited. Can be overridden, but it is
        optional. <br/> An abnormal exit only occurs in
        hierarchy-levels greater 1.<br/> When a parent state is left before the
        substates are finished, the OnBreak()-function is called in the active
        substate and in all it's active substates.<br/> If this function is not
        implemented, the normal OnExit()-function is called.<br/>
        \see onExit()
          */
        virtual void onBreak();




        bool addParameter(StateParameterMap& paramMap, const std::string& key, VariantTypeId type, bool optional, VariantPtr defaultValue = VariantPtr()) const;
        bool addParameterContainer(StateParameterMap& paramMap, const std::string& key, const ContainerType& containerType, bool optional, VariantContainerBasePtr defaultValue = VariantContainerBasePtr()) const;
        void setParameter(StateParameterMap& paramMap, const std::string& key, const Variant& variant);
        void setParameterContainer(StateParameterMap& paramMap, const std::string& key, const VariantContainerBasePtr& valueContainer);
        void setParameterContainer(StateParameterMap& paramMap, const std::string& key, const VariantContainerBase& valueContainer);


        void getParameter(const StateParameterMap& paramMap, const std::string& key, VariantPtr& value) const;
        void getParameterContainer(const StateParameterMap& paramMap, const std::string& key, VariantContainerBasePtr& valueContainer) const;

        bool isParameterSet(const StateParameterMap& paramMap, const std::string& key) const;
        /*!
         * \brief setStateClassName() sets the string, that contains a
         * stringrepresentation of this class. Should not be called usually.
         * The classname gets automatically set in the constructor of the
         * derived class StateTemplate<T>.
         *
         *
         * \param className Stringrepresentation of the classname (e.g. StateBase for this class)
         */
        void setStateClassName(std::string className);

        /**
         * @brief This functions updates the substates. For local states only
         * calls the substates refetch function, but for remoteStates requests the
         * data via Ice.
         */
        virtual void refetchSubstates();

        /**
         * @brief This function returns the event that was triggered by entering an endstate.
         * This is useful in the onExit() function to determine which endstate of the substates was triggered.
         * @return Event that was triggered, NULL if no endstate was reached
         */
        EventPtr getTriggeredEndstateEvent() const;

        mutable ConditionalVariable initCondition;
        mutable Mutex initMutex;
        void setInitialized(bool enable);
    private:

        // Make all inherited properties private
        // can be avoided by casting into StateIceBase
        using StateIceBase::stateName;
        using StateIceBase::stateClassName;
        using StateIceBase::stateType;
        using StateIceBase::globalStateIdentifier;
        using StateIceBase::initialized;
        using StateIceBase::unbreakable;
        using StateIceBase::eventsDelayed;
        using StateIceBase::greedyInputDictionary;
        using StateIceBase::inputParameters;
        using StateIceBase::localParameters;
        using StateIceBase::outputParameters;
        using StateIceBase::subStateList;
        using StateIceBase::initState;
        using StateIceBase::activeSubstate;
        using StateIceBase::transitions;
        using StateIceBase::installedConditions;
        using StateIceBase::initialStateMapping;

        //! enum that specifies the phase in which the state is currently in
        //! used to control the usage of state-functions in the correct context
        enum StatePhase
        {
            ePreDefined,
            eStatechartDefinitions,
            eSubstatesDefinitions,
            eParametersDefinitions,
            eDefined,
            eEntering,
            eEntered,
            eBreaking,
            eExiting,
            eExited
        } statePhase;
        mutable Mutex statePhaseMutex;
        StatePhase getStatePhase() const;
        void setStatePhase(StatePhase newPhase);
        //! Helper function for checking if a function was called in valid position of the statechart
        //! @throw LocalException if allowedType does not match the current phase type
        void __checkPhase(StatePhase allowedType, const char* functionName) const;
        void __checkPhase(const std::vector<StatePhase>& allowedTypes, const char* functionName) const;
        void __checkPhaseMin(StatePhase allowedType, const char* functionName) const;
        void __throwUnknownParameter(const StateParameterMap& paramMap, const std::string& key) const;


        /**
         *Pointer to the StatechartContext to the single instance per
         *application. Contains proxies to observers and units. Used to operate
         *with distributed components over ice.
         **/
        StatechartContextInterface* context;
        StatechartManager* manager;

        //! Unique Identifier counter for this process for identifing states.
        static int __LocalIdCounter;
        static HiddenTimedMutex* __StateInstancesMutex; // pointer to prevent premature destruction
        RecursiveMutex __eventBufferMutex;
        HiddenTimedMutex __eventUnbreakableBufferMutex;
        RecursiveMutex __processEventMutex;
        HiddenTimedMutex __processEventsMutex;
        HiddenTimedMutex __stateMutex;

        void inheritInputParameters();
        std::vector<std::string> inputInheritance;
        long visitCounter;


        bool __useRunFunction;


        /**
         * @brief This variable contains the event of the end substate that was entered to leave this state.
         */
        EventPtr triggeredEndstateEvent;
        void __copyDefaultValuesToInput();

        typedef std::map<int, StateBase*> StateInstanceMap;
        typedef boost::shared_ptr<StateInstanceMap> StateInstanceMapPtr;
        //! Static map that contains all the states that are alive in this process
        static StateInstanceMapPtr StateInstances;
        StateInstanceMapPtr stateInstancesPtr;

        void clearSelfPointer();

        StateBase* __parentState;
        void __setParentState(StateBase* parentState);
        void __updateGlobalStateId();
        virtual void __updateGlobalStateIdRecursive();


        /*! \brief Virtual function to indicate wheter a state has substates or
        not. To be overridden by RemoteState to deligate the call to the real
        state.
          */
        virtual bool __hasSubstates();

        /*! \brief Virtual function to indicate wheter a state has an
        <b>active</b> substate or not. To be overridden by RemoteState to
        deligate the call to the real state.
          */
        virtual bool __hasActiveSubstate();


        //! Combines both maps to one map and returns a new map of only the set parameters
        StringVariantContainerBaseMap __getSetInputAndLocalParameters() const;
        /**
         *  An in this process unique id for the state
         */
        int localUniqueId;

        /**
         * @brief This property is needed for identification at what time this
         * state was entered the last time.
         *
         * The reason is, that events should only be processed by a state, if
         * the events were installed in this visit to this state.
         * @note not used yet.
         */
        long visitTimeStamp;

        /**
         * @brief GetActiveStateLeafs returns the list of leaf-states that are
         * active at the moment.
         * @param toplevelState State from which the active substate is to be returned.
         * @return List of active leaf-states. If no orthogonal states are used,
         * the list has at most 1 item.
         */

        bool cancelEnteringSubstates;
        static std::vector<StateBasePtr> GetActiveStateLeafs(StateBasePtr toplevelState);

        friend class State;
        friend class StateController;
        friend class StateUtility;
        template <class EventType, class StateType> friend class FinalStateBase;
        template <typename ContextType> friend class RemoteStateOfferer;
        template <typename ContextType> friend class XMLRemoteStateOfferer;
        friend class RemoteStateWrapper;
        friend class RemoteState;
        friend class StatechartEventDistributor;
        friend class DynamicRemoteState;
        template<class StateType> friend class StateTemplate;
        friend class XMLState;
    };

}
