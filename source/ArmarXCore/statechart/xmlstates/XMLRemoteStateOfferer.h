/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2013
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include "GroupXmlReader.h"

#include <ArmarXCore/core/system/cmake/CMakePackageFinder.h>
#include <ArmarXCore/statechart/RemoteStateOfferer.h>
#include <ArmarXCore/statechart/xmlstates/XMLState.h>
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <ArmarXCore/observers/exceptions/user/UnknownTypeException.h>
#include <ArmarXCore/statechart/xmlstates/profiles/StatechartProfiles.h>

#include <ArmarXCore/core/system/DynamicLibrary.h>


#include <boost/algorithm/string/split.hpp>

namespace armarx
{
    struct XMLStateOffererFactoryBase;
    typedef  IceInternal::Handle<XMLStateOffererFactoryBase > XMLStateOffererFactoryBasePtr;
    struct XMLStateOffererFactoryBase :
        virtual Ice::Object,
        virtual public AbstractFactoryMethod<XMLStateOffererFactoryBase, StatechartGroupXmlReaderPtr, XMLStateOffererFactoryBasePtr >
    {

    };

    template <typename ContextType = StatechartContext>
    class XMLRemoteStateOfferer :
        public RemoteStateOfferer<ContextType>,
        virtual public XMLStateOffererFactoryBase
    {
    public:

        XMLRemoteStateOfferer(StatechartGroupXmlReaderPtr reader);

        virtual void onInitXMLRemoteStateOfferer() {}
        virtual void onConnectXMLRemoteStateOfferer() {}
        virtual void onExitXMLRemoteStateOfferer() {}

        std::string getStateOffererName() const override
        {
            return statechartGroupName;
        }

        std::string getStatechartGroupName() const;
        void setStatechartGroupName(const std::string& value);
        //void setStatechartGroupReader(StatechartGroupXmlReaderPtr reader) { this->reader = reader;}
    private:
        void onInitRemoteStateOfferer() override;
        void onConnectRemoteStateOfferer() override;
        void onExitRemoteStateOfferer() override;

        bool loadLib(std::string libPath);

        StateBasePtr getStatePtr(const std::string& stateName) const override;
        StatePtr loadState(RapidXmlReaderPtr reader);
        StatePtr addState(StatePtr state);
        std::map<std::string, DynamicLibraryPtr> libraries;
        std::string statechartGroupName;
        std::map<std::string, StatePtr> uuidStateMap;
        StringXMLNodeMapPtr uuidXMLMap;
        StatechartGroupXmlReaderPtr reader;


    };

    template <typename ContextType>
    XMLRemoteStateOfferer<ContextType>::XMLRemoteStateOfferer(StatechartGroupXmlReaderPtr reader) :
        reader(reader)
    {
        if (reader)
        {
            statechartGroupName = reader->getGroupName();
            ManagedIceObject::setName(reader->getGroupName() + "StateOfferer");
        }
        else
        {
            ARMARX_WARNING << "reader ptr must not be NULL";
        }

        Logging::setTag("XMLRemoteStateOfferer");
    }

    template <typename ContextType>
    void XMLRemoteStateOfferer<ContextType>::onInitRemoteStateOfferer()
    {
        uuidXMLMap.reset(new StringXMLNodeMap());
        statechartGroupName = reader->getGroupName();
        Ice::StringSeq xmlStateFileList = reader->getStateFilepaths();

        std::map<std::string, StatechartGroupXmlReader::StateVisibility> visibilities;

        for (unsigned int i = 0; i < xmlStateFileList.size(); i++)
        {
            std::string& filepath = xmlStateFileList.at(i);
            ARMARX_VERBOSE << "Loading xml state file : " << filepath;

            if (!ArmarXDataPath::getAbsolutePath(filepath, filepath))
            {
                ARMARX_ERROR << "Could not find xml state file: " << filepath;
                continue;
            }

            std::string xmlString = RapidXmlReader::ReadFileContents(filepath);
            RapidXmlReaderPtr reader = RapidXmlReader::FromXmlString(xmlString);
            auto uuid = reader->getRoot("State").attribute_value("uuid");
            uuidXMLMap->insert(std::make_pair(uuid, reader));
            visibilities[uuid] = this->reader->getStateVisibility(filepath);
        }

        for (StringXMLNodeMap::iterator it = uuidXMLMap->begin(); it != uuidXMLMap->end(); it++)
        {
            try
            {
                RapidXmlReaderPtr& reader = it->second;

                if (visibilities[it->first] == StatechartGroupXmlReader::ePublic)
                {
                    loadState(reader);
                }
            }
            catch (exceptions::local::ExpressionException& e)
            {
                ARMARX_ERROR << "The loading of the xml state failed due to an invalid xml file '" << it->first << "'.\nError hint: " << e.what();
            }
            catch (std::exception& e)
            {
                ARMARX_ERROR << "The loading of the xml state failed due to an invalid xml file '" << it->first << "'.\nError hint: " << e.what();
            }

        }

        onInitXMLRemoteStateOfferer();
    }

    template <typename ContextType>
    void XMLRemoteStateOfferer<ContextType>::onConnectRemoteStateOfferer()
    {
        onConnectXMLRemoteStateOfferer();
    }

    template <typename ContextType>
    void XMLRemoteStateOfferer<ContextType>::onExitRemoteStateOfferer()
    {
        onExitXMLRemoteStateOfferer();
    }

    template <typename ContextType>
    StateBasePtr XMLRemoteStateOfferer<ContextType>::getStatePtr(const std::string& stateName) const
    {
        for (const auto& substate : StateBase::subStateList)
        {
            if (substate->stateName == stateName)
            {
                return StateBasePtr::dynamicCast(substate);
            }
        }

        auto it = uuidStateMap.find(stateName);

        if (it != uuidStateMap.end())
        {
            if (it->second)
            {
                return it->second;
            }
            else
            {
                ARMARX_ERROR << "State with id '" << stateName << "' has NULL ptr in map!";
            }
        }

        ARMARX_ERROR << "Could not find state with name '" << stateName << "'";
        //            throw LocalException("Could not find state with name '" + stateName + "'");
        return nullptr;
    }


    template <typename ContextType>
    StatePtr XMLRemoteStateOfferer<ContextType>::loadState(RapidXmlReaderPtr reader)
    {
        RapidXmlReaderNode node = reader->getRoot("State");
        std::string stateName = node.attribute_value("name");
        std::string uuid = node.attribute_value("uuid");

        IceInternal::Handle<XMLState> xmlStateInstance = IceInternal::Handle<XMLState>::dynamicCast(XMLStateFactoryBase::fromName(stateName, XMLStateConstructorParams("", reader, this->reader->getSelectedProfile(), uuidXMLMap, StatechartContext::getIceManager()->getCommunicator())));
        StatePtr state = StatePtr::dynamicCast(xmlStateInstance);

        if (!state)
        {
            state = StatePtr::dynamicCast(NoUserCodeState::CreateInstance(XMLStateConstructorParams("", reader, this->reader->getSelectedProfile(), uuidXMLMap, StatechartContext::getIceManager()->getCommunicator())));
            //            xmlStateInstance = IceInternal::Handle<XMLState>::dynamicCast(state);

        }
        //        xmlStateInstance->setXMLStateData(XMLStateConstructorParams("", reader, this->reader->getSelectedProfile(), uuidXMLMap, StatechartContext::getIceManager()->getCommunicator()));


        if (state)
        {
            state->stateName = stateName;
            state->stateClassName = stateName;
            //            uuidStateMap.find()
            uuidStateMap[uuid] = state;
            addState(state);
        }

        return state;

    }

    template <typename ContextType>
    StatePtr XMLRemoteStateOfferer<ContextType>::addState(StatePtr state)
    {
        if (!state)
        {
            return state;
        }

        if (State::findSubstateByName(state->getStateName()))
        {
            throw exceptions::local::eStatechartLogicError("There exists already a substate with name '" + state->getStateName() + "' in this hierarchy level. In one hierarchy level (aka one substatelist) the names must be unique.");
        }

        if (state->getStateName().empty())
        {
            throw exceptions::local::eStatechartLogicError("The statename must not be empty");
        }

        state->__setParentState(this);
        this->StateBase::subStateList.push_back(state);
        state->init(this->StateBase::context, this->StateBase::manager);

        return state;
    }
    template <typename ContextType>
    std::string XMLRemoteStateOfferer<ContextType>::getStatechartGroupName() const
    {
        return statechartGroupName;
    }

    template <typename ContextType>
    void XMLRemoteStateOfferer<ContextType>::setStatechartGroupName(const std::string& value)
    {
        statechartGroupName = value;
    }


}

