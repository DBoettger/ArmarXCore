/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2014
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "XMLStateComponent.h"
#include "GroupXmlReader.h"
#include "XMLRemoteStateOfferer.h"
#include <IceUtil/UUID.h>
#include <ArmarXCore/core/system/cmake/CMakePackageFinder.h>
#include <ArmarXCore/core/system/ArmarXDataPath.h>
#include <ArmarXCore/core/ArmarXManager.h>
#include <ArmarXCore/core/application/Application.h>
#include <ArmarXCore/statechart/xmlstates/profiles/StatechartProfiles.h>
#include "../StatechartEventDistributor.h"
#include <Ice/Properties.h>

namespace armarx
{

    XMLStateComponent::XMLStateComponent()
    {
        uuid = IceUtil::generateUUID();
    }

    void XMLStateComponent::onInitComponent()
    {
        std::string groupFilepathString = PropertyUser::getProperty<std::string>("XMLStatechartGroupDefinitionFile").getValue();
        std::string profileName = PropertyUser::getProperty<std::string>("XMLStatechartProfile").getValue();

        //std::string profileDefinitionFilepath = PropertyUser::getProperty<std::string>("XMLStatechartProfile").getValue();
        profiles = StatechartProfiles::ReadProfileFiles(Application::getInstance()->getDefaultPackageNames());
        selectedProfile = profiles->getProfileByName(profileName);

        if (!selectedProfile)
        {
            throw LocalException("Could not find profile '" + profileName + "'");
        }

        ARMARX_IMPORTANT << "Using profile " << selectedProfile->getFullName();




        Ice::StringSeq includePaths;
        auto packages = armarx::Application::GetProjectDependencies();
        packages.push_back(Application::GetProjectName());

        for (const std::string& projectName : packages)
        {
            if (projectName.empty())
            {
                continue;
            }

            CMakePackageFinder project(projectName);
            Ice::StringSeq projectIncludePaths;
            auto pathsString = project.getIncludePaths();
            boost::split(projectIncludePaths,
                         pathsString,
                         boost::is_any_of(";,"),
                         boost::token_compress_on);
            includePaths.insert(includePaths.end(), projectIncludePaths.begin(), projectIncludePaths.end());

        }

        StatechartGroupXmlReaderPtr reader(new StatechartGroupXmlReader(selectedProfile));

        ArmarXDataPath::getAbsolutePath(groupFilepathString, groupFilepathString, includePaths);
        reader->readXml(boost::filesystem::path(groupFilepathString));

        auto extraProperties = reader->getConfigurationFileContent();
        if (!extraProperties.empty())
        {
            auto props = Ice::createProperties();
            boost::filesystem::path temp = boost::filesystem::temp_directory_path() / boost::filesystem::unique_path();
            temp += ".cfg";
            std::ofstream file(temp.string().c_str());
            file << extraProperties;
            file.close();
            props->load(temp.string());
            boost::filesystem::remove(temp);
            for (auto& prop : props->getPropertiesForPrefix(""))
            {
                tryAddProperty(prop.first, prop.second);
            }
        }
        CMakePackageFinder pack(reader->getPackageName());

        if (pack.packageFound())
        {
            std::string libPath = "lib" +  reader->getGroupName() + ".so";
            std::vector<std::string> libpathList;
            std::string libPaths = pack.getLibraryPaths();
            boost::split(libpathList, libPaths, boost::is_any_of(";"));
            if (!ArmarXDataPath::getAbsolutePath(libPath, libPath, libpathList))
            {
                ARMARX_ERROR << "Could not find state user code lib file: " << libPath;
            }
            else
            {
                loadLib(libPath);
                ArmarXManager::RegisterKnownObjectFactoriesWithIce(getIceManager()->getCommunicator());
            }
        }

        offererName = reader->getGroupName() + STATEOFFERERSUFFIX;
        offerer = XMLStateOffererFactoryBase::fromName(reader->getGroupName() + STATEOFFERERSUFFIX, reader);
        if (!offerer)
        {
            ARMARX_WARNING << "Could not find XMLRemoteStateOfferer with name " << offererName << "\nAvailable classes are:\n" << XMLStateOffererFactoryBase::getAvailableClasses();
            offerer = new XMLRemoteStateOfferer<>(reader);
        }

        if (PropertyUser::getProperty<std::string>("XMLRemoteStateOffererName").isSet())
        {
            offererName = PropertyUser::getProperty<std::string>("XMLRemoteStateOffererName").getValue();
        }
    }

    void XMLStateComponent::onConnectComponent()
    {
        ComponentPtr obj = ComponentPtr::dynamicCast(offerer);
        obj->forceComponentCreatedByComponentCreateFunc();
        StatechartContextPtr::dynamicCast(offerer)->setReportingTopic(getProperty<std::string>("StateReportingTopic").getValue());
        obj->initializeProperties(offererName, getIceProperties(), getConfigDomain());
        if (getProperty<bool>("EnableProfiling").getValue() && !obj->getProperty<bool>("EnableProfiling").isSet())
        {
            obj->enableProfiler(true);
        }

        getArmarXManager()->addObject(obj, false, selectedProfile->getStatechartGroupPrefix() + obj->getName());

        for (auto state : Split(getProperty<std::string>("StatesToEnter").getValue(), ","))
        {
            boost::trim(state);
            createAndEnterInstance(state);
        }
    }

    std::string XMLStateComponent::getDefaultName() const
    {
        return "XMLStateComponent";
    }

    PropertyDefinitionsPtr XMLStateComponent::createPropertyDefinitions()
    {
        return new XMLStateComponentProperties(Component::getConfigIdentifier());
    }


    bool XMLStateComponent::loadLib(std::string libPath)
    {
        if (!ArmarXDataPath::getAbsolutePath(libPath, libPath))
        {
            ARMARX_ERROR << "Could not find state user code lib file: " << libPath;
            return false;
        }

        if (libraries.find(libPath) != libraries.end())
        {
            return true;
        }

        try
        {
            DynamicLibraryPtr lib(new DynamicLibrary());
            ARMARX_VERBOSE << "Loading lib: " << libPath;
            lib->load(libPath);
            libraries[libPath] = lib;
        }
        catch (exceptions::local::DynamicLibraryException& e)
        {
            ARMARX_ERROR << "Library loading failed: " << e.what();
            return false;
        }

        return true;
    }

    void XMLStateComponent::createAndEnterInstance(std::string stateClassName)
    {
        StatechartContextPtr context = StatechartContextPtr::dynamicCast(offerer);
        context->getObjectScheduler()->waitForObjectState(eManagedIceObjectStarted);
        RemoteStateOffererIceBasePtr off = RemoteStateOffererIceBasePtr::dynamicCast(offerer);
        auto stateId = off->createRemoteStateInstance(stateClassName, nullptr, "TopLevel", stateClassName);

        off->callRemoteState(stateId, StringVariantContainerBaseMap());
    }


}



