/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2013
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "XMLState.h"
#include <ArmarXCore/core/IceManager.h>
#include <ArmarXCore/util/json/JSONObject.h>
#include <fstream>

#include <ArmarXCore/statechart/standardstates/FinalState.h>
#include <ArmarXCore/statechart/RemoteState.h>
#include <ArmarXCore/statechart/xmlstates/profiles/StatechartProfiles.h>

#include <ArmarXCore/core/rapidxml/rapidxml.hpp>
#include <ArmarXCore/core/rapidxml/wrapper/RapidXmlReader.h>

#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <ArmarXCore/observers/exceptions/user/UnknownTypeException.h>

#include <boost/algorithm/string/predicate.hpp>
#include <boost/filesystem/path.hpp>

using namespace armarx;
using namespace rapidxml;




NoUserCodeState::SubClassRegistry NoUserCodeState::Registry(NoUserCodeState::GetName(), &NoUserCodeState::CreateInstance);

NoUserCodeState::NoUserCodeState(XMLStateConstructorParams stateData) :
    XMLState(stateData)

{

}



XMLStateFactoryBasePtr NoUserCodeState::CreateInstance(XMLStateConstructorParams stateData)
{
    return XMLStateFactoryBasePtr(new NoUserCodeState(stateData));
}

std::string NoUserCodeState::GetName()
{
    return "NoUserCodeState";
}

StateBasePtr NoUserCodeState::clone() const
{
    return new NoUserCodeState(*this);
}

VariantContainerBasePtr armarx::GetSelectedProfileValue(RapidXmlReaderNode parameterNode, StatechartProfilePtr selectedProfile, Ice::CommunicatorPtr ic, std::string defaultValueJsonString)
{
    VariantContainerBasePtr result;
    std::map<std::string, std::string> defaultValueMap;
    for (RapidXmlReaderNode defaultValueNode : parameterNode.nodes("DefaultValue"))
    {
        if (defaultValueNode.has_attribute("profile"))
        {
            defaultValueMap.insert(std::make_pair(defaultValueNode.attribute_value("profile"), defaultValueNode.attribute_value("value")));
        }
        else
        {
            defaultValueJsonString = defaultValueNode.attribute_value("value");
        }
    }

    StatechartProfilePtr profile = selectedProfile;

    while (profile)
    {
        if (defaultValueMap.find(profile->getName()) != defaultValueMap.end())
        {
            defaultValueJsonString = defaultValueMap.at(profile->getName());
            break;
        }

        profile = profile->getParent();
    }

    JSONObjectPtr json(new JSONObject(ic));

    if (!defaultValueJsonString.empty())
    {
        try
        {
            json->fromString(defaultValueJsonString);
            result = VariantContainerBasePtr::dynamicCast(json->deserializeIceObject());

            if (!result)
            {
                ARMARX_WARNING_S << "Could not deserialize param: " << defaultValueJsonString;
            }

        }
        catch (JSONException& e)
        {
            ARMARX_WARNING_S << "Could not read default value for param: " << defaultValueJsonString;
        }
    }
    return result;
}


StateParameterDeserialization::StateParameterDeserialization(const RapidXmlReaderNode& parameterNode, Ice::CommunicatorPtr ic, StatechartProfilePtr selectedProfile)
{
    name = parameterNode.attribute_value("name");
    optional = parameterNode.attribute_value("optional") == "no" ? false : true;
    typeStr = parameterNode.attribute_value("type");
    typePtr = VariantContainerType::FromString(typeStr);

    std::string defaultValueJsonString = parameterNode.attribute_value_or_default("default", "");

    container = GetSelectedProfileValue(parameterNode,  selectedProfile, ic, defaultValueJsonString);
}

std::string StateParameterDeserialization::getName()
{
    return name;
}

bool StateParameterDeserialization::getOptional()
{
    return optional;
}

std::string StateParameterDeserialization::getTypeStr()
{
    return typeStr;
}

ContainerTypePtr StateParameterDeserialization::getTypePtr()
{
    return typePtr;
}

VariantContainerBasePtr StateParameterDeserialization::getContainer()
{
    return container;
}


namespace armarx
{

    struct PrivateXmlStateClass
    {
        boost::filesystem::path xmlFilepath;
        RapidXmlReaderPtr stateReader;
        RapidXmlReaderNode stateNode;
        StatechartProfilePtr selectedProfile;
        StringXMLNodeMapPtr uuidToXMLMap;
        Ice::CommunicatorPtr ic;
    };
}




XMLState::XMLState(const XMLStateConstructorParams& stateData)
{
    setXMLStateData(stateData);
}

void XMLState::setXMLStateData(const XMLStateConstructorParams& stateData)
{
    privateStateData.reset(new PrivateXmlStateClass
    {
        stateData.xmlFilepath,
        stateData.reader,
        RapidXmlReaderNode::NullNode(),
        stateData.selectedProfile,
        stateData.uuidToXMLMap,
        nullptr
    });

    if (!privateStateData->stateReader && privateStateData->xmlFilepath.empty())
    {
        throw LocalException("Either a xml node or a filepath must be given.");
    }
    else if (!privateStateData->stateReader)
    {
        privateStateData->stateNode = RapidXmlReader::FromFile(privateStateData->xmlFilepath.string())->getRoot("State");
    }
    else
    {
        privateStateData->stateNode = privateStateData->stateReader->getRoot("State");
        privateStateData->xmlFilepath.clear();
    }
}




XMLState::~XMLState()
{
}


StatechartProfilePtr XMLState::getSelectedProfile() const
{
    return privateStateData->selectedProfile;
}


void XMLState::defineParameters()
{
    //        ARMARX_INFO_S << "defineSubstates of " << State::getStateName();
    if (!privateStateData->stateNode.is_valid())
    {
        return;
    }
    StatechartContext* context = StateBase::getContext<StatechartContext>();
    if (!privateStateData->ic && context && context->getIceManager()->getCommunicator())
    {
        privateStateData->ic = context->getIceManager()->getCommunicator();
    }

    this->StateBase::inputParameters = getParameters(privateStateData->stateNode.first_node("InputParameters"));
    this->StateBase::outputParameters = getParameters(privateStateData->stateNode.first_node("OutputParameters"));
    this->StateBase::localParameters = getParameters(privateStateData->stateNode.first_node("LocalParameters"));


}


StateParameterMap XMLState::getParameters(RapidXmlReaderNode  parametersNode)
{
    StateParameterMap result;

    if (!parametersNode.is_valid())
    {
        return result;
    }



    for (RapidXmlReaderNode curParameterNode : parametersNode.nodes("Parameter"))
    {
        StateParameterDeserialization deserialization(curParameterNode, privateStateData->ic, privateStateData->selectedProfile);

        try
        {
            StateBase::addParameterContainer(result, deserialization.getName(), *deserialization.getTypePtr(), deserialization.getOptional(), deserialization.getContainer());
            //                ARMARX_VERBOSE << "Adding param: " << name << ", " << VariantContainerType::allTypesToString(typePtr) << ", optional: " << optional;
            //                ARMARX_VERBOSE << "JSON: " << jsonString;
        }
        catch (exceptions::user::UnknownTypeException& e)
        {
            ARMARX_WARNING << "The type '" << deserialization.getTypeStr() << "' is unknown, Parameter '" << deserialization.getName() << "' not added";
        }
    }

    return result;
}



void XMLState::defineSubstates()
{
    ARMARX_DEBUG << "defineSubstates of " << State::getStateName();
    if (!privateStateData->stateNode.is_valid())
    {
        ARMARX_WARNING << "stateNode is not valid!";

        return;
    }

    const std::string stateName = privateStateData->stateNode.attribute_value("name");

    addXMLSubstates(privateStateData->stateNode.first_node("Substates"), stateName);

    addTransitions(privateStateData->stateNode.first_node("Transitions"));

    setStartState(privateStateData->stateNode.first_node("StartState"));

}



void XMLState::addXMLSubstates(RapidXmlReaderNode substatesNode, const std::string& parentStateName)
{
    if (!substatesNode.is_valid())
    {
        return;
    }

    for (RapidXmlReaderNode curSubstateNode = substatesNode.first_node();
         curSubstateNode.is_valid();
         curSubstateNode = curSubstateNode.next_sibling())
    {
        addXMLSubstate(curSubstateNode, parentStateName);


    }

}

StateBasePtr XMLState::addXMLSubstate(RapidXmlReaderNode stateNode, const std::string& parentStateName)
{
    ARMARX_CHECK_EXPRESSION(privateStateData->uuidToXMLMap);

    const std::string stateType = stateNode.name();



    StateBasePtr state;

    if (boost::iequals(stateType, "LocalState"))
    {
        const std::string refuuid = stateNode.attribute_value("refuuid");
        const std::string instanceName = stateNode.attribute_value("name");

        auto it = privateStateData->uuidToXMLMap->find(refuuid);

        if (it == privateStateData->uuidToXMLMap->end())
        {
            throw LocalException("Could not find local state with UUID ") << refuuid;
        }

        RapidXmlReaderPtr substateReader = it->second;
        const std::string stateName = substateReader->getRoot("State").attribute_value("name");
        IceInternal::Handle<XMLState> xmlStateInstance = IceInternal::Handle<XMLState>::dynamicCast(XMLStateFactoryBase::fromName(stateName, XMLStateConstructorParams("", substateReader, privateStateData->selectedProfile, privateStateData->uuidToXMLMap, privateStateData->ic)));
        state = StateBasePtr::dynamicCast(xmlStateInstance);

        if (!state)
        {
            ARMARX_DEBUG << "Using state with no code for " << stateName << " refuuid: " << refuuid << " instanceName: " << instanceName;
            state = StateBasePtr::dynamicCast(NoUserCodeState::CreateInstance(XMLStateConstructorParams("", substateReader, privateStateData->selectedProfile, privateStateData->uuidToXMLMap, privateStateData->ic)));
            //            xmlStateInstance = IceInternal::Handle<XMLState>::dynamicCast(state);

        }
        //        xmlStateInstance->setXMLStateData(XMLStateConstructorParams("", substateReader, privateStateData->selectedProfile, privateStateData->uuidToXMLMap, privateStateData->ic));

        state->stateName = instanceName;
        state = addState(StatePtr::dynamicCast(state));
        ARMARX_DEBUG << "Added " << stateName << " with instanceName " << instanceName;

    }
    else if (boost::iequals(stateType, "RemoteState"))
    {
        const std::string instanceName = stateNode.attribute_value("name");
        const std::string refuuid = stateNode.attribute_value("refuuid");
        const std::string proxyName = ((!privateStateData->selectedProfile->isRoot()) ? privateStateData->selectedProfile->getName() : "") + stateNode.attribute_value("proxyName");
        ARMARX_DEBUG << "Adding remote state with refuuid " << refuuid << " and instance name: " << instanceName;
        state = State::addRemoteState(refuuid, proxyName, instanceName);
    }

    else if (boost::iequals(stateType, "DynamicRemoteState"))
    {
        const std::string instanceName = stateNode.attribute_value("name");
        ARMARX_DEBUG << "Adding dynamic remote state with instance name: " << instanceName;
        state = State::addDynamicRemoteState(instanceName);
    }
    else if (boost::iequals(stateType, "EndState"))
    {
        const std::string eventName = stateNode.attribute_value("name");
        ARMARX_DEBUG << "Adding end state with event " << eventName;
        EventPtr evt = StateUtility::createEvent(eventName);
        StatePtr  state = FinalState<>::createState(evt);
        state = addState(state);
    }

    else
    {
        throw LocalException("Unknown state type in XML - found state type: ") << stateType;
    }

    return state;
}


StatePtr XMLState::addState(StatePtr state)
{


    StateBase::__checkPhase(StateBase::eSubstatesDefinitions, __PRETTY_FUNCTION__);

    if (State::findSubstateByName(state->stateName))
    {
        throw exceptions::local::eStatechartLogicError("There exists already a substate with name '" + state->StateBase::stateName + "' in this hierarchy level. In one hierarchy level (aka one substatelist) the names must be unique.");
    }

    if (state->stateName.empty())
    {
        throw exceptions::local::eStatechartLogicError("The statename must not be empty");
    }


    state->__setParentState(this);
    this->StateBase::subStateList.push_back(state);
    StatechartContext* context = StateBase::getContext<StatechartContext>();
    state->init(context, this->StateBase::manager);

    return state;
}






void XMLState::addTransitions(const RapidXmlReaderNode& transitionsNode)
{

    if (!transitionsNode.is_valid())
    {
        ARMARX_WARNING << "transition node is not valid!";
        return;
    }

    for (RapidXmlReaderNode curTransitionNode = transitionsNode.first_node("Transition");
         curTransitionNode.is_valid();
         curTransitionNode = curTransitionNode.next_sibling("Transition"))
    {
        const std::string eventName = curTransitionNode.attribute_value("eventName");

        const std::string sourceStateName = curTransitionNode.attribute_value("from");

        if (!curTransitionNode.has_attribute("to"))
        {
            ARMARX_INFO << "Skipping detached transition " << eventName;
            continue;
        }

        const std::string destinationStateName = curTransitionNode.attribute_value("to");


        StateBasePtr source = State::findSubstateByName(sourceStateName);
        StateBasePtr destination = State::findSubstateByName(destinationStateName);

        if (!source)
        {
            throw LocalException("Could not find source state with name :") << sourceStateName;
        }

        if (!destination)
        {
            throw LocalException("Could not find source state with name :") << destinationStateName;
        }

        if (eventName.empty())
        {
            throw LocalException("Event name must not bet empty");
        }

        EventPtr event = State::createEvent(eventName);
        ARMARX_DEBUG << "Adding Transition on event " << eventName;
        ParameterMappingPtr mappingToNextStateInput = getMapping(curTransitionNode.first_node("ParameterMappings"));
        ParameterMappingPtr mappingsToParentsLocal = getMapping(curTransitionNode.first_node("ParameterMappingsToParentsLocal"));
        ParameterMappingPtr mappingsToParentsOutput = getMapping(curTransitionNode.first_node("ParameterMappingsToParentsOutput"));

        State::addTransition(event, source, destination,
                             mappingToNextStateInput, mappingsToParentsLocal, mappingsToParentsOutput);
    }
}


void XMLState::setStartState(const RapidXmlReaderNode& startNode)
{
    if (!startNode.is_valid())
    {
        return;
    }

    PMPtr mapping = getMapping(startNode.first_node("ParameterMappings"));
    this->setInitState(State::findSubstateByName(startNode.attribute_value("substateName")),
                       mapping);

}


ParameterMappingPtr XMLState::getMapping(const RapidXmlReaderNode& mappingNode)
{
    if (!mappingNode.is_valid())
    {
        return nullptr;
    }

    PMPtr mapping = PM::createMapping();

    for (RapidXmlReaderNode curMappingNode = mappingNode.first_node("ParameterMapping");
         curMappingNode.is_valid();
         curMappingNode = curMappingNode.next_sibling("ParameterMapping"))
    {
        MappingSource sourceType = PM::StringToMappingSource(curMappingNode.attribute_value("sourceType"));
        const std::string fromParamName = curMappingNode.attribute_value("from");
        const std::string targetParamName = curMappingNode.attribute_value("to");

        mapping->addMappingEntry(sourceType, fromParamName, targetParamName, GetSelectedProfileValue(curMappingNode, privateStateData->selectedProfile, privateStateData->ic));
    }

    return mapping;
}

XMLStateConstructorParams::XMLStateConstructorParams(const std::string& xmlFilepath, RapidXmlReaderPtr reader, StatechartProfilePtr selectedProfile, StringXMLNodeMapPtr uuidToXMLMap, Ice::CommunicatorPtr ic)
    : xmlFilepath(xmlFilepath),
      reader(reader),
      selectedProfile(selectedProfile),
      uuidToXMLMap(uuidToXMLMap),
      ic(ic)
{}
