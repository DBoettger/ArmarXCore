/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "GroupXmlReader.h"


#include <string>

#include <fstream>
#include <streambuf>
#include <stdexcept>
#include <boost/filesystem.hpp>

#include <ArmarXCore/core/exceptions/local/FileIOException.h>

using namespace armarx;
using namespace rapidxml;


StatechartGroupXmlReader::StatechartGroupXmlReader(const StatechartProfilePtr& selectedProfile)
    : selectedProfile(selectedProfile)
{

}

void StatechartGroupXmlReader::readXml(const boost::filesystem::path& groupDefinitionFile)
{
    RapidXmlReaderPtr wrapper = RapidXmlReader::FromFile(groupDefinitionFile.string());
    this->groupDefinitionFile = groupDefinitionFile;
    basePath = groupDefinitionFile;
    basePath = basePath.remove_filename();
    //ARMARX_IMPORTANT_S << "basePath: " << basePath.string();
    readXml(wrapper);
}

/*void StatechartGroupXmlReader::readXml(const std::string &groupDefinitionXMLString)
{
    readXml(RapidXmlReader::FromXmlString(groupDefinitionXMLString));
}*/

void StatechartGroupXmlReader::readXml(RapidXmlReaderPtr reader)
{

    RapidXmlReaderNode xmlGroupNode = reader->getRoot("StatechartGroup");
    groupName = xmlGroupNode.attribute_value("name");
    description = xmlGroupNode.first_node_value_or_default("Description", "");
    packageName = xmlGroupNode.attribute_value("package");
    generateContext = xmlGroupNode.attribute_as_optional_bool("generateContext", "true", "false", false);

    std::set<std::string> proxySet;
    proxies.clear();
    for (RapidXmlReaderNode proxyNode : xmlGroupNode.nodes("Proxies", "Proxy"))
    {
        proxySet.insert(proxyNode.attribute_value("value"));
    }
    proxySet.insert("ArmarXCoreInterfaces.systemObserver");
    proxySet.insert("ArmarXCoreInterfaces.conditionHandler");
    proxies.insert(proxies.end(), proxySet.begin(), proxySet.end());

    profileConfigurations.clear();
    for (RapidXmlReaderNode configNode : xmlGroupNode.nodes("Configurations", "Configuration"))
    {
        auto profileName = configNode.attribute_value("profileName");
        auto configuration = configNode.value();
        profileConfigurations[profileName] = configuration;
    }

    ReadChildren(xmlGroupNode, basePath, 0);
}

boost::filesystem::path StatechartGroupXmlReader::getGroupDefinitionFilePath() const
{
    return groupDefinitionFile;
}


void StatechartGroupXmlReader::ReadChildren(RapidXmlReaderNode xmlNode, const boost::filesystem::path& path, int nesting)
{
    for (RapidXmlReaderNode xmlFolderNode : xmlNode.nodes("Folder"))
    {
        boost::filesystem::path basename(xmlFolderNode.attribute_value("basename"));

        ReadChildren(xmlFolderNode, path / basename, nesting + 1);
    }

    for (RapidXmlReaderNode xmlStateNode : xmlNode.nodes("State"))
    {
        boost::filesystem::path filename(xmlStateNode.attribute_value("filename").c_str());
        std::string fullpath = (path / filename).string();
        allstateFilePaths.push_back(fullpath);
        std::string visibility = xmlStateNode.attribute_value_or_default("visibility", "");
        stateVisibilityMap[fullpath] = visibility == "public" ? ePublic : ePrivate;
        stateNestingMap[fullpath] = nesting;
    }

}
std::string StatechartGroupXmlReader::getPackageName() const
{
    return packageName;
}

bool StatechartGroupXmlReader::contextGenerationEnabled() const
{
    return generateContext;
}

StatechartGroupXmlReader::StateVisibility StatechartGroupXmlReader::getStateVisibility(const std::string& filepath) const
{
    auto it = stateVisibilityMap.find(filepath);

    if (it == stateVisibilityMap.end())
    {
        throw LocalException("State ") << filepath << " not found.";
    }

    return it->second;
}

int StatechartGroupXmlReader::getStateNestingLevel(std::string filepath) const
{
    filepath = ArmarXDataPath::cleanPath(filepath);
    auto it = stateNestingMap.find(filepath);

    if (it == stateNestingMap.end())
    {
        std::stringstream ss;

        for (std::pair<std::string, int> pair : stateNestingMap)
        {
            ss << "\n" << pair.first;
        }

        throw LocalException("State File ") << filepath << " not found in nesting map. States in map (" << stateNestingMap.size() << "):" << ss.str();
    }

    return it->second;
}

std::vector<std::string> StatechartGroupXmlReader::getProxies() const
{
    return proxies;
}

StatechartProfilePtr StatechartGroupXmlReader::getSelectedProfile() const
{
    return selectedProfile;
}

std::string StatechartGroupXmlReader::getConfigurationFileContent() const
{
    std::string result;
    auto profile = selectedProfile;
    while (profile)
    {
        std::string configs;
        if (profileConfigurations.count(profile->getName()) > 0)
        {
            configs = profileConfigurations.at(profile->getName());
        }
        if (!configs.empty())
        {
            result = "# Profile: " + profile->getName() + "\n"
                     + configs + "\n\n" + result;
        }
        profile = profile->getParent();

    }
    profile = selectedProfile;
    std::string profileProperties;
    while (profile)
    {
        profileProperties = profile->getAdditionalProperties() + profileProperties;
        profile = profile->getParent();

    }
    boost::replace_all(profileProperties, "{#GroupName#}", groupName);
    result = profileProperties + result;
    ARMARX_DEBUG_S << "ConfigurationFileContent: " << result;
    return result;
}

std::string StatechartGroupXmlReader::getDescription() const
{
    return description;
}

