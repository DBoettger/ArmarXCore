/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include "GroupXmlReader.h"

#include <ArmarXCore/core/system/cmake/CMakePackageFinder.h>
#include <ArmarXCore/observers/variant/VariantInfo.h>
#include <boost/filesystem.hpp>

namespace armarx
{

    class StatechartGroupGenerator
    {
    public:
        static Ice::StringSeq findStatechartGroupFiles(const std::string& statechartsPath);

        static void generateStateFile(const std::string& statechartGroupXmlFilePath, const std::string& statePath, const std::string& packagePath);
        static void generateStatechartContextFile(const std::string& statechartGroupXmlFilePath, const std::string& packagePath);

        static bool generateStateFile(const std::string& stateName, RapidXmlReaderPtr reader, boost::filesystem::path buildDir, const std::string& packageName, const std::string& groupName, const std::vector<std::string>& proxies, bool contextGenerationEnabled, const VariantInfoPtr& variantInfo, bool forceRewrite);
        static bool generateStatechartContextFile(boost::filesystem::path buildDir, const std::string& packageName, const std::string& groupName, const std::vector<std::string>& proxies, const VariantInfoPtr& variantInfo, const std::set<std::string>& usedVariantTypes, bool forceRewrite);

        static bool writeFileContentsIfChanged(const std::string& path, const std::string& contents);
        static void writeFileContents(const std::string& path, const std::string& contents);
        static bool generateStatechartGroupCMakeSourceListFile(const std::string& statechartGroupXmlFilePath, const boost::filesystem::path& buildDir, bool forceRewrite);
        static bool generateStatechartGroupCMakeSourceListFiles(const std::string& packageName, const std::string& statechartGroupXmlFilePath,
                const boost::filesystem::path& buildDir, bool forceRewrite, const std::string& dataDir,
                const std::map<std::string, std::string>& dependencies);

        static bool generateStatechartGroupCMakeSourceListFile(const std::string& packageName, const std::string& statechartGroupXmlFilePath,
                const boost::filesystem::path& buildDir, bool forceRewrite, const std::string& dataDir,
                const std::map<std::string, std::string>& dependencies);

        static bool generateStatechartGroupCMakeSourceListFile(StatechartGroupXmlReaderPtr reader, const boost::filesystem::path& buildDir,
                VariantInfoPtr variantInfo, bool forceRewrite);
    private:
        static std::set<std::string> getVariantTypesOfStates(StatechartGroupXmlReaderPtr groupReader, const VariantInfoPtr& variantInfo);
        static std::set<std::string> getVariantTypesOfStatesWithNoCpp(StatechartGroupXmlReaderPtr groupReader, const VariantInfoPtr& variantInfo);
        /**
         * @brief Helper function to read VariantInfo files, if root package is not yet cmaked
         */
        static VariantInfoPtr readVariantInfoWithPaths(const std::string& packageName, const std::string& buildDir, const std::string& dataDir, const std::map<std::string, std::string>& dependencies);
    };

} // namespace armarx

