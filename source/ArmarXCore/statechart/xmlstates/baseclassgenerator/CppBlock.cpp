/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "CppBlock.h"

using namespace armarx;

CppBlock::CppBlock()
{
}

void CppBlock::writeCpp(CppWriterPtr writer)
{
    writer->startBlock();

    for (std::vector<std::string>::iterator it = lines.begin(); it != lines.end(); it++)
    {
        writer->line(*it);
    }

    writer->endBlock();
}

std::string CppBlock::getAsSingleLine()
{
    std::stringstream ss;
    ss << "{ ";

    for (std::string line : lines)
    {
        ss << line << " ";
    }

    ss << "}";
    return ss.str();
}

void CppBlock::addLine(const std::string& line)
{
    lines.push_back(line);
}

void CppBlock::addLine(const boost::basic_format<char>& line)
{
    lines.push_back(boost::str(line));
}
