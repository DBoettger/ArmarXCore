#pragma once

#include "CMakeWriter.h"
#include <ArmarXCore/observers/variant/VariantInfo.h>

#include <set>
#include <string>

#include <ArmarXCore/core/rapidxml/wrapper/RapidXmlReader.h>

namespace armarx
{

    class CMakeSourceListGenerator
    {
    public:
        CMakeSourceListGenerator();

        static std::string GenerateCMakeFile(const std::string& groupName, const std::vector<std::string>& stateXMLFiles,
                                             std::vector<std::string> const& stateSourceFiles,
                                             std::vector<std::string> const& stateHeaderFiles, const std::vector<std::string>& libs);

    };

} // namespace armarx

