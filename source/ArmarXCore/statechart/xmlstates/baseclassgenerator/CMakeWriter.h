#pragma once

#include <string>
#include <sstream>
#include <boost/shared_ptr.hpp>

namespace armarx
{
    class CMakeWriter;
    typedef boost::shared_ptr<CMakeWriter> CmakeWriterPtr;

    class CMakeWriter
    {
    public:
        enum LineType {StartBlock, EndBlock, Normal, Empty};

        CMakeWriter();

        //        void startBlock();
        void startBlock(const std::string& line);
        //        void endBlock();
        void endBlock(const std::string& line);
        void endBlockComment(std::string comment);
        void line();
        void line(const std::string& line);
        void line(const std::string& line, int indentDelta);

        std::string getString();
        LineType getLastLineType();

    private:
        void lineInternal(const std::string& line, int indent);

    private:
        std::stringstream ss;
        int indent;
        std::string indentChars;
        LineType lastLineType;
    };

} // namespace armarx

