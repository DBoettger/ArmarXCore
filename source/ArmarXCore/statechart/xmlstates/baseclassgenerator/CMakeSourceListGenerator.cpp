#include "CMakeSourceListGenerator.h"

namespace armarx
{

    CMakeSourceListGenerator::CMakeSourceListGenerator()
    {

    }

    std::string CMakeSourceListGenerator::GenerateCMakeFile(const std::string& groupName, const std::vector<std::string>& stateXMLFiles, const std::vector<std::string>& stateSourceFiles, const std::vector<std::string>& stateHeaderFiles, const std::vector<std::string>& libs)
    {
        CMakeWriter writer;
        writer.line("SET(HEADERS ${HEADERS}");
        for (auto& file : stateXMLFiles)
        {
            writer.line(file, 1);
        }
        writer.line(")", 1);
        writer.line();
        writer.line("SET(HEADERS ${HEADERS}");
        for (auto& file : stateHeaderFiles)
        {
            writer.line(file, 1);
        }
        writer.line(")", 1);
        writer.line();
        writer.line("SET(SOURCES ${SOURCES}");
        for (auto& file : stateSourceFiles)
        {
            writer.line(file, 1);
        }
        writer.line(")", 1);

        writer.line();
        writer.line("SET(COMPONENT_LIBS ${COMPONENT_LIBS}");
        for (auto& file : libs)
        {
            writer.line(file, 1);
        }
        writer.line(")", 1);


        return writer.getString();
    }

} // namespace armarx
