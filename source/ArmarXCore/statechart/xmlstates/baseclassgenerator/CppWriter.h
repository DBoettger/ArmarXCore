/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <string>
#include <sstream>
#include <boost/shared_ptr.hpp>

namespace armarx
{
    class CppWriter;
    typedef boost::shared_ptr<CppWriter> CppWriterPtr;

    class CppWriter
    {
    public:
        enum LineType {StartBlock, EndBlock, Normal, Empty};

        CppWriter();

        void startBlock();
        void startBlock(const std::string& line);
        void endBlock();
        void endBlock(const std::string& line);
        void endBlockComment(std::string comment);
        void line();
        void line(const std::string& line);
        void line(const std::string& line, int indentDelta);

        std::string getString();
        LineType getLastLineType();

    private:
        void lineInternal(const std::string& line, int indent);

    private:
        std::stringstream ss;
        int indent;
        std::string indentChars;
        LineType lastLineType;
    };
}

