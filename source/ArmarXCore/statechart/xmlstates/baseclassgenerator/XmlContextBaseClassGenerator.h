/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include "CppClass.h"
#include <ArmarXCore/observers/variant/VariantInfo.h>

#include <set>
#include <string>

#include <ArmarXCore/core/rapidxml/wrapper/RapidXmlReader.h>

namespace armarx
{
    class XmlContextBaseClassGenerator
    {
    public:
        XmlContextBaseClassGenerator();

        static std::tuple<std::string, std::string> GenerateCpp(std::vector<std::string> namespaces, std::vector<std::string> proxies, std::string groupName, VariantInfoPtr variantInfo, const std::set<std::string>& usedVariantTypes);

    private:
        static std::tuple<CppClassPtr, CppClassPtr> BuildClass(std::vector<std::string> namespaces, std::vector<std::string> proxies, std::string groupName, VariantInfoPtr variantInfo, const std::set<std::string>& usedVariantTypes);
        static std::string magicNameSplit(std::string name);
        static std::string fmt(const std::string& fmt, const std::string& arg1);
        static std::string fmt(const std::string& fmt, const std::string& arg1, const std::string& arg2);
        static std::string fmt(const std::string& fmt, const std::string& arg1, const std::string& arg2, const std::string& arg3);
    };
}

