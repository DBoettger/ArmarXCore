/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "DoxDoc.h"
#include "DoxLine.h"
#include "DoxTable.h"
#include "DoxTransitiongraph.h"
#include <boost/algorithm/string/split.hpp>
#include <boost/algorithm/string/classification.hpp>
#include <boost/algorithm/string/replace.hpp>
#include <ArmarXCore/util/json/JSONObject.h>


using namespace armarx;
DoxDoc::DoxDoc()
{
}

void DoxDoc::addEntry(DoxEntryPtr entry)
{
    entries.push_back(entry);
}

void DoxDoc::addLine()
{
    addLine("");
}

void DoxDoc::writeDoc(CppWriterPtr writer)
{
    writer->line("/**");

    for (DoxEntryPtr entry : entries)
    {
        std::string doc = entry->getDoxString();
        std::string delimiters = "\n";
        std::vector<std::string> doclines;
        boost::split(doclines, doc, boost::is_any_of(delimiters));

        for (std::string line : doclines)
        {
            writer->line(" * " + line);
        }

    }

    writer->line("*/");
}


void armarx::DoxDoc::addLine(const std::string& line)
{
    DoxEntryPtr entry(new DoxLine(line));
    addEntry(entry);

}


void armarx::DoxDoc::addParameterTable(const RapidXmlReaderNode& parameters, VariantInfoPtr variantInfo, Ice::CommunicatorPtr communicator)
{
    std::vector<std::string> header;
    header.push_back("Name");
    header.push_back("Type");
    header.push_back("Default value");
    header.push_back("Description");

    DoxTablePtr table = DoxTablePtr(new DoxTable(header));


    for (RapidXmlReaderNode parameter : parameters.nodes())
    {
        std::vector<std::string> row;
        row.push_back(parameter.attribute_value("name"));
        std::string nestedType = parameter.attribute_value("type");

        if (variantInfo->getNestedHumanNameFromBaseName(nestedType).compare("") != 0)
        {
            row.push_back(variantInfo->getNestedHumanNameFromBaseName(nestedType));
        }
        else
        {
            row.push_back(nestedType);
        }

        JSONObjectPtr jsonObject = new JSONObject(communicator);
        std::string defaultValue = parameter.attribute_value_or_default("default", "");

        if (defaultValue.size() > 0)
        {
            try
            {
                jsonObject->fromString(defaultValue);
                SerializablePtr obj = jsonObject->deserializeIceObject();
                VariantContainerBasePtr result = VariantContainerBasePtr::dynamicCast(obj);
                std::string resultString = result->toString();
                resultString = boost::replace_all_copy(resultString, "\n", "<br>");
                row.push_back(resultString);
            }
            catch (Ice::Exception& e)
            {
                ARMARX_WARNING_S << "Cannot deserialize defaultValue of parameter " << parameter.attribute_value("name") << ": \"" << defaultValue << "\"\n" << e.ice_name() << ":\n" << e.what() << "\n"  << e.ice_stackTrace();
                row.push_back(" ");
            }
        }
        else
        {
            row.push_back(" ");
        }

        row.push_back(parameter.attribute_value_or_default("description", " "));

        table->addRow(row);
    }

    addEntry(table);
}


void armarx::DoxDoc::addTransitionGraph(const RapidXmlReaderNode& transitions)
{
    DoxTransitionGraphPtr graph = DoxTransitionGraphPtr(new DoxTransitionGraph);

    for (RapidXmlReaderNode transition : transitions.nodes())
    {
        if (transition.has_attribute("from") && transition.has_attribute("to") && transition.has_attribute("eventName"))
        {
            graph->addTransition(DoxTransitionPtr(new DoxTransition(transition.attribute_value("from"), transition.attribute_value("to"), transition.attribute_value("eventName"))));
        }
    }

    addEntry(graph);
}
