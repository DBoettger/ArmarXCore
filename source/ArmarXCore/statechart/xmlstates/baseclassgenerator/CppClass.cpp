/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "CppClass.h"

#include <boost/algorithm/string.hpp>
#include "DoxDoc.h"


using namespace armarx;

CppClass::CppClass(std::vector<std::string> namespaces, std::string name, std::string templates)
{
    this->name = name;
    this->namespaces = namespaces;
    this->templates = templates;
}



void CppClass::writeCpp(CppWriterPtr writer)
{
    if (!includeGuard.empty())
    {
        writer->line(boost::str(boost::format("#ifndef %s") % includeGuard));
        writer->line(boost::str(boost::format("#define %s") % includeGuard));
        writer->line();
    }

    for (std::vector<std::string>::iterator it = includes.begin(); it != includes.end(); it++)
    {
        writer->line(boost::str(boost::format("#include %s") % *it));
    }

    if (includes.size() > 0)
    {
        writer->line("");
    }

    for (std::vector<std::string>::iterator it = namespaces.begin(); it != namespaces.end(); it++)
    {
        writer->startBlock(std::string("namespace ") + *it);
    }

    if (doc)
    {
        doc->writeDoc(writer);
    }

    //addDoc(writer);

    if (!templates.empty())
    {
        writer->line(templates);
    }

    writer->line(std::string("class ") + name);

    for (size_t i = 0; i < inherit.size(); i++)
    {
        writer->line(boost::str(boost::format("%s%s%s") % (i == 0 ? ": " : "  ") % inherit.at(i) % (i < inherit.size() - 1 ? "," : "")), 1);
    }

    writer->startBlock();

    EmptyLineHelperPtr elh(new EmptyLineHelper(writer));

    if (innerClasses.size() > 0)
    {
        elh->line("protected:", -1);
    }

    for (std::vector<CppClassPtr>::iterator it = innerClasses.begin(); it != innerClasses.end(); it++)
    {
        (*it)->writeCpp(writer);
        elh->addEmptyLine();
    }

    if (privateFields.size() > 0)
    {
        elh->line("private:", -1);
    }

    for (std::vector<std::string>::iterator it = privateFields.begin(); it != privateFields.end(); it++)
    {
        writer->line(*it);
    }

    if (protectedFields.size() > 0)
    {
        elh->line("protected:", -1);
    }

    for (std::vector<std::string>::iterator it = protectedFields.begin(); it != protectedFields.end(); it++)
    {
        writer->line(*it);
    }

    if (ctors.size() > 0)
    {
        elh->line("public:", -1);
    }

    for (std::vector<CppCtorPtr>::iterator it = ctors.begin(); it != ctors.end(); it++)
    {
        (*it)->writeCpp(writer);
    }

    if (methods.size() > 0)
    {
        elh->line("public:", -1);
    }

    for (std::vector<CppMethodPtr>::iterator it = methods.begin(); it != methods.end(); it++)
    {
        (*it)->writeCpp(writer);
    }

    writer->endBlock("; // class " + name); // end of class

    for (std::vector<std::string>::reverse_iterator it = namespaces.rbegin(); it != namespaces.rend(); it++)
    {
        writer->endBlockComment(std::string("namespace ") + *it);
    }

    if (!includeGuard.empty())
    {
        writer->line();
        writer->line(boost::str(boost::format("#endif // %s") % includeGuard));
    }

}

void CppClass::WriteCpp(std::vector<CppClassPtr> classes, CppWriterPtr writer)
{
    bool foundIncludeGuard = false;
    for (CppClassPtr& cppClass : classes)
        if (!cppClass->includeGuard.empty())
        {
            foundIncludeGuard = true;
        }
    if (foundIncludeGuard)
    {
        for (CppClassPtr& cppClass : classes)
        {
            if (!cppClass->includeGuard.empty())
            {
                writer->line(boost::str(boost::format("#ifndef %s") % cppClass->includeGuard));
                writer->line(boost::str(boost::format("#define %s") % cppClass->includeGuard));
                writer->line();
                break;
            }
        }
    }

    bool foundIncludes = false;
    for (CppClassPtr& cppClass : classes)
        for (std::vector<std::string>::iterator it = cppClass->includes.begin(); it != cppClass->includes.end(); it++)
        {
            writer->line(boost::str(boost::format("#include %s") % *it));
            foundIncludes = true;
        }

    if (foundIncludes)
    {
        writer->line("");
    }
    for (CppClassPtr& cppClass : classes)
    {
        for (std::vector<std::string>::iterator it = cppClass->namespaces.begin(); it != cppClass->namespaces.end(); it++)
        {
            writer->startBlock(std::string("namespace ") + *it);
        }

        if (cppClass->doc)
        {
            cppClass->doc->writeDoc(writer);
        }

        //addDoc(writer);

        if (!cppClass->templates.empty())
        {
            writer->line(cppClass->templates);
        }

        writer->line(std::string("class ") + cppClass->name);

        for (size_t i = 0; i < cppClass->inherit.size(); i++)
        {
            writer->line(boost::str(boost::format("%s%s%s") % (i == 0 ? ": " : "  ") % cppClass->inherit.at(i) % (i < cppClass->inherit.size() - 1 ? "," : "")), 1);
        }

        writer->startBlock();

        EmptyLineHelperPtr elh(new EmptyLineHelper(writer));

        if (cppClass->innerClasses.size() > 0)
        {
            elh->line("protected:", -1);
        }

        for (std::vector<CppClassPtr>::iterator it = cppClass->innerClasses.begin(); it != cppClass->innerClasses.end(); it++)
        {
            (*it)->writeCpp(writer);
            elh->addEmptyLine();
        }

        if (cppClass->privateFields.size() > 0)
        {
            elh->line("private:", -1);
        }

        for (std::vector<std::string>::iterator it = cppClass->privateFields.begin(); it != cppClass->privateFields.end(); it++)
        {
            writer->line(*it);
        }

        if (cppClass->protectedFields.size() > 0)
        {
            elh->line("protected:", -1);
        }

        for (std::vector<std::string>::iterator it = cppClass->protectedFields.begin(); it != cppClass->protectedFields.end(); it++)
        {
            writer->line(*it);
        }

        if (cppClass->ctors.size() > 0)
        {
            elh->line("public:", -1);
        }

        for (std::vector<CppCtorPtr>::iterator it = cppClass->ctors.begin(); it != cppClass->ctors.end(); it++)
        {
            (*it)->writeCpp(writer);
        }

        if (cppClass->methods.size() > 0)
        {
            elh->line("public:", -1);
        }

        for (std::vector<CppMethodPtr>::iterator it = cppClass->methods.begin(); it != cppClass->methods.end(); it++)
        {
            (*it)->writeCpp(writer);
            elh->addEmptyLine();
        }

        writer->endBlock("; // class " + cppClass->name); // end of class

        for (std::vector<std::string>::reverse_iterator it = cppClass->namespaces.rbegin(); it != cppClass->namespaces.rend(); it++)
        {
            writer->endBlockComment(std::string("namespace ") + *it);
        }

        writer->line();
    }

    if (foundIncludeGuard)
    {
        writer->line();
        writer->line("#endif");
    }

}

void CppClass::addMethod(CppMethodPtr method)
{
    methods.push_back(method);
}

CppMethodPtr CppClass::addMethod(const std::string& header, const std::string& doc)
{
    CppMethodPtr method(new CppMethod(header, doc));
    methods.push_back(method);
    return method;
}

CppMethodPtr CppClass::addMethod(const boost::basic_format<char>& header, const std::string& doc)
{
    return addMethod(boost::str(header), doc);
}

CppCtorPtr CppClass::addCtor(const std::string arguments)
{
    CppCtorPtr ctor(new CppCtor(boost::str(boost::format("%s(%s)") % name % arguments)));
    ctors.push_back(ctor);
    return ctor;
}

void CppClass::addPrivateField(const std::string& field)
{
    privateFields.push_back(field);
}

void CppClass::addPrivateField(const boost::basic_format<char>& field)
{
    privateFields.push_back(boost::str(field));
}

void CppClass::addProtectedField(const std::string& field)
{
    protectedFields.push_back(field);
}

void CppClass::addProtectedField(const boost::basic_format<char>& field)
{
    protectedFields.push_back(boost::str(field));
}

CppClassPtr CppClass::addInnerClass(std::string name)
{
    CppClassPtr innerClass(new CppClass(std::vector<std::string>(), name));
    innerClasses.push_back(innerClass);
    return innerClass;
}

void CppClass::addInherit(const std::string& inherit)
{
    this->inherit.push_back(inherit);
}

void CppClass::addInherit(const boost::basic_format<char>& inherit)
{
    this->inherit.push_back(boost::str(inherit));
}

void CppClass::addInclude(const std::string include)
{
    includes.push_back(include);
}


void CppClass::addClassDoc(const std::string& doc)
{
    docString = doc;
}

void CppClass::addClassDoc(DoxDocPtr docPtr)
{
    doc = docPtr;
}

bool CppClass::hasInclude(const std::string include)
{
    return (std::find(includes.begin(), includes.end(), include) != includes.end());

}

std::string CppClass::getName() const
{
    return name;
}

void CppClass::setIncludeGuard(const std::string& includeGuard)
{
    this->includeGuard = includeGuard;
}


CppCtor::CppCtor(const std::string& header)
{
    this->header = header;
    this->block.reset(new CppBlock());
}

void CppCtor::addInitListEntry(const std::string& target, const std::string& expr)
{
    this->initList.push_back(boost::str(boost::format("%s(%s)") % target % expr));
}

void CppCtor::addLine(const std::string& line)
{
    block->addLine(line);
}

void CppCtor::addLine(const boost::basic_format<char>& line)
{
    block->addLine(line);
}

void CppCtor::writeCpp(CppWriterPtr writer)
{
    writer->line(header);

    for (size_t i = 0; i < initList.size(); i++)
    {
        writer->line(boost::str(boost::format("%s%s%s") % (i == 0 ? ": " : "  ") % initList.at(i) % (i < initList.size() - 1 ? "," : "")), 1);
    }

    block->writeCpp(writer);
}

void CppClass::convertToTextWithMaxWidth(unsigned int maxTextWidth, const std::vector<std::string>& docLines, std::vector<std::string>& linesOut)
{
    for (std::string line : docLines)
    {
        while (line.size() > maxTextWidth)
        {

            auto pos = line.rfind(" ", maxTextWidth);
            linesOut.push_back(line.substr(0, pos));
            line.erase(0, pos);
        }

        linesOut.push_back(line);
    }
}

void CppClass::addDoc(CppWriterPtr writer)
{
    const std::string preSpace = "  ";

    if (!docString.empty())
    {
        writer->line("/**");
        std::vector<std::string> docLines;
        boost::split(docLines,
                     docString,
                     boost::is_any_of("\n"),
                     boost::token_compress_on);
        std::vector<std::string> blockLines;
        convertToTextWithMaxWidth(80, docLines, blockLines);

        for (auto& line : blockLines)
        {
            writer->line(preSpace + line);
        }

        writer->line("*/");
    }
}
