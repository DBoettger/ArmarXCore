/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include "CppClass.h"
#include <ArmarXCore/observers/variant/VariantInfo.h>
#include "DoxDoc.h"
#include "DoxLine.h"
#include "DoxTable.h"
#include "DoxTransitiongraph.h"

#include <set>
#include <string>

#include <ArmarXCore/core/rapidxml/wrapper/RapidXmlReader.h>
#include <Ice/Communicator.h>

namespace armarx
{
    class XmlStateBaseClassGenerator
    {
    public:
        XmlStateBaseClassGenerator();

        static std::string GenerateCpp(std::vector<std::string> namespaces, RapidXmlReaderPtr reader, const std::vector<std::string>& proxies, bool contextGenerationEnabled, std::string groupName, VariantInfoPtr variantInfo);

        static std::set<std::string> GetUsedInnerNonBasicVariantTypes(RapidXmlReaderNode stateNode, VariantInfoPtr variantInfo);
        static std::set<std::string> GetIncludesForInnerNonBasicVariantTypes(RapidXmlReaderNode stateNode, VariantInfoPtr variantInfo);

    private:
        static std::vector<CppClassPtr> BuildClass(std::vector<std::string> namespaces, RapidXmlReaderNode stateNode, std::vector<std::string> proxies, bool contextGenerationEnabled, std::string groupName, VariantInfoPtr variantInfo);
        static std::set<std::string> GetUsedVariantTypes(RapidXmlReaderNode stateNode);
        static void AddGet(RapidXmlReaderNode node, CppClassPtr targetClass, VariantInfoPtr variantInfo, const std::string& parentGetMethodName);
        static void AddIsSet(RapidXmlReaderNode node, CppClassPtr targetClass, VariantInfoPtr variantInfo, const std::string& parentTestParamMethodName);
        static void AddSet(RapidXmlReaderNode node, CppClassPtr targetClass, VariantInfoPtr variantInfo, const std::string& parentGetMethodName);
        static void AddTransitionCodeFunctions(RapidXmlReaderNode stateNode, CppClassPtr targetClass);
        static std::string fmt(const std::string& fmt, const std::string& arg1);
        static std::string fmt(const std::string& fmt, const std::string& arg1, const std::string& arg2);
        static std::string fmt(const std::string& fmt, const std::string& arg1, const std::string& arg2, const std::string& arg3);
        static std::string generateDocString(RapidXmlReaderNode stateNode, const std::string& packageName, const std::vector<std::string>& namespaces, bool isPublicState);
        static std::string generateParameterTableString(RapidXmlReaderNode stateNode);
        static std::string generateDotGraphString(RapidXmlReaderNode stateNode);
        static DoxDocPtr generateDoxDoc(RapidXmlReaderNode stateNode, const std::string& packageName, std::vector<std::string>& namespaces, bool isPublicState, VariantInfoPtr variantInfo, Ice::CommunicatorPtr communicator);
    };
}

