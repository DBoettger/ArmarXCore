/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "CppWriter.h"

using namespace armarx;

CppWriter::CppWriter()
{
    indent = 0;
    indentChars = "    ";
    lastLineType = Empty;
}

void CppWriter::startBlock()
{
    this->line("{");
    indent++;
    lastLineType = StartBlock;
}

void CppWriter::startBlock(const std::string& line)
{
    this->line(line);
    this->line("{");
    indent++;
    lastLineType = StartBlock;
}

void CppWriter::endBlock()
{
    indent--;
    this->line("}");
    lastLineType = EndBlock;
}

void CppWriter::endBlock(const std::string& line)
{
    indent--;
    this->line(std::string("}") + line);
    lastLineType = EndBlock;
}

void CppWriter::endBlockComment(std::string comment)
{
    indent--;
    this->line(std::string("} // ") + comment);
    lastLineType = EndBlock;
}

void CppWriter::line()
{
    lineInternal("", indent);
}

void CppWriter::line(const std::string& line)
{
    lineInternal(line, indent);
}

void CppWriter::line(const std::string& line, int indentDelta)
{
    lineInternal(line, indent + indentDelta);
}

std::string CppWriter::getString()
{
    return ss.str();
}

CppWriter::LineType CppWriter::getLastLineType()
{
    return lastLineType;
}

void CppWriter::lineInternal(const std::string& line, int indent)
{
    for (int i = 0; i < indent; i++)
    {
        ss << indentChars;
    }

    ss << line << std::endl;
    lastLineType = line.empty() ? Empty : Normal;
}
