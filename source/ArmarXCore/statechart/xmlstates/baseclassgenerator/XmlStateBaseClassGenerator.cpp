/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "XmlStateBaseClassGenerator.h"

#include <ArmarXCore/core/system/ArmarXDataPath.h>
//#include <ArmarXCore/core/system/LibRegistryBase.h>
#include <boost/algorithm/string.hpp>
#include <boost/filesystem.hpp>

#include <ArmarXCore/core/system/cmake/CMakePackageFinder.h>
#include <ArmarXCore/core/logging/Logging.h>
#include <ArmarXCore/observers/variant/VariantContainer.h>

using namespace armarx;
using namespace boost;

XmlStateBaseClassGenerator::XmlStateBaseClassGenerator()
{

}


std::string XmlStateBaseClassGenerator::GenerateCpp(std::vector<std::string> namespaces, RapidXmlReaderPtr reader, const std::vector<std::string>& proxies, bool contextGenerationEnabled, std::string groupName, VariantInfoPtr variantInfo)
{
    auto cppClasses = BuildClass(namespaces, reader->getRoot("State"), proxies, contextGenerationEnabled, groupName, variantInfo);

    CppWriterPtr writer(new CppWriter());
    writer->line();
    CppClass::WriteCpp(cppClasses, writer);
    return writer->getString();
}

std::set<std::string> XmlStateBaseClassGenerator::GetUsedInnerNonBasicVariantTypes(RapidXmlReaderNode stateNode, VariantInfoPtr variantInfo)
{
    std::set<std::string> usedInnerNonBasicVariantDataTypes;

    for (std::string typeName : GetUsedVariantTypes(stateNode))
    {
        ContainerTypePtr type = VariantContainerType::FromString(typeName);

        // Find the inner type, all outer types are containers. If there is no container type->subType is null
        while (type->subType)
        {
            type = type->subType;
        }

        if (!variantInfo->isBasic(type->typeId))
        {
            usedInnerNonBasicVariantDataTypes.insert(type->typeId);
        }
    }

    return usedInnerNonBasicVariantDataTypes;
}

std::set<std::string> XmlStateBaseClassGenerator::GetIncludesForInnerNonBasicVariantTypes(RapidXmlReaderNode stateNode, VariantInfoPtr variantInfo)
{
    std::set<std::string> includePaths;

    for (std::string typeName : GetUsedVariantTypes(stateNode))
    {
        ContainerTypePtr type = VariantContainerType::FromString(typeName);

        // Find the inner type, all outer types are containers. If there is no container type->subType is null
        while (type->subType)
        {
            type = type->subType;
        }

        if (!variantInfo->isBasic(type->typeId))
        {
            auto libEntry = variantInfo->findLibByVariant(type->typeId);
            if (libEntry)
            {
                auto includes = libEntry->getVariantIncludes(type->typeId);
                includePaths.insert(includes.begin(), includes.end());
            }
        }
    }

    return includePaths;
}

std::vector<CppClassPtr> XmlStateBaseClassGenerator::BuildClass(std::vector<std::string> namespaces, RapidXmlReaderNode stateNode, std::vector<std::string> proxies, bool contextGenerationEnabled, std::string groupName, VariantInfoPtr variantInfo)
{
    std::vector<CppClassPtr> classes;
    std::string className = stateNode.attribute_value("name");
    std::string baseClassName = className + "GeneratedBase";

    for (std::string& ns : namespaces)
    {
        ns = boost::replace_all_copy(ns, "-", "_");
    }

    CppClassPtr cppClass(new CppClass(namespaces, baseClassName, "template<typename StateType>"));

    // auto docString = generateDocString(stateNode, packageName, namespaces, isPublicState);
    // cppClass->addClassDoc(docString);
    //    DoxDocPtr doxDoc  = generateDoxDoc(stateNode, packageName, namespaces, isPublicState, variantInfo, communicator);
    //    cppClass->addClassDoc(doxDoc);


    CppClassPtr inClass(new CppClass(namespaces, className + "In"));
    CppClassPtr localClass(new CppClass(namespaces, className + "Local"));
    CppClassPtr outClass(new CppClass(namespaces, className + "Out"));

    std::stringstream ss;
    ss << "_ARMARX_XMLUSERCODE_";

    for (auto ns : namespaces)
    {
        ss << boost::to_upper_copy(ns) << "_";
    }

    ss << boost::to_upper_copy(baseClassName) << "_H";
    inClass->setIncludeGuard(ss.str());

    inClass->addInclude("<ArmarXCore/statechart/xmlstates/XMLState.h>");

    if (contextGenerationEnabled)
    {
        /*std::stringstream ssNesting;
        for(int i = 0; i < nestingLevel; i++)
        {
            ssNesting << "../";
        }
        cppClass->addInclude(fmt("\"%s%sStatechartContext.generated.h\"", ssNesting.str(), groupName));*/
        cppClass->addInclude(fmt("\"%sStatechartContextBase.generated.h\"", groupName));
    }

    std::set<VariantInfo::LibEntryPtr> usedLibs;

    for (std::string typeName : GetUsedVariantTypes(stateNode))
    {
        ContainerTypePtr type = VariantContainerType::FromString(typeName);

        // Find the inner type, all outer types are containers. If there is no container type->subType is null
        while (type->subType)
        {
            type = type->subType;
        }

        VariantInfo::LibEntryPtr lib = variantInfo->findLibByVariant(type->typeId);

        if (lib)
        {
            usedLibs.insert(lib);
        }
        else
        {
            inClass->addInclude("MISSING INCLUDE FOR " + type->typeId);
        }
    }
    std::set<std::string> usedInnerNonBasicVariantDataTypes = GetUsedInnerNonBasicVariantTypes(stateNode, variantInfo);
    std::set<std::string> usedInnerNonBasicVariantDataImplementationTypes;
    for (auto& basicType : usedInnerNonBasicVariantDataTypes)
    {
        usedInnerNonBasicVariantDataImplementationTypes.insert(variantInfo->getDataTypeName(basicType));
    }

    std::set<std::string> variantIncludes = GetIncludesForInnerNonBasicVariantTypes(stateNode, variantInfo);
    for (auto& include : variantIncludes)
    {
        cppClass->addInclude(fmt("<%s>", include));
    }

    std::vector<VariantInfo::LibEntryPtr> usedLibsVector(usedLibs.begin(), usedLibs.end());
    std::sort(usedLibsVector.begin(), usedLibsVector.end(), [](VariantInfo::LibEntryPtr a, VariantInfo::LibEntryPtr b)
    {
        return a->getName().compare(b->getName()) < 0;
    });

    std::vector<std::string> usedInnerNonBasicVariantDataTypesVector(usedInnerNonBasicVariantDataImplementationTypes.begin(), usedInnerNonBasicVariantDataImplementationTypes.end());
    std::sort(usedInnerNonBasicVariantDataTypesVector.begin(), usedInnerNonBasicVariantDataTypesVector.end(), [](std::string a, std::string b)
    {
        return a.compare(b) > 0;
    });

    inClass->addInclude("<ArmarXCore/core/system/FactoryCollectionBase.h>");

    //    for (VariantInfo::LibEntryPtr lib : usedLibsVector)
    //    {
    //        for (std::string include : lib->getFactoryIncludes())
    //        {
    //            cppClass->addInclude(fmt("<%s>", include));
    //        }
    //    }

    cppClass->addInherit("virtual public XMLStateTemplate < StateType >");
    cppClass->addInherit("public XMLStateFactoryBase");

    cppClass->addProtectedField(format("%s in;") % inClass->getName());
    cppClass->addProtectedField(format("%s local;") % localClass->getName());
    cppClass->addProtectedField(format("%s out;") % outClass->getName());

    inClass->addPrivateField(("State *parent;"));
    localClass->addPrivateField(("State *parent;"));
    outClass->addPrivateField(("State *parent;"));

    inClass->addCtor(("State *parent"))->addInitListEntry("parent", "parent");
    localClass->addCtor(("State *parent"))->addInitListEntry("parent", "parent");
    outClass->addCtor(("State *parent"))->addInitListEntry("parent", "parent");

    CppCtorPtr ctor = cppClass->addCtor("const XMLStateConstructorParams& stateData");
    ctor->addInitListEntry("XMLState", "stateData");
    ctor->addInitListEntry("XMLStateTemplate < StateType > ", "stateData");
    ctor->addInitListEntry("in", fmt("%s(this)", inClass->getName()));
    ctor->addInitListEntry("local", fmt("%s(this)", localClass->getName()));
    ctor->addInitListEntry("out", fmt("%s(this)", outClass->getName()));

    CppCtorPtr copyCtor = cppClass->addCtor(fmt("const %s &source", baseClassName));
    copyCtor->addInitListEntry("IceUtil::Shared", "source");
    copyCtor->addInitListEntry("armarx::StateIceBase", "source");
    copyCtor->addInitListEntry("armarx::StateBase", "source");
    copyCtor->addInitListEntry("armarx::StateController", "source");
    copyCtor->addInitListEntry("armarx::State", "source");
    copyCtor->addInitListEntry("armarx::XMLState", "source");
    copyCtor->addInitListEntry("XMLStateTemplate < StateType > ", "source");
    copyCtor->addInitListEntry("in", fmt("%s(this)", inClass->getName()));
    copyCtor->addInitListEntry("local", fmt("%s(this)", localClass->getName()));
    copyCtor->addInitListEntry("out", fmt("%s(this)", outClass->getName()));

    std::set<std::string> events;
    for (RapidXmlReaderNode n : stateNode.nodes("Events", "Event"))
    {
        events.insert(n.attribute_value("name"));
    }
    for (RapidXmlReaderNode n : stateNode.nodes("Substates", "EndState"))
    {
        events.insert(n.attribute_value("name"));
    }

    for (const std::string& name : events)
    {
        CppMethodPtr emitEvent = cppClass->addMethod(fmt("void emit%s()", name));
        emitEvent->addLine(fmt("State::sendEvent(State::createEvent(\"%s\"));", name));
        CppMethodPtr installCondition = cppClass->addMethod(fmt("void installConditionFor%s(const Term& condition, const std::string& desc = \"\")", name));
        installCondition->addLine(fmt("State::installCondition(condition, State::createEvent(\"%s\"), desc);", name));
        CppMethodPtr createEvent = cppClass->addMethod(fmt("::armarx::EventPtr createEvent%s()", name));
        createEvent->addLine(fmt("return State::createEvent(\"%s\");", name));
    }

    for (RapidXmlReaderNode p : stateNode.nodes("InputParameters", "Parameter"))
    {
        AddGet(p, inClass, variantInfo, "getInput");
        AddSet(p, inClass, variantInfo, "setInput");
        AddIsSet(p, inClass, variantInfo, "isInputParameterSet");
    }

    for (RapidXmlReaderNode p : stateNode.nodes("LocalParameters", "Parameter"))
    {
        AddGet(p, localClass, variantInfo, "getLocal");
        AddSet(p, localClass, variantInfo, "setLocal");
        AddIsSet(p, localClass, variantInfo, "isLocalParameterSet");
    }

    for (RapidXmlReaderNode p : stateNode.nodes("OutputParameters", "Parameter"))
    {
        AddGet(p, outClass, variantInfo, "getOutput");
        AddSet(p, outClass, variantInfo, "setOutput");
        AddIsSet(p, outClass, variantInfo, "isOutputParameterSet");
    }

    if (contextGenerationEnabled)
    {
        for (std::string proxyId : proxies)
        {
            VariantInfo::ProxyEntryPtr p = variantInfo->getProxyEntry(proxyId);

            if (p)
            {
                std::string name = p->getMemberName();
                name[0] = toupper(name[0]);
                CppMethodPtr getProxy = cppClass->addMethod(fmt("const %s& get%s() const", p->getTypeName(), name));
                getProxy->addLine(fmt("return StateBase::getContext<%sStatechartContextBase>()->%s();", groupName, p->getGetterName()));
                //                std::string include = fmt("<%s>", p->getIncludePath());

                //                if (!cppClass->hasInclude(include))
                //                {
                //                    cppClass->addInclude(include);
                //                }

                for (std::pair<std::string, std::string> method : p->getStateMethods())
                {
                    cppClass->addMethod(method.first)->addLine(boost::replace_all_copy(method.second, "%getContext%", fmt("StateBase::getContext<%sStatechartContextBase>()", groupName)));
                }
            }
            else
            {
                ARMARX_WARNING_S << "Cannot find proxy info for " << proxyId << " for state " << className << " in group " << groupName;
                ARMARX_WARNING_S << "VariantInfo: " << variantInfo->getDebugInfo();
            }
        }
    }


    CppMethodPtr getName(new CppMethod("static std::string GetName()"));
    getName->addLine(fmt("return \"%s\";", className));
    cppClass->addMethod(getName);

    CppMethodPtr cloneFunc(new CppMethod("StateBasePtr clone() const override"));
    cloneFunc->addLine("StatePtr result = new StateType(*dynamic_cast<const StateType*>(this));");
    cloneFunc->addLine("return result;");
    cppClass->addMethod(cloneFunc);

    //    CppMethodPtr createEmptyCopyFunc(new CppMethod("StateBasePtr createEmptyCopy() const override"));
    //    createEmptyCopyFunc->addLine("return new StateType();");
    //    cppClass->addMethod(createEmptyCopyFunc);

    if (usedInnerNonBasicVariantDataTypesVector.size() > 0)
    {
        // Add special method that uses all non-basic types to force compiler/linker to include/load all libraries in binary (ensure GCC does not optimise it out).
        CppMethodPtr forceLibLoading(new CppMethod("void __attribute__((used)) __forceLibLoading()"));
        forceLibLoading->addLine("// Do not call this method.");
        forceLibLoading->addLine("// The sole purpose of this method is to force the compiler/linker to include all libraries.");

        int typeNr = 1;

        for (std::string innerNonBasicVariantDataType : usedInnerNonBasicVariantDataTypesVector)
        {
            forceLibLoading->addLine(fmt("armarx::GenericFactory< %s, %s> ().create(%s::ice_staticId());", innerNonBasicVariantDataType, innerNonBasicVariantDataType, innerNonBasicVariantDataType));

            typeNr++;
        }

        cppClass->addMethod(forceLibLoading);
    }

    AddTransitionCodeFunctions(stateNode, cppClass);

    /*CppMethodPtr createInstance(new CppMethod("static XMLStateFactoryBasePtr CreateInstance(XMLStateConstructorParams stateData)"));
    createInstance->addLine(fmt("return XMLStateFactoryBasePtr(new %s(stateData));", className));
    cppClass->addMethod(createInstance);*/
    classes.push_back(inClass);
    classes.push_back(localClass);
    classes.push_back(outClass);
    classes.push_back(cppClass);
    return classes;
}

std::set<std::string> XmlStateBaseClassGenerator::GetUsedVariantTypes(RapidXmlReaderNode stateNode)
{
    std::set<std::string> usedVariantTypes;

    for (RapidXmlReaderNode p : stateNode.nodes("InputParameters", "Parameter"))
    {
        std::string type = p.attribute_value("type");
        usedVariantTypes.insert(type);
    }

    for (RapidXmlReaderNode p : stateNode.nodes("LocalParameters", "Parameter"))
    {
        std::string type = p.attribute_value("type");
        usedVariantTypes.insert(type);
    }

    for (RapidXmlReaderNode p : stateNode.nodes("OutputParameters", "Parameter"))
    {
        std::string type = p.attribute_value("type");
        usedVariantTypes.insert(type);
    }

    return usedVariantTypes;
}


void XmlStateBaseClassGenerator::AddGet(RapidXmlReaderNode node, CppClassPtr targetClass, VariantInfoPtr variantInfo, const std::string& parentGetMethodName)
{
    std::string name = node.attribute_value("name");
    std::string type = node.attribute_value("type");
    std::string doc = node.first_node_value_or_default("Description", "");
    ContainerTypePtr containerInfo = VariantContainerType::FromString(type);

    // No Container, generated output:
    // [returnType] get[Name]() { return parent->[getInput|getLocal|getOutput]<[dataType]>("[Name]"); }
    if (!containerInfo->subType)
    {
        std::string dataType = variantInfo->getDataTypeName(type);
        std::string returnType = variantInfo->getReturnTypeName(type);
        CppMethodPtr getMethod = targetClass->addMethod(format("%s get%s() const") % returnType % name, doc);
        getMethod->addLine(format("return parent->%s< %s>(\"%s\");") % parentGetMethodName % dataType % name);
    }
    // Single Container, i.e. vector<int>, generated output:
    // [std-container]<[innerReturnType]> get[Name]() { return parent->[getInput|getLocal|getOutput]<[Variant-ContainerType]>("[Name]")->[variant-container-specific-to-std-container-method]<[innerDataType]>(); }
    // all containers are suppoted, as long as these containers are: "::armarx::StringValueMapBase" OR "::armarx::SingleTypeVariantListBase"
    // NOTE: innerDataType == innerReturnType for basic types, innerReturnType = innerDataType + "Ptr" for non basic types, see variantInfo->getDataTypeName and variantInfo->getReturnTypeName
    // ALSO NOTE: the + "Ptr" is hardcoded, so if the convention of a typedef for a ice-pointer is not met invalid code is generated.
    // examples:
    // std::vector<int> getMyParam1() { return parent->getInput< ::armarx::SingleTypeVariantList>()->toStdVector<int>(); }
    else if (!containerInfo->subType->subType)
    {
        std::string innerDataType = variantInfo->getDataTypeName(containerInfo->subType->typeId);
        std::string innerReturnType = variantInfo->getReturnTypeName(containerInfo->subType->typeId);
        std::string containerType = "NOT_FOUND";
        std::string toStdContainerMethodName = "NOT_FOUND";
        std::string returnType = "NOT_FOUND";

        if (containerInfo->typeId == "::armarx::StringValueMapBase")
        {
            containerType = "::armarx::StringValueMap";
            toStdContainerMethodName = "toStdMap";
            returnType = fmt("std::map<std::string, %s>", innerReturnType);
        }
        else if (containerInfo->typeId == "::armarx::SingleTypeVariantListBase")
        {
            containerType = "::armarx::SingleTypeVariantList";
            toStdContainerMethodName = "toStdVector";
            returnType = fmt("std::vector< %s>", innerReturnType);
        }

        CppMethodPtr getMethod = targetClass->addMethod(format("%s get%s() const") % returnType % name, doc);
        getMethod->addLine(format("return parent->%s< %s>(\"%s\")->%s::%s< %s>();") % parentGetMethodName % containerType % name % containerType % toStdContainerMethodName % innerReturnType);

    }
    // 2 or more level container: WHY U EVEN USING THIS?
    // but it is supported nonetheless
    else
    {
        std::string innerDataType = "NOT_FOUND";
        std::string innerReturnType = "NOT_FOUND";

        if (containerInfo->subType->typeId == "::armarx::StringValueMapBase")
        {
            innerDataType = "::armarx::StringValueMapPtr";
            innerReturnType = "::armarx::StringValueMapPtr";
        }
        else if (containerInfo->subType->typeId == "::armarx::SingleTypeVariantListBase")
        {
            innerDataType = "::armarx::SingleTypeVariantListPtr";
            innerReturnType = "::armarx::SingleTypeVariantListPtr";
        }

        std::string containerType = "NOT_FOUND";
        std::string toStdContainerMethodName = "NOT_FOUND";
        std::string returnType = "NOT_FOUND";

        if (containerInfo->typeId == "::armarx::StringValueMapBase")
        {
            containerType = "::armarx::StringValueMap";
            toStdContainerMethodName = "toContainerStdMap";
            returnType = fmt("std::map<std::string, %s>", innerReturnType);
        }
        else if (containerInfo->typeId == "::armarx::SingleTypeVariantListBase")
        {
            containerType = "::armarx::SingleTypeVariantList";
            toStdContainerMethodName = "toContainerStdVector";
            returnType = fmt("std::vector< %s>", innerReturnType);
        }

        CppMethodPtr getMethod = targetClass->addMethod(format("%s get%s() const") % returnType % name, doc);
        getMethod->addLine(format("return parent->%s< %s>(\"%s\")->%s::%s< %s>();") % parentGetMethodName % containerType % name % containerType % toStdContainerMethodName % innerDataType);
    }
}

void XmlStateBaseClassGenerator::AddIsSet(RapidXmlReaderNode node, CppClassPtr targetClass, VariantInfoPtr variantInfo, const std::string& parentTestParamMethodName)
{
    std::string name = node.attribute_value("name");

    CppMethodPtr getMethod = targetClass->addMethod(format("bool is%sSet() const") % name);
    getMethod->addLine(format("return parent->%s(\"%s\");") % parentTestParamMethodName % name);
}

void XmlStateBaseClassGenerator::AddSet(RapidXmlReaderNode node, CppClassPtr targetClass, VariantInfoPtr variantInfo, const std::string& parentSetMethodName)
{
    std::string name = node.attribute_value("name");
    std::string type = node.attribute_value("type");
    std::string doc = node.first_node_value_or_default("Description", "");
    ContainerTypePtr containerInfo = VariantContainerType::FromString(type);

    // No Container
    if (!containerInfo->subType)
    {
        // generate for non-pointer
        std::string dataType = variantInfo->getDataTypeName(containerInfo->typeId);
        CppMethodPtr setMethod = targetClass->addMethod(format("void set%s(const %s & value)") % name % dataType, doc);
        setMethod->addLine(format("parent->%s(\"%s\", value);") % parentSetMethodName % name);

        if (!variantInfo->isBasic(containerInfo->typeId))
        {
            // generate for pointer if type is non-basic
            std::string pointerType = containerInfo->typeId + "Ptr";
            CppMethodPtr ptrSetMethod = targetClass->addMethod(format("void set%s(const %s & value)") % name % pointerType);
            ptrSetMethod->addLine(format("parent->%s(\"%s\", *value);") % parentSetMethodName % name);
        }
    }
    else if (!containerInfo->subType->subType)
    {
        //std::string innerDataType = variantInfo->getDataTypeName(containerInfo->subType->typeId);
        std::string innerParamType = variantInfo->getReturnTypeName(containerInfo->subType->typeId);
        std::string fromStdContainerMethod = "NOT_FOUND";
        std::string paramType = "NOT_FOUND";
        std::string containerType = "NOT_FOUND";

        if (containerInfo->typeId == "::armarx::StringValueMapBase")
        {
            containerType = "::armarx::StringValueMapPtr";
            fromStdContainerMethod = fmt("::armarx::StringValueMap::FromStdMap< %s>", innerParamType);
            paramType = fmt("std::map<std::string, %s>", innerParamType);
        }
        else if (containerInfo->typeId == "::armarx::SingleTypeVariantListBase")
        {
            containerType = "::armarx::SingleTypeVariantListPtr";
            fromStdContainerMethod = fmt("::armarx::SingleTypeVariantList::FromStdVector< %s>", innerParamType);
            paramType = fmt("std::vector< %s>", innerParamType);
        }

        CppMethodPtr setMethod = targetClass->addMethod(format("void set%s(const %s & value)") % name % paramType, doc);
        setMethod->addLine(format("%s container = %s(value);") % containerType % fromStdContainerMethod);
        setMethod->addLine(format("parent->%s(\"%s\", *container);") % parentSetMethodName % name);
    }
    else
    {
        //std::string innerDataType = "NOT_FOUND";
        std::string innerParamType = "NOT_FOUND";

        if (containerInfo->subType->typeId == "::armarx::StringValueMapBase")
        {
            //innerDataType = "::armarx::StringValueMapPtr";
            innerParamType = "::armarx::StringValueMapPtr";
        }
        else if (containerInfo->subType->typeId == "::armarx::SingleTypeVariantListBase")
        {
            //innerDataType = "::armarx::SingleTypeVariantListPtr";
            innerParamType = "::armarx::SingleTypeVariantListPtr";
        }

        std::string fromStdContainerMethod = "NOT_FOUND";
        std::string paramType = "NOT_FOUND";
        std::string containerType = "NOT_FOUND";

        if (containerInfo->typeId == "::armarx::StringValueMapBase")
        {
            containerType = "::armarx::StringValueMapPtr";
            fromStdContainerMethod = fmt("::armarx::StringValueMap::FromContainerStdMap< %s>", innerParamType);
            paramType = fmt("std::map<std::string, %s>", innerParamType);
        }
        else if (containerInfo->typeId == "::armarx::SingleTypeVariantListBase")
        {
            containerType = "::armarx::SingleTypeVariantListPtr";
            fromStdContainerMethod = fmt("::armarx::SingleTypeVariantList::FromContainerStdVector< %s>", innerParamType);
            paramType = fmt("std::vector< %s>", innerParamType);
        }

        CppMethodPtr setMethod = targetClass->addMethod(format("void set%s(const %s & value)") % name % paramType, doc);
        setMethod->addLine(format("%s container = %s(value);") % containerType % fromStdContainerMethod);
        setMethod->addLine(format("parent->%s(\"%s\", *container);") % parentSetMethodName % name);
    }

}

void XmlStateBaseClassGenerator::AddTransitionCodeFunctions(RapidXmlReaderNode stateNode, CppClassPtr targetClass)
{
    auto transitionsNode = stateNode.first_node("Transitions");
    bool foundUserCodeTransition = false;
    for (RapidXmlReaderNode& tNode : transitionsNode.nodes("Transition"))
    {
        bool userCode = tNode.attribute_as_optional_bool("transitionCodeEnabled", "1", "0", false);
        if (userCode)
        {
            std::string toClass = tNode.attribute_value("toClass");
            std::string toGroup = tNode.attribute_value("toGroup");
            std::string toPackage = tNode.attribute_value("toPackage");
            std::string fromClass = tNode.attribute_value("fromClass");
            std::string fromGroup = tNode.attribute_value("fromGroup");
            std::string fromPackage = tNode.attribute_value("fromPackage");
            std::string fromStateInstanceName = tNode.attribute_value("from");
            std::string eventName = tNode.attribute_value("eventName");
            if (!toPackage.empty() && !toGroup.empty() && !toClass.empty())
            {
                targetClass->addInclude(fmt("<%s/statecharts/%s/%s.generated.h>", toPackage, toGroup, toClass));
            }
            if (!fromPackage.empty() && !fromGroup.empty() && !fromClass.empty())
            {
                targetClass->addInclude(fmt("<%s/statecharts/%s/%s.generated.h>", fromPackage, fromGroup, fromClass));
            }
            auto method = targetClass->addMethod(format("virtual void transition%sFrom%s(%s::%sIn &next, const %s::%sOut &prev)") %
                                                 eventName % fromStateInstanceName % toGroup % toClass % fromGroup % fromClass);
            method->setIsPureVirtual(true);
            foundUserCodeTransition = true;
        }
    }

    if (foundUserCodeTransition)
    {
        std::string currentStateName = stateNode.attribute_value("name");
        CppMethodPtr method = targetClass->addMethod("void defineSubstates()");
        method->addLine("XMLStateTemplate < StateType >::defineSubstates();");
        method->addLine("TransitionIceBase t;");
        for (RapidXmlReaderNode& tNode : transitionsNode.nodes("Transition"))
        {
            bool userCode = tNode.attribute_as_optional_bool("transitionCodeEnabled", "1", "0", false);
            if (userCode)
            {
                std::string toClass = tNode.attribute_value("toClass");
                std::string toGroup = tNode.attribute_value("toGroup");
                std::string fromClass = tNode.attribute_value("fromClass");
                std::string fromGroup = tNode.attribute_value("fromGroup");
                std::string fromStateInstanceName = tNode.attribute_value("from");
                std::string eventName = tNode.attribute_value("eventName");
                CppMethodPtr helperMethod = targetClass->addMethod(fmt("void _transition%sFrom%s(StateController* state, const StateIceBasePtr& nextState, const StateIceBasePtr& previousState)", eventName, fromStateInstanceName));
                helperMethod->addLine(format("%s::%sIn input(dynamic_cast<State*>(nextState.get()));") % toGroup % toClass);
                helperMethod->addLine(format("%s::%sOut output(dynamic_cast<State*>(previousState.get()));") % fromGroup % fromClass);
                helperMethod->addLine(format("%sGeneratedBase<StateType>* thisState = dynamic_cast<%sGeneratedBase<StateType>*>(state);") % currentStateName % currentStateName);
                helperMethod->addLine("ARMARX_CHECK_EXPRESSION_DEBUG_ONLY(thisState);");
                helperMethod->addLine(format("thisState->transition%sFrom%s(input, output);")
                                      % eventName % fromStateInstanceName);

                method->addLine(fmt("if (StateController::findTransition(\"%s\", \"%s\", t))", eventName, fromStateInstanceName));
                method->addLine(fmt("\tthis->addTransitionFunction(t, std::bind(&%sGeneratedBase<StateType>::_transition%sFrom%s, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3));", currentStateName, eventName, fromStateInstanceName));

            }
        }
    }

}

std::string XmlStateBaseClassGenerator::fmt(const std::string& fmt, const std::string& arg1)
{
    return str(format(fmt) % arg1);
}

std::string XmlStateBaseClassGenerator::fmt(const std::string& fmt, const std::string& arg1, const std::string& arg2)
{
    return str(format(fmt) % arg1 % arg2);
}

std::string XmlStateBaseClassGenerator::fmt(const std::string& fmt, const std::string& arg1, const std::string& arg2, const std::string& arg3)
{
    return str(format(fmt) % arg1 % arg2 % arg3);
}

std::string XmlStateBaseClassGenerator::generateDocString(RapidXmlReaderNode stateNode, const std::string& packageName, const std::vector<std::string>& namespaces, bool isPublicState)
{
    std::string className = stateNode.attribute_value("name");
    auto docNode = stateNode.first_node("Description");
    std::string docString;
    docString += "@class " + className + "GeneratedBase\n";

    if (namespaces.size() != 0)
    {
        docString += "@ingroup " + packageName + "-" + *namespaces.rbegin() + (isPublicState ? "" : "-Substates") + "\n";
    }

    if (docNode.is_valid())
    {
        docString += docNode.value();
    }

    docString += generateParameterTableString(stateNode);

    auto graphString = generateDotGraphString(stateNode);

    if (!graphString.empty())
    {
        docString += "\n\n" + graphString;
    }

    return docString;
}

std::string XmlStateBaseClassGenerator::generateParameterTableString(RapidXmlReaderNode stateNode)
{
    std::string result;


    /**
        for (RapidXmlReaderNode inputParameter : stateNode.nodes("InputParameters", "Parameter"))
        {
            result +=
        }



        auto inputParameters = stateNode.first_node("InputParameters");
        if(inputParameters.first_node("Parameter").is_valid())
        {
            result = "\n \n Input parameters: \n";
            result += "name | type | description \n";
            for (RapidXmlReaderNode curParameterNode = inputParameters.first_node();
                 curParameterNode.is_valid();
                 curParameterNode = curParameterNode.next_sibling("Parameter"))
            {
                result
            }

        }
    */


    return result;


}


std::string XmlStateBaseClassGenerator::generateDotGraphString(RapidXmlReaderNode stateNode)
{
    std::string result;
    auto transitions = stateNode.first_node("Transitions");

    if (transitions.first_node("Transition").is_valid())
    {

        result = "\n \nTransitiongraph of the substates:\n";
        result += "@dot\n\tdigraph States {\n";

        for (RapidXmlReaderNode curTransitionNode = transitions.first_node("Transition");
             curTransitionNode.is_valid();
             curTransitionNode = curTransitionNode.next_sibling("Transition"))
        {
            if (curTransitionNode.has_attribute("from")
                && curTransitionNode.has_attribute("to")
                && curTransitionNode.has_attribute("eventName"))
            {
                result += "\t\t" + curTransitionNode.attribute_value("from") + "->" +
                          curTransitionNode.attribute_value("to")
                          + "[ label=\"" + curTransitionNode.attribute_value("eventName") + "\" ];\n";
            }
        }

        result += "}\n@enddot";
        return result;
    }

    return "";
}

DoxDocPtr XmlStateBaseClassGenerator::generateDoxDoc(RapidXmlReaderNode stateNode, const std::string& packageName, std::vector<std::string>& namespaces, bool isPublicState, VariantInfoPtr variantInfo, Ice::CommunicatorPtr communicator)
{
    std::string className = stateNode.attribute_value("name");
    DoxDocPtr doc(new DoxDoc());

    doc->addLine(fmt("@defgroup %s-%s-State %s", packageName, className, className));
    std::string doxGroupName = fmt("%s-%s", packageName, namespaces.at(namespaces.size() - 1));

    if (!isPublicState)
    {
        doxGroupName += "-Substates";
    }

    doc->addLine(fmt("@ingroup %s\n", doxGroupName));

    std::string doxBrief;
    std::string doxDescription = stateNode.first_node_value_or_default("Description", "");

    if (doxDescription.size() > 0)
    {
        std::vector<std::string> descLines;
        boost::split(descLines, doxDescription, boost::is_any_of("."));
        doxBrief = descLines[0];
        doc->addLine(fmt("@brief %s\n", doxBrief));
    }
    else
    {
        doxBrief = fmt("@brief Brief description of state %s\n", className);
        doc->addLine(doxBrief);
    }


    // input parameters
    RapidXmlReaderNode inputParameters = stateNode.first_node("InputParameters");

    if (inputParameters.first_node("Parameter").is_valid())
    {
        doc->addLine("Input parameters:");
        doc->addParameterTable(inputParameters, variantInfo, communicator);
    }

    // local parameters
    RapidXmlReaderNode localParameters = stateNode.first_node("LocalParameters");

    if (localParameters.first_node("Parameter").is_valid())
    {
        doc->addLine("Local parameters:");
        doc->addParameterTable(localParameters, variantInfo, communicator);
    }

    // output parameters
    RapidXmlReaderNode outputParameters = stateNode.first_node("OutputParameters");

    if (outputParameters.first_node("Parameter").is_valid())
    {
        doc->addLine("Output parameters");
        doc->addParameterTable(outputParameters, variantInfo, communicator);
    }

    RapidXmlReaderNode transitions = stateNode.first_node("Transitions");

    if (transitions.first_node("Transition").is_valid())
    {
        doc->addLine("Transition graph");
        doc->addTransitionGraph(transitions);
    }


    doc->addLine(fmt("@class %sGeneratedBase", className));

    if (namespaces.size() != 0)
    {
        //doc->addLine(fmt("@ingroup %s-%s%s", packageName, namespaces.at(namespaces.size() - 1), isPublicState ? "" : "-Substates"));
        //doc->addLine(fmt("@ingroup %s", doxGroupName));
        doc->addLine(fmt("@ingroup %s-%s-State", packageName, className));
    }

    doc->addLine(fmt("For a documentation of this state see @ref %s-%s-State", packageName, className));

    if (doxBrief.size() > 0)
    {
        doc->addLine(doxBrief);
    }

    doc->addLine(doxDescription.substr(0, 80));


    return doc;
}
