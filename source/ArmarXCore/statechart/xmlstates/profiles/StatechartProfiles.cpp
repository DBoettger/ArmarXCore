/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "StatechartProfiles.h"

#include <ArmarXCore/core/system/cmake/CMakePackageFinder.h>
#include <boost/algorithm/string.hpp>
#include <boost/filesystem.hpp>
#include <ArmarXCore/core/logging/Logging.h>


using namespace armarx;

StatechartProfiles::StatechartProfiles()
    : rootProfile(new StatechartProfile(StatechartProfiles::GetRootName(), StatechartProfilePtr()))
{
}

void StatechartProfiles::readStatechartProfiles(RapidXmlReaderPtr reader)
{
    RapidXmlReaderNode node = reader->getRoot("Profiles");
    rootProfile->readFromXml(node);
}



StatechartProfilesPtr StatechartProfiles::ReadProfileFiles(const std::vector<std::string>& packages)
{
    StatechartProfilesPtr profiles(new StatechartProfiles());

    for (std::string project : packages)
    {
        boost::filesystem::path profilesFile(CMakePackageFinder(project).getDataDir().c_str());
        profilesFile /= project;
        profilesFile /= "StatechartProfiles-" + project + ".xml";

        if (boost::filesystem::exists(profilesFile))
        {
            try
            {
                RapidXmlReaderPtr xmlReader = RapidXmlReader::FromFile(profilesFile.string());
                profiles->readStatechartProfiles(xmlReader);
            }
            catch (std::exception& e)
            {
                ARMARX_ERROR_S << "Reading " << profilesFile.string() << " failed: " << e.what();
            }
        }
        else
        {
            ARMARX_DEBUG_S << "StatechartProfiles File not found for project " << project << ": " << profilesFile.string();
        }
    }

    return profiles;
}

std::vector<StatechartProfilePtr> StatechartProfiles::getAllLeaves() const
{
    std::vector<StatechartProfilePtr> profiles;
    getAllLeaves(rootProfile, profiles);
    return profiles;
}

std::set<std::string> StatechartProfiles::getAllLeafNames() const
{
    std::set<std::string> result;
    auto leaves = getAllLeaves();
    for (StatechartProfilePtr& leaf : leaves)
    {
        result.insert(leaf->getName());
    }
    return result;
}

StatechartProfilePtr StatechartProfiles::getProfileByName(std::string name)
{
    if (name == "")
    {
        return rootProfile;
    }

    return rootProfile->findByNameRecursive(name);
}

StatechartProfilePtr StatechartProfiles::getRootProfile() const
{
    return rootProfile;
}

void StatechartProfiles::getAllLeaves(StatechartProfilePtr currentProfile, std::vector<StatechartProfilePtr>& profiles) const
{
    if (currentProfile->children.size() == 0)
    {
        profiles.push_back(currentProfile);
    }
    else
    {
        for (StatechartProfilePtr p : currentProfile->children)
        {
            getAllLeaves(p, profiles);
        }
    }
}


StatechartProfile::StatechartProfile(const std::string& name, StatechartProfilePtr parent)
{
    this->name = name;
    this->parent = parent;
}

void StatechartProfile::readFromXml(RapidXmlReaderNode node)
{
    for (RapidXmlReaderNode packageNode : node.nodes("Package"))
    {
        std::string packageName = packageNode.attribute_value("name");

        if (std::find(packages.begin(), packages.end(), packageName) == packages.end())
        {
            packages.push_back(packageName);
        }
    }
    if (node.has_node("Properties"))
    {
        additionalProperties += "#Properties from Statechart profile file: " + node.attribute_value_or_default("name", "NOT-FOUND") + "\n" + node.first_node_value("Properties") + "\n\n";
    }


    for (RapidXmlReaderNode profileNode : node.nodes("Profile"))
    {
        std::string name = profileNode.attribute_value("name");
        StatechartProfilePtr profile = getChildByName(name); // Check if profile has already been read previously. Merge profiles if necessary.

        if (profile)
        {
            ARMARX_INFO_S << "Merging Profile " << name;
        }
        else
        {
            profile.reset(new StatechartProfile(profileNode.attribute_value("name"), shared_from_this()));
            children.push_back(profile);
        }
        profile->readFromXml(profileNode);
    }
}

bool StatechartProfile::isLeaf() const
{
    return children.size() == 0;
}

bool StatechartProfile::isRoot() const
{
    return !parent.lock();
}

std::vector<std::string> StatechartProfile::getAllPackages() const
{
    std::vector<std::string> packages;
    StatechartProfilePtr parent = this->parent.lock();

    if (parent)
    {
        packages = parent->getAllPackages();
    }

    for (std::string package : this->packages)
    {
        if (std::find(packages.begin(), packages.end(), package) == packages.end())
        {
            packages.push_back(package);
        }
    }

    return packages;
}

std::string StatechartProfile::getName() const
{
    return name;
}

std::string StatechartProfile::getFullName() const
{
    StatechartProfilePtr parent = this->parent.lock();

    if (!parent)
    {
        return name;    // root node returns "ROOT"
    }

    if (parent->isRoot())
    {
        return name;    // avoid adding "::" in the front
    }

    return parent->getFullName() + "::" + name;
}

StatechartProfilePtr StatechartProfile::findByNameRecursive(std::string name)  const
{
    if (this->name == name)
    {
        return const_cast<StatechartProfile*>(this)->shared_from_this();
    }

    for (StatechartProfilePtr p : children)
    {
        StatechartProfilePtr match = p->findByNameRecursive(name);

        if (match)
        {
            return match;
        }
    }

    return StatechartProfilePtr();
}

const std::vector<StatechartProfilePtr>& StatechartProfile::getChildren() const
{
    return children;
}

StatechartProfilePtr StatechartProfile::getChildByName(std::string name) const
{
    for (StatechartProfilePtr p : children)
    {
        if (p->name == name)
        {
            return p;
        }
    }

    return StatechartProfilePtr();
}

StatechartProfilePtr StatechartProfile::getParent() const
{
    return parent.lock();
}

int StatechartProfile::getNesting() const
{
    return isRoot() ? 1 : getParent()->getNesting() + 1;
}

std::string StatechartProfile::getStatechartGroupPrefix() const
{
    return isRoot() ? "" : getName();
}

std::string StatechartProfile::getAdditionalProperties() const
{
    return additionalProperties;
}

