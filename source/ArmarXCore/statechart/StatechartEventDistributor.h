/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

// ArmarX Includes
//#include "StateController.h"
#include <ArmarXCore/core/ManagedIceObject.h>
#include <ArmarXCore/interface/statechart/StatechartIce.h>

#include <boost/thread/mutex.hpp>

#include <string>
#include <queue>


namespace armarx
{
    class StateController;
    typedef IceInternal::Handle<StateController> StateControllerPtr;

    class StatechartManager;
    typedef IceUtil::Handle<StatechartManager> StatechartManagerPtr;

    class StateBase;
    typedef IceInternal::Handle<StateBase> StateBasePtr;

    class Event;
    typedef IceInternal::Handle<Event> EventPtr;

    /**
     * @brief The StatechartEventDistributor class is used to distribute
     * incomming events (via Ice) to the correct states.<br/>
     *
     *
     * The events must be registered before they can be distributed.
     * The normal process is to register events here, when the condition or
     * something else is registered on the observer. The registerEvent()-function
     * returns a unique event-string that is to be passed to the observer.<br/>
     * A pointer to this class is passed to the observer, so that the observer
     * can call back.<br/>
     *
     * @note This class is an internal statechart-class and is used by the
     * classes StatechartContext and StateUtility. There is usually only one
     * instance per statechart (it is located in the StatechartContext).
     */
    class StatechartEventDistributor:
        virtual public EventListenerInterface,
        virtual public ManagedIceObject
    {
    public:

        struct EventListenerMapEntry
        {
            StateControllerPtr eventProcessor;
            EventPtr event;
            std::string globalEventIdentifier;
            int useCounter;
            long visitId;
        };


        StatechartEventDistributor();

        void setStatechartManager(const StatechartManagerPtr& statechartManager);
        void setStatechartName(std::string statechartName)
        {
            this->statechartName = statechartName;
            setName(getDefaultName());
        }
        // inherited from Component
        std::string getDefaultName() const override
        {
            return statechartName + "s_EventDistributor";
        }

        typedef std::map<std::string, EventListenerMapEntry> EventListenerMap;


        std::string registerEvent(const EventPtr& event, StateBasePtr eventProcessor, long visitId);
        void reportEvent(const EventBasePtr& event, const Ice::Current&) override;
        std::string getUniqueEventIdentifier(const StateBasePtr& eventProcessor, const EventPtr& event, long visitId);
        void clearEventMap();
        EventListenerInterfacePrx getListener();
    protected:
        std::queue< std::pair<StateControllerPtr, EventPtr > > eventBuffer;
        EventListenerMap eventMap;
        std::string statechartName;
    private:
        // inherited from Component
        void onInitComponent() override;
        void onConnectComponent() override;
        EventListenerInterfacePrx myProxy;
        StatechartManagerPtr statechartManager;

        Mutex eventMapMutex;
        Mutex eventBufferMutex;
        Mutex eventProcessorMutex;
    };
}
