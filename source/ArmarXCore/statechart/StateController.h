/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::statechart
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include "StateBase.h"

#include <queue>
#include <vector>
#include <functional>

namespace armarx
{
    class StateController;
    typedef IceInternal::Handle<StateController> StateControllerPtr;
    template <typename Type>
    class RunningTask;

    namespace Profiler
    {
        class Profiler;
        typedef boost::shared_ptr<Profiler> ProfilerPtr;
        typedef std::set<ProfilerPtr> ProfilerSet;
    }

    /**
     *\class StateController
     *\ingroup StatechartGrp
     * The StateController class processes events and controls the
     * statechart flow.
     */
    class StateController :
        virtual public StateBase
    {
    public:
        typedef std::function<void(StateController* state, const StateIceBasePtr& nextState, const StateIceBasePtr& previousState)> transitionFunction;
        typedef std::map<std::string, transitionFunction > TransitionFunctionMap;

        StateController();
        StateController(const StateController& source);
        ~StateController() override;

        bool isFinished() const;
        /**
         * @brief waitForStateToFinish waits until this thread has finished
         * (i.e. a FinalSubState has been reached).<br/>
         *
         * This function can only be called in a toplevelstate (a state with
         * no parent state).
         *
         * @param timeOutMs
         * @see isFinished()
         */
        void waitForStateToFinish(int timeoutMs = -1) const;


        /**
         * @brief disableRunFunction sets useRunFunction to false and waits
         * (blocking) for the current StateBase::run() function to complete.
         *
         * @note This function needs to be called <b>before</b> the destructor of the
         * state is called. If not called before destructor, the StateBase::run()
         * function might cause a segfault.
         */
        void disableRunFunction();

        /**
         * @brief isRunningTaskStopped checks whether the RunningTask, that
         * executes run() is requested to stop. Check this function periodically in
         * run()
         * @return status of runningtask
         */
        bool isRunningTaskStopped() const;

        /**
         * @brief Checks whether the run() function has already finished
         * @return true if finished
         */
        bool isRunningTaskFinished() const;

        /**
         * @brief Waits until the run-function has finished. This might be useful to do in onBreak() or onEnter()
         * @see onEnter(), onBreak(), onRun()
         */
        void waitForRunningTaskToFinish() const;

        /**
         * @brief Disables the reporting to profilers for this states during state visits.
         */
        void disableStateReporting(bool disable = true);

        void addTransitionFunction(const TransitionIceBase& t, transitionFunction function);
        std::string getTransitionID(const TransitionIceBase& t) const;
        std::string getTransitionID(const std::string& eventName, const std::string sourceStateName) const;
        bool findTransition(const std::string& eventName, const std::string sourceStateName, TransitionIceBase& transition);
    protected:

        /*! \brief Function to set the statemachine in the first state and call OnEnter().
         *
         * Should only be called for the outer most state(machine).
         * All initial substates of this state and of it's substates are also entered.
         * @param inputparameters Dictionary of parameters that should be mapped into the
         * input parameters of this state. Uses '*'=>'*' mapping.
        */
        void enter(const StringVariantContainerBaseMap& tempInputParameters = StringVariantContainerBaseMap());


        /**
         * \brief Called by StateControllerprocessEvent()-function or parentstate. Must NOT
         * be called by user.
         *
         * Calls OnEnter() in this hierarchylevel and of every initialstate of all sub levels.
         */
        virtual void _baseOnEnter() ;

        virtual void _startRun();
        virtual void _baseRun();

        /*! \brief Called by StateController::processEvent()-function or parentstate. Must NOT
        be called by user.

            Calls OnExit() in this hierarchylevel and all sub levels.
            @relates baseOnExit()
          */
        virtual void _baseOnExit() ;

        /*! \brief Called by StateControllerprocessEvent()-function or parentstate. Must NOT be called by user.

          Calls OnBreak() in this hierarchylevel and all sub levels.
          */
        virtual bool _baseOnBreak(const EventPtr evt);


        /**
         * @brief addProfilerRecursive recursively adds a new armarx::Profiler::Profiler object as armarx::StateController::localProfiler(the default one does not do anything at all).
         *
         * \p recursiveLevels determins how many levels deep this new profiler object should be set:
         *
         * \li == -1: all reachable substates
         * \li == 0: only current state
         * \li > 0: current state + \p recursiveLevels deep)
         *
         * This is automtically set in armarx::StatechartContext::setToplevelState() based on statechart properties.
         *
         * @param profiler profiler instance to use in the state for calling armarx::Profiler::Profiler methods
         * @param recursiveLevels how many levels deep the profiler should be set
         */
        void addProfilerRecursive(Profiler::ProfilerPtr profiler, int recursiveLevels = 0);
        void removeProfilerRecursive(Profiler::ProfilerPtr profiler, int recursiveLevels = 0);

    private:
        IceUtil::Handle<RunningTask<StateController> > __runningTask;

        /*! \brief Buffer for all events that are
        delayed due to unbreakable substates.

        This buffer only holds events that are processed by this state.
          */
        std::queue<EventPtr> __unbreakableBuffer;
        bool __eventBufferedDueToUnbreakableState;
        std::vector< std::pair<StateControllerPtr, EventPtr> > __eventBuffer;
        TransitionFunctionMap transitionFunctions;


        /**
         * @brief Getter function that automatically casts the parentState
         * member of StateBase into StateControllerPtr. @throw LocalException
         * if parent state could not be cas @return Pointer to the parent state
         */
        StateControllerPtr __getParentState() const;

        virtual bool __breakActiveSubstate(const EventPtr event);

        /*! \brief Function that gets called, when a state enters a FinalSubstate.
          Virtual function, so that RemoteStateWrapper can override it.
          */
        virtual void __finalize(const EventPtr event);

        /*! \brief Function that gets called, when a state enters a FinalSubstate.
          Virtual function, so that RemoteStateWrapper can override it.
          */
        virtual void __substatesFinished(const EventPtr ev);

        virtual void __enqueueEvent(const EventPtr event);

        /**
         * @brief This function is implemented in StateUtil and removes all conditions
         * that have been installed in onEnter() or run(). It is called after onExit()
         */
        virtual void _removeInstalledConditions() {}


        /** \brief Before:Function to get the unbreakable-buffer status of all
        parent state - recursively. * now: checks the flag for the buffer, that
        gets set externally @return Returns true if no parent has any events in
        the unbreakable buffer, false otherwise.
          */
        virtual bool __getUnbreakableBufferStati() const;

        /*! \brief Main function to control the statemachine/state.

            Determines with Parameter \c event, the active state and the
            transitiontable, which transition to choose.<br/> If no transition
            is found, an eUnexpectedEvent-exception is thrown.
          */
        virtual void __processEvent(const EventPtr event, bool buffered = false);

        /**
         * @brief Apply the mappings during a transitions. The order is parent's local,
         * parent's output and then next state's input.
         * @param srcState
         * @param t Transition that is currently executed
         * @param event Event that led to the transition
         */
        bool __applyMappings(const StateControllerPtr& srcState, const TransitionIceBase& t, const EventPtr& event, TransitionError& error);
        void __printTransitionError(const TransitionError& transitionError, const EventPtr& event) const;


        bool __findValidTransition(const EventPtr& event, const StateIceBasePtr& sourceState, TransitionIceBase& resultTransition, TransitionError& error) const;
        TransitionError __validateTransition(const TransitionIceBase& transition, const EventPtr event, const StateIceBasePtr& sourceState, const StateIceBasePtr& destinationState) const;

        virtual unsigned int __getUnbreakableBufferSize() const;

        /*! This virtual function notifies all substates that an event has been
        * buffered due to unbreakable states and that they must not process any
        * further events when they have reached a breakable state.
        **/
        virtual void __notifyEventBufferedDueToUnbreakableState(bool eventBuffered = true);

        /*! \brief Processes buffered events, that could not be processed immediately due to unbreakable substates.

            Gets called after a transition from a unbreakable substate to a breakable substate after execution of OnEnter() of the breakable state.<br/>
            It is not called yet, when a transition occurs between 2 unbreakable states.<br/>
            The calling substate can be of any level below this state in the hierarchy.<br/>
          */
        virtual void __processBufferedEvents();

        void __waitForRemoteStates() const;

        bool __checkExistenceOfTransition(const TransitionIceBase& transition);

        bool __finished;
        mutable Mutex __finishedMutex;
        mutable boost::condition_variable __finishedCondition;

        /**
         * @brief localProfiler local instance of armarx::Profiler::Profiler which does nothing by default
         *
         * This instance can be changed via armarx::StateController::setProfilerRecursive() (\see armarx::RemoteStateOfferer)
         */
        Profiler::ProfilerSet localProfilers;
        bool profilersDisabled;

        friend class State;
        friend class StateUtility;
        template <class EventType, class StateType> friend class FinalStateBase;
        template <typename ContextType> friend class RemoteStateOfferer;
        friend class RemoteState;
        friend class DynamicRemoteState;
        friend class StatechartEventDistributor;
        friend class StatechartManager;
        friend class StatechartContext;
    };
}
