/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Statechart
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#define BOOST_TEST_MODULE ArmarX::Statechart::StatechartUnbreakableStateTest
#define ARMARX_BOOST_TEST
#include <ArmarXCore/Test.h>

#include <ArmarXCore/statechart/Statechart.h>
#include <ArmarXCore/core/test/IceTestHelper.h>

using namespace armarx;


// Define Events before the first state they are used in
DEFINEEVENT(EvInit)  // this macro declares a new event-class derived vom Event, to have a compiletime check for typos in events
DEFINEEVENT(EvNext)

struct StateResult;
struct StateRun;


struct Statechart_StateBreakableTest : StateTemplate<Statechart_StateBreakableTest>
{

    void defineState() override
    {
        //        setStateName("AdditionTest");
    }

    void defineSubstates() override
    {
        //add substates

        StatePtr stateRun = addState<StateRun>("Running");
        setInitState(stateRun);
        StatePtr stateSuccess = addState<SuccessState>("Success"); // preimplemented state derived from FinalStateBase, that sends triggers a transition on the upper state



        // add transitions
        addTransition<EvNext>(stateRun, stateSuccess);
        addTransition<Success>(stateRun, stateSuccess);



    }



};

struct UnbreakableState;
struct StateRun :
    virtual public StateTemplate<StateRun>

{
    void defineState() override
    {


    }

    void defineSubstates() override
    {
        StatePtr unbreakableInitState = addState<UnbreakableState>("UnbreakableState");
        StatePtr successState = addState<SuccessState>("Success");

        setInitState(unbreakableInitState);

        addTransition<EvNext>(unbreakableInitState, successState);
    }

    void onEnter() override
    {

    }

    void run() override
    {
        ARMARX_LOG << "run of " << getStateName();
        usleep(500000);
        sendEvent<EvNext>();

    }


};

struct UnbreakableState :
    virtual public StateTemplate<UnbreakableState>

{

    void defineState() override
    {
        setUnbreakable();
    }

    void onEnter() override
    {

    }
    void run() override
    {
        ARMARX_LOG << "run of " << getStateName();
        usleep(1500000);
        sendEvent<EvNext>();

    }

};


BOOST_AUTO_TEST_CASE(StatechartUnbreakableStateTest1)
{
    StatePtr statechart;
    StatechartManager manager;

    try
    {

        statechart = Statechart_StateBreakableTest::createInstance();
        statechart->init(nullptr, &manager);
        manager.setToplevelState(statechart);
        BOOST_CHECK(statechart->isInitialized());
        manager.start();
        statechart->waitForStateToFinish();
    }
    catch (LocalException& e)
    {
        std::cout << "exception: " << e.what() << std::endl;
    }

}

