/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include <ArmarXCore/statechart/Statechart.h>
#include <ArmarXCore/statechart/StateUtilFunctions.h>
#include <ArmarXCore/core/test/IceTestHelper.h>

#include <ArmarXCore/core/system/cmake/CMakePackageFinder.h>

#include <ArmarXCore/core/system/FactoryCollectionBase.h>
#include <ArmarXCore/observers/ConditionHandler.h>
#include <ArmarXCore/observers/SystemObserver.h>
#include <ArmarXCore/observers/ProfilerObserver.h>

#include <ArmarXCore/core/IceManager.h>

#include <ArmarXCore/core/ManagedIceObject.h>
#include <IceUtil/UUID.h>

#include "ArmarXCore/interface/statechart/RemoteStateOffererIce.h" //RemoteStateOffererInterfacePrx
#include <ArmarXCore/core/IceGridAdmin.h>

namespace armarx
{
    struct StatechartScenarioTestStateWatcher : armarx::ManagedIceObject, armarx::EventListenerInterface
    {

    public:
        void reportEvent(const armarx::EventBasePtr& event, const Ice::Current&) override
        {
        }

        std::string getDefaultName() const override
        {
            return "StatechartScenarioTestStateWatcher";
        }

        /**
         * @brief Waits until the running statechart has reached a final state or a specified amount of time has passed.
         * @param timeoutMS the amount of milliseconds to wait before throwing time-out-exception
         */
        void waitForStateChartFinished(int timeoutMS)
        {
            scenarioStateMutex.lock();

            if (finished)
            {
                scenarioStateMutex.unlock();
                return;
            }

            // wait for finishing
            boost::unique_lock<boost::mutex> lock(finishingMutex);
            scenarioStateMutex.unlock();
            if (!finishedCondition.timed_wait(lock, boost::posix_time::milliseconds(timeoutMS)))
            {
                throw LocalException("Timeout while waiting for statechart");
            }
        }

        /**
         * @return the output parameters of the statechart being run.
         */
        armarx::StringVariantContainerBaseMap getStatechartOutput()
        {
            armarx::ScopedLock guard(scenarioStateMutex);
            return StateUtilFunctions::getValues(statechartOutput);
        }

    protected:
        void onInitComponent() override
        {
            usingProxy("ConditionHandler");
            usingProxy("ProfilerObserver");
            usingProxy("RobotControlStateOfferer");
        }

        void onConnectComponent() override
        {
            assignProxy(conditionHandler, "ConditionHandler");
            assignProxy(profilerObserver, "ProfilerObserver");
            remoteStateOfferer = getIceManager()->getProxy<armarx::RemoteStateOffererInterfacePrx>("RobotControlStateOfferer");
            subscriptionTask = new armarx::PeriodicTask<StatechartScenarioTestStateWatcher>(this, &StatechartScenarioTestStateWatcher::subscriptionFunction, 200);
            subscriptionTask->start();
        }

        void onDisconnectComponent() override
        {
            subscriptionTask->stop();

        }

        /**
         * @brief Checks whether the running statechart has finished and if so
         * it extracts the output parameters of the statechart.
         */
        void subscriptionFunction()
        {
            armarx::StateIceBasePtr activeFunctionalSubState;
            armarx::StateIceBasePtr functionalState;
            try
            {
                armarx::StateIdNameMap stateMap = remoteStateOfferer->getAvailableStateInstances();
                if (stateMap.size() > 0)
                {
                    functionalState = remoteStateOfferer->refetchRemoteSubstates(stateMap.begin()->first)->activeSubstate;
                    activeFunctionalSubState = functionalState->activeSubstate;
                }

                if (activeFunctionalSubState)
                {
                    if (ExtractStateName(activeFunctionalSubState->globalStateIdentifier) == "RobotInitialized" && !statechartMainStateName.empty())
                    {
                        armarx::StateList functionalSubStates = functionalState->subStateList;
                        for (armarx::StateIceBasePtr& statePtr : functionalSubStates)
                        {
                            if (ExtractStateName(statePtr->globalStateIdentifier) == statechartMainStateName)
                            {
                                armarx::ScopedLock guard(scenarioStateMutex);
                                statechartOutput = statePtr->outputParameters;
                                break;
                            }
                        }

                        armarx::ScopedLock guard(scenarioStateMutex);

                        // notify that statechart has finished
                        boost::unique_lock<boost::mutex> lock(finishingMutex);
                        finishedCondition.notify_all();

                        finished = true;

                        subscriptionTask->stop();
                    }
                    else
                    {
                        statechartMainStateName = activeFunctionalSubState->inputParameters.find("stateName")->second->value->output();
                    }
                }
                else
                {
                    throw LocalException("No active state found in " + remoteStateOfferer->ice_getIdentity().name);
                }
            }
            catch (...)
            {
                armarx::handleExceptions();
            }
        }

        /**
         * @brief Returns the name of the state with the given globalIdString
         * @param globalIdString the global id of a state
         * @return the name of the state specified by the globalIdString
         */
        std::string ExtractStateName(const std::string globalIdString)
        {
            if (globalIdString.find("->") == std::string::npos)
            {
                return globalIdString;
            }
            return globalIdString.substr(globalIdString.find_last_of("->") + 1);
        }

        armarx::PeriodicTask<StatechartScenarioTestStateWatcher>::pointer_type subscriptionTask;
        armarx::ConditionHandlerInterfacePrx conditionHandler;
        armarx::ProfilerObserverInterfacePrx profilerObserver;
        armarx::RemoteStateOffererInterfacePrx remoteStateOfferer;

    private:
        bool finished = false;
        armarx::Mutex scenarioStateMutex;

        armarx::Mutex finishingMutex;
        boost::condition_variable finishedCondition;

        armarx::StateParameterMap statechartOutput;
        std::string statechartMainStateName;

    };
    typedef IceInternal::Handle<StatechartScenarioTestStateWatcher> StatechartScenarioTestStateWatcherPtr;


    /**
     * @class StatechartScenarioTestEnvironment
     * @brief The StatechartScenarioTestEnvironment class provides an environment where scenarios
     * can be started and stopped in the same Ice context specified in the constructor.
     */
    class StatechartScenarioTestEnvironment
    {
    public:
        /**
         * @brief Constructor for the environment which starts an own ice context.
         * @param testName the name of this environment
         * @param registryPort specifies the port for Ice context
         */
        StatechartScenarioTestEnvironment(const std::string& testName, int registryPort = 11220) :
            armarXCorePackage("ArmarXCore")
        {
            this->testName = testName;
            this->registryPort = registryPort;
            Ice::PropertiesPtr properties = Ice::createProperties();

            iceTestHelper = new IceTestHelper(registryPort, registryPort + 1);
            iceTestHelper->startEnvironment();
            manager = new TestArmarXManager(testName, iceTestHelper->getCommunicator(), properties);
            properties->setProperty("ArmarX.ProfilerObserver.MaxHistorySize", "0");
            manager->createComponentAndRun<armarx::ProfilerObserver, armarx::ProfilerObserverInterfacePrx>("ArmarX", "ProfilerObserver", properties);

            // starting watcher
            watcher = new StatechartScenarioTestStateWatcher();
            manager->addObject(watcher, "StatechartScenarioTestStateWatcher" + IceUtil::generateUUID());
        }

        /**
         * @brief Destructor which stops all running scenarios.
         */
        ~StatechartScenarioTestEnvironment()
        {
            manager->removeObjectNonBlocking(watcher->getName());
            for (auto scenario : this->startedScenarios)
            {
                stopScenario(scenario.first, scenario.second);
                usleep(2000000); //without this we might get exceptions from the scenario components that still communicate with the ice manager
            }
            manager->shutdown();
        }

        /**
         * @brief Starts a scenario within the ice context of this environment.
         * @param packageName specifies the package of the scenario
         * @param scenarioName specifies the name of the scenario
         */
        void startScenario(std::string packageName, std::string scenarioName)
        {
            this->startedScenarios.push_back(std::pair<std::string, std::string>(packageName, scenarioName));

            armarx::CMakePackageFinder finder(packageName);
            if (finder.packageFound() && armarXCorePackage.packageFound())
            {
                std::string armarXCoreBinDir = armarXCorePackage.getBinaryDir();
                std::string scenarioDir = finder.getScenariosDir();
                std::string stopCommand = armarXCoreBinDir + "/ScenarioCliRun kill " + scenarioDir + "/" + scenarioName + "/" + scenarioName + ".scx";
                {
                    int result = system(stopCommand.c_str());
                    if (result)
                    {
                        ARMARX_ERROR << "command '" << stopCommand << "' failed with error code " << result;
                    }
                }
                std::string command = armarXCoreBinDir + "/ScenarioCliRun start " + scenarioDir + "/" + scenarioName + "/" + scenarioName + ".scx";
                command = command + " --Ice.Default.Locator=\"IceGrid/Locator:tcp -p " + to_string(this->registryPort) + " -h localhost\"";
                command = command + " --parameters=\"--Ice.Default.Locator=\\\"IceGrid/Locator:tcp -p " + to_string(this->registryPort) + " -h localhost\\\"" + " --IceGrid.Registry.Client.Endpoints=\\\"tcp -p "  + to_string(this->registryPort) + "\\\"\"";
                {
                    int result = system(command.c_str());
                    if (result)
                    {
                        ARMARX_ERROR << "command '" << command << "' failed with error code " << result;
                    }
                }
            }
            else
            {
                throw LocalException() << "Could not find package " << packageName;
            }
        }

        /**
         * @brief Starts a scenario within the Ice context of this environment and then waits until the component with the given name
         * is reachable or a specified amount of time has passed.
         * @param packageName specifies the package of the scenario
         * @param scenarioName specifies the name of the scenario
         * @param componentToWaitFor the name of the component to wait for
         * @param maxWaitTimeMS the amount of milliseconds this method keeps waiting for the component before continuing
         */
        void startScenario(std::string packageName, std::string scenarioName, std::string componentToWaitFor, int maxWaitTimeMS)
        {
            startScenario(packageName, scenarioName);
            int t = 0;
            while (!manager->getIceManager()->isObjectReachable(componentToWaitFor) && t < maxWaitTimeMS * 1000)
            {
                usleep(5000);
                t += 5000;
            }
        }

        /**
         * @brief Stops a scenario.
         * @param packageName specifies the package of the scenario
         * @param scenarioName specifies the name of the scenario
         */
        void stopScenario(std::string packageName, std::string scenarioName, int signal = 2)
        {
            armarx::CMakePackageFinder finder(packageName);
            if (finder.packageFound() && armarXCorePackage.packageFound())
            {
                std::string armarXCoreBinDir = armarXCorePackage.getBinaryDir();
                std::string scenarioDir = finder.getScenariosDir();
                std::string command = armarXCoreBinDir + "/ScenarioCliRun ";
                if (signal != 2)
                {
                    command += "kill ";
                }
                else
                {
                    command += "stop ";
                }
                command += scenarioDir + "/" + scenarioName + "/" + scenarioName + ".scx";
                {
                    int result = system(command.c_str());
                    if (result)
                    {
                        ARMARX_ERROR << "command '" << command << "' failed with error code " << result;
                    }
                }
            }
        }

        armarx::SystemObserverInterfacePrx systemObserver;
        TestArmarXManagerPtr manager;
        IceTestHelperPtr iceTestHelper;
        StatechartScenarioTestStateWatcherPtr watcher;
        std::string testName;

    protected:
        armarx::CMakePackageFinder armarXCorePackage;
    private:
        int registryPort;
        std::list<std::pair<std::string, std::string> > startedScenarios;
    };

    typedef boost::shared_ptr<StatechartScenarioTestEnvironment> StatechartScenarioTestEnvironmentPtr;

}
