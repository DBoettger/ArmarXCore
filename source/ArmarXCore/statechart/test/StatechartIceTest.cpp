/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Statechart
* @author     Mirko Waechter( mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/


#define BOOST_TEST_MODULE ArmarX::Statechart::StatechartIceTest
#define ARMARX_BOOST_TEST
#include <ArmarXCore/Test.h>
#include <ArmarXCore/core/test/IceTestHelper.h>

#include <ArmarXCore/statechart/Statechart.h>
#include <ArmarXCore/core/system/DynamicLibrary.h>
#include <ArmarXCore/core/system/ArmarXDataPath.h>
#include <ArmarXCore/core/system/FactoryCollectionBase.h>
#include <ArmarXCore/observers/ConditionHandler.h>
#include <ArmarXCore/observers/SystemObserver.h>
#include "StatechartTestEnv.h"

using namespace armarx;

//! [StatechartContext MyCustomStatechartContext]
class MyCustomStatechartContext :
    public StatechartContext
{


    // PropertyUser interface
public:
    //    PropertyDefinitionsPtr createPropertyDefinitions();

    // ManagedIceObject interface
protected:
    std::string getDefaultName() const override
    {
        return "MyCustomStatechartContext";
    }

    // StatechartContext interface
protected:
    void onInitStatechartContext() override
    {

    }

    void onConnectStatechartContext() override
    {

    }
};
//! [StatechartContext MyCustomStatechartContext]



// Define Events before the first state they are used in
DEFINEEVENT(EvInit)  // this macro declares a new event-class derived vom Event, to have a compiletime check for typos in events
DEFINEEVENT(EvNext)

struct StateResult;
struct StateRun;
struct StateInit;
struct Level1RemoteState : StateTemplate<Level1RemoteState>
{

    void defineParameters() override
    {
        addToOutput("counter", VariantType::Int, false);
        //        setStateName("AdditionTest");
    }

    void defineSubstates() override
    {
        PMPtr mapping = createMapping()->mapFromOutput("*");
        //add substates

        StatePtr stateInit = addState<StateInit>("Init");
        setInitState(stateInit);
        StatePtr stateRun = addState<StateRun>("Running");
        StateBasePtr stateResult =  addRemoteState("Level0RemoteState", "Level0RemoteStateOfferer");

        StatePtr stateSuccess = addState<SuccessState>("Success"); // preimplemented state derived from FinalStateBase, that sends triggers a transition on the upper state


        // add transitions
        addTransition<EvNext>(stateInit, stateRun,
                              mapping);
        addTransition<EvNext>(stateRun, stateResult,
                              mapping);
        addTransition<Success>(stateResult, stateRun,
                               mapping);
        addTransition<Success>(stateRun, stateSuccess,
                               mapping);


        // ...add more transitions
    }



};

struct StateInit : StateTemplate<StateInit>
{
    void defineParameters() override
    {
        addToOutput("counter", VariantType::Int, false);
    }

    void onEnter() override
    {

        setOutput("counter", (int)0);
        sendEvent<EvNext>();

    }

};

struct StateRun : StateTemplate<StateRun>
{
    void defineParameters() override
    {
        addToInput("counter", VariantType::Int, false);
        addToOutput("counter", VariantType::Int, false);
    }

    void onEnter() override
    {
        setOutput("counter",  getInput<int>("counter"));

        if (getInput<int>("counter") > 3)
        {
            sendEvent<Success>();
        }
        else
        {
            sendEvent<EvNext>();
        }
    }

};



//struct StateRunFunction : StateTemplate<StateRunFunction>
//{

//    void defineParameters()
//    {
//        addToInput("counter", VariantType::Int, false);
//        addToOutput("counter", VariantType::Int, false);
//    }

//    void run()
//    {
//        //Variant var(ChannelRefPtr(new ChannelRef(NULL,"")));
//        ARMARX_VERBOSE << "run()";
//        //setLocal("test",var);
//        setLocal("bool", true);
//        getLocal<bool>("bool");
//        sendEvent<Success>();
//    }
//};

//struct Statechart_StateRunFunctionTest : StateTemplate<Statechart_StateRunFunctionTest>
//{

//    void defineSubstates()
//    {
//        StatePtr run = addState<StateRunFunction>("StateRunFunction");
//        StatePtr success = addState<SuccessState>("StateSuccess");

//        setInitState(run);

//        addTransition<Success>(run, success);
//    }



//};

struct InstantExitState : StateTemplate<InstantExitState>
{

    void defineParameters() override
    {
        addToInput("counter", VariantType::Int, false);
        addToOutput("counter", VariantType::Int, false);
    }

    void defineSubstates() override
    {

        setInitState(addState<State>("DummySubState"));
        addState<FailureState>("FailureState");
    }

    void onEnter() override
    {
        setOutput("counter", getInput<int>("counter") + 1);
        ARMARX_INFO << "Sending event Success";
        sendEvent<Success>();

    }



};

class Level0RemoteStateStateOfferer : public RemoteStateOfferer<MyCustomStatechartContext>
{


    // RemoteStateOfferer interface
public:
    std::string getStateOffererName() const override
    {
        return "Level0Remote";
    }
    void onInitRemoteStateOfferer() override
    {
        addState<InstantExitState>("Level0RemoteState");
    }
};



BOOST_AUTO_TEST_CASE(StatechartRemoteStateTest)
{
    LogSender::SetGlobalMinimumLoggingLevel(eDEBUG);
    Ice::PropertiesPtr properties = Ice::createProperties();
    StatechartEnvironment env("StatechartRemoteStateTest");
    StatechartContextPtr context = StatechartContextPtr::dynamicCast(Component::create<Level0RemoteStateStateOfferer>());
    BOOST_CHECK(context);


    StatePtr statechart;
    StatechartManager manager;
    int result;

    try
    {
        env.manager->addObject(context);
        context->getObjectScheduler()->waitForDependencies(3000);
        statechart = Level1RemoteState::createInstance();
        statechart->init(context.get(), &manager);

        manager.setToplevelState(statechart);
        manager.start();

        statechart->waitForStateToFinish(10000);
        result = statechart->getOutput<int>("counter");
        ;

    }
    catch (...)
    {
        handleExceptions();
    }

    try
    {
        manager.shutdown();
        context->getArmarXManager()->shutdown();
    }
    catch (...)
    {
        handleExceptions();
        //        throw;
        BOOST_CHECK_EQUAL("caught exception", "must not catch exception - see exception above");
    }

    BOOST_CHECK_EQUAL(result, 4);
}


class Level1RemoteStateOffererTest : public RemoteStateOfferer<MyCustomStatechartContext>
{


    // RemoteStateOfferer interface
public:
    std::string getStateOffererName() const override
    {
        return "Level1Remote";
    }
    void onInitRemoteStateOfferer() override
    {
        addState<Level1RemoteState>("Level1RemoteState");
    }
};

struct Level2RemoteState : StateTemplate<Level2RemoteState>
{

    void defineParameters() override
    {
        addToOutput("counter", VariantType::Int, false);
    }

    void defineSubstates() override
    {
        PMPtr mapping = createMapping()->mapFromOutput("*");
        //add substates

        StateBasePtr stateInit =  addRemoteState("Level1RemoteState", "Level1RemoteStateOfferer");
        setInitState(stateInit);
        StatePtr stateSuccess = addState<SuccessState>("Success"); // preimplemented state derived from FinalStateBase, that sends triggers a transition on the upper state


        // add transitions

        addTransition<Success>(stateInit, stateSuccess,
                               mapping);


        // ...add more transitions
    }



};


BOOST_AUTO_TEST_CASE(Statechart3LevelRemoteStateTest)
{
    LogSender::SetGlobalMinimumLoggingLevel(eDEBUG);
    Ice::PropertiesPtr properties = Ice::createProperties();
    StatechartEnvironment env("StatechartRemoteStateTest");
    StatechartContextPtr context = StatechartContextPtr::dynamicCast(Component::create<Level0RemoteStateStateOfferer>());
    StatechartContextPtr context2 = StatechartContextPtr::dynamicCast(Component::create<Level1RemoteStateOffererTest>());
    StatechartEnvironment env2("StatechartRemoteStateTest2", 11220, false);

    BOOST_CHECK(context);


    StatePtr statechart;
    StatechartManager manager;
    int result = 0;

    try
    {
        env.manager->addObject(context);
        env2.manager->addObject(context2);
        context2->getObjectScheduler()->waitForDependencies(3000);
        context->getObjectScheduler()->waitForDependencies(3000);
        statechart = Level2RemoteState::createInstance("Level2RemoteState");
        statechart->init(context2.get(), &manager);



        manager.setToplevelState(statechart);
        manager.start();
        statechart->waitForStateToFinish(10000);
        result = statechart->getOutput<int>("counter");

    }
    catch (...)
    {
        handleExceptions();
        //        throw;
        BOOST_CHECK_EQUAL("caught exception", "must not catch exception - see exception above");
    }

    try
    {
        manager.shutdown();
        context->getArmarXManager()->shutdown();
        context2->getArmarXManager()->shutdown();
    }
    catch (...)
    {
        handleExceptions();
        //        throw;
        BOOST_CHECK_EQUAL("caught exception", "must not catch exception - see exception above");
    }

    BOOST_CHECK_EQUAL(result, 4);

}






