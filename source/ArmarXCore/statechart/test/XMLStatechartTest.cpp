/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Statechart
* @author     Mirko Waechter( mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/
#define BOOST_TEST_MODULE ArmarX::Statechart::XMLStatechartTest
#define ARMARX_BOOST_TEST
#include <ArmarXCore/Test.h>

#include <ArmarXCore/statechart/Statechart.h>
#include <ArmarXCore/core/test/IceTestHelper.h>
#include <ArmarXCore/core/system/DynamicLibrary.h>
#include <ArmarXCore/core/system/ArmarXDataPath.h>
#include <ArmarXCore/core/system/FactoryCollectionBase.h>
#include <ArmarXCore/statechart/xmlstates/GroupXmlReader.h>
#include <ArmarXCore/statechart/xmlstates/XMLState.h>
#include <ArmarXCore/statechart/xmlstates/XMLRemoteStateOfferer.h>
#include <ArmarXCore/observers/variant/VariantInfo.h>
using namespace armarx;




BOOST_AUTO_TEST_CASE(StatechartGroupReadXMLTest)
{
    std::string xmlString = "<?xml version=\"1.0\" encoding=\"utf-8\"?>  \
            <StatechartGroup name=\"TestGroup\" package=\"TestPackage\">                        \
                <Description>Line1                                      \
            Line2                                                       \
            Line3</Description>                                         \
                <Folder basename=\"Top Level Folder\">                  \
                    <Folder basename=\"Empty Folder\"/>                 \
                    <Folder basename=\"Helpers\">                       \
                        <State filename=\"SubState2.xml\"/>             \
                        <State filename=\"SubState3.xml\"/>             \
                        <State filename=\"SubState4.xml\"/>             \
                    </Folder>                                           \
                    <State filename=\"SubState1.xml\"/>                 \
                </Folder>                                               \
                <Folder basename=\"Umlaute äöü\"/>                      \
                <State filename=\"MyTopLevelState.xml\"/>               \
                <State filename=\"SubState.xml\"/>                      \
                <State filename=\"TestState.xml\"/>                     \
            </StatechartGroup>";

    StatechartGroupXmlReaderPtr reader(new StatechartGroupXmlReader(StatechartProfilePtr()));
    reader->readXml(RapidXmlReader::FromXmlString(xmlString));
    ARMARX_INFO_S << reader->getStateFilepaths();

}

class TestState : public XMLState
{
public:
    TestState(XMLStateConstructorParams stateData) : XMLState(stateData)
    {

    }
    void test()
    {
        init(nullptr, nullptr);
        //        defineParameters();
        //        defineSubstates();
    }
};


BOOST_AUTO_TEST_CASE(ReadXMLStateTest)
{
    return;
    Ice::CommunicatorPtr ic;
    ic = Ice::initialize();

    ArmarXManager::RegisterKnownObjectFactoriesWithIce(ic);


    std::string xmlInput = "<?xml version=\"1.0\" encoding=\"utf-8\"?><State version=\"1.0\" name=\"RootStateName\" uuid=\"09F54A1A-3A29-4560-B20C-299069288823\" width=\"123\" height=\"456\"><Description>Test description\nLine 2</Description><InputParameters><Parameter name=\"InputParameter1Name\" type=\"string\" optional=\"no\" default='{\"defaultValue\":{\"type\":-2125418521,\"value\":\"DefaultValue\"}}'><Description>Input Parameter 1 Description</Description></Parameter><Parameter name=\"InputParameter2Name\" type=\"float\" optional=\"yes\" default='{\"defaultValue\":{\"type\":1826906882,\"value\":13.36999988555908}}'/></InputParameters><OutputParameters><Parameter name=\"OutputParameter1Name\" type=\"string\" optional=\"yes\"/></OutputParameters><LocalParameters><Parameter name=\"LocalParameter1Name\" type=\"bool\" optional=\"no\" default='{\"defaultValue\":{\"type\":-869238820,\"value\":false}}'/></LocalParameters><Substates><EndState name=\"EndSubstateName\" event=\"Success\" left=\"12\" top=\"34\" boundingSquareSize=\"50\"/><LocalState name=\"LocalSubstateName\" refuuid=\"CF3662A8-9234-46BD-82C1-856344108823\" left=\"12\" top=\"34\" boundingSquareSize=\"50\"/><RemoteState name=\"RemoteSubstateName\" refuuid=\"148C7D1D-1DB4-4A67-893F-D22D36A88823\" proxyName=\"RemoteSubstateProxyName\" left=\"12\" top=\"34\" boundingSquareSize=\"50\"/></Substates><Events><Event name=\"Success\"><Description>Event Description</Description></Event><Event name=\"Failure\"/></Events><StartState substateName=\"LocalSubstateName\"><ParameterMappings><ParameterMapping sourceType=\"Parent\" from=\"StartParameterMapping1SourceKey\" to=\"StartParameterMapping1DestinationKey\"/><ParameterMapping sourceType=\"Output\" from=\"StartParameterMapping2SourceKey\" to=\"StartParameterMapping2DestinationKey\"/></ParameterMappings></StartState><Transitions><Transition from=\"LocalSubstateName\" to=\"RemoteSubstateName\" eventName=\"Transition1EventName\"><ParameterMappings><ParameterMapping sourceType=\"Event\" from=\"Transition1ParameterMappingSourceKey\" to=\"Transition1ParameterMappingDestinationKey\"/></ParameterMappings><SupportPoints><SupportPoint posX=\"12\" posY=\"34\"/><SupportPoint posX=\"56\" posY=\"78\"/></SupportPoints></Transition><Transition from=\"RemoteSubstateName\" to=\"EndSubstateName\" eventName=\"Transition2EventName\"><ParameterMappings/><SupportPoints/></Transition></Transitions></State>";

    RapidXmlReaderPtr reader = RapidXmlReader::FromXmlString(xmlInput);
    RapidXmlReaderNode root = reader->getRoot("State");
    ARMARX_INFO_S << "state:" << root.name();
    IceInternal::Handle<TestState> state = new TestState(XMLStateConstructorParams("", reader, StatechartProfilePtr(), StringXMLNodeMapPtr(new StringXMLNodeMap), ic));
    state->test();
}


class TestXMLOfferer :
    public XMLRemoteStateOfferer<>
{
    TestXMLOfferer(StatechartGroupXmlReaderPtr reader) :
        XMLRemoteStateOfferer<>(reader)
    {

    }

    static XMLStateOffererFactoryBasePtr CreateInstance(StatechartGroupXmlReaderPtr reader)
    {
        return XMLStateOffererFactoryBasePtr(new TestXMLOfferer(reader));
    }

    static std::string GetName()
    {
        return "TestXMLOfferer";
    }
    static SubClassRegistry Registry;
};

TestXMLOfferer::SubClassRegistry TestXMLOfferer::Registry(TestXMLOfferer::GetName(), &TestXMLOfferer::CreateInstance);




BOOST_AUTO_TEST_CASE(VariantInfoTest)
{
    VariantInfoPtr info = VariantInfo::ReadInfoFilesRecursive("ArmarXCore", "", false);
    std::string lib = info->findLibByVariant("::armarx::ChannelRefBase")->getAbsoluteLibPath();
    ARMARX_INFO_S << VAROUT(lib);
    BOOST_CHECK(!lib.empty());

    BOOST_CHECK_THROW(info->findLibByVariant("::armarx::ChannelRefBase2")->getAbsoluteLibPath(), LocalException);

}
