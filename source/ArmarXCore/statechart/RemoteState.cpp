/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarXCore::Statechart
* @author     Mirko Waechter( mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/
#include "RemoteState.h"
#include "StateUtilFunctions.h"
#include <IceUtil/IceUtil.h>
#include <ArmarXCore/statechart/StatechartObjectFactories.h>
#include <ArmarXCore/core/application/Application.h>
#include "standardstates/FinalState.h"
#include <ArmarXCore/core/ArmarXObjectScheduler.h>

namespace armarx
{
    RemoteState::RemoteState()
    {
        stateName = "";
        stateType = eRemoteState;
        remoteStateId = -1;
        proxyName = "";

        setName(getDefaultName());
    }

    RemoteState::RemoteState(const RemoteState& source) :
        IceUtil::Shared(source),
        Ice::Object(source),
        StateIceBase(source),
        RemoteStateInterface(source),
        RemoteStateIceBase(source),
        Logging(source),
        StateBase(source),
        StateController(source),
        State(source),
        ManagedIceObject(source)
    {
        myProxy = nullptr;

        if (false)
        {
            remoteStateId = source.remoteStateId;
        }
        else
        {
            remoteStateId = -1;
        }

        stateOffererPrx = source.stateOffererPrx;
        setName(getDefaultName());
    }

    RemoteState& RemoteState::operator =(const RemoteState& source)
    {
        assert(0);
        return *this;
    }

    RemoteState::~RemoteState()
    {
        if (remoteStateId >= 0 && stateOffererPrx)
        {
            ARMARX_DEBUG << "Removing remoteinstance" << flush;

            try
            {
                stateOffererPrx->removeInstance(remoteStateId);
            }
            catch (Ice::NotRegisteredException& e)
            {
                ARMARX_WARNING << "Could not remove instance of RemoteState '" << stateClassName << "'; probably the remote component is not alive anymore" << flush;
            }
        }
    }

    void RemoteState::setStateName(const std::string& stateName)
    {
        if (!stateName.empty())
        {
            this->stateName = stateName;
        }

        setName(getDefaultName());
    }

    void RemoteState::setProxyName(const std::string& proxyName)
    {
        if (!proxyName.empty())
        {
            this->proxyName = proxyName;
        }

        setName(getDefaultName());
    }


    void RemoteState::onInitComponent()
    {
        usingProxy(proxyName);
    }

    void RemoteState:: onConnectComponent()
    {
        //ARMARX_INFO << "getting proxy " << proxyName <<  "\n" << flush;

        stateOffererPrx = getProxy<RemoteStateOffererIceBasePrx>(proxyName);

        if (!stateOffererPrx)
        {
            throw exceptions::local::eNullPointerException("Couldnt get remote state proxy");
        }

        //ARMARX_INFO <<  "stateoffererprxy: " <<stateOffererPrx._ptr << flush;
        myProxy = RemoteStateIceBasePrx::checkedCast(getProxy());
        StateUtilFunctions::copyDictionary(stateOffererPrx->getRemoteInputParameters(stateClassName), inputParameters);

        //        ARMARX_INFO << "Getting input of " << stateClassName << "input: " << StateUtilFunctions::getDictionaryString(inputParameters);
        //        StateUtilFunctions::unsetParameters(inputParameters);
        StateUtilFunctions::copyDictionary(stateOffererPrx->getRemoteOutputParameters(stateClassName), outputParameters);
        remoteStateId = stateOffererPrx->createRemoteStateInstance(stateClassName, myProxy, globalStateIdentifier, stateName);
        StateIceBasePtr state = stateOffererPrx->getStatechartInstance(remoteStateId);

        subStateList = state->subStateList;
        transitions = state->transitions;
        activeSubstate = state->activeSubstate;
        initState = state->initState;

    }

    void RemoteState::onDisconnectComponent()
    {
        if (getStatePhase() >= eEntering && getStatePhase() < eBreaking)
        {
            ARMARX_INFO << "Remote State '" << this->getStateName() << "' was disconnected during execution -> sending failure event";
            // Event only needed when this state is currently active
            EventPtr evt = new Failure(this->getStateName());
            __enqueueEvent(evt);
        }

        try
        {
            if (remoteStateId != -1 && stateOffererPrx)
            {
                stateOffererPrx->removeInstance(remoteStateId);
            }
        }
        catch (...)
        {

        }

        remoteStateId = -1;
        stateOffererPrx = nullptr;

    }


    StateBasePtr RemoteState::clone() const
    {
        RemoteStatePtr result = new RemoteState(*this);

        if (getArmarXManager())
        {
            getArmarXManager()->addObject(ManagedIceObjectPtr::dynamicCast(result), false);
            //            ARMARX_DEBUG << "Waiting for RemoteState '" << result->getName() << "'";

            result->getObjectScheduler()->waitForObjectStateMinimum(eManagedIceObjectStarted);
        }

        return result;
    }


    StateBasePtr RemoteState::createEmptyCopy() const
    {
        RemoteStatePtr result = new RemoteState();

        result->setName(result->getDefaultName());
        result->setStateName(stateName);
        result->setStateClassName(getStateClassName());
        result->setProxyName(proxyName);

        getArmarXManager()->addObject(ManagedIceObjectPtr::dynamicCast(result), false);
        return result;
    }

    std::string RemoteState::getDefaultName() const
    {
        std::stringstream componentName;
        componentName << "RemoteState_" << localUniqueId << "_" << proxyName  << "_" << globalStateIdentifier;
        return componentName.str();
    }




    void
    RemoteState::remoteProcessBufferedEvents(const ::Ice::Current& c)
    {
        if (!__getParentState())
        {
            throw exceptions::local::eNullPointerException("parentState in remoteState is NULL");
        }
        else
        {
            __getParentState()->__processBufferedEvents();
        }
    }

    void RemoteState::remoteProcessEvent(const EventBasePtr& evt, bool buffered, const Ice::Current& c)
    {
        ARMARX_DEBUG << "entering remoteProcessEvent()" <<  "\n" << flush;

        if (__getParentState())
        {
            __getParentState()->__processEvent(EventPtr::dynamicCast(evt), buffered);
        }
        else
        {
            throw exceptions::local::eNullPointerException("parentstate of remoteState " + stateName + " is NULL");
        }
    }

    void RemoteState::remoteEnqueueEvent(const EventBasePtr& evt, const Ice::Current& c)
    {
        //        ARMARX_LOG << eINFO << "entering remoteEnqueueEvent()" <<  "\n" << flush;
        if (!__getParentState())
        {
            throw exceptions::local::eNullPointerException("parentstate of remoteState " + stateName + " is NULL");
        }

        __enqueueEvent(EventPtr::dynamicCast(evt));

    }

    void RemoteState:: remoteFinalize(const StringVariantContainerBaseMap& properties, const EventBasePtr& event, const Ice::Current& c)
    {
        //        ARMARX_LOG << eINFO << "RemoteState::finalize: " << stateName <<  "\n" << flush;
        StateUtilFunctions::fillDictionary(properties, outputParameters);

        //        remoteStateFinished = true;
        if (!__getParentState())
        {
            throw exceptions::local::eNullPointerException("parentstate of remoteState " + stateName + " is NULL");
        }

        __finalize(EventPtr::dynamicCast(event));

    }



    Ice::Int RemoteState::getRemoteUnbreakableBufferSize(const Ice::Current&) const
    {
        if (!__getParentState())
        {
            throw exceptions::local::eNullPointerException("parentstate of remoteState " + stateName + " is NULL");
        }

        try
        {
            return __getParentState()->__getUnbreakableBufferSize();
        }
        catch (...)
        {
            return 0;
        }

    }

    bool RemoteState::getRemoteUnbreakableBufferStati(const Ice::Current&) const
    {
        if (!__getParentState())
        {
            throw exceptions::local::eNullPointerException("parentstate of remoteState " + stateName + " is NULL");
        }

        try
        {
            return __getParentState()->__getUnbreakableBufferStati();
        }
        catch (...)
        {
            return true;
        }
    }




    bool RemoteState::__hasSubstates()
    {
        if (!stateOffererPrx || remoteStateId == -1)
        {
            return false;
        }

        try
        {
            return stateOffererPrx->hasSubstatesRemote(stateClassName);
        }
        catch (...)
        {
            return false;
        }
    }

    bool RemoteState::__hasActiveSubstate()
    {
        if (!stateOffererPrx || remoteStateId == -1)
        {
            return false;
        }

        try
        {
            return stateOffererPrx->hasActiveSubstateRemote(remoteStateId);
        }
        catch (...)
        {
            return false;
        }
    }

    StateParameterMap RemoteState::getInputParameters()
    {
        if (stateOffererPrx)
        {
            if (remoteStateId != -1)
            {
                inputParameters = stateOffererPrx->getRemoteInputParametersById(remoteStateId);
            }
            else
            {
                inputParameters =  stateOffererPrx->getRemoteInputParameters(stateClassName);
            }
        }

        StateParameterMap result;
        StateUtilFunctions::copyDictionary(inputParameters, result);
        return result;
    }
    StateParameterMap& RemoteState::getOutputParameters()
    {
        //        ARMARX_VERBOSE << "Retrieving output parameters for " << stateName ;
        if (stateOffererPrx
            && getStatePhase() < eExited) // if finished, the outputparamters are already set
        {
            if (remoteStateId != -1)
            {
                outputParameters = stateOffererPrx->getRemoteOutputParametersById(remoteStateId);
            }
            else
            {
                outputParameters = stateOffererPrx->getRemoteOutputParameters(stateClassName);
            }
        }

        //        ARMARX_VERBOSE << "params " << StateUtilFunctions::getDictionaryString(outputParameters);
        return outputParameters;
    }


    StateBasePtr RemoteState::getRemoteStatePtr()
    {
        if (remoteStateId == -1 || !stateOffererPrx)
        {
            return nullptr;
        }

        return StateBasePtr::dynamicCast(stateOffererPrx->getStatechartInstance(remoteStateId));
    }

    bool RemoteState::waitForInitialization(int timeoutMS) const
    {
        auto start = IceUtil::Time::now();
        if (!StateBase::waitForInitialization(timeoutMS))
        {
            return false;
        }
        double waitTime = (IceUtil::Time::now() - start).toMilliSecondsDouble();
        return getObjectScheduler()->waitForObjectStateMinimum(eManagedIceObjectStarted, timeoutMS < 0 ? -1 : std::max(0.0, timeoutMS - waitTime));
    }

    bool RemoteState::isInitialized() const
    {
        return initialized && getState() >= eManagedIceObjectStarted;
    }

    void
    RemoteState::_baseOnEnter()
    {
        if (!getObjectScheduler()->waitForObjectState(eManagedIceObjectStarted, 20000))
        {
            // Event only needed when this state is currently active
            ARMARX_WARNING << "Trying to enter remote state '" << stateName << "', but it is not reachable yet!";
            EventPtr evt = new Failure(this->getStateName());
            __enqueueEvent(evt);
            return;

        }

        HiddenTimedMutex::ScopedTryLock lock(__stateMutex);
        setStatePhase(eEntering);
        ARMARX_DEBUG << "calling a state remotely "  << stateName <<  "\n" << flush;

        if (lock.owns_lock())
        {
            try
            {
                ARMARX_DEBUG << "input for remotestate " << stateName << " (" << remoteStateId << "):\n" << StateUtilFunctions::getDictionaryString(inputParameters) << "\n" << flush;
                stateOffererPrx->callRemoteState(remoteStateId, StateUtilFunctions::getSetValues(inputParameters));
            }
            catch (Ice::Exception& e)
            {
                ARMARX_LOG << eERROR << "Caught exception in " << stateName << "::baseOnEnter(): " << e.what() << "\n" << e.ice_stackTrace() << flush;
            }
            catch (std::exception& e)
            {
                ARMARX_LOG << eERROR << "Caught exception in " << stateName << "::baseOnEnter(): " << e.what() << "\n" << flush;
            }
        }
        else
        {
            ARMARX_LOG << eWARN << "failed to get lock" <<  "\n" << flush;
        }

        setStatePhase(eEntered);
    }

    bool
    RemoteState::_baseOnBreak(const EventPtr evt)
    {
        HiddenTimedMutex::ScopedLock lock(__stateMutex);
        //        ARMARX_LOG << eINFO << "Entering RemoteState::baseOnBreak()" <<  "\n" << flush;
        setStatePhase(eBreaking);

        //call baseOnBreak() over Ice

        if (remoteStateId == -1)
        {
            ARMARX_WARNING << "RemoteStateId is unknown in baseOnbreak()" << flush;
            return true;
        }

        //throw LocalException("RemoteStateId is unknown in baseOnbreak()");
        bool result = stateOffererPrx->breakRemoteState(remoteStateId, evt);

        outputParameters = stateOffererPrx->getRemoteOutputParametersById(remoteStateId);

        //        ARMARX_LOG << eINFO << "remotebaseonbreak returned" <<  "\n" << flush;
        setStatePhase(eExited);

        return result;
    }

    void RemoteState::_baseOnExit()
    {
        HiddenTimedMutex::ScopedLock lock(__stateMutex);

        //        ARMARX_LOG << eINFO << "RemoteState " << stateName << "::baseOnExit()" <<  "\n" << flush;
        //        ARMARX_LOG << eINFO << "stateId is: " << remoteStateId <<  "\n" << flush;
        if (remoteStateId >= 0 && getStatePhase() < eBreaking)
        {
            setStatePhase(eExiting);
            stateOffererPrx->exitRemoteState(remoteStateId);
            outputParameters = stateOffererPrx->getRemoteOutputParametersById(remoteStateId);

        }

        onExit();
        setStatePhase(eExited);
    }

    void RemoteState::__notifyEventBufferedDueToUnbreakableState(bool eventBuffered)
    {
        ARMARX_DEBUG << "notifying state with id " << remoteStateId << " eventBuffered:" << std::string((eventBuffered) ? "true" : "false") << flush;

        if (remoteStateId != -1 && stateOffererPrx)
        {
            stateOffererPrx->begin_notifyEventBufferedDueToUnbreakableStateRemote(remoteStateId, eventBuffered);
        }
    }

    void RemoteState::refetchSubstates()
    {
        if (remoteStateId != -1 && stateOffererPrx)
        {
            StateIceBasePtr state = stateOffererPrx->refetchRemoteSubstates(remoteStateId);
            subStateList = state->subStateList;
            transitions = state->transitions;
            activeSubstate = state->activeSubstate;
            initState = state->initState;
        }
    }

    void RemoteState::__updateGlobalStateIdRecursive()
    {
        __updateGlobalStateId();
        if (remoteStateId != -1 && stateOffererPrx)
        {
            auto id = getGlobalHierarchyString();
            auto pos = id.find_last_of(">");
            if (pos != std::string::npos)
            {
                id.erase(id.find_last_of(">") - 1);
            }
            stateOffererPrx->updateGlobalStateIdRecursive(remoteStateId, id);
        }
    }


    bool RemoteState::__breakActiveSubstate(const EventPtr event)
    {
        //        ARMARX_LOG << eINFO << eINFO <<"RemoteState " << stateName << "::breakActiveSubstate()" <<  "\n" << flush;
        //ARMARX_LOG << eINFO << "stateId is: " << remoteStateId <<  "\n" << flush;
        return stateOffererPrx->breakActiveSubstateRemotely(remoteStateId, event);
    }
    StateIceBasePtr RemoteState::getParentStateLayout(const Ice::Current&) const
    {
        return __getParentState();
    }

}


