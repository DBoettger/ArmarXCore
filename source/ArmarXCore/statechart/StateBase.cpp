/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarXCore::Statechart
* @author     Mirko Waechter( mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

// Statechart Includes
#include "StateBase.h"
#include "standardstates/FinalState.h"
#include "RemoteState.h"
#include "StateParameter.h"
#include "StateUtilFunctions.h"
#include "StatechartManager.h"

// ArmarX core Includes
#include <ArmarXCore/core/application/Application.h>

// boost Includes
#include <boost/regex.hpp>
#include <boost/filesystem.hpp>



namespace armarx
{
    using namespace StateUtilFunctions;

    int StateBase::__LocalIdCounter = 0;
    StateBase::StateInstanceMapPtr StateBase::StateInstances(new StateBase::StateInstanceMap());
    HiddenTimedMutex* StateBase::__StateInstancesMutex = new HiddenTimedMutex();

    StateBase::StateBase() :
        StateIceBase(),
        statePhase(ePreDefined),
        __eventUnbreakableBufferMutex(30000, "__eventUnbreakableBufferMutex"),
        __processEventsMutex(30000, "__processEventsMutex"),
        __stateMutex(30000, "__stateMutex"),
        __useRunFunction(true),
        __parentState(nullptr)
    {
        visitCounter = 0;
        context = nullptr;
        manager = nullptr;

        setTag(std::string("Statechart"));

        initialStateMapping = nullptr;

        reset();
        HiddenTimedMutex::ScopedLock lock(*__StateInstancesMutex);
        localUniqueId = __LocalIdCounter++;
        stateInstancesPtr = StateInstances;
        (*stateInstancesPtr)[localUniqueId] = this;
    }

    StateBase::StateBase(const StateBase& source):
        IceUtil::Shared(source),
        Ice::Object(source),
        StateIceBase(source),
        Logging(source),
        statePhase(source.statePhase),
        __useRunFunction(true),
        __parentState(nullptr)
    {
        visitCounter = 0;
        context = nullptr;
        manager = nullptr;

        setTag(std::string("Statechart"));
        initialStateMapping = nullptr;


        deepCopy(source);

        HiddenTimedMutex::ScopedLock lock(*__StateInstancesMutex);
        localUniqueId = __LocalIdCounter++;
        stateInstancesPtr = StateInstances;
        (*stateInstancesPtr)[localUniqueId] = this;
    }

    StateBase& StateBase::operator =(const StateBase& source)
    {
        assert(0); //this function shouldnt be called, since it is not tested
        deepCopy(source);
        return *this;
    }


    StateBase::~StateBase()
    {
        //        ARMARX_INFO << "~StateBase " << stateClassName << std::endl;
        for (unsigned int i = 0; i < subStateList.size(); i++)
        {
            RemoteStatePtr remoteStatePtr = RemoteStatePtr::dynamicCast(subStateList.at(i));

            if (remoteStatePtr
                && remoteStatePtr->getArmarXManager()) // if the remotestate was not added with addRemoteState(),
                // this pointer is NULL
                // (e.g. in case of creation through ObjectFactories)
            {
                try
                {
                    remoteStatePtr->getArmarXManager()->removeObjectNonBlocking(remoteStatePtr->getName());
                }
                // destructor must never throw
                catch (...) {}
            }

        }

        {
            HiddenTimedMutex::ScopedLock lock(*__StateInstancesMutex);
            stateInstancesPtr->erase(localUniqueId);
        }
    }

    void StateBase::__checkPhase(StateBase::StatePhase allowedType, const char* functionName) const
    {
        if (allowedType != getStatePhase())
        {
            std::string hint;

            if (allowedType == eSubstatesDefinitions)
            {
                hint = "This function must be called in defineSubstates().";
            }
            else if (allowedType == eParametersDefinitions)
            {
                hint = "This function must be called in defineParameters().";
            }

            else if (allowedType == eStatechartDefinitions)
            {
                hint = "This function must be called in defineState().";
            }

            else if (allowedType == eEntering)
            {
                hint = "This function must be called in onEnter().";
            }

            if (!hint.empty())
            {
                hint = "\n" + hint;
            }

            std::stringstream typeStr;
            typeStr << (int) getStatePhase() << "/" << int(allowedType) ;
            throw LocalException("It is not allowed to call the function " + std::string(functionName) + " here! (Current Phase/Allowed Phase): " + typeStr.str() + ") " + hint);
        }
    }

    void StateBase::__checkPhase(const std::vector<StateBase::StatePhase>& allowedTypes, const char* functionName) const
    {
        if (!std::any_of(allowedTypes.begin(), allowedTypes.end(), [&](StatePhase value)
    {
        return value == getStatePhase();
        }))
        {
            std::string hint;

            for (auto& allowedType : allowedTypes)
            {
                if (!hint.empty())
                {
                    hint += " or ";
                }

                if (allowedType == eSubstatesDefinitions)
                {
                    hint += "defineSubstates()";
                }
                else if (allowedType == eParametersDefinitions)
                {
                    hint += "defineParameters()";
                }

                if (allowedType == eStatechartDefinitions)
                {
                    hint += "defineState()";
                }

                if (allowedType == eEntering)
                {
                    hint += "onEnter()";
                }

                if (allowedType == eEntered)
                {
                    hint += "onRun()";
                }


            }

            if (!hint.empty())
            {
                hint = "\nThis function must be called in " + hint;
            }

            std::stringstream typeStr;
            typeStr << (int) getStatePhase();
            throw LocalException("It is not allowed to call the function " + std::string(functionName) + " here! (Current Phase: " + typeStr.str() + ") " + hint);

        }
    }

    void StateBase::__checkPhaseMin(StateBase::StatePhase allowedType, const char* functionName) const
    {
        if (allowedType > getStatePhase())
        {
            std::stringstream typeStr;
            typeStr << (int) getStatePhase() << "/" << int(allowedType) ;
            throw LocalException("It is not allowed to call the function " + std::string(functionName) + " here! (Current Phase/min. Allowed Phase): " + typeStr.str() + ") ");
        }
    }






    void StateBase::clearSelfPointer()
    {
        // currently deprecated since shared_from_this is a normal pointer
        //        Shared::__setNoDelete(true);
        //        __shared_from_this = NULL;
        //        Shared::__setNoDelete(false);
    }

    void StateBase::__setParentState(StateBase* parentState)
    {
        __parentState = parentState;
        __updateGlobalStateIdRecursive();
    }

    void StateBase::__updateGlobalStateId()
    {
        if (__parentState)
        {
            if (!__parentState->globalStateIdentifier.empty())
            {
                globalStateIdentifier = __parentState->getGlobalHierarchyString() + "->" + stateName;
            }
            else
            {
                globalStateIdentifier = getLocalHierarchyString();    // generate local hierarchy string
            }
        }
        else
        {
            globalStateIdentifier = stateName;
        }
    }

    void StateBase::__updateGlobalStateIdRecursive()
    {
        __updateGlobalStateId();

        for (unsigned int i = 0; i < subStateList.size(); i++)
        {
            StateBasePtr::dynamicCast(subStateList.at(i))->__updateGlobalStateIdRecursive();
        }
    }

    void StateBase::setInitialized(bool enable)
    {
        ScopedLock lock(initMutex);
        initialized = enable;
        initCondition.notify_all();
    }

    bool StateBase::__hasSubstates()
    {
        if (subStateList.size() > 0)
        {
            return true;
        }

        return false;
    }

    bool StateBase::__hasActiveSubstate()
    {
        boost::recursive_mutex::scoped_lock lock(__processEventMutex);

        if (activeSubstate._ptr)
        {
            return true;
        }

        return false;
    }




    void
    StateBase::deepCopy(const StateBase& sourceState, bool reset)
    {
        try
        {
            if (!sourceState.initialized)
            {
                throw exceptions::local::eNotInitialized();
            }


            StateUtilFunctions::copyDictionary(sourceState.inputParameters, inputParameters);
            StateUtilFunctions::copyDictionary(sourceState.localParameters, localParameters);
            StateUtilFunctions::copyDictionary(sourceState.outputParameters, outputParameters);
            initState = nullptr;

            if (sourceState.initialStateMapping)
            {
                initialStateMapping = PMPtr::dynamicCast(sourceState.initialStateMapping)->clone();
            }

            __useRunFunction = sourceState.__useRunFunction;

            statePhase = sourceState.statePhase;
            __parentState = nullptr;
            activeSubstate = nullptr;
            context = sourceState.getContext(false);
            manager = sourceState.manager;
            cancelEnteringSubstates = sourceState.cancelEnteringSubstates;
            //        __eventBufferedDueToUnbreakableState = sourceState.__eventBufferedDueToUnbreakableState;
            triggeredEndstateEvent = sourceState.triggeredEndstateEvent;

            if (sourceState.subStateList.size() > 0)
            {
                for (unsigned int i = 0; i < sourceState.subStateList.size(); i++)
                {
                    StateBasePtr curSubstate = StateBasePtr::dynamicCast(sourceState.subStateList.at(i))->clone();
                    subStateList.at(i) = curSubstate;

                    if (!curSubstate._ptr)
                    {
                        throw LocalException("StatePtrePtr::dynamicCast failed in deepCopy() in file " + std::string(__FILE__));
                    }

                    curSubstate->__parentState = this;

                    if (sourceState.initState._ptr == sourceState.subStateList.at(i)._ptr)
                    {
                        initState = curSubstate;
                    }

                    if (sourceState.activeSubstate._ptr == sourceState.subStateList.at(i)._ptr)
                    {
                        activeSubstate = curSubstate;
                    }

                    // recreate transitionTable

                    for (unsigned int j = 0; j < sourceState.transitions.size(); j++)
                    {
                        if (sourceState.transitions[j].mappingToNextStatesInput)
                        {
                            transitions[j].mappingToNextStatesInput = PMPtr::dynamicCast(sourceState.transitions[j].mappingToNextStatesInput)->clone();
                        }

                        if (sourceState.transitions[j].mappingToParentStatesLocal)
                        {
                            transitions[j].mappingToParentStatesLocal = PMPtr::dynamicCast(sourceState.transitions[j].mappingToParentStatesLocal)->clone();
                        }

                        if (sourceState.transitions[j].mappingToParentStatesOutput)
                        {
                            transitions[j].mappingToParentStatesOutput = PMPtr::dynamicCast(sourceState.transitions[j].mappingToParentStatesOutput)->clone();
                        }

                        if (sourceState.transitions[j].sourceState._ptr == sourceState.subStateList.at(i)._ptr)
                        {
                            transitions[j].sourceState = curSubstate;
                        }

                        if (sourceState.transitions[j].destinationState._ptr == sourceState.subStateList.at(i)._ptr)
                        {
                            transitions[j].destinationState = curSubstate;
                        }
                    }
                }

                if (reset)
                {
                    activeSubstate = nullptr;
                }
            }
            else
            {
                initState = nullptr;
            }
        }
        catch (...)
        {
            handleExceptions();
        }

    }

    bool
    StateBase::init(StatechartContextInterface* context, StatechartManager* manager)
    {
        if (!context)
        {
            ARMARX_WARNING << "The StatechartContext should not be null" << std::endl;
        }

        if (!manager)
        {
            ARMARX_WARNING << "The StatechartManager should not be null" << std::endl;
        }

        this->manager = manager;
        this->context = context;


        setStatePhase(eStatechartDefinitions);
        defineState();
        setTag("State: " + stateName);
        setStatePhase(eParametersDefinitions);
        defineParameters();
        setStatePhase(eSubstatesDefinitions);
        defineSubstates();
        setStatePhase(eParametersDefinitions);
        inheritInputParameters();
        setStatePhase(eDefined);


        __copyDefaultValuesToInput();

        if (subStateList.size() > 0)
        {
            //            // perform some logic checks
            //            if (subStateList.size() == 1)
            //            {
            //                throw exceptions::local::eStatechartLogicError("A state must contain atleast 2 substates if it has any.");
            //            }

            //            bool foundFinalState = false;
            //            for(unsigned int i= 0; i < subStateList.size(); i++)
            //                if(dynamic_cast<FinalStateBase<SuccessState>*>(subStateList.at(i)._ptr) != NULL
            //                        ||dynamic_cast<FinalStateBase<FailureState>*>(subStateList.at(i)._ptr) != NULL )
            //                    foundFinalState = true;
            //            if(!foundFinalState)
            //                throw exceptions::local::eLogicError("A state with substates must contain a state of type FinalStateBase!");
            if (!initState._ptr)
            {
                throw exceptions::local::eNullPointerException("The initial state of state '" + stateName + "' was not set!");
            }
        }

        activeSubstate = nullptr;
        setInitialized(true);
        return true;
    }

    StatechartContextInterface*
    StateBase::getContext(bool checkNULL) const
    {
        if (!context && checkNULL)
        {
            throw exceptions::local::eNullPointerException("Statechart Context is NULL");
        }

        return context;
    }


    void
    StateBase::onBreak()
    {
        onExit(); //if OnBreak is not overridden, class-standard OnExit() is called
    }


    void
    StateBase::onEnter()
    {
    }

    void StateBase::run()
    {
        __useRunFunction = false; // if not implemented, this function does not need to be called, so disable it after first run
    }

    void
    StateBase::onExit()
    {
    }




    void StateBase::setContext(StatechartContextInterface* context)
    {
        this->context = context;
    }

    StateParameterMap& StateBase::getOutputParameters()
    {
        return outputParameters;
    }

    std::string StateBase::getStateName() const
    {
        return stateName;
    }

    Ice::Int StateBase::getLocalUniqueId() const
    {
        return localUniqueId;
    }

    const std::string& StateBase::getStateClassName() const
    {
        return stateClassName;
    }

    std::string StateBase::getLocalHierarchyString() const
    {
        StateIceBase* curParent = __parentState;
        std::string result;

        while (curParent)
        {
            result = curParent->stateName + "->" + result;
            curParent = StateBasePtr::dynamicCast(curParent)->__parentState;
        }

        result += stateName;

        return result;
    }

    std::string StateBase::getGlobalHierarchyString() const
    {
        return globalStateIdentifier;
    }

    bool StateBase::waitForInitialization(int timeoutMS) const
    {
        ScopedLock lock(initMutex);
        while (!initialized)
        {
            if (timeoutMS >= 0)
            {
                if (!initCondition.timed_wait(lock, boost::posix_time::milliseconds(timeoutMS)))
                {
                    return false;
                }
            }
            else
            {
                initCondition.wait(lock);
            }
        }
        return true;
    }

    bool StateBase::isInitialized() const
    {
        ScopedLock lock(initMutex);
        return initialized;
    }





    //    bool StateBase::addParameter(StateParameterMap &paramMap, const std::string &key, const Variant &defaultValue, bool optional, bool preSet)
    //    {
    //        if(key.empty())
    //            throw LocalException("The key-string of a parameter must not be empty.");
    //        static const boost::regex e("^([a-zA-Z0-9_\\.]+)$");
    //        if(!boost::regex_match(key, e))
    //            throw LocalException("Invalid character in new key '" + key + "'. Only a-zA-Z0-9_ are allowed.");
    //        std::string newKey = /*stateName + "." + */key;

    //        bool result = true;
    //        StateParameterMap::iterator it = paramMap.find(newKey);
    //        if(it != paramMap.end()){
    //            result = false;
    //            it->second.value->setVariant( new Variant(type));
    //            it->second.optionalParam = optionalParam;
    //            it->second.set = preSet;
    //        }else{
    //            StateParameter param;
    //            param.value->setVariant(new Variant(type));
    //            param.optionalParam = optionalParam;
    //            param.set = preSet;
    //            paramMap[newKey] = param;
    //        }
    //        return result;
    //    }

    bool StateBase::addParameter(StateParameterMap& paramMap, const std::string& key, VariantTypeId type, bool optional, VariantPtr defaultValue) const
    {

        Variant variant;
        variant.setType(type);
        VariantContainerBasePtr variantContainer = new SingleVariant(variant);
        VariantContainerBasePtr defaultContainer;

        if (defaultValue)
        {
            defaultContainer = new SingleVariant(*defaultValue);
        }

        return addParameterContainer(paramMap, key, *variantContainer->getContainerType(), optional, defaultContainer);
    }

    bool StateBase::addParameterContainer(StateParameterMap& paramMap, const std::string& key, const ContainerType& containerType, bool optional, VariantContainerBasePtr defaultValue) const
    {
        if (key.empty())
        {
            throw LocalException("The key-string of a parameter must not be empty.");
        }

        const boost::regex e("^([a-zA-Z0-9_\\.]+)$");

        if (!boost::regex_match(key, e))
        {
            throw LocalException("Invalid character in new key '" + key + "'. Only a-zA-Z0-9_ are allowed.");
        }


        StateParameterPtr param = StateParameter::create();
        //        std::string containerStrId = containerType.typeId;

        //        Ice::ObjectFactoryPtr objFac = getContext()->getIceManager()->getCommunicator()->findObjectFactory(containerStrId);
        //        if(objFac)
        //        {
        //            param->value = VariantContainerBasePtr::dynamicCast(objFac->create(containerStrId));


        //        }
        if (containerType.subType)
        {
            // it has a complex container type. the type will be set afterwards
            param->value = new ContainerDummy();
        }
        else
        {
            Variant var = Variant();
            var.setType(Variant::hashTypeName(containerType.typeId));
            param->value = new SingleVariant(var);

        }

        //        else
        //            throw exceptions::local::eNullPointerException("Could not find ObjectFactory for string '"+containerStrId+"' and could not identiy VariantTypeId") << ;

        param->value->setContainerType(containerType.clone());

        if (defaultValue)
        {
            param->defaultValue = defaultValue->cloneContainer();
        }

        param->optionalParam = optional;
        param->set = false;

        if (paramMap.find(key) != paramMap.end())
        {
            return false;
        }

        paramMap[key] = param;
        return true;
    }

    void StateBase::setParameter(StateParameterMap& paramMap, const std::string& key, const Variant& variant)
    {
        StateParameterMap::iterator it = paramMap.find(/*stateName + ".out." + */key);

        if (it == paramMap.end())
        {
            __throwUnknownParameter(paramMap, key);
        }

        //        if(it->second->value->getContainerType() != eVariantParam)
        //            throw exceptions::local::eLogicError("Cannot assign a VariantParameter to another type of a Parameter");
        if ((it->second->value->getContainerType()->typeId != Variant::typeToString(variant.getType())))
            throw exceptions::local::eStatechartLogicError("Cannot assign the parameter '"
                    + key + "' with type '"
                    + Variant::typeToString(variant.getType())
                    + "' to another type of a Variant ("
                    + it->second->value->getContainerType()->typeId + ")!");

        it->second->value = new SingleVariant(variant);
        it->second->set = true;

    }

    void StateBase::setParameterContainer(StateParameterMap& paramMap, const std::string& key, const VariantContainerBasePtr& valueContainer)
    {
        setParameterContainer(paramMap, key, *valueContainer);
    }

    void StateBase::setParameterContainer(StateParameterMap& paramMap, const std::string& key, const VariantContainerBase& valueContainer)
    {
        StateParameterMap::iterator it = paramMap.find(/*stateName + ".out." + */key);

        if (it == paramMap.end())
        {
            __throwUnknownParameter(paramMap, key);
        }

        if (valueContainer.getSize() > 0 && VariantContainerType::allTypesToString(it->second->value->getContainerType()).find(::armarx::InvalidVariantData::ice_staticId()) == std::string::npos)
        {
            if (!VariantContainerType::compare(it->second->value->getContainerType(), valueContainer.getContainerType()))
                throw exceptions::local::eStatechartLogicError("Cannot assign the parameter container '"
                        + key + "' with type '"
                        + VariantContainerType::allTypesToString(valueContainer.getContainerType())
                        + "' to another type of container ("
                        + VariantContainerType::allTypesToString(it->second->value->getContainerType()) + ")!");
        }
        auto type = it->second->value->getContainerType();
        it->second->value = valueContainer.cloneContainer();
        it->second->value->setContainerType(type);
        it->second->set = true;
    }

    void StateBase::getParameterContainer(const StateParameterMap& paramMap, const std::string& key, VariantContainerBasePtr& valueContainer) const
    {
        StateParameterMap::const_iterator it = paramMap.find(key);

        if (it == paramMap.end())
        {
            __throwUnknownParameter(paramMap, key);
        }

        if (!it->second->set)
        {
            throw LocalException("Requested parameter '" + key + "' was not set in state " + stateName);
        }

        valueContainer = it->second->value;
    }

    bool StateBase::isParameterSet(const StateParameterMap& paramMap, const std::string& key) const
    {
        StateParameterMap::const_iterator it = paramMap.find(key);

        if (it == paramMap.end())
        {
            throw LocalException("Requested parameter '" + key + "' not found in state " + stateName);
        }

        return it->second->set;
    }

    void StateBase::__throwUnknownParameter(const StateParameterMap& paramMap, const std::string& key) const
    {
        std::string parameters;

        for (StateParameterMap::const_iterator it = paramMap.begin(); it != paramMap.end(); it++, parameters += ",\n")
        {
            parameters += it->first + "(" + VariantContainerType::allTypesToString(it->second->value->getContainerType()) + ")";
        }

        throw exceptions::local::eUnknownParameter(stateName, key, "Available parameters:\n" + parameters);
    }

    void StateBase::inheritInputParameters()
    {
        for (size_t i = 0; i < inputInheritance.size(); i++)
        {
            StateBasePtr state = findSubstateByName(inputInheritance.at(i));

            if (state)
            {
                StateParameterMap substateInput = state->getInputParameters();

                for (StateParameterMap::iterator it = substateInput.begin(); it != substateInput.end(); it++)
                {
                    StateParameterIceBasePtr param = it->second;
                    addParameterContainer(inputParameters, inputInheritance.at(i) + "." + it->first, *param->value->getContainerType(), param->optionalParam);
                }
            }
        }
    }

    void StateBase::__copyDefaultValuesToInput()
    {
        for (StateParameterMap::iterator i = inputParameters.begin(); i != inputParameters.end(); i++)
        {
            StateParameterIceBasePtr p = i->second;

            if (p->defaultValue)
            {
                p->value = p->defaultValue->cloneContainer();
                p->set = true;
            }
        }
    }

    StateBasePtr StateBase::findSubstateByName(const std::string& substateName)
    {
        for (unsigned int i = 0; i < subStateList.size(); i++)
        {
            if (subStateList.at(i)->stateName == substateName)
            {
                return StateBasePtr::dynamicCast(subStateList.at(i));
            }
        }

        return nullptr;
    }

    void StateBase::getParameter(const StateParameterMap& paramMap, const std::string& key, VariantPtr& value) const
    {
        StateParameterMap::const_iterator it = paramMap.find(/*stateName + ".in." +*/ key);

        if (it == paramMap.end())
        {
            __throwUnknownParameter(paramMap, key);
        }

        if (!it->second->set)
        {
            throw LocalException("Requested parameter '" + key + "' was not set in state " + stateName);
        }

        SingleVariantPtr sVar = SingleVariantPtr::dynamicCast(it->second->value);
        if (!sVar)
        {
            throw InvalidTypeException("Parameter '" +  key + "' is not a single variant!");
        }

        value = sVar->get();
    }

    void StateBase::setStateClassName(std::string className)
    {
        //        __checkPhase(eStatechartDefinitions, __PRETTY_FUNCTION__);
        stateClassName = className;
    }

    void StateBase::refetchSubstates()
    {
        for (auto s : subStateList)
        {
            StateBasePtr state = StateBasePtr::dynamicCast(s);
            state->refetchSubstates();
        }
    }

    EventPtr StateBase::getTriggeredEndstateEvent() const
    {
        return triggeredEndstateEvent;
    }

    StateBase::StatePhase StateBase::getStatePhase() const
    {
        ScopedLock lock(statePhaseMutex);
        return statePhase;
    }

    void StateBase::setStatePhase(StateBase::StatePhase newPhase)
    {
        ScopedLock lock(statePhaseMutex);
        statePhase = newPhase;
    }

    StringVariantContainerBaseMap StateBase::__getSetInputAndLocalParameters() const
    {
        StringVariantContainerBaseMap result = StateUtilFunctions::getSetValues(localParameters);
        StringVariantContainerBaseMap inputSetValues = StateUtilFunctions::getSetValues(inputParameters);
        result.insert(inputSetValues.begin(), inputSetValues.end());
        return result;
    }

    std::vector<StateBasePtr> StateBase::GetActiveStateLeafs(StateBasePtr toplevelState)
    {
        if (!toplevelState)
        {
            throw LocalException("The toplevelState must not be NULL");
        }

        std::vector<StateBasePtr> activeStates;
        StateBasePtr activeState = toplevelState;
        boost::recursive_mutex::scoped_lock lock(toplevelState->__processEventMutex);

        while (activeState->activeSubstate)
        {
            activeState = StateBasePtr::dynamicCast(activeState->activeSubstate);
        }

        activeStates.push_back(activeState);
        return activeStates;
    }




    void StateBase::reset()
    {
        cancelEnteringSubstates = false;
        stateName = "";
        initialized = false;
        unbreakable = false;
        eventsDelayed = false;
        greedyInputDictionary = false;
        //        __eventBufferedDueToUnbreakableState = false;
        inputParameters.clear();
        localParameters.clear();
        outputParameters.clear();
        subStateList.clear();
        transitions.clear();
        initialStateMapping = nullptr;
        initState = nullptr;
        activeSubstate = nullptr;
        __parentState = nullptr;
        triggeredEndstateEvent = nullptr;
        //        __unbreakableBuffer = queue<EventPtr>();
        //        __eventBuffer = vector< pair<StateBasePtr, EventPtr> >();
    }

}




//void armarx::StateBase::__decRef()
//{
//    ARMARX_DEBUG << "DecRef of " << stateName << " of class " << stateClassName;
//    GCShared::__decRef();
//}
