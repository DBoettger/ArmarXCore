/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarXCore::Statechart
* @author     Mirko Waechter( mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/
#pragma once
#ifndef _ARMARX_CORE_STATE_H
#define _ARMARX_CORE_STATE_H

// statechart includes
#include "StateUtil.h"

// boost includes
#include <boost/smart_ptr.hpp>
#include <boost/utility/enable_if.hpp>
#include <boost/type_traits/is_base_of.hpp>

// observers includes
#include <ArmarXCore/observers/variant/StringValueMap.h>
#include <ArmarXCore/observers/variant/SingleTypeVariantList.h>





namespace armarx
{
    template <class StateType>
    class StateTemplate;

    class RemoteState;
    typedef IceInternal::Handle<RemoteState> RemoteStatePtr;

    class State;
    typedef IceInternal::Handle<State> StatePtr;

    /**
      @class State
      @ingroup StatechartGrp
      Class that offers the main functionality needed to create a statechart.
      Only functions from this class and StateUtility should be used to define
      custom statecharts.
    */
    class State :
        virtual public StateUtility
    {

    protected:
        State();
        State(const State& source);
        State& operator=(const State& source);
        ~State() override;
    public:
        /*! @brief Function to initialize this state. Must be called in the highest
         * level of the hierarchy - and only there.
         * @note init() of substates is automatically called.
         * @param context pointer to a StatechartContext, that holds the objects to
         * communicate with other components like observers, kinematic unit etc.
         */
        bool init(StatechartContextInterface* context, StatechartManager* manager);




        //! Returns an id to this state, that is guaranteed to be unique in this process.
        unsigned int getId() const;

        /**
         * @brief Getter for the global state identifier string. It contains all parent states
         * to the top of the statechart including remote states.
         * Example: RobotControl->Functional->VisualServo
         */
        std::string getGlobalIdString() const;



        /*!
         * @brief Getter for the initial state. The initial state is automatically entered,
         * when this state is entered.
         * @return Shared pointer to the initial state, NULL if not set.
         * @see setInitState()
         */
        StateBasePtr getInitState() const;

        bool isUnbreakable() const;

        /*!
         * @brief getter function to get the map of output parameters
         * @return returns the reference to this state's output parameters (the complete map)
         * @see getOutput, setOutput, getInput, getInputParameters
         */
        StateParameterMap& getOutputParameters() override;

        /*!
        * @brief getOutput can be used to access a specific input parameter.<br/>
        *
        * This version is for complex variant types.
        * @throw InvalidTypeException
        * @tparam T Type of the requested variant. The return type depends on this.
        * @param key of the parameter
        * @return A pointer to the requested value
        **/
        template<typename T>
        typename boost::enable_if<boost::is_base_of<VariantDataClass, T>, IceInternal::Handle<T> >::type
        getOutput(const std::string& key) const;

        /*!
        * @brief getOutput can be used to access a specific input parameter.<br/>
        *
        * This version is for basic variant types like int, float, bool, string.
        * @throw InvalidTypeException
        * @tparam T Type of the requested variant. The return type depends on this.
        * @param key of the parameter
        * @return A copy of the requested value.
        **/
        template<typename T>
        typename boost::disable_if_c < boost::is_base_of<VariantDataClass, T>::value || boost::is_base_of<VariantContainerBase, T>::value, T >::type
        getOutput(const std::string& key) const;


    public:

        StateBasePtr addState(StateBasePtr pNewState);

        /*! @brief Function to add a new substate to this state. Should be called in the defineState()-function.

          @param derivedState Template parameter that specifies the derived state class of the added state. Must be derived from StateTemplate or class State itself if no further functionality is required.
          @param stateName String that describes this state. Should be unique in this hierarchy level.
          @note Can only be called in defineSubstates() (exception otherwise).
          @returns returns a shared pointer to the created state for convenience. The state is inserted into the std::vector substateList as well.
          */
        template <class derivedState>
        IceInternal::Handle<derivedState> addState(std::string const& stateName = "");

        /*! @brief Function to add a new remote substate to this state.
         *
         * A Remote State is only a placeholder for the real state that is
         * located in another application. Most functioncalls to a Remote State
         * are redirected to the state in other application. The interface is
         * the same as to a normal state.
         * @param stateName String that describes this state. Must exist on the
         * specified proxy, otherwise an exception is thrown. It is the name,
         * that was set by setStateName(stateName) or addState(stateName).
         * @param proxyName Name of the Ice Proxy, that offers the state
         * @note Can only be called in defineSubstates() (exception otherwise).
         * @returns returns a shared pointer to the created state for convenience.
         * The state is inserted into the std::vector substateList as well.
         */
        virtual RemoteStatePtr addRemoteState(std::string stateName, std::string proxyName, std::string instanceName = "");

        /*! @brief Function to add a new dynamic remote substate to this state.

        A Dynamic Remote State
        is only a placeholder for the real state that is located in another
        application. Most functioncalls to a Remote State are redirected to the
        state in other application. The interface is the same as to a normal
        state.<br/>
        The differene to a Remote State is, that the location of
        the real state is unspecified until the Dynamic Remote State-onEnter()
        function is called by the system. The location of the real state must
        be specified in the input parameters of the state.<br>
        So the inputparameter map must contain a field 'proxyName' and a field
        'stateName' with the corresponding values.
        @note Can only be called in defineSubstates() (exception otherwise).
        @returns returns a shared pointer to the created state for convenience.
                 The state is inserted into the std::vector substateList as well.
          */
        virtual RemoteStatePtr addDynamicRemoteState(std::string instanceName);


        TransitionIceBase&  addTransition(EventPtr event, StateIceBasePtr sourceState,
                                          StateIceBasePtr destinationState,
                                          ParameterMappingIceBasePtr mappingToNextStatesInput = nullptr,
                                          ParameterMappingIceBasePtr mappingToParentStatesLocal = nullptr,
                                          ParameterMappingIceBasePtr mappingToParentStatesOutput = nullptr);

        /*! @brief Function to add a new transition between to substates to this state.
          @tparam EventType Type of the event that trigers this transition.
          @param pSourceState Shared pointer to the state, that must be the active state before this transition. OnExit() is called there.
          @param pDestination Shared pointer to the state, in which this transition will end. OnEnter() is called there.
          @param pMapping Mapping from the outputDictionary of pSourceState and the eventDictionary to the inputDictionary of pDestinationState
          @note Can only be called in defineSubstates() (exception otherwise).

          @return returns reference to added transition
          */
        template <class EventType>
        TransitionIceBase&  addTransition(StateIceBasePtr sourceState,
                                          StateIceBasePtr destinationState,
                                          ParameterMappingIceBasePtr mappingToNextStatesInput = nullptr,
                                          ParameterMappingIceBasePtr mappingToParentStatesLocal = nullptr,
                                          ParameterMappingIceBasePtr mappingToParentStatesOutput = nullptr);

        /**
         * @brief Function to add a new transition from all substates to @p destinationState.
         * @note Can only be called in defineSubstates() (exception otherwise).
         *
         */
        template <class EventType>
        TransitionIceBase&  addTransitionFromAllStates(
            StateIceBasePtr destinationState,
            ParameterMappingIceBasePtr mappingToNextStatesInput = nullptr,
            ParameterMappingIceBasePtr mappingToParentStatesLocal = nullptr,
            ParameterMappingIceBasePtr mappingToParentStatesOutput = nullptr);

        void inheritInputFromSubstate(std::string stateName);

        /*! @brief Adds a key,type-pair to the input parameters
          @param key String to describe the parameter
          @param type Variable that defines the type of this parameter
          @param optional Flag to indicate whether or not this parameter must be set, when this state is entered
          @return returns false if the key already exists. The old type resides.
          @note Can only be called in defineParameters() (exception otherwise).

          */
        bool addToInput(const std::string& key, const ContainerType& type, bool optional, VariantContainerBasePtr defaultValue = VariantContainerBasePtr());

        /*! @brief Adds a new parameter list to the input parameters with a specific type.
          @param key String to describe the parameter
          @param type Variable that defines the type of this parameter
          @param optional Flag to indicate whether or not this parameter must be set, when this state is entered
          @return returns false if the key already exists. The old type resides.
          @note Can only be called in defineParameters() (exception otherwise).
          */
        bool addToInput(const std::string& key, VariantTypeId type, bool optional, VariantPtr defaultValue = VariantPtr());

        /*! @brief Adds a key,type-pair to the local parameters
          @param key String to describe the parameter
          @param type Variable that defines the type of this parameter
          @param optional Flag to indicate whether or not this parameter must be set, when this state is entered
          @return returns false if the key already exists. The old type resides.
          @note Can only be called in defineParameters() (exception otherwise).
          */
        bool addToLocal(const std::string& key, VariantTypeId type, VariantPtr defaultValue = VariantPtr());

        /*! @brief Adds a new parameter list to the local parameters with a specific type.
          @param key String to describe the parameter
          @param type Variable that defines the type of this parameter
          @param optional Flag to indicate whether or not this parameter must be set, when this state is entered
          @return returns false if the key already exists. The old type resides.
          @note Can only be called in defineParameters() (exception otherwise).
          */
        bool addToLocal(const std::string& key, const ContainerType& type, VariantContainerBasePtr defaultValue = VariantContainerBasePtr());

        /*! @brief Adds a key,type-pair to the output parameters
          @param key String to describe the parameter
          @param type Variable that defines the type of this parameter
          @param optional Flag to indicate whether or not this parameter must be set, when this state is entered
          @return returns false if the key already exists. The old type resides.
          @note Can only be called in defineParameters() (exception otherwise).
          */
        bool addToOutput(const std::string& key, VariantTypeId type, bool optional);

        /*! @brief Adds a new parameter list to the output parameters with a specific type.
          @param key String to describe the parameter
          @param type Variable that defines the type of this parameter
          @param optional Flag to indicate whether or not this parameter must be set, when this state is entered
          @return returns false if the key already exists. The old type resides.
          @note Can only be called in defineParameters() (exception otherwise).
          */
        bool addToOutput(const std::string& key, const ContainerType& type, bool optional);







        //! @brief Generates a new copy of this state with the same statename, substates, transitions, overidden functions etc.
        StateBasePtr clone() const override;
        //! @brief Generates a new copy of this state with the same overidden functions. stateName, substates, transition etc. are not set.
        StateBasePtr createEmptyCopy() const override;

        //**************************
        //    Getters
        //**************************


        /** @brief Returns a new copy of the inputparameters-dictionary, so that the
        caller cannot modify them (const won't work due to pointers).

        These values are reset immediately after this state is left (onExit/onBreak
        was called)
        * @return new instance of the inputparameters-dictionary
        **/
        StateParameterMap getInputParameters() override;

        /*!
        * @brief getInput can be used to access a specific input parameter.<br/>
        *
        * This version is for complex variant types.
        * @throw InvalidTypeException
        * @tparam T Type of the requested variant. The return type depends on this.
        * @param key of the parameter
        * @return Since the user must not be able to modify the inputparameters
        * and constant IceSharedPointers are not possible, a copy of the requested
        * Variant is returned.
        **/
        template<typename T>
        typename boost::enable_if<boost::is_base_of<VariantDataClass, T>, IceInternal::Handle<T> >::type
        getInput(const std::string& key) const;

        /*!
        * @brief getInput can be used to access a specific input parameter.<br/>
        *
        * This version is for basic variant types like int, float, bool, string.
        * @throw InvalidTypeException
        * @tparam T Type of the requested variant. The return type depends on this.
        * @param key of the parameter
        * @return Since the user must not be able to modify the inputparameters
        * and constant IceSharedPointers are not possible, a copy of the requested
        * Variant is returned.
        **/
        template<typename T>
        typename boost::disable_if_c < boost::is_base_of<VariantDataClass, T>::value || boost::is_base_of<VariantContainerBase, T>::value, T >::type
        getInput(const std::string& key) const;

        /*!
         * @brief getInput can be used to access a specific input parameter.
         *
         * @throw InvalidTypeException
         * @tparam T Type of the requested variant. The return type depends on this.
         * @param key of the parameter
         * @return Since the user must not be able to modify the inputparameters
         * and constant IceSharedPointers are not possible, a copy of the requested
         * Variant is returned.
         */
        VariantPtr getInput(const std::string& key) const;


        /*!
         * @brief Getter for the local parameter map.
         *
         * @return returns a reference to the local parameters map.
         */
        StateParameterMap& getLocalParameters();

        /*!
        * @brief getLocal can be used to access a specific input parameter.<br/>
        *
        * This version is for complex variant types.
        * @throw InvalidTypeException
        * @tparam T Type of the requested variant. The return type depends on this.
        * @param key of the parameter
        * @return A pointer to the requested value
        **/
        template<typename T>
        typename boost::enable_if<boost::is_base_of<VariantDataClass, T>, IceInternal::Handle<T> >::type
        getLocal(const std::string& key) const;

        /*!
        * @brief getLocal can be used to access a specific input parameter.<br/>
        *
        * This version is for basic variant types like int, float, bool, string.
        * @throw InvalidTypeException
        * @tparam T Type of the requested variant. The return type depends on this.
        * @param key of the parameter
        * @return A copy of the requested value.
        **/
        template<typename T>
        typename boost::disable_if_c < boost::is_base_of<VariantDataClass, T>::value || boost::is_base_of<VariantContainerBase, T>::value, T >::type
        getLocal(const std::string& key) const;




        VariantContainerBasePtr getLocalContainer(std::string const& key);



        /**
          * Checks whether a given input parameter is set or not.
          * @param key Name of the input parameter
          * @return returns true if the given parameter is set.
          **/
        bool isInputParameterSet(const std::string& key) const;
        bool isLocalParameterSet(const std::string& key) const;
        bool isOutputParameterSet(const std::string& key) const;

        //**************************
        //    Setters
        //**************************
        /*!
         * @brief setInput() sets an input parameter.
         *
         * @param key of the parameter
         * @param value of the parameter of any by the Variant supported type
         */
        void setInput(std::string const& key, const Variant& value);

        /*!
         * @brief setInput() sets an input parameter <b>list</b>.
         *
         * @param key of the parameter
         * @param value of the parameter of any by the Variant supported type
         */
        void setInput(std::string const& key, const VariantContainerBase& valueList);


        /*!
         * @brief setLocal() sets a local parameter.
         *
         * Local means, that this parameter
         * can be written and read in this state's onEnter(), onBreak(), onEnter() and only
         * there.
         *
         * @param key of the parameter
         * @param value of the parameter of any by the Variant supported type
         */
        void setLocal(std::string const& key, const Variant& value);

        /*!
         * @brief setLocal() sets a local parameter <b>list</b>.
         *
         * Local means, that this parameter
         * can be written and read in this state's onEnter(), onBreak(), onEnter() and only
         * there.
         *
         * @param key of the parameter
         * @param value of the parameter of any by the Variant supported type
         */
        void setLocal(std::string const& key, const VariantContainerBase& valueList);


        /*!
         * @brief setOuput() sets an output parameter of this state.<br/>
         *
         * Can be called in onEnter(), onBreak(), onExit(). Following states
         * can access this parameter.
         *
         * @param key of the parameter
         * @param value of the parameter of any by the Variant supported type
         */
        void setOutput(std::string const& key, const Variant& value);

        /*!
         * @brief setOuput() sets an output parameter LIST of this state.<br/>
         *
         * Can be called in onEnter(), onBreak(), onExit(). Following states
         * can access this parameter.
         *
         * @param key of the parameter
         * @param value of the parameter of any by the Variant supported type
         */
        void setOutput(std::string const& key, const VariantContainerBase& valueList);


        /** @brief Sets the initial substate of this state.
          * This initial state is automatically entered, when the parent state is entered.
          * On the implicit transition a parameter mapping from the parent's input and
          * local parameters is done. The mapping can be specified
          * with setInitialStateMapping()
          * @param initState: The state that should be the initial state
          * @param initialStateMapping: A ParameterMapping that maps *this's input to
          * the state's input in the first parameter(use mapFromParent()). This mapping is
          * copied and then stored.
          *
          * @note Can only be called in defineSubstates() (exception otherwise).
          *
          */
        StateBasePtr setInitState(StateBasePtr initState, ParameterMappingPtr initialStateMapping = ParameterMappingPtr());
        /*!
         * @brief With this function the state can be made unbreakable.
         *
         * Unbreakable means that a parent state cannot be left, while this state is active.
         * @param set: Set to true if the state should be made unbreakable
         */
        void setUnbreakable(bool setUnbreakable = true);

        /**
         * @brief setUseRunFunction can be used to configurate whether the thread
         * with the async. run-function should be started/used.
         *
         * This should be set to false in time-critical states.
         *
         * @see StateBase::run(), _baseRun(), _startRun()
         * @param useRunFuntion
         */
        void setUseRunFunction(bool useRunFuntion);

        /**
         * @brief Use this function in the onEnter() function, if you want to avoid
         * that the substates (i.e. the initial state) are entered.
         */
        void cancelSubstates();

        void setStateName(const std::string& newName);

    public:
        //            /*! @brief Function to jump directly into a state. Should only be called for statemachine-debugging.

        //            @param stateName Name of the target state
        //            @param inputDictionary Dictionary for the state specified by @c stateName. Content is copied to the @c inputDictionary of @c stateName.
        //          */
        //            bool JumpToState(std::string stateName, StringVariantMap inputDictionary);

        StatePtr operator [](std::string const& stateName);

        template <class EventType, class StateType> friend class FinalStateBase;
        template <typename ContextType> friend class RemoteStateOfferer;
        friend class RemoteStateWrapper;
        template <class StateType> friend class StateTemplate;

    private:
        mutable Mutex inputParameterMutex;
        mutable Mutex localParameterMutex;
        mutable Mutex outputParameterMutex;
    };


    ///////////////////////////////////////////////////////////////
    //////     Implementations of State
    ///////////////////////////////////////////////////////////////

    template <> StatePtr State::addState<State>(std::string const& stateName);

    template <class derivedState>
    IceInternal::Handle<derivedState> State::addState(std::string const& stateName)
    {
        // if the compiler points to the next line, something with the template parameter has been done wrong. see the message-string in the macro for further information
        BOOST_STATIC_ASSERT_MSG((
                                    boost::is_same<typename derivedState::Type, derivedState>::value),
                                "The template parameter 'derivedState' must be derived from StateTemplate<derivedState> or the class 'State' itself! It must not inherit from a class that inherits from StateTemplate!");


        __checkPhase(eSubstatesDefinitions, __PRETTY_FUNCTION__);

        IceInternal::Handle<derivedState> pNewState = derivedState::createInstance(stateName);
        //        stateName = pNewState->getStateName();

        addState(pNewState);

        return pNewState;
    }


    template <class EventType>
    TransitionIceBase& State::addTransition(StateIceBasePtr sourceState,
                                            StateIceBasePtr destinationState,
                                            ParameterMappingIceBasePtr mappingToNextStatesInput,
                                            ParameterMappingIceBasePtr mappingToParentStatesLocal,
                                            ParameterMappingIceBasePtr mappingToParentStatesOutput)
    {
        return addTransition(createEvent<EventType>(), sourceState, destinationState, mappingToNextStatesInput, mappingToParentStatesLocal, mappingToParentStatesOutput);
    }


    template <class EventType>
    TransitionIceBase& State::addTransitionFromAllStates(
        StateIceBasePtr destinationState,
        ParameterMappingIceBasePtr mappingToNextStatesInput,
        ParameterMappingIceBasePtr mappingToParentStatesLocal,
        ParameterMappingIceBasePtr mappingToParentStatesOutput)
    {
        __checkPhase(eSubstatesDefinitions, __PRETTY_FUNCTION__);
        TransitionIceBase t;

        assert(destinationState._ptr);
        t.evt = new EventType(EVENTTOALL);
        t.destinationState = destinationState;

        if (mappingToNextStatesInput)
        {
            t.mappingToNextStatesInput = PMPtr::dynamicCast(mappingToNextStatesInput)->clone();
        }

        if (mappingToParentStatesLocal)
        {
            t.mappingToParentStatesLocal = PMPtr::dynamicCast(mappingToParentStatesLocal)->clone();
        }

        if (mappingToParentStatesOutput)
        {
            t.mappingToParentStatesOutput = PMPtr::dynamicCast(mappingToParentStatesOutput)->clone();
        }

        t.fromAll = true;

        if (__checkExistenceOfTransition(t))
            throw exceptions::local::eStatechartLogicError("Cannot insert a general transition on event '" + t.evt->eventName
                    + "' from any state to '" + destinationState->stateName + "' into state '" + getStateName() + "'! There already exists a transition to that state on that event.");

        transitions.push_back(t);
        return *transitions.rbegin();
    }

    template<typename T>
    typename boost::enable_if<boost::is_base_of<VariantDataClass, T>, IceInternal::Handle<T> >::type
    State::getInput(const std::string& key) const
    {
        __checkPhaseMin(eDefined, __PRETTY_FUNCTION__);
        VariantContainerBasePtr varContainer;
        getParameterContainer(inputParameters, key, varContainer);
        if (varContainer && !SingleVariantPtr::dynamicCast(varContainer)) // we are only interested in real containers, not singlevariant containers
        {
            return IceInternal::Handle<T>::dynamicCast(varContainer);
        }
        else
        {
            VariantPtr var;
            getParameter(inputParameters, key, var);
            return IceInternal::Handle<T>::dynamicCast(var->get<T>());
        }
    }


    template<typename T>
    typename boost::disable_if_c < boost::is_base_of<VariantDataClass, T>::value || boost::is_base_of<VariantContainerBase, T>::value, T >::type
    State::getInput(const std::string& key) const
    {
        __checkPhaseMin(eDefined, __PRETTY_FUNCTION__);
        VariantPtr var;
        getParameter(inputParameters, key, var);
        return var->get<T>();
    }


    template<typename T>
    typename boost::enable_if<boost::is_base_of<VariantDataClass, T>, IceInternal::Handle<T> >::type
    State::getLocal(const std::string& key) const
    {
        __checkPhaseMin(eDefined, __PRETTY_FUNCTION__);
        VariantContainerBasePtr varContainer;
        getParameterContainer(localParameters, key, varContainer);
        if (varContainer && !SingleVariantPtr::dynamicCast(varContainer)) // we are only interested in real containers, not singlevariant containers
        {
            return IceInternal::Handle<T>::dynamicCast(varContainer);
        }
        else
        {
            VariantPtr var;
            getParameter(localParameters, key, var);
            return IceInternal::Handle<T>::dynamicCast(var->get<T>());
        }
    }


    template<typename T>
    typename boost::disable_if_c < boost::is_base_of<VariantDataClass, T>::value || boost::is_base_of<VariantContainerBase, T>::value, T >::type
    State::getLocal(const std::string& key) const
    {
        __checkPhaseMin(eDefined, __PRETTY_FUNCTION__);
        VariantPtr var;
        getParameter(localParameters, key, var);
        return var->get<T>();
    }


    template<typename T>
    typename boost::enable_if<boost::is_base_of<VariantDataClass, T>, IceInternal::Handle<T> >::type
    State::getOutput(const std::string& key) const
    {
        __checkPhaseMin(eDefined, __PRETTY_FUNCTION__);
        VariantContainerBasePtr varContainer;
        getParameterContainer(outputParameters, key, varContainer);
        if (varContainer && !SingleVariantPtr::dynamicCast(varContainer)) // we are only interested in real containers, not singlevariant containers
        {
            return IceInternal::Handle<T>::dynamicCast(varContainer);
        }
        else
        {
            VariantPtr var;
            getParameter(outputParameters, key, var);
            return IceInternal::Handle<T>::dynamicCast(var->get<T>());
        }
    }


    template<typename T>
    typename boost::disable_if_c < boost::is_base_of<VariantDataClass, T>::value || boost::is_base_of<VariantContainerBase, T>::value, T >::type
    State::getOutput(const std::string& key) const
    {
        __checkPhaseMin(eDefined, __PRETTY_FUNCTION__);
        VariantPtr var;
        getParameter(outputParameters, key, var);
        return var->get<T>();
    }



}
#endif
