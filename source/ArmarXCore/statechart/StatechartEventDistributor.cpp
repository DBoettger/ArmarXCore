/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "StatechartEventDistributor.h"
#include "StateController.h"
#include "StatechartManager.h"

using namespace armarx;

StatechartEventDistributor::StatechartEventDistributor()
{
    statechartName = "UnknownStatechart";
    setTag("StatechartEventDistributor");
}

void StatechartEventDistributor::setStatechartManager(const StatechartManagerPtr& statechartManager)
{
    this->statechartManager = statechartManager;
}

std::string StatechartEventDistributor::registerEvent(const EventPtr& event, StateBasePtr eventProcessor, long visitId)
{
    std::string uniqueEventIdentifier = getUniqueEventIdentifier(eventProcessor, event, visitId);
    StatechartEventDistributor::EventListenerMapEntry entry;
    entry.event = new Event(*event);
    entry.eventProcessor = StateControllerPtr::dynamicCast(eventProcessor);
    entry.globalEventIdentifier = uniqueEventIdentifier;
    entry.useCounter = 1;
    entry.visitId = visitId;
    ScopedLock lock(eventMapMutex);
    StatechartEventDistributor::EventListenerMap::iterator it = eventMap.find(uniqueEventIdentifier);

    if (it != eventMap.end())
    {
        it->second.useCounter++;
    }
    else
    {
        eventMap[uniqueEventIdentifier] = entry;
    }

    return uniqueEventIdentifier;
}

void StatechartEventDistributor::onInitComponent()
{

}

void StatechartEventDistributor::onConnectComponent()
{
    myProxy = EventListenerInterfacePrx::checkedCast(getProxy());
}




void StatechartEventDistributor::reportEvent(const EventBasePtr& event, const Ice::Current& c)
{
    {
        ScopedLock lock(eventMapMutex);
        EventListenerMap::iterator it =  eventMap.find(event->eventName);

        if (it == eventMap.end())
        {
            ARMARX_ERROR << "Could not find Event in EventListenerMap" << event->eventName << " receiver: " << event->eventReceiverName;
            throw LocalException("Could not find Event in EventListenerMap - eventName") << event->eventName << " receiver: " << event->eventReceiverName;
        }

        EventListenerMapEntry& entry = it->second;
        entry.event->properties = event->properties;

        if (!statechartManager->addEvent(entry.event, entry.eventProcessor))
        {
            return;
        }

        entry.useCounter--;

        if (entry.useCounter <= 0)
        {
            eventMap.erase(it);
        }
    }
}






std::string StatechartEventDistributor::getUniqueEventIdentifier(const StateBasePtr& eventProcessor, const EventPtr& event, long visitId)
{
    std::stringstream str;
    str << getName() << "___" <<
        eventProcessor->stateName <<
        "(" << eventProcessor->localUniqueId <<
        ")___Event:" << event->eventName <<
        "___EventRecv:" << event->eventReceiverName <<
        "__visitId:" << visitId;
    return str.str();
}

void StatechartEventDistributor::clearEventMap()
{
    eventMap.clear();
}

EventListenerInterfacePrx StatechartEventDistributor::getListener()
{
    return myProxy;
}

