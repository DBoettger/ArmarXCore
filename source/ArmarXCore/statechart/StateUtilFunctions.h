/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

// ArmarX
#include <ArmarXCore/observers/variant/Variant.h>
#include <ArmarXCore/observers/Event.h>
#include <ArmarXCore/interface/statechart/StatechartIce.h>

// C++
#include <string>

// boost
#include <boost/thread/mutex.hpp>


//! this macro declares a new event-class derived vom Event, to have a compiletime check for typos in events
#define DEFINEEVENT(NEWEVENT) struct NEWEVENT : Event{ \
        NEWEVENT(std::string eventReceiverName): \
            Event(eventReceiverName, #NEWEVENT){ }\
    };




namespace armarx
{

    class SingleTypeVariantList;
    typedef IceInternal::Handle<SingleTypeVariantList> SingleTypeVariantListPtr;

    enum TransitionErrorType  // order of errors is importance
    {
        eTransitionErrorUndefined,
        eTransitionErrorUnexpectedEvent,
        eTransitionErrorInput,
        eTransitionNoError // this one must be last
    };

    struct TransitionError
    {
        TransitionErrorType errorType;
        //! vector to store info data in
        std::vector<std::string> infos;
    };

    namespace StateUtilFunctions
    {
        std::string transitionErrorToString(TransitionErrorType type);
        StringVariantContainerBaseMap getSetValues(const StateParameterMap& paramMap);
        StringVariantContainerBaseMap getValues(const StateParameterMap& paramMap);


        /*! \brief Sets all entries of the given dictionary to the stored default values

          */
        void unsetParameters(StateParameterMap& paramMap);
        bool checkForCompleteParameters(const StateParameterMap& paramMap, std::string* logOutput = nullptr);


        /**
         * @brief Waits for all ChannelRefs to be initialized in the given Map.
         *
         * Times out after some delay.
         * @param paramMap parameter map, that is to be checked for initialized ChannelRefs
         * @return true if all ChannelRef are initialized
         */
        bool waitForChannelRefs(const StateParameterMap& paramMap);

        //! \brief Adds the (key,defaulfValue) pair to the event-dictionary. The type of the entry is determined with defaultValue.
        //! \deprecated
        bool addToDictionary(EventPtr event, const std::string key, const Variant& value);
        bool addToDictionary(EventPtr event, const std::string key, const SingleTypeVariantListPtr& valueList);
        //! \brief Checks whether the maps have equal keys and equal Types.
        bool equalKeys(const StringVariantContainerBaseMap& dict1, const StringVariantContainerBaseMap& dict2);
        //! \brief Checks whether the maps have equal keys and equal Types.
        bool equalKeys(const StateParameterMap& dict1, const StateParameterMap& dict2);
        //! \brief Clears the destination map and copies the parameters of the source in it.
        //! The copies have their own memory.
        void copyDictionary(const StringVariantContainerBaseMap& source, StringVariantContainerBaseMap& destination);
        //! \brief Clears the destination map and copies the parameters of the source in it.
        //! The copies have their own memory.
        void copyDictionary(const StateParameterMap& source, StateParameterMap& destination);
        //! \brief Clears the destination map and copies the parameters of the source in it.
        //! The copies have their own memory.
        void copyDictionary(const StateParameterMap& source, StringVariantContainerBaseMap& destination);
        //! \brief Clears the destination map and copies the parameters of the source in it.
        //! The copies have their own memory.
        void copyDictionary(const StringVariantContainerBaseMap& source, StateParameterMap& destination);
        //! \brief Tries to fill the destination map with matching entries of the source map. Entries that could not be found, are not changed.
        void fillDictionary(const StringVariantContainerBaseMap& source, StringVariantContainerBaseMap& destination);
        //! \brief Tries to fill the destination map with matching entries of the source map. Entries that could not be found, are not changed.
        void fillDictionary(const StringVariantContainerBaseMap& source, StateParameterMap& destination);

        //! Converts the map into a string-representation.
        std::string getDictionaryString(const StringVariantContainerBaseMap& mymap);
        //! Converts the map into a string-representation.
        std::string getDictionaryString(const StateParameterMap& mymap);

        std::string getVariantString(const VariantBasePtr& var, const std::string& name = "");
        std::string getVariantString(const VariantPtr& var, const std::string& name = "");

        class StateLock:
            boost::timed_mutex
        {
        public:
            void lock();
            bool try_lock();
            bool timed_lock();
            void unlock();
        };
    }
}

