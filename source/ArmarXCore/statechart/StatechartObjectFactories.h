/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Core
* @author     Kai Welke (welke _at_ kit _dot_ edu)
* @date       2012 Kai Welke
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <Ice/ObjectFactory.h>
#include <ArmarXCore/core/system/FactoryCollectionBase.h>
#include <ArmarXCore/statechart/StateBase.h>
#include <ArmarXCore/statechart/StateTemplate.h>
#include <ArmarXCore/statechart/RemoteState.h>
//#include <ArmarXCore/statechart/RemoteStateOfferer.h>
#include <ArmarXCore/statechart/ParameterMapping.h>
#include <ArmarXCore/statechart/StateParameter.h>

#include <typeinfo>

namespace armarx
{
    // State for ObjectFactory only
    struct IceGeneratedState: StateTemplate<IceGeneratedState> {};

    class StateObjectFactory : public Ice::ObjectFactory
    {
    public:
        Ice::ObjectPtr create(const std::string& type) override
        {
            assert(type == armarx::StateIceBase::ice_staticId());
            return IceGeneratedState::createInstance();
        }
        void destroy() override {}
    };

    class RemoteStateObjectFactory : public Ice::ObjectFactory
    {
    public:
        Ice::ObjectPtr create(const std::string& type) override
        {
            assert(type == armarx::RemoteStateIceBase::ice_staticId());
            return new RemoteState();
        }
        void destroy() override {}
    };

    class ParameterMappingObjectFactory : public Ice::ObjectFactory
    {
    public:
        Ice::ObjectPtr create(const std::string& type) override
        {
            assert(type == armarx::ParameterMappingIceBase::ice_staticId());
            return createMapping();
        }
        void destroy() override {}
    };

    class StateParameterObjectFactory : public Ice::ObjectFactory
    {
    public:
        Ice::ObjectPtr create(const std::string& type) override
        {
            assert(type == armarx::StateParameterIceBase::ice_staticId());
            return StateParameter::create();
        }
        void destroy() override {}
    };



    namespace ObjectFactories
    {
        /**
        * @class StatechartObjectFactories
        * @brief
        */
        class StatechartObjectFactories : public FactoryCollectionBase
        {
        public:
            ObjectFactoryMap getFactories() override
            {
                ObjectFactoryMap map;

                map.insert(std::make_pair(armarx::RemoteStateIceBase::ice_staticId(), new RemoteStateObjectFactory));
                map.insert(std::make_pair(armarx::StateIceBase::ice_staticId(), new StateObjectFactory));
                map.insert(std::make_pair(armarx::ParameterMappingIceBase::ice_staticId(), new ParameterMappingObjectFactory));
                map.insert(std::make_pair(armarx::StateParameterIceBase::ice_staticId(), new StateParameterObjectFactory));

                return map;
            }
        };
        const FactoryCollectionBaseCleanUp StatechartObjectFactoriesVar = FactoryCollectionBase::addToPreregistration(new StatechartObjectFactories());
    }
}

