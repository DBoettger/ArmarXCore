armarx_component_set_name("ArmarXFileLoggerApp")

set(COMPONENT_LIBS
    ArmarXCoreInterfaces
    ArmarXCore
    ArmarXFileLogger
)

set(EXE_SOURCE main.cpp)

armarx_add_component_executable("${EXE_SOURCE}")
