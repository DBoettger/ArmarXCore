
armarx_component_set_name(ExampleUnitObserver)

set(COMPONENT_LIBS ArmarXCore ArmarXCoreObservers)

set(SOURCES main.cpp)

armarx_add_component_executable("${SOURCES}")
