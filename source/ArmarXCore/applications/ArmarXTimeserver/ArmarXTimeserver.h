/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::application::ArmarXTimeserver
 * @author     Clemens Wallrath ( uagzs at student dot kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */

#pragma once


#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/services/tasks/PeriodicTask.h>
#include <ArmarXCore/interface/core/TimeServerInterface.h>

#include <ArmarXCore/core/time/TimeKeeper.h>

namespace armarx
{
    /**
     * @class ArmarXTimeserverPropertyDefinitions
     * @brief
     */
    class ArmarXTimeserverPropertyDefinitions:
        public armarx::ComponentPropertyDefinitions
    {
    public:
        ArmarXTimeserverPropertyDefinitions(std::string prefix):
            armarx::ComponentPropertyDefinitions(prefix)
        {
            defineOptionalProperty<int>("TimeStepMS", 1, "The number of milliseconds the time advances with every step()")
            .setMin(1);
        }
    };

    /**
     * @class ArmarXTimeserver
     * @brief simple implementation of the TimeserverInterface
     * @ingroup VirtualTime
     *
     * The ArmarXTimeserver provides a global synchronized time that can be stopped, stepped and changed in speed
     */
    class ArmarXTimeserver :
        virtual public armarx::Component,
        virtual public TimeServerInterface
    {
    public:
        /**
         * @see armarx::ManagedIceObject::getDefaultName()
         */
        std::string getDefaultName() const override
        {
            return GLOBAL_TIMESERVER_NAME;
        }

        ~ArmarXTimeserver() override;

    protected:

        /**
         * @brief broadcastTimeTask calls broadcastTime() periodically
         */
        PeriodicTask<ArmarXTimeserver>::pointer_type broadcastTimeTask;

        TimeKeeper clock;

        /**
         * time the clock advances dirung one call of step() in milliseconds
         */
        int stepTimeMS;

        /**
         * @brief getTime returns the current (simulator) time as milliseconds (as in IceUtil::Time::toMilliSeconds)
         * @return current time in milliseconds
         */
        Ice::Long getTime(const ::Ice::Current& = ::Ice::Current()) override;
        void stop(const ::Ice::Current& = ::Ice::Current()) override;
        void start(const ::Ice::Current& = ::Ice::Current()) override;
        /**
         * @brief step() advances the time by the value of ArmarX.ArmarXTimeserver.StepTimeMS (in milliseconds)
         * the default step length is 1 millisecond
         */
        void step(const ::Ice::Current& = ::Ice::Current()) override;
        void setSpeed(Ice::Float newSpeed, const ::Ice::Current& = ::Ice::Current()) override;
        Ice::Float getSpeed(const ::Ice::Current& = ::Ice::Current()) override;
        Ice::Int getStepTimeMS(const ::Ice::Current& = ::Ice::Current()) override;

        /**
         * @brief a handle for the topic "Time"
         */
        TimeServerListenerPrx timeTopicPrx;

        /**
         * @brief periodically writes the current time to the "Time" topic
         */
        void broadcastTime();

        /**
         * @see armarx::ManagedIceObject::onInitComponent()
         */
        void onInitComponent() override;

        /**
         * @see armarx::ManagedIceObject::onConnectComponent()
         */
        void onConnectComponent() override;

        /**
         * @see armarx::ManagedIceObject::onDisconnectComponent()
         */
        void onDisconnectComponent() override;

        /**
         * @see armarx::ManagedIceObject::onExitComponent()
         */
        void onExitComponent() override;

        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;
    };
}

