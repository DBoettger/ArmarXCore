armarx_component_set_name("ArmarXTimeserverApp")

set(COMPONENT_LIBS ArmarXCoreInterfaces ArmarXCore)

set(EXE_SOURCE ArmarXTimeserver.h ArmarXTimeserver.cpp main.cpp)

armarx_add_component_executable("${EXE_SOURCE}")
