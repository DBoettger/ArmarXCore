armarx_component_set_name("TopicRecorderApp")

set(COMPONENT_LIBS ArmarXCoreInterfaces ArmarXCore ArmarXCoreTopicRecording)

set(EXE_SOURCE main.cpp)

armarx_add_component_executable("${EXE_SOURCE}")
