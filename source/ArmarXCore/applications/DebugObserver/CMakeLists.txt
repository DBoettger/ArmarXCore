
armarx_component_set_name(DebugObserver)

set(COMPONENT_LIBS ArmarXCoreInterfaces ArmarXCore ArmarXCoreObservers)

set(SOURCES main.cpp)

armarx_add_component_executable("${SOURCES}")
