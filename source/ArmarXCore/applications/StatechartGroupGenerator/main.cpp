/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    Core::application::StatechartGroupDocGenerator
 * @author     Mirko Waechter
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include <ArmarXCore/core/logging/Logging.h>
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <ArmarXCore/statechart/xmlstates/StateGroupGenerator.h>

int main(int argc, char* argv[])
{
    auto parseDeps = [](std::string const & dependencies)
    {
        auto depSplit = armarx::Split(dependencies, ";", true, true);
        std::map<std::string, std::string> depsMap;
        for (auto& pairStr : depSplit)
        {
            auto pair = armarx::Split(pairStr, ":", true, true);
            ARMARX_CHECK_EQUAL_W_HINT(pair.size(), 2, pairStr);
            depsMap[pair[0]] = pair[1];
        }
        return depsMap;
    };
    if (argc == 4 && std::string(argv[1]) == "context")
    {
        std::string statechartGroupXmlFilePath(argv[2]);
        std::string packagePath(argv[3]);
        armarx::StatechartGroupGenerator::generateStatechartContextFile(statechartGroupXmlFilePath, packagePath);
    }
    else if (argc == 7 && std::string(argv[1]) == "package-cmake")
    {
        std::string packageName(argv[2]);
        std::string statechartsDir(argv[3]);
        std::string buildDir(argv[4]);
        std::string dataDir(argv[5]);
        std::string dependencies(argv[6]);
        auto depsMap = parseDeps(dependencies);
        armarx::StatechartGroupGenerator::generateStatechartGroupCMakeSourceListFiles(packageName, statechartsDir, buildDir, false, dataDir, depsMap);

    }
    else if (argc == 7 && std::string(argv[1]) == "cmake")
    {
        std::string packageName(argv[2]);
        std::string statechartGroupXmlFilePath(argv[3]);
        std::string buildDir(argv[4]);
        std::string dataDir(argv[5]);
        std::string dependencies(argv[6]);
        auto depsMap = parseDeps(dependencies);
        armarx::StatechartGroupGenerator::generateStatechartGroupCMakeSourceListFile(packageName, statechartGroupXmlFilePath, buildDir, false, dataDir, depsMap);
    }
    else if (argc == 4 && std::string(argv[1]).find(".scgxml") != std::string::npos)
    {
        std::string statechartGroupXmlFilePath(argv[1]);
        std::string stateFile(argv[2]);
        std::string packagePath(argv[3]);
        armarx::StatechartGroupGenerator::generateStateFile(statechartGroupXmlFilePath, stateFile, packagePath);
    }
    else
    {
        throw armarx::LocalException("Incorrect parameters. Expected: (<Statechartgroupfile.scgxml> <statefile.xml> <packagePath> "
                                     "| context <Statechartgroupfile.scgxml> <packagePath> "
                                     "| cmake <Statechartgroupfile.scgxml> <builddir> <dependenciesWithPaths>"
                                     "| package-cmake <StatechartDir> <builddir> <dependenciesWithPaths>) .");
    }
    return 0;
}
