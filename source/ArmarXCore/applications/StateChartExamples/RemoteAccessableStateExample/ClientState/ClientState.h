/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarXCore::StatechartExamples::RemoteAccessableStateExample::
* @author     Mirko Waechter( mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/system/ImportExportComponent.h>
//#include <interface/TestCenter/TestCenter.h>

#include <ArmarXCore/statechart/Statechart.h>

namespace armarx
{
    class ARMARXCOMPONENT_IMPORT_EXPORT ClientState :
        virtual public StatechartContext
    {
    public:

        // inherited from Component
        std::string getDefaultName() const override
        {
            return "ClientState";
        };
        void onInitStatechart() override;
        void onConnectStatechart() override;



    };

    //Event Definitions
    DEFINEEVENT(OuterTimer)

    struct StatechartClient : StateTemplate<StatechartClient>
    {
        void defineState() override
        {
            //            setStateName("Client");
        }
        void defineParameters() override
        {

            addToInput("x", VariantType::Float, true);
            addToInput("y", VariantType::Float, true);

            addToOutput("result", VariantType::Float, false);
        }

        void defineSubstates() override
        {
            //add substates
            StateBasePtr remoteState = addRemoteState("add_x_to_y", "RemoteAccessableStateOfferer");
            setInitState(remoteState, createMapping()->mapFromParent("*", "*"));
            StatePtr finalSuccess = addState<SuccessState>("Success");
            StatePtr finalFailure = addState<FailureState>("Failure");

            // add transitions
            addTransition<OuterTimer>(getInitState(),
                                      finalFailure);
            addTransition<Success>(remoteState,
                                   finalSuccess);
        }
    };




}

