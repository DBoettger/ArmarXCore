
armarx_component_set_name(TimeoutExample)

#find_package(Simox QUIET)
#armarx_build_if(Simox_FOUND "Simox-VirtualRobot not available")


set(COMPONENT_LIBS ArmarXCoreInterfaces ArmarXCore ArmarXCoreObservers ArmarXCoreStatechart)

set(SOURCES main.cpp
    TimeoutExample.cpp
    TimeoutExample.h
)

armarx_add_component_executable("${SOURCES}")
