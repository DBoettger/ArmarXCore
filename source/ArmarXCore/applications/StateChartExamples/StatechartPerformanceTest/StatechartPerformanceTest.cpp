/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::StatechartPerfomanceTest
* @author     ( at kit dot edu)
* @date
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "StatechartPerformanceTest.h"

namespace armarx
{


#ifdef REMOTE
    void StatechartPerfomanceTest::onInitRemoteStateOfferer()
    {
        addState<Statechart_StatechartPerfomanceTest>("PerfTest");
    }
#else
    void StatechartPerformanceTest::onInitStatechart()
    {
        std::cout << "onInitComponent" << std::endl;
        StringVariantContainerBaseMap input;

        input["Timeout"] = new SingleVariant(4);
        input["name"] = new SingleVariant(std::string("myname"));
        input["file"] = new SingleVariant(std::string("file://////"));
        SingleTypeVariantListPtr list = new SingleTypeVariantList(VariantType::Map(VariantType::Float));
        StringValueMapPtr map = new StringValueMap(VariantType::Float);
        map->addVariant("left", 0.3f);
        list->addElement(map);
        input["sampleList"] = list;
        setToplevelState(Statechart_StatechartPerfomanceTest::createInstance(), input);
    }

    void StatechartPerformanceTest::onConnectStatechart()
    {
        ARMARX_LOG << eINFO << "Starting StatechartPerfomanceTest" << flush;

    }

    void StatechartPerformanceTest::onExitStatechart()
    {
        ARMARX_VERBOSE << "thread count: " << ThreadList::getApplicationThreadList()->getRunningTaskNames().size();
    }
#endif



    void Statechart_StatechartPerfomanceTest::defineParameters()
    {
        setLocalMinimumLoggingLevel(eWARN);

        //        this->setLocalMinimumLoggingLevel();
        addToInput("Timeout", VariantType::Int, false);
        addToInput("name", VariantType::String, false);
        addToInput("file", VariantType::String, false);
        addToInput("sampleList", VariantType::List(VariantType::Map(VariantType::Float)), false);
    }

    void Statechart_StatechartPerfomanceTest::defineSubstates()
    {

        PMPtr mapping = createMapping()
                        ->mapFromParent("Timeout", "Timeout")
                        ->mapFromParent("name", "name")
                        ->mapFromParent("file", "file")
                        ->mapFromParent("sampleList");
        //add substates


        StatePtr stateRun = addState<StateRun>("Running");

        StatePtr stateRun2 = addState<StateRun>("Running2");
        setInitState(stateRun, mapping);
        StatePtr stateSuccess = addState<SuccessState>("Success"); // preimplemented state derived from FinalState, that triggers a transition on the upper state
        StatePtr stateFailure = addState<FailureState>("Failure"); // preimplemented state derived from FinalState, that triggers a transition on the upper state



        // add transitions
        //        addTransition<EvNext>(getInitState(), stateRun, mapping);
        addTransition<EvNext>(stateRun, stateRun2, mapping);
        addTransition<EvTimeout>(stateRun, stateRun2, mapping);
        addTransitionFromAllStates<Failure>(stateFailure, mapping);
        addTransition<EvNext>(stateRun2, stateRun, mapping);
        addTransition<EvTimeout>(stateRun2, stateRun, mapping);
        addTransition<Success>(stateRun, stateSuccess);
        addTransition<Success>(stateRun2, stateSuccess);
        // ...add more transitions
    }



    void StateRun::defineParameters()
    {
        addToInput("Timeout", VariantType::Int, false);
        addToInput("name", VariantType::String, false);
        addToInput("file", VariantType::String, false);
        addToInput("sampleList", VariantType::List(VariantType::Map(VariantType::Float)), false);
        counter = 0;
    }

    void StateRun::onEnter()
    {

        //        lastCall =  IceUtil::Time::now();
        //        if(counter <= 2)
        //            setTimeoutEvent(100, createEvent<EvTimeout>());
        if (counter == 0)
        {
            lastCall =  IceUtil::Time::now();
        }

        counter++;

        //sleep(1);
        if (counter > 1000)
        {
            IceUtil::Time dura = IceUtil::Time::now() - lastCall;
            ARMARX_ERROR << "c: " << counter << " Dura total: " << dura.toMilliSeconds() << " avg Duration in ms: " << dura.toMilliSecondsDouble() / counter / 2 << flush;
            ARMARX_ERROR << "Timeout: " << getInput<int>("Timeout") << flush;
            ARMARX_ERROR << "List Entry: " << getInput<SingleTypeVariantList>("sampleList")->getElement<StringValueMap>(0)->getVariant("left")->getFloat() << flush;
            counter = 0;
            lastCall =  IceUtil::Time::now();
            sendEvent<Failure>();
        }
        else
        {
            //            ARMARX_VERBOSE << "counter :" << counter << std::endl;
            //            setTimeoutEvent(100, createEvent<EvNext>());
            sendEvent<EvNext>();
            //            ARMARX_VERBOSE << "counter after:" << counter << std::endl;
        }

        //        usleep(100000);

    }



    void StateRun::onExit()
    {
        ARMARX_VERBOSE <<  "exiting StateRun" << std::endl;
    }



}
