/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarXCore::StatechartExamples::StateParameterExample
* @author     Mirko Waechter (mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "StateParameterExample.h"
#include <ArmarXCore/core/system/ArmarXDataPath.h>

namespace armarx
{



    void StateParameterExample::onInitStatechart()
    {

        std::cout << "onInitComponent" << std::endl;
        setToplevelState(Statechart_StateParameterExample::createInstance("Statechart_StateParameterExample"));

    }


    void StateParameterExample::onConnectStatechart()
    {
        //        ARMARX_LOG << eINFO << "Starting StateParameterExample" << flush;

        //        StringParameterBaseMap input;
        ////        input["x"] = new VariantParameter(3.f);
        ////        input["y"] = new VariantParameter(4.f);
        ////        input["timeout"] = new VariantParameter(3);
        //        if(statechart->isInitialized())
        //            statechart->start(&input); // if the initial state requires input parameters, pass them as a StringParameterBaseMap to start()
        //        else{
        //            ARMARX_LOG << eERROR << "Cannot start statechart. Statechart  '" << statechart->getStateName() << "' was not initialized.\n" << flush;
        //        }

    }

    void StateParameterExample::run()
    {
    }

    void Statechart_StateParameterExample::defineState()
    {
    }

    void Statechart_StateParameterExample::defineSubstates()
    {
        //add substates
        StatePtr stateRun = addState<StateRun>("Running");
        setInitState(stateRun, createMapping()
                     ->mapFromParent("*", "*")
                     ->mapFromParent("timeout", "timeout2")
                     ->mapFromParent("x", "x")
                     ->mapFromParent("y", "y")
                     ->mapFromParent("ObjectList", "ObjectList")
                     ->setSourcePriority(3, eOutput)
                     ->setSourcePriority(2, eParent));
        StatePtr stateResult = addState<StateResult>("Result");
        StatePtr stateSuccess = addState<SuccessState>("Success"); // preimplemented state derived from FinalState, that sends triggers a transition on the upper state
        StatePtr stateFailure = addState<FailureState>("Failure"); // preimplemented state derived from FinalState, that sends triggers a transition on the upper state


        // add transitions
        addTransition<EvNext>(stateRun, stateResult,
                              createMapping()->mapFromOutput("*", "my.*"));
        addTransition<EvNext>(stateResult, stateSuccess,
                              createMapping()->mapFromOutput("*", "*"));


        // ...add more transitions
    }

    void Statechart_StateParameterExample::defineParameters()
    {


        // add input
        addToInput("timeout", VariantType::Int, false);
        addToInput("x", VariantType::Float, true);
        addToInput("y", VariantType::Float, true);
        addToInput("ObjectList", VariantType::List(VariantType::String), false);



        // add output
        addToOutput("result", VariantType::Float, true);

    }

    void Statechart_StateParameterExample::onEnter()
    {
        //ARMARX_INFO << "Pos: " << getInput<Variant>("Position")->getLinkedPosition()->x << endl;

    }

    void StateRun::defineParameters()
    {
        //add input
        addToInput("timeout2", VariantType::Int, false);
        addToInput("x", VariantType::Float, true);
        addToInput("y", VariantType::Float, true);
        addToInput("ObjectList", VariantType::List(VariantType::String), false);

        // add output
        addToOutput("result", VariantType::Float, true);

    }

    void StateRun::onEnter()
    {
        // call is optional, just a simple output that this state was entered
        setOutput("result",  Variant(getInput<float>("x") + getInput<float>("y")));
        sendEvent<EvNext>();

    }

    void StateRun::onBreak()
    {

    }

    void StateRun::onExit()
    {
        removeCondition(condId);

    }

    void StateResult::defineParameters()
    {
        //add input
        addToInput("my.result", VariantType::Float, false);
        // add output
        addToOutput("result", VariantType::Float, true);

    }

    void StateResult::onEnter()
    {
        setOutput("result", getInput<float>("my.result"));
        ARMARX_INFO << "my.result: " << getInput<float>("my.result") << flush;
        sendEvent(createEvent<EvNext>());

    }


}
