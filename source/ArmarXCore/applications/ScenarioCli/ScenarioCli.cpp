/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Cedric Seehausen (usdnr at kit dot edu)
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include "ScenarioCli.h"

#include <ArmarXCore/util/ScenarioManagerCommon/parser/PackageBuilder.h>
#include <ArmarXCore/util/ScenarioManagerCommon/parser/XMLScenarioParser.h>
#include <ArmarXCore/util/ScenarioManagerCommon/executor/StopStrategyFactory.h>
#include <ArmarXCore/util/ScenarioManagerCommon/executor/StopperFactory.h>
#include <ArmarXCore/util/ScenarioManagerCommon/executor/StarterFactory.h>
#include <ArmarXCore/core/logging/Logging.h>
#include <ArmarXCore/core/system/ArmarXDataPath.h>
#include <ArmarXCore/core/application/ApplicationOptions.h>
#include <ArmarXCore/core/time/TimeUtil.h>
#include <iostream>
#include <boost/filesystem.hpp>

using namespace armarx;
using namespace ScenarioManager;
using namespace Parser;
using namespace Data_Structure;
using namespace Exec;

ScenarioCli::ScenarioCli()
{

}

void ScenarioCli::setup(const ManagedIceObjectRegistryInterfacePtr& registry,
                        Ice::PropertiesPtr properties)
{
    //this->registry = registry;
}

int ScenarioCli::exec(const ArmarXManagerPtr& armarXManager)
{
    if (!options.wait || options.print)
    {
        armarXManager->shutdown();
    }

    armarXManager->waitForShutdown();
    return 0;
}

int ScenarioCli::run(int argc, char* argv[])
{
    try
    {
        setIceProperties(IceProperties::create(ApplicationOptions::mergeProperties(communicator()->getProperties()->clone(), argc, argv)));

        ScenarioManagerCliOptions optionsManager;
        options = optionsManager.parseCmdOptions(argc, argv);


        //Handle help | error of parsing
        if (options.showHelp || options.error)
        {
            if (options.error)
            {
                optionsManager.showHelp(options, "Could not parse input parameters");
                return 0;
            }
            optionsManager.showHelp(options);
            return 0;
        }
        if (!options.parameters.empty())
        {
            std::cout << "Additional command line parameters are: " << options.parameters << std::endl;
        }

        //capture edge cases
        if (options.print && options.command != "start")
        {
            optionsManager.showHelp(options, "Print is only a valid option for the start command");
            return 0;
        }
        else if (options.wait && (options.command == "list" || options.command == "status"))
        {
            optionsManager.showHelp(options, "Wait is not valid for the list or status command");
            return 0;
        }

        PackageBuilder builder;

        //Handle list
        if (options.command.compare("list") == 0)
        {
            std::cout << "Listing all Scenarios out of default Packages" << std::endl;
            for (auto package : getDefaultPackageNames())
            {
                std::cout << "Scenarios in " << package << ":" << std::endl;
                PackagePtr currentPackage = builder.parsePackage(package);
                if (currentPackage.get() == nullptr)
                {
                    std::cout << "Package " << package << " is listed in default packages, but could not be found" << std::endl << std::endl << std::endl;
                    continue;
                }
                for (auto scenario : * (currentPackage->getScenarios()))
                {
                    std::cout << scenario->getName() << std::endl;
                }
                std::cout << std::endl << std::endl;
            }
            return 0;
        }

        std::string packageName = options.packageName;
        std::string scenarioName = ScenarioManagerCliOptions::GetScenarioNameByCommandLineInput(options);

        if (options.packageName.empty())
        {
            if (boost::filesystem::is_regular_file(boost::filesystem::path(options.scxPath))
                || boost::filesystem::is_directory(boost::filesystem::path(options.scxPath)))
            {
                XMLScenarioParser parser;
                std::string filePackageName;
                if (boost::filesystem::is_regular_file(boost::filesystem::path(options.scxPath)))
                {
                    filePackageName = parser.getPackageNameFromScx(options.scxPath);
                }
                else
                {
                    filePackageName = parser.getPackageNameFromScx((boost::filesystem::path(options.scxPath) / boost::filesystem::path(scenarioName + ".scx")).string());
                }

                if (filePackageName != "")
                {
                    packageName = filePackageName;
                }
                else
                {
                    packageName = packageChoosingDialog(scenarioName);
                    if (packageName.empty())
                    {
                        options.error = true;
                        optionsManager.showHelp(options, "Could not find any package for " + options.scxPath + ". Please add a PackageName or Add the PackageName to your default config.");
                        return 0;
                    }
                }
            }
            else
            {
                packageName = packageChoosingDialog(scenarioName);
                if (packageName.empty())
                {
                    options.error = true;
                    optionsManager.showHelp(options, "Could not find any package for " + options.scxPath + ". Please add a PackageName or Add the PackageName to your default config.");
                    return 0;
                }
            }
        }

        if (scenarioName.empty())
        {
            options.error = true;
            optionsManager.showHelp(options, "Could not find Scenario " + options.scxPath + " in Package " + packageName);
            return 0;
        }

        PackagePtr package;
        ScenarioPtr scenario;

        CMakePackageFinder finder(packageName);
        if (!finder.packageFound())
        {
            options.error = true;
            optionsManager.showHelp(options, "CMakePackageFinder could not find package " + packageName);
            return 0;
        }

        XMLScenarioParser parser;
        std::vector<std::string> scenarios = parser.getScenariosFromFolder(finder.getScenariosDir());

        for (auto scen : scenarios)
        {
            auto sFolderPos = scen.rfind("/");
            if ((sFolderPos == std::string::npos && scen == scenarioName)
                || (sFolderPos != std::string::npos && scen.substr(sFolderPos + 1) == scenarioName))
            {
                package = builder.parsePackage(packageName, StringList {scen + "::Package::" + packageName});
                break;
            }
        }

        if (package.get() == nullptr)
        {
            optionsManager.showHelp(options, "Failed to parse package " + packageName);
            return 0;
        }

        scenario = package->getScenarioByName(scenarioName);


        if (scenario.get() == nullptr)
        {
            options.error = true;
            optionsManager.showHelp(options, "Could not find Scenario " + scenarioName + " in Package " + package->getName());
            return 0;
        }


        if (options.applicationName.empty())
        {
            startScenario(options.command, scenario, options.print, options.parameters);
        }
        else
        {
            ApplicationInstancePtr app = scenario->getApplicationByName(options.applicationName);
            if (app.get() != nullptr)
            {
                startApplication(options.command, app, options.print, options.parameters);
            }
            else
            {
                options.error = true;
                optionsManager.showHelp(options, "Could not find Application " + options.applicationName + " in Scenario " + scenarioName);
                return 0;
            }
        }

        if (!options.print)
        {
            std::cout << "Finished" << std::endl;
        }

        return armarx::Application::run(1, argv);
    }
    catch (LocalException& e)
    {
        std::cout << "Command failed: " << e.getReason() << std::endl;
        return 1;
    }
}

std::string ScenarioCli::packageChoosingDialog(std::string scenarioName)
{
    StringList packages = PackageBuilder::FilterPackagesForScenario(scenarioName);

    if (packages.size() == 0)
    {
        return "";
    }
    else if (packages.size() == 1)
    {
        return packages[0];
    }
    else if (packages.size() > 1)
    {
        size_t answer = -1;
        while (answer > packages.size())
        {
            std::cout << "Found multiple possible packages for this Scenario path, please select: " << std::endl;
            int number = 1;
            for (auto package : packages)
            {
                std::cout << number << ") " << package << std::endl;
                number++;
            }
            std::cout << "Please enter a number now: " << std::endl;

            std::string answerString;
            std::cin >> answerString;
            try
            {
                answer = boost::lexical_cast<size_t>(answerString);
            }
            catch (...)
            {
                std::cout << "ERROR: Could not convert input to a number - please type in a number between 1 and " << packages.size() << std::endl << std::endl;
                continue;
            }

            if (answer > packages.size() || answer == 0)
            {
                answer = -1;
                continue;
            }
            answer -= 1;
        }
        return packages[answer];
    }

    return "";
}

void ScenarioCli::startScenario(std::string command, ScenarioManager::Data_Structure::ScenarioPtr scenario, bool printOnly, const std::string& commandLineParameters)
{
    StopStrategyFactory stopStrategyFactory;
    Executor executor(stopStrategyFactory.getStopStrategy(StopperFactory::getFactory()->getPidStopper()), StarterFactory::getFactory()->getStarter());

    std::future<void> result;

    //look if the app is startet or not and look for pids
    for (auto app : *scenario->getApplications())
    {
        app->setStatus(executor.getApplicationStatus(app));
    }


    if (command == "status")
    {
        std::cout << "Status of " << scenario->getName() << " is: ";

        if (scenario->getStatus() == "Running")
        {
            coloredStdOut("Running", 92);
        }
        else if (scenario->getStatus() == "Stopped")
        {
            coloredStdOut("Stopped", 31);
        }
        else if (scenario->getStatus() == "Mixed")
        {
            coloredStdOut("Mixed", 93);
        }
        std::cout << std::endl;
        for (auto app : *scenario->getApplications())
        {
            std::cout << "\t " << app->getName() << " : ";
            if (app->getStatus() == "Stopped")
            {
                coloredStdOut("Stopped", 31);
            }
            else if (app->getStatus() == "Running")
            {
                coloredStdOut("Running", 92);
            }
            std::cout << std::endl;
        }
        return;
    }
    else if (command == "periodic_status")
    {
        while (true)
        {
            std::cout << "Status of " << scenario->getName() << " is: ";

            if (scenario->getStatus() == "Running")
            {
                coloredStdOut("Running", 92);
            }
            else if (scenario->getStatus() == "Stopped")
            {
                coloredStdOut("Stopped", 31);
            }
            else if (scenario->getStatus() == "Mixed")
            {
                coloredStdOut("Mixed", 93);
            }
            std::cout << std::endl;
            for (auto app : *scenario->getApplications())
            {
                app->setStatus(executor.getApplicationStatus(app));
            }
            TimeUtil::SleepMS(1000);
        }
    }
    else if (command.compare("start") == 0)
    {
        if (!printOnly)
        {
            std::cout << "Starting Scenario ";
        }
        result = executor.startScenario(scenario, printOnly, commandLineParameters);
    }
    else if (command.compare("stop") == 0)
    {
        std::cout << "Stopping Scenario ";
        result = executor.stopScenario(scenario);
    }
    else if (command.compare("kill") == 0)
    {
        std::cout << "Killing Scenario ";
        executor.setStopStrategy(stopStrategyFactory.getStopAndKillStrategy(StopperFactory::getFactory()->getPidStopper(), 500));
        result = executor.stopScenario(scenario);
    }
    else if (command.compare("restart") == 0)
    {
        std::cout << "Restarting Scenario ";
        executor.setStopStrategy(stopStrategyFactory.getStopAndKillStrategy(StopperFactory::getFactory()->getPidStopper(), 500));
        result = executor.restartScenario(scenario, printOnly);
    }

    if (!printOnly)
    {
        std::cout << scenario->getName() << std::endl;
    }

    result.wait();
}

void ScenarioCli::startApplication(std::string command, ScenarioManager::Data_Structure::ApplicationInstancePtr app, bool printOnly, const std::string& commandLineParameters)
{
    StopStrategyFactory stopStrategyFactory;
    Executor executor(stopStrategyFactory.getStopStrategy(StopperFactory::getFactory()->getPidStopper()), StarterFactory::getFactory()->getStarter());

    std::future<void> result;

    //look for app pid
    app->setStatus(executor.getApplicationStatus(app));

    if (command == "status")
    {
        std::cout << "Status of " << app->getName() << "is: ";
        if (app->getStatus() == "Stopped")
        {
            coloredStdOut("Stopped", 31);
        }
        else if (app->getStatus() == "Running")
        {
            coloredStdOut("Running", 92);
        }
        std::cout << std::endl;
        return;
    }
    else if (command.compare("start") == 0)
    {
        if (!printOnly)
        {
            std::cout << "Starting Application ";
        }
        result = executor.startApplication(app, printOnly, commandLineParameters);
    }
    else if (command.compare("stop") == 0)
    {
        std::cout << "Stopping Application ";
        result = executor.stopApplication(app);
    }
    else if (command.compare("kill") == 0)
    {
        std::cout << "Killing Application ";
        executor.setStopStrategy(stopStrategyFactory.getStopAndKillStrategy(StopperFactory::getFactory()->getByNameStopper(), 0));
        result = executor.stopApplication(app);
    }
    else if (command.compare("restart") == 0)
    {
        std::cout << "Restarting Application ";
        executor.setStopStrategy(stopStrategyFactory.getStopAndKillStrategy(StopperFactory::getFactory()->getByNameStopper(), 0));
        result = executor.restartApplication(app, printOnly);
    }

    if (!printOnly)
    {
        std::cout << app->getName() << std::endl;
    }
    result.wait();
}

void ScenarioCli::coloredStdOut(std::string message, int colorCode)
{
    std::cout << "\033[1;" << colorCode << "m" << message << "\033[0m";
}
