/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Cedric Seehausen (usdnr at kit dot edu)
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once

#include <string>
#include <boost/program_options.hpp>

namespace armarx
{
    struct CmdOptions
    {
        bool showHelp;
        bool error;
        bool print;
        bool wait;
        std::string scxPath;
        std::string command;
        std::string packageName;
        std::string applicationName;
        std::string parameters;
        boost::shared_ptr<boost::program_options::options_description> description;
    };

    class ScenarioManagerCliOptions
    {
    public:
        CmdOptions parseCmdOptions(int argc, char* argv[]);

        void showHelp(CmdOptions options, std::string errorMessage = "");

        static std::string GetScenarioNameByCommandLineInput(CmdOptions string);
    };
}

