/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Cedric Seehausen (usdnr at kit dot edu)
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once

#include "ArmarXCore/applications/ScenarioCli/ScenarioCliOptions.h"

#include <ArmarXCore/core/application/Application.h>
#include <ArmarXCore/util/ScenarioManagerCommon/executor/Executor.h>




namespace armarx
{
    class ScenarioCli : virtual public armarx::Application
    {
    public:
        ScenarioCli();

    protected:
        void setup(const ManagedIceObjectRegistryInterfacePtr& registry,
                   Ice::PropertiesPtr properties) override;

        int exec(const ArmarXManagerPtr& armarXManager) override;

    private:
        int run(int argc, char* argv[]) override;

        std::string packageChoosingDialog(std::string scenarioName);

        void startScenario(std::string command, ScenarioManager::Data_Structure::ScenarioPtr scenario, bool printOnly, const std::string& commandLineParameters = "");
        void startApplication(std::string command, ScenarioManager::Data_Structure::ApplicationInstancePtr app, bool printOnly, const std::string& commandLineParameters = "");

        void coloredStdOut(std::string message, int colorCode);

        CmdOptions options;
    };
}

