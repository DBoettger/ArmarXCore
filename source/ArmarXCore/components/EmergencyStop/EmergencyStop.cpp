/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::ArmarXObjects::EmergencyStop
 * @author     Stefan Reither ( stef dot reither at web dot de )
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "EmergencyStop.h"


using namespace armarx;

void EmergencyStopMaster::setEmergencyStopState(EmergencyStopState state, const ::Ice::Current&)
{
    this->emergencyStopState = state;
    std::string s;
    switch (state)
    {
        case EmergencyStopState::eEmergencyStopActive :
            s = "eEmergencyStopActive";
            break;
        case EmergencyStopState::eEmergencyStopInactive :
            s = "eEmergencyStopInactive";
            break;
        default:
            s = "(Unknown)";
    }

    ARMARX_INFO_S << "Setting EmergencyStopState to " << s;
    this->emergencyStopTopic->reportEmergencyStopState(state);
}

EmergencyStopState EmergencyStopMaster::getEmergencyStopState(const ::Ice::Current&) const
{
    return this->emergencyStopState;
}

void EmergencyStopMaster::onInitComponent()
{
    armarx::ManagedIceObject::offeringTopic(getProperty<std::string>("EmergencyStopTopic").getValue());
}


void EmergencyStopMaster::onConnectComponent()
{
    emergencyStopTopic = armarx::ManagedIceObject::getTopic<EmergencyStopListenerPrx>(getProperty<std::string>("EmergencyStopTopic").getValue());

    // Default state is inactive
    this->setEmergencyStopState(EmergencyStopState::eEmergencyStopInactive);
}

armarx::PropertyDefinitionsPtr EmergencyStopMaster::createPropertyDefinitions()
{
    return armarx::PropertyDefinitionsPtr(new EmergencyStopPropertyMasterDefinitions(
            getConfigIdentifier()));
}
