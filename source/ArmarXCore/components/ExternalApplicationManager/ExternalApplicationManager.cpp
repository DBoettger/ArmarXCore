/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::ArmarXObjects::ExternalApplicationManager
 * @author     Stefan Reither ( stef dot reither at web dot de )
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "ExternalApplicationManager.h"

#include <ArmarXCore/core/ArmarXManager.h>
#include <ArmarXCore/core/system/cmake/CMakePackageFinder.h>
#include <ArmarXCore/core/system/ArmarXDataPath.h>
#include <ArmarXCore/core/logging/Logging.h>
#include <ArmarXCore/core/time/TimeUtil.h>
#include <ArmarXCore/core/application/Application.h>

#include <boost/filesystem.hpp>
#include <boost/process/initializers.hpp>

#include <IceUtil/UUID.h>
#include <signal.h>
#include <sys/prctl.h>
#include <thread>




using namespace armarx;
using namespace boost::process;
using namespace boost::process::initializers;
using namespace boost::iostreams;
using namespace boost::filesystem;


void ExternalApplicationManager::parseAdditionalArguments()
{
    const std::string argsStr = getProperty<std::string>("ApplicationArguments").getValue();

    const boost::regex re(",(?=(?:[^\\\"]*\\\"[^\\\"]*\\\")*[^\\\"]*$)");

    boost::match_results<std::string::const_iterator> what;
    boost::match_flag_type flags = boost::match_default;
    std::string::const_iterator s = argsStr.begin();
    std::string::const_iterator e = argsStr.end();
    size_t begin = 0;
    while (boost::regex_search(s, e, what, re, flags))
    {
        int pos = what.position();
        auto arg = argsStr.substr(begin, pos);
        if (!arg.empty())
        {
            args.push_back(arg);
        }
        std::string::difference_type l = what.length();
        std::string::difference_type p = what.position();
        begin += l + p;
        s += p + l;
    }
    std::string lastArg = argsStr.substr(begin);
    if (!lastArg.empty())
    {
        args.push_back(lastArg);
    }
}

void ExternalApplicationManager::onInitComponent()
{
    redirectToArmarXLog = getProperty<bool>("RedirectToArmarXLog").getValue();

    if (Application::getInstance() && Application::getInstance()->getProperty<std::string>("ApplicationName").isSet())
    {
        LogSender::SetComponentName(Application::getInstance()->getProperty<std::string>("ApplicationName").getValue());
    }
    else
    {
        LogSender::SetComponentName("ExtApp");
    }
    std::string applicationPath = getProperty<std::string>("ApplicationPath").getValue();

    try
    {
        workingDir = getProperty<bool>("BinaryPathAsWorkingDirectory") ?
                     boost::filesystem::path(application).parent_path().string()
                     : boost::filesystem::current_path().string();
    }
    catch (std::exception& e)
    {
        ARMARX_ERROR << "caught exception in line " << __LINE__ << " what:\n" << e.what();
        throw;
    }

    if (getProperty<std::string>("WorkingDirectory").isSet())
    {
        workingDir = getProperty<std::string>("WorkingDirectory").getValue();
        ArmarXDataPath::ReplaceEnvVars(workingDir);
        CMakePackageFinder::ReplaceCMakePackageFinderVars(workingDir);
        try
        {
            boost::filesystem::current_path(workingDir);
        }
        catch (std::exception& e)
        {
            ARMARX_ERROR << "caught exception in line " << __LINE__
                         << " when setting the current working directory to '"
                         << workingDir << "'. except.what:\n" << e.what();
            throw;
        }
        ARMARX_INFO << "Using '" << workingDir << "' as working directory";
    }

    if (getProperty<std::string>("ArmarXPackage").isSet())
    {
        CMakePackageFinder finder(getProperty<std::string>("ArmarXPackage").getValue());
        if (finder.packageFound())
        {
            application = finder.getBinaryDir() + "/" + applicationPath;
            if (!boost::filesystem::exists(application))
            {
                throw LocalException() << applicationPath << " does not exist in ArmarX-package " << finder.getName();
            }
        }
        else
        {
            throw LocalException() << finder.getName() << " is not an ArmarX-package";
        }
    }
    else
    {
        ArmarXDataPath::ResolveHomePath(applicationPath);
        CMakePackageFinder::ReplaceCMakePackageFinderVars(applicationPath);
        ArmarXDataPath::ReplaceEnvVars(applicationPath);
        if (!boost::filesystem::exists(applicationPath))
        {
            const std::string path = boost::process::search_path(applicationPath);
            if (path.empty())
            {
                throw LocalException() << applicationPath << " does not represent an executable.";
            }
            else
            {
                application = path;
            }
        }
        else
        {
            application = applicationPath;
        }
    }


    ARMARX_INFO << "Application absolute path: " << application;
    boost::filesystem::path appPath(application);
    setTag(appPath.stem().string());
    parseAdditionalArguments();


    args.insert(args.begin(), application);
    restartWhenCrash = getProperty<bool>("RestartInCaseOfCrash").getValue();
    disconnectWhenCrash = getProperty<bool>("DisconnectInCaseOfCrash").getValue();

    if (getProperty<bool>("PythonUnbuffered"))
    {
        envVars.push_back("PYTHONUNBUFFERED=1");
    }
    if (getProperty<Ice::StringSeq>("AdditionalEnvVars").isSet())
    {
        auto addEnvVrs = getProperty<Ice::StringSeq>("AdditionalEnvVars").getValue();
        envVars.insert(envVars.end(), addEnvVrs.begin(), addEnvVrs.end());

    }

    for (auto& var : envVars)
    {
        auto split = Split(var, "=");
        if (split.size() >= 2)
        {
            setenv(split.at(0).c_str(), split.at(1).c_str(), 1);
        }
    }

    startUpKeyword = getProperty<std::string>("FakeObjectDelayedStartKeyword");
    starterUUID = "ExternalApplicationManagerStarter" + IceUtil::generateUUID();
    depObjUUID = "ExternalApplicationManagerDependency" + IceUtil::generateUUID();

    usingProxy(depObjUUID);
    addStarterObject();
}


void ExternalApplicationManager::onConnectComponent()
{
    ARMARX_DEBUG << "External App Manager started";
    //    startApplication();

}


void ExternalApplicationManager::onDisconnectComponent()
{
    ARMARX_DEBUG << "Disconnecting " << getName();
}


void ExternalApplicationManager::onExitComponent()
{
    ARMARX_DEBUG << "Exiting " << getName();
}

armarx::PropertyDefinitionsPtr ExternalApplicationManager::createPropertyDefinitions()
{
    return armarx::PropertyDefinitionsPtr(new ExternalApplicationManagerPropertyDefinitions(
            getConfigIdentifier()));
}

void ExternalApplicationManager::restartApplication(const Ice::Current&)
{
    stopApplication();
    startApplication();
}

void ExternalApplicationManager::terminateApplication(const Ice::Current&)
{
    stopApplication();
}

std::string ExternalApplicationManager::getPathToApplication(const Ice::Current&)
{
    return application;
}

bool ExternalApplicationManager::isApplicationRunning(const Ice::Current&)
{
    return isAppRunning;
}

void ExternalApplicationManager::waitForApplication()
{
    if (isAppRunning)
    {
        try
        {
            wait_for_exit(*childProcess);
            cleanUp();

            if (restartWhenCrash && !appStoppedOnPurpose)
            {
                ARMARX_INFO << "Restarting application ...";
                startApplication();
            }
            else
            {
                isAppRunning = false;
                if (this->disconnectWhenCrash)
                {
                    this->getArmarXManager()->removeObjectBlocking(starterUUID);
                }
            }
        }
        catch (...)
        {
        }
    }
}

void ExternalApplicationManager::addDependencyObject()
{
    depObj = new ExternalApplicationManagerDependency;
    depObj->setParent(this);

    this->getArmarXManager()->addObject(depObj, false, depObjUUID);
    ARMARX_INFO << "Adding dummy app with name " << depObjUUID;

}

void ExternalApplicationManager::addStarterObject()
{
    starter = new ExternalApplicationManagerStarter();
    starter->setParent(this);
    starter->setDependencies(getProperty<Ice::StringSeq>("Dependencies").getValue());

    this->getArmarXManager()->addObject(starter, false, starterUUID);
}

void ExternalApplicationManager::setupStream(StreamMetaData& meta)
{


    meta.read =  [&, this](const boost::system::error_code & error, std::size_t length)
    {
        if (!error)
        {
            std::ostringstream ss;
            ss << &meta.input_buffer;
            std::string s = ss.str();

            boost::filesystem::path appPath(application);
            if (redirectToArmarXLog)
            {
                *((ARMARX_LOG_S) .setBacktrace(false)->setTag(LogTag(appPath.stem().string()))) << meta.level << s;
            }
            else
            {
                std::cout << s << std::flush;
            }

            if (!startUpKeyword.empty() && !depObj)
            {
                if (Contains(s, startUpKeyword))
                {
                    addDependencyObject();
                }
            }
            boost::asio::async_read_until(meta.pend, meta.input_buffer, "\n",
                                          meta.read);
        }
        else if (error == boost::asio::error::not_found)
        {
            std::cout << "Did not receive ending character!" << std::endl;
        }

    };
    boost::asio::async_read_until(meta.pend, meta.input_buffer, "\n",
                                  meta.read);
    auto run = [&]()
    {
        meta.io_service.run();
    };

    std::thread {run} .detach();

}

void ExternalApplicationManager::startApplication()
{
    appStoppedOnPurpose = false;

    Ice::StringSeq managedObjects = this->getArmarXManager()->getManagedObjectNames();
    bool dummyAlreadyAdded = false;
    for (std::string s : managedObjects)
    {
        if (s == depObjUUID)
        {
            dummyAlreadyAdded = true;
        }
    }

    outMetaData.reset(new StreamMetaData());
    errMetaData.reset(new StreamMetaData());

    {
        outMetaData->sink = file_descriptor_sink(outMetaData->pipe.sink, close_handle);
        errMetaData->sink = file_descriptor_sink(errMetaData->pipe.sink, close_handle);




        if (!getProperty<std::string>("ApplicationArguments").getValue().empty())
        {
            ARMARX_INFO << "Application arguments: " << getProperty<std::string>("ApplicationArguments").getValue();
        }
        ARMARX_INFO << "Commandline: " << boost::algorithm::join(args, " ");
        childProcess.reset(new child(execute(
                                         set_args(args),
                                         start_in_dir(workingDir),
                                         bind_stdout(outMetaData->sink),
                                         bind_stderr(errMetaData->sink),
                                         inherit_env(),
                                         // Guarantees that the child process gets killed, even when this process recieves SIGKILL(9) instead of SIGINT(2)
                                         on_exec_setup([](executor&)
        {
            ::prctl(PR_SET_PDEATHSIG, SIGKILL);
        })
                                     )));
    }

    outMetaData->level = eINFO;
    errMetaData->level = eWARN;
    setupStream(*outMetaData);
    setupStream(*errMetaData);


    isAppRunning = true;

    waitTask = new RunningTask<ExternalApplicationManager>(this, &ExternalApplicationManager::waitForApplication);
    waitTask->start();

    usleep(1000 * getProperty<int>("FakeObjectStartDelay").getValue());
    if (!dummyAlreadyAdded && startUpKeyword.empty())
    {
        addDependencyObject();
    }

    ARMARX_INFO << "Application started";
}


void ExternalApplicationManager::stopApplication()
{

    if (isAppRunning)
    {
        int processId = (*childProcess).pid;
        ARMARX_INFO << "Stopping application with PID " << processId << " with SIGINT";
        appStoppedOnPurpose = true;

        if (this->disconnectWhenCrash && this->getArmarXManager())
        {
            this->getArmarXManager()->removeObjectBlocking(depObjUUID);
            depObj = nullptr;
        }

        ::kill(processId, SIGINT);

        ARMARX_VERBOSE << "waiting for application " << processId;
        if (!waitForProcessToFinish(processId, getProperty<int>("KillDelay").getValue()))
        {
            ARMARX_VERBOSE << "Application wont stop - killing it now";
            boost::process::terminate(*childProcess); // Sends SIGKILL to the process
        }

        cleanUp();
        isAppRunning = false;

    }
}

void ExternalApplicationManager::cleanUp()
{
    if (waitTask)
    {
        waitTask->stop();
    }
    errMetaData->io_service.stop();
    outMetaData->io_service.stop();
    ARMARX_VERBOSE << "Application cleaned up";

}

bool ExternalApplicationManager::waitForProcessToFinish(int pid, int timeoutMS)
{
    if (pid <= 0)
    {
        return false;
    }

    double startTime = TimeUtil::GetTime().toMilliSecondsDouble();

    while ((startTime + timeoutMS > TimeUtil::GetTime().toMilliSecondsDouble()) && ::kill(pid, 0) != -1)
    {
        TimeUtil::MSSleep(100);
    }

    if (::kill(pid, 0) == -1)
    {
        return true;
    }
    else
    {
        return false;
    }
}

void ExternalApplicationManagerStarter::onInitComponent()
{
    for (auto& dep : dependencies)
    {
        usingProxy(dep);
    }

}

void ExternalApplicationManagerStarter::onConnectComponent()
{
    parent->startApplication();
}

void ExternalApplicationManagerStarter::onDisconnectComponent()
{
    parent->stopApplication();
}

void ExternalApplicationManagerStarter::onExitComponent()
{
    parent = nullptr;
}

void ExternalApplicationManagerDependency::onInitComponent()
{
    usingProxy(parent->starterUUID);
}

void ExternalApplicationManagerDependency::onConnectComponent()
{


}

void ExternalApplicationManagerDependency::onExitComponent()
{
    parent = nullptr;
}
