/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::ArmarXObjects::ExternalApplicationManager
 * @author     Stefan Reither ( stef dot reither at web dot de )
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once


#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/services/tasks/RunningTask.h>
#include <ArmarXCore/interface/components/ExternalApplicationManagerInterface.h>

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-result"
#include <boost/process.hpp>
#include <boost/process/child.hpp>
#include <boost/iostreams/stream.hpp>
#include <boost/scoped_ptr.hpp>
#include <boost/asio.hpp>
#include <boost/iostreams/device/file_descriptor.hpp>
#pragma GCC diagnostic pop

#if defined(BOOST_WINDOWS_API)
typedef boost::asio::windows::stream_handle pipe_end;
#elif defined(BOOST_POSIX_API)
typedef boost::asio::posix::stream_descriptor pipe_end;
#endif

namespace armarx
{
    class ExternalApplicationManager;
    typedef IceInternal::Handle<ExternalApplicationManager> ExternalApplicationManagerPtr;
    /**
     * @class ExternalApplicationManagerPropertyDefinitions
     * @brief
     */
    class ExternalApplicationManagerPropertyDefinitions:
        public armarx::ComponentPropertyDefinitions
    {
    public:
        ExternalApplicationManagerPropertyDefinitions(std::string prefix):
            armarx::ComponentPropertyDefinitions(prefix)
        {
            defineRequiredProperty<std::string>("ApplicationPath", "Either the name of the application if its directory is part of the PATH environment variable of the system or it is located in an ArmarX-package, or the relative or absolute path to the application.")
            .setCaseInsensitive(false);
            defineOptionalProperty<std::string>("ApplicationArguments", "", "Comma separated list of command line parameters. Commas in double-quoted strings are ignored.");
            defineOptionalProperty<std::string>("ArmarXPackage", "", "Set this property if the application is located in an ArmarX-package.");
            defineOptionalProperty<std::string>("WorkingDirectory", "", "If set, this path is used as working directory for the external app. This overrides BinaryPathAsWorkingDirectory. ${HOME} for env vars, $C{RobotAPI:BINARY_DIR} for CMakePackageFinder vars");

            defineOptionalProperty<bool>("BinaryPathAsWorkingDirectory", false, "If true the path of the binary is set as the working directory.");
            defineOptionalProperty<bool>("RestartInCaseOfCrash", false, "Whether the application should be restarted in case it crashed.");

            defineOptionalProperty<bool>("DisconnectInCaseOfCrash", true, "Whether this component should disconnect as long as the application is not running.");
            defineOptionalProperty<int>("FakeObjectStartDelay", 0, "Delay in ms after which the fake armarx object is started on which other apps can depend. Not used if property FakeObjectDelayedStartKeyword is used.");
            defineOptionalProperty<std::string>("FakeObjectDelayedStartKeyword", "", "If not empty, the start up of the fake armarx object will be delayed until this keyword is found in the stdout of the subprocess.");
            defineOptionalProperty<int>("KillDelay", 2000, "Delay ins ms before the subprocess is killed after sending the stop signal");
            defineOptionalProperty<bool>("PythonUnbuffered", true, "If true, PYTHONUNBUFFERED=1 is added to the environment variables.");
            defineOptionalProperty<bool>("RedirectToArmarXLog", true, "If true, all outputs from the subprocess are printed with ARMARX_LOG, otherwise with std::cout");
            defineOptionalProperty<Ice::StringSeq>("AdditionalEnvVars", {}, "Comma-seperated list of env-var assignment, e.g. MYVAR=1,ADDPATH=/tmp");
            defineOptionalProperty<Ice::StringSeq>("Dependencies", {}, "Comma-seperated list of Ice Object dependencies. The external app will only be started after all dependencies have been found.");


        }
    };

    struct ExternalApplicationManagerStarter : virtual public armarx::ManagedIceObject
    {
        Ice::StringSeq dependencies;
        void setDependencies(Ice::StringSeq deps)
        {
            this->dependencies = deps;
        }
        ExternalApplicationManagerPtr parent;
        void setParent(ExternalApplicationManagerPtr parent)
        {
            this->parent = parent;
        }


        /**
         * @see armarx::ManagedIceObject::getDefaultName()
         */
        std::string getDefaultName() const override
        {
            return "ExternalApplicationManagerStarter";
        }
        void onInitComponent() override;
        void onConnectComponent() override;

        void onDisconnectComponent() override;
        void onExitComponent() override;
    };

    typedef IceInternal::Handle<ExternalApplicationManagerStarter> ExternalApplicationManagerDummyPtr;

    struct ExternalApplicationManagerDependency : virtual public armarx::ManagedIceObject
    {

        ExternalApplicationManagerPtr parent;
        void setParent(ExternalApplicationManagerPtr parent)
        {
            this->parent = parent;
        }


        /**
         * @see armarx::ManagedIceObject::getDefaultName()
         */
        std::string getDefaultName() const override
        {
            return "ExternalApplicationManagerDependency";
        }
        void onInitComponent() override;
        void onConnectComponent() override;

        void onDisconnectComponent() override {}
        void onExitComponent() override;
    };
    typedef IceInternal::Handle<ExternalApplicationManagerDependency> ExternalApplicationManagerDependencyPtr;


    /**
     * @defgroup Component-ExternalApplicationManager ExternalApplicationManager
     * @ingroup ArmarXCore-Components
     * The ExternalApplicationManager can run any executale file. It can be configured to restart the given application and/or to disconnect itself if said application crashes.
     * Applications within an ArmarX-Project can be started by setting its package and its name in the config file.
     * In case of applications outside of ArmarX the whole path or its name if its directory is part of the PATH variable is required.\n
     *
     * Furthermore it provides functionalities over Ice to stop and restart the executed application.
     *
     * @class ExternalApplicationManager
     * @ingroup Component-ExternalApplicationManager
     * @brief Executes a given application and keeps track if it is still running. Provides methods for stopping and restarting said application.
     *
     *
     */
    class ExternalApplicationManager :
        virtual public armarx::Component,
        virtual public armarx::ExternalApplicationManagerInterface
    {
    public:
        /**
         * @see armarx::ManagedIceObject::getDefaultName()
         */
        std::string getDefaultName() const override
        {
            return "ExternalApplicationManager";
        }

    protected:
        /**
         * @see armarx::ManagedIceObject::onInitComponent()
         */
        void onInitComponent() override;

        /**
         * @see armarx::ManagedIceObject::onConnectComponent()
         */
        void onConnectComponent() override;

        /**
         * @see armarx::ManagedIceObject::onDisconnectComponent()
         */
        void onDisconnectComponent() override;

        /**
         * @see armarx::ManagedIceObject::onExitComponent()
         */
        void onExitComponent() override;

        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

        void addDependencyObject();

        void addStarterObject();
    private:
        // Either name of application or relative or absolute path
        std::string application;
        Ice::StringSeq args, envVars;
        bool restartWhenCrash;
        bool disconnectWhenCrash;
        std::string startUpKeyword;
        std::string workingDir;
        bool redirectToArmarXLog;

        boost::scoped_ptr<boost::process::child> childProcess;

        bool isAppRunning;
        bool appStoppedOnPurpose;
        struct StreamMetaData
        {
            StreamMetaData() :
                pipe(boost::process::create_pipe()),
                pend(pipe_end(io_service, pipe.source)) {}

            StreamMetaData(const StreamMetaData& data) :
                pipe(boost::process::create_pipe()),
                pend(pipe_end(io_service, pipe.source)) {}
            MessageType level;
            boost::asio::io_service io_service;
            boost::process::pipe pipe;
            pipe_end pend;
            boost::asio::streambuf input_buffer;
            boost::iostreams::file_descriptor_sink sink;
            std::function<void(const boost::system::error_code& error, std::size_t size)> read;
        };
        typedef boost::shared_ptr<StreamMetaData> StreamMetaDataPtr;

        void waitForApplication();
        RunningTask<ExternalApplicationManager>::pointer_type waitTask;

        boost::iostreams::stream<boost::iostreams::file_descriptor_source> out_stream;
        StreamMetaDataPtr outMetaData;

        boost::iostreams::stream<boost::iostreams::file_descriptor_source> err_stream;
        StreamMetaDataPtr errMetaData;

        void startApplication();
        void stopApplication();

        void cleanUp();

        bool waitForProcessToFinish(int pid, int timeoutMS);


        boost::iostreams::file_descriptor_source stdout_source;
        boost::iostreams::file_descriptor_source stderr_source;

        ExternalApplicationManagerDummyPtr starter;
        ExternalApplicationManagerDependencyPtr depObj;
        std::string starterUUID, depObjUUID;

        // ExternalApplicationManagerInterface interface
        void parseAdditionalArguments();

        void setupStream(StreamMetaData& meta);
        friend struct ExternalApplicationManagerStarter;
        friend struct ExternalApplicationManagerDependency;
    public:
        void restartApplication(const Ice::Current&) override;
        void terminateApplication(const Ice::Current&) override;
        std::string getPathToApplication(const Ice::Current&) override;
        bool isApplicationRunning(const Ice::Current&) override;
    };
}

