/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::ArmarXObjects::ArmarXFileLogger
 * @author     Mirko Waechter ( mirko dot waechter at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once


#include <ArmarXCore/core/Component.h>
#include <fstream>

#define ARMARX_LOG_DEFAULT_PATH "$HOME/.armarx/log/"
namespace armarx
{
    /**
     * @class ArmarXFileLoggerPropertyDefinitions
     * @brief
     */
    class ArmarXFileLoggerPropertyDefinitions:
        public armarx::ComponentPropertyDefinitions
    {
    public:
        ArmarXFileLoggerPropertyDefinitions(std::string prefix):
            armarx::ComponentPropertyDefinitions(prefix)
        {
            //defineRequiredProperty<std::string>("PropertyName", "Description");
            defineOptionalProperty<int>("MaxLogFileCount", 100, "Number of log files to keep before starting to delete old files. If zero, no file will be deleted.");
            defineOptionalProperty<std::string>("LogFileName", "ArmarXLog", "Logfilename without ending");
            defineOptionalProperty<std::string>("LogFileEnding", "log", "Logfilename  ending");
            defineOptionalProperty<std::string>("LogDir", "", std::string("If set, log will be stored in this path. If not set, log will be stored in ") + ARMARX_LOG_DEFAULT_PATH);
            defineOptionalProperty<std::string>("LogTopicName", "Log", "Name of the topic to be used");
            defineOptionalProperty<bool>("SplitByApplication", true, "Create additional log files for each application");
        }
    };

    /**
     * @defgroup Component-ArmarXFileLogger ArmarXFileLogger
     * @ingroup ArmarXCore-Components
     * A description of the component ArmarXFileLogger.
     *
     * @class ArmarXFileLogger
     * @ingroup Component-ArmarXFileLogger
     * @brief Brief description of class ArmarXFileLogger.
     *
     * Detailed description of class ArmarXFileLogger.
     */
    class ArmarXFileLogger :
        virtual public armarx::Component,
        public Log
    {
    public:
        /**
         * @see armarx::ManagedIceObject::getDefaultName()
         */
        std::string getDefaultName() const override
        {
            return "ArmarXFileLogger";
        }

        // Log interface
        void writeLog(const LogMessage&, const Ice::Current&) override;


    protected:
        /**
         * @see armarx::ManagedIceObject::onInitComponent()
         */
        void onInitComponent() override;

        /**
         * @see armarx::ManagedIceObject::onConnectComponent()
         */
        void onConnectComponent() override;

        /**
         * @see armarx::ManagedIceObject::onDisconnectComponent()
         */
        void onDisconnectComponent() override;

        /**
         * @see armarx::ManagedIceObject::onExitComponent()
         */
        void onExitComponent() override;

        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;
        std::vector<std::string> getDirListOrderedByDate(const std::string& dir) const;
        std::string getNextLogFileNameAndDeleteOldFiles(const std::string& dir, int maxLogFileCount, const std::string& fileBasename, const std::string& fileNameEnding) const;

        std::ofstream fileStream;
        std::string applicationsLogFileBasePath;
        std::map<std::string, std::shared_ptr<std::ofstream>> applicationFileStreams;
        Mutex mutex;
    };
}

