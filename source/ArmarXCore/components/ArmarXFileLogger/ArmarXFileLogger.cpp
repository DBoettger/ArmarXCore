/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::ArmarXObjects::ArmarXFileLogger
 * @author     Mirko Waechter ( mirko dot waechter at kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "ArmarXFileLogger.h"
#include <ArmarXCore/core/util/FileSystemPathBuilder.h>
#include <ArmarXCore/core/system/ArmarXDataPath.h>
#include <ArmarXCore/core/util/StringHelpers.h>
#include <boost/range/adaptor/reversed.hpp>
#include <boost/format.hpp>

using namespace armarx;



void ArmarXFileLogger::onInitComponent()
{
    auto dirString = getProperty<std::string>("LogDir").isSet() ? getProperty<std::string>("LogDir").getValue() : ARMARX_LOG_DEFAULT_PATH;
    ArmarXDataPath::ReplaceEnvVars(dirString);
    boost::filesystem::path dir(dirString);
    ARMARX_DEBUG << "Checking " << dir.string();
    if (!boost::filesystem::exists(dir))
    {
        ARMARX_INFO << "Log path not found - creating it: " << dir.string();
        boost::system::error_code errcode;
        boost::filesystem::create_directories(dir, errcode);
        if (errcode)
        {
            ARMARX_ERROR << "Creating directories failed: " << errcode.message();
            return;
        }
    }

    auto ending = getProperty<std::string>("LogFileEnding").getValue();
    auto fileBasename = getProperty<std::string>("LogFileName").getValue();
    auto filename = getNextLogFileNameAndDeleteOldFiles(dirString, getProperty<int>("MaxLogFileCount").getValue(), fileBasename, ending);


    if (getProperty<bool>("SplitByApplication").getValue())
    {
        auto filePath = boost::filesystem::path(filename);
        dir /= filePath.stem();
        if (!boost::filesystem::exists(dir))
        {
            ARMARX_INFO << "Log path not found - creating it: " << dir;
            boost::system::error_code errcode;
            boost::filesystem::create_directories(dir, errcode);
            if (errcode)
            {
                ARMARX_ERROR << "Creating directories failed: " << errcode.message();
                return;
            }
        }
        applicationsLogFileBasePath = dir.string() + "/" + filePath.stem().string() + "-%1%." + ending;
    }

    //    FileSystemPathBuilder builder(getProperty<std::string>("LogFileName").getValue());
    auto logfilePath = boost::filesystem::path(dirString)  / boost::filesystem::path(filename);
    fileStream.open(logfilePath.string().c_str(), std::ofstream::out);
    if (!fileStream.is_open())
    {
        ARMARX_ERROR << "Failed to open " << logfilePath.string();
        return;
    }
    ARMARX_INFO << "Logging to " << logfilePath.string();
    fileStream << "Log starting at " << IceUtil::Time::now().toDateTime() << "\n";
    usingTopic(getProperty<std::string>("LogTopicName").getValue());
}


void ArmarXFileLogger::onConnectComponent()
{

}


void ArmarXFileLogger::onDisconnectComponent()
{
    ScopedLock lock(mutex);
    fileStream << std::flush;
    fileStream.close();
    applicationFileStreams.clear();
}


void ArmarXFileLogger::onExitComponent()
{

}

armarx::PropertyDefinitionsPtr ArmarXFileLogger::createPropertyDefinitions()
{
    return armarx::PropertyDefinitionsPtr(new ArmarXFileLoggerPropertyDefinitions(
            getConfigIdentifier()));
}

std::vector<std::string> ArmarXFileLogger::getDirListOrderedByDate(const std::string& dir) const
{
    std::vector<std::string> result;
    namespace fs = boost::filesystem;
    fs::path someDir(dir);
    fs::directory_iterator end_iter;

    typedef std::multimap<std::time_t, fs::path> result_set_t;
    result_set_t result_set;

    if (fs::exists(someDir) && fs::is_directory(someDir))
    {
        for (fs::directory_iterator dir_iter(someDir) ; dir_iter != end_iter ; ++dir_iter)
        {
            if (fs::is_regular_file(dir_iter->status()))
            {
                result_set.insert(result_set_t::value_type(fs::last_write_time(dir_iter->path()), *dir_iter));
            }
        }
    }
    for (auto& elem : boost::adaptors::reverse(result_set))
    {
        result.push_back(elem.second.string());
    }
    return result;
}

std::string ArmarXFileLogger::getNextLogFileNameAndDeleteOldFiles(const std::string& dir, int maxLogFileCount, const std::string& fileBasename, const std::string& fileNameEnding) const
{
    auto files = getDirListOrderedByDate(dir);
    //    ARMARX_INFO << files;
    std::string connectorString = "-";
    const boost::regex e(fileBasename + connectorString + "([0-9]+)\\.");
    int trials = 0;
    int highestNumber = 0;
    for (auto file : files)
    {
        boost::match_results<std::string::const_iterator> what;
        file = boost::filesystem::path(file).filename().string();
        ARMARX_DEBUG << file;
        bool found_match = boost::regex_search(file, what, e);
        if (found_match)
        {
            for (size_t i = 1; i < what.size(); i += 2)
            {
                std::string numberStr = what[i];
                int number = atoi(numberStr.c_str());
                highestNumber = std::max(number, highestNumber);
                //                ARMARX_INFO << "testing number " << number << VAROUT( trials) << VAROUT(highestNumber);
                auto filenameCandidate = fileBasename + connectorString + ValueToString(number) + "." + fileNameEnding;
                if (boost::filesystem::exists(dir + "/" + filenameCandidate))
                {
                    //                    ARMARX_INFO << "File exists " << VAROUT(filenameCandidate) << VAROUT(trials);
                    if (trials >= maxLogFileCount && maxLogFileCount > 0)
                    {
                        ARMARX_VERBOSE << "Deleting old log " << filenameCandidate;
                        boost::filesystem::remove(dir + "/" + filenameCandidate);
                    }
                }
                trials++;
            }
        }
    }
    highestNumber++;
    return fileBasename + connectorString + ValueToString(highestNumber) + "." + fileNameEnding;

}



void armarx::ArmarXFileLogger::writeLog(const LogMessage& msg, const Ice::Current&)
{
    std::string catStr;

    if (!msg.tag.empty())
    {
        catStr = "[" + msg.tag + "]: ";
    }
    else
    {
        catStr = "[" + LogSender::CropFunctionName(msg.function) + "]: ";
    }
    IceUtil::Time time = IceUtil::Time::microSeconds(msg.time);
    ScopedLock lock(mutex);
    if (!fileStream.is_open())
    {
        return;
    }
    std::stringstream newLines;
    newLines << "[" + time.toDateTime().substr(time.toDateTime().find(' ') + 1) + "]" + "[" + msg.who + "]" + catStr;
    newLines << msg.what << "\n";
    fileStream << newLines.str();
    if (getProperty<bool>("SplitByApplication").getValue())
    {
        auto it = applicationFileStreams.find(msg.who);
        if (it == applicationFileStreams.end())
        {
            auto filepath = boost::format(applicationsLogFileBasePath) % msg.who;
            it = applicationFileStreams.emplace(msg.who, std::shared_ptr<std::ofstream>(new std::ofstream(filepath.str().c_str()))).first;
            //            it = applicationFileStreams.find(msg.who);
            if (!it->second->is_open())
            {
                ARMARX_WARNING << "Failed to open for logging: " << filepath.str();
                return;
            }
        }

        *(it->second) << newLines.str();
    }
}
