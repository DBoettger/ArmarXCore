/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Core
* @author     Kai Welke (welke _at_ kit _dot_ edu)
* @date       2012 Kai Welke
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include <ArmarXCore/observers/condition/Operations.h>

using namespace armarx;

OperationType Operation::getOperationType(const Ice::Current& c) const
{
    return opType;
}

Operation::Operation()
{
    type = eOperation;
    opType = eUndefinedOperation;
}

// OperationAnd
OperationAnd::OperationAnd(TermImplPtr a, TermImplPtr b)
{
    opType = eOperationAnd;
    addChild(a);
    addChild(b);
}

void OperationAnd::update(const Ice::Current& c)
{
    if (childs.size() != 2)
    {
        return;
    }

    value = childs.at(0)->getValue() && childs.at(1)->getValue();

    TermImpl::update();
}

void OperationAnd::updateWithData(const Ice::Current&)
{
    if (childs.size() != 2)
    {
        return;
    }
    datafieldValues = childs.at(0)->getDatafields();
    auto values2 = childs.at(1)->getDatafields();
    datafieldValues.insert(values2.begin(), values2.end());

    value = childs.at(0)->getValue() && childs.at(1)->getValue();

    TermImpl::updateWithData();
}

void OperationAnd::output(std::ostream& out) const
{
    if (childs.size() != 2)
    {
        return;
    }

    out << "(" << TermImplPtr::dynamicCast(childs.at(0)) << " && " << TermImplPtr::dynamicCast(childs.at(1)) << ")";
}

std::string OperationAnd::getOperationString(const Ice::Current& c)
{
    return "&";
}

Ice::ObjectPtr OperationAnd::ice_clone() const
{
    return new OperationAnd(TermImplPtr::dynamicCast(childs.at(0)->ice_clone()), TermImplPtr::dynamicCast(childs.at(1)->ice_clone()));
}

OperationAnd::OperationAnd()
{
    opType = eOperationAnd;
}

// OperationOr
OperationOr::OperationOr(TermImplPtr a, TermImplPtr b)
{
    opType = eOperationOr;

    addChild(a);
    addChild(b);
}

void OperationOr::update(const Ice::Current& c)
{
    if (childs.size() != 2)
    {
        return;
    }

    value = childs.at(0)->getValue() || childs.at(1)->getValue();

    TermImpl::update();
}

void OperationOr::updateWithData(const Ice::Current&)
{
    if (childs.size() != 2)
    {
        return;
    }

    value = childs.at(0)->getValue() || childs.at(1)->getValue();
    datafieldValues = childs.at(0)->getDatafields();
    auto values2 = childs.at(1)->getDatafields();
    datafieldValues.insert(values2.begin(), values2.end());
    TermImpl::updateWithData();
}

void OperationOr::output(std::ostream& out) const
{
    if (childs.size() != 2)
    {
        return;
    }

    out << "(" << TermImplPtr::dynamicCast(childs.at(0)) << " || " << TermImplPtr::dynamicCast(childs.at(1)) << ")";
}

std::string OperationOr::getOperationString(const Ice::Current& c)
{
    return "|";
}

Ice::ObjectPtr OperationOr::ice_clone() const
{
    return new OperationOr(TermImplPtr::dynamicCast(childs.at(0)->ice_clone()), TermImplPtr::dynamicCast(childs.at(1)->ice_clone()));
}

OperationOr::OperationOr()
{
    opType = eOperationOr;
}

// OperationNot
OperationNot::OperationNot(TermImplPtr a)
{
    opType = eOperationNot;

    addChild(a);
}

void OperationNot::update(const Ice::Current& c)
{
    if (childs.size() != 1)
    {
        return;
    }

    value = !childs.at(0)->getValue();

    TermImpl::update();
}

void OperationNot::updateWithData(const Ice::Current&)
{
    if (childs.size() != 1)
    {
        return;
    }

    value = !childs.at(0)->getValue();
    datafieldValues = childs.at(0)->getDatafields();
    TermImpl::updateWithData();
}

void OperationNot::output(std::ostream& out) const
{
    if (childs.size() != 1)
    {
        return;
    }

    out << "!(" << TermImplPtr::dynamicCast(childs.at(0)) << ")";
}

std::string OperationNot::getOperationString(const Ice::Current& c)
{
    return "!";
}

Ice::ObjectPtr OperationNot::ice_clone() const
{
    return new OperationNot(TermImplPtr::dynamicCast(childs.at(0)->ice_clone()));
}

OperationNot::OperationNot()
{
    opType = eOperationNot;
}
