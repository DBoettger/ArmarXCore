/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Core
* @author     Kai Welke (welke _at_ kit _dot_ edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <string>
#include <boost/thread/mutex.hpp>
#include <boost/shared_ptr.hpp>
#include <IceUtil/Handle.h>
#include <ArmarXCore/core/system/ImportExport.h>

#include <ArmarXCore/observers/condition/TermImpl.h>
#include <ArmarXCore/interface/observers/VariantBase.h>
#include <ArmarXCore/interface/observers/Event.h>
#include <ArmarXCore/observers/variant/DatafieldRef.h>


namespace armarx
{
    class ConditionRoot;
    class LiteralImpl;

    /**
     * Typedef of ConditionRootPtr as IceInternal::Handle<ConditionRoot> for convenience.
     */
    typedef IceInternal::Handle<ConditionRoot> ConditionRootPtr;
    typedef IceInternal::Handle<LiteralImpl> LiteralImplPtr;

    /**
    * @class ConditionRoot
    * @brief ConditionRoot Condition roots are top-level nodes of the expression tree.
    * @ingroup Conditions
    *
    * Condition roots are the root node of expression tree. They have an event as member, which is
    * fires once the expression described by the tree is fulfilled.
    */
    class ARMARXCORE_IMPORT_EXPORT ConditionRoot :
        virtual public TermImpl,
        virtual public ConditionRootBase
    {
        friend class ConditionHandler;
        template <class BaseClass, class VariantClass>
        friend class GenericFactory;

    public:
        /**
        * Update state f the ConditionRoot object based on the child.
        * Fire ConditionRoot::event if value is true.
        * The event is only fired, if no previous event has been fired.
        */
        void update(const Ice::Current& c = ::Ice::Current()) override;
        void updateWithData(const Ice::Current& c = ::Ice::Current()) override;

        /**
        * output to stream. pure virtual.
        *
        * @param stream
        */
        void output(std::ostream& out) const override;

    protected:
        /**
        * Creates an empty ConditionRoot. Used by Ice factory.
        */
        ConditionRoot();

        /**
        * Creates and initializes a ConditionRoot. Used by ConditionHandler.
        */
        ConditionRoot(const EventListenerInterfacePrx& listener, const EventBasePtr& event, const std::string& description, bool onlyFireOnce, const DatafieldRefList& refs);

    public:
        /**
         * stream operator for Condition
         */
        friend std::ostream& operator<<(std::ostream& stream, const ConditionRoot& rhs)
        {
            rhs.output(stream);

            return stream;
        }

        /**
         * stream operator for ConditionPtr
         */
        friend std::ostream& operator<<(std::ostream& stream, const ConditionRootPtr& rhs)
        {
            rhs->output(stream);

            return stream;
        }

        static std::vector<LiteralImplPtr> ExtractLiterals(const TermImplBasePtr& expression);


    private:
        boost::mutex updateMutex;
        DatafieldRefList refs;
    };
}

