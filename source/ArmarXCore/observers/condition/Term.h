/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Core
* @author     Kai Welke (welke _at_ kit _dot_ edu)
* @date       2012 Kai Welke
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

// variant includes
#include <ArmarXCore/observers/variant/DataFieldIdentifier.h>
#include <ArmarXCore/observers/variant/Variant.h>

// condition includes
#include <ArmarXCore/observers/condition/TermImpl.h>
#include <ArmarXCore/observers/condition/LiteralImpl.h>

namespace armarx
{
    /**
        \defgroup Conditions Conditions
        \ingroup ObserversGrp
        The ArmarX condition mechanism allows to define a distributed set of condition checks on sensory data and
        generate events based on the condition checks. These events are used to trigger state transitions in the
        robot program.

        The condition mechanism comprises two main elements:
          - the definition of condition checks on sensory data available in an ArmarX scenario
          - the definition of boolean expression on top of the condition checks that allow the generation of events in an intuitive manner.

        \section ConditionChecks Condition checks
        Condition checks are provided by each observer component in the ArmarX scenario. Condition
        checks can be instantiated on each datafield provided by the
        observer. Thereby, datafields are identified by their armarx::DataFieldIdentifier and condition checks
        are identifier by their name. Further a list of parameters is passed to condition checks.

        Typical generic condition checks include "equals", "larger", "inrange", "contains" which are already
        implemented for all applicable datatypes. Further, the condition checks are easily extensible if special checks
        are needed. See armarx::ConditionCheck for details.

        \section Expression Definition of conditions
        Conditions are defined using logical expressions. Each literal in the logical expression represents the state of
        a distributed condition check. Using logical operations, complex expressions can
        be achieved. In general, the definition of a condition is very similar to the if-statement:
        \include Conditions.dox

        In order to allow a more convenient way for assembling conditions in a loop, an empty term can be defined
        which is successively defined by adding more literals. Logical operations on empty are not applied, they
        are replaced by an assignment:
        \include EmptyTerm.dox

        \section Implementation Implementation of conditions
        The implementation of conditions consists of two parts: the user frontend and the framework implementation. A basic
        overview is shown in this figure, where the user frontend is colored red and the framework implementation blue.
        \image html condition.png "" witdh=400px
        \subsection ImplementationFrontend User frontend
        The user frontend offers a convenient way to define a condition by means of assembling a Term using binary operators &&, || and NOT. The condition is
        an expression composed of literals as introduced in \ref Expression.

        Internally, each term builds a TermImpl which is used within the ArmarX framework, to hande the condition checks and to evaluate the term.
        This implementation is described in the following.

        \subsection ImplementationFramework Framework implementation
        The condition is internally represented as binary expression tree. Each node in the tree corresponds to a logical operation such as &&, || and NOT.
        The leaves of the expression tree correspond to literals, which are associated with condition checks (see \ref ConditionChecks).

        Each node of the tree inherits from the TermImpl superclass, which stores the linkage in the tree and a boolean value which encodes the current
        state of the node. Further, an update method is provided, which takes the value from the children of the node and performes the operation specific
        to the node in order to update its state. Once the value has been updated, the update of the parent node is called.

        Concrete implementations of TermImpl are provided by subclassing the TermImpl. Three different subclasses are implemented: the LiteralImpl, the Operation, and
        the ConditionRoot. The LiteralImpl correspond to condition checks and are updated by evaluation atomar checks on datafields. The value of a LiteralImpl
        corresponds to the result of the condition check. On top of the LiteralImpl, a set of operations can be defined using the Operation subclass. The available
        operations correspond to the logical operations &&, || and NOT provided by the user frontend. Finally, the top node of the tree is realized by the
        class ConditionRoot, which fires an event, once the expression represented by the tree is fulfilled.

        The following image illustrates a complete expression tree as build by the ArmarX framework:
        \image html ExpressionTree.png "" witdh=300px

        The expression tree corresponds to the example code given in section \ref Expression. The trees can be visualized including their state and associated checks
        with the ConditionViewerWidget from the ArmarX Gui.

    * @class Term
    * @ingroup Conditions
    *
    * Terms are part of the user front end of the ArmarX condition mechanism.
    * Terms describe boolean expressions of condition checks. The logical operations AND, OR and
    * NOT are implemented for terms. Terms are composed of several Literals
    * which correspond to condition checks.
    *
    * \see Conditions
    * \see Literal
    */
    class ARMARXCORE_IMPORT_EXPORT Term
    {
    public:
        /**
         * Construct an empty term. For empty terms, the first operations is interpreted as
         * assignment.
         * An empty term is always evaluated to 'true'.
         */
        Term();

        /**
         * Copy constructor.
         *
         * @param other to copy
         */
        Term(const Term& other);

        /**
         * Construct term by term implementation
         *
         * @param implementation (will not be copied)
         */
        Term(TermImplPtr impl);

        /**
         * The logical AND operator
         *
         * @param another term or literal
         * @return the resulting expression
         */
        Term operator&&(const Term& right) const;

        /**
         * The logical OR operator
         *
         * @param another term or literal
         * @return the resulting expression
         */
        Term operator||(const Term& right) const;

        /**
         * The logical NOT operator
         *
         * @return the resulting expression
         */
        Term operator!(void) const;

        /**
         * The assignment operator
         *
         * @param another term or literal
         * @return the term
         */
        Term& operator=(const Term& right);

        /**
         * Retrieve term implementation object as used in the ArmarX Framework in order to build distributed expression trees.
         *
         * @return the term implementation
         */
        TermImplPtr getImpl() const;


        /**
         * Streaming operator. Calls the streaming operator of the implementation
         *
         * @param stream stream
         * @param rhs right hand side
         * @return stream
         */
        friend std::ostream& operator<<(std::ostream& stream, const Term& rhs)
        {
            // call streaming operator on implementation
            if (rhs.termImpl)
            {
                stream << rhs.termImpl;
            }

            return stream;
        }

    protected:
        // reference to term implementation
        TermImplPtr termImpl;
    };


    typedef std::vector<Variant> VarList;
    /**
    * @class Literal
    * @ingroup Conditions
    *
    * Literals are part of the user front end of the ArmarX condition mechanism.
    * Literals describe condition checks in the distributed ArmarX scenario.
    *
    * \see Conditions
    */
    class ARMARXCORE_IMPORT_EXPORT Literal :
        public Term
    {
    public:
        /**
         * Construct a literal using a datafieldidentifier as string
         *
         * @param dataFieldIdentifierStr dataFieldIdentifier as string
         * @param checkName name of the check as available in the observer. The usual checks can be found in the namespace ::armarx::checks. For the available checks for the specific observer, check the Gui ObserverView while the Observer is running
         * @param checkParameters parameter list for the check
         */
        Literal(const std::string& dataFieldIdentifierStr, const std::string& checkName, const VarList& checkParameters = createParameterList());


        /**
         * Construct a literal using a datafieldidentifier object
         *
         * @param dataFieldIdentifier dataFieldIdentifier object
         * @param checkName name of the check as available in the observer. The usual checks can be found in the namespace ::armarx::checks. For the available checks for the specific observer, check the Gui ObserverView while the Observer is running
         * @param checkParameters parameter list for the check
         */
        Literal(const DataFieldIdentifier& dataFieldIdentifier, const std::string& checkName, const VarList& checkParameters = createParameterList());

        /**
         * Construct a literal using a datafieldidentifier pointer
         *
         * @param dataFieldIdentifier dataFieldIdentifier pointer
         * @param checkName name of the check as available in the observer. The usual checks can be found in the namespace ::armarx::checks. For the available checks for the specific observer, check the Gui ObserverView while the Observer is running
         * @param checkParameters parameter list for the check
         */
        Literal(const DataFieldIdentifierPtr& dataFieldIdentifier, const std::string& checkName, const VarList& checkParameters = createParameterList());

        Literal(const DatafieldRefBasePtr& datafieldRef, const std::string& checkName, const VarList& checkParameters = createParameterList());

        /**
         * Static helper method to create an empty parameterlist
         *
         * @return parameterlist as required for the construction of a literal
         */
        static VarList createParameterList();

        /**
         * Static helper method to create a parameterlist
         *
         * @param param1 first parameter
         * @return parameterlist as required for the construction of a literal
         */
        static VarList createParameterList(const Variant& param1);

        /**
         * Static helper method to create a parameterlist
         *
         * @param param1 first parameter
         * @param param2 second parameter
         * @return parameterlist as required for the construction of a literal
         */
        static VarList createParameterList(const Variant& param1, const Variant& param2);

        /**
         * Static helper method to create a parameterlist
         *
         * @param param1 first parameter
         * @param param2 second parameter
         * @param param3 third parameter
         * @return parameterlist as required for the construction of a literal
         */
        static VarList createParameterList(const Variant& param1, const Variant& param2, const Variant& param3);

        ParameterList toParamList(const VarList& varList) const;
    };
}

