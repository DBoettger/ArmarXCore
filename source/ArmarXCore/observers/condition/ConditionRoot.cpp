/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Core
* @author     Kai Welke (welke _at_ kit _dot_ edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include <ArmarXCore/observers/condition/ConditionRoot.h>
#include "LiteralImpl.h"
#include <cstdarg>
#include <ArmarXCore/observers/variant/VariantContainer.h>

using namespace armarx;

void ConditionRoot::update(const Ice::Current& c)
{
    boost::mutex::scoped_lock lock(updateMutex);

    if (childs.size() != 1)
    {
        return;
    }

    // empty terms is always evaluated to true
    if ((!fired || !onlyFireOnce) && (!childs.at(0) || childs.at(0)->getValue()))
    {
        value = true;
        ARMARX_DEBUG_S << "Condition fulfilled, sending event " << event->eventName << std::endl;
        try
        {
            event->properties.clear();
            for (const DatafieldRefBasePtr& ref : refs)
            {
                DatafieldRefPtr r = DatafieldRefPtr::dynamicCast(ref);
                event->properties[r->getDataFieldIdentifier()->getIdentifierStr()] = new SingleVariant(*r->getDataField());
            }
            //needs to be async otherwise it can lead to deadlock if the listener uses a datafield of an observer
            listener->begin_reportEvent(event);
        }
        catch (std::exception& e)
        {
            ARMARX_ERROR_S << "Could not report event: " << e.what();
        }

        fired = true;
    }
}

void ConditionRoot::updateWithData(const Ice::Current& c)
{
    boost::mutex::scoped_lock lock(updateMutex);

    if (childs.size() != 1)
    {
        return;
    }

    // empty terms is always evaluated to true
    if ((!fired || !onlyFireOnce) && (!childs.at(0) || childs.at(0)->getValue()))
    {
        value = true;
        ARMARX_VERBOSE_S << "Condition fulfilled, sending event with data " << event->eventName << std::endl;
        try
        {
            StringVariantBaseMap values = childs.at(0)->getDatafields();
            event->properties.clear();
            ARMARX_INFO_S << VAROUT(event->eventName);
            for (auto& v : values)
            {
                event->properties[v.first] = new SingleVariant(*VariantPtr::dynamicCast(v.second));
                ARMARX_INFO_S << v.first << " cond val: " << VariantPtr::dynamicCast(v.second)->getOutputValueOnly();
            }
            for (const DatafieldRefBasePtr& ref : refs)
            {
                DatafieldRefPtr r = DatafieldRefPtr::dynamicCast(ref);
                auto id = r->getDataFieldIdentifier()->getIdentifierStr();
                if (event->properties.count(id) > 0)
                {
                    continue;
                }
                event->properties[id] = new SingleVariant(*r->getDataField());
                ARMARX_INFO_S << r->getDataFieldIdentifier()->getIdentifierStr() << " val: " << r->getDataField()->getOutputValueOnly();
            }

            //needs to be async otherwise it can lead to deadlock if the listener uses a datafield of an observer
            listener->begin_reportEvent(event);
        }
        catch (std::exception& e)
        {
            ARMARX_ERROR_S << "Could not report event: " << e.what();
        }

        fired = true;
    }
}

void ConditionRoot::output(std::ostream& out) const
{
    out << "Event: " << event->eventName << " - ";

    if (childs.size() > 0)
    {
        out << "Expression: " << TermImplPtr::dynamicCast(childs.at(0));
    }
}

ConditionRoot::ConditionRoot()
{
    type = eConditionRoot;
}

ConditionRoot::ConditionRoot(const EventListenerInterfacePrx& listener, const EventBasePtr& event, const std::string& description, bool onlyFireOnce, const DatafieldRefList& refs)
{
    type = eConditionRoot;
    this->listener = listener->ice_collocationOptimized(false); // events are reported with begin_reportEvent -> this does not work if the event receiver is in the same process and collocation is used
    this->event = event;
    this->description = description;
    this->onlyFireOnce = onlyFireOnce;
    this->refs = refs;
}

std::vector<LiteralImplPtr> ConditionRoot::ExtractLiterals(const TermImplBasePtr& expression)
{
    std::list<TermImplPtr> terms;
    std::vector<LiteralImplPtr> literals;

    terms.push_back(TermImplPtr::dynamicCast(expression));

    while (terms.size() != 0)
    {
        // retrieve front of list
        TermImplPtr term = terms.front();
        terms.pop_front();

        // test type
        if (term->getType() == eLiteral)
        {
            literals.push_back(LiteralImplPtr::dynamicCast(term));
        }
        else
        {
            // add childs to list
            TermImplSequence childs = term->getChilds();
            TermImplSequence::iterator iterChilds = childs.begin();

            while (iterChilds != childs.end())
            {
                terms.push_back(TermImplPtr::dynamicCast(*iterChilds));
                iterChilds++;
            }
        }

    }



    return literals;
}
