/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Core
* @author     Kai Welke (welke _at_ kit _dot_ edu)
* @date       2011
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <ArmarXCore/core/system/ImportExport.h>
#include <ArmarXCore/observers/ConditionCheck.h>

/**
 * \defgroup ConditionChecks Condition Checks
 * \ingroup Conditions
 *
 * ArmarX provides a set of integrated condition checks.
 */
namespace armarx
{
    /**
     * \ingroup ConditionChecks
     *
     * Checks if the numbers published in the relevant data fields equal a reference value.
     *
     * Parameters: The reference value
     * Supported types: Int, Float, Double
     */
    class ARMARXCORE_IMPORT_EXPORT ConditionCheckEquals :
        public ConditionCheck
    {
    public:
        ConditionCheckEquals()
        {
            setNumberParameters(1);

            addSupportedType(VariantType::Bool, createParameterTypeList(1, VariantType::Bool));
            addSupportedType(VariantType::Int, createParameterTypeList(1, VariantType::Int));
            addSupportedType(VariantType::String, createParameterTypeList(1, VariantType::String));
            addSupportedType(VariantType::Float, createParameterTypeList(1, VariantType::Float));
            addSupportedType(VariantType::Double, createParameterTypeList(1, VariantType::Double));
        }


        ConditionCheck* clone() override
        {
            return new ConditionCheckEquals(*this);
        }

        bool evaluate(const StringVariantMap& dataFields) override
        {
            if (dataFields.size() != 1)
            {
                printf("Size of dataFields: %d\n", (int)dataFields.size());
                throw InvalidConditionException("Wrong number of datafields for condition equals ");
            }

            const Variant& value = dataFields.begin()->second;
            VariantTypeId type = value.getType();

            if (type == VariantType::Bool)
            {
                return value.getBool() == getParameter(0).getBool();
            }

            if (type == VariantType::Int)
            {
                return value.getInt() == getParameter(0).getInt();
            }

            if (type == VariantType::Float)
            {
                return value.getFloat() == getParameter(0).getFloat();
            }

            if (type == VariantType::Double)
            {
                return value.getDouble() == getParameter(0).getDouble();
            }

            if (type == VariantType::String)
            {
                return value.getString().compare(getParameter(0).getString()) == 0;
            }

            return false;
        }
    };
}

