/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Core
* @author     Kai Welke (welke _at_ kit _dot_ edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <ArmarXCore/core/system/ImportExport.h>
#include <ArmarXCore/observers/ConditionCheck.h>

namespace armarx
{
    /**
     * \ingroup ConditionChecks
     *
     * Checks if the relevant data fields have been updated since the installation
     * of this condition.
     *
     * Parameters: None
     * Supported types: String, Int, Bool, Float, Double
     */
    class ARMARXCORE_IMPORT_EXPORT ConditionCheckChanged :
        public ConditionCheck
    {
    public:
        ConditionCheckChanged()
        {
            setNumberParameters(0);
            addSupportedType(VariantType::Bool, createParameterTypeList(0));
            addSupportedType(VariantType::Int, createParameterTypeList(0));
            addSupportedType(VariantType::String, createParameterTypeList(0));
            addSupportedType(VariantType::Float, createParameterTypeList(0));
            addSupportedType(VariantType::Double, createParameterTypeList(0));
        }

        ConditionCheck* clone() override
        {
            return new ConditionCheckChanged(*this);
        }

        bool evaluate(const StringVariantMap& dataFields) override
        {
            if (dataFields.size() != 1)
            {
                ARMARX_INFO_S << "Size of dataFields: " << dataFields.size();
                throw InvalidConditionException("Wrong number of datafields for condition changed ");
            }
            const Variant& value = dataFields.begin()->second;

            // on initial check this condition should not return true
            if (!lastValue)
            {
                lastValue = value.clone();
                return false;
            }
            VariantTypeId type = value.getType();

            bool result = false;
            if (!lastValue->getInitialized() && !value.getInitialized())
            {
                return false;
            }

            if (lastValue->getInitialized() != value.getInitialized())
            {
                result = true;
            }

            else if (type == VariantType::String)
            {
                result = value.getString() != lastValue->getString();
            }

            else if (type == VariantType::Bool)
            {
                result = value.getBool() != lastValue->getBool();
            }

            else if (type == VariantType::Int)
            {
                result = value.getInt() != lastValue->getInt();
            }

            else if (type == VariantType::Float)
            {
                result = value.getFloat() != lastValue->getFloat();
            }

            else if (type == VariantType::Double)
            {
                result = value.getDouble() != lastValue->getDouble();
            }
            lastValue = value.clone();

            return result;
        }
    private:
        VariantPtr lastValue;
    };
}

