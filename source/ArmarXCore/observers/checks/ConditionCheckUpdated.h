/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Core
* @author     Kai Welke (welke _at_ kit _dot_ edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <ArmarXCore/core/system/ImportExport.h>
#include <ArmarXCore/observers/ConditionCheck.h>

namespace armarx
{
    /**
     * \ingroup ConditionChecks
     *
     * Checks if the relevant data fields have been updated since the installation
     * of this condition.
     *
     * Parameters: None
     * Supported types: Any
     */
    class ARMARXCORE_IMPORT_EXPORT ConditionCheckUpdated :
        public ConditionCheck
    {
    public:
        ConditionCheckUpdated() :
            firstCheck(true)
        {
            setNumberParameters(0);
            addSupportedType();
        }

        ConditionCheck* clone() override
        {
            return new ConditionCheckUpdated(*this);
        }

        bool evaluate(const StringVariantMap& dataFields) override
        {
            if (dataFields.size() != 1)
            {
                ARMARX_INFO_S << "Size of dataFields: " << dataFields.size();
                throw InvalidConditionException("Wrong number of datafields for condition updated ");
            }

            // on initial check this condition should not return true
            if (firstCheck)
            {
                firstCheck = false;
                return false;
            }

            return true;
        }
    private:
        bool firstCheck;
    };
}

