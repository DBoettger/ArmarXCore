/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2014
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <ArmarXCore/core/system/ImportExport.h>
#include <ArmarXCore/observers/ConditionCheck.h>

namespace armarx
{
    /**
     * \ingroup ConditionChecks
     *
     * Checks if the relevant data fields contain valid values.
     *
     * Parameters: None
     * Supported types: Any
     */
    class ARMARXCORE_IMPORT_EXPORT ConditionCheckValid :
        public ConditionCheck
    {
    public:
        ConditionCheckValid()
        {
            setNumberParameters(0);
            addSupportedType();
        }

        ConditionCheck* clone() override
        {
            return new ConditionCheckValid(*this);
        }

        bool evaluate(const StringVariantMap& dataFields) override
        {
            if (dataFields.size() != 1)
            {
                printf("Size of dataFields: %d\n", (int) dataFields.size());
                throw InvalidConditionException("Wrong number of datafields for condition equals ");
            }

            const Variant& value = dataFields.begin()->second;
            return value.getInitialized() && value.validate();
        }
    };
}


