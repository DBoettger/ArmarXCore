/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarXCore::core::Test
* @author     Kai Welke
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#define BOOST_TEST_MODULE ArmarX::Core::VariantTest
#define ARMARX_BOOST_TEST
#include <ArmarXCore/Test.h>

#include <ArmarXCore/interface/observers/VariantBase.h>
#include <ArmarXCore/interface/observers/ParameterBase.h>
#include <ArmarXCore/interface/observers/Complex.h>
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>
#include <ArmarXCore/core/system/FactoryCollectionBase.h>

#include <ArmarXCore/observers/variant/Variant.h>
#include <ArmarXCore/observers/variant/SingleTypeVariantList.h>
#include <ArmarXCore/observers/variant/StringValueMap.h>
#include <ArmarXCore/observers/variant/ChannelRef.h>
#include <ArmarXCore/observers/variant/TimestampVariant.h>
#include <ArmarXCore/observers/AbstractObjectSerializer.h>
#include <ArmarXCore/util/json/JSONObject.h>
#include <ArmarXCore/core/ArmarXManager.h>
#include <ArmarXCore/observers/exceptions/user/InvalidTypeException.h>

#include <Eigen/Core>

// Ice Includes
#include <IceUtil/UUID.h>
#include <Ice/Initialize.h>


using namespace armarx;

BOOST_AUTO_TEST_CASE(VariantSimpleSetterGetter)
{
    //! [VariantDocumentation Initialization1]
    Variant varInt;
    varInt.setInt(2);
    //! [VariantDocumentation Initialization1]
    printf("setInt\n");

    Variant varBool;
    varBool.setBool(true);
    printf("setBool\n");

    Variant varFloat;
    varFloat.setFloat(1.25);
    printf("setFloat\n");

    Variant varString;
    varString.setString("susigames.com");
    printf("setString\n");

    BOOST_CHECK(varInt.getInt() == 2);
    BOOST_CHECK(varBool.getBool() == true);
    BOOST_CHECK(varFloat.getFloat() == 1.25);
    BOOST_CHECK(varString.getString() == "susigames.com");
}

/*BOOST_AUTO_TEST_CASE(VariantClassTypeSetterGetter)
{
    ChannelRefPtr channelRef = new ChannelRef();
    channelRef->observerName = "edgebomber.com";

    Variant varChannelRef;
    varChannelRef.setClass(channelRef);

    ChannelRefPtr channelRefGot = varChannelRef.getClass<ChannelRef>();

    BOOST_CHECK(channelRefGot->observerName == "edgebomber.com");
}
*/

struct ChannelRefTest : ChannelRef
{

};

BOOST_AUTO_TEST_CASE(VariantConstructorGetter)
{
    //! [VariantDocumentation Initialization2]
    Variant varInt(2);
    //! [VariantDocumentation Initialization2]

    Variant varBool(true);

    Variant varFloat(1.25f);
    Variant varStdString(std::string("susigames.com"));

    Variant varString("susigames.com");


    Variant varChannelRef(*ChannelRefPtr(new ChannelRefTest()));
    Variant varChannelRefPtr(new ChannelRefTest());
    Variant varChannelRefSharedPtr(ChannelRefPtr(new ChannelRefTest()));

    BOOST_CHECK(varInt.getType() == VariantType::Int);
    BOOST_CHECK(varBool.getType() == VariantType::Bool);
    BOOST_CHECK(varFloat.getType() == VariantType::Float);
    BOOST_CHECK(varString.getType() == VariantType::String);

    //! [VariantDocumentation Retrieval]
    int value = varInt.getInt();
    //! [VariantDocumentation Retrieval]

    BOOST_CHECK(value == 2);
    BOOST_CHECK(varBool.getBool() == true);
    BOOST_CHECK(varFloat.getFloat() == 1.25);
    BOOST_CHECK(varString.getString() == "susigames.com");
    BOOST_CHECK(varChannelRef.getType() == VariantType::ChannelRef);
}

BOOST_AUTO_TEST_CASE(VariantTemplateSetterGetter)
{
    // bool template setter
    Variant varBool;
    varBool.set<bool>(true);

    // int template setter
    //! [VariantDocumentation Initialization3]
    Variant varInt;
    varInt.set<int>(2);
    //! [VariantDocumentation Initialization3]

    // float template setter
    Variant varFloat;
    varFloat.set<float>(1.25);

    // string template setter
    Variant varString;
    varString.set<std::string>("susigames.com");

    // channelRef template setter
    /*  ChannelRefPtr channelRef = new ChannelRef();
        channelRef->observerName = "edgebomber.com";
        Variant varChannelRef;
        varChannelRef.set<ChannelRef>(channelRef);
    */

    // getters
    BOOST_CHECK(varInt.get<int>() == 2);
    BOOST_CHECK(varBool.get<bool>() == true);
    BOOST_CHECK(varFloat.get<float>() == 1.25);
    BOOST_CHECK(varString.get<std::string>() == "susigames.com");
    //BOOST_CHECK(varChannelRef.get<ChannelRef>()->observerName == "edgebomber.com");
}

//! [TestComplexFloat Variant Type]
namespace armarx
{
    namespace VariantType
    {
        const VariantTypeId TestComplexFloat = Variant::addTypeName("::armarx::ComplexFloatBase");
    }
}
//! [TestComplexFloat Variant Type]

/*
 * Demo implementation of a custom Variant type for complex numbers.
 */
//! [TestComplexFloat Class Definition]
class TestComplexFloat : virtual public ComplexFloatBase
{
    template <class BaseClass, class VariantClass>
    friend class GenericFactory;

public:
    TestComplexFloat()
    {
        real = 0;
        imag = 0;
    }

    TestComplexFloat(float real, float imag)
    {
        this->real = real;
        this->imag = imag;
    }

    float getReal()
    {
        return real;
    }

    float getImag()
    {
        return imag;
    }

    Ice::ObjectPtr ice_clone() const override
    {
        return this->clone();
    }

    VariantDataClassPtr clone(const Ice::Current& c = ::Ice::Current()) const override
    {
        return new TestComplexFloat(*this);
    }

    std::string output(const Ice::Current& c = ::Ice::Current()) const override
    {
        std::stringstream s;
        s << "(" << real << " + " << imag << "i)";
        return s.str();
    }

    VariantTypeId getType(const Ice::Current& c = ::Ice::Current()) const override
    {
        return armarx::VariantType::TestComplexFloat;
    }

    bool validate(const Ice::Current& c = ::Ice::Current()) override
    {
        return true;
    }


public:
    void serialize(const armarx::ObjectSerializerBasePtr& serializer, const ::Ice::Current& = ::Ice::Current()) const override
    {
        AbstractObjectSerializerPtr obj = AbstractObjectSerializerPtr::dynamicCast(serializer);

        obj->setFloat("real", real);
        obj->setFloat("imag", imag);
    }

    void deserialize(const armarx::ObjectSerializerBasePtr& serializer, const ::Ice::Current& = ::Ice::Current()) override
    {
        AbstractObjectSerializerPtr obj = AbstractObjectSerializerPtr::dynamicCast(serializer);

        real = obj->getFloat("real");
        imag = obj->getFloat("imag");
    }
};
//! [TestComplexFloat Class Definition]

//! [TestComplexFloat Pointer Type Definition]
typedef IceInternal::Handle<TestComplexFloat> TestComplexFloatPtr;
//! [TestComplexFloat Pointer Type Definition]

//! [TestComplexFloat ObjectFactory]
namespace armarx
{
    namespace ObjectFactories
    {
        class TestFactories : public FactoryCollectionBase
        {
        public:
            ObjectFactoryMap getFactories() override
            {
                ObjectFactoryMap map;
                add<ComplexFloatBase, TestComplexFloat>(map);
                return map;
            }
            static const FactoryCollectionBaseCleanUp TestFactoriesVar;
        };
    }
}
//! [TestComplexFloat ObjectFactory]

//! [TestComplexFloat ObjectFactoryCPP]

namespace armarx
{
    namespace ObjectFactories
    {
        const FactoryCollectionBaseCleanUp TestFactories::TestFactoriesVar = FactoryCollectionBase::addToPreregistration(new armarx::ObjectFactories::TestFactories());
    }
}
//! [TestComplexFloat ObjectFactoryCPP]


/*
 * Test cases for custom Variant types.
 */
BOOST_AUTO_TEST_CASE(VariantCustomType)
{
    //! [TestComplexFloat Usage]
    Variant varComplex(new TestComplexFloat(1, 2));
    //! [TestComplexFloat Usage]

    TestComplexFloatPtr value(new TestComplexFloat(1, 2));

    Variant varComplex1;
    varComplex1.set<TestComplexFloat>(value);

    Variant varComplex2;
    varComplex2.setType(VariantType::TestComplexFloat);
    varComplex2.setClass(value);

    BOOST_CHECK(varComplex1.getType() == VariantType::TestComplexFloat);
    BOOST_CHECK(varComplex1.get<TestComplexFloat>()->getReal() == 1);
    BOOST_CHECK(varComplex1.get<TestComplexFloat>()->getImag() == 2);

    //! [TestComplexFloat Usage2]
    BOOST_CHECK(varComplex.getType() == VariantType::TestComplexFloat);
    BOOST_CHECK(varComplex.get<TestComplexFloat>()->getReal() == 1);
    BOOST_CHECK(varComplex.get<TestComplexFloat>()->getImag() == 2);
    //! [TestComplexFloat Usage2]
}

BOOST_AUTO_TEST_CASE(VariantKnownTypes)
{
    std::map<VariantTypeId, std::string> types = Variant::getTypes();
    std::map<VariantTypeId, std::string>::iterator iter = types.begin();

    while (iter != types.end())
    {
        printf("%s\n", iter->second.c_str());
        iter++;
    }

    BOOST_CHECK(true);
}

bool is_thrown(InvalidTypeException const& ex)
{
    return true;
}

BOOST_AUTO_TEST_CASE(VariantTypeCheck)
{
    Variant var;
    BOOST_CHECK(var.getType() == VariantType::Invalid);
    var.setType(VariantType::Int);
    //    BOOST_CHECK_EXCEPTION(var = 3.4f, InvalidTypeException, is_thrown)
    BOOST_CHECK(var.getType() == VariantType::Int);

}

BOOST_AUTO_TEST_CASE(VariantCheckInit)
{
    Variant var;
    BOOST_CHECK(!var.getInitialized());
    var.setType(VariantType::Int);
    BOOST_CHECK(!var.getInitialized());
    var.setInt(0.0);
    BOOST_CHECK(var.getInitialized());

}

BOOST_AUTO_TEST_CASE(VariantCreationDeletionTest)
{
    Variant var;
    var.setInt(500);
    //VariantPtr ptr = &var;

    BOOST_CHECK(true);
}

BOOST_AUTO_TEST_CASE(VariantTypeFromString)
{
    std::vector<float> vec;
    vec.push_back(1.2f);
    vec.push_back(1.3f);
    ARMARX_INFO_S << vec;
    ContainerTypePtr origType = VariantType::List(VariantType::Map(VariantType::Float)).clone();
    std::string typeString = VariantContainerType::allTypesToString(origType);
    std::cout  << typeString << std::endl;
    ContainerTypePtr reconstructed = VariantContainerType::FromString(typeString);
    std::cout << VariantContainerType::allTypesToString(reconstructed) << std::endl;
    BOOST_CHECK(VariantContainerType::compare(origType, reconstructed));
}


BOOST_AUTO_TEST_CASE(SingleTypeVariantListToVectorTest)
{


    SingleTypeVariantList list(VariantType::Float);

    try
    {
        list.addVariant(3.0f);
    }
    catch (...)
    {
        handleExceptions();
    }

    BOOST_CHECK(list.toStdVector<float>().at(0) = 3.0f);
}


BOOST_AUTO_TEST_CASE(ComplexSingleTypeVariantListToVectorTest)
{


    SingleTypeVariantList list(VariantType::TestComplexFloat);

    try
    {
        list.addVariant(new TestComplexFloat(1.0f, 0.1f));
    }
    catch (...)
    {
        handleExceptions();
    }

    BOOST_CHECK(list.toStdVector<TestComplexFloatPtr>().at(0)->real = 1.0f);
}

BOOST_AUTO_TEST_CASE(ComplexStringValueMapToStdMapTest)
{


    StringValueMap map(VariantType::TestComplexFloat);

    try
    {
        map.addVariant("key", new TestComplexFloat(1.0f, 0.1f));
    }
    catch (...)
    {
        handleExceptions();
    }

    ARMARX_INFO << VAROUT(map.toStdMap<TestComplexFloatPtr>().at("key")->real);
    std::map<std::string, TestComplexFloatPtr> stdmap = map.toStdMap<TestComplexFloatPtr>();
    BOOST_CHECK(stdmap.at("key")->real = 1.0f);
}


BOOST_AUTO_TEST_CASE(VariantChangeTypeTwice)
{
    Variant var;
    var.setType(VariantType::Int);
    BOOST_CHECK_THROW(var.setType(VariantType::Bool), LocalException);

    Variant var2;
    var2.setInt(0);
    BOOST_CHECK_THROW(var.setType(VariantType::String), LocalException);
}








BOOST_AUTO_TEST_CASE(TestEigenThrow)
{
    Eigen::VectorXf vec5(5);
    Eigen::VectorXf vec3(3);
    vec3.setZero();

    BOOST_CHECK_THROW(vec5 * vec3, armarx::exceptions::local::ExpressionException);

}


BOOST_AUTO_TEST_CASE(VariantSerializeMapToJSONAndBack)
{

    Ice::CommunicatorPtr iceCommunicator;
    iceCommunicator = Ice::initialize();
    //! [VariantJSON Usage]
    // Needed for deserialization - the communicator can be retrieved
    // in any ManagedIceObject with getCommunicator()
    // The next line is already done in a standard ArmarX app.
    ArmarXManager::RegisterKnownObjectFactoriesWithIce(iceCommunicator);

    JSONObjectPtr jsonSerialize(new JSONObject(iceCommunicator));

    // create Variant - every variant data type can be passed to the constructor
    VariantPtr var = new Variant(3.0f);
    jsonSerialize->setVariant("value", var); // key and value

    std::string prettyJSONstring = jsonSerialize->asString(true); // true -> pretty JSON

    // Now create the variant back from the string
    JSONObjectPtr jsonDeserialize(new JSONObject(iceCommunicator));
    jsonDeserialize->fromString(prettyJSONstring); // parse the JSON string
    ARMARX_INFO << prettyJSONstring;
    VariantPtr newVariant = jsonDeserialize->getVariant("value");
    ARMARX_INFO << "float value: " << newVariant->getFloat();
    //! [VariantJSON Usage]


    BOOST_CHECK(newVariant->getFloat() == 3.0f);
}

BOOST_AUTO_TEST_CASE(VariantSerializeMapToJSONAndBackMap)
{

    Ice::CommunicatorPtr iceCommunicator;
    iceCommunicator = Ice::initialize();
    ArmarXManager::RegisterKnownObjectFactoriesWithIce(iceCommunicator);
    StringValueMapPtr map = new StringValueMap(VariantType::Float);
    map->addVariant("firstentry", Variant(3.0f));
    JSONObjectPtr json(new JSONObject(iceCommunicator));
    json->serializeIceObject(map);
    ARMARX_INFO_S << json->asString(true);
    JSONObjectPtr json2(new JSONObject(iceCommunicator));
    json2->fromString(json->asString(true));
    //    StringValueMap map2;
    StringValueMapPtr map2 = StringValueMapPtr::dynamicCast(json2->deserializeIceObject());
    //    map2.deserialize(json);

    BOOST_CHECK(map->getVariant("firstentry")->getFloat() == map2->getVariant("firstentry")->getFloat());

}



BOOST_AUTO_TEST_CASE(VariantSerializationListWithJSON)
{

    Ice::CommunicatorPtr ic;
    ic = Ice::initialize();

    ArmarXManager::RegisterKnownObjectFactoriesWithIce(ic);



    SingleTypeVariantListPtr  list = new SingleTypeVariantList(VariantType::Float);
    Ice::Current c;
    list->addVariant(Variant(3.0f));
    list->addVariant(Variant(5.0f));

    JSONObjectPtr json(new JSONObject(ic));
    json->serializeIceObject(list);
    ARMARX_INFO_S << json->asString(true);
    SingleTypeVariantListPtr list2 =   SingleTypeVariantListPtr::dynamicCast(json->deserializeIceObject());

    BOOST_CHECK(list->getVariant(0)->getFloat() == list2->getVariant(0)->getFloat());

}




BOOST_AUTO_TEST_CASE(TimestampVariantSerializeTest)
{

    Ice::CommunicatorPtr ic;
    ic = Ice::initialize();

    ArmarXManager::RegisterKnownObjectFactoriesWithIce(ic);



    TimestampVariantPtr timestamp = TimestampVariant::nowPtr();
    TimedVariantPtr t = new TimedVariant(Variant(timestamp), timestamp->toTime());
    JSONObjectPtr json(new JSONObject(ic));
    ARMARX_INFO_S << "timestamp: " << timestamp->getTimestamp();
    json->setVariant("var", t);
    ARMARX_INFO_S << json->asString(true);
    TimedVariantPtr t2 =   TimedVariantPtr::dynamicCast(json->getVariant("var"));
    TimestampVariantPtr timestamp2 = TimestampVariantPtr::dynamicCast(t2->get<TimestampVariant>());

    BOOST_CHECK(timestamp->getTimestamp() == timestamp2->getTimestamp());
    BOOST_CHECK(t->getTimestamp() == t2->getTimestamp());

}

BOOST_AUTO_TEST_CASE(VariantAutoTypeOperator)
{

    Variant i(3);
    int myint = i;
    BOOST_CHECK_EQUAL(3, myint);

    TestComplexFloatPtr c = new TestComplexFloat(3, 5);
    Variant complexVar(c);
    TestComplexFloatPtr c2 = complexVar; // auto type operator
    BOOST_CHECK_EQUAL(c2->getImag(), c->getImag());

    //    TestComplexFloat c3 = complexVar;
    //    BOOST_CHECK_EQUAL(c3.getImag(), c->getImag());

    int faultint = 0;
    BOOST_CHECK_THROW(faultint = *VariantPtr(new Variant(3.0f)), armarx::exceptions::user::InvalidTypeException);
    ARMARX_INFO_S << faultint;

}

#include <chrono>

BOOST_AUTO_TEST_CASE(TimestampVariantTest)
{
    TimestampVariant timestamp {std::chrono::high_resolution_clock::now()};
    TimedVariant timed {timestamp, std::chrono::high_resolution_clock::now().time_since_epoch()};

    BOOST_CHECK(timestamp.getTimestamp() <= timed.getTimestamp());
}
