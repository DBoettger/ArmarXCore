/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotAPI::units
 * @author     Peter Kaiser (peter dot kaiser at kit dot edu)
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "ExampleUnit.h"

using namespace armarx;

//! [ObserversDocumentation UnitComponent3]
void ExampleUnit::onInitComponent()
{
    period = getProperty<int>("ReportPeriod").getValue();

    periodicValue = VariantPtr(new Variant(10));

    offeringTopic("PeriodicExampleValue");
}

void ExampleUnit::onConnectComponent()
{
    listenerPrx = getTopic<ExampleUnitListenerPrx>("PeriodicExampleValue");

    if (periodicTask)
    {
        periodicTask->stop();
    }

    periodicTask = new PeriodicTask<ExampleUnit>(this, &ExampleUnit::reportValue, period, false, "ExampleValueProducer");
    periodicTask->start();
}

void ExampleUnit::onDisconnectComponent()
{
    periodicTask->stop();
}

void ExampleUnit::setPeriodicValue(const VariantBasePtr& value,  const Ice::Current& c)
{
    periodicValue = VariantPtr::dynamicCast(value);

}

PropertyDefinitionsPtr ExampleUnit::createPropertyDefinitions()
{
    return PropertyDefinitionsPtr(new ExampleUnitPropertyDefinitions(getConfigIdentifier()));
}

void ExampleUnit::reportValue()
{
    listenerPrx->reportPeriodicValue(periodicValue, IceUtil::Time::now().toMicroSecondsDouble());
}
//! [ObserversDocumentation UnitComponent3]
