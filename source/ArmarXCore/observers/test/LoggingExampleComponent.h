/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    Core::test
 * @author     Erem Aksoy (eren dot aksoy at kit dot edu)
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/application/properties/Properties.h>
#include <ArmarXCore/core/system/ImportExportComponent.h>
#include <ArmarXCore/observers/variant/Variant.h>
#include <ArmarXCore/core/services/tasks/PeriodicTask.h>
#include <ArmarXCore/interface/observers/ObserverInterface.h>


typedef ::std::map< ::std::string, ::Ice::Float> NameValueMap;

namespace armarx
{
    //! [ObserversDocumentation UnitComponent1]
    class LoggingExampleComponentPropertyDefinitions:
        public ComponentPropertyDefinitions
    {
    public:
        LoggingExampleComponentPropertyDefinitions(std::string prefix):
            ComponentPropertyDefinitions(prefix)
        {
            defineRequiredProperty<int>("ReportPeriod", "The unit's reporting period in ms");
        }
    };
    //! [ObserversDocumentation UnitComponent1]

    //! [ObserversDocumentation UnitComponent2]
    class LoggingExampleComponent :
        virtual public Component
    {
    public:
        std::string getDefaultName() const override
        {
            return "LoggingExampleComponent";
        }

        void onInitComponent() override;
        void onConnectComponent() override;
        void onExitComponent() override;


        PropertyDefinitionsPtr createPropertyDefinitions() override;

    protected:

        int counter;
        float position;
        StringVariantBaseMap mapValues;

        void run();
        PeriodicTask<LoggingExampleComponent>::pointer_type task;


    private:

        DebugObserverInterfacePrx debugObserver;
    };
    //! [ObserversDocumentation UnitComponent2]
}


