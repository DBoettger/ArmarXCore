/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarXCore::core::Test
* @author     Mirko Waechter (waechter at kit dot edu)
* @date       2015
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#define BOOST_TEST_MODULE ArmarX::Core::ConditionHandlerTest
#define ARMARX_BOOST_TEST
#include <ArmarXCore/Test.h>
#include <ArmarXCore/core/ArmarXManager.h>


#include <ArmarXCore/observers/variant/Variant.h>
#include <ArmarXCore/observers/variant/ChannelRef.h>


#include <ArmarXCore/observers/exceptions/user/InvalidTypeException.h>

#include <ArmarXCore/observers/ObserverObjectFactories.h>
#include <ArmarXCore/observers/filters/AverageFilter.h>
#include <ArmarXCore/observers/filters/GaussianFilter.h>
#include <ArmarXCore/observers/filters/MedianFilter.h>
#include <ArmarXCore/observers/filters/MinMaxFilter.h>


#include <ArmarXCore/core/test/IceTestHelper.h>
#include <ArmarXCore/observers/ConditionHandler.h>
#include <ArmarXCore/observers/Event.h>
#include <ArmarXCore/observers/SystemObserver.h>
#include <ArmarXCore/observers/condition/Term.h>

using namespace armarx;
IceTestHelperPtr iceTestHelper;
struct EventListenerImpl : EventListenerInterface
{


    // EventListenerInterface interface
public:
    void reportEvent(const EventBasePtr& evt, const Ice::Current&) override
    {
        ARMARX_INFO << "Event was reported";
        gotEvent = true;
        BOOST_CHECK_EQUAL(evt->eventName, eventName);

    }
    bool gotEvent = false;
    std::string eventName = "TestEvent";
};



struct Fixture
{
    Fixture()
    {
        LogSender::SetGlobalMinimumLoggingLevel(eDEBUG);

        int registryPort = 11220;
        Ice::PropertiesPtr properties = Ice::createProperties();
        iceTestHelper = new IceTestHelper(registryPort, registryPort + 1);

        iceTestHelper->startEnvironment();
        manager = new TestArmarXManager("ConditionHandlerTest", iceTestHelper->getCommunicator(), properties);
        manager->createComponentAndRun<ConditionHandler, ConditionHandlerInterfacePrx>("ArmarX", "ConditionHandler", properties);
        manager->createComponentAndRun<SystemObserver, SystemObserverInterfacePrx>("ArmarX", "SystemObserver", properties);
    }

    ~Fixture()
    {

        manager->shutdown();
        iceTestHelper = nullptr;
    }

    TestArmarXManagerPtr manager;

};

#if  BOOST_VERSION  < 106200
BOOST_GLOBAL_FIXTURE(Fixture)
#else
BOOST_GLOBAL_FIXTURE(Fixture);
#endif

BOOST_AUTO_TEST_CASE(InstallConditionTest)
{
    IceInternal::Handle<EventListenerImpl> listener = new EventListenerImpl();
    EventListenerInterfacePrx listenerPrx = EventListenerInterfacePrx::uncheckedCast(iceTestHelper->registerAdapter("EventListener", listener));
    ConditionHandlerInterfacePrx conditionHandler = iceTestHelper->getProxy<ConditionHandlerInterfacePrx>("ConditionHandler");
    SystemObserverInterfacePrx systemObs = iceTestHelper->getProxy<SystemObserverInterfacePrx>("SystemObserver");
    BOOST_CHECK(conditionHandler);
    BOOST_CHECK(systemObs);

    ChannelRefPtr actionId = ChannelRefPtr::dynamicCast(systemObs->startTimer("TestTimer_10ms"));
    Literal expression(actionId->getDataFieldIdentifier("elapsedMs"), "larger", {10});
    EventPtr event = new Event("", listener->eventName);
    conditionHandler->installConditionWithDescription(listenerPrx, expression.getImpl(), event, "Test Event Description", true, false, {});
    ARMARX_INFO << "Active Conditions: " << conditionHandler->getActiveConditions().size();
    BOOST_CHECK_GT(conditionHandler->getActiveConditions().size(), 0);
    ARMARX_INFO << "Active Condition listener: " << conditionHandler->getActiveConditions().begin()->second->listener->ice_getIdentity().name;
    ARMARX_INFO << "Active Condition fired: " << conditionHandler->getActiveConditions().begin()->second->fired;
    usleep(300000);
    ARMARX_INFO << "Active Condition fired after wait: " << conditionHandler->getActiveConditions().begin()->second->fired;
    ARMARX_INFO << "Elapsed: " <<  actionId->getDataField("elapsedMs")->getInt();
    BOOST_CHECK(listener->gotEvent);


}
