/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarXCore::core::Test
* @author     Nils Adermann (naderman at naderman dot de)
* @date       2010
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#define BOOST_TEST_MODULE ArmarX::Core::ObserverFilterTest
#define ARMARX_BOOST_TEST
#include <ArmarXCore/Test.h>
#include <ArmarXCore/core/ArmarXManager.h>


#include <ArmarXCore/observers/variant/Variant.h>
#include <ArmarXCore/observers/variant/DatafieldRef.h>


#include <ArmarXCore/observers/exceptions/user/InvalidTypeException.h>

#include <ArmarXCore/observers/ObserverObjectFactories.h>
#include <ArmarXCore/observers/filters/AverageFilter.h>
#include <ArmarXCore/observers/filters/GaussianFilter.h>
#include <ArmarXCore/observers/filters/MedianFilter.h>
#include <ArmarXCore/observers/filters/rtfilters/GaussianFilter.h>


#include <ArmarXCore/core/test/IceTestHelper.h>
#include "ExampleUnitObserver.h"

using namespace armarx;

BOOST_AUTO_TEST_CASE(FilterConstructionTest)
{
    AverageFilterBasePtr averageFilter = new filters::AverageFilter();
    averageFilter->windowFilterSize = 10;
    averageFilter = NULL;
}

BOOST_AUTO_TEST_CASE(FilterPerformanceTest)
{
    AverageFilterBasePtr averageFilter = new filters::AverageFilter();
    averageFilter->windowFilterSize = 100;
    IceUtil::Time start = IceUtil::Time::now();
    for (int i = 0; i < 10000; ++i)
    {
        averageFilter->update(IceUtil::Time::now().toMicroSeconds(), new Variant(i + rand() % 5));
    }
    IceUtil::Time end = IceUtil::Time::now();
    ARMARX_INFO << "Duration: " << (end - start).toMilliSecondsDouble();
}


BOOST_AUTO_TEST_CASE(ObserverMedianFilterTest)
{

    Ice::PropertiesPtr properties = Ice::createProperties();

    int registryPort = 11220;
    IceTestHelperPtr iceTestHelper = new IceTestHelper(registryPort, registryPort + 1);
    iceTestHelper->startEnvironment();
    TestArmarXManagerPtr manager = new TestArmarXManager("ObserverMedianFilterTest", iceTestHelper->getCommunicator(), properties);
    //    ExampleUnitObserverPrx obs = manager->createComponentAndRun<ExampleUnitObserver, ExampleUnitObserverInterfacePrx>("ArmarX", "ExampleUnitObserver");
    ExampleUnitObserverPtr observer = Component::create<ExampleUnitObserver>();
    manager->addObject(observer);

    if (!observer->getObjectScheduler()->waitForObjectState(eManagedIceObjectStarted, 5000))
    {
        throw LocalException("ExampleUnitObserver couldnot be started");
    }

    observer->reportPeriodicValue(new Variant(0), 0);
    //! [TestObserverFilter Getting DatafieldRef]
    DatafieldRefPtr datafield = new DatafieldRef(observer.get(), "exampleChannel", "exampleDataFieldInt");
    //! [TestObserverFilter Getting DatafieldRef]
    DatafieldRefPtr strDatafield = new DatafieldRef(observer.get(), "exampleChannel", "exampleDataFieldType");

    //! [TestObserverFilter Installing Filter]
    DatafieldRefPtr filtered = DatafieldRefPtr::dynamicCast(observer->createFilteredDatafield(
                                   new filters::MedianFilter(11 /*windowSize*/),
                                   datafield));
    //! [TestObserverFilter Installing Filter]
    DatafieldRefPtr avgfiltered = DatafieldRefPtr::dynamicCast(observer->createFilteredDatafield(
                                      DatafieldFilterBasePtr(new filters::AverageFilter()),
                                      datafield));
    DatafieldRefPtr gaussfiltered = DatafieldRefPtr::dynamicCast(observer->createFilteredDatafield(
                                        DatafieldFilterBasePtr(new filters::GaussianFilter()),
                                        datafield));
    DatafieldRefPtr gaussfiltered2 = DatafieldRefPtr::dynamicCast(observer->createFilteredDatafield(
                                         DatafieldFilterBasePtr(new filters::GaussianFilter()),
                                         datafield));
    DatafieldRefPtr gaussfiltered3 = DatafieldRefPtr::dynamicCast(observer->createFilteredDatafield(
                                         DatafieldFilterBasePtr(new filters::GaussianFilter()),
                                         datafield));

    BOOST_CHECK_THROW(DatafieldRefPtr::dynamicCast(observer->createFilteredDatafield(
                          DatafieldFilterBasePtr(new filters::GaussianFilter()),
                          strDatafield)), exceptions::user::UnsupportedTypeException);

    for (int i = 0; i < 15; ++i)
    {
        int value = i * i;
        observer->reportPeriodicValue(new Variant(value), 0);
        usleep(10000);
        //        ARMARX_INFO_S << "v: " << value;
    }

    // default filter length: 11 samples.
    BOOST_CHECK_EQUAL(observer->getDataField(avgfiltered->getDataFieldIdentifier())->getInt(), 91); // 91 = avg(14*14, 13*13, ... 4*4)
    BOOST_CHECK_EQUAL(observer->getDataField(filtered->getDataFieldIdentifier())->getInt(), 81); // 81 = median(14*14, 13*13, ... 4*4) = 9*9
    ARMARX_INFO_S << "avgfiltered " << observer->getDataField(avgfiltered->getDataFieldIdentifier())->getInt();
    //! [TestObserverFilter Retrieving filtered data]
    ARMARX_INFO_S << "filtered " << filtered->getDataField()->getInt();
    //! [TestObserverFilter Retrieving filtered data]
    ARMARX_INFO_S << "gauss " << observer->getDataField(gaussfiltered->getDataFieldIdentifier())->getInt();
    BOOST_CHECK(observer->existsDataField(avgfiltered->getChannelRef()->channelName, avgfiltered->datafieldName));

    observer->removeDatafield(avgfiltered->getDataFieldIdentifier());
    BOOST_CHECK(!observer->existsDataField(avgfiltered->getChannelRef()->channelName, avgfiltered->datafieldName));
    BOOST_CHECK(observer->existsDataField(filtered->getChannelRef()->channelName, filtered->datafieldName));

    //! [TestObserverFilter Removing filter]
    observer->removeFilteredDatafield(filtered);
    //! [TestObserverFilter Removing filter]
    manager->shutdown();
}

BOOST_AUTO_TEST_CASE(ObserverGaussianFilterTest2)
{
    return;
    Ice::PropertiesPtr properties = Ice::createProperties();

    int registryPort = 11220;
    IceTestHelperPtr iceTestHelper = new IceTestHelper(registryPort, registryPort + 1);
    iceTestHelper->startEnvironment();
    TestArmarXManagerPtr manager = new TestArmarXManager("ObserverMedianFilterTest", iceTestHelper->getCommunicator(), properties);
    //    ExampleUnitObserverPrx obs = manager->createComponentAndRun<ExampleUnitObserver, ExampleUnitObserverInterfacePrx>("ArmarX", "ExampleUnitObserver");
    ExampleUnitObserverPtr obs = Component::create<ExampleUnitObserver>();
    manager->addObject(obs);

    if (!obs->getObjectScheduler()->waitForObjectState(eManagedIceObjectStarted, 5000))
    {
        throw LocalException("ExampleUnitObserver couldnot be started");
    }

    obs->reportPeriodicValue(new Variant(0), 0);
    obs->reportPeriodicValue(new Variant(0.0f), 0);

    DatafieldRefPtr datafield = new DatafieldRef(obs.get(), "exampleChannel", "exampleDataFieldFloat");



    DatafieldRefPtr gaussfiltered = DatafieldRefPtr::dynamicCast(obs->createFilteredDatafield(
                                        DatafieldFilterBasePtr(new filters::GaussianFilter()),
                                        datafield));


    for (float i = 0; i < 3; i += 0.2)
    {
        float value = sin(i);
        obs->reportPeriodicValue(new Variant(value), 0);
        usleep(10000);

        //        ARMARX_INFO_S << "v: " << value;
    }


    for (float i = 0; i < 15; i += 0.2)
    {
        float value = (float)(sin(i) + (rand() % 100) / 300);
        obs->reportPeriodicValue(new Variant(value), 0);
        usleep(10000);

        //        ARMARX_INFO_S << "v: " << value << " filtered: " << obs->getDataField(gaussfiltered->getDataFieldIdentifier())->getFloat();
    }


}

BOOST_AUTO_TEST_CASE(RTFilterGauss)
{
    {
        armarx::rtfilters::GaussianFilter filter(30, 5);
        armarx::rtfilters::GaussianFilter filter2(30, IceUtil::Time::milliSecondsDouble(5));
        int size =  15;
        for (int i = 0; i < size; ++i)
        {
            auto filtered = filter.update(IceUtil::Time::milliSeconds(i), i > size / 2 ? 1 : 0);
            auto filtered2 = filter2.update(IceUtil::Time::milliSeconds(i), i > size / 2 ? 1 : 0);
            BOOST_CHECK_CLOSE(filtered, filtered2, 0.001);
            ARMARX_INFO << VAROUT(i) << VAROUT(filtered) << VAROUT(filtered2);
        }
    }

}



