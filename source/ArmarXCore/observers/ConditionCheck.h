/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Core
* @author     Kai Welke (welke _at_ kit _dot_ edu)
* @date       2011
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <map>
#include <string>
#include <ArmarXCore/core/system/ImportExport.h>

#include <ArmarXCore/interface/observers/VariantBase.h>
#include <ArmarXCore/interface/observers/ConditionCheckBase.h>
#include <ArmarXCore/interface/observers/ObserverInterface.h>

#include <ArmarXCore/observers/variant/Variant.h>
#include <ArmarXCore/observers/variant/DataFieldIdentifier.h>

namespace armarx
{
    typedef std::vector<VariantTypeId> VariantTypeIdList;

    class ConditionCheck;
    typedef IceInternal::Handle<ConditionCheck> ConditionCheckPtr;

    /**
     * @class ConditionCheck
     * @ingroup Conditions
     *
     * A ConditionCheck implements a check on the sensor data stream of a Sensor-Actor Unit.
     * ConditionCheck instances can be arbitrarily combined to complex Terms.
     *
     * All custom condition checks should inherit from this base class.
     */
    class ARMARXCORE_IMPORT_EXPORT ConditionCheck :
        virtual public ConditionCheckBase
    {
    public:
        /**
         * Creates and initializes a ConditionCheck instance
         */
        ConditionCheck();

        /**
         *  Destructor
         */
        ~ConditionCheck() override {}

        /**
         * Creates a new ConditionCheck instance.
         * It checks if channel and datafield named in configuration are availabel and if the type of the datafield
         * is supported by the check.
         *
         * @param configuration The configuration of the check to instantiate
         * @param channelRegistry The current registry of available channels
         *
         * @throws InvalidConditionException if the requested channel or datafield could not be found.
         *
         * @return Pointer to a ConditionCheck instance
         */
        virtual ConditionCheckPtr createInstance(const CheckConfiguration& configuration, const ChannelRegistry& channelRegistry);

        /**
         * Clones the current check. Implement this in each subclass of the check.
         *
         * @return new instance
         */
        virtual ConditionCheck* clone()
        {
            return new ConditionCheck();
        }

        /**
         * Resets the status flags of the check such as check results.
         */
        void reset();

        /**
         * Evaluates the condition based on the given registry of data fields.
         *
         * The result of the evaluation can be accessed by calling getFulFilled.
         *
         * @param dataFields The registry of data fields to consider
         */
        void evaluateCondition(const DataFieldRegistry& dataFields);

        /**
         * Retrieve result of a condition evaluation initiated by evaluateCondition
         *
         * @return true if test was successful, false otherwise
         */
        bool getFulFilled();

        friend std::ostream& operator<<(std::ostream& stream, const ConditionCheck& rhs)
        {
            // stream << ElementaryConditionPtr::dynamicCast(rhs.condition) << " = " << rhs.fulFilled;
            return stream;
        }

        friend std::ostream& operator<<(std::ostream& stream, const ConditionCheckPtr& rhs)
        {
            // stream << ElementaryConditionPtr::dynamicCast(rhs->condition) << " = " << rhs->fulFilled;
            return stream;
        }

    protected:
        /**
         * Sets the number of paramaters required for this check
         *
         * @param numberParameters number of parameters
         */
        void setNumberParameters(int numberParameters);

        /**
         * Add a supported type for elementary condition check
         * marks pairs of (dataFieldType,EvaluationTypes)
         *
         * @param dataFieldType valid type for the dataField variant. Set to 0 for: all types are supported.
         * @param parameterTypes types of the parameters
         */
        void addSupportedType(VariantTypeId dataFieldType = 0, ParameterTypeList parameterTypes = ParameterTypeList());

        /**
         * Retrieve parameters of check
         *
         * @param index of the parameter
         *
         * @return reference to check parameter
         */
        const Variant& getParameter(int index);

        /**
         * Evaluate the condition based on the current data field values.
         * This method should be overwritten in order to create a custom condition check.
         *
         * @param dataFields The values of the relevant data fields to check against
         *
         * @return In this implementation, returns always true
         */
        virtual bool evaluate(const StringVariantMap& dataFields)
        {
            return true;
        }

        static ParameterTypeList createParameterTypeList(int numberTypes, ...);

    private:
        void assureTypeSupported(const CheckConfiguration& configuration, VariantTypeId dataFieldType);

        bool firstEval;
    };

    namespace checks
    {
        class CheckBase;
        typedef boost::shared_ptr<CheckBase> CheckBasePtr;
        typedef boost::shared_ptr<const CheckBase> CheckBaseConstPtr;

        class CheckBase
        {
        public:
            virtual ~CheckBase()
            {
            }
            virtual const std::string& getCheckStr() const
            {
                return _checkStr;
            }

            static CheckBasePtr create(const std::string& check)
            {
                return CheckBasePtr(new CheckBase(check));
            }
        private:
            const std::string _checkStr;
            CheckBase() {}
            CheckBase(std::string check) : _checkStr(check) {}
        };

    }

#define ARMARX_CREATE_CHECK(OFFERER,NEWCHECK) namespace checks { namespace OFFERER { \
            const CheckBaseConstPtr NEWCHECK =  CheckBase::create(#NEWCHECK);\
        }}

    /*
    //    namespace checks
    //    {
    //        class CheckBase;
    //        typedef IceUtil::Handle<CheckBase> CheckBasePtr;
    //        struct CheckBase : virtual public IceUtil::Shared{
    //            virtual std::string getCheck() = 0;//{ throw LocalException("CheckBase class must not be constructed. Create a derived class instead."); return "";}
    //            //virtual CheckBasePtr create() = 0;
    //        protected:
    //            CheckBase() {}
    //        };

    //    }

    //#define ARMARX_CREATE_CHECK(OFFERER,NEWCHECK) namespace checks { namespace OFFERER {struct NEWCHECK : CheckBase{ \
    //    std::string getCheck(){ return #NEWCHECK; }\
    //    static CheckBasePtr create(){ return new NEWCHECK(); }\
    //    };\
    //    }}
    */
}
extern template class ::IceInternal::Handle<::armarx::ConditionCheck>;
