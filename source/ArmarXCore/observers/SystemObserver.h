/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Core
* @author     Kai Welke (welke _at_ kit _dot_ edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <string>
#include <map>
#include <boost/thread/mutex.hpp>

#include <ArmarXCore/core/system/ImportExport.h>
#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/services/tasks/PeriodicTask.h>

#include <ArmarXCore/interface/observers/VariantBase.h>
#include <ArmarXCore/interface/observers/SystemObserverInterface.h>

#include <ArmarXCore/observers/ConditionCheck.h>
#include <ArmarXCore/observers/Observer.h>


namespace armarx
{
#define ARMARX_SYSTEM_OBSERVER SystemObserver
    ARMARX_CREATE_CHECK(ARMARX_SYSTEM_OBSERVER, valid)
    ARMARX_CREATE_CHECK(ARMARX_SYSTEM_OBSERVER, updated)
    ARMARX_CREATE_CHECK(ARMARX_SYSTEM_OBSERVER, equals)
    ARMARX_CREATE_CHECK(ARMARX_SYSTEM_OBSERVER, inrange)
    ARMARX_CREATE_CHECK(ARMARX_SYSTEM_OBSERVER, smaller)
    ARMARX_CREATE_CHECK(ARMARX_SYSTEM_OBSERVER, larger)

    /**
     * @ingroup ObserversSub
     */
    struct SystemObserverTimer
    {
        std::string timerName;
        int startTimeMs;
        int elapsedMs;
        bool paused;
    };

    typedef std::map<std::string, SystemObserverTimer> SystemObserverTimerMap;

    /**
     * @ingroup ObserversSub
     */
    struct SystemObserverCounter
    {
        std::string counterName;
        int value;
    };

    typedef std::map<std::string, SystemObserverCounter> SystemObserverCounterMap;

    /**
     * @defgroup Component-SystemObserver SystemObserver
     * @ingroup ObserversSub ArmarXCore-Components
     * The SystemObserver component offers functionality to create timers and distributed counters.
     * Since the SystemObserver is an \ref Observers "observer" it is possible to install \ref Conditions "conditions"
     * on the timers (e.g. for timeouts) and counters.
     *
     * @class SystemObserver
     * @ingroup Component-SystemObserver
     * @brief Provides timers for timeout events and counters
     */
    class ARMARXCORE_IMPORT_EXPORT SystemObserver :
        virtual public Observer,
        virtual public SystemObserverInterface
    {
    public:
        // framework hooks
        void onInitObserver() override;
        void onConnectObserver() override;
        void onExitObserver() override;

        std::string getDefaultName() const override
        {
            return "SystemObserver";
        }

        // implementation of SystemObserverInterface
        // timers
        /**
          Creates a new timer with name \p timerBaseName and starts it.
          @param timerBaseName This is only the basename of the counter to make it human-comprehensable.
                 To get the real countername, check the channelName value of the return value
          */
        ChannelRefBasePtr startTimer(const std::string& timerBaseName, const Ice::Current& c = ::Ice::Current()) override;
        /**
         * @brief resetTimer sets the start time of \p timer to Time.Now() and start the timer
         * @param timer ChannelRef obtained via SystemObserver::startTimer()
         */
        void resetTimer(const ChannelRefBasePtr& timer, const Ice::Current& c = ::Ice::Current()) override;
        /**
         * @brief pauseTimer pauses the advance of time in \p timer. Can be resumed with SystemObserver::unpauseTimer().
         * @param timer ChannelRef obtained via SystemObserver::startTimer()
         * @see unpauseTimer
         */
        void pauseTimer(const ChannelRefBasePtr& timer, const Ice::Current& c = ::Ice::Current()) override;
        /**
         * @brief unpauseTimer resumes the advancing in time of \p timer. Call this after pausing a timer with SystemObserver::pauseTimer().
         * @param timer ChannelRef obtained via SystemObserver::startTimer()
         * @see pauseTimer
         */
        void unpauseTimer(const ChannelRefBasePtr& timer, const Ice::Current& c = ::Ice::Current()) override;
        /**
         * @brief removeTimer stops \p timer and removes it from the SystemObserver.
         * @param timer ChannelRef obtained via SystemObserver::startTimer()
         */
        void removeTimer(const ChannelRefBasePtr& timer, const Ice::Current& c = ::Ice::Current()) override;

        // counters
        /**
          Creates a new counter and starts it.
          @param counterBaseName This is only the basename of the counter to make it human-comprehensable.
                 To get the real countername, check the channelName value of the return value
          */
        ChannelRefBasePtr startCounter(int initialValue, const std::string& counterBaseName, const Ice::Current& c = ::Ice::Current()) override;
        int incrementCounter(const ChannelRefBasePtr& counter, const Ice::Current& c = ::Ice::Current()) override;
        int decrementCounter(const ChannelRefBasePtr& counter, const Ice::Current& c = ::Ice::Current()) override;
        void resetCounter(const ChannelRefBasePtr& counter, const Ice::Current& c = ::Ice::Current()) override;
        void setCounter(const ChannelRefBasePtr& counter, int counterValue, const Ice::Current& c = ::Ice::Current()) override;
        void removeCounter(const ChannelRefBasePtr& counter, const Ice::Current& c = ::Ice::Current()) override;

    protected:
        virtual void runTimerTask();

    private:
        // timer tools
        void resetTimer(SystemObserverTimer& timer);
        void updateTimer(SystemObserverTimer& timer);
        int getCurrentTimeMs();
        int getElapsedTimeMs(int referenceTimeMs);

        // counter tools
        void updateCounter(SystemObserverCounterMap::iterator& iterCounter);

        SystemObserverTimerMap timers;
        boost::mutex timersMutex;

        SystemObserverCounterMap counters;
        boost::mutex countersMutex;
        int maxTimerId;
        int maxCounterId;

        PeriodicTask<SystemObserver>::pointer_type periodicTask;
    };
}

