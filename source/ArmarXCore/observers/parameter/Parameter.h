/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Core
* @author     Kai Welke (welke _at_ kit _dot_ edu)
* @date       2012 Kai Welke
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <ArmarXCore/core/system/ImportExport.h>
#include <ArmarXCore/observers/variant/Variant.h>
#include <ArmarXCore/interface/observers/VariantBase.h>
#include <ArmarXCore/interface/observers/ParameterBase.h>
#include <string>
#include <ostream>

namespace armarx
{
    class Parameter;
    /**
     * Typedef of ParameterPtr as IceInternal::Handle<Parameter> for convenience.
     */
    typedef IceInternal::Handle<Parameter> ParameterPtr;

    /**
    */
    class ARMARXCORE_IMPORT_EXPORT Parameter :
        virtual public ParameterBase
    {
    public:
        /**
        * Creates an empty Parameter. Required for Ice ObjectFactory
        */
        Parameter() {  }
        Parameter(const Parameter& source);
        virtual Parameter& operator=(const Parameter& source);

        ParameterBasePtr clone(const Ice::Current& c = ::Ice::Current()) const override;
        // setter
        void setVariant(const VariantBasePtr& variant, const Ice::Current& c = ::Ice::Current()) override;
        void setVariantList(const SingleTypeVariantListBasePtr& variantList, const Ice::Current& c = ::Ice::Current()) override;
        void setDataFieldIdentifier(const DataFieldIdentifierBasePtr& dataFieldIdentifier, const Ice::Current& c = ::Ice::Current()) override;


        // getter
        ParameterType getParameterType(const Ice::Current& c = ::Ice::Current()) const override;
        VariantTypeId getVariantType(const Ice::Current& c = ::Ice::Current()) const override;
        VariantBasePtr getVariant(const Ice::Current& c = ::Ice::Current()) const override;
        SingleTypeVariantListBasePtr getVariantList(const Ice::Current& c = ::Ice::Current()) const override;
        DataFieldIdentifierBasePtr getDataFieldIdentifier(const Ice::Current& c = ::Ice::Current()) const override;

        bool validate(const Ice::Current& c = ::Ice::Current()) const override;
    };
}

