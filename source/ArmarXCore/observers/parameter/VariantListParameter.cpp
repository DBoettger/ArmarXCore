/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Core
* @author     Kai Welke (welke _at_ kit _dot_ edu)
* @date       2012 Kai Welke
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include <ArmarXCore/observers/parameter/VariantListParameter.h>
#include <ArmarXCore/observers/variant/Variant.h>

using namespace armarx;

void VariantListParameter::setVariantList(const SingleTypeVariantListBasePtr& variantList, const Ice::Current& c)
{
    if (!this->variantList)
    {
        this->variantList = new SingleTypeVariantList(variantList->getContainerType());
    }

    this->variantList = SingleTypeVariantListBasePtr::dynamicCast(variantList->cloneContainer());

}

VariantTypeId VariantListParameter::getVariantType(const Ice::Current& c) const
{
    return variantList->getContainerType();
}

VariantListParameter::VariantListParameter(const VariantListParameter& source) :
    IceUtil::Shared(source),
    Ice::Object(source),
    ParameterBase(source),
    Parameter(source),
    VariantListParameterBase(source)
{
    throw LocalException("NYI");
    type = source.type;
    variantList = new SingleTypeVariantList(source.variantList->getContainerType());

    for (int i = 0 ; i < source.variantList->getSize() ; i++)
    {
        VariantPtr variant = new Variant();
        *variant = *(VariantPtr::dynamicCast(source.variantList->getElementBase(i)));

        //        this->variantList->addElement(variant);
    }
}

Parameter& VariantListParameter::operator =(const Parameter& source)
{
    if (type != source.getParameterType())
    {
        throw InvalidTypeException();
    }

    type = source.getParameterType();
    *this->variantList = *source.getVariantList();
    return *this;
}

VariantListParameter::VariantListParameter(const SingleTypeVariantList& source)
{
    variantList = new SingleTypeVariantList(source.getContainerType());
    type = eVariantListParam;
    *variantList = source;
}

ParameterBasePtr VariantListParameter::clone(const Ice::Current& c) const
{
    return new VariantListParameter(*this);
}


SingleTypeVariantListBasePtr VariantListParameter::getVariantList(const Ice::Current& c) const
{
    return variantList;
}

bool VariantListParameter::validate(const Ice::Current& c) const
{
    for (int i = 0 ; i < variantList->getSize() ; i++)
    {
        if (!variantList->getElementBase(i)->validateElements())
        {
            return false;
        }
    }

    return true;
}

