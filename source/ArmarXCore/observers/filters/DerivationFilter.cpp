/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarX
 * @author     Mirko Waechter( mirko.waechter at kit dot edu)
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "DerivationFilter.h"
#include <ArmarXCore/observers/exceptions/user/InvalidTypeException.h>
#include <iterator>
namespace armarx
{
    namespace filters
    {

        DerivationFilter::DerivationFilter()
        {
            windowFilterSize = 2;
        }

        VariantBasePtr DerivationFilter::calculate(const Ice::Current& c) const
        {
            ScopedLock lock(historyMutex);
            if (dataHistory.size() == 0)
            {
                return nullptr;
            }

            VariantTypeId type = dataHistory.begin()->second->getType();
            double diff = 0;

            double deriv = 0;
            for (auto it = std::next(dataHistory.begin()); it != dataHistory.end(); it++)
            {
                auto itPrev = std::prev(it);
                if (type == VariantType::Float)
                {
                    diff = (it->second->getFloat() - itPrev->second->getFloat());
                }
                else if (type == VariantType::Double)
                {
                    diff = (it->second->getDouble() - itPrev->second->getDouble());
                }
                else if (type == VariantType::Int)
                {
                    diff = (it->second->getInt() - itPrev->second->getInt());
                }
                double deltaT = it->first - itPrev->first;
                if (deltaT > 0)
                {
                    deriv += diff / (deltaT * 0.000001);
                }
            }

            deriv /= dataHistory.size() - 1;




            if (type == VariantType::Float)
            {
                return new Variant((float)deriv);
            }
            else if (type == VariantType::Double)
            {
                return new Variant(deriv);
            }
            else if (type == VariantType::Int)
            {
                return new Variant((int)deriv);
            }

            throw exceptions::user::UnsupportedTypeException(type);
        }

        ParameterTypeList DerivationFilter::getSupportedTypes(const Ice::Current& c) const
        {
            ParameterTypeList result;
            result.push_back(VariantType::Int);
            result.push_back(VariantType::Float);
            result.push_back(VariantType::Double);
            return result;
        }

        StringFloatDictionary DerivationFilter::getProperties(const Ice::Current& c) const
        {
            return StringFloatDictionary {{"minSampleTimeDelta", minSampleTimeDelta}, {"windowFilterSize", windowFilterSize}};
        }

        void DerivationFilter::setProperties(const StringFloatDictionary& newValues, const Ice::Current& c)
        {
            auto it = newValues.find("minSampleTimeDelta");
            if (it != newValues.end())
            {
                minSampleTimeDelta = it->second;
            }
            it = newValues.find("windowFilterSize");
            if (it != newValues.end())
            {
                windowFilterSize = it->second;
            }
        }

    }
}
