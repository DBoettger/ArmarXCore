/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once
#include "DatafieldFilter.h"

#include <ArmarXCore/interface/observers/ObserverInterface.h>
#include <ArmarXCore/interface/observers/Filters.h>
#include <ArmarXCore/observers/exceptions/user/InvalidTypeException.h>

namespace armarx
{
    template <class IceBaseClass, class DerivedClass>
    class GenericFactory;
    namespace filters
    {

        /**
         * @class MedianFilter
         * @ingroup ObserverFilters
         * @brief The MedianFilter class provides an implementation
         *  for a median for datafields of type float, int and double.
         */
        class MedianFilter :
            virtual public MedianFilterBase,
            virtual public DatafieldFilter
        {
        public:
            MedianFilter(int windowSize = 11);

            // DatafieldFilterBase interface
        public:
            VariantBasePtr calculate(const Ice::Current& c = GlobalIceCurrent) const override;

            /**
             * @brief This filter supports: Int, Float, Double
             * @return List of VariantTypes
             */
            ParameterTypeList getSupportedTypes(const Ice::Current& c = GlobalIceCurrent) const override;

            template <typename Type>
            static std::vector<Type> SortVariants(const TimeVariantBaseMap& map)
            {
                std::vector<Type> values;

                for (auto v : map)
                {
                    VariantPtr v2 = VariantPtr::dynamicCast(v.second);
                    values.push_back(v2->get<Type>());
                }

                std::sort(values.begin(), values.end());
                return values;
            }

        };



    } // namespace Filters
} // namespace armarx

