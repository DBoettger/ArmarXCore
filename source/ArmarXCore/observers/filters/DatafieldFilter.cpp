/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "DatafieldFilter.h"
#include <ArmarXCore/core/logging/Logging.h>

namespace armarx
{

    DatafieldFilter::DatafieldFilter() :
        DatafieldFilterBase()
    {
    }

    DatafieldFilter::DatafieldFilter(const armarx::DatafieldFilter& filter) :
        IceUtil::Shared(filter),
        DatafieldFilterBase(filter),
        dataHistory(filter.dataHistory)
    {

    }


    void DatafieldFilter::update(Ice::Long timestamp, const VariantBasePtr& value, const Ice::Current&)
    {
        if (!value || !value->getInitialized())
        {
            return;
        }
        bool recalculate = false;
        {
            ScopedLock lock(historyMutex);
            //            if ((int)dataHistory.capacity() != windowFilterSize)
            //            {
            //                dataHistory.set_capacity(windowFilterSize);
            //            }
            if (dataHistory.size() == 0 || timestamp - dataHistory.rbegin()->first >= minSampleTimeDelta)
            {
                if (dataHistory.size() >= (unsigned)windowFilterSize)
                {
                    dataHistory.pop_front();
                }
                dataHistory.push_back(std::make_pair(timestamp, value));
                recalculate = true;
            }
        }
        if (recalculate)
        {
            filteredValue = calculate();
        }

    }

    VariantBasePtr DatafieldFilter::getValue(const Ice::Current&) const
    {
        if (filteredValue)
        {
            return filteredValue;
        }
        else
        {
            return calculate();
        }
    }

    bool DatafieldFilter::checkTypeSupport(VariantTypeId variantType, const Ice::Current&) const
    {
        auto types = getSupportedTypes();

        if (std::find(types.begin(), types.end(), variantType) != types.end())
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    armarx::StringFloatDictionary DatafieldFilter::getProperties(const Ice::Current&) const
    {
        return StringFloatDictionary {{"windowFilterSize", windowFilterSize},
            {"minSampleTimeDelta", minSampleTimeDelta}
        };
    }

    void DatafieldFilter::setProperties(const armarx::StringFloatDictionary& newValues, const Ice::Current&)
    {
        auto it = newValues.find("windowFilterSize");
        if (it != newValues.end())
        {
            windowFilterSize = it->second;
        }
        it = newValues.find("minSampleTimeDelta");
        if (it != newValues.end())
        {
            minSampleTimeDelta = it->second;
        }
    }

    const TimeVariantBaseMap& DatafieldFilter::getDataHistory() const
    {
        return dataHistory;
    }




} // namespace armarx



