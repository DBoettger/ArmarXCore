/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarX
 * @author     Mirko Waechter( mirko.waechter at kit dot edu)
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "GaussianFilter.h"

#include <ArmarXCore/observers/exceptions/user/InvalidTypeException.h>

using namespace armarx;

armarx::filters::GaussianFilter::GaussianFilter(int filterSizeInMs, int windowSize)
{
    this->filterSizeInMs = filterSizeInMs;
    this->windowFilterSize = windowSize;
}

armarx::VariantBasePtr armarx::filters::GaussianFilter::calculate(const Ice::Current&) const
{
    ScopedLock lock(historyMutex);

    if (dataHistory.size() == 0)
    {
        return nullptr;
    }

    VariantTypeId type = dataHistory.begin()->second->getType();

    if (type == VariantType::Float)
    {
        return new Variant(calcGaussianFilteredValue<float>(dataHistory));
    }
    else if (type == VariantType::Double)
    {
        return new Variant(calcGaussianFilteredValue<double>(dataHistory));
    }
    else if (type == VariantType::Int)
    {
        return new Variant(calcGaussianFilteredValue<int>(dataHistory));
    }

    throw exceptions::user::UnsupportedTypeException(type);
}

armarx::ParameterTypeList armarx::filters::GaussianFilter::getSupportedTypes(const Ice::Current&) const
{
    ParameterTypeList result;
    result.push_back(VariantType::Int);
    result.push_back(VariantType::Float);
    result.push_back(VariantType::Double);
    return result;
}


armarx::StringFloatDictionary armarx::filters::GaussianFilter::getProperties(const Ice::Current&) const
{
    auto props = DatafieldFilter::getProperties();
    props["filterSizeInMs"] = filterSizeInMs;
    return props;
}

void armarx::filters::GaussianFilter::setProperties(const armarx::StringFloatDictionary& newValues, const Ice::Current&)
{
    auto it = newValues.find("filterSizeInMs");
    if (it != newValues.end())
    {
        filterSizeInMs = it->second;
    }
    DatafieldFilter::setProperties(newValues);
}
