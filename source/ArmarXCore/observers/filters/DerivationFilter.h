/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarX
 * @author     Mirko Waechter( mirko.waechter at kit dot edu)
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include "DatafieldFilter.h"
#include <ArmarXCore/interface/observers/Filters.h>
namespace armarx
{
    namespace filters
    {

        /**
         * @brief The DerivationFilter calculates the derivate of a datafield.
         */
        class DerivationFilter :
            public DatafieldFilter,
            public DerivationFilterBase
        {
        public:
            DerivationFilter();

            VariantBasePtr calculate(const Ice::Current& c = GlobalIceCurrent) const override;

            /**
             * @brief This filter supports: Int, Float, Double
             * @return List of VariantTypes
             */
            ParameterTypeList getSupportedTypes(const Ice::Current& c = GlobalIceCurrent) const override;
            armarx::StringFloatDictionary getProperties(const Ice::Current& c = GlobalIceCurrent) const override;
            void setProperties(const armarx::StringFloatDictionary& newValues, const Ice::Current& c = GlobalIceCurrent) override;
        };

    }
}

