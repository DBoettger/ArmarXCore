/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2017, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarX
 * @author     Mirko Waechter( mirko.waechter at kit dot edu)
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "ButterworthFilter.h"
#include <ArmarXCore/core/logging/Logging.h>

namespace armarx
{
    namespace rtfilters
    {

        ButterworthFilter::ButterworthFilter(double frequency, int sampleRate, PassType filterPassType, double resonance) :
            RTFilterBase(0)
        {
            this->resonance = resonance;
            this->frequency = frequency;
            this->sampleRate = sampleRate;
            this->filterPassType = filterPassType;
            switch (filterPassType)
            {
                case Lowpass:
                    c = 1.0 / tan(M_PI * frequency / sampleRate);
                    a1 = 1.0 / (1.0 + resonance * c + c * c);
                    a2 = 2.0 * a1;
                    a3 = a1;
                    b1 = 2.0 * (1.0 - c * c) * a1;
                    b2 = (1.0 - resonance * c + c * c) * a1;
                    break;
                case Highpass:
                    c = tan(M_PI * frequency / sampleRate);
                    a1 = 1.0 / (1.0 + resonance * c + c * c);
                    a2 = -2.0 * a1;
                    a3 = a1;
                    b1 = 2.0 * (c * c - 1.0) * a1;
                    b2 = (1.0 - resonance * c + c * c) * a1;
                    break;
                default:
                    ARMARX_INFO << "Unknown pass type";

            }
        }

        void ButterworthFilter::setInitialValue(double value)
        {
            for (auto& v : inputHistory)
            {
                v = value;
            }
            for (auto& v : outputHistory)
            {
                v = value;
            }
            newestValue = value;
        }

        double ButterworthFilter::calculate()
        {
            double newOutput = a1 * newestValue + a2 * this->inputHistory[0] + a3 * this->inputHistory[1] - b1 * this->outputHistory[0] - b2 * this->outputHistory[1];

            this->inputHistory[1] = this->inputHistory[0];
            this->inputHistory[0] = newestValue;

            this->outputHistory[2] = this->outputHistory[1];
            this->outputHistory[1] = this->outputHistory[0];
            this->outputHistory[0] = newOutput;
            return newOutput;
        }

        double ButterworthFilter::update(const IceUtil::Time& timestamp, double newValue)
        {
            newestValue = newValue;
            return calculate();
        }


    } // namespace rtfilters
} // namespace armarx




