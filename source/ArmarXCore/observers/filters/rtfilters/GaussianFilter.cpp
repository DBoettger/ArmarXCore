/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2017, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarX
 * @author     Mirko Waechter( mirko.waechter at kit dot edu)
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "GaussianFilter.h"

namespace armarx
{
    namespace rtfilters
    {

        GaussianFilter::GaussianFilter(size_t windowSize, double filterWidthInMs) :
            RTFilterBase(windowSize),
            filterWidthInSec(0.001f * filterWidthInMs)
        {

        }

        GaussianFilter::GaussianFilter(size_t windowSize, IceUtil::Time filterWidth) :
            RTFilterBase(windowSize),
            filterWidthInSec(filterWidth.toSecondsDouble())
        {

        }


        double GaussianFilter::calculate()
        {
            if (dataHistory.empty())
            {
                return 0.0;
            }

            const double sigma = filterWidthInSec / 3.0;
            //-log(0.05) / pow(itemsToCheck+1+centerIndex,2) * ( sqrt(1.0/h) * sqrt(2*3.14159265));

            double weightedSum = 0;
            double sumOfWeight = 0;



            auto newestTimestamp = dataHistory.rbegin()->first.toMilliSecondsDouble();

            const double sqrt2PI = sqrt(2 * M_PI);
            double quotient = (2 * sigma * sigma);
            double quotient2 = sigma * sqrt2PI;

            for (auto it = dataHistory.begin();
                 it != dataHistory.end();
                 it++
                )
            {
                double value = it->second;
                double diff = 0.001 * (it->first.toMilliSecondsDouble() - newestTimestamp);

                double squared = diff * diff;
                const double gaussValue = exp(-squared / quotient) / (quotient2);
                sumOfWeight += gaussValue;
                weightedSum += gaussValue * value;

            }

            double result;
            result = weightedSum / sumOfWeight;
            return result;
        }

    } // namespace rtfilters
} // namespace armarx
