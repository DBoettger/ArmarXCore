/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2017, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarX
 * @author     Mirko Waechter( mirko.waechter at kit dot edu)
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#ifndef RTFILTERBASE_H
#define RTFILTERBASE_H

#include <boost/circular_buffer.hpp>

#include <IceUtil/Time.h>

namespace armarx
{
    namespace rtfilters
    {

        /**
         * @brief The RTFilterBase class is the base class for all real time capable filters.
         * Real Time capable means here that no heap memory allocation is done and no mutexes are locked
         * during the update() function. Also the runtime must be the same during each call (e.g. no random searches).
         */
        class RTFilterBase
        {
        public:
            RTFilterBase(size_t historySize);
            virtual ~RTFilterBase() {}
            virtual void reset();
            virtual double update(IceUtil::Time const& timestamp, double newValue);
            double update(double deltaSec, double newValue);
            virtual double calculate() = 0;
            double getCurrentValue() const;

        protected:
            boost::circular_buffer<std::pair<IceUtil::Time, double> > dataHistory;
            double currentValue = 0.0;
        };
        using RTFilterBasePtr = std::shared_ptr<RTFilterBase>;

    } // namespace rtfilters
} // namespace armarx

#endif // RTFILTERBASE_H
