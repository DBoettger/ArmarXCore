/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2012-2016, High Performance Humanoid Technologies (H2T),
 * Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @author     Simon Ottenhaus (simon dot ottenhaus at kit dot edu)
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "MedianFilter.h"

using namespace armarx;
using namespace rtfilters;


rtfilters::MedianFilter::MedianFilter(size_t windowSize)
    : RTFilterBase(windowSize)
{

}

double armarx::rtfilters::MedianFilter::calculate()
{
    std::vector<double> dataVec;
    for (const auto& e : dataHistory)
    {
        dataVec.push_back(e.second);
    }
    std::sort(dataVec.begin(), dataVec.end());
    float result = dataVec.size() % 2 == 0 ? (dataVec.at(dataVec.size() / 2 - 1) + dataVec.at(dataVec.size() / 2)) / 2 : dataVec.at(dataVec.size() / 2);
    return result;
}
