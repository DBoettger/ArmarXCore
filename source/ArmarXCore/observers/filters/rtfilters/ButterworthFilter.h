/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2017, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarX
 * @author     Mirko Waechter( mirko.waechter at kit dot edu)
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#ifndef BUTTERWORTHFILTER_H
#define BUTTERWORTHFILTER_H

#include "RTFilterBase.h"
#include <ArmarXCore/interface/observers/Filters.h>


namespace armarx
{
    namespace rtfilters
    {

        class ButterworthFilter : RTFilterBase
        {
        public:
            ButterworthFilter(double frequency, int sampleRate, PassType filterPassType, double resonance);
            void setInitialValue(double value);
            // RTFilterBase interface
        public:
            double calculate() override;
            double update(const IceUtil::Time& timestamp, double newValue) override;
        protected:
            double newestValue;
            /// Array of input values, latest are in front
            std::vector<double> inputHistory = std::vector<double>(2, 0.0);

            /// Array of output values, latest are in front
            std::vector<double> outputHistory = std::vector<double>(3, 0.0);

            double resonance;

            double frequency;

            int sampleRate;

            ::armarx::PassType filterPassType;

            double c;

            double a1;

            double a2;

            double a3;

            double b1;

            double b2;
        };

    } // namespace rtfilters
} // namespace armarx

#endif // BUTTERWORTHFILTER_H
