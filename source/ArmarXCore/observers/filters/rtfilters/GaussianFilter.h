/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2017, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarX
 * @author     Mirko Waechter( mirko.waechter at kit dot edu)
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#ifndef GAUSSIANFILTER_H
#define GAUSSIANFILTER_H

#include "RTFilterBase.h"



namespace armarx
{
    namespace rtfilters
    {

        class GaussianFilter : public RTFilterBase
        {
        public:
            GaussianFilter(size_t windowSize, double filterWidthInMs);
            GaussianFilter(size_t windowSize, IceUtil::Time filterWidth);

            // RTFilterBase interface
        public:
            double calculate() override;
        protected:
            double filterWidthInSec;
        };

    } // namespace rtfilters
} // namespace armarx

#endif // GAUSSIANFILTER_H
