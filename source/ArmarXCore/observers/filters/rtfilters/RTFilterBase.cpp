/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2017, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarX
 * @author     Mirko Waechter( mirko.waechter at kit dot edu)
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "RTFilterBase.h"

namespace armarx
{
    namespace rtfilters
    {

        RTFilterBase::RTFilterBase(size_t historySize)
        {
            dataHistory.set_capacity(historySize);
        }

        void RTFilterBase::reset()
        {
            dataHistory.clear();
            currentValue = 0.0;
        }

        double RTFilterBase::update(IceUtil::Time const& timestamp, double newValue)
        {
            dataHistory.push_back(std::make_pair(timestamp, newValue));
            currentValue = calculate();
            return currentValue;
        }

        double RTFilterBase::getCurrentValue() const
        {
            return currentValue;
        }

        double armarx::rtfilters::RTFilterBase::update(double deltaSec, double newValue)
        {
            IceUtil::Time timestamp = dataHistory.empty() ? IceUtil::Time::microSeconds(0) : dataHistory.rbegin()->first + IceUtil::Time::secondsDouble(deltaSec);
            return update(timestamp, newValue);
        }

    } // namespace rtfilters
} // namespace armarx
