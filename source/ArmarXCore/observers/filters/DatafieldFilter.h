/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once
#include <ArmarXCore/interface/observers/ObserverInterface.h>
#include <ArmarXCore/observers/variant/Variant.h>
//#include <boost/circular_buffer.hpp> //<-- boost::circular_buffer causes segfaults


namespace armarx
{
    const Ice::Current staticIceCurrent = Ice::Current();
    typedef std::list<std::pair<long, VariantBasePtr> > TimeVariantBaseMap;

    /**
     * @class  DatafieldFilter
     * @ingroup ObserverFilters
     * @brief The DatafieldFilter class is the base class for all filters
     * and filter implementation should derive from this class.
     * For usage explanation see \ref Observers-Filters.
     */
    class DatafieldFilter :
        virtual public DatafieldFilterBase
    {
    public:
        DatafieldFilter();
        DatafieldFilter(DatafieldFilter const& filter);

        // DatafieldFilterBase interface

        /**
         * @brief Adds the given value to the data map, erases old values if maximum size was reached,
         * and calculates the new filtered value.
         * @param timestamp Timestamp at which the data value was taken.
         * @param value The new data value.
         */
        void update(Ice::Long timestamp, const VariantBasePtr& value, const Ice::Current& c = GlobalIceCurrent) override;

        /**
         * @brief Retrieves the current, filtered value.
         * Triggers calculates if if filtered value pointer is empty.
         */
        VariantBasePtr getValue(const Ice::Current& c = GlobalIceCurrent) const override;

        /**
         * @brief Checks whether the given type is supported.
         * @param variantType
         * @return
         */
        bool checkTypeSupport(VariantTypeId variantType, const Ice::Current& c = GlobalIceCurrent) const override;
        armarx::StringFloatDictionary getProperties(const Ice::Current& c = GlobalIceCurrent) const override;
        void setProperties(const armarx::StringFloatDictionary& newValues, const Ice::Current& c = GlobalIceCurrent) override;
        const TimeVariantBaseMap& getDataHistory() const;

    protected:
        TimeVariantBaseMap dataHistory;
        mutable Mutex historyMutex;
    };

} // namespace armarx

