/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include "DatafieldFilter.h"
#include <ArmarXCore/interface/observers/Filters.h>
#include <ArmarXCore/observers/exceptions/user/InvalidTypeException.h>

namespace armarx
{
    namespace filters
    {


        /**
         * @class  AverageFilter
         * @ingroup ObserverFilters
         * @brief The AverageFilter class provides a simple filter by calculating the
         * average value of a window for datafields of type float, int and double.
         */
        class AverageFilter :
            virtual public DatafieldFilter,
            virtual public AverageFilterBase
        {
        public:
            AverageFilter();

            // DatafieldFilterBase interface
        public:
            VariantBasePtr calculate(const Ice::Current& c = GlobalIceCurrent) const override;

            /**
             * @brief This filter supports: Int, Float, Double
             * @return List of VariantTypes
             */
            ParameterTypeList getSupportedTypes(const Ice::Current& c = GlobalIceCurrent) const override;

        private:
            template <typename Type>
            static Type CalcAvg(const TimeVariantBaseMap& map)
            {
                if (map.empty())
                {
                    return 0;
                }
                double sum = 0;

                for (auto& v : map)
                {
                    VariantPtr v2 = VariantPtr::dynamicCast(v.second);
                    sum += v2->get<Type>();
                }

                double result = sum / map.size();
                return result;
            }
        };

    } // namespace filters
} // namespace armarx

