/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarX
 * @author     Mirko Waechter( mirko.waechter at kit dot edu)
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "ButterworthFilter.h"
using namespace armarx;

armarx::filters::ButterworthFilter::ButterworthFilter() {}

void armarx::filters::ButterworthFilter::reset(double frequency, int sampleRate, PassType filterPassType, double resonance)
{
    this->resonance = resonance;
    this->frequency = frequency;
    this->sampleRate = sampleRate;
    this->filterPassType = filterPassType;
    switch (filterPassType)
    {
        case Lowpass:
            c = 1.0 / tan(M_PI * frequency / sampleRate);
            a1 = 1.0 / (1.0 + resonance * c + c * c);
            a2 = 2.0 * a1;
            a3 = a1;
            b1 = 2.0 * (1.0 - c * c) * a1;
            b2 = (1.0 - resonance * c + c * c) * a1;
            break;
        case Highpass:
            c = tan(M_PI * frequency / sampleRate);
            a1 = 1.0 / (1.0 + resonance * c + c * c);
            a2 = -2.0 * a1;
            a3 = a1;
            b1 = 2.0 * (c * c - 1.0) * a1;
            b2 = (1.0 - resonance * c + c * c) * a1;
            break;
        default:
            ARMARX_INFO << "Unknown pass type";

    }
}

armarx::filters::ButterworthFilter::ButterworthFilter(double frequency, int sampleRate, armarx::PassType filterPassType, double resonance)
{
    reset(frequency, sampleRate, filterPassType, resonance);
}

void filters::ButterworthFilter::setInitialValue(double value)
{
    for (auto& v : inputHistory)
    {
        v = value;
    }
    for (auto& v : outputHistory)
    {
        v = value;
    }
}

armarx::ParameterTypeList armarx::filters::ButterworthFilter::getSupportedTypes(const Ice::Current&) const
{
    ParameterTypeList result;
    result.push_back(VariantType::Int);
    result.push_back(VariantType::Float);
    result.push_back(VariantType::Double);
    return result;
}

void armarx::filters::ButterworthFilter::update(Ice::Long, const armarx::VariantBasePtr& value, const Ice::Current&)
{
    //no lock required since no member data is changed
    VariantPtr var = VariantPtr::dynamicCast(value);
    if (!var->getInitialized())
    {
        return;
    }

    double newInput = 0.0;
    auto type = var->getType();
    if (type == VariantType::Float)
    {
        newInput = static_cast<double>(var->getFloat());
    }
    else if (type == VariantType::Double)
    {
        newInput = var->getDouble();
    }
    else if (type == VariantType::Int)
    {
        newInput = static_cast<double>(var->getInt());
    }
    update(newInput);
}

void armarx::filters::ButterworthFilter::update(double newInput)
{
    ScopedLock lock(historyMutex);
    double newOutput = a1 * newInput + a2 * this->inputHistory[0] + a3 * this->inputHistory[1] - b1 * this->outputHistory[0] - b2 * this->outputHistory[1];

    this->inputHistory[1] = this->inputHistory[0];
    this->inputHistory[0] = newInput;

    this->outputHistory[2] = this->outputHistory[1];
    this->outputHistory[1] = this->outputHistory[0];
    this->outputHistory[0] = newOutput;
}

armarx::VariantBasePtr armarx::filters::ButterworthFilter::getValue(const Ice::Current&) const
{
    ScopedLock lock(historyMutex);

    return new Variant(outputHistory[0]);
}

double armarx::filters::ButterworthFilter::getRawValue() const
{
    ScopedLock lock(historyMutex);
    return outputHistory[0];
}

armarx::VariantBasePtr armarx::filters::ButterworthFilter::calculate(const Ice::Current&) const
{
    ScopedLock lock(historyMutex);

    return new Variant(outputHistory[0]);
}


StringFloatDictionary armarx::filters::ButterworthFilter::getProperties(const Ice::Current&) const
{
    StringFloatDictionary props;
    props["resonance"] = resonance;
    props["frequency"] = frequency;
    props["sampleRate"] = sampleRate;
    props["filterPassType"] = (int)(filterPassType);
    return props;
}

void armarx::filters::ButterworthFilter::setProperties(const StringFloatDictionary& newValues, const Ice::Current&)
{
    auto it = newValues.find("resonance");
    if (it != newValues.end())
    {
        resonance = it->second;
    }

    it = newValues.find("frequency");
    if (it != newValues.end())
    {
        frequency = it->second;
    }

    it = newValues.find("sampleRate");
    if (it != newValues.end())
    {
        sampleRate = it->second;
    }

    it = newValues.find("filterPassType");
    if (it != newValues.end())
    {
        filterPassType = static_cast<PassType>(it->second);
    }
    reset(frequency, sampleRate, filterPassType, resonance);
}
