/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2015
* @copyright  http://www.gnu.org/licenses/gpl.txt
*             GNU General Public License
*/

#pragma once

#include "DatafieldFilter.h"
#include <ArmarXCore/interface/observers/Filters.h>
#include <ArmarXCore/observers/exceptions/user/InvalidTypeException.h>

namespace armarx
{
    namespace filters
    {


        /**
         * @class  MaxFilter
         * @ingroup ObserverFilters
         * @brief The MaxFilter class provides a simple filter by calculating the
         * average value of a window for datafields of type float, int and double.
         */
        class MaxFilter :
            public DatafieldFilter,
            public MaxFilterBase
        {
        public:
            MaxFilter();

            // DatafieldFilterBase interface
        public:
            VariantBasePtr calculate(const Ice::Current& c = GlobalIceCurrent) const override;

            /**
             * @brief This filter supports: Int, Float, Double
             * @return List of VariantTypes
             */
            ParameterTypeList getSupportedTypes(const Ice::Current& c = GlobalIceCurrent) const override;
        private:
            template <typename Type>
            static Type CalcMax(const TimeVariantBaseMap& map)
            {
                double curMax = std::numeric_limits<double>::min();
                for (auto v : map)
                {
                    VariantPtr v2 = VariantPtr::dynamicCast(v.second);
                    Type val = v2->get<Type>();

                    if (val > curMax)
                    {
                        curMax = val;
                    }
                }
                return curMax;
            }
        };

    } // namespace filters
} // namespace armarx


