/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Core
* @author     Kai Welke (welke _at_ kit _dot_ edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txtg
*             GNU General Public License
*/

#include "SystemObserver.h"
#include <ArmarXCore/observers/checks/ConditionCheckUpdated.h>
#include <ArmarXCore/observers/checks/ConditionCheckValid.h>
#include <ArmarXCore/observers/checks/ConditionCheckEquals.h>
#include <ArmarXCore/observers/checks/ConditionCheckInRange.h>
#include <ArmarXCore/observers/checks/ConditionCheckLarger.h>
#include <ArmarXCore/observers/checks/ConditionCheckSmaller.h>
#include <ArmarXCore/observers/variant/ChannelRef.h>
#include <ArmarXCore/observers/ObserverObjectFactories.h>
#include <ArmarXCore/core/exceptions/Exception.h>
#include <ArmarXCore/core/time/TimeUtil.h>
#include <IceUtil/Time.h>

using namespace armarx;



// ********************************************************************
// observer framework hooks
// ********************************************************************
void SystemObserver::onInitObserver()
{
    maxTimerId = 0;
    periodicTask = new PeriodicTask<SystemObserver>(this, &SystemObserver::runTimerTask, 1, false, "SystemObserver", false);

    // register all checks
    offerConditionCheck(checks::SystemObserver::updated->getCheckStr(), new ConditionCheckUpdated());
    offerConditionCheck(checks::SystemObserver::valid->getCheckStr(), new ConditionCheckValid());
    offerConditionCheck(checks::SystemObserver::equals->getCheckStr(), new ConditionCheckEquals());
    offerConditionCheck(checks::SystemObserver::inrange->getCheckStr(), new ConditionCheckInRange());
    offerConditionCheck(checks::SystemObserver::larger->getCheckStr(), new ConditionCheckLarger());
    offerConditionCheck(checks::SystemObserver::smaller->getCheckStr(), new ConditionCheckSmaller());
}

void SystemObserver::onConnectObserver()
{
    periodicTask->start();
}

void SystemObserver::onExitObserver()
{
    periodicTask->stop();
}

void SystemObserver::runTimerTask()
{
    boost::mutex::scoped_lock lock(timersMutex);
    SystemObserverTimerMap::iterator iter = timers.begin();

    while (iter != timers.end())
    {
        try
        {
            // update the timer
            updateTimer(iter->second);

            // update data field and channel
            setDataField(iter->second.timerName, "elapsedMs", int(iter->second.elapsedMs));
            updateChannel(iter->second.timerName);
        }
        catch (armarx::LocalException& e)
        {
            ARMARX_LOG << eFATAL << e.what();
        }
        catch (armarx::UserException& e)
        {
            ARMARX_LOG << eFATAL << e.reason;
        }

        iter++;
    }

}

// ********************************************************************
// timers interface implementation
// ********************************************************************
ChannelRefBasePtr SystemObserver::startTimer(const std::string& timerBaseName, const Ice::Current& c)
{
    std::stringstream temp;
    temp << ++maxTimerId << "_" << timerBaseName;
    std::string timerName = temp.str();

    boost::mutex::scoped_lock lock(timersMutex);

    if (timers.find(timerName) != timers.end())
    {
        std::string reason = "Timer " + timerName + " already exists";
        throw InvalidTimerException(reason.c_str());
    }

    // create new timer
    SystemObserverTimer timer;
    timer.timerName = timerName;
    resetTimer(timer);

    std::pair<std::string, SystemObserverTimer> entry;
    entry.first = timerName;
    entry.second = timer;

    timers.insert(entry);

    boost::recursive_mutex::scoped_lock registryLock(channelsMutex);

    // add channel and datafield
    offerChannel(timerName, "Timer " + timerName);
    offerDataFieldWithDefault(timerName, "elapsedMs", int(0), "Elapsed milliseconds of Timer " + timerName);
    updateChannel(timerName);

    // return datafield identifier for easy condition installation
    ChannelRefBasePtr channelRef = new ChannelRef(this, timerName);

    return channelRef;
}

void SystemObserver::resetTimer(const ChannelRefBasePtr& timer, const Ice::Current& c)
{
    boost::mutex::scoped_lock lock(timersMutex);
    std::string timerName = ChannelRefPtr::dynamicCast(timer)->getChannelName();

    SystemObserverTimerMap::iterator iter = timers.find(timerName);

    if (iter != timers.end())
    {
        resetTimer(iter->second);
    }
    else
    {
        std::string reason = "Timer " + timerName + " unknown";
        throw InvalidTimerException(reason.c_str());
    }
}

void SystemObserver::pauseTimer(const ChannelRefBasePtr& timer, const Ice::Current& c)
{
    boost::mutex::scoped_lock lock(timersMutex);
    std::string timerName = ChannelRefPtr::dynamicCast(timer)->getChannelName();

    SystemObserverTimerMap::iterator iter = timers.find(timerName);

    if (iter != timers.end())
    {
        iter->second.paused = true;
    }
    else
    {
        std::string reason = "Timer " + timerName + " unknown";
        throw InvalidTimerException(reason.c_str());
    }
}

void SystemObserver::unpauseTimer(const ChannelRefBasePtr& timer, const Ice::Current& c)
{
    boost::mutex::scoped_lock lock(timersMutex);
    std::string timerName = ChannelRefPtr::dynamicCast(timer)->getChannelName();

    SystemObserverTimerMap::iterator iter = timers.find(timerName);

    if (iter != timers.end())
    {
        iter->second.paused = false;
        iter->second.startTimeMs += getElapsedTimeMs(iter->second.startTimeMs + iter->second.elapsedMs);
    }
    else
    {
        std::string reason = "Timer " + timerName + " unknown";
        throw InvalidTimerException(reason.c_str());
    }
}

void SystemObserver::removeTimer(const ChannelRefBasePtr& timer, const Ice::Current& c)
{
    boost::mutex::scoped_lock lock(timersMutex);
    std::string timerName = ChannelRefPtr::dynamicCast(timer)->getChannelName();

    SystemObserverTimerMap::iterator iter = timers.find(timerName);

    if (iter != timers.end())
    {
        removeChannel(timerName);
        timers.erase(iter);
    }
}



// ********************************************************************
// counters implementation
// ********************************************************************
ChannelRefBasePtr SystemObserver::startCounter(int initialValue, const std::string& counterBaseName, const Ice::Current& c)
{
    std::stringstream temp;
    temp << ++maxCounterId << "_" << counterBaseName;
    std::string counterName = temp.str();

    boost::mutex::scoped_lock lock(countersMutex);

    if (counters.find(counterName) != counters.end())
    {
        std::string reason = "Counter " + counterName + " already exists";
        throw InvalidCounterException(reason.c_str());
    }



    // create new counter
    SystemObserverCounter counter;
    counter.counterName = counterName;
    counter.value = initialValue;

    std::pair<std::string, SystemObserverCounter> entry;
    entry.first = counterName;
    entry.second = counter;


    SystemObserverCounterMap::iterator iter;
    iter = (counters.insert(entry)).first;


    // add channel and datafield
    offerChannel(counterName, "Counter " + counterName);
    offerDataField(counterName, "value", VariantType::Int, "Current value of counter " + counterName);

    updateCounter(iter);

    // return datafield identifier for easy condition installation
    return new ChannelRef(this, counterName);

}

int SystemObserver::incrementCounter(const ChannelRefBasePtr& counter, const Ice::Current& c)
{
    boost::mutex::scoped_lock lock(countersMutex);
    std::string counterName = ChannelRefPtr::dynamicCast(counter)->getChannelName();

    SystemObserverCounterMap::iterator iter = counters.find(counterName);

    if (iter != counters.end())
    {
        iter->second.value++;
    }
    else
    {
        std::string reason = "Counter " + counterName + " unknown";
        throw InvalidCounterException(reason.c_str());
    }

    updateCounter(iter);
    return iter->second.value;
}

int SystemObserver::decrementCounter(const ChannelRefBasePtr& counter, const Ice::Current& c)
{
    boost::mutex::scoped_lock lock(countersMutex);
    std::string counterName = ChannelRefPtr::dynamicCast(counter)->getChannelName();

    SystemObserverCounterMap::iterator iter = counters.find(counterName);

    if (iter != counters.end())
    {
        iter->second.value--;
    }
    else
    {
        std::string reason = "Counter " + counterName + " unknown";
        throw InvalidCounterException(reason.c_str());
    }

    updateCounter(iter);
    return iter->second.value;
}

void SystemObserver::resetCounter(const ChannelRefBasePtr& counter, const Ice::Current& c)
{
    boost::mutex::scoped_lock lock(countersMutex);
    std::string counterName = ChannelRefPtr::dynamicCast(counter)->getChannelName();

    SystemObserverCounterMap::iterator iter = counters.find(counterName);

    if (iter != counters.end())
    {
        iter->second.value = 0;
    }
    else
    {
        std::string reason = "Counter " + counterName + " unknown";
        throw InvalidCounterException(reason.c_str());
    }

    updateCounter(iter);
}

void SystemObserver::setCounter(const ChannelRefBasePtr& counter, int counterValue, const Ice::Current& c)
{
    boost::mutex::scoped_lock lock(countersMutex);
    std::string counterName = ChannelRefPtr::dynamicCast(counter)->getChannelName();

    SystemObserverCounterMap::iterator iter = counters.find(counterName);

    if (iter != counters.end())
    {
        iter->second.value = counterValue;
    }
    else
    {
        std::string reason = "Counter " + counterName + " unknown";
        throw InvalidCounterException(reason.c_str());
    }

    updateCounter(iter);
}

void SystemObserver::removeCounter(const ChannelRefBasePtr& counter, const Ice::Current& c)
{
    boost::mutex::scoped_lock lock(countersMutex);
    std::string counterName = ChannelRefPtr::dynamicCast(counter)->getChannelName();

    SystemObserverCounterMap::iterator iter = counters.find(counterName);

    if (iter != counters.end())
    {
        removeChannel(counterName);
        counters.erase(iter);
    }
}




// ********************************************************************
// private methods
// ********************************************************************
void SystemObserver::resetTimer(SystemObserverTimer& timer)
{
    timer.startTimeMs = getCurrentTimeMs();
    timer.elapsedMs = getElapsedTimeMs(timer.startTimeMs);
    timer.paused = false;
}

void SystemObserver::updateTimer(SystemObserverTimer& timer)
{
    if (!timer.paused)
    {
        timer.elapsedMs = getElapsedTimeMs(timer.startTimeMs);
    }
}

int SystemObserver::getCurrentTimeMs()
{
    IceUtil::Time current = TimeUtil::GetTime();

    return current.toMilliSeconds();
}

int SystemObserver::getElapsedTimeMs(int referenceTimeMs)
{
    IceUtil::Time current = TimeUtil::GetTime();
    IceUtil::Time reference = IceUtil::Time::milliSeconds(referenceTimeMs);
    IceUtil::Time interval = current - reference;

    return interval.toMilliSeconds();
}


void SystemObserver::updateCounter(SystemObserverCounterMap::iterator& iterCounter)
{
    // update data field and channel
    setDataField(iterCounter->second.counterName, "value", iterCounter->second.value);
    updateChannel(iterCounter->second.counterName);
}
