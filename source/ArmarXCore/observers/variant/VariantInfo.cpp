/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "VariantInfo.h"

#include <ArmarXCore/core/system/cmake/CMakePackageFinder.h>
#include <boost/algorithm/string.hpp>
#include <boost/filesystem.hpp>
#include <ArmarXCore/core/logging/Logging.h>
#include <ArmarXCore/interface/observers/VariantContainers.h>
#include <ArmarXCore/observers/variant/SingleTypeVariantList.h>
#include <ArmarXCore/observers/variant/StringValueMap.h>
#include <ArmarXCore/observers/variant/VariantContainer.h>
#include <ArmarXCore/core/system/DynamicLibrary.h>
#include <ArmarXCore/core/exceptions/local/ExpressionException.h>

using namespace armarx;

VariantInfo::VariantInfo()
{
}

std::vector<VariantInfo::LibEntryPtr> VariantInfo::readVariantInfo(RapidXmlReaderPtr reader, const std::string& packagePath, const std::string& packageName)
{
    std::vector<LibEntryPtr> libs;
    if (packagePaths.find(packagePath) != packagePaths.end())
    {
        ARMARX_DEBUG_S << "path '" << packagePath << "' already in there.";
        return libs;
    }

    packagePaths[(packagePath)] = packageName;
    RapidXmlReaderNode node = reader->getRoot("VariantInfo");
    for (RapidXmlReaderNode libNode = node.first_node("Lib"); libNode.is_valid(); libNode = libNode.next_sibling("Lib"))
    {
        LibEntryPtr lib(new LibEntry(libNode, packageName));
        this->libs.push_back(lib);
        libs.push_back(lib);

        for (VariantEntryPtr e : lib->variants)
        {
            variantToLibMap.insert(std::make_pair(e->baseTypeName, lib));
            variantMap.insert(std::make_pair(e->baseTypeName, e));
            humanNameToVariantMap.insert(std::make_pair(e->humanName, e));
        }

        for (ProxyEntryPtr p : lib->proxies)
        {
            proxyMap.insert(std::make_pair(lib->name + "." + p->memberName, p));
        }
    }
    return libs;
}

VariantInfo::LibEntryPtr VariantInfo::findLibByVariant(std::string variantTypeName) const
{
    std::map<std::string, LibEntryPtr>::const_iterator it = variantToLibMap.find(variantTypeName);

    if (it == variantToLibMap.end())
    {
        return LibEntryPtr();
    }
    else
    {
        return it->second;
    }
}

VariantInfo::LibEntryPtr VariantInfo::findLibByProxy(std::string proxyTypeName) const
{
    for (VariantInfo::LibEntryPtr lib : getLibs())
    {
        std::string libName = lib->getName();
        for (VariantInfo::ProxyEntryPtr proxy : lib->getProxies())
        {
            if (proxy->getMemberName() == proxyTypeName)
            {
                return lib;
            }

        }
    }
    return LibEntryPtr();
}

Ice::StringSeq VariantInfo::findLibNames(const Ice::StringSeq& variantTypeNames, const Ice::StringSeq& proxyTypeNames) const
{
    Ice::StringSeq result;
    for (const LibEntryPtr& lib : findLibs(variantTypeNames, proxyTypeNames))
    {
        if (lib)
        {
            result.push_back(lib->getName());
        }
    }
    return result;
}

std::set<VariantInfo::LibEntryPtr> VariantInfo::findLibs(const Ice::StringSeq& variantTypeNames, const Ice::StringSeq& proxyTypeNames) const
{
    std::set<VariantInfo::LibEntryPtr> result;
    for (auto& var : variantTypeNames)
    {
        auto lib = findLibByVariant(var);
        if (lib)
        {
            result.insert(lib);
        }
    }
    for (auto& proxy : proxyTypeNames)
    {
        auto lib = findLibByProxy(proxy);
        if (lib)
        {
            result.insert(lib);
        }
    }
    return result;
}

std::string VariantInfo::getDataTypeName(std::string variantBaseTypeName)
{
    std::map<std::string, VariantEntryPtr>::const_iterator it = variantMap.find(variantBaseTypeName);

    if (it == variantMap.end())
    {
        return "NOT_FOUND";
    }
    else
    {
        return it->second->dataTypeName;
    }
}

std::string VariantInfo::getReturnTypeName(std::string variantBaseTypeName)
{
    std::map<std::string, VariantEntryPtr>::const_iterator it = variantMap.find(variantBaseTypeName);

    if (it == variantMap.end())
    {
        return "NOT_FOUND";
    }
    else
    {
        // NOTE: Here a typedef in the form of "typedef IceInternal::Handle<[VariantDataType]> [VariantDataType]Ptr;" is assumed.
        // If this typedef is missing, the statechart code generator WILL GENERATE INVALID CODE.
        VariantEntryPtr e = it->second;
        return e->basic ? e->dataTypeName : e->dataTypeName + "Ptr";
    }
}

bool VariantInfo::isBasic(std::string variantBaseTypeName)
{
    std::map<std::string, VariantEntryPtr>::const_iterator it = variantMap.find(variantBaseTypeName);

    if (it == variantMap.end())
    {
        return false;
    }
    else
    {
        VariantEntryPtr e = it->second;
        return e->basic;
    }
}

std::vector<VariantInfo::LibEntryPtr> VariantInfo::getLibs() const
{
    return libs;
}

VariantInfo::ProxyEntryPtr VariantInfo::getProxyEntry(std::string proxyId)
{
    std::map<std::string, ProxyEntryPtr>::const_iterator it = proxyMap.find(proxyId);

    if (it == proxyMap.end())
    {
        return ProxyEntryPtr();
    }
    else
    {
        return it->second;
    }
}

VariantInfo::VariantEntryPtr VariantInfo::getVariantByName(std::string variantBaseTypeName)
{
    std::map<std::string, VariantEntryPtr>::const_iterator it = variantMap.find(variantBaseTypeName);

    if (it == variantMap.end())
    {
        return VariantEntryPtr();
    }
    else
    {
        return it->second;
    }
}

VariantInfo::VariantEntryPtr VariantInfo::getVariantByHumanName(std::string humanName)
{
    std::map<std::string, VariantEntryPtr>::const_iterator it = humanNameToVariantMap.find(humanName);

    if (it == humanNameToVariantMap.end())
    {
        return VariantEntryPtr();
    }
    else
    {
        return it->second;
    }
}

std::string VariantInfo::getNestedHumanNameFromBaseName(std::string variantBaseTypeName)
{
    ContainerTypePtr containerInfo = VariantContainerType::FromString(variantBaseTypeName);
    std::stringstream containers;
    std::stringstream parenthesis;

    while (containerInfo->subType)
    {
        if (containerInfo->typeId == SingleTypeVariantList::getTypePrefix())
        {
            containers << "List(";
        }
        else if (containerInfo->typeId == StringValueMap::getTypePrefix())
        {
            containers << "Map(";
        }
        else
        {
            return "";
        }

        parenthesis << ")";
        containerInfo = containerInfo->subType;
    }

    VariantEntryPtr entry = getVariantByName(containerInfo->typeId);

    if (!entry)
    {
        return "";
    }

    return containers.str() + entry->humanName + parenthesis.str();
}

std::string VariantInfo::getNestedBaseNameFromHumanName(std::string humanName)
{
    ContainerTypePtr containerInfo = VariantContainerType::FromString(humanName);
    std::stringstream containers;
    std::stringstream parenthesis;

    while (containerInfo->subType)
    {
        if (containerInfo->typeId == "List")
        {
            containers << SingleTypeVariantList::getTypePrefix() << "(";
        }
        else if (containerInfo->typeId == "Map")
        {
            containers << StringValueMap::getTypePrefix() << "(";
        }
        else
        {
            return "";
        }

        parenthesis << ")";
        containerInfo = containerInfo->subType;
    }

    VariantEntryPtr entry = getVariantByHumanName(containerInfo->typeId);

    if (!entry)
    {
        return "";
    }

    return containers.str() + entry->baseTypeName + parenthesis.str();
}

const std::map<std::string, std::string>& VariantInfo::getPackagePaths() const
{
    return packagePaths;
}

bool VariantInfo::isPackageLoaded(const std::string packageName) const
{
    for (const auto& p : packagePaths)
    {
        if (p.second == packageName)
        {
            return true;
        }
    }
    return false;
}

std::string VariantInfo::getDebugInfo() const
{
    std::stringstream ss;

    for (const LibEntryPtr& e : libs)
    {
        ss << e->getName() << "\n";

        for (const VariantEntryPtr& v : e->variants)
        {
            ss << "  " << v->getHumanName() << ": " << v->dataTypeName << "\n";
        }

        for (const ProxyEntryPtr& p : e->getProxies())
        {
            ss << "  " << p->getHumanName() << ": " << p->getIncludePath() << "\n";
        }
    }

    return ss.str();
}

VariantInfoPtr VariantInfo::ReadInfoFilesRecursive(const std::string& rootPackageName, const std::string& rootPackagePath, bool showErrors, VariantInfoPtr variantInfo)
{
    if (!variantInfo)
    {
        variantInfo.reset(new VariantInfo());
    }

    auto finder = CMakePackageFinder(rootPackageName, rootPackagePath, false, true);
    ARMARX_CHECK_EXPRESSION_W_HINT(finder.packageFound(), rootPackageName << " at " << "'" << rootPackagePath << "'\n");

    boost::filesystem::path variantInfoFile(finder.getDataDir().c_str());
    variantInfoFile /= rootPackageName;
    variantInfoFile /= "VariantInfo-" + rootPackageName + ".xml";

    if (boost::filesystem::exists(variantInfoFile))
    {
        if (!variantInfo->isPackageLoaded(rootPackageName))
        {
            try
            {
                RapidXmlReaderPtr xmlReader = RapidXmlReader::FromFile(variantInfoFile.string());
                variantInfo->readVariantInfo(xmlReader, rootPackagePath, rootPackageName);
                ARMARX_DEBUG_S << "Read " << variantInfoFile.string();
            }
            catch (std::exception& e)
            {
                ARMARX_ERROR_S << "Reading " << variantInfoFile.string() << " failed: " << e.what();
            }
        }
    }
    else if (showErrors)
    {
        ARMARX_WARNING_S << "VariantInfo File not found for project " << rootPackageName << ": " << variantInfoFile.string();
    }

    if (finder.packageFound())
    {
        auto depPaths = finder.getDependencyPaths();

        for (auto& depPath : depPaths)
        {
            if (variantInfo->getPackagePaths().find(depPath.second) != variantInfo->getPackagePaths().end())
            {
                continue;
            }

            if (!depPath.first.empty() && !depPath.second.empty())
            {
                std::string packagePath;

                if (!depPath.second.empty() && boost::filesystem::exists(depPath.second))
                {
                    packagePath = depPath.second;
                }

                variantInfo = ReadInfoFilesRecursive(depPath.first, packagePath, showErrors, variantInfo);
            }
        }
    }

    return variantInfo;
}

VariantInfoPtr VariantInfo::ReadInfoFiles(const std::vector<std::string>& packages, bool showErrors, bool throwOnError)
{
    VariantInfoPtr variantInfo(new VariantInfo());

    for (std::string package : packages)
    {
        auto finder = CMakePackageFinder(package);
        if (throwOnError)
        {
            ARMARX_CHECK_EXPRESSION_W_HINT(finder.packageFound(), package);
        }
        boost::filesystem::path variantInfoFile(finder.getDataDir().c_str());
        variantInfoFile /= package;
        variantInfoFile /= "VariantInfo-" + package + ".xml";

        if (boost::filesystem::exists(variantInfoFile))
        {
            try
            {
                RapidXmlReaderPtr xmlReader = RapidXmlReader::FromFile(variantInfoFile.string());
                std::vector<LibEntryPtr> libs = variantInfo->readVariantInfo(xmlReader, finder.getConfigDir(), package);
                ARMARX_INFO_S << "Read " << variantInfoFile.string();
                std::stringstream ss;
                for (LibEntryPtr e : libs)
                {
                    ss << "Lib " << e->getName() << "\n";

                    for (const VariantEntryPtr& v : e->variants)
                    {
                        ss << "  Variant " << v->getHumanName() << ": " << v->dataTypeName << "\n";
                    }

                    for (const ProxyEntryPtr& p : e->getProxies())
                    {
                        ss << "  Proxy " << p->getHumanName() << ": " << p->getIncludePath() << "\n";
                    }
                }
                ARMARX_INFO_S << "VariantInfo for " << package << ":\n" << ss.str();
            }
            catch (std::exception& e)
            {
                ARMARX_ERROR_S << "Reading " << variantInfoFile.string() << " failed: " << e.what();
            }
        }
        else if (showErrors)
        {
            ARMARX_WARNING_S << "VariantInfo File not found for project " << package << ": " << variantInfoFile.string();
        }
    }

    return variantInfo;
}

DynamicLibraryPtr VariantInfo::loadLibraryOfVariant(std::string variantTypeName) const
{
    DynamicLibraryPtr result(new DynamicLibrary());
    auto lib = findLibByVariant(variantTypeName);
    if (!lib)
    {
        ARMARX_WARNING << "Could not find lib for variant " << variantTypeName;
        return DynamicLibraryPtr();
    }
    ARMARX_INFO_S << "Loading lib " << lib->getAbsoluteLibPath();
    result->load(lib->getAbsoluteLibPath());
    if (result->isLibraryLoaded())
    {
        return result;
    }
    else
    {
        return DynamicLibraryPtr();
    }
}




VariantInfo::LibEntry::LibEntry(RapidXmlReaderNode node, const std::string& packageName) :
    packageName(packageName)
{
    this->name = node.attribute_value("name");

    for (RapidXmlReaderNode includeNode = node.first_node("VariantFactory"); includeNode.is_valid(); includeNode = includeNode.next_sibling("VariantFactory"))
    {
        factoryIncludes.push_back(includeNode.attribute_value("include"));
    }

    for (RapidXmlReaderNode variantNode = node.first_node("Variant"); variantNode.is_valid(); variantNode = variantNode.next_sibling("Variant"))
    {
        VariantEntryPtr entry(new VariantEntry(variantNode));
        variants.push_back(entry);
    }

    for (RapidXmlReaderNode proxyNode : node.nodes())
    {
        if (proxyNode.name() == "Proxy" || proxyNode.name() == "Topic")
        {
            ProxyEntryPtr proxy(new ProxyEntry(proxyNode));
            proxies.push_back(proxy);
        }
    }
}

std::vector<std::string> VariantInfo::LibEntry::getFactoryIncludes() const
{
    return factoryIncludes;
}

std::vector<std::string> VariantInfo::LibEntry::getVariantIncludes(const std::string& variantBaseTypeName) const
{
    auto it = std::find_if(variants.begin(), variants.end(), [&](const VariantEntryPtr & entry)
    {
        return entry->getBaseTypeName() == variantBaseTypeName;
    });
    if (it != variants.end() && (*it)->getIncludePath())
    {
        return {*(*it)->getIncludePath()};
    }
    else
    {
        return factoryIncludes;
    }
}

std::string VariantInfo::LibEntry::getPackageName() const
{
    return packageName;
}

std::string VariantInfo::LibEntry::getAbsoluteLibPath() const
{
    CMakePackageFinder finder(packageName);
    if (!finder.packageFound())
    {
        throw LocalException() << "Could not find package '" << packageName << "'!";
    }
    if (finder.getLibraryPaths().empty())
    {
        throw LocalException() << "Library path for package '" << packageName << "' is empty!";
    }
    boost::filesystem::path path = finder.getLibraryPaths();
    path /= "lib" + getName() + "." + DynamicLibrary::GetSharedLibraryFileExtension();
    if (!boost::filesystem::exists(path))
    {
        throw LocalException() << "Library path '" << path.string() << "' does not exist!";
    }
    return path.string();
}

std::string VariantInfo::LibEntry::getName() const
{
    return name;
}

std::vector<VariantInfo::ProxyEntryPtr> VariantInfo::LibEntry::getProxies() const
{
    return proxies;
}

const std::vector<VariantInfo::VariantEntryPtr>& VariantInfo::LibEntry::getVariants() const
{
    return variants;
}

VariantInfo::VariantEntry::VariantEntry(RapidXmlReaderNode node)
{
    baseTypeName = node.attribute_value("baseType");
    dataTypeName = node.attribute_value("dataType");
    humanName = node.attribute_value("humanName");
    includePath = node.has_attribute("include") ? boost::optional<std::string>(node.attribute_value("include")) : boost::optional<std::string>();
    basic = node.attribute_as_optional_bool("basic", "true", "false", false);
}

const std::string& VariantInfo::VariantEntry::getHumanName() const
{
    return humanName;
}

const std::string& VariantInfo::VariantEntry::getBaseTypeName() const
{
    return baseTypeName;
}

const std::string& VariantInfo::VariantEntry::getDataTypeName() const
{
    return dataTypeName;
}

const boost::optional<std::string>& VariantInfo::VariantEntry::getIncludePath() const
{
    return includePath;
}


void VariantInfo::ProxyEntry::readVector(RapidXmlReaderNode node, const char* name, std::vector<std::string>& vec)
{
    for (RapidXmlReaderNode n : node.nodes(name))
    {
        vec.push_back(n.value());
    }
}

VariantInfo::ProxyEntry::ProxyEntry(RapidXmlReaderNode node)
{
    includePath = node.attribute_value("include");
    humanName = node.attribute_value("humanName");
    typeName = node.attribute_value("typeName");
    memberName = node.attribute_value("memberName");
    getterName = node.attribute_value("getterName");
    propertyName = node.attribute_value("propertyName");
    propertyIsOptional = node.attribute_as_optional_bool("propertyIsOptional", "true", "false", false);
    propertyDefaultValue = node.attribute_value_or_default("propertyDefaultValue", "");
    proxyType = node.name() == "Topic" ? Topic : SingleProxy;

    readVector(node, "include", includes);
    readVector(node, "library", libraries);
    readVector(node, "member", members);
    readVector(node, "onInit", onInit);
    readVector(node, "onConnect", onConnect);

    for (RapidXmlReaderNode n : node.nodes("method"))
    {
        methods.push_back(std::make_pair(n.attribute_value("header"), n.value()));
    }

    for (RapidXmlReaderNode n : node.nodes("stateMethod"))
    {
        stateMethods.push_back(std::make_pair(n.attribute_value("header"), n.value()));
    }
}
