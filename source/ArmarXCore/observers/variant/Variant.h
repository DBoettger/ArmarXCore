/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarXCore::core
* @author     Kai Welke (welke@kit.edu)
* @date       2011
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

// ArmarX
#include <ArmarXCore/core/system/ImportExport.h>
#include <ArmarXCore/interface/observers/VariantBase.h>
#include <ArmarXCore/interface/observers/VariantContainers.h>
#include <ArmarXCore/observers/exceptions/user/NotInitializedException.h>
#include <ArmarXCore/core/system/SafeShared.h>
#include <ArmarXCore/core/logging/Logging.h>

// Ice
#include <IceUtil/Handle.h>

// boost
#include <boost/utility/enable_if.hpp>
#include <boost/type_traits/is_base_of.hpp>

#include <string>
#include <map>

#include <cxxabi.h>


namespace armarx
{

    class Variant;
    typedef IceInternal::Handle<Variant> VariantPtr;

    typedef Ice::Int VariantTypeId;

    /**
    @page Variants Variants
    @tableofcontents

    Variants provide a mechanism to store several types of data in one construct and send them via \ref ice "Ice".
    The Variant class offers a unified interface for storing and accessing values of several data types.
    It is one of the essential data types of ArmarX and can be used in Ice communication.
    Variants can be extended and several Variant-based implementations of simple or complex data types
    like 6D poses are available in ArmarX.
    @note Complex data types in the variant context are all that are not int, float, bool, double or string.

    @section ArmarXCore-Variants-Usage Basic Variant Usage
    Variants can store instances of all classes that implement the *VariantDataClass* Ice interface.
    For the basic data types *int, bool, float, double* and *std::string* there are three main ways for creating a Variant.

    In the following code three int-Variant instances are created using the different ways of instantiation:

    @snippet ArmarXCore/observers/test/VariantTest.cpp VariantDocumentation Initialization1

    @snippet ArmarXCore/observers/test/VariantTest.cpp VariantDocumentation Initialization2

    @snippet ArmarXCore/observers/test/VariantTest.cpp VariantDocumentation Initialization3

    This works analogously for the other basic data types.
    The setting of complex datatypes work similarly:

    @snippet ArmarXCore/observers/test/VariantTest.cpp TestComplexFloat Usage
    @note The `TestComplexFloat` is from the HowTo @ref ArmarXCore-HowTos-CustomVariant "How to Create Custom Variant Types".

    The stored values can be retrieved using the respective getter methods:

    @snippet ArmarXCore/observers/test/VariantTest.cpp VariantDocumentation Retrieval

    Or for complex types:

    @snippet ArmarXCore/observers/test/VariantTest.cpp TestComplexFloat Usage2

    Note that after assigning a value to a variant and therefore after initially determining the Variant's type,
    changing the type is not possible anymore. Any assignment of data of a different than the set type will result
    in a LocalException.

    There exist several implementations of Variant-types apart from the elementary ones:
     - TimestampVariant for timestamp values in Variants
     - MatrixFloat for matrices in Variants
     - ChannelRef for channel references in Variants
     - DatafieldRef for a reference to a channel's data field
     .
     List of all Variants: \ref VariantsGrp "Variant Group"

    There are further classes that realize flexible containers of Variant-values, which are Variants themselves:
     - VariantContainer, the base class for container types that store Variants
     - StringValueMap for string-Variant mappings
     - SingleTypeVariantList for lists of Variants

     These containers are needed for the same reason as the Variant themselves: We need to be able to send different data via the same Ice interface.
     Since Ice interfaces always have a specific type, we need these classes with a common base class (VariantDataClass and VariantContainerBase).

     Take a look at @ref ArmarXCore-HowTos-CustomVariant "How to Create Custom Variant Types" if you want to implement your own specialized Variant classes.

    @section ArmarXCore-Variants-Serialization Serialization of Variants as JSON
    It is easy to serialize a Variant into a JSON format. Every Variant type has this feature built in.
    You only need the armarx::JSONObject for the serialization and deserialization:

    @snippet ArmarXCore/observers/test/VariantTest.cpp VariantJSON Usage

    The JSON then looks as follows:
    \verbatim
    {
       "value" : {
          "typeName" : "::armarx::FloatVariantData",
          "value" : 3.0
       }
    }
    \endverbatim



    @page ArmarXCore-HowTos-CustomVariant How to Create Custom Variant Types

    Apart from the built-in types, Variants are capable of handling instances of arbitrary types as long as they inherit from VariantDataClass.
    To create a custom Variant type, four steps need to be taken:
     - Write an Ice interface with the basic type description
     - Add a Variant type id for  the new type
     - Write a C++ implementation of the type
     - Add the created type to the object factory registry
     .


    @subsection custom_variants_interface Ice Interface

    The first part of a custom Variant type is an Ice interface that allows using the new type in Ice communication.
    The interface should inherit from VariantDataClass and define the intended data structure.

    In this example, we implement a custom Variant type for complex numbers, defined by a real and an imaginary part, both stored as floats.

    @code
    module armarx
    {
        class ComplexFloatBase extends VariantDataClass
        {
            float real;
            float imag;
        };
    };
    @endcode


    @subsection custom_variants_type_id Type Id


    Add a VariantTypeId declaration somewhere in the variant's header.
    @snippet ArmarXCore/observers/test/VariantTest.cpp TestComplexFloat Variant Type
    @b Note: @a armarx::ComplexFloatBase must be unique in all ArmarX projects.

    @subsection custom_variants_implementation C++ Implementation

    The second part of a custom Variant data type is the C++ implementation of the defined Ice interface ComplexFloatBase.
    There are three types of methods that need to be present in the implementation:
     - Custom data type interface
       - Parameterized constructors
       - getReal, getImag for accessing the data value
       .
     - Standard data type interface
       - ice_clone
       - clone
       - output for printing the data value
       - getType for retrieving the Id of the new type
       - validate for checking the data's sanity
       .
     - Serialization   -
       - serialize, deserialize for serializing to JSON for MemoryX
       .
     .

    @snippet ArmarXCore/observers/test/VariantTest.cpp TestComplexFloat Class Definition

    The next step is to define a pointer type for the new data type:

    @snippet ArmarXCore/observers/test/VariantTest.cpp TestComplexFloat Pointer Type Definition


    @subsection custom_variants_object_factory Object Factory

    Finally, the new type needs to be registered to allow it to be used in Ice communication.
    The factory tells ArmarX, which exact type to instantiate when the respective interface is used in communication.
    In this case, whenever a ComplexFloatBase is transferred via Ice, it is translated into a TestComplexFloat on reception.

    @snippet ArmarXCore/observers/test/VariantTest.cpp TestComplexFloat ObjectFactory

    The variable needs to be initalized in the the cpp-file of the Object Factory like this:

    @snippet ArmarXCore/observers/test/VariantTest.cpp TestComplexFloat ObjectFactoryCPP


    @subsection custom_variants_usage Usage

    The new data type can be used as if it was one of the types already included in ArmarX:

    @snippet ArmarXCore/observers/test/VariantTest.cpp TestComplexFloat Usage

    If you want to receive Variants via Ice (e.g. implement an interface that uses variants),
    the ObjectFactory-header of the library, where the variant is implemented, needs to
    be included and the library needs to be linked.

    \@note If you want to use the variant in the statechart editor, see @ref ArmarXGui-HowTos-Add-DataType-To-StatechartContext

    @defgroup VariantsGrp Variants
    @ingroup ObserversGrp
    @copydoc Variants
    */
    /**
     * @class Variant
     * @ingroup VariantsGrp
     * @brief The Variant class is described here: @ref Variants
     */
    class ARMARXCORE_IMPORT_EXPORT Variant :
        virtual public VariantBase,
        virtual public SafeShared<Variant>
    {
    public:
        Variant();
        Variant(const Variant& source);

        /**
         * Construct a Variant from a non-VariantDataClass instance, e.g. from an int
         *
         * @tparam T            The desired type of the Variant
         * @param var           The initialization value as a T-instance
         * @param t             For type checking only: Do not use
         */
        template <class T>
        Variant(const T& var,
                typename boost::disable_if_c < boost::is_base_of< VariantDataClass, T >::value
                || boost::is_pointer<T>::value >::type* t = nullptr)
        {
            invalidate();
            set<T>(var);
        }

        /**
         * Construct a Variant from a string.
         *
         * @param var           The initialization value
         */
        Variant(char const  var[])
        {
            invalidate();
            setString(var);
        }

        /**
         * Construct a Variant from a reference to a VariantDataClass instance. The VariantDataClass is cloned!
         *
         * @tparam T            The desired type of the Variant
         * @param var           The initialization value as a T-instance
         * @param t             For type checking only: Do not use.
         */
        template <class T>
        Variant(const T& var, typename boost::enable_if<boost::is_base_of< VariantDataClass, T > >::type* t = nullptr)
        {
            invalidate();
            setClass(var);
        }

        /**
         * Construct a Variant from an IceHandle to a VariantDataClass instance. This is a float pointer copy.
         *
         * @tparam T            The desired type of the Variant
         * @param var           The initialization value as a handle to a T-instance
         * @param t             For type checking only: Do not use.
         */
        template <class T>
        Variant(const IceInternal::Handle<T>& var,
                typename boost::enable_if<boost::is_base_of< VariantDataClass, T > >::type* t = nullptr)
        {
            invalidate();
            setClass(var);
        }

        /**
         * Construct a Variant from a pointer to a VariantDataClass instance. The VariantDataClass is cloned!
         *
         * @tparam T            The desired type of the Variant
         * @param var           The initialization value as a pointer to a T-instance
         * @param t             For type checking only: Do not use.
         */
        template <class T>
        Variant(const T* var, typename boost::enable_if<boost::is_base_of< VariantDataClass, T > >::type* t = nullptr)
        {
            invalidate();
            setClass(*var);
        }


        /**
         * Checks if the Variant is initialized and the stored value is valid with respect to the Variant's type.
         *
         * Inherited from VariantBase Ice interface.
         */
        bool validate(const Ice::Current& c = GlobalIceCurrent) const override;

        /**
         * Returns a copy of the Variant.
         */
        virtual VariantPtr clone() const;




        // Setters

        /**
         * Sets the Variant's type to typeId. A Variant's type can not be changed after it has been set.
         *
         * @throws LocalException if a different type has already been set.
         *
         * Inherited from VariantBase Ice interface.
         */
        void setType(VariantTypeId typeId, const Ice::Current& c = GlobalIceCurrent) override;

        /**
         * Sets the Variant's value to n. The Variant's type is fixed to VariantType::Int.
         *
         * @throws InvalidTypeException if a different type has already been set.
         *
         * Inherited from VariantBase Ice interface.
         */
        void setInt(int n, const Ice::Current& c = GlobalIceCurrent) override;

        /**
         * Sets the Variant's value to f. The Variant's type is fixed to VariantType::Float.
         *
         * @throws InvalidTypeException if a different type has already been set.
         *
         * Inherited from VariantBase Ice interface.
         */
        void setFloat(float f, const Ice::Current& c = GlobalIceCurrent) override;

        /**
         * Sets the Variant's value to d. The Variant's type is fixed to VariantType::Double.
         *
         * @throws InvalidTypeException if a different type has already been set.
         *
         * Inherited from VariantBase Ice interface.
         */
        void setDouble(double d, const Ice::Current& c = GlobalIceCurrent) override;

        /**
         * Sets the Variant's value to s. The Variant's type is fixed to VariantType::String.
         *
         * @throws InvalidTypeException if a different type has already been set.
         *
         * Inherited from VariantBase Ice interface.
         */
        void setString(const std::string& s, const Ice::Current& c = GlobalIceCurrent) override;

        /**
         * Sets the Variant's value to b. The Variant's type is fixed to VariantType::Bool.
         *
         * @throws InvalidTypeException if a different type has already been set.
         *
         * Inherited from VariantBase Ice interface.
         */
        void setBool(bool b, const Ice::Current& c = GlobalIceCurrent) override;

        /**
         * Sets the Variant's value to variantDataClass. The Variant's type is fixed to the type of variantDataClass. Flat pointer copy!
         *
         * @throws InvalidTypeException if a different type has already been set.
         *
         * Inherited from VariantBase Ice interface.
         */
        void setClass(const VariantDataClassPtr& variantDataClass);

        /**
         * Sets the Variant's value to variantDataClass. The Variant's type is fixed to the type of variantDataClass. The data class is cloned!
         *
         * @throws InvalidTypeException if a different type has already been set.
         *
         * Inherited from VariantBase Ice interface.
         */
        void setClass(const VariantDataClass& variantDataClass);




        // Getters


        /**
         * Return the Variant's value as int.
         *
         * @throws InvalidTypeException if the Variant's type is not VariantType::Int
         * @throws NotInitializedException if the Variant is uninitialized
         *
         * Inherited from VariantBase Ice interface.
         */
        int getInt(const Ice::Current& c = GlobalIceCurrent) const override;

        /**
         * Return the Variant's value as float.
         *
         * @throws InvalidTypeException if the Variant's type is not VariantType::Float
         * @throws NotInitializedException if the Variant is uninitialized
         *
         * Inherited from VariantBase Ice interface.
         */
        float getFloat(const Ice::Current& c = GlobalIceCurrent) const override;

        /**
         * Return the Variant's value as double.
         *
         * @throws InvalidTypeException if the Variant's type is not VariantType::Double
         * @throws NotInitializedException if the Variant is uninitialized
         *
         * Inherited from VariantBase Ice interface.
         */
        double getDouble(const Ice::Current& c = GlobalIceCurrent) const override;

        /**
         * Return the Variant's value as string.
         *
         * @throws InvalidTypeException if the Variant's type is not VariantType::String
         * @throws NotInitializedException if the Variant is uninitialized
         *
         * Inherited from VariantBase Ice interface.
         */
        std::string getString(const Ice::Current& c = GlobalIceCurrent) const override;

        /**
         * Return the Variant's value as bool.
         *
         * @throws InvalidTypeException if the Variant's type is not VariantType::Bool
         * @throws NotInitializedException if the Variant is uninitialized
         *
         * Inherited from VariantBase Ice interface.
         */
        bool getBool(const Ice::Current& c = GlobalIceCurrent) const override;

        /**
         * Return the Variant's value as TVariantDataClass.
         *
         * @tparam TVariantDataClass       The type for returning the data
         * @throws InvalidTypeException if the Variant's type is not compatible to TVariantDataClass
         * @throws NotInitializedException if the Variant is uninitialized
         */
        template<class TVariantDataClass>
        typename IceInternal::Handle<TVariantDataClass> getClass() const
        {
            if (!getInitialized())
            {
                throw exceptions::user::NotInitializedException();
            }

            IceInternal::Handle<TVariantDataClass> ptr;
            ptr = IceInternal::Handle<TVariantDataClass>::dynamicCast(this->data);

            if (!ptr)
            {
                throw InvalidTypeException("Variant::getClass failed: actual type of value: " + data->ice_id() + " stored type id: " + typeToString(getType()) + ", desired type: " + GetType<TVariantDataClass>());
            }

            return ptr;
        }




        // Helper setter method using templates

        /**
         * Template-based setter for the Variant's value for VariantDataClass-instances.
         *
         * @tparam T            The desired type of the Variant
         *
         * @throws InvalidTypeException if a type other than T has already been set.
         *
         * @returns For type checking only: Do not use
         */
        template<typename T>
        typename boost::enable_if<boost::is_base_of<VariantDataClass, T> >::type
        set(const VariantDataClassPtr& variantDataClass)
        {
            setClass(variantDataClass);
        }

        /**
         * Template-based setter for the Variant's value for non-VariantDataClass-instances.
         * Values not derived from VariantDataClass are not allowed, thus this method always throws an exception.
         *
         * @tparam T            The desired type of the Variant
         *
         * @throws InvalidTypeException
         *
         * @returns For type checking only: Do not use
         */
        template<typename T>
        typename boost::disable_if<boost::is_base_of<VariantDataClass, T> >::type
        set(const T& value)
        {

            BOOST_STATIC_ASSERT_MSG((boost::is_base_of<VariantData, typename T::element_type>::value), "Only basic types or from VariantDataClass derived classes can be template parameters");
            static int invType = hashTypeName(InvalidVariantData::ice_staticId());

            if (getType() == invType)
            {
                setType(hashTypeName(value->ice_id()));
            }
            else if (getType() != hashTypeName(value->ice_id()))
            {
                throw InvalidTypeException();
            }

            data = T::dynamicCast(value->ice_clone());
        }



        // Helper getter method using templates

        /**
         * Template-based getter for the Variant's value.
         *
         * @tparam T Type of the desired value, that inherits from VariantDataClass (e.g: Vector3, LinkedPose, ChannelRef).
         *
         * @throws NotInitializedException if the Variant has not been initialized yet
         * @throws InvalidTypeException if T is not compatible with the Variant's internal type
         *
         * @returns a shared pointer to the desired data type (e.g: Vector3Ptr)
         */
        template<typename T>
        typename boost::enable_if<boost::is_base_of<VariantDataClass, T>, IceInternal::Handle<T> >::type
        get() const
        {
            return getClass<T>();
        }

        template<typename T>
        typename boost::enable_if<boost::is_base_of<VariantDataClass, typename T::element_type>, T>::type
        get() const
        {
            return getClass<typename T::element_type>();
        }

        /**
         * This getter is only called if a wrong template parameter is given (i.e: the type does not inherit from VariantDataClass).
         *
         * @throws InvalidTypeException
         *
         * @returns For type checking only: Do not use
         *
         * @note There exist specializations of this method that allow the basic types bool, int, float, double and std::string
         */
        template<typename T>
        typename boost::enable_if_c < boost::is_pod<T>::value || boost::is_same<std::string, T>::value, T >::type
        get() const
        {

            throw LocalException("This function must not be called directly, it is only for primitive types which have specializations");
        }

        //        template <typename T, typename = typename std::enable_if<!std::is_base_of<VariantDataClass, T>::value >::type>
        //        operator T()
        //        {
        //            return get<T>();
        //        }

        //        template <typename T, typename = typename std::enable_if<std::is_base_of<VariantDataClass, T>::value >::type>
        //        operator T()
        //        {
        //            return *get<T>();
        //        }


        template <typename T>
        operator T()
        {
            return get<T>();
        }
        // Properties

        /**
         * Return the Variant's internal type.
         *
         * @return The Variant's type as a VariantTypeId
         */
        VariantTypeId getType(const Ice::Current& c = GlobalIceCurrent) const override;

        /**
         * Return the Variant's internal type.
         *
         * @return The Variant's type as a string
         */
        std::string getTypeName(const Ice::Current& c = GlobalIceCurrent) const override;

        /**
         * Tells if the Variant is properly initialized.
         */
        bool getInitialized(const Ice::Current& c = GlobalIceCurrent) const override;




        // Operators

        Variant& operator= (int n)
        {
            setInt(n);
            return *this;
        }
        Variant& operator= (float f)
        {
            setFloat(f);
            return *this;
        }
        Variant& operator= (double d)
        {
            setDouble(d);
            return *this;
        }
        Variant& operator= (const std::string& s)
        {
            setString(s);
            return *this;
        }
        Variant& operator= (bool b)
        {
            setBool(b);
            return *this;
        }
        Variant& operator= (const VariantDataClass& prototype);
        Variant& operator= (const VariantDataClassPtr& prototype);
        Variant& operator= (const Variant& prototype);




        // Streaming operator

        friend std::ostream& operator<<(std::ostream& stream, const Variant& rhs)
        {
            rhs.output(stream);
            return stream;
        }

        friend std::ostream& operator<<(std::ostream& stream, const VariantPtr& rhs)
        {
            if (rhs)
            {
                rhs->output(stream);
            }
            else
            {
                stream << "Null VariantPtr";
            }

            return stream;
        }




        // Static tools

        /**
         * Return the name of the registered type typeId.
         *
         * @throws UnknownTypeException if typeId is not registered yet.
         */
        static std::string typeToString(VariantTypeId typeId);

        /**
         * Returns the mapping of currently registered types.
         */
        static const std::map<VariantTypeId, std::string>& getTypes();

        /**
         * Register a new type for the use in a Variant.
         *
         * @throws LocalException if an already registered type has the same hash value as typeName.
         */
        static VariantTypeId addTypeName(const std::string& typeName);

        /**
         * Template-based getter for a type name
         */
        template <typename Type>
        static std::string GetType()
        {
            //            throw LocalException("This type is not implemented!");
            std::string typeName;
            char* demangled = nullptr;
            int status = -1;
            Type* type;
            demangled = abi::__cxa_demangle(typeid(type).name(), nullptr, nullptr, &status);
            typeName = demangled;
            free(demangled);

            if (typeName.size() > 1)
            {
                typeName = typeName.substr(0, typeName.size() - 1);
            }

            return typeName;
        }

        /**
         * Returns the formatted content of the Variant as a string.
         */
        virtual std::string getOutputValueOnly() const;
    protected:
        /**
         * Outputs a formatted representation of the Variant to stream.
         */
        void output(std::ostream& stream) const;
        mutable bool initialized;


    private:

        /**
         * Invalidates the Variant.
         */
        void invalidate();

    public:

        /**
         * Compute and return a hash value for a given type name.
         */
        static int hashTypeName(const std::string& typeName);
    private:

        /**
         * Create and return the internal mapping of registered types.
         */
        static std::map<VariantTypeId, std::string>& types();

    };

    typedef std::map<std::string, Variant> StringVariantMap;

    // *******************************************************
    // Constructors
    // *******************************************************
    template <> Variant::Variant(const int& var, void* t);
    template <> Variant::Variant(const float& var, void* t);
    template <> Variant::Variant(const double& var, void* t);
    template <> Variant::Variant(const bool& var, void* t);
    template <> Variant::Variant(const std::string& var, void* t);

    // *******************************************************
    // Template specialization for getter and setter
    // *******************************************************
    template<> bool Variant::get<bool>() const;
    template<> int Variant::get<int>()  const;
    template<> unsigned int Variant::get<unsigned int>() const;
    template<> float Variant::get<float>()  const;
    template<> double Variant::get<double>()  const;
    template<> std::string Variant::get<std::string>()  const;

    template<> void Variant::set<bool>(const bool& value);
    template<> void Variant::set<int>(const int& value);
    template<> void Variant::set<int16_t>(const int16_t& value);
    template<> void Variant::set<float>(const float& value);
    template<> void Variant::set<double>(const double& value);
    template<> void Variant::set<std::string>(const std::string& value);

    // *******************************************************
    // Types for basic variants
    // *******************************************************
    namespace VariantType
    {
        // These const variables are abbreviations to the function that provides the type id of Variants
        const VariantTypeId Invalid = Variant::addTypeName("::armarx::InvalidVariantData");
        const VariantTypeId Bool = Variant::addTypeName("::armarx::BoolVariantData");
        const VariantTypeId Int = Variant::addTypeName("::armarx::IntVariantData");
        const VariantTypeId Float = Variant::addTypeName("::armarx::FloatVariantData");
        const VariantTypeId Double = Variant::addTypeName("::armarx::DoubleVariantData");
        const VariantTypeId String = Variant::addTypeName("::armarx::StringVariantData");
        bool IsBasicType(VariantTypeId id);

        inline void suppressWarningUnusedVariableForVariantTypeId()
        {
            ARMARX_DEBUG_S << VAROUT(Invalid);
            ARMARX_DEBUG_S << VAROUT(Bool);
            ARMARX_DEBUG_S << VAROUT(Int);
            ARMARX_DEBUG_S << VAROUT(Float);
            ARMARX_DEBUG_S << VAROUT(Double);
            ARMARX_DEBUG_S << VAROUT(String);
        }
    }
}
extern template class ::IceInternal::Handle< ::armarx::Variant>;

namespace std
{
    ARMARXCORE_IMPORT_EXPORT ostream& operator<<(ostream& stream, const armarx::VariantDataClass& variant);
}

