/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Core
* @author     Kai Welke (welke _at_ kit _dot_ edu)
* @date       2011 Kai Welke
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include <ArmarXCore/observers/variant/DataFieldIdentifier.h>
#include <boost/algorithm/string.hpp>
#include <ArmarXCore/core/util/StringHelpers.h>

using namespace armarx;

template class ::IceInternal::Handle<::armarx::DataFieldIdentifier>;

DataFieldIdentifier::DataFieldIdentifier(std::string dataFieldIdentifierStr)
{
    const std::string tmpStr = dataFieldIdentifierStr + "..";

    const size_t pos1 = tmpStr.find_first_of(".") + 1;   // one after the point
    const size_t pos2 = tmpStr.find_first_of(".", pos1); // on the point
    observerName = tmpStr.substr(0, pos1 - 1);
    channelName =  tmpStr.substr(pos1, pos2 - pos1);
    if (pos2 < dataFieldIdentifierStr.size())
    {
        //we do not want to copy the trailing points
        datafieldName = dataFieldIdentifierStr.substr(pos2 + 1);
    }
}

DataFieldIdentifier::DataFieldIdentifier(std::string observerName, std::string channelName, std::string datafieldName)
{
    this->observerName = observerName;
    this->channelName = channelName;
    this->datafieldName = datafieldName;
}


std::string DataFieldIdentifier::getIdentifierStr() const
{
    return observerName + "." + channelName + "." + datafieldName;
}

bool DataFieldIdentifier::equals(const DataFieldIdentifier& dataFieldIdentifier)
{
    std::string thisStr = getIdentifierStr();
    std::string otherStr = dataFieldIdentifier.getIdentifierStr();

    return (thisStr.compare(otherStr) == 0);
}

bool DataFieldIdentifier::beginsWith(const DataFieldIdentifier& dataFieldIdentifier)
{
    std::string thisStr = getIdentifierStr();
    std::string otherStr = dataFieldIdentifier.getIdentifierStr();

    return (thisStr.compare(0, otherStr.length(), otherStr) == 0);
}

