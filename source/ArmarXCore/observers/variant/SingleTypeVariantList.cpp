/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Kai Welke (welke at kit dot edu)
 * @date       2011
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include <ArmarXCore/observers/variant/SingleTypeVariantList.h>
#include <ArmarXCore/observers/exceptions/user/InvalidTypeException.h>
#include <ArmarXCore/observers/AbstractObjectSerializer.h>

#include <IceUtil/UUID.h>
#include <Ice/ObjectAdapter.h>
#include <Ice/ObjectFactory.h>

using namespace armarx;

SingleTypeVariantList::SingleTypeVariantList()
{
    typeContainer = VariantType::List(VariantType::Invalid).clone();
}

SingleTypeVariantList::SingleTypeVariantList(const ContainerType& subType)

{
    this->typeContainer = VariantType::List(subType).clone();
}

SingleTypeVariantList::SingleTypeVariantList(VariantTypeId subType)
{
    this->typeContainer = VariantType::List(subType).clone();
}

SingleTypeVariantList::SingleTypeVariantList(const SingleTypeVariantList& source) :
    IceUtil::Shared(source),
    VariantContainerBase(source),
    SingleTypeVariantListBase(source)
{
    *this = source;
}

SingleTypeVariantList& SingleTypeVariantList::operator =(const SingleTypeVariantList& source)
{
    typeContainer = ContainerTypePtr::dynamicCast(source.typeContainer->clone());
    elements.clear();

    for (unsigned int i = 0; i < source.elements.size(); i++)
    {
        elements.push_back(source.elements[i]->cloneContainer());
    }

    return *this;
}

VariantContainerBasePtr SingleTypeVariantList::cloneContainer(const Ice::Current& c) const
{
    VariantContainerBasePtr result = new SingleTypeVariantList(*this);
    return result;
}

Ice::ObjectPtr SingleTypeVariantList::ice_clone() const
{
    return this->clone();
}

void SingleTypeVariantList::addElement(const VariantContainerBasePtr& variantContainer, const Ice::Current& c)
{
    if (!VariantContainerType::compare(variantContainer->getContainerType(), getContainerType()->subType)
        && getContainerType()->subType->typeId != Variant::typeToString(VariantType::Invalid))
    {
        throw exceptions::user::InvalidTypeException(getContainerType()->subType->typeId, variantContainer->getContainerType()->typeId);
    }

    if (getContainerType()->subType->typeId == Variant::typeToString(VariantType::Invalid))
    {
        getContainerType()->subType = variantContainer->getContainerType()->clone();
    }

    elements.push_back(variantContainer->cloneContainer());
}

void SingleTypeVariantList::addVariant(const Variant& variant)
{
    if (getContainerType()->subType->typeId == Variant::typeToString(VariantType::Invalid))
    {
        getContainerType()->subType = new ContainerTypeI(variant.getType());
    }
    else if (!getContainerType()->subType || getContainerType()->subType->typeId != Variant::typeToString(variant.getType()))
    {
        throw exceptions::user::InvalidTypeException(getContainerType()->subType->typeId, Variant::typeToString(variant.getType()));
    }

    addElement(new SingleVariant(variant));
}

void SingleTypeVariantList::clear(const Ice::Current& c)
{
    elements.clear();
}

VariantTypeId SingleTypeVariantList::getStaticType(const Ice::Current& c)
{
    return Variant::addTypeName(getTypePrefix());
}

int SingleTypeVariantList::getSize(const Ice::Current& c) const
{
    return int(elements.size());
}

bool SingleTypeVariantList::validateElements(const Ice::Current& c)
{
    VariantContainerBaseList::iterator it = elements.begin();
    bool result = true;

    for (; it != elements.end(); it++)
    {
        result = result && (*it)->validateElements();
    }

    return result;
}


VariantContainerBasePtr SingleTypeVariantList::getElementBase(int index, const Ice::Current& c) const
{
    if (index >= getSize(c))
    {
        throw IndexOutOfBoundsException();
    }

    return elements.at(index);
}


VariantPtr SingleTypeVariantList::getVariant(int index) const
{
    VariantPtr ptr = getElement<SingleVariant>(index)->get();

    if (!ptr)
    {
        throw InvalidTypeException();
    }

    return ptr;
}

void SingleTypeVariantList::serialize(const ObjectSerializerBasePtr& serializer, const Ice::Current&) const
{
    AbstractObjectSerializerPtr obj = AbstractObjectSerializerPtr::dynamicCast(serializer);
    AbstractObjectSerializerPtr arr = obj->createElement();
    arr->setElementType(ElementTypes::eArray);

    for (size_t i = 0; i < elements.size(); i++)
    {
        arr->setIceObject(i, elements.at(i));
    }

    obj->setString("type", ice_id());
    obj->setElement("array", arr);
}

void SingleTypeVariantList::deserialize(const ObjectSerializerBasePtr& serializer, const Ice::Current&)
{
    AbstractObjectSerializerPtr obj = AbstractObjectSerializerPtr::dynamicCast(serializer);
    AbstractObjectSerializerPtr arr = obj->getElement("array");

    for (size_t i = 0; i < arr->size(); i++)
    {
        VariantContainerBasePtr c = VariantContainerBasePtr::dynamicCast(arr->getIceObject(i));

        if (c)
        {
            addElement(c);
        }
        else
        {
            throw LocalException("Could not cast to VariantContainerBasePtr");
        }
    }
}

std::string SingleTypeVariantList::getTypePrefix()
{
    return "::armarx::SingleTypeVariantListBase";
}


std::string SingleTypeVariantList::toString(const Ice::Current& c) const
{
    std::stringstream ss;

    for (const auto& element : elements)
    {
        if (elements[elements.size() - 1] != element)
        {
            ss << element->toString() << "\n";
        }
        else
        {
            ss << element->toString();
        }
    }

    return ss.str();
}


Ice::Int armarx::SingleTypeVariantList::getType(const Ice::Current&) const
{
    return SingleTypeVariantList::getStaticType();
}
