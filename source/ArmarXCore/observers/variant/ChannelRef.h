/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Core
* @author     Kai Welke (welke@kit.edu)
* @date       2011 Kai Welke
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <ArmarXCore/core/system/ImportExport.h>
#include <ArmarXCore/interface/observers/ObserverInterface.h>
#include <ArmarXCore/interface/observers/ChannelRefBase.h>
#include <ArmarXCore/interface/observers/ObserverInterface.h>
#include <ArmarXCore/observers/variant/DataFieldIdentifier.h>
#include <ArmarXCore/observers/variant/TimedVariant.h>
#include <ArmarXCore/observers/exceptions/local/InvalidDataFieldException.h>

#include <boost/utility/enable_if.hpp>
#include <boost/type_traits/is_base_of.hpp>

namespace armarx
{
    class Observer;
    class ChannelRef;
    typedef IceInternal::Handle<ChannelRef> ChannelRefPtr;

    /**
     * @class ChannelRef
     * @ingroup VariantsGrp
     * @brief The ChannelRef class is a reference to a channel on an \ref Observer.
     * It is used to access data directly from a channel or to be passed to function
     * as an identifier for a channel.
     *
     */
    class ARMARXCORE_IMPORT_EXPORT ChannelRef :
        virtual public ChannelRefBase
    {
        template <class BaseClass, class VariantClass>
        friend class GenericFactory;
    protected:
        ChannelRef() :
            validationTimeout(4000),
            waitIntervallMs(10)
        {
            initialized = false;
        }


    public:
        ChannelRef(Observer* observer, std::string channelName);
        ChannelRef(ObserverInterfacePrx observerPrx, std::string channelName);
        ~ChannelRef() override {}

        // inherited from VariantDataClass
        Ice::ObjectPtr ice_clone() const override
        {
            return this->clone();
        }

        VariantDataClassPtr clone(const Ice::Current& c = ::Ice::Current()) const override;
        std::string output(const Ice::Current& c = ::Ice::Current()) const override;
        VariantTypeId getType(const Ice::Current& c = ::Ice::Current()) const override;
        bool validate(const Ice::Current& c = ::Ice::Current()) override;

        // datafield access
        DataFieldIdentifierPtr getDataFieldIdentifier(const std::string& datafieldName);
        TimedVariantPtr getDataField(const std::string& datafieldName);
        const Ice::StringSeq& getDataFieldNames() const;
        bool hasDatafield(const std::string& datafieldName) const;
        bool getInitialized();
        void refetchChannel();
        // helper methods for variant content access
        template<typename T>
        typename boost::enable_if<boost::is_base_of<VariantDataClass, T>, IceInternal::Handle<T> >::type
        get(const std::string& key)
        {
            VariantPtr var = getDataField(key);

            if (!var)
            {
                throw armarx::exceptions::local::InvalidDataFieldException(getChannelName(), key);
            }

            return var->get<T>();
        }

        template<typename T>
        typename boost::disable_if<boost::is_base_of<VariantDataClass, T>, T >::type
        get(const std::string& key)
        {
            VariantPtr var = getDataField(key);

            if (!var)
            {
                throw armarx::exceptions::local::InvalidDataFieldException(getChannelName(), key);
            }

            return var->get<T>();
        }

        // channel access
        const std::string& getChannelName() const;

        // observer access
        const std::string& getObserverName() const;
        const ObserverInterfacePrx& getObserverProxy();

        /**
        * stream operator for DataFieldIdentifier
        */
        friend std::ostream& operator<<(std::ostream& stream, const ChannelRef& rhs)
        {
            stream << rhs.output();
            return stream;
        }

        /**
        * stream operator for Ice shared pointer of DataFieldIdentifier
        */
        friend std::ostream& operator<<(std::ostream& stream, const ChannelRefPtr& rhs)
        {
            stream << rhs->output();
            return stream;
        }

    public: // serialization
        void serialize(const ObjectSerializerBasePtr& serializer, const ::Ice::Current& = ::Ice::Current()) const override;
        void deserialize(const ObjectSerializerBasePtr& serializer, const ::Ice::Current& = ::Ice::Current()) override;

    private:
        bool assureProxy();
        void initializeDataFields();

        Ice::CommunicatorPtr communicator;
        const IceUtil::Int64 validationTimeout;
        const int waitIntervallMs;

        // Object interface
    public:
        void ice_postUnmarshal() override;
    };

    namespace VariantType
    {
        const VariantTypeId ChannelRef = Variant::addTypeName("::armarx::ChannelRefBase");
        inline void suppressWarningUnusedVariableForChannelRef()
        {
            ARMARX_DEBUG_S << VAROUT(ChannelRef);
        }
    }
}
extern template class ::IceInternal::Handle<::armarx::ChannelRef>;


