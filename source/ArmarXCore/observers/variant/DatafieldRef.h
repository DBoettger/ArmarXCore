/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Core
* @author     Kai Welke (welke@kit.edu)
* @date       2011 Kai Welke
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <ArmarXCore/core/system/ImportExport.h>
#include <ArmarXCore/interface/observers/ObserverInterface.h>
#include <ArmarXCore/interface/observers/DataFieldIdentifierBase.h>
#include <ArmarXCore/observers/variant/DataFieldIdentifier.h>
#include <ArmarXCore/observers/variant/TimedVariant.h>
#include <ArmarXCore/observers/variant/ChannelRef.h>
//#include <ArmarXCore/observers/Observer.h>
#include <ArmarXCore/observers/exceptions/local/InvalidDataFieldException.h>

#include <boost/utility/enable_if.hpp>
#include <boost/type_traits/is_base_of.hpp>

namespace armarx
{


    class DatafieldRef;
    typedef IceInternal::Handle<DatafieldRef> DatafieldRefPtr;

    /**
     * @class DatafieldRef
     * @ingroup VariantsGrp
     * @brief The DatafieldRef class is similar to the \ref ChannelRef, but points
     * to a specific Datafield instead of to a complete channel.
     * It can be used to query (@ref getDataField()) the data of the datafield from the \ref Observer.
     */
    class ARMARXCORE_IMPORT_EXPORT DatafieldRef :
        virtual public DatafieldRefBase
    {
        template <class BaseClass, class VariantClass>
        friend class GenericFactory;
    protected:
        DatafieldRef();
    public:
        DatafieldRef(Observer* observer, const std::string& channelName, const std::string& datafieldName, bool performValidation = true);

        DatafieldRef(ObserverInterfacePrx observerPrx, const std::string& channelName, const std::string& datafieldName, bool performValidation = true);
        DatafieldRef(ChannelRefPtr channelRef, const std::string& datafieldName, bool performValidation = true);
        ~DatafieldRef() override {}

        // inherited from VariantDataClass
        Ice::ObjectPtr ice_clone() const override;

        VariantDataClassPtr clone(const Ice::Current& c = ::Ice::Current()) const override;
        std::string output(const Ice::Current& c = ::Ice::Current()) const override;
        VariantTypeId getType(const Ice::Current& c = ::Ice::Current()) const override;
        bool validate(const Ice::Current& c = ::Ice::Current()) override;


        // datafield access
        DataFieldIdentifierPtr getDataFieldIdentifier() const;

        /**
         * @brief Retrieves the value of the datafield from the \ref Observer.
         * @return Variant with the type and value of the datafield.
         */
        TimedVariantPtr getDataField() const;


        // helper methods for variant content access
        template<typename T>
        typename boost::enable_if<boost::is_base_of<VariantDataClass, T>, IceInternal::Handle<T> >::type
        get() const
        {
            VariantPtr var = getDataField();

            if (!var)
            {
                throw armarx::exceptions::local::InvalidDataFieldException(channelRef->channelName, datafieldName);
            }

            return var->get<T>();
        }

        template<typename T>
        typename boost::disable_if<boost::is_base_of<VariantDataClass, T>, T >::type
        get() const
        {
            VariantPtr var = getDataField();

            if (!var)
            {
                throw armarx::exceptions::local::InvalidDataFieldException(channelRef->channelName, datafieldName);
            }

            return var->get<T>();
        }

        bool getBool(const Ice::Current& c = ::Ice::Current()) const
        {
            return get<bool>();
        }
        int getInt(const Ice::Current& c = ::Ice::Current()) const
        {
            return get<int>();
        }
        float getFloat(const Ice::Current& c = ::Ice::Current()) const
        {
            return get<float>();
        }
        double getDouble(const Ice::Current& c = ::Ice::Current()) const
        {
            return get<double>();
        }
        std::string getString(const Ice::Current& c = ::Ice::Current()) const
        {
            return get<std::string>();
        }


        ChannelRefPtr getChannelRef() const;

        /**
        * stream operator for DataFieldIdentifier
        */
        friend std::ostream& operator<<(std::ostream& stream, const DatafieldRef& rhs)
        {
            stream << rhs.output();
            return stream;
        }

        /**
        * stream operator for Ice shared pointer of DataFieldIdentifier
        */
        friend std::ostream& operator<<(std::ostream& stream, const DatafieldRefPtr& rhs)
        {
            stream << rhs->output();
            return stream;
        }

    public: // serialization
        void serialize(const ObjectSerializerBasePtr& serializer, const ::Ice::Current& = ::Ice::Current()) const override;
        void deserialize(const ObjectSerializerBasePtr& serializer, const ::Ice::Current& = ::Ice::Current()) override;

    private:
        mutable DataFieldIdentifierPtr id;
    };


    namespace VariantType
    {
        const VariantTypeId DatafieldRef = Variant::addTypeName("::armarx::DatafieldRefBase");
        inline void suppressWarningUnusedVariableForDatafieldRef()
        {
            ARMARX_DEBUG_S << VAROUT(DatafieldRef);
        }
    }

}

extern template class ::IceInternal::Handle<::armarx::DatafieldRef>;



