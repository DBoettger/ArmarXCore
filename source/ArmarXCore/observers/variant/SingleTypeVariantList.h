/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarXCore::core
* @author     Kai Welke (welke@kit.edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <ArmarXCore/core/system/ImportExport.h>
#include <ArmarXCore/observers/variant/Variant.h>
#include <ArmarXCore/observers/variant/VariantContainer.h>
#include <ArmarXCore/interface/observers/VariantContainers.h>
#include <ArmarXCore/observers/exceptions/user/InvalidTypeException.h>

#include <IceUtil/Handle.h>


namespace armarx
{
    class SingleTypeVariantList;
    typedef IceInternal::Handle<SingleTypeVariantList> SingleTypeVariantListPtr;
    typedef SingleTypeVariantList STVarList;

    /**
     * \class SingleTypeVariantList
     * \ingroup VariantsGrp
     * \brief The SingleTypeVariantList class is a subclass of VariantContainer and is comparable to a std::vector<T> containing values of type T.
     */
    class ARMARXCORE_IMPORT_EXPORT SingleTypeVariantList :
        virtual public VariantContainer,
        virtual public SingleTypeVariantListBase
    {
    public:
        SingleTypeVariantList();
        SingleTypeVariantList(const SingleTypeVariantList& source);
        explicit SingleTypeVariantList(const ContainerType& subType);
        explicit SingleTypeVariantList(VariantTypeId subType);
        SingleTypeVariantList& operator=(const SingleTypeVariantList& source);
        VariantContainerBasePtr cloneContainer(const Ice::Current& c = ::Ice::Current()) const override;
        Ice::ObjectPtr ice_clone() const override;

        // element manipulation
        void addElement(const VariantContainerBasePtr& variantContainer, const Ice::Current& c = ::Ice::Current()) override;
        void addVariant(const Variant& variant);
        void clear(const Ice::Current& c = ::Ice::Current()) override;

        // getters
        Ice::Int getType(const Ice::Current& c = ::Ice::Current()) const override;
        static VariantTypeId getStaticType(const Ice::Current& c = ::Ice::Current());
        int getSize(const Ice::Current& c = ::Ice::Current()) const override;
        bool validateElements(const Ice::Current& c = ::Ice::Current()) override;

        /**
         * @brief getElementBase is the slice-interface implementation for
         * getting an Element and only returns a basepointer, so a manual upcast
         * is usually necessary.
         *
         * This function exists only for completeness and compatibility. Usually
         * you should use the getElement()-function.
         * @param index is the index of the Element in the list
         * @param c Not needed, leave blank.
         * @throw IndexOutOfBoundsException
         * @return a base variant pointer
         */
        VariantContainerBasePtr getElementBase(int index, const Ice::Current& c = ::Ice::Current()) const override;

        template <typename ContainerType>
        IceInternal::Handle<ContainerType> getElement(int index) const
        {
            IceInternal::Handle<ContainerType> ptr = IceInternal::Handle<ContainerType>::dynamicCast(getElementBase(index));

            if (!ptr)
            {
                throw InvalidTypeException();
            }

            if (!ptr)
            {
                throw exceptions::user::InvalidTypeException(ContainerType::ice_staticId(), "");
            }

            return ptr;
        }
        VariantPtr getVariant(int index) const;


        template <typename T1, typename T2>
        static std::map<T1, T2> MakeMap(SingleTypeVariantListPtr l1, SingleTypeVariantListPtr l2)
        {
            if (l1->getSize() != l2->getSize())
            {
                throw LocalException("List lengths do not match:") << l1->getSize() << " != " << l2->getSize();
            }

            std::map<T1, T2> result;
            size_t size = l1->getSize();

            for (size_t i = 0; i < size; i++)
            {
                result[l1->getVariant(i)->get<T1>()] = l2->getVariant(i)->get<T2>();
            }

            return result;
        }

        template <typename T1>
        static SingleTypeVariantListPtr FromStdVector(const std::vector<T1>& vec)
        {
            SingleTypeVariantListPtr result = new SingleTypeVariantList();

            for (size_t i = 0; i < vec.size(); i++)
            {
                result->addVariant(vec.at(i));
            }

            return result;
        }

        template <typename T1>
        static SingleTypeVariantListPtr FromContainerStdVector(const std::vector<T1>& vec)
        {
            SingleTypeVariantListPtr result = new SingleTypeVariantList();

            for (size_t i = 0; i < vec.size(); i++)
            {
                result->addElement(vec.at(i));
            }

            return result;
        }

    public: // serialization
        void serialize(const ObjectSerializerBasePtr& serializer, const ::Ice::Current& = ::Ice::Current()) const override;
        void deserialize(const ObjectSerializerBasePtr& serializer, const ::Ice::Current& = ::Ice::Current()) override;


        static std::string getTypePrefix();

        template <typename Type>
        std::vector<Type> toStdVector() const
        {
            std::vector<Type> result(getSize());

            for (int i = 0; i < getSize(); i++)
            {
                result.at(i) = getVariant(i)->get<Type>();
            }

            return result;
        }
        template <typename Type>
        std::vector<Type> toContainerStdVector() const
        {
            std::vector<Type> result(getSize());

            for (int i = 0; i < getSize(); i++)
            {
                Type p = getElement<typename Type::element_type>(i);
                result.at(i) = p;
            }

            return result;
        }

        // VariantContainerBase interface
    public:
        std::string toString(const Ice::Current& c) const override;
    };

    namespace VariantType
    {
        const VariantContainerType List = VariantContainerType(SingleTypeVariantList::getTypePrefix());

        const VariantTypeId SingleTypeVariantList = Variant::addTypeName(SingleTypeVariantList::getTypePrefix());
        inline void suppressWarningUnusedVariableForSingleTypeVariantList()
        {
            ARMARX_DEBUG_S << VAROUT(SingleTypeVariantList);
        }
    }
}

