/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Core
* @author     Kai Welke (welke@kit.edu)
* @date       2012 Kai Welke
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#include "DatafieldRef.h"

#include <ArmarXCore/observers/exceptions/local/InvalidDataFieldException.h>
#include <ArmarXCore/core/exceptions/local/ProxyNotInitializedException.h>
#include <ArmarXCore/observers/AbstractObjectSerializer.h>
#include <ArmarXCore/statechart/Exception.h>

#include <IceUtil/Exception.h>

#include <boost/lexical_cast.hpp>

#include <algorithm>

using namespace armarx;
using namespace armarx::exceptions::local;

template class ::IceInternal::Handle<::armarx::DatafieldRef>;

// *******************************************************
// Construction / destruction
// *******************************************************
DatafieldRef::DatafieldRef()
{

}

DatafieldRef::DatafieldRef(Observer* observer, const std::string& channelName, const std::string& datafieldName, bool performValidation)
{
    channelRef = new ChannelRef(observer, channelName);
    this->datafieldName = datafieldName;

    if (performValidation)
        if (!validate())
        {
            ARMARX_WARNING_S << "Could not validate ChannelRef for " << getDataFieldIdentifier()->getIdentifierStr();
        }
}

DatafieldRef::DatafieldRef(ObserverInterfacePrx observerPrx, const std::string& channelName, const std::string& datafieldName, bool performValidation)
{
    channelRef = new ChannelRef(observerPrx, channelName);
    this->datafieldName = datafieldName;

    if (performValidation)
        if (!validate())
        {
            ARMARX_WARNING_S << "Could not validate ChannelRef for " << getDataFieldIdentifier()->getIdentifierStr();
        }
}

DatafieldRef::DatafieldRef(ChannelRefPtr channelRef, const std::string& datafieldName, bool performValidation)
{
    if (!channelRef)
    {
        throw exceptions::local::eNullPointerException("ChannelRef must not be NULL");
    }

    this->channelRef = channelRef;
    this->datafieldName = datafieldName;

    if (performValidation)
        if (!validate())
        {
            ARMARX_WARNING_S << "Could not validate ChannelRef for " << getDataFieldIdentifier()->getIdentifierStr();
        }
}

Ice::ObjectPtr DatafieldRef::ice_clone() const
{
    return this->clone();
}

// *******************************************************
// Datafield access
// *******************************************************
DataFieldIdentifierPtr DatafieldRef::getDataFieldIdentifier() const
{
    if (!id)
    {
        id = getChannelRef()->getDataFieldIdentifier(datafieldName);
    }

    return id;
}

TimedVariantPtr DatafieldRef::getDataField() const
{
    return getChannelRef()->getDataField(datafieldName);
}


ChannelRefPtr DatafieldRef::getChannelRef() const
{
    return ChannelRefPtr::dynamicCast(channelRef);
}


// *******************************************************
// Inherited from VariantDataClass
// *******************************************************
VariantDataClassPtr DatafieldRef::clone(const Ice::Current& c) const
{
    DatafieldRefPtr dfr = new DatafieldRef(*this);

    return dfr;
}

std::string DatafieldRef::output(const Ice::Current& c) const
{
    std::stringstream s;
    s << "Reference to datafield " << DataFieldIdentifierPtr::dynamicCast(getDataFieldIdentifier())->getIdentifierStr();

    return s.str();
}

VariantTypeId DatafieldRef::getType(const Ice::Current& c) const
{
    return VariantType::DatafieldRef;
}

bool DatafieldRef::validate(const Ice::Current& c)
{

    if (!getChannelRef())
    {
        return false;
    }

    if (!getChannelRef()->hasDatafield(datafieldName))
    {
        return false;
    }

    if (!getChannelRef()->getDataField(datafieldName)->getInitialized())
    {
        return false;
    }

    return true;
}





void DatafieldRef::serialize(const ObjectSerializerBasePtr& serializer, const ::Ice::Current&) const
{
    AbstractObjectSerializerPtr obj = AbstractObjectSerializerPtr::dynamicCast(serializer);

    channelRef->serialize(serializer);
    obj->setString("datafieldName", datafieldName);
}

void DatafieldRef::deserialize(const ObjectSerializerBasePtr& serializer, const ::Ice::Current& c)
{
    AbstractObjectSerializerPtr obj = AbstractObjectSerializerPtr::dynamicCast(serializer);

    channelRef->deserialize(serializer, c);
    datafieldName = obj->getString("datafieldName");



}

