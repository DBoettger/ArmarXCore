/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <ArmarXCore/core/rapidxml/wrapper/RapidXmlReader.h>
#include <Ice/BuiltinSequences.h>
#include <vector>
#include <string>
#include <set>
#include <boost/shared_ptr.hpp>
#include <boost/optional.hpp>

namespace armarx
{
    class VariantInfo;
    typedef boost::shared_ptr<VariantInfo> VariantInfoPtr;

    class DynamicLibrary;
    typedef boost::shared_ptr<DynamicLibrary> DynamicLibraryPtr;

    class VariantInfo
    {
    public:
        class VariantEntry;
        typedef boost::shared_ptr<VariantEntry> VariantEntryPtr;

        class VariantEntry
        {
            friend class VariantInfo;

        public:
            VariantEntry(RapidXmlReaderNode node);
            const std::string& getHumanName() const;
            const std::string& getBaseTypeName() const;
            const std::string& getDataTypeName() const;
            const boost::optional<std::string>& getIncludePath() const;
        private:
            std::string baseTypeName;
            std::string dataTypeName;
            std::string humanName;
            boost::optional<std::string> includePath;
            bool basic;
        };

        enum ProxyType { SingleProxy, Topic };

        class ProxyEntry;
        typedef boost::shared_ptr<ProxyEntry> ProxyEntryPtr;

        class ProxyEntry
        {
            friend class VariantInfo;

        private:
            std::string includePath;
            std::string humanName;
            std::string typeName;
            std::string memberName;
            std::string getterName;
            std::string propertyName;
            bool propertyIsOptional;
            std::string propertyDefaultValue;
            ProxyType proxyType;

            std::vector<std::string> includes;
            std::vector<std::string> libraries;
            std::vector<std::pair<std::string, std::string>> methods;
            std::vector<std::string> members;
            std::vector<std::string> onInit;
            std::vector<std::string> onConnect;
            std::vector<std::pair<std::string, std::string>> stateMethods;

            void readVector(RapidXmlReaderNode node, const char* name, std::vector<std::string>& vec);

        public:
            ProxyEntry(RapidXmlReaderNode node);
            std::string getIncludePath()
            {
                return includePath;
            }
            std::string getHumanName()
            {
                return humanName;
            }
            std::string getTypeName()
            {
                return typeName;
            }
            std::string getMemberName()
            {
                return memberName;
            }
            std::string getGetterName()
            {
                return getterName;
            }
            std::string getPropertyName()
            {
                return propertyName;
            }
            bool getPropertyIsOptional()
            {
                return propertyIsOptional;
            }
            std::string getPropertyDefaultValue()
            {
                return propertyDefaultValue;
            }
            ProxyType getProxyType()
            {
                return proxyType;
            }
            std::string getProxyTypeAsString()
            {
                return proxyType == Topic ? "Topic" : "Proxy";
            }

            std::vector<std::string> getIncludes()
            {
                return includes;
            }
            std::vector<std::string> getLibraries()
            {
                return libraries;
            }
            std::vector<std::pair<std::string, std::string>> getMethods()
            {
                return methods;
            }
            std::vector<std::string> getMembers()
            {
                return members;
            }
            std::vector<std::string> getOnInit()
            {
                return onInit;
            }
            std::vector<std::string> getOnConnect()
            {
                return onConnect;
            }
            std::vector<std::pair<std::string, std::string>> getStateMethods()
            {
                return stateMethods;
            }
        };

        class LibEntry;
        typedef boost::shared_ptr<LibEntry> LibEntryPtr;

        class LibEntry
        {
            friend class VariantInfo;

        public:
            LibEntry(RapidXmlReaderNode node, const std::string& packageName);
            std::vector<std::string> getFactoryIncludes() const;
            std::string getName() const;
            std::vector<ProxyEntryPtr> getProxies() const ;
            const std::vector<VariantEntryPtr>& getVariants() const;
            std::string getPackageName() const;
            std::string getAbsoluteLibPath() const;
            /**
             * @brief Returns a list of includes for a specific variant (usually only one).
             * If not set in the variantinfo xml for that particular variant, it will return the factory include.
             * @param variantBaseTypeName
             */
            std::vector<std::string> getVariantIncludes(const std::string& variantBaseTypeName) const;

        private:
            std::vector<std::string> factoryIncludes;
            std::vector<VariantEntryPtr> variants;
            std::vector<ProxyEntryPtr> proxies;
            std::string name;
            std::string packageName;
        };

    public:
        VariantInfo();
        std::vector<LibEntryPtr> readVariantInfo(RapidXmlReaderPtr reader, const std::string& packagePath, const std::string& packageName);
        LibEntryPtr findLibByVariant(std::string variantTypeName) const;
        LibEntryPtr findLibByProxy(std::string proxyTypeName) const;
        std::set<LibEntryPtr> findLibs(const Ice::StringSeq& variantTypeNames, const Ice::StringSeq& proxyTypeNames = {}) const;
        Ice::StringSeq  findLibNames(const Ice::StringSeq& variantTypeNames, const Ice::StringSeq& proxyTypeNames = {}) const;
        std::string getDataTypeName(std::string variantBaseTypeName);
        std::string getReturnTypeName(std::string variantBaseTypeName);
        bool isBasic(std::string variantBaseTypeName);
        std::vector<LibEntryPtr> getLibs() const;
        ProxyEntryPtr getProxyEntry(std::string proxyId);
        VariantEntryPtr getVariantByName(std::string variantBaseTypeName);
        VariantEntryPtr getVariantByHumanName(std::string humanName);
        std::string getNestedHumanNameFromBaseName(std::string variantBaseTypeName);
        std::string getNestedBaseNameFromHumanName(std::string humanName);
        const std::map<std::string, std::string>& getPackagePaths() const;
        bool isPackageLoaded(const std::string packageName) const;
        std::string getDebugInfo() const;

        static VariantInfoPtr ReadInfoFilesRecursive(const std::string& rootPackageName, const std::string& rootPackagePath, bool showErrors, VariantInfoPtr variantInfo = VariantInfoPtr());
        static VariantInfoPtr ReadInfoFiles(const std::vector<std::string>& packages, bool showErrors = true, bool throwOnError = true);

        armarx::DynamicLibraryPtr loadLibraryOfVariant(std::string variantTypeName) const;

    private:
        std::vector<LibEntryPtr> libs;
        std::map<std::string, LibEntryPtr> variantToLibMap;
        std::map<std::string, VariantEntryPtr> variantMap;
        std::map<std::string, VariantEntryPtr> humanNameToVariantMap;
        std::map<std::string, ProxyEntryPtr> proxyMap;
        std::map<std::string, std::string> packagePaths;
    };
}

