/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarXCore::core
* @author     Kai Welke (welke@kit.edu)
* @date       2011
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

// ArmarX
#include <ArmarXCore/core/system/ImportExport.h>
#include <ArmarXCore/interface/observers/VariantContainers.h>
#include <ArmarXCore/interface/observers/VariantBase.h>
#include <ArmarXCore/core/system/SafeShared.h>
#include <ArmarXCore/core/logging/Logging.h>
#include <ArmarXCore/observers/variant/Variant.h>

// Ice
#include <IceUtil/Handle.h>

// boost
#include <boost/utility/enable_if.hpp>
#include <boost/type_traits/is_base_of.hpp>

#include <string>
#include <map>


namespace armarx
{
    namespace VariantType
    {
        const VariantTypeId VariantContainer = Variant::addTypeName("::armarx::VariantContainerBase");
        inline void suppressWarningUnusedVariableForVariantContainer()
        {
            ARMARX_DEBUG_S << VAROUT(VariantContainer);
        }
    }

    class VariantContainer;
    typedef IceInternal::Handle<VariantContainer> VariantContainerPtr;


    class SingleVariant;
    typedef IceInternal::Handle<SingleVariant> SingleVariantPtr;

    /**
     * \class VariantContainer
     * \ingroup VariantsGrp
     * \brief VariantContainer is the base class of all other Variant container classes.
     *
     * Each VariantContainer can contain values of the types
     *
     * \li VariantContainer or subclasses thereof
     * \li SingleVariant
     *
     * Since VariantContainer inherits from VariantDataClass it is possible
     * to put a VariantContainer into a Variant to send it via Ice.
     * \see StringValueMap
     * \see SingleTypeVariantList
     *
     \code
     // VariantContainer can be replaced with any subclass of it
     armarx::VariantContainerPtr variantContainer;
     // create variant from VariantContainer
     armarx::VariantPtr variant = new armarx::Variant(variantContainer);
     // get VariantContainer from Variant
     armarx::VariantContainerPtr variantContainer2 = variant->get<armarx::VariantContainer>();
     if (variantContainer2)
     {
        // work with the container
     }
     else
     {
        // value of the variant was not a VariantContainer
     }
     \endcode
     */
    class ARMARXCORE_IMPORT_EXPORT VariantContainer :
        virtual public VariantContainerBase
    {
    public:
        ContainerTypePtr getContainerType(const Ice::Current& c = ::Ice::Current()) const override;
        void setContainerType(const ContainerTypePtr& containerType, const Ice::Current& c = ::Ice::Current()) override;

        // VariantDataClass interface
    public:
        VariantDataClassPtr clone(const Ice::Current& c = Ice::Current()) const override;
        std::string output(const Ice::Current& c = Ice::Current()) const override;
        Ice::Int getType(const Ice::Current& c = Ice::Current()) const override;
        bool validate(const Ice::Current& c = Ice::Current()) override;
    };


    /**
     * \class SingleVariant
     * \ingroup VariantsGrp
     * \brief The SingleVariant class is required to store single Variant instances in VariantContainer subclasses.
     *
     * Without this class the compiler would not be able to compile code which is storing single Variants instead of
     * VariantContainers as values inside a VariantContainer instance .
     */
    class ARMARXCORE_IMPORT_EXPORT SingleVariant :
        virtual public VariantContainer,
        virtual public SingleVariantBase
    {
    public:
        SingleVariant();
        SingleVariant(const Variant& variant);
        SingleVariant(const SingleVariant& source);
        SingleVariant& operator=(const SingleVariant& source);
        VariantContainerBasePtr cloneContainer(const::Ice::Current& = Ice::Current()) const override;
        Ice::ObjectPtr ice_clone() const override;
        VariantBasePtr getElementBase(const Ice::Current& = Ice::Current()) const override;
        VariantPtr get() const;
        void setVariant(const VariantPtr& variant);

        int getSize(const::Ice::Current& = Ice::Current()) const override;
        ContainerTypePtr getContainerType(const Ice::Current& c = ::Ice::Current()) const override;
        void setContainerType(const ContainerTypePtr& containerType, const Ice::Current& c = ::Ice::Current()) override;
        static VariantTypeId getStaticType(const Ice::Current& c = ::Ice::Current());
        //        int getSubType(const::Ice::Current & = Ice::Current()) const { return subType;}
        void clear(const::Ice::Current& = Ice::Current()) override { }
        bool validateElements(const::Ice::Current& = Ice::Current()) override;

    public: // serialization
        void serialize(const ObjectSerializerBasePtr& serializer, const ::Ice::Current& = ::Ice::Current()) const override;
        void deserialize(const ObjectSerializerBasePtr& serializer, const ::Ice::Current& = ::Ice::Current()) override;

        template<class T>
        static T fromString(const std::string& s)
        {
            std::istringstream stream(s);
            T t;
            stream >> t;
            return t;
        }

        static std::string getTypePrefix();

        // VariantContainerBase interface
    public:
        std::string toString(const Ice::Current& c) const override;
    };

    class ContainerDummy : virtual public VariantContainer
    {
    public:
        ContainerDummy();
        ContainerDummy(const ContainerDummy& source);
        VariantContainerBasePtr cloneContainer(const Ice::Current& = ::Ice::Current()) const override;
        Ice::ObjectPtr ice_clone() const override;
        ContainerTypePtr getContainerType(const Ice::Current&   = ::Ice::Current()) const override;
        void setContainerType(const ContainerTypePtr& containerType, const Ice::Current& = ::Ice::Current()) override;
        void clear(const Ice::Current& = ::Ice::Current()) override {}
        int getSize(const Ice::Current& = ::Ice::Current()) const override;
        bool validateElements(const Ice::Current& = ::Ice::Current()) override;
    public: // serialization
        void serialize(const ObjectSerializerBasePtr& serializer, const ::Ice::Current& = ::Ice::Current()) const override {}
        void deserialize(const ObjectSerializerBasePtr& serializer, const ::Ice::Current& = ::Ice::Current()) override {}

        // VariantContainerBase interface
    public:
        std::string toString(const Ice::Current& c) const override;
    };

    class ContainerTypeI : virtual public ContainerType
    {
    public:
        ContainerTypeI() {}
        ContainerTypeI(VariantTypeId variantType);
        ContainerTypePtr clone(const Ice::Current& = Ice::Current()) const override;
    };
    typedef IceInternal::Handle<ContainerTypeI> ContainerTypeIPtr;


    class VariantContainerType
    {
    public:
        VariantContainerType(std::string containerType);
        const ContainerTypeI operator()(VariantTypeId typeId) const;
        const ContainerTypeI operator()(const ContainerType& subType) const;
        static bool compare(const ContainerTypePtr& type1, const ContainerTypePtr& secondType);
        static std::string allTypesToString(const ContainerTypePtr& type);
        static ContainerTypePtr FromString(const std::string& typeStr);
        static std::string GetInnerType(const std::string& typeStr);
        static std::string GetInnerType(ContainerTypePtr type);
    private:
        std::string containerType;
        ContainerTypePtr thisType;
    };

    namespace VariantType
    {
        const VariantContainerType SingleVariantContainer = VariantContainerType(SingleVariant::getTypePrefix());
    }

}

extern template class ::IceInternal::Handle< ::armarx::SingleVariant>;
extern template class ::IceInternal::Handle< ::armarx::ContainerTypeI>;



