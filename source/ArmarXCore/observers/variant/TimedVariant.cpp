/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2015
* @copyright  http://www.gnu.org/licenses/gpl.txt
*             GNU General Public License
*/

#include "TimedVariant.h"

using namespace armarx;

template class ::IceInternal::Handle<::armarx::TimedVariant>;


TimedVariant::TimedVariant()
{

}

TimedVariant::TimedVariant(const TimedVariant& source) :
    IceUtil::Shared(source),
    Ice::Object(source),
    VariantBase(source),
    SafeShared<Variant>(source),
    Variant(dynamic_cast<const Variant&>(source)),
    TimedVariantBase(source)
{

}

TimedVariant::TimedVariant(const Variant& variant, const IceUtil::Time& time) :
    IceUtil::Shared(variant),
    Ice::Object(variant),
    VariantBase(variant),
    SafeShared<Variant>(variant),
    Variant(variant),
    TimedVariantBase()

{
    timestamp = time.toMicroSeconds();
    initialized = variant.getInitialized(GlobalIceCurrent);
}

TimedVariant::TimedVariant(const VariantPtr& variant, const IceUtil::Time& time)
{
    data = variant->data;
    Ice::Current c;
    typeId = variant->getType(c);
    timestamp = time.toMicroSeconds();
    initialized = variant->getInitialized(c);
}

TimedVariant::~TimedVariant()
{

}

IceUtil::Time TimedVariant::getTime() const
{
    return IceUtil::Time::microSeconds(timestamp);
}

Ice::Long TimedVariant::getTimestamp(const Ice::Current& c) const
{
    return timestamp;
}

Ice::ObjectPtr TimedVariant::ice_clone() const
{
    return new TimedVariant(*this);
}

std::string TimedVariant::getOutputValueOnly() const
{
    return Variant::getOutputValueOnly() + " \nTime: " + IceUtil::Time::microSeconds(timestamp).toDateTime();
}

