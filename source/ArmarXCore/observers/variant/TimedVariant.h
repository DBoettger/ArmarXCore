/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2015
* @copyright  http://www.gnu.org/licenses/gpl.txt
*             GNU General Public License
*/

#pragma once

#include <chrono>

#include "Variant.h"

#include <IceUtil/Time.h>


namespace armarx
{
    class TimedVariant;
    typedef IceInternal::Handle<TimedVariant> TimedVariantPtr;

    class TimedVariant :
        public armarx::Variant,
        public TimedVariantBase
    {
    public:
        TimedVariant();
        TimedVariant(const TimedVariant& source);
        TimedVariant(const Variant& variant, const IceUtil::Time& time);
        TimedVariant(const VariantPtr& variant, const IceUtil::Time& time);

        template<class Var, class...Ts>
        TimedVariant(const Var& variant, std::chrono::duration<Ts...> duration) :
            TimedVariant(variant, IceUtil::Time::microSeconds(std::chrono::duration_cast<std::chrono::microseconds>(duration).count()))
        {}
        template<class Var, class...Ts>
        TimedVariant(const Var& variant, std::chrono::time_point<Ts...> timepoint) :
            TimedVariant(variant, timepoint.time_since_epoch())
        {}

        /**
         * Construct a TimedVariant from a non-VariantDataClass instance, e.g. from an int
         *
         * @tparam T            The desired type of the TimedVariant
         * @param var           The initialization value as a T-instance
         * @param t             For type checking only: Do not use
         */
        template <class T>
        TimedVariant(const T& var, const IceUtil::Time& time,
                     typename boost::disable_if_c < boost::is_base_of< VariantDataClass, T >::value
                     || boost::is_pointer<T>::value >::type* t = 0)
            : Variant(var)
        {
            timestamp = time.toMicroSeconds();
        }

        /**
         * Construct a TimedVariant from a string.
         *
         * @param var           The initialization value
         */
        TimedVariant(char const  var[], const IceUtil::Time& time)
            : Variant(var)
        {
            timestamp = time.toMicroSeconds();
        }

        ~TimedVariant() override;

        IceUtil::Time getTime() const;
        void setTime(const IceUtil::Time time)
        {
            timestamp = time.toMicroSeconds();
        }
        void setMicroseconds(Ice::Long microseconds)
        {
            timestamp = microseconds;
        }
        // TimedVariantBase interface
        //! Timestamp in micro seconds
        Ice::Long getTimestamp(const Ice::Current& c = Ice::Current()) const override;

        // Object interface
        Ice::ObjectPtr ice_clone() const override;

        std::string getOutputValueOnly() const override;
        friend std::ostream& operator<<(std::ostream& stream, const TimedVariantPtr& rhs)
        {
            if (rhs)
            {
                rhs->output(stream);
            }
            else
            {
                stream << "Null VariantPtr";
            }

            return stream;
        }

    };
}
extern template class ::IceInternal::Handle<::armarx::TimedVariant>;


