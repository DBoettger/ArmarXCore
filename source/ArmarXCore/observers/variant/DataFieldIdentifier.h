/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Core
* @author     Kai Welke (welke _at_ kit _dot_ edu)
* @date       2011 Kai Welke
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <ArmarXCore/core/system/ImportExport.h>
#include <ArmarXCore/interface/observers/DataFieldIdentifierBase.h>

#include <string>
#include <ostream>

namespace armarx
{
    class DataFieldIdentifier;
    /**
     * Typedef of DataFieldIdentifierPtr as IceInternal::Handle<DataFieldIdentifier> for convenience.
     */
    typedef IceInternal::Handle<DataFieldIdentifier> DataFieldIdentifierPtr;

    /**
    * DataFieldIdentifier provide the basis to identify data field within a distributed
    * ArmarX scenario.
    * Data fields are provided by observers which again are fed by SensorActor units.
    * An Observer allows defining several condition checks on data fields
    * that generate the events necessary for the robot program (Operation) logic.
    */
    class ARMARXCORE_IMPORT_EXPORT DataFieldIdentifier :
        virtual public DataFieldIdentifierBase
    {
    public:
        /**
        * Creates an empty DataFieldIdentifier. Required for Ice ObjectFactory
        */
        DataFieldIdentifier() {}

        /**
        * Creates a DataFieldIdentifier from string.
        *
        * @param dataFieldIdentifierStr string in the format "observername.channelname.datafieldname"
        */
        DataFieldIdentifier(std::string dataFieldIdentifierStr);

        /**
        * Creates a DataFieldIdentifier from string.
        *
        * @param observerName name of the observer
        * @param channelName name of the channel
        * @param datafieldName name of the datafield
        */
        DataFieldIdentifier(std::string observerName, std::string channelName, std::string datafieldName);

        /**
        * Retrieve data field identifier string
        *
        * @return string which identifies the datafield
        */
        std::string getIdentifierStr() const;

        /**
        * Test two DataFieldIdentifier for equality
        *
        * @param dataFieldIdentifier identifier to test for equality
        * @return equality
        */
        bool equals(const DataFieldIdentifier& dataFieldIdentifier);

        /**
        * Test if one DataFieldIdentifier contains the other
        *
        * @param dataFieldIdentifier identifier to check for standing at the beginning
        * @return beginsWith
        */
        bool beginsWith(const DataFieldIdentifier& dataFieldIdentifier);

        /**
        * Retrieve observer name
        *
        * @return name of the observer
        */
        std::string getObserverName() const
        {
            return observerName;
        }

        /**
        * Retrieve channel name
        *
        * @return name of the channel
        */
        std::string getChannelName() const
        {
            return channelName;
        }

        /**
        * Retrieve datafield name
        *
        * @return name of the datafield
        */
        std::string getDataFieldName() const
        {
            return datafieldName;
        }

        /**
        * stream operator for DataFieldIdentifier
        */
        friend std::ostream& operator<<(std::ostream& stream, const DataFieldIdentifier& rhs)
        {
            stream << rhs.getIdentifierStr();
            return stream;
        }

        /**
        * stream operator for Ice shared pointer of DataFieldIdentifier
        */
        friend std::ostream& operator<<(std::ostream& stream, const DataFieldIdentifierPtr& rhs)
        {
            stream << rhs->getIdentifierStr();
            return stream;
        }
    };
}
extern template class ::IceInternal::Handle< ::armarx::DataFieldIdentifier>;

