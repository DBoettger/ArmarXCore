/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <ArmarXCore/observers/Observer.h>
#include <ArmarXCore/interface/observers/ObserverInterface.h>

namespace armarx
{

    /**
     * \class DebugObserverPropertyDefinitions
     * \brief
     */
    class DebugObserverPropertyDefinitions:
        public ObserverPropertyDefinitions
    {
    public:
        DebugObserverPropertyDefinitions(std::string prefix):
            ObserverPropertyDefinitions(prefix)
        {
            defineOptionalProperty<std::string>("DebugObserverTopicName", "DebugObserver", "Name of the topic the DebugObserver listens on");

        }
    };

    /**
     * @defgroup Component-DebugObserver DebugObserver
     * @ingroup ObserversSub ArmarXCore-Components
     * Logging of frequently updated values to the console with printf, cout etc.
     * pollutes the console to an unreadable degree.
     * Therefore, the DebugObserver exists. The user can log data from anywhere
     * in the armarx framework by creating temporary observer channels on the DebugObserver.
     * These channels can be inspected in the armarx::ObserverGuiPlugin or, in the case of floats or ints,
     * in the armarx::ArmarXPlotter Widget.
     *
     * To interact with the DebugObserver, it need to be started since it is a seperate component (\ref DebugObserverApp).
     * Then the application needs to retrieve a proxy to this object (armarx::IceManager::getProxy(), \ref HowtoGetProxy).
     * With the proxy and the function setDebugDatafield() a channel and datafield can be created or
     * updated (if it already exists).
     * Unneeded channels or datafields can be removed with removeDebugChannel() resp. removeDebugDatafield().
     *
     * @class DebugObserver
     * @ingroup Component-DebugObserver
     * @brief The DebugObserver is a component for logging debug data, which is updated frequently.
     */
    class DebugObserver :
        public Observer,
        public DebugObserverInterface
    {
    public:
        DebugObserver();

        // framework hooks
        std::string getDefaultName() const override
        {
            return "DebugObserver";
        }
        void onInitObserver() override;
        void onConnectObserver() override;

        PropertyDefinitionsPtr createPropertyDefinitions() override;

        /**
         * @brief Creates or updates (if it already exists) a datafield in a channel.
         * @param channelName Name of the channel (anything the users likes)
         * @param datafieldName Name of the datafield (anything the users likes)
         * @param value A Variant that the user wants to log
         */
        void setDebugDatafield(const std::string& channelName, const std::string& datafieldName, const VariantBasePtr& value, const Ice::Current& c = Ice::Current()) override;
        void setDebugChannel(const std::string& channelName, const StringVariantBaseMap& valueMap, const Ice::Current& c = Ice::Current()) override;
        /**
         * @brief Removes a datafield from the DebugObserver. Should be called to clean up the DebugObserver.
         * @param channelName
         * @param datafieldName
         */
        void removeDebugDatafield(const ::std::string& channelName, const ::std::string& datafieldName, const ::Ice::Current& = ::Ice::Current()) override;
        /**
         * @brief Removes a channel and all its datafield it has.
         * @param channelName
         */
        void removeDebugChannel(const ::std::string& channelName, const ::Ice::Current& = ::Ice::Current()) override;
        /**
         * @brief Removes all channels.
         */
        void removeAllChannels(const ::Ice::Current& = ::Ice::Current()) override;
    };

}

