/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarX::Core
 * @author     Peter Kaiser <peter.kaiser@kit.edu>
 * @date       2014
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <ArmarXCore/observers/variant/Variant.h>
#include <ArmarXCore/interface/observers/Matrix.h>

#include <Eigen/Core>
//#include <Eigen/Geometry>

namespace armarx
{
    namespace VariantType
    {
        // Variant types
        const VariantTypeId MatrixFloat = Variant::addTypeName("::armarx::MatrixFloatBase");
        const VariantTypeId MatrixDouble = Variant::addTypeName("::armarx::MatrixDoubleBase");
    }


    /**
     * @class MatrixFloat
     * @ingroup VariantsGrp
     * @brief The MatrixFloat class
     */
    class MatrixFloat : virtual public MatrixFloatBase
    {
        template <class BaseClass, class VariantClass>
        friend class GenericFactory;
    protected:
        MatrixFloat();
    public:
        MatrixFloat(int rows, int cols);
        MatrixFloat(const Eigen::MatrixXf&);
        MatrixFloat(int rows, int cols, const std::vector<float>& entries);

        //void setMatrix(int width, int height, const std::vector<float> &entries);

        virtual Eigen::MatrixXf toEigen() const;

        std::vector<float> toVector() const;

        // inherited from VariantDataClass
        Ice::ObjectPtr ice_clone() const override
        {
            return this->clone();
        }
        VariantDataClassPtr clone(const Ice::Current& c = ::Ice::Current()) const override
        {
            return new MatrixFloat(*this);
        }
        std::string output(const Ice::Current& c = ::Ice::Current()) const override
        {
            std::stringstream s;
            s << toEigen();
            return s.str();
        }
        VariantTypeId getType(const Ice::Current& c = ::Ice::Current()) const override
        {
            return VariantType::MatrixFloat;
        }
        bool validate(const Ice::Current& c = ::Ice::Current()) override
        {
            return true;
        }

        float& operator()(const int x, const int y);

        std::string toJsonRowMajor();

        friend std::ostream& operator<<(std::ostream& stream, const MatrixFloat& rhs)
        {
            stream << "MatrixFloat: " << std::endl << rhs.output() << std::endl;
            return stream;
        }

    public:
        // serialization
        void serialize(const armarx::ObjectSerializerBasePtr& serializer, const ::Ice::Current& = ::Ice::Current()) const override;
        void deserialize(const armarx::ObjectSerializerBasePtr& serializer, const ::Ice::Current& = ::Ice::Current()) override;
    };

    typedef IceInternal::Handle<MatrixFloat> MatrixFloatPtr;



    /**
     * @class MatrixDouble
     * @ingroup VariantsGrp
     * @brief The MatrixDouble class
     */
    class MatrixDouble : virtual public MatrixDoubleBase
    {
        template <class BaseClass, class VariantClass>
        friend class GenericFactory;
    protected:
        MatrixDouble();
    public:
        MatrixDouble(int rows, int cols);
        MatrixDouble(const Eigen::MatrixXd&);
        MatrixDouble(int rows, int cols, const std::vector<double>& entries);

        //void setMatrix(int width, int height, const std::vector<double> &entries);

        virtual Eigen::MatrixXd toEigen() const;

        // inherited from VariantDataClass
        Ice::ObjectPtr ice_clone() const override
        {
            return this->clone();
        }
        VariantDataClassPtr clone(const Ice::Current& c = ::Ice::Current()) const override
        {
            return new MatrixDouble(*this);
        }
        std::string output(const Ice::Current& c = ::Ice::Current()) const override
        {
            std::stringstream s;
            s << toEigen();
            return s.str();
        }
        VariantTypeId getType(const Ice::Current& c = ::Ice::Current()) const override
        {
            return VariantType::MatrixDouble;
        }
        bool validate(const Ice::Current& c = ::Ice::Current()) override
        {
            return true;
        }

        double& operator()(const int x, const int y);

        std::string toJsonRowMajor();


        friend std::ostream& operator<<(std::ostream& stream, const MatrixDouble& rhs)
        {
            stream << "MatrixDouble: " << std::endl << rhs.output() << std::endl;
            return stream;
        }

    public:
        // serialization
        void serialize(const armarx::ObjectSerializerBasePtr& serializer, const ::Ice::Current& = ::Ice::Current()) const override;
        void deserialize(const armarx::ObjectSerializerBasePtr& serializer, const ::Ice::Current& = ::Ice::Current()) override;
    };

    typedef IceInternal::Handle<MatrixDouble> MatrixDoublePtr;
}
