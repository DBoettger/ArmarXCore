/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2014
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#pragma once

#include <ArmarXCore/core/system/FactoryCollectionBase.h>
#include <ArmarXCore/observers/variant/Variant.h>
#include "MatrixVariant.h"
#include <Ice/ObjectFactory.h>

namespace armarx
{


    namespace ObjectFactories
    {

        /**
        * @class ObserverObjectFactories
        * @brief
        */
        class Eigen3VariantObjectFactories : public FactoryCollectionBase
        {
        public:
            ObjectFactoryMap getFactories() override
            {
                ObjectFactoryMap map;

                add<MatrixFloatBase, MatrixFloat>(map);
                add<MatrixDoubleBase, MatrixDouble>(map);

                return map;
            }
            static const FactoryCollectionBaseCleanUp VariantObjectFactoriesVar;
        };

    }

}

