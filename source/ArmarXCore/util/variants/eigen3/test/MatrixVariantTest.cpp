/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarXCore::core::Test
* @author     Nils Adermann (naderman at naderman dot de)
* @date       2010
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#define BOOST_TEST_MODULE ArmarX::Core::DataPath
#define ARMARX_BOOST_TEST
#include <ArmarXCore/Test.h>
#include <ArmarXCore/core/system/ArmarXDataPath.h>
#include <ArmarXCore/core/system/cmake/ArmarXPackageToolInterface.h>
#include <ArmarXCore/core/system/cmake/CMakePackageFinder.h>
#include <ArmarXCore/core/test/IceTestHelper.h>
#include <ArmarXCore/core/application/Application.h>
#include <ArmarXCore/util/json/JSONObject.h>
#include <ArmarXCore/util/variants/eigen3/MatrixVariant.h>

#include <iostream>

using namespace armarx;



BOOST_AUTO_TEST_CASE(testMatrixSerialization)
{
    IceTestHelper ice;

    JSONObjectPtr json = new JSONObject(ice.getCommunicator());

    Eigen::Matrix4d mat = Eigen::Matrix4d::Identity();
    MatrixDoublePtr m = new MatrixDouble(mat);
    m->serialize(json);
    ARMARX_INFO_S << json->toString();
    MatrixDoublePtr m2 = new MatrixDouble(mat);
    m2->deserialize(json);
    BOOST_CHECK(mat.isApprox(m2->toEigen()));
    ARMARX_INFO_S << m2->toEigen();

}




