/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX
* @author     Mirko Waechter( mirko.waechter at kit dot edu)
* @date       2016
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/
#pragma once


#include "TopicUtil.h"
#include <boost/shared_ptr.hpp>
#include <vector>
#include <string>


namespace armarx
{

    class TopicReaderInterface
    {
    public:
        TopicReaderInterface();
        virtual bool read(TopicUtil::TopicData& data) = 0;
        virtual bool seekTo(IceUtil::Time timestamp) = 0;
        virtual IceUtil::Time getReplayLength() = 0;
        virtual std::vector<std::string> getReplayTopics() = 0;
    };

    typedef boost::shared_ptr<TopicReaderInterface> TopicReaderInterfacePtr;

} // namespace armarx

