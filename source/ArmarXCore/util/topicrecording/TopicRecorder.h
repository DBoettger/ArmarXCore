/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX
* @author     Mirko Waechter( mirko.waechter at kit dot edu)
* @date       2016
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/
#pragma once



#include "GenericTopicSubscriber.h"
#include "TopicWriterInterface.h"

#include <ArmarXCore/interface/components/TopicRecorderInterface.h>

#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/system/Synchronization.h>

#include <ArmarXCore/core/services/tasks/RunningTask.h>
#include <boost/filesystem/path.hpp>

#include <mutex>

namespace armarx
{

    class TopicRecorderProperties :
        public ComponentPropertyDefinitions
    {
    public:
        TopicRecorderProperties(std::string prefix):
            ComponentPropertyDefinitions(prefix)
        {
            defineRequiredProperty<std::string>("TopicsToLog", "Comma seperated list of topics to log. Use * to record all. To specify the max frequency append a : to the topic name, e.g. \"RobotState:10,Log\" Topic names must not contain : or ,");
            defineOptionalProperty<std::string>("Outputfile", "topic_recording.bag", "File to write the output to. The file will be overwritten if the file exists");
            defineOptionalProperty<bool>("TimestampedFilename", true, "If true, a timestamp is added to the filename");
            defineOptionalProperty<std::string>("StorageMode", "database", "Storage variant to use, currently 'database' (default) and 'file' are available");
            defineOptionalProperty<bool>("EnableRecording", true, "Immediately start recording after the component is launched.");
            defineOptionalProperty<int>("Duration", 0, "Limit recording.");
        }
    };

    /**
     * \page ArmarXCore-TopicRecording Recording and Replaying Topics
     * It is possible to easily record and replay any topic available in ArmarX.
     * For recording a topic use the app *TopicRecorderAppRun* like this:
     * \verbatim
     $ARMARX_CORE/build/bin/TopicRecorderAppRun --ArmarX.TopicRecorder.Outputfile=test.bag --ArmarX.TopicRecorder.TopicsToLog=* --ArmarX.TopicRecorder.TimestampedFilename=false
     \endverbatim
     * This will log any topic that was running, when the recorder was **started** and write it to the file *test.bag*.
     *
     * To replay this file, use the GUI \ref ArmarXGui-GuiPlugins-TopicReplayer or use the command line tool *TopicReplayerrAppRun* like this:
     * \verbatim
     $ARMARX_CORE/build/bin/TopicReplayerAppRun --ArmarX.TopicReplayer.RecordFile=test.bag
     \endverbatim
     * This will replay the recorded topics in the same speed as they were recorded.
     *
     * You can add the optional values
     * \verbatim
     --ArmarX.TopicRecorder.StorageMode="database"
     --ArmarX.TopicReplayer.StorageMode="database"
     \endverbatim
     * or
     * \verbatim
     --ArmarX.TopicRecorder.StorageMode="file"
     --ArmarX.TopicReplayer.StorageMode="file"
     \endverbatim
     * to the Recorder and Replayer to switch between the available storage modes, which currently are *sqlite3* database and simple file storage in JSON format with base64 data-encoding.
     * If you do not specify the storage mode, the default value 'database' is used.
     *
     * If you want to play the topic alongside their normal components you need to redirect their topic output.
     * You can do this by adding the following property to all or some components:
     * \verbatim
     --ArmarX.TopicSuffix=/dev/null
     \endverbatim
     * This will add a topic suffix to all topics this component requested with the armarx::IceManager convenience functions
     * (does not work if the component used Ice directly).
     */

    class TopicRecorderComponent : public Component,
        public TopicRecorderInterface
    {
    public:
        TopicRecorderComponent();


        void startRecording(const int maxDuration, const Ice::Current& c = Ice::Current()) override;

        void stopRecording(const Ice::Current& c = Ice::Current()) override;

        void setOutputFilename(const std::string& newFilename, const Ice::Current& c = Ice::Current()) override
        {
            outputFilename = newFilename;
        }


        bool isRecording(const Ice::Current& c = Ice::Current()) override
        {
            return isRecordingEnabled;
        }


        // PropertyUser interface
        PropertyDefinitionsPtr createPropertyDefinitions() override;
        void wakeUp();

        // ManagedIceObject interface
    protected:
        void onInitComponent() override;
        void onConnectComponent() override;
        void onDisconnectComponent() override;
        void onExitComponent() override;
        std::string getDefaultName() const override;

        void write();

        // Blobject interface

    protected:
        TopicWriterInterfacePtr writer;

        IceUtil::Time startTime;

        RunningTask<TopicRecorderComponent>::pointer_type queueTask;
        std::map<std::string, GenericTopicSubscriberPtr> topicsSubscribers;
        Mutex queueMutex;
        boost::condition_variable idleCondition;
        boost::shared_ptr<std::ofstream> log;
        boost::filesystem::path outputfilePath;
        std::string outputFilename;

        int maxDuration;

        bool isRecordingEnabled;

        std::mutex mutex;

        TopicRecorderListenerInterfacePrx topicRecoderListener;


    };

} // namespace armarx


