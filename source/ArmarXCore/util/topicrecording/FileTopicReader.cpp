/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX
* @author     Mirko Waechter( mirko.waechter at kit dot edu)
* @date       2016
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/
#include "FileTopicReader.h"
#include <fstream>
#include <ArmarXCore/util/json/JSONObject.h>

namespace armarx
{

    FileTopicReader::FileTopicReader(std::istream* stream) :
        stream(stream)
    {


    }

    FileTopicReader::FileTopicReader(boost::filesystem::path path)
    {
        std::ifstream* i = new std::ifstream(path.string().c_str());
        if (!i->is_open())
        {
            ARMARX_ERROR_S << "Could not open '" << path.string() << "'!";
        }
        stream = i;
    }

    FileTopicReader::~FileTopicReader()
    {
        if (!filepath.empty())
        {
            delete stream;
        }
    }

    bool FileTopicReader::read(TopicUtil::TopicData& data)
    {
        std::string line;
        if (!std::getline(*stream, line))
        {
            return false;
        }
        JSONObjectPtr json = new JSONObject();
        json->fromString(line);
        auto payloadEncoded = json->getString("data");
        data.topicName = json->getString("topic");
        std::string payloadStr = TopicUtil::Decode64(payloadEncoded);
        data.inParams.assign(payloadStr.begin(), payloadStr.end());
        data.operationName = json->getString("op");
        data.timestamp = IceUtil::Time::microSecondsDouble(json->getDouble("t"));
        return true;
    }

    bool FileTopicReader::seekTo(IceUtil::Time timestamp)
    {
        //Seeking is not supported
        return false;
    }

    IceUtil::Time FileTopicReader::getReplayLength()
    {
        //Finding length of replay file is not supported
        return IceUtil::Time::microSeconds(0);
    }

    std::vector<std::string> FileTopicReader::getReplayTopics()
    {
        return std::vector<std::string>();
    }

} // namespace armarx
