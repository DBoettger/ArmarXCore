/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarX
 * @author     Mirko Waechter( mirko.waechter at kit dot edu)
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#pragma once

#include <string>
#include <vector>
#include <IceUtil/Time.h>
#include <Ice/BuiltinSequences.h>


namespace armarx
{

    namespace TopicUtil
    {
        struct TopicData
        {
            TopicData() {}
            TopicData(const std::string& topicName,
                      const IceUtil::Time& timestamp,
                      const std::string& operationName,
                      const std::vector<Ice::Byte>& inParams);

            std::string topicName;
            IceUtil::Time timestamp;
            std::string operationName;
            std::vector<Ice::Byte> inParams;

        };


        std::string Encode64(const std::string& val);

        std::string Decode64(const std::string& val);
    }

}

