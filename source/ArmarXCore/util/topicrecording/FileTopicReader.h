/*
* This file is part of ArmarX.
*
* Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX
* @author     Mirko Waechter( mirko.waechter at kit dot edu)
* @date       2016
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/
#pragma once

#include "TopicReaderInterface.h"

#include <boost/filesystem/path.hpp>

namespace armarx
{

    class FileTopicReader : public TopicReaderInterface
    {
    public:
        FileTopicReader(std::istream* stream);
        FileTopicReader(boost::filesystem::path path);
        ~FileTopicReader();
        // TopicReaderInterface interface
    public:
        bool read(TopicUtil::TopicData& data) override;
        bool seekTo(IceUtil::Time timestamp) override;
        IceUtil::Time getReplayLength() override;
        std::vector<std::string> getReplayTopics() override;
    private:
        std::istream* stream;
        boost::filesystem::path filepath;
    };

    typedef boost::shared_ptr<FileTopicReader> FileTopicReaderPtr;
} // namespace armarx

