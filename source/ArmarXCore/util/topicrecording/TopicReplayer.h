/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX
* @author     Mirko Waechter( mirko.waechter at kit dot edu)
* @date       2016
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/
#pragma once


#include <ArmarXCore/interface/components/TopicRecorderInterface.h>

#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/time/TimeKeeper.h>
#include <ArmarXCore/core/services/tasks/RunningTask.h>

#include <ArmarXCore/observers/DebugObserver.h>

#include "TopicReaderInterface.h"

#include <unordered_set>


namespace armarx
{

    class TopicReplayerProperties :
        public ComponentPropertyDefinitions
    {
    public:
        TopicReplayerProperties(std::string prefix):
            ComponentPropertyDefinitions(prefix)
        {
            defineOptionalProperty<bool>("Loop", false, "Loop playback");
            defineRequiredProperty<std::string>("RecordFile", "File with recorded data");
            defineOptionalProperty<std::string>("StorageMode", "database", "Storage variant to use, currently 'database' (default) and 'file' are available");
            defineOptionalProperty<std::string>("DebugObserverName", "DebugObserver", "Name of the topic the DebugObserver listens on");
        }
    };


    class TopicReplayer : public Component
    {
    public:
        TopicReplayer();

        std::string getDefaultName() const override;
        void pauseReplay();
        void resumeReplay();
        void setReplaySpeed(double factor);

        std::vector<std::string> getRecordedTopics();
        void setReplayingTopics(std::vector<std::string> topics);

        IceUtil::Time getReplayLength() const;
        IceUtil::Time getCurrentTimePosition() const;
        void jumpToPosition(IceUtil::Time timestamp);
        void setAutoplay(bool autoplay);
        void setLoop(bool loop);

        // ManagedIceObject interface
        PropertyDefinitionsPtr createPropertyDefinitions() override;
    protected:
        void onInitComponent() override;
        void onConnectComponent() override;
        void onDisconnectComponent() override;

        void play();
        RunningTask<TopicReplayer>::pointer_type task;

    private:
        TopicReaderInterfacePtr replayer;
        TopicReplayerListenerInterfacePrx replayerListener;
        TimeKeeper timeKeeper;
        bool autoplay = true;
        bool loop;
        std::unordered_set<std::string> replayingTopics;
        bool replayingTopicsNotSupportedByFile = false;
        DebugObserverInterfacePrx debugObserver;
    };

}

