/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX
* @author     Mirko Waechter( mirko.waechter at kit dot edu)
* @date       2016
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/
#pragma once

#include "TopicWriterInterface.h"

#include <Ice/Object.h>

#include <queue>

#include <ArmarXCore/core/system/Synchronization.h>


namespace armarx
{

    class TopicRecorderComponent;
    typedef IceInternal::Handle<TopicRecorderComponent> TopicRecorderComponentPtr;

    class GenericTopicSubscriber : public Ice::Blobject
    {
    public:
        GenericTopicSubscriber(TopicRecorderComponentPtr recorder, const std::string& topicName, IceUtil::Time startTimestamp, float maxFrequency = -1.0f);
        void getData(std::queue<TopicUtil::TopicData>& data);

        void setTime(IceUtil::Time time)
        {
            startTimestamp = time;
        }

        // Blobject interface
    public:
        bool ice_invoke(const std::vector<Ice::Byte>& inParams, std::vector<Ice::Byte>& outParams, const Ice::Current& current) override;
    private:
        bool checkTimestamp(const std::string& operationName, const IceUtil::Time& timestamp);
        std::string topicName;
        std::queue<TopicUtil::TopicData> dataQueue;
        Mutex queueMutex;
        TopicRecorderComponentPtr recorder;
        IceUtil::Time startTimestamp;
        float maxFrequency;
        std::map<std::string, IceUtil::Time> functionCallTimestamps;
    };

    typedef IceInternal::Handle<GenericTopicSubscriber> GenericTopicSubscriberPtr;


}

