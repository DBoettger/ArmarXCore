/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Cedric Seehausen (usdnr at kit dot edu)
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include "DependenciesGenerator.h"

#include <ArmarXCore/core/system/cmake/CMakePackageFinder.h>
#include <ArmarXCore/core/system/ArmarXDataPath.h>
#include <ArmarXCore/core/logging/Logging.h>
#include <boost/filesystem.hpp>
#include <fstream>


using namespace ScenarioManager;
using namespace Parser;
using namespace Data_Structure;
using namespace std;
using namespace armarx;

std::map<std::string, DependencyTree> DependenciesGenerator::cachedTrees;

DependenciesGenerator::DependenciesGenerator()
{
}

DependencyTree DependenciesGenerator::getDependencieTree(string packageName)
{
    if (cachedTrees.count(packageName))
    {
        return cachedTrees[packageName];
    }
    CMakePackageFinder parser = CMakePackageFinderCache::GlobalCache.findPackage(packageName);

    vector<string> dependencies = parser.getDependencies();
    vector<string> depsConcat = dependencies;

    if (!dependencies.empty() && !dependencies.at(0).empty())
    {
        for (auto package : dependencies)
        {
            vector<string> deps = getDependencieTree(package);
            for (auto dep : deps)
            {
                if (std::find(depsConcat.begin(), depsConcat.end(), dep) == depsConcat.end())
                {
                    depsConcat.push_back(dep);
                }
            }
        }
    }
    else
    {
        dependencies.clear();
        dependencies.push_back(packageName);
        cachedTrees[packageName] = dependencies;
        return dependencies;
    }

    depsConcat.push_back(packageName);

    cachedTrees[packageName] = depsConcat;

    return depsConcat;
}

void DependenciesGenerator::generateDependenciesCfg(PackagePtr package)
{
    if (package.get() == nullptr)
    {
        ARMARX_WARNING_S << "Unable to generate Dependencies cfg for unknown package";
    }
    boost::filesystem::path cacheFolderPath = boost::filesystem::path(ArmarXDataPath::GetCachePath()) / boost::filesystem::path("ComponentFiles/");

    if (!boost::filesystem::exists(cacheFolderPath))
    {
        boost::filesystem::create_directories(cacheFolderPath);
        // return value of create_directories is buggy ?! check again
        if (!boost::filesystem::exists(cacheFolderPath))
        {
            ARMARX_ERROR_S << "Could not create Cache folder for ScenarioManagerPlugin at " << cacheFolderPath.string();
        }
    }

    boost::filesystem::path cacheFilePath = cacheFolderPath / boost::filesystem::path("./" + package->getName() + ".dependencies.cfg");

    ofstream out(cacheFilePath.string());

    out << "ArmarX.ProjectName=" << package->getName() << endl;

    DependencyTree deps = getDependencieTree(package->getName());
    string dataPaths = "";
    string depsString = "";

    size_t pos = 0;
    for (auto dep : deps)
    {
        CMakePackageFinder pFinder = CMakePackageFinderCache::GlobalCache.findPackage(dep);
        dataPaths += pFinder.getDataDir();
        depsString += dep;

        if (pos != deps.size() - 1)
        {
            dataPaths += ";";
            depsString += ";";
        }

        ++pos;
    }

    out << "ArmarX.ProjectDatapath=" << dataPaths << endl;
    out << "ArmarX.ProjectDependencies=" << depsString << endl;

    out.close();
}
