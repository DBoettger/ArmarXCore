/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Cedric Seehausen (usdnr at kit dot edu)
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once

#include "../data_structure/Package.h"
#include <vector>
#include <string>
#include <map>
#include <ArmarXCore/core/system/cmake/CMakePackageFinderCache.h>

namespace ScenarioManager
{
    namespace Parser
    {
        typedef std::vector<std::string> DependencyTree;

        class DependenciesGenerator
        {
        public:
            DependenciesGenerator();

            static DependencyTree getDependencieTree(std::string packageName);

            void generateDependenciesCfg(ScenarioManager::Data_Structure::PackagePtr package);

        private:
            static std::map<std::string, DependencyTree> cachedTrees;
        };
    }
}

