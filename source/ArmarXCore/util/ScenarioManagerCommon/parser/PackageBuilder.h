/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Cedric Seehausen (usdnr at kit dot edu)
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once

#include <ArmarXCore/util/ScenarioManagerCommon/data_structure/Package.h>
#include <string>
#include <map>



namespace ScenarioManager
{
    namespace Parser
    {
        typedef std::vector<std::string> StringList;

        /**
        * @class CMakeParser
        * @ingroup parser
        * @brief This class has multiple methods for extracting data out of packages, based on their name.
        */
        class PackageBuilder
        {

        public:
            /**
            * Parses the package of the given name and creates a Package out of the parsed data.
            * @param name name of the package to be parsed
            * @return a Package containing the data parsed out of the original package
            */
            ScenarioManager::Data_Structure::PackagePtr parsePackage(std::string name);

            /**
             * @brief Parses the package of the given name and creates a Package out of the parsed data.
             * @param name name of the package to be parsed
             * @param openedScenarios filters the package scenarios
             * @return
             */
            ScenarioManager::Data_Structure::PackagePtr parsePackage(std::string name, StringList openedScenarios);

            /**
            * @return {@code true} if a package with the given name was found
            */
            bool packageFound(std::string name);

            static StringList FilterPackagesForScenario(std::string scenario);

            static StringList GetDefaultPackages();

        private:
            static std::map<std::string, StringList> cachedTrees;
        };
    }
}
