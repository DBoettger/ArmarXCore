/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Cedric Seehausen (usdnr at kit dot edu)
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include "PackageBuilder.h"
#include "ArmarXCore/core/application/Application.h"
#include "ArmarXCore/util/ScenarioManagerCommon/parser/StringUtil.hpp"
#include "ArmarXCore/util/ScenarioManagerCommon/parser/DependenciesGenerator.h"
#include "ArmarXCore/util/ScenarioManagerCommon/parser/XMLScenarioParser.h"
#include "ArmarXCore/core/system/cmake/CMakePackageFinder.h"
#include "ArmarXCore/core/logging/Logging.h"
#include "ArmarXCore/core/system/ArmarXDataPath.h"
#include <boost/filesystem.hpp>
#include <boost/algorithm/string.hpp>
#include <algorithm>
#include <fstream>

using namespace ScenarioManager;
using namespace Parser;
using namespace Data_Structure;
using namespace std;

std::map<std::string, DependencyTree> PackageBuilder::cachedTrees;

PackagePtr PackageBuilder::parsePackage(string name)
{
    armarx::CMakePackageFinder pFinder(name);
    XMLScenarioParser parser;

    StringList scenarios = parser.getScenariosFromFolder(pFinder.getScenariosDir());

    //format them into the right parsing
    int i = 0;
    for (auto scenario : scenarios)
    {
        scenario.append("::Package::" + name);
        scenarios[i] = scenario;
        ARMARX_INFO_S << scenario << " " << scenarios[i];
        i++;
    }

    return parsePackage(name, scenarios);
}

PackagePtr PackageBuilder::parsePackage(string name, StringList openedScenarios)
{
    armarx::CMakePackageFinder pFinder = armarx::CMakePackageFinderCache::GlobalCache.findPackage(name);

    if (pFinder.packageFound())
    {
        PackagePtr result;

        result = PackagePtr(new Package(name, pFinder.getConfigDir(), pFinder.getScenariosDir()));

        DependenciesGenerator generator;
        generator.generateDependenciesCfg(result);

        std::vector<string> executables = Split(pFinder.getExecutables(), ' ');

        for (auto executable : executables)
        {
            //reduce Run from the name
            size_t index = executable.rfind("Run");
            if (index != std::string::npos)
            {
                Data_Structure::ApplicationPtr application(new Application(executable.substr(0, index), pFinder.getBinaryDir(), result->getName()));
                result->addApplication(application);
            }
        }

        std::string scenarioDir = pFinder.getScenariosDir();
        XMLScenarioParser parser;
        vector<string> packageScenarios = parser.getScenariosFromFolder(scenarioDir);

        for (auto scenarioName : packageScenarios)
        {
            for (unsigned int i = 0; i < openedScenarios.size(); i++)
            {
                StringList stringSplit;

                auto pos = openedScenarios[i].find("::Package::");
                stringSplit.push_back(openedScenarios[i].substr(0, pos));
                stringSplit.push_back(openedScenarios[i].substr(pos + 11));

                if (scenarioName.compare(stringSplit.at(0)) == 0 && result->getName().compare(stringSplit.at(1)) == 0)
                {
                    ScenarioPtr scenario;

                    auto subfolderPos = scenarioName.find("/");
                    if (subfolderPos != string::npos)
                    {
                        scenario = parser.parseScenario(result, scenarioName.substr(subfolderPos + 1), scenarioName.substr(0, subfolderPos));
                    }
                    else
                    {
                        scenario = parser.parseScenario(result, scenarioName);
                    }

                    if (scenario.get() != nullptr)
                    {
                        scenario->reloadAppInstances();
                        result->addScenario(scenario);
                    }
                    else
                    {
                        ARMARX_INFO_S << "Could not find / parse Scenario at " + scenarioDir + " despite CMake knows it.";
                    }
                    continue;
                }
            }
        }
        return result;
    }
    return PackagePtr();
}

StringList PackageBuilder::FilterPackagesForScenario(std::string scenario)
{
    StringList result;

    StringList packages = GetDefaultPackages();
    for (auto package : packages)
    {
        armarx::CMakePackageFinder finder = armarx::CMakePackageFinderCache::GlobalCache.findPackage(package);
        XMLScenarioParser parser;

        StringList scenarios = parser.getScenariosFromFolder(finder.getScenariosDir());

        if (std::find(scenarios.begin(), scenarios.end(), scenario) != scenarios.end())
        {
            result.push_back(package);
        }
    }

    return result;
}

StringList PackageBuilder::GetDefaultPackages()
{
    StringList packages;
    if (armarx::Application::getInstance())
    {
        return armarx::Application::getInstance()->getDefaultPackageNames();
    }
    return packages;
}
