/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Cedric Seehausen (usdnr at kit dot edu)
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include "iceparser.h"

#include <ArmarXCore/core/rapidxml/wrapper/RapidXmlReader.h>
#include <ArmarXCore/core/rapidxml/wrapper/RapidXmlWriter.h>

#include <ArmarXCore/core/application/properties/PropertyDefinitionContainerFormatter.h>
#include <ArmarXCore/core/application/properties/PropertyDefinitionFormatter.h>
#include <ArmarXCore/core/application/properties/PropertyDefinitionConfigFormatter.h>
#include <ArmarXCore/core/logging/Logging.h>
#include <ArmarXCore/core/system/ArmarXDataPath.h>
#include <Ice/Properties.h>


#include <boost/filesystem.hpp>
#include <iostream>
#include <algorithm>
#include <fstream>

using namespace ScenarioManager;
using namespace Data_Structure;
using namespace Parser;
using namespace rapidxml;
using namespace std;
using namespace armarx;

armarx::PropertyDefinitionsPtr IceParser::mergeXmlAndCfg(ApplicationInstanceWPtr wApp, bool firstLoad)
{
    ApplicationInstancePtr app = wApp.lock();

    std::string configPath = app->getConfigPath();

    std::string cachePath = ArmarXDataPath::GetCachePath();

    if (!boost::filesystem::exists(boost::filesystem::path(configPath)))
    {
        std::ofstream out(configPath);
        out.close();
    }

    Ice::PropertiesPtr cfgProperties = IceProperties::create();
    armarx::IceProperties* propsInternals = static_cast<armarx::IceProperties*>(cfgProperties.get());
    propsInternals->setInheritanceSolver(nullptr);

    cfgProperties->load(configPath);


    boost::filesystem::path xmlPath = boost::filesystem::path(cachePath) / boost::filesystem::path("ComponentFiles") / boost::filesystem::path(app->getPackageName() + "." + app->getName() + ".xml");
    if (!boost::filesystem::exists(xmlPath))
    {
        ARMARX_WARNING_S << "Could not find Cached XML file at " << xmlPath;
        PropertyDefinitionsPtr result = PropertyDefinitionsPtr(new PropertyDefinitionContainer(""));
        result->setProperties(cfgProperties);
        return result;
    }

    armarx::PropertyDefinitionsPtr xmlProperties = loadFromXml(xmlPath.string(), app);

    Ice::PropertyDict dict = cfgProperties->getPropertiesForPrefix("");
    for (auto const& property : dict)
    {
        //If it is no default property just set it with no Description
        if (!app->isDefaultProperty(property.first))
        {
            xmlProperties->defineOptionalProperty(property.first, property.second, "No Description");
            xmlProperties->getProperties()->setProperty(property.first, property.second);
            app->setIsDefaultProperty(property.first, false);
        }
        //If it is an default property set it to enabled and set the value
        else
        {
            if (!firstLoad)
            {
                if (property.second.compare("<set value!>") &&
                    property.second.compare("::_NOT_SET_::"))
                {
                    app->setDefaultPropertyEnabled(property.first, true);
                    xmlProperties->getProperties()->setProperty(property.first, property.second);
                }
            }
        }

    }

    return xmlProperties;
}

//used to create the default ini of an ApplicationInstance
armarx::PropertyDefinitionsPtr IceParser::loadFromXml(std::string path, ApplicationInstanceWPtr wApp)
{
    ApplicationInstancePtr app = wApp.lock();

    Ice::PropertiesPtr properties = IceProperties::create();
    armarx::IceProperties* cfgInternal = static_cast<armarx::IceProperties*>(properties.get());
    cfgInternal->setInheritanceSolver(nullptr);

    PropertyDefinitionsPtr container = PropertyDefinitionsPtr(new PropertyDefinitionContainer(""));
    container->setProperties(properties);

    RapidXmlReaderPtr reader;
    try
    {
        reader = RapidXmlReader::FromFile(path);
    }
    catch (...)
    {
        ARMARX_INFO_S << "Failed to parse " << path << std::endl;
        ARMARX_INFO_S << "It is most likely that the File has no valid XML formatting" << std::endl;
        return container;
    }

    for (RapidXmlReaderNode property_node = reader->getRoot("property"); property_node.is_valid(); property_node = property_node.next_sibling())
    {
        const string propertyName = property_node.attribute_value("name");
        const string description = property_node.first_node("description").value();
        RapidXmlReaderNode attributes_node = property_node.first_node("attributes");

        RapidXmlReaderNode current_attribute = attributes_node.first_node("attribute");

        string defaultValue = "";
        //bool caseSensitive = false;
        bool required = false;

        while (current_attribute.is_valid())
        {
            //if it is no attribute continue to the next sibling
            if (current_attribute.name().compare("attribute") != 0)
            {

            }
            else if (current_attribute.attribute_value("name").find("Default") != string::npos)
            {
                defaultValue = current_attribute.value();
            }
            else if (current_attribute.attribute_value("name").find("CaseSensitivity") != string::npos)
            {
            }
            else if (current_attribute.attribute_value("name").find("Required") != string::npos)
            {
                if (current_attribute.value().compare("yes") == 0)
                {
                    required = true;
                }
            }
            current_attribute = current_attribute.next_sibling();
        }

        if (required)
        {
            container->defineRequiredProperty<std::string>(propertyName, description);
            container->getProperties()->setProperty(propertyName, "::_NOT_SET_::");
        }
        else
        {
            container->defineOptionalProperty(propertyName, defaultValue, description);
            container->getProperties()->setProperty(propertyName, defaultValue);
        }
        //Always set default propertys disabled
        //they get enabled by checking the cfg file (see mergeXmlAndCfg)
        if (app.get() != nullptr)
        {
            app->setIsDefaultProperty(propertyName, true);
            app->setDefaultPropertyEnabled(propertyName, false);
        }
    }

    return container;
}

void IceParser::saveCfg(ScenarioManager::Data_Structure::ApplicationInstanceWPtr wAppInstance)
{
    ApplicationInstancePtr appInstance = wAppInstance.lock();

    if (!appInstance->isConfigWritable())
    {
        return;
    }

    PropertyDefinitionsPtr props = appInstance->getProperties();
    armarx::IceProperties* propsInternals = static_cast<armarx::IceProperties*>(props->getProperties().get());
    propsInternals->setInheritanceSolver(nullptr);

    std::string resultStr;

    PropertyDefinitionFormatter* defFormatter = new PropertyDefinitionConfigFormatter();
    PropertyDefinitionContainerFormatter*  pdcFormatter = new PropertyDefinitionContainerFormatter(*defFormatter);
    pdcFormatter->setProperties(props->getProperties());
    resultStr = pdcFormatter->formatPropertyDefinitionContainer(props);

    Ice::PropertyDict dict = props->getProperties()->getPropertiesForPrefix("");

    //disable all not enabled default properties
    for (auto const& property : dict)
    {
        if (appInstance->isDefaultProperty(property.first) && !appInstance->isDefaultPropertyEnabled(property.first))
        {
            disableProperty(resultStr, property.first);
        }
    }

    //filter Ice.Config
    size_t begin = resultStr.rfind("# Ice.Config:");
    size_t end = resultStr.rfind("Ice.Config = <set value!>");

    if (end == std::string::npos)
    {
        (end = resultStr.rfind("Ice.Config = \"\"")) != std::string::npos ? end += 15 : end = std::string::npos;
    }
    else
    {
        end += 25;
    }
    if (begin != std::string::npos && end != std::string::npos)
    {
        resultStr.erase(begin, end - begin);
    }

    std::ofstream cfgFile;
    cfgFile.open(appInstance->getConfigPath(), std::ofstream::out | std::ofstream::trunc);

    if (cfgFile.fail())
    {
        ARMARX_WARNING_S << "Failed to write to Cfg file at " << appInstance->getConfigPath();
        return;
    }

    cfgFile << resultStr;

    cfgFile.close();
}

std::string IceParser::getCacheDir()
{
    std::string cachePath = ArmarXDataPath::GetCachePath();
    return (boost::filesystem::path(cachePath) / boost::filesystem::path("ComponentFiles")).string();
}

void IceParser::disableProperty(string& result, const string propertyName)
{
    //comment out default properties
    if (result.find_last_of(propertyName) != string::npos)
    {
        size_t it1 = result.rfind(propertyName + " = ");
        std::string commentedName = "# " + propertyName + " = ";
        size_t it2 = result.find(commentedName, it1);

        if (it1 != string::npos && it2 == string::npos)
        {
            result.insert(it1, "# ");
        }
    }
}

void IceParser::clearXmlCacheDir()
{
    boost::filesystem::remove_all(boost::filesystem::path(getCacheDir()));
}

