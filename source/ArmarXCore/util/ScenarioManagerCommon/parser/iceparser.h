/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Cedric Seehausen (usdnr at kit dot edu)
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once

#include "../data_structure/Application.h"
#include "../data_structure/ApplicationInstance.h"
#include <ArmarXCore/core/application/properties/IceProperties.h>
#include <ArmarXCore/core/application/properties/PropertyDefinitionContainer.h>
#include <vector>
#include <fstream>


namespace ScenarioManager
{
    namespace Parser
    {
        class IceParser
        {
        public:
            //used to load cfg
            armarx::PropertyDefinitionsPtr mergeXmlAndCfg(ScenarioManager::Data_Structure::ApplicationInstanceWPtr app, bool firstLoad = false);

            //used to default parameters of an application
            armarx::PropertyDefinitionsPtr loadFromXml(std::string path, ScenarioManager::Data_Structure::ApplicationInstanceWPtr app = ScenarioManager::Data_Structure::ApplicationInstancePtr(nullptr));

            void saveCfg(ScenarioManager::Data_Structure::ApplicationInstanceWPtr appInstance);

            static std::string getCacheDir();

            static void clearXmlCacheDir();
        private:
            void disableProperty(std::string& string, const std::string propertyName);
        };
    }
}

