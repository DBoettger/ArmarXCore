#pragma once

#include "../data_structure/ApplicationInstance.h"

namespace ScenarioManager
{
    class PidManager
    {
    public:
        PidManager();

        int loadPid(Data_Structure::ApplicationInstancePtr app);
        void savePid(Data_Structure::ApplicationInstancePtr app);

        static void clearCache();
    private:
    };
}

#include "../executor/Executor.h"

