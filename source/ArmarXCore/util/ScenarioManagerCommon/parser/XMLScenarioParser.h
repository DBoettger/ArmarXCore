/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Cedric Seehausen (usdnr at kit dot edu)
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once

#include "../data_structure/Scenario.h"
#include "../data_structure/Package.h"
#include <string>
#include <vector>
#include <ArmarXCore/core/system/cmake/CMakePackageFinderCache.h>

namespace ScenarioManager
{
    namespace Parser
    {
        /**
        * @class XMLScenarioParser
        * @ingroup parser
        * @brief This class provides different methods to parse and save scenario data in XML-Files.
        * To save Scenario data between sessions, scenarios should be saved in XML-Files using the following
        * format:
        *
        * <scenario name="name" lastChange="YYYY-MM-DD.hh:mm:ss" creation="YYYY-MM-DD.hh:mm:ss">
        *       ...
        *       <application instance="instanceName" name="applicationName" execPath="./pathToExec"></application>
        *       ...
        * </scenario>
        *
        * This class only parses XML-Files of the specified format.
        */
        class XMLScenarioParser
        {

        public:
            /**
            * Finds all .xml scenario files in a folder and returns a list of paths to them
            * @param folder path to the folder
            * @return list of paths of the scenario files
            */
            std::vector<std::string> getScenariosFromFolder(std::string folder);

            /**
            * Parses a .xml scenario file and creates a Scenario object out of it.
            * @param path to the scenario file
            * @return a new Scenario object containing the data parsed out of the file
            */
            ScenarioManager::Data_Structure::ScenarioPtr parseScenario(Data_Structure::PackagePtr package, std::string name, std::string subfolder = "");
            ScenarioManager::Data_Structure::ScenarioPtr parseScenario(ScenarioManager::Data_Structure::ScenarioPtr scenario);

            //            /**
            //             * @brief parseScenarioByFile parses an scenario file and returns the curresponding ScenarioPtr.
            //             *        (Used for CLI, it is possible to create an scenario without an package pointer. Warning Scenario without PackagePtr need to have an dependencie.config of the package in the CacheFolder)
            //             * @param path path to scx file
            //             * @param package (optional else nullptr)
            //             * @return
            //             */
            //            ScenarioManager::Data_Structure::ScenarioPtr parseScenarioByFile(std::string path, Data_Structure::PackagePtr package);


            /**
            * Finds the package that contains the given scenario.
            * @param scenario Scenario whose package is to be searched
            * @param packages list of packages which are searched for the scenario
            * @return the Package which contains the Scenario
            */
            ScenarioManager::Data_Structure::PackagePtr getScenarioPackage(ScenarioManager::Data_Structure::ScenarioPtr scenario, ScenarioManager::Data_Structure::PackageVectorPtr packages);

            /**
            * Creates a new scenario with the given name within the given package
            * @param name name of the new scenario
            * @param package package the new scenario is created in
            * @return new Scenario
            */
            ScenarioManager::Data_Structure::ScenarioPtr createNewScenario(std::string name, ScenarioManager::Data_Structure::PackagePtr package);

            /**
            * Saves a Scenario by recreating its .xml file based on its data
            * @param scenario Scenario which is to be saved
            */
            void saveScenario(ScenarioManager::Data_Structure::ScenarioWPtr scenario);

            std::string getPackageNameFromScx(std::string path);

            /**
             * @brief isScenarioExistend
             * @param name name of the scenario
             * @param package package the scenario is located in
             * @return returns true if the scenario exists in the file system else it returns false
             */
            bool isScenarioExistend(std::string name, ScenarioManager::Data_Structure::PackagePtr package, std::string subPath = "");
        private:


        };
    }
}
