/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Dennis Weigelt
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once

#include <boost/shared_ptr.hpp>
#include <boost/enable_shared_from_this.hpp>
#include <vector>
#include <string>

namespace ScenarioManager
{
    namespace Data_Structure
    {
        class Scenario;
        typedef boost::shared_ptr<Scenario> ScenarioPtr;
        typedef boost::weak_ptr<Scenario> ScenarioWPtr;
        typedef boost::shared_ptr<std::vector<ScenarioPtr>> ScenarioVectorPtr;
    }
}

#include "ApplicationInstance.h"
#include "Package.h"


namespace ScenarioManager
{
    namespace Data_Structure
    {
        class Package;
        typedef boost::shared_ptr<Package> PackagePtr;

        /**
        * @class Scenario
        * @ingroup data_structure
        * @brief Class containing data about a scenario and its applications.
        * Provides methods to get and set the data contained in the scenario. It is only representative
        * and doesn't actually manage the scenario.
        */
        class Scenario : public boost::enable_shared_from_this<Scenario>
        {
        private:
            std::string name;
            std::string creationTime;
            std::string lastChangedTime;
            std::string globalConfigName;
            std::string subfolder;
            boost::weak_ptr<Package> package;
            ApplicationInstanceVectorPtr applications;

            armarx::PropertyDefinitionsPtr globalConfig;

        public:
            /** Constructor that sets some base information about the scenario.
            * @param name name of the scenario
            * @param creationTime when this scenario was first created
            * @param lastChangedTime when this scenario was last changed. Should usually be the same as the time of
            * creation, except when creating an exact copy of another Scenario.
            * @param path path to the root of this scenario
            */
            Scenario(std::string name, std::string creationTime, std::string lastChangedTime, PackagePtr package, std::string globalConfigName = "./config/global.cfg", std::string subfolder = "");

            /*
            * @return name of this scenario
            */
            std::string getName();

            /*
            * Sets the name of this scenario.
            * @param name new name
            */
            void setName(std::string name);

            /**SCENARIO_H
            * @return time this scenario was created.
            */
            std::string getCreationTime();

            /**
            * @return time this scenario was last changed
            */
            std::string getLastChangedTime();

            std::string getGlobalConfigName();
            void setGlobalConfigName(std::string name);

            /**
            * @return path to the root of this scenario
            */
            std::string getPath();
            bool isScenarioFileWriteable();

            PackagePtr getPackage();

            /**
            * @return status of this scenario
            */
            std::string getStatus();

            /**
            * @returns subfolder
            **/
            std::string getSubfolder();

            /**
            * Sets the last-changed-time to now
            */
            void setLastChangedTime(std::string time);

            /**
            * @return all applications this scenario contains
            */
            ApplicationInstanceVectorPtr getApplications();

            ApplicationInstancePtr getApplicationByName(std::string name);

            /**
            * Adds an Application to this scenario.
            * @param application Application to be added to this scenario
            */
            void addApplication(ApplicationInstancePtr application);

            /**
            * Removes an Application from this scenario
            * @param application application to be removed
            */
            void removeApplication(ApplicationInstancePtr application);

            void updateApplicationByName(std::string name);

            void updateApplication(ApplicationInstancePtr application);

            void save();


            void reloadAppInstances();
            void reloadGlobalConf();

            armarx::PropertyDefinitionsPtr getGlobalConfig();

            std::string getGlobalConfigPath();
            bool isGlobalConfigWritable();
        };
    }
}

