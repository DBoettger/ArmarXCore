/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Dennis Weigelt
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include "Application.h"
#include <Ice/Properties.h>
#include <boost/filesystem.hpp>
#include "../../../core/logging/Logging.h"

using namespace ScenarioManager;
using namespace Data_Structure;
using namespace std;
using namespace armarx;

Application::Application(string name, string executablePath, string packageName)
    : name(name),
      executablePath(executablePath),
      packageName(packageName),
      found(false),
      properties(new PropertyDefinitionContainer(name))
{
    boost::filesystem::path file(executablePath + "/" + name + "Run");
    if (boost::filesystem::exists(file))
    {
        found = true;
    }

    Ice::PropertiesPtr cfgProperties = IceProperties::create();
    armarx::IceProperties* cfgInternal = static_cast<armarx::IceProperties*>(cfgProperties.get());
    cfgInternal->setInheritanceSolver(nullptr);
    properties->setProperties(cfgProperties);
}

Application::Application(Application& application)
    : name(application.name)
    , executablePath(application.executablePath)
    , packageName(application.packageName)
    , found(application.found)
    , properties(new PropertyDefinitionContainer(name))
{
    Ice::PropertiesPtr cfgProperties = IceProperties::create(application.getProperties()->getProperties()->clone());
    armarx::IceProperties* cfgInternal = static_cast<armarx::IceProperties*>(cfgProperties.get());
    cfgInternal->setInheritanceSolver(nullptr);
    properties->setProperties(cfgProperties);
}


string Application::getName()
{
    return this->name;
}

string Application::getPathToExecutable()
{
    return this->executablePath;
}

string Application::getPackageName()
{
    return packageName;
}

PropertyDefinitionsPtr Application::getProperties()
{
    return this->properties;
}

void Application::setProperties(PropertyDefinitionsPtr properties)   //oder Ice::StringSeq und create-Methode
{
    armarx::IceProperties* cfgInternal = static_cast<armarx::IceProperties*>(properties->getProperties().get());
    cfgInternal->setInheritanceSolver(nullptr);

    this->properties = properties;
    this->properties->setPrefix("");
    this->properties->setDescription(name + " properties");
}

bool Application::getFound()
{
    return found;
}

void Application::updateFound()
{
    boost::filesystem::path file(executablePath + "/" + name + "Run");

    if (boost::filesystem::exists(file))
    {
        found = true;
    }
    else
    {
        found = false;
    }
}

bool Application::isDefaultPropertyEnabled(std::string name)
{
    if (defaultMap.count(name) == 0)
    {
        return false;
    }
    if (enabledMap.count(name) == 0)
    {
        return false;
    }
    return enabledMap[name];
}

void Application::setDefaultPropertyEnabled(std::string name, bool enabled)
{
    if (defaultMap.count(name) == 0)
    {
        return;
    }
    enabledMap[name] = enabled;
}


bool Application::isDefaultProperty(std::string name)
{
    if (defaultMap.count(name) == 0)
    {
        return false;
    }
    return defaultMap[name];
}

void Application::setIsDefaultProperty(std::string name, bool defaultValue)
{
    defaultMap[name] = defaultValue;
}
