/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Dennis Weigelt
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once

#include <ArmarXCore/core/application/properties/IceProperties.h>
#include <ArmarXCore/core/application/properties/PropertyDefinitionContainer.h>
#include <boost/shared_ptr.hpp>
#include <string>


namespace ScenarioManager
{
    namespace Data_Structure
    {
        typedef std::map<std::string, bool> PropertyEnabledMap;

        /**
        * @class Application
        * @ingroup data_structure
        * @brief Class containing data about an application
        * Provides methods to get and set the date contained in the application. It doesn't manage the application
        * therefore changes in this class won't synchronize to the actual configuration files.
        */
        class Application
        {

        private:
            std::string name;
            std::string executablePath;
            std::string packageName;
            bool found;

        protected:
            armarx::PropertyDefinitionsPtr properties;
            PropertyEnabledMap enabledMap;
            PropertyEnabledMap defaultMap;

        public:
            /**
            * Constructor that sets the name and the path to the executable of the application.
            * @param name name of the application
            * @param executablePath path to the binary executable of the application
            */
            Application(std::string name, std::string executablePath, std::string packageName);

            /**
            * Copy constructor
            * @param name name of the application
            * @param executablePath path to the binary executable of the application
            */
            Application(Application& app);

            /**
            * @return the name of this application
            */
            std::string getName();

            /**
            * @return the path to the executable file of this application
            */
            std::string getPathToExecutable();

            std::string getPackageName();

            bool isDefaultPropertyEnabled(std::string name);
            void setDefaultPropertyEnabled(std::string name, bool enabled);

            bool isDefaultProperty(std::string name);
            void setIsDefaultProperty(std::string name, bool defaultValue);

            /**
            * @return the properties of this application
            **/
            armarx::PropertyDefinitionsPtr getProperties();

            /**
            * Sets the properties of this application. These properties (along with the
            * default config) define the behaviour of the application on start.
            * @param properties new properties
            */
            void setProperties(armarx::PropertyDefinitionsPtr properties);

            bool getFound();

            void updateFound();
        };
        typedef boost::shared_ptr<Application> ApplicationPtr;
        typedef boost::shared_ptr<std::vector<ApplicationPtr>> ApplicationVectorPtr;
    }
}

