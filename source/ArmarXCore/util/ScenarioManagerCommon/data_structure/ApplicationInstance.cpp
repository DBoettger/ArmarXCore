/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Dennis Weigelt
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include "ApplicationInstance.h"

#include "../parser/iceparser.h"
#include <ArmarXCore/core/application/properties/PropertyDefinitionConfigFormatter.h>
#include <ArmarXCore/core/application/properties/PropertyDefinitionFormatter.h>
#include <ArmarXCore/core/application/properties/PropertyDefinitionContainerFormatter.h>
#include <ArmarXCore/core/application/properties/PropertyDefinitionConfigFormatter.h>
#include <ArmarXCore/core/logging/Logging.h>
#include <boost/filesystem.hpp>


using namespace ScenarioManager;
using namespace Data_Structure;
using namespace std;
using namespace armarx;

const string ApplicationStatus::Stopped = "Stopped";
const string ApplicationStatus::Running = "Running";
const string ApplicationStatus::Waiting = "Waiting";
const string ApplicationStatus::Mixed = "Mixed";
const string ApplicationStatus::Unknown = "Unknown";

ApplicationInstance::ApplicationInstance(std::string name, std::string executablePath, std::string instanceName, std::string configPath, std::string packageName, ScenarioPtr scenario, bool enabled)
    : Application(name, executablePath, packageName), instanceName(instanceName)
    , configPath(configPath)
    , status(ApplicationStatus::Unknown), scenario(scenario), pid(-1), statusWriteBlock(false)
    , enabled(enabled)
{
}

ApplicationInstance::ApplicationInstance(Application application, string instanceName, string configPath, ScenarioPtr scenario, bool enabled)
    : Application(application),
      instanceName(instanceName),
      configPath(configPath),
      status(ApplicationStatus::Unknown),
      scenario(scenario),
      pid(-1),
      statusWriteBlock(false),
      enabled(enabled)
{
}

ScenarioPtr ApplicationInstance::getScenario()
{
    return scenario;
}

string ApplicationInstance::getInstanceName()
{
    return this->instanceName;
}

void ApplicationInstance::setInstanceName(std::string newName)
{
    this->instanceName = newName;
}

string ApplicationInstance::getConfigPath()
{
    return this->configPath;
}

void ApplicationInstance::setConfigPath(string configPath)
{
    this->configPath = configPath;
}

void ApplicationInstance::resetConfigPath()
{
    boost::filesystem::path scenariosFolder(scenario->getPackage()->getScenarioPath());
    boost::filesystem::path scenarioFolder = scenariosFolder / boost::filesystem::path("./" + scenario->getName());
    boost::filesystem::path scenarioCfgFolder = scenarioFolder / boost::filesystem::path("./config");

    boost::filesystem::path configPath = scenarioCfgFolder;

    if (!this->instanceName.empty() || !this->instanceName.compare("\"\"") || !this->instanceName.compare("\" \""))
    {
        configPath = configPath / boost::filesystem::path("./" + this->getName() + "." + this->getInstanceName() + ".cfg");
    }
    else
    {
        configPath = configPath / boost::filesystem::path("./" + this->getName() + ".cfg");;
    }

    boost::filesystem::remove(boost::filesystem::path(this->configPath));
    this->configPath = configPath.string();
}

string ApplicationInstance::getStatus()
{
    return this->status;
}

bool ApplicationInstance::setStatus(const std::string& status)
{
    bool result = status != this->status;
    if (result)
    {
        this->status = status;
    }
    return result;
}

int ApplicationInstance::getPid()
{
    return this->pid;
}

void ApplicationInstance::setPid(int pid)   // if PID < -1 Error?
{
    this->pid = pid;
}

void ApplicationInstance::modifyProperty(string name, string value)
{
    if (!properties->isPropertySet(name))
    {
        properties->defineOptionalProperty(name, std::string("::NOT_DEFINED::"), "Custom Property");
    }
    properties->getProperties()->setProperty(name, value);
}

void ApplicationInstance::addProperty(string name, string value)
{
    if (!properties->isPropertySet(name))
    {
        properties->defineOptionalProperty(name, std::string("::NOT_DEFINED::"), "Custom Property");
    }
    properties->getProperties()->setProperty(name, value);
}

void ApplicationInstance::save()
{
    Parser::IceParser parser;
    parser.saveCfg(shared_from_this());
}

void ApplicationInstance::load(bool firstLoad)
{
    if (!boost::filesystem::exists(configPath))
    {
        ARMARX_WARNING_S << "Cannot find ApplicationInstance Config at:" << configPath;
    }
    else
    {
        Parser::IceParser parser;
        setProperties(parser.mergeXmlAndCfg(shared_from_this(), firstLoad));
    }
}

bool ApplicationInstance::isConfigWritable()
{
    std::ofstream ofs;

    ofs.open(configPath.c_str(), std::ofstream::out | std::ofstream::app);
    ARMARX_DEBUG << configPath << " is writeable: " << ofs.is_open();

    return ofs.is_open();
}

bool ApplicationInstance::getStatusWriteBlock()
{
    return statusWriteBlock;
}

void ApplicationInstance::setStatusWriteBlock(bool blocked)
{
    statusWriteBlock = blocked;
}

bool ApplicationInstance::getEnabled()
{
    return enabled;
}

void ApplicationInstance::setEnabled(bool enabled)
{
    this->enabled = enabled;
}
