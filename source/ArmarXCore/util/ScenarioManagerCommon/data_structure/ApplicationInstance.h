/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Dennis Weigelt
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once

#include "Application.h"

namespace ScenarioManager
{
    namespace Data_Structure
    {
        class ApplicationInstance;
        typedef boost::shared_ptr<ApplicationInstance> ApplicationInstancePtr;
        typedef boost::weak_ptr<ApplicationInstance> ApplicationInstanceWPtr;
        typedef boost::shared_ptr<std::vector<ApplicationInstancePtr>> ApplicationInstanceVectorPtr;
    }
}

#include "Scenario.h"

#include <boost/shared_ptr.hpp>
#include <boost/enable_shared_from_this.hpp>
#include <string>

namespace ScenarioManager
{
    namespace Data_Structure
    {
        struct ApplicationStatus
        {
            static const std::string Stopped;
            static const std::string Running;
            static const std::string Waiting;
            static const std::string Mixed;
            static const std::string Unknown;
        };

        /**
        * @class ApplicationInstance
        * @ingroup Data_Structure
        * @brief Class containing data about the instance of an application.
        * Provides methods to get and set the data. It is only representative and doesn't actually synchronize
        * with the configuration files.
        */
        class ApplicationInstance : public Application, public boost::enable_shared_from_this<ApplicationInstance>
        {

        private:
            std::string instanceName;
            std::string configPath;
            std::string status;
            ScenarioPtr scenario;
            int pid;
            bool statusWriteBlock;
            bool enabled;

        public:
            /**
            * Constructor that sets the base data of this ApplicationInstance.
            * @param name The name of the Application
            * @param executablePath The path of the Executable
            * @param instanceName a name that distinguishes this instance from other instances
            * @param configPath path to the config file of this instance
            */
            ApplicationInstance(std::string name, std::string executablePath, std::string instanceName, std::string configPath, std::string packageName, ScenarioPtr scenarioName, bool enabled);

            /**
            * Constructor that sets the base data of this ApplicationInstance.
            * @param application The application this class instantiates.
            * @param instanceName a name that distinguishes this instance from other instances
            * @param configPath path to the config file of this instance
            */
            ApplicationInstance(Application application, std::string instanceName, std::string configPath, ScenarioPtr scenarioName, bool enabled);

            /**
            * @return name of this ApplicationInstance
            */
            std::string getInstanceName();

            /**
            * Sets the name of this ApplicationInstance
            * @param new name
            */
            void setInstanceName(std::string newName); //wird die setter gebraucht?

            /**
            * @return config path of this ApplicationInstance
            */
            std::string getConfigPath();
            std::string getGlobalConfigPath();
            bool isConfigWritable();

            ScenarioPtr getScenario();

            /**
            * Sets the config path of this ApplicationInstance
            * @param new config path
            */
            void setConfigPath(std::string configPath);

            /**
             * @brief resetConfigPath. should be called when the instanceName is changed/ deletes the old cfg file and resets the local config Path to where the file should be
             * you should call save after this
             */
            void resetConfigPath();

            /**
            * Returns the status. Doesn't update automatically, is not necessarily synchronized with
            * the actual status of the process. Use the Executor to get the actual status of this application.
            * @return status of this ApplicationInstance
            * @see Executor
            */
            std::string getStatus();

            /**
            * Sets the status of this ApplicationInstance
            * @param new status. Should be either "running" or "stopped" for Scenario statuses to work
            * properly.
            * @return tree if status changed.
            */
            bool setStatus(const std::string& status);

            /**
            * @return pid of this ApplicationInstance
            */
            int getPid();

            /**
            * Sets the pid of this ApplicationInstance. This pid gets used by the Executor to
            * stop this application, therefore it is not suggested to set this to random values.
            * Set this to -1, if no pid is known.
            * @param pid new pid
            */
            void setPid(int pid);

            /**
            * Changes the value of the specified property.
            * @param name name of the property whose value is to be changed
            * @param value new value of the property
            */
            void modifyProperty(std::string name, std::string value);

            /**
            * Adds a new property with the specified name and value to this ApplicationInstance.
            * @param name name of the new property
            * @param value value of the new property
            */
            void addProperty(std::string name, std::string value);


            /**
            * Saves the IceProperties to the configPath
            */
            void save();

            /**
            * Loades the IceProperties from the configPath
            * Firstload determins if properties that equal there default value get uncommented
            */
            void load(bool firstLoad = false);

            bool getStatusWriteBlock();
            void setStatusWriteBlock(bool blocked);


            bool getEnabled();
            void setEnabled(bool enabled);
        };
    }
}

