/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Nicola Miskowiec
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once

#include "../data_structure/ApplicationInstance.h"
#include "ApplicationStopper.h"

namespace ScenarioManager
{
    namespace Exec
    {
        /**
        * @class ByNameStopperLinux
        * @ingroup exec
        * @brief Stops or kills applications on Linux based on their name.
        * This ApplicationStopper stops (killall -15) or kills (killall -9) application processes on Linux.
        * It should not be used on other operating systems, due to the usage of system calls.
        */
        class ByNameStopperLinux : public ApplicationStopper
        {

        private:
            void killAllShellCall(std::string sig, std::string applicationName);

        public:
            /**
            * Stops an application based on its name. Uses killall -15.
            * @param application application to be stopped
            */
            void stop(Data_Structure::ApplicationInstancePtr application) override;

            /**
            * Stops an application based on its name. Uses killall -9.
            * @param application application to be killed
            */
            void kill(Data_Structure::ApplicationInstancePtr application) override;
        };

    }
}

