/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Nicola Miskowiec
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once

#include "../data_structure/ApplicationInstance.h"
#include "ApplicationStopper.h"

namespace ScenarioManager
{
    namespace Exec
    {
        /**
        * @class PidStopperLinux
        * @ingroup exec
        * @brief Stops or kills applications on Linux based on their pid.
        * This ApplicationStopper stops (kill -15) or kills (kill -9) application processes on Linux.
        * It should not be used on other operating systems, due to the usage of system calls.
        */
        class PidStopperLinux : public ApplicationStopper
        {

        public:
            /**
            * Stops an application based on its pid. Uses kill -15.
            * @param application application to be killed
            */
            void stop(Data_Structure::ApplicationInstancePtr application) override;

            /**
            * Kills an application based on its pid. Uses kill -9.
            * @param application application to be killed
            */
            void kill(Data_Structure::ApplicationInstancePtr application) override;
        };

    }
}

