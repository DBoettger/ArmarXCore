/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Nicola Miskowiec
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include "Executor.h"
#include <ArmarXCore/core/exceptions/Exception.h>
#include <ArmarXCore/core/logging/Logging.h>
#include <boost/foreach.hpp>
#include <sys/types.h>
#include <signal.h>
#include <chrono>
#include <thread>

using namespace ScenarioManager;
using namespace Exec;
using namespace Data_Structure;
using namespace std;

Executor::Executor(StopStrategyPtr strategy, ApplicationStarterPtr starter)
{
    setStopStrategy(strategy);
    setStarter(starter);
}

std::future<void> Executor::startApplication(ApplicationInstancePtr application, bool printOnly, const std::string& commandLineParameters)
{
    if (application->getPid() != -1 || application->getStatusWriteBlock())
    {
        return std::future<void>();
    }


    application->setStatusWriteBlock(true);
    std::packaged_task<void()> task(std::bind(&ApplicationStarter::start, starter.get(), application, pidManager, commandLineParameters, printOnly)); // wrap the function

    std::future<void> result = task.get_future();  // get a future
    std::thread t(std::move(task));
    if (printOnly)
    {
        t.join(); //if only the commands should be printed then we want sync behaviour
    }
    else
    {
        t.detach();
    }
    return result;
}

std::future<void> Executor::stopApplication(ApplicationInstancePtr application)
{
    if (application->getStatusWriteBlock())
    {
        return std::future<void>();
    }

    application->setStatusWriteBlock(true);
    std::packaged_task<void()> task(std::bind(&StopStrategy::stop, stopStrategy.get(), application)); // wrap the function
    std::future<void> result = task.get_future();  // get a future
    std::thread(std::move(task)).detach(); // launch on a thread

    return result;
}

void Executor::asyncApplicationRestart(ApplicationInstancePtr application, bool printOnly)
{
    stopStrategy->stop(application);

    int waitCount = 0;
    while (starter->getStatus(application, pidManager) != "Stopped")
    {
        std::this_thread::sleep_for(std::chrono::milliseconds(100));
        waitCount ++;
        //Try to stop for 20 secs
        if (waitCount == 200)
        {
            ARMARX_INFO_S << "The application " << application->getName() << " is not Stopping please force kill it or try again. Aborting restart";
            return;
        }
    }
    starter->start(application, pidManager, "", printOnly);
}

std::future<void> Executor::restartApplication(ApplicationInstancePtr application, bool printOnly)
{
    if (application->getStatusWriteBlock())
    {
        return std::future<void>();
    }

    application->setStatusWriteBlock(true);

    std::packaged_task<void()> task(std::bind(&Executor::asyncApplicationRestart, this, application, printOnly)); // wrap the function

    std::future<void> result = task.get_future();  // get a future
    std::thread(std::move(task)).detach(); // launch on a thread

    return result;
}

std::string Executor::getApplicationStatus(ApplicationInstancePtr application)
{
    if (application->getStatusWriteBlock())
    {
        return "Waiting";
    }
    return starter->getStatus(application, pidManager);
}

void Executor::asyncScenarioStart(ScenarioPtr scenario, bool printOnly, const std::string& commandLineParameters)
{
    std::vector<std::future<void>> futures;

    std::vector<ApplicationInstancePtr> apps = *scenario->getApplications();
    for (auto it = apps.begin(); it != apps.end(); it++)
    {
        futures.push_back(startApplication(*it, printOnly, commandLineParameters));
    }

    for (auto future = futures.begin(); future != futures.end(); ++future)
    {
        future->wait();
    }
}

std::future<void> Executor::startScenario(boost::shared_ptr<Data_Structure::Scenario> scenario, bool printOnly, const std::string& commandLineParameters)
{
    std::packaged_task<void()> task(std::bind(&Executor::asyncScenarioStart, this, scenario, printOnly, commandLineParameters)); // wrap the function

    std::future<void> result = task.get_future();  // get a future
    std::thread(std::move(task)).detach(); // launch on a thread

    return result;
}

void Executor::asyncScenarioStop(ScenarioPtr scenario)
{
    std::vector<std::future<void>> futures;

    std::vector<ApplicationInstancePtr> apps = *scenario->getApplications();
    for (auto it = apps.begin(); it != apps.end(); it++)
    {
        futures.push_back(stopApplication(*it));
    }

    for (auto future = futures.begin(); future != futures.end(); ++future)
    {
        future->wait();
    }
}

std::future<void> Executor::stopScenario(ScenarioPtr scenario)
{
    std::packaged_task<void()> task(std::bind(&Executor::asyncScenarioStop, this, scenario)); // wrap the function
    std::future<void> result = task.get_future();  // get a future
    std::thread(std::move(task)).detach(); // launch on a thread

    return result;
}

void Executor::asyncScenarioRestart(ScenarioPtr scenario, bool printOnly)
{
    std::vector<std::future<void>> futures;
    for (auto app : *scenario->getApplications())
    {
        futures.push_back(restartApplication(app, printOnly));
    }

    for (auto future = futures.begin(); future != futures.end(); ++future)
    {
        future->wait();
    }
}

std::future<void> Executor::restartScenario(ScenarioPtr scenario, bool printOnly)
{
    std::packaged_task<void()> task(std::bind(&Executor::asyncScenarioRestart, this, scenario, printOnly)); // wrap the function
    std::future<void> result = task.get_future();  // get a future
    std::thread(std::move(task)).detach(); // launch on a thread

    return result;
}

void Executor::loadAndSetCachedProperties(Data_Structure::ApplicationPtr application, std::string path, bool reload, bool set)
{
    starter->loadAndSetCachedProperties(application, path, reload, set);
}


void Executor::setStopStrategy(StopStrategyPtr strategy)
{
    this->stopStrategy = strategy;
}

void Executor::setStarter(ApplicationStarterPtr starter)
{
    this->starter = starter;
}
