#pragma once

#include "../data_structure/ApplicationInstance.h"

typedef boost::shared_ptr<ScenarioManager::Data_Structure::ApplicationInstance> ApplicationInstancePtr;
namespace ScenarioManager
{
    namespace Exec
    {
        /**
        * @class StopStrategy
        * @ingroup exec
        * @brief Interface for classes that define how an application get stopped.
        */
        class StopStrategy
        {

        public:
            /**
            * Stops an application. Implement this method to define the behaviour when stopping applications.
            * @param application application to be stopped.
            */
            virtual void stop(ApplicationInstancePtr application) = 0;
        };

        typedef boost::shared_ptr<StopStrategy> StopStrategyPtr;
    }
}

