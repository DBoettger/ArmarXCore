/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Nicola Miskowiec
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include "StopAndKill.h"
#include "Executor.h"
#include <chrono>
#include <thread>

using namespace ScenarioManager;
using namespace Data_Structure;
using namespace Exec;

StopAndKill::StopAndKill(ApplicationStopperPtr stopper, int delay)
{
    this->stopper = stopper;
    this->timer = delay;
}

void StopAndKill::stop(ApplicationInstancePtr application)
{
    int previousPid = application->getPid();
    stopper->stop(application);
    std::this_thread::sleep_for(std::chrono::milliseconds(timer));
    //Check if the application got restartet while waiting for the Kill timer
    if (application->getPid() == previousPid)
    {
        stopper->kill(application);
    }

    application->setStatusWriteBlock(false);
}

ApplicationStopperPtr StopAndKill::getStopper()
{
    return this->stopper;
}
