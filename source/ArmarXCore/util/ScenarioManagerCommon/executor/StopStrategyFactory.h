/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Nicola Miskowiec
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once

#include "ApplicationStopper.h"
#include "StopStrategy.h"

namespace ScenarioManager
{
    namespace Exec
    {
        /**
        * @class StopStrategyFactory
        * @ingroup exec
        * @brief Creates different instances of StopStrategy.
        * This Factory creates different types of stop-strategies.
        */
        class StopStrategyFactory
        {

        public:
            /**
            * Creates a StopStrategy that simply tries to stop the application.
            * @param appStopper stopper used by the strategy to stop the application
            * @return the created strategy
            */
            StopStrategyPtr getStopStrategy(ApplicationStopperPtr appStopper);

            /**
            * Creates a StopStrategy that first tries to stop the application, the waits a certain amount of time,
            * then kills the application.
            * @param appStopper stopper used by the strategy to stop the application
            * @param delay delay between stop and kill
            * @return the created strategy
            */
            StopStrategyPtr getStopAndKillStrategy(ApplicationStopperPtr appStopper, int delay);
        };

    }
}

