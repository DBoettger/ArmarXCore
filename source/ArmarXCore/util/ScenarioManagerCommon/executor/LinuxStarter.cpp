/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Nicola Miskowiec
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#include "LinuxStarter.h"
#include "../parser/iceparser.h"
#include "../parser/DependenciesGenerator.h"
#include <ArmarXCore/core/exceptions/Exception.h>
#include <ArmarXCore/core/logging/Logging.h>
#include <ArmarXCore/core/system/ArmarXDataPath.h>
#include <boost/algorithm/string.hpp>
#include <boost/filesystem.hpp>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <signal.h>
#include <syslog.h>
#include <fcntl.h>
#include <string>
#include <iostream>
#include <sstream>
#include <iostream>
#include <sys/time.h>
#include <boost/process.hpp>

#include <boost/range/algorithm/transform.hpp>

using namespace ScenarioManager;
using namespace Exec;
using namespace Parser;
using namespace Data_Structure;
using namespace armarx;

std::string get_process_name_by_pid(const int pid)
{
    char* name = (char*)calloc(1024, sizeof(char));
    if (name)
    {
        sprintf(name, "/proc/%d/cmdline", pid);
        FILE* f = fopen(name, "r");
        if (f)
        {
            size_t size;
            size = fread(name, sizeof(char), 1024, f);
            if (size > 0)
            {
                if ('\n' == name[size - 1])
                {
                    name[size - 1] = '\0';
                }
            }
            fclose(f);
        }
        std::string result(name);
        free(name);
        return result;
    }
    return std::string();
}

void LinuxStarter::start(ApplicationInstancePtr app, PidManager pidManager, const std::string& commandLineParameters, bool printOnly)
{
    if (!app->getEnabled())
    {
        app->setStatusWriteBlock(false);
        return;
    }

    std::string runName = app->getName();
    if (app->getName().find("Run") == std::string::npos)
    {
        runName.append("Run");
    }

    boost::filesystem::path componentFolderPath = boost::filesystem::path(ArmarXDataPath::GetCachePath()) / boost::filesystem::path("ComponentFiles/");

    if (!boost::filesystem::exists(componentFolderPath))
    {
        if (!boost::filesystem::create_directories(componentFolderPath))
        {
            std::cout << "Could not create Cache folder for ScenarioManagerPlugin at " << componentFolderPath.string() << std::endl;
        }
    }


    boost::filesystem::path dependenciesFilePath = componentFolderPath / boost::filesystem::path("./" + app->getScenario()->getPackage()->getName() + ".dependencies.cfg");

    if (!boost::filesystem::exists(dependenciesFilePath))
    {
        Parser::DependenciesGenerator builder;
        builder.generateDependenciesCfg(Data_Structure::PackagePtr(app->getScenario()->getPackage()));
    }

    if (!boost::filesystem::exists(boost::filesystem::path(app->getScenario()->getGlobalConfigPath()))
        || !boost::filesystem::exists(boost::filesystem::path(app->getConfigPath()))
        || !boost::filesystem::exists(dependenciesFilePath))
    {
        std::cout << "Launching " << app->getName() << " in " << app->getScenario()->getPackage()->getName() << " without an needed cfg file";
    }
    if (!boost::filesystem::exists(boost::filesystem::path(app->getPathToExecutable().append("/").append(runName))))
    {
        std::cout << "\033[1;31m" << "Warning: Could not launch " << app->getName() << " in " << app->getScenario()->getPackage()->getName() << " because the executable is missing at " << app->getPathToExecutable() << "\033[0m" << std::endl;
    }
    std::string args;
    args += app->getPathToExecutable().append("/").append(runName);
    args += std::string(" --Ice.Config=").append(app->getScenario()->getGlobalConfigPath())
            .append(",").append(app->getConfigPath());
    args += std::string(" --ArmarX.DependenciesConfig=").append(dependenciesFilePath.string());

    if (!commandLineParameters.empty())
    {
        Ice::StringSeq additional = Split(commandLineParameters, " ");

        bool inString = false;
        std::string inStringChar;
        std::string toPush = "";
        for (auto command : additional)
        {
            if (!inString && command.find("\"") != std::string::npos)
            {
                inString = true;
                inStringChar = "\"";
            }
            else if (!inString && command.find("\'") != std::string::npos)
            {
                inString = true;
                inStringChar = "\'";
            }
            else if (inString && command.find(inStringChar) != std::string::npos)
            {
                inString = false;
                toPush += command;
                args += " " + toPush;
                toPush = "";
                continue;
            }

            if (inString)
            {
                toPush += command + " ";
            }
            else
            {
                if (command != "")
                {
                    args += " " + command;
                }
            }
        }
    }
    else
    {
        Ice::StringSeq additional = Split(this->commandLineParameters, " ");
        for (auto arg : additional)
        {
            args += arg + " ";
        }
    }

    using namespace boost::process;
    using namespace boost::process::initializers;
    std::string appPath = app->getPathToExecutable().append("/").append(runName);
#if defined(BOOST_POSIX_API)
    // prevent child to become a zombie process because the parent has reacted on a signal yet
    signal(SIGCHLD, SIG_IGN);
#endif

    if (printOnly)
    {
        for (auto arg : args)
        {
            std::cout << arg;
        }
        std::cout << " &" << std::endl << std::endl;
        return;
    }

    //this apperantly prodcuces escaping errors
    //child c = execute(set_args(args), inherit_env());

    //this is the solution
    child c = execute(run_exe(appPath), set_cmd_line(args), inherit_env());

    int pid = c.pid;

    app->setPid(pid);
    pidManager.savePid(app);
    app->setStatusWriteBlock(false);
}

std::string LinuxStarter::getStatus(ApplicationInstancePtr application, PidManager pidManager)
{
    //if the app has no pid ask the pid manager if there is an cached Pid for this app
    if (application->getPid() < 0)
    {
        application->setPid(pidManager.loadPid(application));

        //if there is none just return stopped
        if (application->getPid() < 0)
        {
            return ApplicationStatus::Stopped;
        }
        else
        {
            std::string systemAppName = getSystemAppName(application);
            //std::string systemAppName = get(application->getPid());


            //if there is no more file there the app got stopped while checking (should only occure very rarely)
            if (systemAppName.empty())
            {
                application->setPid(-1);
                pidManager.savePid(application);
                return ApplicationStatus::Stopped;
            }
            else
            {
                std::string runName = application->getName().append("Run");
                if (systemAppName.compare(runName) == 0)
                {
                    return getStatus(application, pidManager);
                }
                else
                {
                    //else delete the saved pid and inform the user
                    ARMARX_INFO_S << "The Process name with Pid (" << application->getPid() << ":" << systemAppName << ") does not correspond with app name (" << application->getName() << "). Resetting Saved Pid.";
                    application->setPid(-1);
                    pidManager.savePid(application);
                    return ApplicationStatus::Stopped;
                }
            }
        }
    }
    else
    {
        std::string status = getSystemAppStatus(application);
        if (status == ApplicationStatus::Stopped)
        {
            application->setPid(-1);
            pidManager.savePid(application);
            return ApplicationStatus::Stopped;
        }
        else
        {
            return status;
        }
    }
}

std::string LinuxStarter::getSystemAppName(Data_Structure::ApplicationInstancePtr application)
{
    std::string namepluspath = get_process_name_by_pid(application->getPid());

    if (namepluspath.empty())
    {
        return "";
    }
    else
    {
        std::string result = namepluspath.substr(namepluspath.rfind("/") + 1);

        return result;
    }
}

std::string LinuxStarter::getSystemAppStatus(Data_Structure::ApplicationInstancePtr application)
{
    std::string processFilePath = "/proc/";
    processFilePath << application->getPid() << "/status";
    std::ifstream t(processFilePath);

    if (!t.is_open())
    {
        return ApplicationStatus::Stopped;
    }

    std::string stateLine;
    std::string line;
    while (std::getline(t, line))
    {
        if (Contains(line, "State:"))
        {
            stateLine = line;
            break;
        }
    }
    if (stateLine.empty())
    {
        return ApplicationStatus::Stopped;
    }
    std::string result = stateLine.substr(stateLine.find(':') + 1);

    boost::trim(result);

    result = result.at(0);

    if (result == "R" || result == "S")
    {
        return ApplicationStatus::Running;
    }
    else if (result == "D" || result == "Z")
    {
        return ApplicationStatus::Unknown;
    }
    else if (result == "T" || result == "X")
    {
        return ApplicationStatus::Stopped;
    }
    else
    {
        ARMARX_DEBUG_S << "Uncatched app return state";
        return ApplicationStatus::Unknown;
    }
}

void LinuxStarter::loadAndSetCachedProperties(ApplicationPtr application, std::string path, bool reload, bool set)
{
    boost::filesystem::path xmlFilePath = boost::filesystem::path(path) / boost::filesystem::path(application->getPackageName() + "." + application->getName() + ".xml");

    if (reload || !boost::filesystem::exists(xmlFilePath))
    {
        boost::filesystem::create_directories(path);

        std::string strCommand = application->getPathToExecutable().append("/").append(application->getName()).append("Run -p -f xml -o ").append(xmlFilePath.string());
        int ret = system(strCommand.c_str());
        if (ret != 0)
        {
            ARMARX_INFO << "Failed to generate properties xml for " << application->getName() << "\nCommand was: " << strCommand;
        }
    }
    else
    {
        application->updateFound();
        if (!application->getFound())
        {
            return;
        }
        //if executable more recent that xml reload xml
        boost::posix_time::ptime xmlDate = boost::posix_time::from_time_t(boost::filesystem::last_write_time(xmlFilePath));
        boost::posix_time::ptime execDate = boost::posix_time::from_time_t(boost::filesystem::last_write_time(application->getPathToExecutable().append("/").append(application->getName()).append("Run")));
        if (execDate > xmlDate)
        {
            loadAndSetCachedProperties(application, path, true, set);
            return;
        }
    }

    if (set)
    {
        IceParser parser;
        application->setProperties(parser.loadFromXml(xmlFilePath.string()));
    }
}
