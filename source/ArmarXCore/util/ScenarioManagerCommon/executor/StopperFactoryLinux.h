/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarXCore::core
 * @author     Nicola Miskowiec
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */


#pragma once

#include "StopperFactory.h"
#include "PidStopperLinux.h"

namespace ScenarioManager
{
    namespace Exec
    {
        /**
        * @class StopperFactoryLinux
        * @ingroup exec
        * @brief Creates different ApplicationStopper for Linux.
        * Shouldn't be used on other operating systems, since the stoppers use system calls to stop applications.
        */
        class StopperFactoryLinux : public StopperFactory
        {

        public:
            /**
            * Creates a PidStopperLinux and returns it. This stopper stops applications based on their pid.
            * @return a PidStopperLinux
            */
            ApplicationStopperPtr getPidStopper() override;

            /**
            * Creates a ByNameStopperLinux and returns it. This stopper stops applications based on their name.
            * @return a ByNameStopperLinux
            */
            ApplicationStopperPtr getByNameStopper() override;
        };
    }
}

