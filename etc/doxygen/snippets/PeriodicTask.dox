#include <ArmarXCore/core/Component.h>
#include <ArmarXCore/core/services/tasks/PeriodicTask.h>

namespace armarx
{
    class PeriodicTaskExample :
        virtual public Component
    {
    public:
        virtual std::string getDefaultName()
        {
            return "PeriodicTaskExample";
        }
            
        virtual void onInitComponent()
        {
        }

        virtual void onConnectComponent()
        {
            // create a running task which uses exampleThreadMethod in this class as thread method
            // namespace needs to be specified with the method name
            // will be called every second
            exampleTask = new PeriodicTask<PeriodicTaskExample>(this, &PeriodicTaskExample::exampleThreadMethod, 1000);

            // start the thread
            exampleTask->start();
        }
            
        virtual void onDisconnectComponent()
        {
            // stop the thread and wait for join
            exampleTask->stop();
        }
            
        void exampleThreadMethod()
        {
            // do the work
            printf("Thread method\n");
        }

    private:
        // shared pointer to PeriodicTask
        PeriodicTask<PeriodicTaskExample>::pointer_type exampleTask;
    };

}
