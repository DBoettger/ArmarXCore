/**
\page core-utility-doc Utility Features

ArmarX provides a set of utility features to aid you in developing new
applications:

\li \subpage LoggingDoc "Logging"
<br/>Since one of ArmarX's main principles is distributed processing, ArmarX provides
means for distributed logging.
\li \subpage ExceptionsDoc "Exception Handling"
<br/>ArmarX defines already several exceptions and generates for all exceptions backtraces.
\li \subpage armarx-profiling-doc "Profiling"
<br/>In the premise of state disclosure ArmarX provides profiling capabilities like system usage or statechart usage.
\li \subpage SharedMemoryDoc "Shared Memory"
<br/>Usually, ArmarX uses Ice to connect two processes to each other. But in the case of big data packages (i.e. images) shared memory is the faster way to transfer data between processes.
\li \subpage ThreadsDoc "Threading"
<br/>ArmarX is inherently multithreaded and offers therefore easy means to run code in additional threads.
\li \subpage VirtualTimeDoc "VirtualTime"
<br/>ArmarX allows synchronization via a dedicated VirtualTime concept.
\li \subpage PropertiesDoc "Properties"
<br/>ArmarX offers a convenient way to parameterize your component via config files with the Properties concept.
\li \subpage OnScopeExitGuardsDoc "Execution of code when a scope is left"
<br/>ArmarX offers a macro to execute code on scope exit.
\li \subpage ComponentFactoriesDoc "Component factories"
<br/>ArmarX offers a way to register components with a name. This name can be used to instantiate the component registered under it.
\li \subpage ArmarXCore-TopicRecording
<br/>It is easily possible to record and replay any topic with ArmarX.

A list of all utility classes can be found here: \ref core-utility

*/
