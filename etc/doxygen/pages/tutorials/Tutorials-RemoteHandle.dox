/**
\page ArmarXCore-Tutorials-RemoteHandles RemoteHandle tutorials

\section ArmarXCore-Tutorials-RemoteHandles-struct What does what?
There are three classes in play:
\li \ref armarx::RemoteHandleControlBlock : Holds the object alive and keeps the reference count. (server side)
\li \ref armarx::ClientSideRemoteHandleControlBlock : Increases/Decreases the reference count. (send to the client)
\li \ref armarx::RemoteHandle <ProxyType>: Wraps a ClientSideRemoteHandleControlBlock for easy use by the user.

\section ArmarXCore-Tutorials-RemoteHandles-gen General problems and limitations 
\warning
The QtCreator does not display possible function calls for \ref armarx::RemoteHandle<ProxyType> correctly!
You can still call the functions for the ProxyType <br />
It ignores following rule:<br />
"If a user-defined operator-> is provided, the operator-> is called again on the value that it returns, recursively, until an operator-> is reached that returns a plain pointer. After that, built-in semantics are applied to that pointer."<br />
(http://en.cppreference.com/w/cpp/language/operator_member_access#Built-in_member_access_operators)

\subsection ArmarXCore-Tutorials-RemoteHandles-gen-leak Leaking
A object may leak if a connection error stops the message send when a handle is deleted (this stops the usecount from reaching zero).
The deleter will be called when the \ref armarx::ArmarXManager shuts down.
A server can force delete the object by calling \ref armarx::RemoteHandleControlBlock::forceDeletion on the direct handle returned by \ref RemoteHandleControlBlock::create
\subsection ArmarXCore-Tutorials-RemoteHandles-gen-del Premature deletion
A object may be deleted with handles still alive if:
    -# the object's deletion was forced by calling \ref armarx::RemoteHandleControlBlock::forceDeletion
    -# a scenario equivalent to this:
        -# pc A has the object and sends a handle to B
        -# pc B has now the only handle
        -# pc B now sends the handle to pc C and delets the handle before C deserialized the object (this could be done via a broadcast)
        -# The handle is deserialized after the deletion timeout set via property (default 3 sec) has passed.
    
This scenario 2) is possible but should not happen on a stable LAN if ice objects are not keept in a serialized form for a longer period


\section ArmarXCore-Tutorials-RemoteHandles-client The client side 
You will probably know the type of proxy to expect.
E.g.: A \ref memoryx::CommonStorageInterfacePrx.
\code
{
    armarx::RemoteHandle<memoryx::CommonStorageInterfacePrx> rh = getCSHandleFromServer();
    //now use rh just as you would use a proxy
}
//rh is out of scope now.
//the server decrements the reference count.
\endcode
If you don't know the proxy type to expect:
\code
    armarx::RemoteHandle<Ice::ObjectPrx> rh = getMysteryHandleFromServer();
    if(rh->ice_isA(memoryx::WorkingMemoryInterface::ice_staticId()))
    {
        std::cout << "the mystery is a working memory";
        auto wmrh = armarx::RemoteHandle<memoryx::WorkingMemoryInterfacePrx>::uncheckedCast(rh);
        //use wmrh
    }

    auto csrh = armarx::RemoteHandle<memoryx::CommonStorageInterfacePry>::checkedCast(rh);
    if(csrh)
    {
        std::cout << "the mystery is a common storage";
        //use csrh
    }
\endcode

\section ArmarXCore-Tutorials-RemoteHandles-server The server side 
Create a \ref armarx::RemoteHandleControlBlock like this:
\code
{
    //get the stuff we want to register
    auto objectPtr = createObject();
    getArmarXManager()->addObject(objectPtr, true, "somename");
    //the self proxy could still be null => wait for it
    auto objectProxy = objectPtr->getProxy(-1);

    //register
    auto axManager = getArmarXManager();
    ManagementData mdat = RemoteHandleControlBlock::create(
                         getArmarXManager()->getAdapter(),
                         objectProxy,
                         [axManager, objectPtr]
                        {
                            axManager->removeObjectNonBlocking(objectPtr);
                        }
     );
    
    //send to client
    sendToClient(mdat.clientSideRemoteHandleControlBlock);
    //maybe store mdat.directHandle
}
//mdat.directHandle is now out of scope
//it is ok if it was deleted
\endcode

Maybe you want to clean up (if you don't do this and your network is unstable you may leak the \ref armarx::RemoteHandle until the \ref armarx::ArmarXManager shuts down):
\code
//you are shutting down your server and want to make sure the created \ref armarx::RemoteHandle is deleted now.
storedDirectHandle->forceDeletion();
\endcode
*/
