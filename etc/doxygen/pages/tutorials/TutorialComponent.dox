/**

\page ArmarXCore-Tutorial-Component Creating new components
\tableofcontents
Prerequisites: None

This tutorial will cover two aspects.
First, you will learn how to create and implement new components using the armarx-package tool.
Second, you will learn how to integrate an external C++ library into a component and how to offer its functionality through Ice.

For the sake of simplicity, this tutorial will teach you how to implement a very mundane random number generator by providing access to the external Boost.random library through an Ice interface.

\section secCT_A Create a new package, component and application
Use the \ref armarx-package "armarx-package tool" to create a new package called ComponentsExample by running the following commands:
\code{sh}
cd ~/armarx/
armarx-package init ComponentsExample
cd ComponentsExample
armarx-package add component RNGProvider
armarx-package add application RNGProvider
\endcode
The package will contain everything related to what we are implementing in this tutorial.
The RNGProvider component will provide access to the Boost.Random library through Ice.
The RNGProvider application will then get a randomly generated number by accessing the component.

\note Make sure to give the component and the application the same name. Otherwise the generated code will not compile without modifications.

\section secCT_B Define an Ice interface for the RNGProvider component
Navigate to the `ComponentsExample/source/ComponentsExample/interface` folder.
Next, create an empty file called RNGComponentProviderIceInterface.ice.
In this interface file, we define the methods that a component provides access to over Ice.
In our example, this looks like this:
\code{.cpp}
#ifndef ARMARX_RNGPROVIDERCOMPONENT_SLICE
#define ARMARX_RNGPROVIDERCOMPONENT_SLICE

module armarx
{
    /**
    *   
    */
    interface RNGProviderComponentInterface
    {
        int generateRandomInt();
    };

};

#endif
\endcode

Navigate to the build directory of the package and run cmake.

\section secCT_C Edit CMakeLists.txt files
Next, we need to tell CMake about our interface so it can auto-generate the code stubs for it.
This is done by adding it to the `SLICE_FILES` CMake variable.
To do so, open the CMakeLists.txt located within the interface folder in QtCreator.
\note Don't forget to configure your project when first opening it in QtCreator: set the build directory of your package folder as default. 

Now add the interface file to the `set(SLICE_FILES)` command like this:
\code{.cmake}
set(SLICE_FILES
RNGComponentProviderIceInterface.ice
)
\endcode

We also need to inform our component about the interface library which will be generated.
This can be done by editing the CMakelists.txt located in the `ComponentsExample/source/ComponentsExample/components/RNGProvider` folder like this:
\code{.cmake}
set(COMPONENT_LIBS ArmarXCoreInterfaces ArmarXCore ComponentsExampleInterfaces)
\endcode

\section secCT_D Preparation of component and application
Our component needs to inherit from the Ice interface we defined.
This inheritance has to be declared in the RNGProvider.h which can be found at `ComponentExample/source/ComponentsExample/components/RNGProvider`.
Edit the following line of code
\code{.cpp}
class RNGProvider :
    virtual public armarx::Component,
    virtual public armarx::RNGProviderComponentInterface
\endcode
For this to work, we also need to include the RNGComponentProviderIceInterface.h.
\note
Here's a neat trick to do this with QTCreator.
Click once on RNGProviderComponentInterface which you just added to the header file.
Then, press ALT+Enter.
A menu should pop up and one option reads:
\code{.sh}
Add #include <ComponentsExample/interface/RNGComponentProviderIceInterface.h>
\endcode
Press Enter and your include should appear in the include section of the header.
\endnote

Alternatively you can write the following line of code to the include section of the header file
\code{.cpp}
#include <ComponentsExample/interface/RNGComponentProviderIceInterface.h>
\endcode



Add the declaration of the generateRandomInt() method to the public section of the class:
\code
virtual Ice::Int generateRandomInt(const Ice::Current &);
\endcode 
\note
Again, the refactoring trick can be used to add the declaration of the method definied in the the interface to the header.
Highlight RNGProviderComponentInterface by left-clicking on it and press Alt+Enter.
Another menu should pop up which offers you to
\code{.sh}
Insert Virtual Functions of Base Classes
\endcode
In the following dialog, you can check which methods you want to have inserted.
Here, only the generateRandomInt method should be checked.
Press ok.
If you now take a look at the RNGProvider.h file, you will find the method declaration for the generateRandomInt() method.
\endnote

Now, add the method definiton of the generateRandomInt() method to the RNGProvider.cpp file.
Just add it underneath all the other method definitions:
\code
Ice::Int RNGProvider::generateRandomInt(const Ice::Current &c)
{
}
\endcode
\note
If you do the very same refactoring trick again on the newly created method declaration, by choosing
\code{.sh}
Add Definition in RNGProvider.cpp
\endcode
a method stub is added to the source file, waiting for you to be implemented!
\endnote

Before we actually implement the method, we also need to ensure that our application (which accesses our component) links against the component.
This is achieved by editing the CMakeLists.txt file located in the `ComponentsExample/source/ComponentsExample/applications/RNGProvider` folder such as:
\code{.cmake}
set(COMPONENT_LIBS ArmarXCoreInterfaces ArmarXCore RNGProvider)
\endcode
\note Since the component and application have the same name this step is not necessary. The RNGProvider component was added automatically. If you have multiple components in a single application then you will need to add all of them to the COMPONENT_LIBS variable.

At this point, the package should compile without any errors.
To make sure it does so, first run CMake and then build the package.

\section secCT_E Adding an external library
We want our RNGProvider component to offer access to the `Boost.Random` library through Ice.
Thus, we need to include the library in our component and link against it.
First, we again open the RNGProvider.h located at `ComponentsExample/source/ComponentsExample/components/RNGProvider`.
Add the following line to the include section:
\code{.cpp}
#include <boost/random.hpp>
\endcode
Next, open the the corresponding CMakeLists.txt located in the same folder and add the following lines:
\verbatim
find_package(Boost COMPONENTS random QUIET)
armarx_build_if(Boost_FOUND "Boost.random not available")

if(Boost_FOUND)
    include_directories(${Boost_INCLUDE_DIRS})
endif()
\endverbatim

Now we can finally implement our random number generator! Woo!

\section secCT_F Implementation of the random number generator
First, we add a private member variable to the RNGProvider.h such as
\code{.cpp}
    private:
        boost::random::mt19937 gen;
\endcode
While we're in the header file, we update the declaration of the generateRandomInt() method to
\code{.cpp}
Ice::Int generateRandomInt(const Ice::Current &c = ::Ice::Current());
\endcode

Take a look at the screenshot below to make sure the code for the RNGProvider class looks the same.
\image html components-tutorial-RNGProviderClass.png


Then, we open the RNGProvider.cpp and implement the onInitComponent() method as follows:
\code{.cpp}
void RNGProvider::onInitComponent()
{
    std::time_t now = std::time(0);
    gen = boost::random::mt19937{static_cast<std::uint32_t>(now)};
}
\endcode
This will instantiate a random number generator with the current system time as seed on initialization of the component.
Finally, we implement the generateRandomInt() method.
\code{.cpp}
Ice::Int RNGProvider::generateRandomInt(const Ice::Current &c)
{
    return gen();
}
\endcode
To test whether or not our RNGProvider is actually doing something, we add the following lines to the onConnectComponent() method
\code{.cpp}
ARMARX_IMPORTANT << "RNG output: " << generateRandomInt();
\endcode
This will produce an output to the ARMARX_IMPORTANT debug channel when the component is connected.
Run CMake on the package once again and compile it afterwards!

\section secCT_G Testing the component
Now let's have a look at what our implementation does so far.
Make sure the Ice service is running on your local system or in your network and that you can connect to it.
This can be done via the following command:
\code{.sh}
armarx status
\endcode
If you get a message like the following you first need to start Ice
\code
Exception while initializing Ice:
Ice.ConnectionRefusedException:
Connection refused
IceGrid is not running
Try starting it with 'armarx start'
\endcode

Next, start ArmarXGui via
\code{.sh}
armarx gui
\endcode
Once ArmarXGui opens, add the LogViewer widget to it by clicking on 'Add Widget' in the menu bar, then 'Meta', then 'LogViewer' (if it's not open by default anyway).
The LogViewer provides access to all the (debug) messages that are created by ArmarX and also includes useful filter functions.
With the LogViewer running, execute the RNGProvider be running the RNGProviderAppRun binary in a terminal window from the `ComponentsExample/build/bin` folder.
\code{.sh}
./RNGProviderAppRun
\endcode
If you now take a look at the LogViewer there should be an output by the RNGProvider, highlighted in green.
This means that our component actually can generate random integers, just as we wanted.

You might ask yourself what all this effort about the Ice interface was about if we are not using it anyway.
Well, if you made it this far in the tutorial, you're about to find out!


\section secCT_H Create another component

To call our random number generator via Ice, we're creating another application/component pair, the 'RNGCaller'.
This is done in exactly the same way as for the RNGProvider.
Navigate to the root folder of the ComponentExample package.
Then use the armarx-package tool as follows:
\code{.sh}
armarx-package add component RNGCaller
armarx-package add application RNGCaller
\endcode
This command also automatically updates the relevant CMakeLists.txt files so that after running CMake on your package, the RNGCaller application and component should appear in your project hierarchy.
 
This component does not offer any functionality through Ice and thus, we don't need to define an Ice interface for it.

\section secCT_I Implement component and update application
First, our newly created component needs to learn about the RNGProviderComponentIceInterface.
This is done by including it in the include section of the ComponentsExample/components/RNGCaller/RNGCaller.h
\code{.cpp}
#include <ComponentsExample/interface/RNGComponentProviderIceInterface.h>
\endcode
Additionally, we need to link against the ComponentsExampleInterfaces library.
This is done by updating the CMakeList.txt file located in the same folder as the RNGCaller component header file with the following line:
\code{.cmake}
set(COMPONENT_LIBS ArmarXCoreInterfaces ArmarXCore ComponentsExampleInterfaces)
\endcode
Then, we need to define a string property which holds the name of the component we want to access through Ice.
In our case, that's the RNGProvider.
To do so, we add the following line of code to the constructor of the RNGCallerPropertyDefinition class:
\code{.cpp}
defineOptionalProperty<std::string>("RNGProviderName", "RNGProvider", "Name of the component that offers the RNG");
\endcode

\image html components-tutorial-constructor.png


Additionally, we need to declare the proxy object that handles the actual interaction with the RNGProvider component through Ice.
We just add a private member to the RNGCaller class in the header file of the RNGCaller component such as this:
\code{.cpp}
private:
        RNGProviderComponentInterfacePrx interfacePrx;
\endcode

We're almost done now.
Head over the the RNGCaller source file.
Add this line to the onInitComponent() method
\code{.cpp}
usingProxy(getProperty<std::string>("RNGProviderName").getValue());
\endcode
Finally, implement the onConnectComponent() method as follows:
\code{.cpp}
interfacePrx = getProxy<RNGProviderComponentInterfacePrx>(getProperty<std::string>("RNGProviderName").getValue());
ARMARX_IMPORTANT << "RNGProvider component output over Ice:  " << interfacePrx->generateRandomInt();
\endcode
This instantiates the proxy to the RNGProvider prior to using it to generate a random integer value when the component is initialized.

Run CMake on the package once again and compile it!
Afterwards you will have a two applications executing the two components separately.

We're done! Woo!
The only thing remaining now is having a look at what our code does.

\section secCT_J Testing both components
Again, as already done previously, open the ArmarX GUI and append the LogViewer plugin.
If you now run the binaries RNGProviderAppRun and RNGCallerAppRun located in `ComponentsExample/build/bin` the LogViewer shows you two debug messages highlighted in green.
The first is generated by the RNGProvider component itself as already observed earlier in this tutorial.
The second one is issued on intialization of the RNGCaller component by calling the generateRandomInt() method on the proxy that talks over the Ice interface with the RNGProvider component.

Alternatively you can run both components at once. To do so first open the
scenario manager (Add widget -> Meta -> ScenarioManger).
Now we have to register our package. Click on the wrench symbol next to Edit Mode and press open. Once you typed the name of our package (ComponentsExample) the source should be generated automatically.  
Add a new scenario to the scenario view by clicking the new scenario button. Name it how ever you like and make sure to select our newly created package.
Now enable the edit mode and search the application database for our RNGCaller and RNGProvider Apps. Drag and drop them to our new scenario. 
After pressing the start button you should see a similar output to the log viewer as above.

And that's how you create new components in a few easy steps!

*/
