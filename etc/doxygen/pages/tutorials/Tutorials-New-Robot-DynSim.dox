/**
\page ArmarXCore-Tutorials-new-robot-dynsim New Robot Tutorial 3: Starting the dynamic kinematic simulation

\tableofcontents

This tutorial will expand upon the \ref ArmarXCore-Tutorials-new-robot-sim "first tutorial" by extending the existing YouBotSimulation scenario. It also doesn't hurt to be
familiar with the KinematicUnitGUI introduced in the \ref ArmarXCore-Tutorials-new-robot-kinsim "last tutorial".

We start off by adding the components KinematicUnitDynamicSimulationApp and RobotStateComponent to YouBotSimulation. Make sure the scenario view looks similar to the following screenshot (the added applications are highlighted):
\image html Tutorials-new-robot-dyn-added-apps.png

And again, configure the added applications by setting the following values:
<table>
        <tr><td>Application</td><td>Properties to set</td></tr>

<tr><td>KinematicUnitDynamicSimulationApp.cfg</td><td><pre>
ArmarX.KinematicUnitDynamicSimulation.RobotNodeSetName = Joints_Revolute
ArmarX.KinematicUnitDynamicSimulation.RobotFileName = YouBotTutorial/YouBot.xml
</pre></td></tr>

<tr><td>RobotStateComponent.cfg</td><td><pre>
ArmarX.RobotStateComponent.AgentName = youBot
ArmarX.RobotStateComponent.RobotNodeSetName = Joints_Revolute
ArmarX.RobotStateComponent.RobotFileName = YouBotTutorial/YouBot.xml
</pre></td></tr>
</table>
\note Again, replace the robot file and robot node set to match your robot.

To continue, start the scenario using the GUI or the CLI:
\verbatim
\verbatim
${ArmarX_DIR}/ArmarXCore/build/bin/armarx scenario start YouBotSimulation -p YouBotTutorial
\endverbatim
\endverbatim

You should already see the simulation.
In ArmarXGui, ```Add Widget -> RobotControl -> KinematicUnitGUI``` and setup your robot. Any joint angle changes you make in the KinematicUnitGUI should be reflected
in the simulation. Your setup should look similar to this:

\image html Tutorials-new-robot-dynsim.png

*/
