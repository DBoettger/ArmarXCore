/**
\page ArmarXCore-Tutorials-sce-waving Extended Statechart Editor Tutorial

\tableofcontents

\section ArmarXCore-Tutorials-sce-waving-introduction Introduction

In this second tutorial covering programming in ArmarX using the Statechart Editor, we will expand the program that we
created in the \ref ArmarXCore-Tutorials-sce-introduction "first tutorial" quite a bit. We will add another widget to the GUI that
lets us see a 3D simulation of Armar3. We will create and add states that let Armar3 move in this simulation. Once our
program is completed, compiled and running, Armar3 will wave his left hand at us. The waving actions will be 
counted by our CounterState. Once Armar has waved the specified number of times, the program will terminate.

\note The goals of this tutorial is to make you even more familiar with the Statechart Editor. You will get to know and learn
how to use the KinematicUnit that lets you connect to the robot simulation. You will furthermore learn how to use more
complex parameters for states in the forms of maps, scripted in JSON.

\attention This tutorial assumes that you have completed the \ref ArmarXCore-Tutorials-sce-introduction "first tutorial" and saved the
code that you have created. Steps that were described in the first tutorial won't be described as detailed again.

\section ArmarXCore-Tutorials-sce-waving-getting-started Getting Started

If your Statechart Editor in the ArmarX GUI is still open from the first tutorial, you are ready to go. If not, make sure
Ice is running on your system by executing
\code{.sh}
${ArmarX_DIR}/ArmarXCore/build/bin/armarx start
\endcode

and start the ArmarX GUI with the following command:
\code{.sh}
${ArmarX_DIR}/ArmarXCore/build/bin/armarx gui
\endcode

Open the Statechart Editor widget. Now we will open what we have created in the first tutorial. To do so, hit the "Open
Statechart Group" button as shown in the following image. The statechart that we previously created should be located in
 ${ArmarX_DIR}/StateChartTutorial/source/StateChartTutorial/statecharts/ExampleStateChartGroup/ExampleStateChartGroup.scgxml.
 Find this file and open it.
\note Alternatively, you can click on the gearwheel icon and just select the StateChartTutorial folder with "Add Path".
This will cause the Statechart Editor to search all subfolders for any scgxml files to open, which will save you a lot of clicking in the long run.

\image html waving-tutorial-00-load-state-chart.png

\section ArmarXCore-Tutorials-sce-waving-GUI-Implementation Expand the statechart in the GUI

\subsection ArmarXCore-Tutorials-sce-waving-create-states	Create two new state classes

By selecting the folder "ExampleStateChartGroup" and clicking the button "New State Definition", create two new states (don't forget to check "Create C++ Files").
One should be named "MoveJoints" and one should be named "SetJoints". After creating these two states, your Statechart
explorer should look similar to this:
\image html waving-tutorial-00a-state-tree.png

MoveJoints will be able to move one or more joints in the robot simulation with specified velocities until they reach their
specified target positions.
SetJoints will simply set the joints to their specified target positions with more or less infinite "velocity".
Both states will fire events that trigger transitions when the target pose is reached.



\subsection ArmarXCore-Tutorials-sce-waving-edit-states Edit the state classes in the GUI

It is now time to outline the structure of our program: Once the program starts, it will enter a state that sets the
position of Armar's joints to an initial value. This state will be of the type SetJoints. From there, it will transition into
a state that lets Armar move his hand forward. This state will be of type MoveJoints. It will then transition into a second
MoveJoints state that lets Armar wave his hand backwards again. This will continue for a while. However, in between waving forward
and backward we will hook up our CounterState that will keep track of how often Armar has waved yet. If he has waved often enough,
the counter will not transition to the next MoveJoints state but to the end state, and the program will terminate.

\subsubsection ArmarXCore-Tutorials-sce-waving-edit-the-Statechart Edit the Statechart
In case MainState is not public yet, right click on MainState and make it a public state. Now double click on MainState to open it in the GUI.

To create the three new states in the statechart, drag&drop SetJoints once and MoveJoints twice into the main state.
Rename SetJoints to "InitPose", "MoveJoints" to "WaveFwd" and "MoveJoints_2" to "WaveBack" (right click on the respective state -> select "edit state" -> select tab "General" -> set "State Instance name").
Feel free to delete the Success/Failure sub-states within InitPose and WaveFwd/WaveBack.

Both newly created state classes (SetJoints and MoveJoints) need parameters and events. For both states, add the event "OnPoseReached" as it was described
in the first tutorial. The following image is just a reminder as of how to create events:
\image html waving-tutorial-01-setjoints-events.png

\note It does not matter whether you edit WaveBack or WaveFwd, as they are both instances of the same class MoveJoints.
If you add an event in WaveBack for example, it will automatically appear in WaveFwd, too.

Both states also need input parameters. InitPose (instance of SetJoints) only needs to know where to set the joints and therefore only needs one parameter,
that we will name "JointTargetPose". It is of type Map(float), which allows us
to specify multiple joint names and
their respective positions in only one parameter. We will look into how to edit this map later. For now, just make sure that
your input parameter tab of the InitialPose state looks like this:
\image html waving-tutorial-02-setjoints-input-parameters.png
\note You can only select what the value type of Map is (here it's float); the key type is always string

MoveJoints will have the very same input parameter. But not only does it need to know where to set the joints but also how fast it
should move them. So it needs a second parameter that specifies the joint velocities. We will name it "JointVelocity" and it will
also be of the same map-type.
The input parameter section of your MoveJoints instances WaveBack and WaveFwd should look like this now:
\image html waving-tutorial-04-movejoints-input-parameters.png

Connect the transitions as seen in the picture below.
\note You can set a state as initial state by right clicking on it and selecting „set as initial state“. 
Drag the tips of the other positions to the respective state.

\image html waving-tutorial-05-main-state.png

Once you click on the Layout State button, it should look like this:
\image html waving-tutorial-06-connect-transitions.png

\subsection ArmarXCore-Tutorials-sce-waving-edit-the-mainstate Edit the Main State
So far, we have not really learned anything new. This will change now. We will edit the MainState, we will give it a lot
of maps as input parameters and we will learn how to edit these parameter maps as JSON scripts.
Open the input parameter tab of the MainState and add the five new parameters that you can find in this list:

| Key | Type | Optional | Def |
| --- | ---- | -------- | --- |
| JointValueMapInitPose | Map(float) | false | yes |
| JointValueMapWaveBack | Map(float) | false | yes |
| JointValueMapWaveFwd | Map(float) | false | yes |
| JointVelocityMapWaveBack | Map(float) | false | yes |
| JointVelocityMapWaveFwd | Map(float) | false | yes |

After that, the input parameter tab should look like this:
\image html waving-tutorial-07-mainstate-input-parameters.png

Now comes the interesting part. Similarly to "counterMaxValue" we can also define default values
for complex types such as "Map(float)". Click on "Click to set" and then on the "Edit" button of "JointValueMapInitPose". A small editor window will open
where you can input what your JointValueMapInitPose should contain. It is essentially a 2D list that contains any number
of joints and their positions as absolute joint angle values. Have a look at the syntax to understand more about it.
You can copy and paste the default values for the initial pose from the following block of code:
\verbatim
{
  "Shoulder 1 L": -1.25,
  "Shoulder 2 L": 1.5,
  "Elbow L": -1.38,
  "Underarm L": 1.0
}
\endverbatim

Similarily, copy and paste the other scripts into their respective editor windows.

Default values for JointValueMapWaveBack:
\verbatim
{
  "Elbow L": -0.8
}
\endverbatim

Default values for JointValueMapWaveFwd:
\verbatim
{
  "Elbow L": 0.3
}
\endverbatim

Default values for JointVelocityMapWaveBack:
\verbatim
{
  "Elbow L": -1.0
}
\endverbatim

Default values for JointVelocityMapWaveFwd:
\verbatim
{
  "Elbow L": 1.0
}
\endverbatim

\subsection ArmarXCore-Tutorials-sce-waving-edit-transitions Edit the transitions
You will need to edit all the transitions in your statechart diagram to tell them which parameters to pass and from where
they come. To do so, right click on the arrow that represents a transition. The parameters expected by the target state will
already be displayed. All you have to do is to select "Parent input" as Source Type and to choose a feasible source parameter
from the drop-down list. With the help of the
parameter names, this should be relatively easy. The following image gives you an example of how the edited transition from
the main state to the InitPose should look like:
\image html waving-tutorial-08-init-transitions.png

\note In case there are no source parameters in the drop-down list or a parameter is missing,
you selected an incorrect type for some parameter somewhere.

\section ArmarXCore-Tutorials-sce-waving-preparing-the-kinematic-unit-proxy Preparing the Kinematic Unit Proxy
In order to manipulate the joint angles, we need access to a KinematicUnitInterface. A kinematic unit, which
implements a KinematicUnitInterface, is a sensor-actor-unit that allows us to control the joints of a robot.
Here, this will allow us to make a simulation of Armar move its arm, which we will be able to see in a GUI plugin.

\subsection ArmarXCore-Tutorials-sce-waving-add-the-kinematic-unit-proxy Add the Kinematic Unit Proxy

First, right-click the ExampleStateChartGroup and select Group Properties. Under the tab "Proxies", select "[RobotAPIInterfaces] Kinematic Unit" and confirm with OK:
\image html waving-tutorial-09-add-proxy.png

Doing this will allow us to communicate with the kinematic unit by calling getKinematicUnit() in any of the states. We will use that to set the joint angle positions/velocities.
Once you're done, don't forget to save your statechart. The generated state file will now contain a function to get the Kinematic Unit proxy (getKinematicUnit()).

\subsection ArmarXCore-Tutorials-sce-waving-configure-the-kinematic-unit-proxy Configure the Kinematic Unit Proxy

Now open the tab "Configuration". Select the profile "Armar3Base" and add the following line to the configuration text box:
\code{.ini}
ArmarX.ExampleStateChartGroupRemoteStateOfferer.KinematicUnitName = Armar3KinematicUnit
\endcode
Alternatively you can press the button "Add Default Properties" which generates the property for the KinematicUnitName and you only have to fill in the name, i. e. Armar3KinematicUnit.
\note Don't forget to remove the comment "#" from the property line.
\image html waving-tutorial-group-add-configuration.png
It tells our statechart which kinematic unit to use.

\section ArmarXCore-Tutorials-sce-waving-edit-the-source Edit the source code

\subsection ArmarXCore-Tutorials-sce-waving-add-dependencies Add dependencies

In order to use the Kinematic Unit proxy we need to add a dependency on RobotAPI in 
${ArmarX_DIR}/StateChartTutorial/CMakeLists.txt.
\code{.txt}
depends_on_armarx_package(RobotAPI)
\endcode

In the CMakeLists of our Statechart Group
(${ArmarX_DIR}/StateChartTutorial/source/StateChartTutorial/Statecharts/ExampleStateChartGroup/CMakeLists.txt), we need to
add the dependencies "Eigen3" and "Simox". You can do this by uncommenting this code (Ctrl+/ in Qt Creator)
\code{.txt}
find_package(Eigen3 QUIET)
find_package(Simox QUIET)


armarx_build_if(Eigen3_FOUND "Eigen3 not available")
armarx_build_if(Simox_FOUND "Simox-VirtualRobot not available")


if (Eigen3_FOUND AND Simox_FOUND)
    include_directories(
        ${Eigen3_INCLUDE_DIR}
        ${Simox_INCLUDE_DIRS}
    )
endif()
\endcode
and by editing the component list so that it contains the following items:
\code
set(COMPONENT_LIBS
    RobotAPIInterfaces
    ArmarXCoreInterfaces ArmarXCore ArmarXCoreStatechart ArmarXCoreObservers)
\endcode



Finally, configure your project by running CMake inside a terminal:
\code{.sh}
cd ${ArmarX_DIR}/StateChartTutorial/build
cmake ..
\endcode
or from inside Qt Creator.

\subsection ArmarXCore-Tutorials-sce-waving-edit-the-sources Edit MoveJoints.cpp and SetJoints.cpp
So far we only defined the OnPoseReached event in MoveJoints and SetJoints. We have not yet specified when these events will
be triggered. This is what will do now in the .cpp-source of our two states.

Open MoveJoints.cpp. Find the onEnter()-method which should be empty. Insert the following code. It will trigger the
OnPoseReached event if the actual pose is very close to the specified pose (+/- 0.05).

\code{.cpp}
// get the target joint values
std::map<std::string, float> jointValueMap = in.getJointTargetPose();

//build conditions for OnPoseReached
Term poseReachedConditions;
const float eps = 0.05f;
for (const auto& jointNameValue : jointValueMap)
{
    std::string jointNameDatafield = "Armar3KinematicUnitObserver.jointangles." + jointNameValue.first;
    float jointValue = jointNameValue.second;

    Literal jointValueReached(jointNameDatafield, "inrange",
                              Literal::createParameterList(jointValue - eps, jointValue + eps));
    poseReachedConditions = poseReachedConditions && jointValueReached;
}

installConditionForOnPoseReached(poseReachedConditions);
\endcode

Do the same in SetJoints.cpp.
\note By calling the function \ref armarx::StateUtility::installCondition "installCondition"ForOnPoseReached(poseReachedConditions) we are telling
the \ref armarx::ConditionHandler "ConditionHandler" to emit an event OnPoseReached once the conditions
we define in the term currentConditions are fulfilled. The joint name refers to a
\ref armarx::KinematicUnitObserver "KinematicUnitObserver" that observes the joint values in our \ref armarx::KinematicUnit "KinematicUnit". See \ref Observers
to learn more about observers and sensor-actor-units.

Now we will actually and finally tell the two states what they are supposed to do. In the run()-method of MoveJoints.cpp,
replace any existing code with the following. Have a look at the code, it is quite interesting. The essence lies in the very last line, of course.
Don't forget to uncomment the definition of the run()-Method in the header file.
\code{.cpp}
std::map<std::string, float> jointVelocityMap = in.getJointVelocity();
NameControlModeMap velocityControlModeMap;

for (const auto& jointVelocity : jointVelocityMap)
{
    velocityControlModeMap[jointVelocity.first] = eVelocityControl;
}

KinematicUnitInterfacePrx kinUnit = getKinematicUnit();
kinUnit->switchControlMode(velocityControlModeMap);
kinUnit->setJointVelocities(jointVelocityMap);
\endcode

First, we tell the kinematic unit that we wish to velocity control all the joints defined in our input joint velocity vector. Then, in the last line we set the desired velocity.

Delete what you can find in SetJoints::run() and insert the following code. You can see a close similarity to the code above.
Again, the essence of what to do lies in the last line.
\code{.cpp}
std::map<std::string, float> jointValueMap = in.getJointTargetPose();
NameControlModeMap positionControlModeMap;

//sets to position control mode the joints in the map
for (const auto& jointNameValue : jointValueMap)
{
    positionControlModeMap[jointNameValue.first] = ePositionControl;
}

KinematicUnitInterfacePrx kinUnit = getKinematicUnit();
//switch to position control
kinUnit->switchControlMode(positionControlModeMap);
// set the angles defined by the joint target pose
kinUnit->setJointAngles(jointValueMap);
\endcode

\section ArmarXCore-Tutorials-sce-waving-compile Compile your program
Compile your program from the terminal by executing cmake and make in your build directory:
\code{.sh}
cd ${ArmarX_DIR}/StateChartTutorial/build
cmake ..
make -j4
\endcode

\section ArmarXCore-Tutorials-sce-waving-scenario Edit the scenario

We will first open our scenario in edit mode. Then we will add and configure new applications.

\subsection ArmarXCore-Tutorials-sce-waving-open-scenario-manager Open scenario in edit mode

Open the scenario manager ("Add Widget"->Meta->ScenarioManager). If the StartStateChart scenario
is not loaded automatically, use the open scenario button. You can use the text box to search for
our scenario and load it via the open button next to its name.
\image html waving-tutorial-open-scenario.png

Enable the edit mode by clicking the corresponding toggle button. Otherwise you will neither be able
to edit the properties of the scenario nor add new applications.
\image html waving-tutorial-scenario-edit-mode.png

\subsection ArmarXCore-Tutorials-sce-waving-add-new-apps Add new applications

Use the search in the application database to search for KinematicUnitObserver and KinematicUnitSimulation. Add these application to the StartStateChart scenario using drag and drop.
The result should look similar to the following screenshot:
\image html waving-tutorial-add-scenario-dependencies.png

If you get an error similar to the following:
\image html waving-tutorial-add-scenario-dep-error.png
you should make sure that you added the dependency on RobotAPI to the correct CMakeLists.txt and CMake has been run after this change. It my be necessary to restart the scenario manager so all changes are detected.

\subsection ArmarXCore-Tutorials-sce-waving-configure-new-apps Configure applications

If you take a look at the scenario view, you'll notice that in contrast to the first tutorial
we now have two additional applications: KinematicUnitSimulation and KinematicUnitObserver.
The application KinematicUnitSimulation creates a simulated kinematic unit.
KinematicUnitObserver creates an observer that observes our simulated kinematic unit, which
we need to detect when we reached our target poses.

Before we can run our program and see Armar wave, we need to edit the properties of these applications.

First click on KinematicUnitObserver in the scenario view and set the values for the following properties:
\code{.ini}
ArmarX.KinematicUnitObserver.RobotFileName = RobotAPI/robots/Armar3/ArmarIII.xml
ArmarX.KinematicUnitObserver.RobotNodeSetName = Robot
ArmarX.KinematicUnitObserver.ObjectName = Armar3KinematicUnitObserver
\endcode

This tells the component KinematicUnitObserver which robot model to use and what the
name of the observer should be. Note that we used the very same name in our onEnter()-methods.

Next repeat the process for the KinematicUnitSimulation and set these properties:
\code{.ini}
ArmarX.KinematicUnitSimulation.RobotFileName = RobotAPI/robots/Armar3/ArmarIII.xml
ArmarX.KinematicUnitSimulation.RobotNodeSetName = Robot
ArmarX.KinematicUnitSimulation.ObjectName = Armar3KinematicUnit
\endcode

As stated before the KinematicUnitSimulation creates the kinematic unit use in our run()-methods.
It simulates the kinematics of the specified robot - here Armar3. The object name defines the name of the kinematic unit.

\section ArmarXCore-Tutorials-sce-waving-run-the-program Run the program

Now start the scenario via the GUI or use the terminal:
\code{.sh}
${ArmarX_DIR}/ArmarXCore/build/bin/armarx scenario start StartStateChart -p StateChartTutorial
\endcode

In order to visualize what the scenario is actually doing, we have to set up the GUI. If the GUI is not open, open it. Add the widget
RobotControl->KinematicUnitGUI. In the dialog that pops up ArmarIII kinematic unit must appear automatically. Just confirm the default configuration parameters.
Opening the KinematicUnitGUI will also open the 3D Viewer in which Armar3 is displayed.
It might take a few seconds before you can see a full body model of Armar3!


You can arrange the widgets by dragging them around. A useful configuration of the GUI with the scenario manager and the
KinematicUnit GUI might look like this:
\image html waving-tutorial-10-gui-full.png

You should finally see that Armar3 waves his left hand five times and then stops!

You can now stop and restart your scenario to see it again!
To stop the scenario either use buttons in the scenario manager or the terminal:
\code{.sh}
${ArmarX_DIR}/ArmarXCore/build/bin/armarx scenario stop StartStateChart -p StateChartTutorial
\endcode

*/
