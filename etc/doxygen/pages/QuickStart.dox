/**

\page ArmarXCore-QuickStart QuickStart

\tableofcontents

First, you should follow the instructions at \ref ArmarXDoc-Installation "ArmarX Installation".

After installing ArmarX either via packages or from sources you are ready to continue with the Quick Start guide below. In addition you can find various Tutorials at the \ref ArmarXDoc-Tutorials "ArmarX Tutorials Section".

\section ArmarXCore-QuickStart-Memory Initializing the Robot Memory

Initially, the robot's memory needs to be filled with Prior Knowledge and snapshots containing an initial scene (more details on the memory concepts of ArmarX can be found here: \ref MemoryX-Overview "MemoryX"). 
\subsection ArmarXCore-QuickStart-Getting-DB Getting the memory snapshot
The standard memory snapshot for starting with ArmarX is stored in a gitlab repository: <a href="https://gitlab.com/ArmarX/ArmarXDB">ArmarXDB</a>
If you have compiled from sources it should be checked out to $ARMARX_DIR/ArmarXDB. Otherwise check it out now.

The example below adds some predefined objects and a kitchen environment to the mogoDB database.
Execute the following commands in a Linux shell (if you installed from sources make sure you export ${ArmarX_DIR}/ArmarXCore/build/bin and ${ArmarX_DIR}/MemoryX/build/bin to your $PATH variable):

\code
# start Ice
armarx start

# start MongoDB
# check http://armarx.humanoids.kit.edu/MemoryX-gettingstarted.html in case of errors
armarx memory start

# import environment and object models into the database
mongoimport.sh ${ArmarXDB_DIR}/data/ArmarXDB/dbexport/memdb

\endcode

\section ArmarXCore-QuickStart-Simulator-Package The Armar3 Simulation Scenario

The \ref ArmarXSimulation-Overview "ArmarXSimulation" package contains all you need to start up a simulated robot and environment. The simulator can be started with the <i>Armar3Simulation</i> scenario which is part of the Tutorials package and contains a simulated Armar3 robot and a kitchen environment.
To start the simulated scenario run the following commands in a terminal.
\code
#start the ArmarX simulation scenario
cd..
${ArmarX_DIR}/ArmarXCore/build/bin/armarx scenario start Armar3Simulation
\endcode
\note Instead of giving just the name of the scenario you can also specify the path to the *.scx file.

The simulator viewer should now pop up showing a view on the simulated scene, similar to the picture below.
\image html "images/SimulatorStart.png"

\section ArmarXCore-QuickStart-Gui The ArmarX Gui

Several GUIs are provided via the \ref ArmarXGui-Overview "ArmarXGui" which can be used to send commands to the robot and to inspect the current state of the system. An ArmarXGui main window can be started with 
\code
armarx gui
\endcode

The ArmarX Gui Use Case Selection will open up. For now it doesn’t matter use case you choose. Just pick one or click „open empty GUI“. 
In the following you will have a look at the basic widgets. To open a widget click on add widget and then choose the appropriate one.


\subsection ArmarXCore-QuickStart-Gui-Simulator The Simulator Control GuiPlugin
The ArmarXSimulator can be accessed with the <i>SimulatorControlGuiPlugin</i>. This plugin gives you a quick overview of your current robot setups. In order to see some values the ArmarXSimulation needs to be running.

\image html "images/SimulatorGui.png"

\subsection ArmarXCore-QuickStart-Gui-SystemState The System State GuiPlugin
The Gui Plugin <i>Meta->SystemStateMonitor</i> can be used to inspect the dependencies and whether all components are running properly:
\image html "images/SystemStateGui.png"

\subsection ArmarXCore-QuickStart-Gui-WM The Working Memory GuiPlugin
The <i>MemoryX->WorkingMemoryGui</i> shows the current content of the robot's Working Memory.
\hint When you open this plugin, two tabs will open. They can be arranged as shown below by dragging and dropping them.
\image html "images/WorkingMemory.png"

\subsection ArmarXCore-QuickStart-Gui-Units Robot Control GuiPlugins
Commands can be sent to the robot via the <i>RobotControl->KinematicUnitGui</i> and the <i>RobotControl->PlatformUnitGui</i>:
\image html "images/UnitGuis.png"
Choose one joint by clicking on it and move the slider on top. You should see the changes of the joint position on your robot on the right. Go ahead and try it out with a few joints.


\subsection ArmarXCore-QuickStart-Gui-Vision The VisionX ImageMonitor GuiPlugin
The simulated cameras can be inspected with the <i>VisionX->ImageMonitor</i> gui plugin. Note, that you need to configure the plugin via the small spanner icon and the playback needs to be started with the play button.
\image html "images/ImageMonitor.png"

\section ArmarXCore-QuickStart-Stop Shutting Down ArmarX
ArmarX can be stopped with
\code
#stop the ArmarX simulation scenario
${ArmarX_DIR}/ArmarXCore/build/bin/armarx scenario stop Armar3Simulation 
#or kill with
${ArmarX_DIR}/ArmarXCore/build/bin/armarx scenario kill Armar3Simulation

armarx memory stop
armarx stop
\endcode
*/
