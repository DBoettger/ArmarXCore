/**
\page armarx-package  ArmarX Package Tool
\tableofcontents


\section armarx-packages 1 ArmarX Packages

In a sentence, armarx-package is a tool to create ArmarX packages and their components.
Before we get into details about the armarx-package tool, lets take a brief look at the ArmarX package.


Organizing projects whether small or big is essential to maintain a clear overview and increase reusability.
As for ArmarX projects you will encounter various component offered by the ArmarX framework.
An ArmarX package is a way to organize these components along with your source code
and documentations and make them available as a package to be used in other packages.


A package is defined by its structure. This means there is no manifest file to describe a package.
However, the underlying structure is based on CMake and can be altered or extended as might be desired.
If you are not yet familiar with CMake build process, that is fine.
The armarx-package tool will do most of the work for you.
Otherwise, you will be guided in the few cases where manual CMake modifications are required.
For deeper insight check out \ref cmake "CMake" and \ref cmake-internals "CMake internals".
The minimal package structure is depicted below:

\verbatim
- <Package-Name>
  - source
    - <Package-Name>
  - etc
    - doxygen
    - cmake
  - interface
    - slice
      - <Package-Name>
  - build
  - data
\endverbatim

\note Check out \ref structure for more details on the package structure.


Now that we know briefly what an ArmarX package is, we can continue and create a package using the armarx-package tool


\section create-package 2 Creating ArmarX packages

armarx-package tool operates on sub-commands just as svn or git do.
The first sub-command we will take a closer look at is \em armarx-package \em init.
To create a package, first go to the location where you want to create your package and execute the following:

 \verbatim $ armarx-package init HelloWorldPackage \endverbatim

or you specify the location of your package simply using the --dir option:

 \verbatim $ armarx-package init HelloWorldPackage --dir=/home/Me/MyPackages \endverbatim

The latter case allows working with the armarx-package tool from any location. The created package should look like this:

\verbatim
- HelloWorldPackage
  - source
    - CMakeLists.txt
    - HelloWorldPackage
  - etc
    - doxygen
    - cmake
  - interface
    - slice
      - HelloWorldPackage
  - build
  - data
  - CMakeLists.txt [Top level CMake file]
  - gpl-2.0.txt
  - README.txt
\endverbatim

As you can see, armarx-package already created all CMake files needed for the package with the required configuration.
However, the package is still empty and doesn't contain any components.
The next section will discuss how to create package components.

\note Use the help command to get the list of available sub-commands\n
 \verbatim $ armarx-package help \endverbatim
 To get the help content of a sub-command use 'armarx-package help <sub-command>' or 'armarx-package <sub-command> --help' !\n
 \n
 For the sake of convenience, as for now most of \em CMakeLists.txt files will be hidden.


\section create-component-app 3 Creating package components

The ArmarX framework comes with various different components.
Each for a different purpose and with different capabilities to build up a comprehensive robot software.
One basic component type is the ArmarX application.
Creating a component using the tool is straightforward and is done in a  single step:

\verbatim
$ /home/Me/MyPackages/HelloWorldPackage$ armarx-package add application HelloWorld
> HelloWorld application component created.
\endverbatim

The sub-command used here is \em add:

\verbatim
usage: armarx-package add [-h] [-j] [-d DIR] [--verbose] componentType componentName
\endverbatim

As you can see, no paths have to be specified.
armarx-package creates the application component in the right place and performs
the CMake modifications needed to integrate the created component.
Depending on the component type, most of them will be placed in the \em source folder within the package.
If everything goes well, the package should now look like this:

\verbatim
- HelloWorldPackage
  - source
    - HelloWorldPackage
      - application
        - HelloWorld
          - CMakeLists.txt
          - HellowWorldApp.h
          - HellowWorldApp.cpp
          - main.cpp
  - etc
    ...
\endverbatim

Run the 'help' command on 'add' to get the list of components which can be
created by the armarx-package tool.

\verbatim
armarx-package help add
\endverbatim

\section list-component-types 4 Package status

The \em status sub-command will give you an overview of the package content

\verbatim
/home/Me/MyPackages/HelloWorldPackage$ armarx-package status
Current package: HelloWorldPackage

Content:

 application components:
  - HelloWorld
\endverbatim

\section removing-components 5 Removing package components

Package components are not a simple directory within the package.
They can consist of multiple directories and files which are integrated within the CMake structure.
For this reason removing a component is not straightforward if you are not familiar with CMake package internals.
The \em remove sub-command will do all the work for you given the component type and name:

\verbatim
$ armarx-package remove application HelloWorld
> HelloWorld component has been removed from the package.
> Note: The component is no longer a part of the package yet the files still remain in:
 /home/Me/MyPackages/HelloWorldPackage/source/HelloWorldPackage/application/HelloWorld
\endverbatim

The note displayed tells you that the component has been merely removed from
the CMake structure and still remains in the stated location.
This is helpful if you are using revision control software such as apache's  Subversion (SVN).

The output of running \em status should look like the following:

\verbatim
$ armarx-package status
Current package: HelloWorldPackage

Content:

> No components included
\endverbatim

To maintain an overview about the deleted components you can run
\em status \em -all which will list all components within a package including the removed ones:

\verbatim
$ armarx-package status --all
Current package: HelloWorldPackage

Content:

 application components:
  - HelloWorld [removed]
\endverbatim


Nevertheless, if you know what you are doing and insist on removing
the component entirely, use the \em --force option to delete the component's files as well:

\verbatim
$ armarx-package remove application HelloWorld --force
Deleting component files ...
> HelloWorld component has been removed completely from the package.
\endverbatim


\section reintegrate-components 6 Reintegrating package components

The counterpart of \em remove is \em add \em --join.
Assume you accidently removed the wrong component and want to reintegrate it
but you don't want to do that manually.
To do so, you have to merely add the component again using the \em --join option:

\verbatim
$ armarx-package add --join application HelloWorld
> HelloWorld component has been re-joined to the package.
\endverbatim

This will work unless you have removed the entire component using \em --force option.

\note The combination of \em remove and \em add \em --join can be used to move a component from one package to another.

\section command-synopses 7 Command synopses

\subsection cmd-armarx-package armarx-package
\verbatim armarx-package [-h] [-v] [--verbose] {init,add,remove,status,help} .. \endverbatim
optional arguments:\n
  - -h, --help            show this help message and exit\n
  - -v, --version         show program's version number and exit\n
  - --verbose             Provides additional information on errors\n
\n

\subsection cmd-armarx-package-new armarx-package init
\verbatim armarx-package init [-h] [-d DIR] [--verbose] packageName \endverbatim
optional arguments:\n
  - -h, --help         show this help message and exit\n

global arguments:\n
  - -d DIR, --dir DIR  ArmarX package top-level directory\n
  - --verbose          Provides additional information on errors\n

\n

\subsection cmd-armarx-package-create armarx-package add
\verbatim armarx-package add [-h] [-j] [-d DIR] [--verbose] component componentName\endverbatim
optional arguments:\n
  - -h, --help         show this help message and exit\n
  - -j, --join         Joins an existing component into the package\n

global arguments:\n
  - -d DIR, --dir DIR  ArmarX package top-level directory\n
  - --verbose          Provides additional information on errors\n
\n

\subsection cmd-armarx-package-remove armarx-package remove
\verbatim armarx-package remove [-h] [-f] [-d DIR] [--verbose] component componentName\endverbatim
optional arguments:\n
  - -h, --help         show this help message and exit\n
  - -f, --force        Force complete removal\n


global arguments:\n
  - -d DIR, --dir DIR  ArmarX package top-level directory\n
  - --verbose          Provides additional information on errors\n

\n

\subsection cmd-armarx-package-list-components armarx-package status
\verbatim armarx-package status [-h] [-a] [-p] [-d DIR] [--verbose] \endverbatim
optional arguments:\n
  - -h, --help            show this help message and exit\n
  - -a, --all             List all components including removed ones\n
  - -p, --print-location  Prints the component location\n


global arguments:\n
  - -d DIR, --dir DIR     ArmarX package top-level directory\n
  - --verbose             Provides additional information on errors\n

\n
*/
