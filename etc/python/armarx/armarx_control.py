##
# This file is part of ArmarX.
#
# ArmarX is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2 as
# published by the Free Software Foundation.
#
# ArmarX is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# @package    ArmarXCore::scripts
# @author     Manfred Kroehnert (Manfred dot Kroehnert at kit dot edu)
# @date       2015
# @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
#             GNU General Public License

__author__ = 'kroehner'

import Configuration
from commands import *
import ArmarXBuilder as ArmarXBuilder
import IceHelper
from Profile import Profile

import os
import sys
import ConfigParser
import argparse

from sys import modules

def checkAndCorrectProfile(profilename, configuration):
    """
    check if create Profile if it does not exist
    link the profile afterwards
    :return:
    """

    profile = Profile(profilename)

    if os.path.exists(os.path.join(configuration.armarxUserConfigurationDirectory, "armarx_sync_dir.icegrid.xml")):
        os.remove(os.path.join(configuration.armarxUserConfigurationDirectory, "armarx_sync_dir.icegrid.xml"))


    if not profile.exists():
        if not profile.create():
            raise StandardError("Error creating profile")
    if not profile.update():
        print "Warning: updating generated files failed!"
    profile.link()

    defaultsFiles = [configuration.getIceGridConfigForProfile(profilename), configuration.getIceGridGeneratedConfigForProfile(profilename)]

    configuration.defaultsFiles = defaultsFiles

    return profile


def checkAndGetArmarXCorePath():
    generator = ArmarXBuilder.ArmarXMakeGenerator(0)
    builder = ArmarXBuilder.ArmarXBuilder(generator, True, True, True, False)

    armarXCoreDir = Configuration.Configuration.armarxCoreDirectory

    if armarXCoreDir and os.path.exists(armarXCoreDir):
        return armarXCoreDir
    # try to retrieve ArmarXCore location via CMake if armarXCoreDir does not exist
    armarXCorePackageData = builder.getArmarXPackageData("ArmarXCore")

    if not armarXCorePackageData:
        return

    baseDirectories = builder.getArmarXPackageDataValue(armarXCorePackageData, "PROJECT_BASE_DIR")
    buildDirectories = builder.getArmarXPackageDataValue(armarXCorePackageData, "BUILD_DIR")
    includeDirectories = builder.getArmarXPackageDataValue(armarXCorePackageData, "INCLUDE_DIRS")
    configDirectories = builder.getArmarXPackageDataValue(armarXCorePackageData, "CONFIG_DIR")

    armarXCoreExists = baseDirectories and len(baseDirectories) > 0
    armarXCoreExists = armarXCoreExists or (buildDirectories and len(buildDirectories) > 0)
    armarXCoreExists = armarXCoreExists or (includeDirectories and len(includeDirectories) > 0)
    armarXCoreExists = armarXCoreExists or (configDirectories and len(configDirectories) > 0)

    if not armarXCoreExists:
        return

    if baseDirectories and baseDirectories[0]:
        armarXCoreDir = baseDirectories[0]
    else:
        armarXCoreDir = ""
    if buildDirectories and buildDirectories[0]:
        armarXCoreBuildDir = buildDirectories[0]
    else:
        armarXCoreBuildDir = ""
    armarxCoreIncludeDirs = includeDirectories[0]
    armarxCoreConfigDir = configDirectories[0]
    if os.path.exists(armarxCoreIncludeDirs) and os.path.exists(armarxCoreConfigDir):
        Configuration.Configuration.setArmarXCoreDir(armarXCoreDir, armarXCoreBuildDir, armarxCoreIncludeDirs, armarxCoreConfigDir)



def collectCommands(commandType = None):
    """
    collect all subclasses of the armarx.commands.Command class and create a dictionary{commandName -> commandClass)
    :return:
    """
    commands = dict()
    for cls in Command.getSubclasses(commandType):
        commands[cls.getCommandName()] = cls
    return commands


def autoCompletionEnabled(armarxUserConfigDir, scriptName, inipath, skipQuestion = False):
    if not os.path.exists(armarxUserConfigDir):
        os.makedirs(armarxUserConfigDir)
    Config = ConfigParser.ConfigParser()
    if skipQuestion:
        print("Not asking for autocompletion")
    Config.read(inipath)
    try:
        enabled = Config.get('AutoCompletion', scriptName) if Config.has_section('AutoCompletion')  else '0'
        if enabled == '1':
            return True
        else:
            return False
    except:
        # not found, ask what to do
        if not os.isatty(sys.stdout.fileno()) or skipQuestion:
            # non interactive shell -> so ask the user
            return False

        newbashline = 'eval "$(register-python-argcomplete ' + scriptName + ')"'
        newbashlineEscaped = 'eval \\"\\$(register-python-argcomplete ' + scriptName + ')\\"'
        cmd ='echo "' + newbashlineEscaped + '" >> $HOME/.bashrc'
        print "For autocompletion it is needed to add the following line to .bashrc:\n", newbashline
        decision = raw_input("Enable autocompletion in .bashrc? (Decision will be stored in " + inipath +") (Y/n) ")

        if decision == '' or decision == 'Y' or decision == 'y':
            enabledAutoCompletion = '1'

            os.system(cmd)
            print "You need to source your .bashrc again and run the script again: source $HOME/.bashrc"

        else:
            enabledAutoCompletion = '0'

        # lets create that config file for next time...
        cfgfile = open(inipath,'w')
        # add the settings to the structure of the file, and lets write it out...
        if not Config.has_section('AutoCompletion'):
            Config.add_section('AutoCompletion')
        Config.set('AutoCompletion',scriptName,enabledAutoCompletion)
        Config.write(cfgfile)
        cfgfile.close()
        if enabledAutoCompletion == '1':
            exit(1)
        return enabledAutoCompletion


def cli(scriptName, commandTypes = None):
    """
    1. get configuration
    2. initialize Ice connection if Ice is running
    3. collect all subclasses of Command and store them in a dictionary
    4. select the matching command from the first commandline parameter
    5. check provided arguments with number of required arguments
    6. Command.execute(commandArguments)
    7. shutdown Ice connection

    :return:
    """

    checkAndGetArmarXCorePath()
    configuration = Configuration.Configuration
    Profile.setDirectories(configuration.armarx_profiles_directory, configuration.armarxUserConfigurationDirectory, configuration.armarxCoreConfigurationDirectory)

    Profile.setConfigurationFiles([configuration.iceGridDefaultConfigurationFilename, configuration.iceGridGeneratedDefaultConfigurationFilename, configuration.iceGridSyncVariablesFile])

    iceHelperConfig = IceHelper.IceHelperConfig(configuration.iceGridDefaultConfigurationFile, configuration.iceGridRegistryName, configuration.iceGridUsername, configuration.iceGridPassword)


    commands = collectCommands(commandTypes)

    parser = argparse.ArgumentParser(description='ArmarX command line tool for various interactions with the ArmarX RDE',
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-y',
                           help='Do not ask for enabling-auto-completion',
                           action="store_true", dest="no_interrupt")
    no_interrupt = False
    if len(sys.argv) > 1:
        no_interrupt = sys.argv[1] == '-y'
        if sys.argv[1] == '-y':
            del(sys.argv[1])

    subparserAction = parser.add_subparsers(dest='command')
    for cmd in commands:
        commands[cmd].setIceHelperConfig(iceHelperConfig)
        commands[cmd].setConfiguration(configuration)
        subparser = subparserAction.add_parser(cmd, description=commands[cmd].getHelpString(), help=commands[cmd].getHelpString(), formatter_class=argparse.ArgumentDefaultsHelpFormatter)
        commands[cmd].addtoSubArgParser(subparser)


    if autoCompletionEnabled(configuration.armarxUserConfigurationDirectory, scriptName, configuration.armarx_ini, no_interrupt):
        try:
            from argcomplete import autocomplete
            autocomplete(parser, False)
        except:
            print "No Autocompletion available"

    args = parser.parse_args()






    ini_parser = ConfigParser.ConfigParser({"profile": "default"})
    if os.path.exists(configuration.armarx_ini):
        with open(configuration.armarx_ini, "r") as ini_file:
            ini_parser.readfp(ini_file)

    configuration.profilename = ini_parser.get("DEFAULT", "profile")

    profile = checkAndCorrectProfile(configuration.profilename, configuration)





    commandName, commandArguments = "", []

    commandName = args.command
    if len(sys.argv) > 2:
        commandArguments = sys.argv[2:]


    commandClass = commands[commandName]

    if commandClass.getRequiresIce():
        if not IceHelper.IceAvailable:
            print("Could not import Ice python module!")
            return 1

        iceHelper = iceHelperConfig.createIceHelper()
        if not iceHelper.isIceGridRunning(configuration):
            print "IceGrid is not running"
            print "Try starting it with 'armarx start'"
            return 1



    if commandClass.getRequiresIce():
        print "Profile: " + profile.profilename

    command = commandClass(profile)
    returnValue = 1
    try:
        result = command.execute(commandArguments)
        if result is not None:
            if isinstance(result, bool):
                if result:
                    returnValue = 0
                else:
                    returnValue = 1
            elif isinstance(result, (int, float)):
                returnValue = result

        else:
            returnValue = 0
    except Exception, e:
        print ArmarXBuilder.bcolors.BOLD, ArmarXBuilder.bcolors.FAIL, "Error: ", str(e)


    ini_parser = ConfigParser.ConfigParser()
    if os.path.exists(configuration.armarx_ini):
        with open(configuration.armarx_ini, "r") as ini_file:
            ini_parser.readfp(ini_file)
    # the profilename can get changed by the "switch" command
    ini_parser.set("DEFAULT", "profile", configuration.profilename)

    with open(configuration.armarx_ini, "wb") as ini_file:
        ini_parser.write(ini_file)

    try:
        if iceHelper:
            iceHelper.iceShutdown()
    except NameError:
        i = 0
    return returnValue

def main_user():
    return cli("armarx", [Command.CommandType.User])

def main_developer():
    return cli("armarx-dev", [Command.CommandType.Developer])
