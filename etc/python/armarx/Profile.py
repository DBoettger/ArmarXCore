##
# This file is part of ArmarX.
#
# ArmarX is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2 as
# published by the Free Software Foundation.
#
# ArmarX is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# @package    ArmarXCore::scripts
# @author     Manfred Kroehnert (Manfred dot Kroehnert at kit dot edu)
# @date       2015
# @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
#             GNU General Public License

__author__ = 'kroehner'

import armarx.Util as Util

import os
import shutil
import random
import tempfile

class Profile:

    profiles_directory = ""
    armarxUserConfigurationDirectory = ""
    templateDirectory = ""
    configuration_files = []


    def __init__(self, profilename):
        self.profilename = profilename


    def exists(self):
        profile_directory = self.getProfileDirectory()
        # check if directory exists
        if not os.path.exists(profile_directory):
            return False
        # check if all configuration files exist
        for configuration_file in self.configuration_files:
            if not os.path.exists(os.path.join(profile_directory, configuration_file)):
                return False
        return True


    def create(self):
        Util.mkdir_p(self.getProfileDirectory())
        profile_directory = self.getProfileDirectory()
        # create symlinks for all necessary configuration files
        for configuration_file in self.configuration_files:
            symlinkDestination = os.path.join(self.armarxUserConfigurationDirectory, configuration_file)
            symlinkTarget = os.path.join(profile_directory, configuration_file)
            if os.path.isfile(symlinkDestination) and not os.path.islink(symlinkDestination):
                shutil.move(symlinkDestination, symlinkTarget)
            elif not os.path.exists(symlinkTarget):
                print "copying " + configuration_file + " template to: " + symlinkTarget
                templatefile = os.path.join(self.templateDirectory, configuration_file)
                if not os.path.exists(templatefile):
                    print "Template file does not exist: " + templatefile
                    print "Aborting"
                    return False
                shutil.copyfile(templatefile, symlinkTarget)
                if configuration_file == "default.cfg" :
                    print "    Randomizing Ports in: " + symlinkTarget
                    new_port = random.randint(7000, 27000)
                    print "    New port is " + str(new_port) + " / " + str(new_port + 1)
                    fh, abs_path = tempfile.mkstemp()
                    #replace
                    with os.fdopen(fh,'w') as new_file:
                         with open(symlinkTarget) as old_file:
                            for line in old_file:
                                new_file.write(line.replace("tcp -p 4061", "tcp -p "+ str(new_port)).replace("ArmarX.MongoPort=27017", "ArmarX.MongoPort=" + str(new_port + 1)))
                    #Remove original file
                    os.remove(symlinkTarget)
                    #Move new file
                    shutil.move(abs_path, symlinkTarget)
        return True

    def update(self):
        Util.mkdir_p(self.getProfileDirectory())
        profile_directory = self.getProfileDirectory()
        # create symlinks for all necessary configuration files
        for configuration_file in self.configuration_files:
            if ".generated." in configuration_file:
                symlinkDestination = os.path.join(self.armarxUserConfigurationDirectory, configuration_file)
                symlinkTarget = os.path.join(profile_directory, configuration_file)
                templatefile = os.path.join(self.templateDirectory, configuration_file)
                if not os.path.exists(templatefile):
                    print "Template file does not exist: " + templatefile
                    print "Aborting"
                    return False
                shutil.copyfile(templatefile, symlinkTarget)
        return True
    def remove(self):
        pass


    def link(self):
        if not self.exists():
            print "Profile does not exist: " + self.profilename
            return

        profile_directory = self.getProfileDirectory()
        # create symlinks for all necessary configuration files
        for configuration_file in self.configuration_files:
            symlinkDestination = os.path.join(self.armarxUserConfigurationDirectory, configuration_file)
            symlinkTarget = os.path.join(profile_directory, configuration_file)
            # delete old symlink
            if os.path.lexists(symlinkDestination):
                os.remove(symlinkDestination)
            # create new symlink
            if os.path.exists(symlinkTarget):
                os.symlink(symlinkTarget, symlinkDestination)
            else:
                print "Can not create symlink to: " + symlinkTarget
                print "File does not exist"


    def getProfileDirectory(self):
        return os.path.join(self.profiles_directory, self.profilename)


    @classmethod
    def setDirectories(cls, profiles_directory, armarxUserConfigurationDirectory, templateDirectory):
        cls.profiles_directory = profiles_directory
        cls.armarxUserConfigurationDirectory = armarxUserConfigurationDirectory
        cls.templateDirectory = templateDirectory


    @classmethod
    def setConfigurationFiles(cls, configuration_files):
        cls.configuration_files = configuration_files


    @classmethod
    def getAvailableProfiles(cls):
        profiles = []
        for profilename in os.listdir(cls.profiles_directory):
            profiles.append(profilename)
        return profiles
