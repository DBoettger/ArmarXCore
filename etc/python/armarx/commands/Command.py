##
# This file is part of ArmarX.
#
# ArmarX is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2 as
# published by the Free Software Foundation.
#
# ArmarX is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# @package    ArmarXCore::scripts
# @author     Manfred Kroehnert (Manfred dot Kroehnert at kit dot edu)
# @date       2015
# @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
#             GNU General Public License

__author__ = 'kroehner'

import argparse

"""
Define commands executable by the armarx.py script.

All commands must inherit from Command,
set the class variables commandName and requiredArgumentCount
and define the methods execute(args) and getHelpString().

If the command does not require Ice the class variable requiresIce
must be set to False.

Each command is identified via the class variable commandName
"""

class CommandType:
    User = 0,
    Developer = 1

def getSubclasses(commandTypes = None):
    commands = Command.__subclasses__()
    if commandTypes is None:
        return commands

    result = []
    for cmd in commands:
        if cmd.getCommandType() in commandTypes:
            result.append(cmd)
    return result



class Command(object):

    commandName = None
    commandType = CommandType.User

    subArgParse = None

    requiresIce = True

    iceHelperConfig = None
    iceHelperInst = None
    configuration = None
    def __init__(self, profile):

        self.profile = profile


    @classmethod
    def setIceHelperConfig(cls, iceHelperConfig):
        cls.iceHelperConfig = iceHelperConfig

    @classmethod
    def setConfiguration(cls, configuration):
        cls.configuration = configuration

    @classmethod
    def iceHelper(cls):
        if not cls.iceHelperInst:
            cls.iceHelperInst = cls.iceHelperConfig.createIceHelper()
        return cls.iceHelperInst


    @classmethod
    def getCommandName(cls):
        if not cls.commandName:
            raise RuntimeError(str(cls) + " must set the class property <commandName>")
        return cls.commandName

    @classmethod
    def addtoSubArgParser(cls, subParser):
        return


    @classmethod
    def getRequiresIce(cls):
        return cls.requiresIce

    @classmethod
    def getCommandType(cls):
        return cls.commandType

    @classmethod
    def getHelpString(cls):
        if issubclass(cls, Command):
            #import inspect; print(inspect.currentframe().f_code.co_name)
            raise RuntimeError("getHelpString() must be implemented by " + cls.__name__)


    def execute(self, args):
        if isinstance(self, Command):
            raise RuntimeError("execute() must be implemented by " + type(self).__name__)

    @classmethod
    def getCompletionPackages(cls, prefix, **kwargs):
        from armarx import ArmarXBuilder

        return cls.configuration.getUsedPackageNames() + ArmarXBuilder.getArmarXDefaultPackages(prefix, **kwargs)

