##
# This file is part of ArmarX.
#
# ArmarX is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2 as
# published by the Free Software Foundation.
#
# ArmarX is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# @package    ArmarXCore::scripts
# @author     Manfred Kroehnert (Manfred dot Kroehnert at kit dot edu)
# @date       2015
# @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
#             GNU General Public License

__author__ = 'kroehner'

from Command import Command
from DeployApplication import DeployApplication

import armarx.Util as Util

import os
import argparse
import sys
import subprocess


class IceStart(Command):

    commandName = "start"

    requiredArgumentCount = 0

    requiresIce = False

    parser = argparse.ArgumentParser(description='Starts IceGrid nodes')

    def __init__(self, profile):
        super(IceStart, self).__init__(profile)


    @classmethod
    def addtoSubArgParser(cls, subParser):
        subParser.add_argument('icegridnodes', nargs='*', metavar="icegridnode_name", default=["NodeMain"], help='Name of IceGrid nodes to start (registry node must be named NodeMain)')


    def execute(self, args):
        """
        check if icegridnodes are running and start them if required
        and deploy IceStorm after all nodes are running
        :param args:
        :return:
        """

        self.addtoSubArgParser(self.parser)
        args = self.parser.parse_args(args)

        for nodeName in args.icegridnodes:
            # use the isIceGridNodeRunning() method defined here
            # it uses the "icegridadmin" console command to check the connection
            # otherwise it would be necessary to reinitialize self.iceHelper() after the registry node has been started
            if self.isIceGridNodeRunning(nodeName):
                print "icegridnode already running: " + nodeName
                continue

            print "starting icegridnode: " + nodeName + "\n"
            self.startIceGridNode(nodeName)
            if not self.isIceGridNodeRunning(nodeName):
                print "\nIce could not be started"
                print "\nDo you need to switch profiles? (armarx switch <profilename>)"
                print "\nYou can try to kill your local Ice server with 'armarx killIce' and/or to reset it with 'armarx reset'."
                return 1

        iceStormDeploymentXml = os.path.join(self.configuration.armarxCoreConfigurationDirectory, "IceStorm.icegrid.xml")
        if os.path.exists(iceStormDeploymentXml):
            # only deploy on Registry node and only if IceStorm is not already running
            if nodeName == self.configuration.iceGridRegistryNodeName and not "IceStorm" in self.iceHelper().getApplicationList():
                print "Deploying IceStorm"
                DeployApplication(self.profile).execute([iceStormDeploymentXml])
        else:
            print "IceStorm deployment file not found: " + iceStormDeploymentXml
            return 1

        self.cleanUpDeadObjects()
        return 0

    @classmethod
    def getHelpString(cls):
        return "start IceGrid and do required setup of default configuration files if required"


    def isIceGridNodeRunning(self, nodeName):
        """
        try to contact the icegridnode by name and return True if it is already up and running
        :param nodeName: name of the icegridnode to contact
        :return: True if node is running, false otherwise
        """
        command = self.getIceGridAdminCommand("node ping " + nodeName)
        process = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
        process.wait()
        return 0 == process.returncode


    def getIceGridAdminCommand(self, commandString):
        """
        create the string for executing icegridadmin with the script given as parameter
        :param commandString: command to execute with icegridadmin
        :return:
        """
        iceGridAdminCommand = "icegridadmin --Ice.Config=\"" + self.configuration.iceGridDefaultConfigurationFile
        iceGridAdminCommand += "\" -u " + self.configuration.iceGridUsername
        iceGridAdminCommand += " -p " + self.configuration.iceGridPassword
        iceGridAdminCommand += " -e \"" + commandString + "\""
        return iceGridAdminCommand


    def startIceGridNode(self, nodeName):
        """
        create data directories required for running the icegridnode
        check if configuration files exist
        create the shell command for running the icegridnode and execute it in a subshell
        :param nodeName:
        :return:
        """
        # TODO: abort if icegridnode is to be started on a remote host
        iceGridNodeDirectory = os.path.join(self.configuration.iceGridNodeDirectory, nodeName)
        self.createIceGridNodeDataDirectories(iceGridNodeDirectory)

        if not os.path.exists(self.configuration.armarxCoreConfigurationDirectory):
            print "ArmarXCore is not located at: " + self.configuration.armarxCoreConfigurationDirectory
            return
        for file in self.configuration.defaultsFiles:
            if not os.path.exists(file):
                print "ArmarX default config does no exist at: " + file
                return

        gridNodeCommand = self.createIceGridNodeStartCommand(nodeName)

        process = subprocess.Popen(gridNodeCommand, cwd=iceGridNodeDirectory, stdout=sys.stdout, stderr=sys.stderr, shell=True)
        process.communicate()


    def createIceGridNodeStartCommand(self, nodeName):
        """
        create string containing the command to start an icegridnode
        appends the IceGridRegistry configuration file if the nodeName is equal to the name of the registry node
        :param nodeName:
        :return:
        """

        # create a copy
        configFiles = list(self.configuration.defaultsFiles)

        # append the registry config file if the node is started as registry
        if nodeName == self.configuration.iceGridRegistryNodeName:
            configFiles.append(os.path.join(self.configuration.armarxCoreConfigurationDirectory, "icegrid_registry.cfg"))

        # append the general IceGrid node configuration
        configFiles.append(os.path.join(self.configuration.armarxCoreConfigurationDirectory, "armarx-icegridnode.cfg"))

        configs = ','.join(configFiles)


        gridNodeCommand = "icegridnode --IceGrid.Registry.DefaultTemplates=" + os.path.join(self.configuration.armarxCoreConfigurationDirectory, "icegrid_default_templates.xml")
        gridNodeCommand += " --Ice.Config=\"" + configs + "\""
        gridNodeCommand += " --daemon --nochdir --Ice.ProgramName=" + nodeName + " --IceGrid.Node.Name=" + nodeName
        
        return gridNodeCommand

    def cleanUpDeadObjects(self):

        admin = self.iceHelper().getAdminSession().getAdmin()
        objectInfos = admin.getAllObjectInfos("*")
        if len(objectInfos) > 0:
            asyncResults = []
            for info in objectInfos:
                asyncResult = None
                timeoutProxy = None
                try:
                    timeoutProxy = info.proxy.ice_timeout(300)
                    asyncResult = timeoutProxy.begin_ice_ping()
                except:
                    asyncResult = None
                    pass
                if asyncResult is not None:
                    asyncResults.append((info, timeoutProxy, asyncResult))

            for info, timeoutProxy, asyncResult in asyncResults:
                try:
                    try:
                        timeoutProxy.end_ice_ping(asyncResult)
                    except:
                        print "Removing dead object " + info.proxy.ice_getIdentity().name
                        admin.removeObject(info.proxy.ice_getIdentity())
                except:
                    pass

    def createIceGridNodeDataDirectories(self, iceGridNodeDirectory):
        """
        create the directories required to run an icegridnode
        :param iceGridNodeDirectory:
        :return:
        """
        for subDirectory in ["log", "data", "registry"]:
            Util.mkdir_p(os.path.join(iceGridNodeDirectory, subDirectory), 0o755)
