##
# This file is part of ArmarX.
#
# ArmarX is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2 as
# published by the Free Software Foundation.
#
# ArmarX is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# @package    ArmarXCore::scripts
# @author     Manfred Kroehnert (Manfred dot Kroehnert at kit dot edu)
# @date       2015
# @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
#             GNU General Public License
from docutils.nodes import topic

__author__ = 'kroehner'

from Command import Command
import argparse

class List(Command):

    commandName = "list"
    requiresIce = True

    requiredArgumentCount = 1

    parser = argparse.ArgumentParser(description="List Ice applications/objects/topics")

    def __init__(self, profile):
        super(List, self).__init__(profile)

    @classmethod
    def addtoSubArgParser(cls, subParser):
        subParser.add_argument('type', choices = ['applications', 'objects', 'topics'], help='What type of data to list')

    def execute(self, args):
        self.addtoSubArgParser(self.parser)
        args = self.parser.parse_args(args)
        if args.type == 'applications':
            print "List of deployed/running ArmarX scenarios: "
            for application in self.iceHelper().getApplicationList():
                print "\t " + application
        elif args.type == 'objects':
            print "\nList of Ice objects:\n"

            adminSession = self.iceHelper().getAdminSession()
            admin = adminSession.getAdmin()
            objects = admin.getAllObjectInfos("*")
            for obj in objects:
                proxy = obj.proxy.ice_timeout(1)
                name = proxy.ice_getIdentity().name
                print (name)
                try:
                    for id in proxy.ice_ids():
                        if id != "::Ice::Object":
                            print("\t" + id)
                except:
                    print ("\tConnection failed - most derived type: " + obj.type)
        elif args.type == 'topics':
            print "\nList of Ice topics:\n"

            topicManager = self.iceHelper().getTopicManager()
            topics = topicManager.retrieveAll()
            # print(topics)
            for topic in topics:
                print(topic)
                print("  Subscribers:")
                for sub in topics[topic].getSubscribers():
                    print("\t" + sub.name)
                print("\n")
                # print (proxy)

        else:
            raise ValueError(args.type)


    @classmethod
    def getHelpString(cls):
        return "List Ice applications/objects/topics"