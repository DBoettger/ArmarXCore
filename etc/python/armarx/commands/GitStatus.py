##
# This file is part of ArmarX.
#
# ArmarX is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2 as
# published by the Free Software Foundation.
#
# ArmarX is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# @package    ArmarXCore::scripts
# @author     Simon Ottenhaus (simon dot ottenhaus at kit dot edu)
# @date       2017
# @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
#             GNU General Public License
import os
import subprocess

__author__ = 'ottenhaus'


from Command import Command, CommandType
import argparse
from armarx import ArmarXBuilder

class ExecCommand(Command):
    helpString = "Execute git status in the project base dir of a package and its dependencies." \
               "\nExample: armarx-dev gitstatus RobotAPI"
    commandName = "gitstatus"
    commandType = CommandType.Developer
    requiresIce = False

    requiredArgumentCount = 0
    parser = argparse.ArgumentParser(description=helpString)
    def __init__(self, profile):
        super(ExecCommand, self).__init__(profile)

    @classmethod
    def addtoSubArgParser(cls, subParser):
        subParser.add_argument('--no-deps', help='Execute command only for the given package', action="store_true", dest="nodeps")
        subParser.add_argument('package').completer = cls.getCompletionPackages
        #subParser.add_argument('cmd', nargs=argparse.REMAINDER)


    def execute(self, args):
        self.addtoSubArgParser(self.parser)
        args = self.parser.parse_args(args)
        generator = ArmarXBuilder.ArmarXMakeGenerator(0)
        #if len(args.cmd) == 0:
        #    print "You need to specify a command!"
        #    return
        builder = ArmarXBuilder.ArmarXBuilder(None, False, False, False, False, False)
        deps = builder.getOrderedDependencyList(args.package)
        if args.nodeps:
            package = builder.getPackageForName(deps[-1][0])
            self.execCmd(package, "git status -b -s")
        else:
            for dep in deps:
                package = builder.getPackageForName(dep[0])
                self.execCmd(package, "git status -b -s")

    def execCmd(self, package, cmd):
            sourceDir = package.sourceDir
            if len(sourceDir) == 0:
                print "No source dir available for ", package.packageName, " - skipping"
                return

            cmd = 'cd ' + sourceDir + ' && ' + cmd

            print ArmarXBuilder.bcolors.BOLD +  "git status for package " + package.packageName +":" +  ArmarXBuilder.bcolors.ENDC
            os.system(cmd)



    @classmethod
    def getHelpString(cls):
        return cls.helpString
