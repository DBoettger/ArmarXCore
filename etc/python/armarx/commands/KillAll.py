##
# This file is part of ArmarX.
#
# ArmarX is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2 as
# published by the Free Software Foundation.
#
# ArmarX is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# @package    ArmarXCore::scripts
# @author     Mirko Waechter ( mirko dot waechter at kit dot edu )
# @date       2017
# @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
#             GNU General Public License
from Command import Command
import argparse

__author__ = 'waechter'


class KillAll(Command):

    commandName = "killAll"
    requiresIce = False

    requiredArgumentCount = 0

    parser = argparse.ArgumentParser(description='-')


    def __init__(self, profile):
        super(KillAll, self).__init__( profile)

    @classmethod
    def addtoSubArgParser(cls, subParser):
        subParser.add_argument('--gui', '-g', help='Kill also all ArmarXGui Instances', action="store_true", dest="gui")


    def execute(self, args):
        self.addtoSubArgParser(self.parser)
        args = self.parser.parse_args(args)

        self.killAll(args.gui)

    @classmethod
    def killAll(cls, killGui):
        try:
            import psutil
        except:
            print cls.commandName + " command not available since 'psutil' module is missing"
            return
        if 'get_pid_list' in dir(psutil):
            pids = psutil.get_pid_list()
        else:
            pids = psutil.pids()
        exceptionList = []
        if not killGui:
            exceptionList.append("ArmarXGuiRun")
        killed = False
        for pid in pids:
                p = psutil.Process(pid)
                if type(p.name) is str:
                    name = p.name
                else:
                    name = p.name()
                if name.endswith("Run") and name not in exceptionList:
                    print "Killing " + name + " (pid: " + str(pid) + ")"
                    p.kill()
                    killed = True

        if not killed:
            print "No applications to kill"

    @classmethod
    def getHelpString(cls):
        return "Kills all local ArmarX processes except the ArmarX GUI (to be more precise every " \
               "process that ends on 'Run')"
