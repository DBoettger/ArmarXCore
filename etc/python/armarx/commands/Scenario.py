##
# This file is part of ArmarX.
#
# ArmarX is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2 as
# published by the Free Software Foundation.
#
# ArmarX is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# @package    ArmarXCore::scripts
# @author     Cedric Seehausen (usdnr at student dot kit dot edu)
# @date       2015
# @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
#             GNU General Public License


__author__ = 'seehausen'

import argparse
import os.path, os
from subprocess import call

from armarx import ArmarXBuilder

from Command import Command


class Scenario(Command):

    mainHelpString = 'Start/Stop/Kill/Restart/List ArmarX Scenarios.\nExample usage: armarx scenario start Armar3Simulation'
    commandName = "scenario"

    builder = ArmarXBuilder.ArmarXBuilder(None, False, False, False, False)
    requiredArgumentCount = 1

    requiresIce = False

    parser = argparse.ArgumentParser(description=mainHelpString)

    def __init__(self, profile):
        super(Scenario, self).__init__(profile)

    @classmethod
    def addtoSubArgParser(cls, subParser):
        subParser.add_argument('scenario_command', choices = ['help', 'start', 'stop', 'kill', 'restart', 'list', 'status', 'periodic_status'], help=cls.mainHelpString)
        subParser.add_argument('scenario', nargs='?', help="Path to a scenario file (*.scx or just the directory), "
                                                           "or a Scenario Name to search for").completer = cls.findAllScenarios

        subParser.add_argument('--package', '-p', dest='scenario_package', metavar='ArmarXPackage',
                               help="ArmarXPackage to search Scenario in").completer = cls.getCompletionPackages
        subParser.add_argument('--application', '-a', dest='a', help="Application out of the chosen scenario")
        subParser.add_argument('--parameters', dest='scenario_parameters', help="Optional parameters to start the Scenario/Application with")
        subParser.add_argument('--print', action='store_true', dest='printOnly', help="Only print start commands instead of executing them")
        subParser.add_argument('--wait', '-w', action='store_true', dest='wait', help="Wait for the applicaitons to exit before exiting")

    def execute(self, args):
        self.addtoSubArgParser(self.parser)
        args = self.parser.parse_args(args)

        self.builder = ArmarXBuilder.ArmarXBuilder(None, False, False, False, False)
        packageData = self.builder.getArmarXPackageData('ArmarXCore')

        if not len(packageData):
            print 'Could not find the ArmarXCore package!'
            return

        core_binary_dir = self.builder.getArmarXPackageDataValue(packageData, 'BINARY_DIR')

        if core_binary_dir is None:
            print 'Could not read binary directory from the package cmake data!'
            return

        scenariocli_executable = os.path.join(core_binary_dir[0], 'ScenarioCliRun')

        cli_command = scenariocli_executable
        if args.scenario_command == 'help':
            cli_command += ' -h'
        elif args.scenario_command == 'list':
            cli_command += ' list'
        else:
            if args.scenario is None:
                print 'Expecting a Scenario name/path after the command: ' + args.scenario_command
                print 'Call \"armarx scenario help\" for more information'
                return
            cli_command += ' ' + args.scenario_command + ' ' + args.scenario
            if args.scenario_package:
                cli_command += ' ' + args.scenario_package
            if args.a:
                cli_command += ' -a ' + args.a
            if args.scenario_parameters:
                cli_command += ' --parameters=' + args.scenario_parameters
            if args.printOnly == True:
                cli_command += ' --print '
            if args.wait == True:
                cli_command += ' --wait '

        os.system(cli_command)

    @classmethod
    def findScenarios(cls, packageName):
        import fnmatch
        import os

        packageData = cls.builder.getArmarXPackageData(packageName)
        packageData = cls.builder.getArmarXPackageDataValue(packageData, 'SCENARIOS_DIR')
        matches = []
        print packageData
        if len(packageData) == 0:
            return matches
        for root, dirnames, filenames in os.walk(packageData[0]):
            for filename in fnmatch.filter(filenames, '*.scx'):
                matches.append(os.path.splitext(filename)[0])
        return matches

    @classmethod
    def findAllScenarios(cls, prefix, **kwargs):
        matches = []
        # import sys
        # args = cls.parser.parse_args(sys.argv)
        # if args.scenario_package:
        #      packages = [args.scenario_package]
        # else:
        packages = cls.getCompletionPackages("")
        for p in packages:
            matches = matches + cls.findScenarios(p)
        return matches

    @classmethod
    def getHelpString(cls):
        return cls.mainHelpString
