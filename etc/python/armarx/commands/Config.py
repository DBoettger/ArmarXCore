##
# This file is part of ArmarX.
#
# ArmarX is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2 as
# published by the Free Software Foundation.
#
# ArmarX is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# @package    ArmarXCore::scripts
# @author     Markus Grotz (markus dot grotz at kit dot edu)
# @date       2015
# @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
#             GNU General Public License

import logging
import argparse
import os


from Command import Command

import ConfigParser

logger = logging.getLogger(__name__)

class Config(Command):

    commandName = 'config'

    requiredArgumentCount = 1

    requiresIce = False
    description='''ArmarX Config. Allows to
                                     configure some basic ArmarX convenience settings'''
    parser = argparse.ArgumentParser(description=description)

    def __init__(self, profile):
        super(Config, self).__init__(profile)

        self.config = ConfigParser.ConfigParser()

    @classmethod
    def addtoSubArgParser(cls, sub_parser):
        group = sub_parser.add_mutually_exclusive_group()
        group.add_argument('-e', '--enable-autostart', action='store_true', help='enable autostart')
        group.add_argument('-d', '--disable-autostart', action='store_true', help='disable autostart')


    def create_upstart(self):
        file_name = os.path.expanduser('~/.config/upstart/ice_start.conf')
        logger.debug('creating upstart file {}'.format(file_name))
        with open(file_name, 'w') as f:
            f.write('description "ArmarX start"\n')
            f.write('start on desktop-start\n')
            f.write('script\n')
            f.write('armarx start\n')
            f.write('armarx memory start\n')
            f.write('end script\n')

        file_name = os.path.expanduser('~/.config/upstart/ice_stop.conf')
        logger.debug('creating upstart file {}'.format(file_name))
        with open(file_name, 'w') as f:
            f.write('description "ArmarX stop"\n')
            f.write('start on desktop-end\n')
            f.write('script\n')
            f.write('armarx stop\n')
            f.write('armarx memory stop\n')
            f.write('end script\n')

    def remove_upstart(self):
        config_files = [os.path.expanduser('~/.config/upstart/ice_start.conf'),
                        os.path.expanduser('~/.config/upstart/ice_stop.conf')]

        for f in config_files:
            logger.debug('removing upstart file {}'.format(f))
            os.remove(f)

    def execute(self, args):
        self.addtoSubArgParser(self.parser)
        args = self.parser.parse_args(args)

        if args.enable_autostart:
            self.create_upstart()
        elif args.disable_autostart:
            self.remove_upstart()

    @classmethod
    def getHelpString(cls):
        return cls.description
