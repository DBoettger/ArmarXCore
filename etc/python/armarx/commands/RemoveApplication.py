##
# This file is part of ArmarX.
#
# ArmarX is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2 as
# published by the Free Software Foundation.
#
# ArmarX is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# @package    ArmarXCore::scripts
# @author     Manfred Kroehnert (Manfred dot Kroehnert at kit dot edu)
# @date       2015
# @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
#             GNU General Public License

__author__ = 'kroehner'

import argparse
from Command import Command


class RemoveApplication(Command):

    commandName = "remove"

    requiredArgumentCount = 1

    parser = argparse.ArgumentParser(description='Removes an ArmarX scenario from IceGrid')


    def __init__(self, profile):
        super(RemoveApplication, self).__init__(profile)

    @classmethod
    def addtoSubArgParser(cls, subParser):
        subParser.add_argument('application', nargs='+').completer = RemoveApplication.getIceApplications


    def execute(self, args):
        self.addtoSubArgParser(self.parser)
        args = self.parser.parse_args(args)

        self.iceHelper().removeApplications(args.application)

    @classmethod
    def getHelpString(cls):
        return "stop and remove all ArmarX scenarios from IceGrid whose name was given as a command parameter"

    @classmethod
    def getIceApplications(cls, prefix, **kwargs):
        return cls.iceHelper().getApplicationList()
