import logging
import os
import Ice
from IceGrid import ObjectExistsException
from IceStorm import TopicManagerPrx, NoSuchTopic, AlreadySubscribed

from armarx.Configuration import Configuration as armarx_cfg
from armarx import IceHelper
from armarx.ArmarXBuilder import ArmarXBuilder
from armarx.Profile import Profile
import fnmatch
import os


logger = logging.getLogger(__name__)


def load_armarx_slice(project, filename):
    """
    simple helper function to load a slice definition file.  The imported slice
    is then available through the python import function.

    :param project: name of the amrarx package
    :type project: str
    :param filename: relative path to the slice interface
    :type filename: str
    """
    armarx_builder = ArmarXBuilder(None, False, False, False, False)
    dependencies = armarx_builder.getOrderedDependencyList(project)
    include_path = ['-I{}'.format(Ice.getSliceDir())]

    package_data = armarx_builder.getArmarXPackageData(project)
    interface_dir = armarx_builder.getArmarXPackageDataValue(package_data, 'INTERFACE_DIRS')

    filename = os.path.join(interface_dir[0], project, 'interface', filename)

    for (package_name, _) in dependencies:
        package_data = armarx_builder.getArmarXPackageData(package_name)
        interface_dir = armarx_builder.getArmarXPackageDataValue(package_data, 'INTERFACE_DIRS')
        include_path.append(interface_dir[0])

    search_path = ' -I'.join(include_path)
    logger.debug('Looking for slice files in {}'.format(search_path))
    if not os.path.exists(filename):
        raise IOError("Path not found: " + filename)
    Ice.loadSlice('{} --underscore --all {}'.format(search_path, filename))

def load_all_slice_files(project):
    armarx_builder = ArmarXBuilder(None, False, False, False, False)
    dependencies = armarx_builder.getOrderedDependencyList(project)
    include_path = set()
    # include_path.add(Ice.getSliceDir())
    package_data = armarx_builder.getArmarXPackageData(project)
    interface_dir = armarx_builder.getArmarXPackageDataValue(package_data, 'INTERFACE_DIRS')
    slicepath = os.path.join(interface_dir[0], project, 'interface')

    for (package_name, _) in dependencies:
        package_data = armarx_builder.getArmarXPackageData(package_name)
        interface_dir = armarx_builder.getArmarXPackageDataValue(package_data, 'INTERFACE_DIRS')
        include_path.add(interface_dir[0])
    search_path = ' -I'.join(include_path)
    matches = []
    slice_files = []
    for include_path in include_path:
        print("Loading slice files from {}".format(include_path))
        for root, dirnames, filenames in os.walk(include_path):
            for filename in fnmatch.filter(filenames, '*.ice'):
                # print('loading slice file {}'.format(filename))
                slice_files.append(os.path.join(root, filename))
        if len(slice_files) >  0:
            Ice.loadSlice('-I {} -I {} --underscore --all'.format(Ice.getSliceDir(), search_path), slice_files)
    print "loaded all slice files"



def register_object(ice_object, ice_object_name):
    """
    Register an local ice object with ice under the given name
    :param ice_object: Local ice object instance
    :param ice_object_name: Name with which the object should be registered
    :return: Proxy to this object
    """
    adapter = ice_helper.iceCommunicator.createObjectAdapterWithEndpoints(ice_object_name, 'tcp')
    ice_object_id = ice_helper.iceCommunicator.stringToIdentity(ice_object_name)
    adapter.add(ice_object, ice_object_id)
    adapter.activate()
    proxy = adapter.createProxy(ice_object_id)
    admin = ice_helper.getAdminSession().getAdmin()
    try:
        logger.info('adding new object {}'.format(ice_object_name))
        admin.addObjectWithType(proxy, proxy.ice_id())
    except ObjectExistsException:
        logger.info('updating new object {}'.format(ice_object_name))
        admin.updateObject(proxy)
    return proxy


def get_topic(cls, topic_name):
    """
    Retrieve a topic proxy casted to the first parameter
    :param cls: Type of the topic
    :param topic_name: Name of the topic
    :return: a casted topic proxy
    """
    topic_manager = TopicManagerPrx.checkedCast(ice_helper.iceCommunicator.stringToProxy('IceStorm/TopicManager'))
    topic = None
    try:
        topic = topic_manager.retrieve(topic_name)
    except NoSuchTopic:
        topic = topic_manager.create(topic_name)
    logger.info("Publishing to topic " + topic_name)
    pub = topic.getPublisher().ice_oneway()
    return cls.uncheckedCast(pub)


def using_topic(proxy, topic_name):
    """
    .. seealso:: :func:`register_object`

    :param proxy: the instance where the topic event should be called
    :param topic_name: the name of the topic to connect to
    :type topic_name: str
    """
    topic_manager = TopicManagerPrx.checkedCast(ice_helper.iceCommunicator.stringToProxy('IceStorm/TopicManager'))
    topic = None
    try:
        topic = topic_manager.retrieve(topic_name)
    except NoSuchTopic:
        topic = topic_manager.create(topic_name)
    try:
        topic.subscribeAndGetPublisher(None, proxy)
    except AlreadySubscribed:
        topic.unsubscribe(proxy)
        topic.subscribeAndGetPublisher(None, proxy)
    logger.info("Subscribing to topic " + topic_name)
    return topic


def get_proxy(cls, proxy_name):
    """
    Connects to a proxy.

    :param cls: the class definition of an ArmarXComponent
    :param proxy_name: name of the proxy
    :type proxy_name: str
    :returns: the retrieved proxy
    :rtype: an instance of cls
    """
    proxy = ice_helper.iceCommunicator.stringToProxy(proxy_name)
    return cls.checkedCast(proxy)


def _get_ice_helper():
    Profile.setDirectories(armarx_cfg.armarx_profiles_directory, armarx_cfg.armarxUserConfigurationDirectory, armarx_cfg.armarxCoreConfigurationDirectory)
    Profile.setConfigurationFiles([armarx_cfg.iceGridDefaultConfigurationFilename, armarx_cfg.iceGridGeneratedDefaultConfigurationFilename, armarx_cfg.iceGridSyncVariablesFile])

    ice_config_files = [armarx_cfg.iceGridDefaultConfigurationFile,
                        os.path.join(os.path.dirname(armarx_cfg.iceGridDefaultConfigurationFile),
                        armarx_cfg.iceGridGeneratedDefaultConfigurationFilename)]

    ice_helper_config = IceHelper.IceHelperConfig(','.join(ice_config_files), armarx_cfg.iceGridRegistryName, armarx_cfg.iceGridUsername, armarx_cfg.iceGridPassword)

    ice_helper = ice_helper_config.createIceHelper()
    if not ice_helper.isIceGridRunning(armarx_cfg):
        logger.error('Ice is not running')
        raise Exception('Ice is not running')

    return ice_helper

ice_helper = _get_ice_helper()


ice_communicator = ice_helper.iceCommunicator
