#!/usr/bin/env python3
##
# This file is part of ArmarX.
#
# ArmarX is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2 as
# published by the Free Software Foundation.
#
# ArmarX is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# @package    ArmarXCore::scripts
# @author     Mirko Waechter (waechter at kit dot edu)
# @date       2016
# @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
#             GNU General Public License


import os, sys

if len(sys.argv) < 5:
    print("Required parameters: buildDir projectName projectBaseDir searchPath [searchPath2 ...] ")
    exit(1)

import re
regex = {}
regex[""] = r"(@|\\)page ([A-Z0-9a-z\-]+) "
regex["group__"] = r"(@|\\)defgroup ([A-Z0-9a-z\-]+) "
regex["classarmarx_1_1"] = r"(@|\\)class ([A-Z0-9a-z\-]+)"

pageSourceMap = {}
buildDoxygenPath = sys.argv[1] + "/doxygen/html"
project = sys.argv[2]
projectBaseDir = sys.argv[3]
print(sys.argv)
searchPaths = sys.argv[4:]#["etc/doxygen","source"]
gitlabBaseLink = "https://gitlab.com/ArmarX/" + project +"/blob/master/"
doxygenSearchText = "<li class=\"footer\">"


for path in searchPaths:
    for root, dirnames, filenames in os.walk(path):
        #print(root,dirnames, filenames)
        for filename in filenames:
            relFilePath = os.path.join(root, filename)
            try:
                fileContent = open(relFilePath).read()
                lines = fileContent.splitlines()
                lineNumber = 0
                for line in lines:
                    lineNumber+=1
                    for regexKey in regex:
                        matches = re.finditer(regex[regexKey], line)
                        for matchNum, match in enumerate(matches):
                            matchNum = matchNum + 1
                            link = "https://gitlab.com/ArmarX/ArmarXCore/blob/master/" + os.path.relpath(relFilePath,projectBaseDir) +"#L" + str(lineNumber)
                            #print ("Match:" + match.group(2) +" in file " + link)
                            pageSourceMap[regexKey+match.group(2)] = (os.path.relpath(relFilePath,projectBaseDir),lineNumber)

                exit(0)
            except:
                #print("Reading " + relFilePath + " failed")
                pass
import fileinput


changeCount = 0

for page in pageSourceMap:
    filePath = buildDoxygenPath + "/" + page + ".html"
    replaceText = "<li class=\"footer\" > <A href='" + gitlabBaseLink + pageSourceMap[page][0] +"#L"+str(pageSourceMap[page][1])+"' target='_blank'><b>Edit Page</b></A> - "
    if os.path.isfile(filePath):
        #print ("Changing file " +  filePath)
        changeCount+=1
        with fileinput.FileInput(filePath, inplace=True) as file:
            for line in file:
                print(line.replace(doxygenSearchText, replaceText), end='')

print ("Added " + str(changeCount) + " source page links for project " + project)