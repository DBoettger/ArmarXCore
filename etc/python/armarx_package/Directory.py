##
# This file is part of ArmarX.
#
# ArmarX is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2 as
# published by the Free Software Foundation.
#
# ArmarX is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# @package    ArmarXCore::tools
# @author     Jan Issac (jan dot issac at gmail dot com)
# @date       2012
# @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
#             GNU General Public License

from os import path
from os import makedirs
from os import walk
from shutil import rmtree
from DirectoryDefinition import DirectoryDefinition
from Template import Template
from Exceptions import DirectoryExistsException
from Exceptions import MissingDirectoryException
from Exceptions import MissingFileException

import Component
import re


class Directory(DirectoryDefinition):

    ##
    # Constructor
    #
    # @param directoryDefinition
    # @param parentDirectory
    # @param forceMake
    #
    def __init__(self, dirDef, parentDirectory = None, forceMake = False):
        #=======================================================================
        # Directory definition members
        #=======================================================================
        self._parentDefinition = parentDirectory
        self._directoryNode = dirDef.getDirectoryNode()
        self._name = dirDef.getName()
        self._required = dirDef.isRequired()
        self._templateDefinitions = dirDef.getTemplateDefinitions()
        self._subDirectoryDefinitions = dirDef.getSubDirectoryDefinitions()
        self._componentDefinitions = dirDef._componentDefinitions

        #=======================================================================
        # Directory members
        #=======================================================================
        self._forceMake = forceMake
        self._subDirectories = []
        self._templates = []
        self._parentDirectory = parentDirectory

        self.templateReplacementStrategy = dirDef.templateReplacementStrategy

        #=======================================================================
        # create sub-directory instances from definitions
        #=======================================================================
        for subDirectoryDefintion in self.getSubDirectoryDefinitions():
            self._subDirectories.append(Directory(subDirectoryDefintion, self, forceMake))

        #=======================================================================
        # create template instances within this directory from definition
        #=======================================================================
        for templateDefinition in self.getTemplateDefinitions():
            self._templates.append(Template(templateDefinition))


    ##
    # Returns whether to force building the directory
    #
    def forceMake(self):
        return self._forceMake


    ##
    # Returns whether the directory is required to be build or not.
    #
    def isRequired(self):
        return (DirectoryDefinition.isRequired(self) or self.forceMake())


    ##
    # Return the path to this directory
    #
    def getPath(self):
        if self._parentDirectory:
            return path.join(self._parentDirectory.getPath(), self._name)
        else:
            return self._name


    ##
    # Returns the parent directory object
    #
    def getParentDirectory(self):
        return self._parentDirectory


    ##
    # Creates and returns a component instance of a certain type
    #
    # @param typeName    Component type name
    # @param name        Component instance name
    #
    def createComponent(self, typeName, name):
        if self._componentDefinitions.has_key(typeName):
            return Component.Component(self._componentDefinitions[typeName], name, self)
        else:
            for subDirectory in self._subDirectories:
                component = subDirectory.createComponent(typeName, name)
                if component:
                    return component

        return None

    ##
    # Returns the requested component if exists, otherwise None
    #
    def getComponent(self, typeName, name):
        if self._componentDefinitions.has_key(typeName):
            return Component.Component(self._componentDefinitions[typeName], name, self)
        else:
            for subDirectory in self._subDirectories:
                component = subDirectory.createComponent(typeName, name)
                if component:
                    return component


    ##
    # Scans for components within the directory
    #
    def getComponents(self, variables={}, recursive=True):
        # output component list
        components = []

        componentNameRegexSet = self.getComponentSearchPatterns()
        
        try:
            name = None
            top, dirs, files = next(walk(self.getPath()))
            elements = dirs + files
            for element in elements:                
                for regex in componentNameRegexSet:
                    pattern = re.compile(regex)
                    name = pattern.findall(element)

                if name:
                    variables["@COMPONENT_NAME@"] = name[0]
                    for typeName in self._componentDefinitions.keys():
                        component = self.createComponent(typeName, name[0])
                        try:                            
                            component.bind(variables)
                            component.validate()
                            components.append(component)
                        except:
                            pass
        except StopIteration:
            pass

        if recursive:
            for subDirectory in self._subDirectories:
                components = components + subDirectory.getComponents(variables, recursive)

        return components



    ##
    # Instantiates the directory definition by replacing all variables in the
    # definition to specific values
    #
    def bind(self, variables):
        for variable, value in variables.items():
            self._name = self._name.replace(variable, value)

        for subdirectory in self._subDirectories:
            subdirectory.bind(variables)

        for template in self._templates:
            template.set_variables(variables)


    ##
    # Creates the directory and its sub-directories and compiles the skeleton
    # files within the directories
    #
    def make(self, recursive=True, silent=False):
        if self.isRequired():
            try:
                makedirs(self.getPath())
                
                print "%s %s %s ..." % (u'>', 'Creating directory '.ljust(25, '.'), self.getPath())
            except OSError:
                if not silent:
                    raise DirectoryExistsException(self.getPath())

            if recursive:
                for subdirectory in self._subDirectories:
                    subdirectory.make(recursive, silent)

            for template in self._templates:
                template.writeToDirectory(self, self.templateReplacementStrategy)

    def makeAll(self, recursive=True, silent=False):
        if self.getParentDirectory():
            self.getParentDirectory().setRequired(True)
            self.getParentDirectory().make(False, True)

        self.make(recursive, silent)

    ##
    # Checks whether the directory exists
    #
    def exists(self, recursive=False):
        exists = path.exists(self.getPath())
        if exists and recursive:
            # check this directory
            if not exists:
                return False

            # check sub-directory
            for subdirectory in self._subDirectories:
                if not subdirectory.exists(recursive):
                    return False

            return True
        else:
            return exists


    ##
    # Validates this and the sub-directories
    #
    def validate(self, mustExist=False):
        if self.isRequired() or mustExist:
            if not self.exists():
                raise MissingDirectoryException(self.getPath())

            for template in self._templates:
                if not template.exists(self):
                    print "Warning: Template file {0} is missing".format(path.join(self.getPath(), template.getName()))
                else:
                    template.validate(self)

            for subdirectory in self._subDirectories:
                subdirectory.validate()


    ##
    # Remove this directory from the file system
    #
    def remove(self):
        print '%s %s %s ...' % (u'>', 'Removing '.ljust(25, '.'), self.getPath())
        rmtree(self.getPath())
