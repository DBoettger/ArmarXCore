##
# This file is part of ArmarX.
#
# ArmarX is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2 as
# published by the Free Software Foundation.
#
# ArmarX is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# @package    ArmarXCore::tools
# @author     Jan Issac (jan dot issac at gmail dot com)
# @date       2012
# @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
#             GNU General Public License

import DirectoryDefinition as DirDef
from TemplateDefinition import TemplateDefinition
from TemplateReplacementStrategy import TemplateReplacementStrategy

class ComponentDefinition(object):

    ##
    # Constructor
    #
    # @param componentNode            XML component definition element
    # @param rootDirectoryDefinition  Root directory definition of this
    #                                 component
    #
    def __init__(self, componentNode, templateReplacementStrategy):
        self._dependencies = []
        self._directoryDefinitions = []
        self._templateDefinitions = []
        
        self._type = componentNode.get("name")
        self._description = componentNode.get("description")

        self.templateReplacementStrategy = templateReplacementStrategy
        
        if componentNode.get("dependencies"):
            self._dependencies = componentNode.get("dependencies").split(",")

        for subDirNode in componentNode.findall("directory"):
            self.addDirectoryDefinition(DirDef.DirectoryDefinition(subDirNode, templateReplacementStrategy=self.templateReplacementStrategy))

        for templateNode in componentNode.findall("file"):
            self.addTemplateDefinition(TemplateDefinition(templateNode))


    ##
    # Returns the component type name
    #
    def getType(self):
        return self._type


    ##
    # Returns the component description
    #
    def getDescription(self):
        return self._description

    ## 
    # Returns the component dependency list
    #
    def getDependencies(self):
        return self._dependencies

    ##
    # Adds a sub-directory definition
    #
    # @param subDirectory    The new sub-directory
    #
    def addDirectoryDefinition(self, subDirectory):
        self._directoryDefinitions.append(subDirectory)


    ##
    # Adds a template definition
    #
    def addTemplateDefinition(self, templateDefinition):
        self._templateDefinitions.append(templateDefinition)


    ##
    # Returns the directory definitions
    #
    def getDirectoryDefinitions(self):
        return self._directoryDefinitions


    ##
    # Returns the component top-level template definitions
    #
    def getTemplateDefinitions(self):
        return self._templateDefinitions
