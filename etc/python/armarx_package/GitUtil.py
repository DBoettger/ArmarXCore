##
# This file is part of ArmarX.
#
# ArmarX is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2 as
# published by the Free Software Foundation.
#
# ArmarX is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# @package    ArmarXCore::tools
# @author     Jan Issac (jan dot issac at gmail dot com)
# @date       2012
# @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
#             GNU General Public License

from os import popen

##
# Returns the user name read from Git global config settings
#
def getUserName():
    defaultName = "[Author Name]"
    
    if not isGitAvailable():
        return defaultName
    
    username = popen("git config --global user.name").read()
    
    if "error:" in username:
        return defaultName
    
    username = username.strip()
    
    if not username:
        print "You may want use 'git config --global user.name=\"YourName\"'",\
              "This will include your name in any component you create."
        return defaultName
    
    return username.strip()


##
# Returns the user email read from Git global config settings
#
def getUserEmail():
    defaultEmail = "[Author Email]"
    if not isGitAvailable():
        return defaultEmail
    
    email = popen("git config --global user.email").read()
    
    if "error:" in email:
        return defaultEmail
    
    email = email.strip()
    
    if not email:
        print "You may want use 'git config --global user.email=YourEmail'",\
              "This will include your email in any component you create."
        return defaultEmail
    
    return email.strip()


##
# Checks whether Git is available and returns true if so, otherwise false
#
def isGitAvailable():
    version = popen("git --version").read()
    return ("git version" in version)

