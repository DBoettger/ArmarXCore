/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    @PACKAGE_NAME@::statechart::@COMPONENT_NAME@
 * @author     @AUTHOR_NAME@ ( @AUTHOR_EMAIL@ )
 * @date       @YEAR@
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#define BOOST_TEST_MODULE @PACKAGE_NAME@::statechart::@COMPONENT_NAME@

#define ARMARX_BOOST_TEST

#include <@PACKAGE_NAME@/Test.h>
#include <@PACKAGE_NAME@/statecharts/@COMPONENT_NAME@/@COMPONENT_NAME@.h>

#include <iostream>

BOOST_AUTO_TEST_CASE(testExample)
{
    armarx::@COMPONENT_NAME@ instance;

    BOOST_CHECK_EQUAL(true, true);
}
