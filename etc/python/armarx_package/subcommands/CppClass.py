##
# This file is part of ArmarX.
#
# ArmarX is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2 as
# published by the Free Software Foundation.
#
# ArmarX is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# @package    ArmarXCore::tools
# @author     Jan Issac (jan dot issac at gmail dot com)
# @date       2013
# @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
#             GNU General Public License

from os import path
from argparse import RawTextHelpFormatter
from armarx_package.SubCommand import SubCommand
from armarx_package.Structure import Structure
from armarx_package.TemplateReplacementStrategy import TemplateReplacementStrategy

from armarx_package.DirectoryDefinition import DirectoryDefinition
from armarx_package.Directory import Directory
from armarx_package.TemplateDefinition import TemplateDefinition
from armarx_package.Template import Template

from armarx_package.Manpage import Manpage

from armarx_package.Completers import PositionalCompleter


class CppClassSubCmd(SubCommand):

    ##
    # init sub-command constructor
    #
    def __init__(self, parserContainer):

        self.name = 'cpp-class'

        self.manpage = Manpage()

        self.manpage.setProgName('armarx-package')
        self.manpage.setName(self.name)
        self.manpage.setDescription('''
Create a C++ class consisting of a source and a header file.

Note:
  You may modify the CMakeLists.txt by add the class files to SOURCES and HEADERS.
''')
        self.manpage.setBriefDescription('Create a c++ class')
        self.manpage.setExamples('''
.B 1.
Create a C++ class within a package element

    $ cd /path/to/workspace/source/HelloWorldPackage/applications/myapp/
    $ armarx-package cpp-class MyClass .
    $ %s Generating .............. /path/to/workspace/source/HelloWorldPackage/applications/myapp/MyClass.cpp ...
    $ %s Generating .............. /path/to/workspace/source/HelloWorldPackage/applications/myapp/MyClass.h ...

''' % (u'>', u'>'))

        self.parser = parserContainer.add_parser(
                             self.name,
                             help="Create a C++ class",
                             formatter_class=RawTextHelpFormatter,
                             description=self.manpage.getBriefDescription(),
                             epilog='Checkout \'armarx-package help '\
                                   'cpp-class\' for more details and examples!')

        self.parser.add_argument("className",
                                 help="C++ class name").completer = \
                                 PositionalCompleter('className')

        self.parser.add_argument("location",
                                 help="Class location. Enter '.' for current "\
                                      "working directory").completer = \
                                 PositionalCompleter('location')

        self.parser.set_defaults(func=self.execute)

        self.addGlobalArguments(False)

    def execute(self, args):
        skeletonPath = self.getSkeletonPath()
        structureDefinitionFilePath = path.join(skeletonPath, 'package-structure.xml')

        templateReplacementStrategy = TemplateReplacementStrategy.getStrategyForComponentType(args.className, "default")

        structure = Structure.CreateStructureFromDefinition(structureDefinitionFilePath, '', '', False, templateReplacementStrategy)

        location = path.abspath(args.location)

        cppTemplate = path.join(skeletonPath,    'CppClass.tmp.cpp')
        headerTemplate = path.join(skeletonPath, 'CppClass.tmp.h')

        dirDefinition = DirectoryDefinition.CreateFromPath(location)
        directory = Directory(dirDefinition)

        classCpp = Template(TemplateDefinition.Create('%s.cpp' % args.className, cppTemplate))
        classHeader = Template(TemplateDefinition.Create('%s.h' % args.className, headerTemplate))

        structure.defineVariable('CLASS_NAME', args.className)
        variables = structure.getVariables()

        classCpp.set_variables(variables)
        classHeader.set_variables(variables)

        try:
            classCpp.writeToDirectory(directory, templateReplacementStrategy)
            classHeader.writeToDirectory(directory, templateReplacementStrategy)

            print self.outputPrefix, "Do not forget to add the class to the appropriate CMakeLists.txt!"
        except Exception as exc:
            self.handleException(args, exc)
