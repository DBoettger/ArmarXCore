##
# This file is part of ArmarX.
#
# ArmarX is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2 as
# published by the Free Software Foundation.
#
# ArmarX is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# @package    ArmarXCore::tools
# @author     Jan Issac (jan dot issac at gmail dot com)
# @date       2012
# @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
#             GNU General Public License

import sys
from os import path
import traceback
from Structure import Structure
from StructureDefinition import StructureDefinition
from Exceptions import InvalidPackageException
from Exceptions import UndefinedComponentException
from Exceptions import ComponentExistsException
from Exceptions import UnknownSubCommandException

from Manpage import Manpage

class SubCommand(object):

    ##
    # SubCommand execution function
    #
    def execute(self, args):
        raise Exception("Implement this sub-command function!")

    ##
    # Adds the debug option to the given parser
    #
    def addVerboseArgument(self, parser):
        parser.add_argument("--verbose",
                          action='store_true',
                          help="Provides additional information on errors")

    ##
    #
    #
    def addTemplateReplacementStrategyArgument(self):
        self.parser.add_argument('-s', '--strategy', action='store', default="default",
                         help='Specifies in which strategy to use for replacing Template specifications.')



    ##
    # Adds global arguments to the given parser
    #
    def addGlobalArguments(self, dirOption = True):
        globalGroup = self.parser.add_argument_group('global arguments')
        if dirOption:
            globalGroup.add_argument("-d", "--dir",
                                     default=".",
                                     help="ArmarX package top-level directory")
        self.addVerboseArgument(globalGroup)

    ##
    # Handles the error display in terms of debug settings
    #
    def handleException(self, args, exception):
        if args.verbose:
            print traceback.print_exc()
        try:
            if exception:
                raise exception
        except InvalidPackageException:
            print self.outputPrefix, "Error: %s: %s" % (exception, exception.reason)

            print "Make sure you execute this command in the top-level",\
                   "directory of the package or specify the package top-level",\
                   "directory with the --dir option."
            sys.exit(2)
        except:

            print '%s Error: %s' % (self.outputPrefix, exception)


    ##
    # Determines the package skeleton directory path
    #
    def getSkeletonPath(self):
        structureFileDir = path.dirname(path.realpath(__file__))
        skeletonPath = path.join(structureFileDir, "skeleton")
        return path.realpath(skeletonPath)

    ##
    # Prints the available categories
    #
    def printAvailableCategories(self, categoryDefinitions):
        # list defined component types
        print self.outputPrefix, "The following element categories are supported:"

        for  definition in categoryDefinitions.values():
            print "    %-25s %s" % ("'%s'" % definition.getType(), definition.getDescription())

    ##
    # Returns a list of available categories
    #
    def getCategoryList(self):
        if self.categoryList:
            return self.categoryList

        structureDefinitionFilePath = path.join(self.getSkeletonPath(),"package-structure.xml")

        definition = StructureDefinition(structureDefinitionFilePath)
        definition.getComponentDefinitions(True)

        componentDefinitions = definition.getComponentDefinitions(True)

        self.categoryList = []

        for definition in componentDefinitions.values():
            self.categoryList.append(definition.getType())

        return self.categoryList

    def getName(self):
        return self.name

    def getManpage(self):
        return self.manpage

    def getParser(self):
        return self.parser

    parser = None
    name = 'Undefined'
    outputPrefix = ">"
    manpage = None
    categoryList = []

