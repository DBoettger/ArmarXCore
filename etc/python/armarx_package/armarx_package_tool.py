#!/usr/bin/env python2
# encoding: utf-8

##
# This file is part of ArmarX.
#
# ArmarX is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2 as
# published by the Free Software Foundation.
#
# ArmarX is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# @package    ArmarXCore::tools
# @author     Jan Issac (jan dot issac at gmail dot com)
# @date       2012
# @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
#             GNU General Public License
#
#  ArmarX Package
#
# is a tool to create and modify ArmarX packages.
#

import sys
import os
import traceback

try:
    from argparse import RawDescriptionHelpFormatter
except:
    print "Your Python version is not up to date. At least version 2.7 is required"
    import sys
    sys.exit()

from argcomplete import autocomplete

from armarx_package.CommandParser import CommandParser

from armarx_package.Exceptions import ArgumentParserException
from armarx_package.subcommands.Init import InitSubCmd
from armarx_package.subcommands.Add import AddSubCmd
from armarx_package.subcommands.Help import HelpSubCmd
from armarx_package.subcommands.Remove import RemoveSubCmd
from armarx_package.subcommands.Status import StatusSubCmd
from armarx_package.subcommands.CppClass import CppClassSubCmd

__all__ = []
__version__ = 1.2
__date__ = '2012-10-16'
__updated__ = '2013-04-10'

def main(argv=None):

    if argv is None:
        argv = sys.argv
    else:
        sys.argv.extend(argv)

    verbose = False

    program_name = os.path.basename(sys.argv[0])
    program_version = "v%s" % __version__
    program_build_date = str(__updated__)
    program_version_message = '%%(prog)s %s (%s)' % (program_version,
                                                     program_build_date)
    program_shortdesc = "  ArmarX package creation tool"
    program_help_usage = "Use 'armarx-package help <sub-command>' for more "\
                         "details on a specific sub-command."
    program_license = '''%s

  Copyright 2013 H2T, Karlsruhe Institute of Technology. All rights reserved.

  Licensed GNU General Public License
  http://www.gnu.org/licenses/gpl-2.0.txt

''' % (program_shortdesc)

    try:

        # Setup main command parser
        parser = CommandParser(description=program_license,
                                formatter_class=RawDescriptionHelpFormatter,
                                version=program_version_message,
                                epilog=program_help_usage)

        subParsers = parser.getSubParserContainer()

        # sub-commands
        initSubCmd = InitSubCmd(subParsers)
        parser.registerSubCommand(initSubCmd)

        addSubCmd = AddSubCmd(subParsers)
        parser.registerSubCommand(addSubCmd)

        removeSubCmd = RemoveSubCmd(subParsers)
        parser.registerSubCommand(removeSubCmd)

        statusSubCmd = StatusSubCmd(subParsers)
        parser.registerSubCommand(statusSubCmd)

        cppClassSubCmd = CppClassSubCmd(subParsers)
        parser.registerSubCommand(cppClassSubCmd)

        helpSubCmd = HelpSubCmd(subParsers)
        parser.registerSubCommand(helpSubCmd)

        # Process arguments
        try:
            autocomplete(parser, False)
            args = parser.parse_args()
        except ArgumentParserException, exc:
            # if the 'help' sub-command has been used without arguments, print
            # only help instead of error message
            if exc.progName.endswith("help"):
                parser.print_help()
                sys.exit(0)
            elif len(exc.progName.split(" ")) == 1:
                if exc.message.endswith('too few arguments'):
                    parser.print_help()
                    sys.exit(2)
            print exc.usage, exc.message
            sys.exit(2)

        verbose = args.verbose
        args.commands = parser.getSubCommands()
        args.func(args)

        return 0
    except KeyboardInterrupt:
        ### handle keyboard interrupt ###
        return 0
    except Exception, e:
        indent = len(program_name) * " "
        sys.stderr.write(program_name + ": " + repr(e) + "\n")
        sys.stderr.write(indent + "  for help use --help\n")
        #if verbose:
        exc_type, exc_value, exc_traceback = sys.exc_info()
        traceback.print_tb(tb=exc_traceback, file=sys.stderr)
        return 2
