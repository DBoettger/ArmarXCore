##
# This file is part of ArmarX.
#
# ArmarX is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2 as
# published by the Free Software Foundation.
#
# ArmarX is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# @package    ArmarXCore::tools
# @author     Jan Issac (jan dot issac at gmail dot com)
# @date       2012
# @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
#             GNU General Public License

from os import path
from TemplateDefinition import TemplateDefinition
from Exceptions import WritingFileException
from Exceptions import FileNotFoundException

import re

class Template(TemplateDefinition):

    ##
    # Constructor
    #
    def __init__(self, templateDefinition):
        self._node = templateDefinition._node
        self._name = templateDefinition._name
        self._content = templateDefinition._content
        self._templateFile = templateDefinition._templateFile
        self._superclass = templateDefinition._superclass
        self._content = None
        self._variables = {}
        self._empty = templateDefinition._empty

    def set_variables(self, variables):
        self._variables = variables
        for variable, value in self._variables.items():
            self._name = self._name.replace(variable, value)
            self._templateFile = self._templateFile.replace(variable, value)

    ##
    # Compiles and writes the template
    #
    def writeToDirectory(self, directory, templateReplacementStrategy):

        destinationPath = path.join(directory.getPath(), self.getName())


        # bind definition

        if self.exists(directory):
            with open(destinationPath, "r") as existingFile:
                content = existingFile.read()
            print '%s %s %s ...' % (u'>', 'Updating'.ljust(25, '.'), destinationPath)
        else:
            # load template content
            try:
                with open(self._templateFile, 'r') as templateFile:
                    content = templateFile.read()
            except:
                raise FileNotFoundException("Skeleton template file: " + self._templateFile)
            print '%s %s %s ...' % (u'>', 'Generating '.ljust(25, '.'), destinationPath)

        # replace templates according to the selected strategy
        if not self._empty:
            content = templateReplacementStrategy.replaceTemplates(content)

        # compile template content
        self._content = ""
        for line in content.splitlines(True):
            #Ignore comments (template lines)
            if not line.startswith("#@TEMPLATE_LINE@"):
                for variable, value in self._variables.items():
                    line = line.replace(variable, value)
            self._content += line

        try:
            with open(destinationPath, 'w+') as destinationFile:
                destinationFile.write(self.getContent())
        except:
            raise WritingFileException(destinationPath)




    ##
    # Checks whether the compiled template file already exists in the
    # specified directory
    #
    def exists(self, directory):
        destinationPath = path.join(directory.getPath(), self.getName())
        return path.exists(destinationPath)

    def getContentIn(self, directory):
        filePath = path.join(directory.getPath(), self.getName())
        with open(filePath, 'r') as templateFile:
            return templateFile.read()

    ##
    # Validates the bound file.
    #
    def validate(self, directory):
        superclassVerified = False

        if self.hasSuperclass():
            # check of super class
            superclassRegex = r":\s+(virtual\s+)?(public\s+)?(armarx::)?%s" % self.getSuperclass()
            pattern = re.compile(superclassRegex)

            content = self.getContentIn(directory)

            if pattern.search(content):
                superclassVerified = True

            if not superclassVerified:
                raise Exception("Missing super class %s" % self.getSuperclass())

    ##
    # Remove this file
    #
    def remove(self, directory):
        destinationPath = path.join(directory.getPath(), self.getName())
        #rmtree(destinationPath)
        print "removing", destinationPath
        
    ##
    # Returns whether the file is required to exist
    #
    def isRequired(self):
        return TemplateDefinition.isRequired(self)
