##
# This file is part of ArmarX.
#
# ArmarX is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2 as
# published by the Free Software Foundation.
#
# ArmarX is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# @package    ArmarXCore::tools
# @author     Jan Issac (jan dot issac at gmail dot com)
# @date       2013
# @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
#             GNU General Public License
#

from argparse import RawTextHelpFormatter

SUPPRESS = '==SUPPRESS=='

class ManpageFormatter(RawTextHelpFormatter):

    def _format_heading(self, heading):
        return '.TP\n%s\n' % heading.upper()

    def _format_usage(self, usage, actions, groups, prefix):
        return super(ManpageFormatter, self)._format_usage(
                       usage, actions, groups, '')

    class _ManpageSection(RawTextHelpFormatter._Section):
        def format_help(self):
            # format the indented section
            if self.parent is not None:
                self.formatter._indent()
            join = self.formatter._join_parts
            for func, args in self.items:
                func(*args)
            item_help = join([func(*args) for func, args in self.items])
            if self.parent is not None:
                self.formatter._dedent()

            # return nothing if the section was empty
            if not item_help:
                return ''

            # add the heading if the section was non-empty
            if self.heading is not SUPPRESS and self.heading is not None:
                current_indent = self.formatter._current_indent
                heading = '.SH %*s%s\n' % (current_indent, '', self.heading.upper())
            else:
                heading = ''

            # join the section-initial newline, the heading and the help
            return join(['\n', heading, item_help, '\n'])

    def start_section(self, heading):
        self._indent()
        section = self._ManpageSection(self, self._current_section, heading)
        self._add_item(section.format_help, [])
        self._current_section = section

    ##
    # format arguments and help text
    #
    def _format_action(self, action):
        return '%s%s\n\n' % (self._format_action_invocation(action), self._expand_help(action))

    ##
    # format argument names
    #
    def _format_action_invocation(self, action):
        return '.IP "%s"\n' % super(ManpageFormatter, self)._format_action_invocation(action)

    ##
    # format description
    #
    def _format_text(self, text):
        return ''
