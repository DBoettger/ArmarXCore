##
# This file is part of ArmarX.
#
# ArmarX is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2 as
# published by the Free Software Foundation.
#
# ArmarX is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.
#
# @package    ArmarXCore::tools
# @author     Jan Issac (jan dot issac at gmail dot com)
# @date       2012
# @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
#             GNU General Public License

from os import path

class UnknownSubCommandException(Exception):
    def __init__(self, subCommand):
        Exception.__init__(self, "Unknown sub-command: " + subCommand)

class ArgumentParserException(Exception):
    def __init__(self, message, progName, usage):
        Exception.__init__(self, message)
        self.message = '%s: error: %s' % (progName, message)
        self.progName = progName
        self.usage = usage

class UndefinedComponentException(Exception):
    def __init__(self, typeName):
        Exception.__init__(self, "Undefined component type: " + typeName)


class InvalidComponentException(Exception):
    def __init__(self, name, typeName, reason):
        Exception.__init__(self, name + " is not a valid " + typeName + \
                           " component. \n - " + reason)

class InvalidPackageException(Exception):
    def __init__(self, packageName, packageLocation, reason):
        error = "Invalid package '" + packageName + "' in " \
                + path.dirname(packageLocation)
        self.reason = reason
        Exception.__init__(self,  error)

class DirectoryExistsException(Exception):
    def __init__(self, directoryPath):
        Exception.__init__(self, "Directory already exists: " + directoryPath)


class DirectoryNotFoundException(Exception):
    def __init__(self, directoryPath):
        Exception.__init__(self, "Cannot find: " + directoryPath)


class MissingDirectoryException(Exception):
    def __init__(self, directoryPath):
        Exception.__init__(self, "Missing directory: " + directoryPath)


class FileExistsException(Exception):
    def __init__(self, filePath):
        Exception.__init__(self, "File already exists: " + filePath)


class MissingFileException(Exception):
    def __init__(self, filePath):
        Exception.__init__(self, "Missing file: " + filePath)


class FileNotFoundException(Exception):
    def __init__(self, filePath):
        Exception.__init__(self, "Cannot find: " + filePath)


class WritingFileException(Exception):
    def __init__(self, filePath):
        Exception.__init__(self, "Modifying file failed: " + filePath)


class ComponentExistsException(Exception):
    def __init__(self, componentName, componentType, componentLocation):
        error = "The " + componentName + " " + componentType \
                + " component already exists in " + componentLocation
        Exception.__init__(self, error)


class ComponentNotFoundException(Exception):
    def __init__(self, componentName, comcponentType):
        error = "The " + componentName + " " + comcponentType \
                + " component does not exist"
        Exception.__init__(self, error)


class ComponentRemovedException(Exception):
    def __init__(self, componentName, comcponentType):
        error = "The " + componentName + " " + comcponentType \
                + " component is already not a part of the package"
        Exception.__init__(self, error)


class ComponentJoinedException(Exception):
    def __init__(self, componentName, comcponentType):
        error = "The " + componentName + " " + comcponentType \
                + " component is already a part of the package"
        Exception.__init__(self, error)

class UnmanagedComponentException(Exception):
    def __init__(self, componentName, comcponentType):
        error = "The " + componentName + " " + comcponentType \
                + " component is not explicitly managed by CMake. " \
                + "It cannot be removed from CMake unless deleted."
        Exception.__init__(self, error)
