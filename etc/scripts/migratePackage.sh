#!/bin/bash

# ArmarXCoreInterfaces rename
find source/* -type f -exec sed -i 's/ArmarXInterfaces/ArmarXCoreInterfaces/g' {} \;
# ArmarXCore include rename
find source/* -type f -exec sed -i 's/<Core\//<ArmarXCore\//g' {} \;
# ArmarXGui include rename
find source/* -type f -exec sed -i 's/<Gui\//<ArmarXGui\//g' {} \;
# ArmarXGui promoted widgets rename
find source/* -iname "*.ui" -exec sed -i 's/>Gui\//>ArmarXGui\//g' {} \;
# RobotComponents extraction
find . -type f -exec sed -i 's/<RobotSkills\//<RobotComponents\//g' {} \;
find . -type f -exec sed -i 's/RobotSkillsInterfaces/RobotComponentsInterfaces/g' {} \;
# RobotSkillTemplates rename
find . -type f -exec sed -i 's/RobotSkills/RobotSkillTemplates/g' {} \;

# changes to armarx_add_library() and armarx_gui_library() macros
# remove LIB_VERSION and LIB_SOVERSION parameters
find . -iname "*.txt" -exec sed -E -i 's/^(.*armarx_add_library\(.*)("\$\{LIB_VERSION\}" "\$\{LIB_SOVERSION\}")(.*\))/\1\3/g' {} \;
find . -iname "*.txt" -exec sed -E -i 's/^(.*armarx_gui_library\(.*)("\$\{LIB_VERSION\}" "\$\{LIB_SOVERSION\}")\)/\1)/g' {} \;
# remove LIB_VERSION and LIB_SOVERSION variables
find . -iname "*.txt" -exec sed -E -i 's/^(.*set\(LIB_SOVERSION.*)//g' {} \;
find . -iname "*.txt" -exec sed -E -i 's/^(.*set\(LIB_VERSION.*)//g' {} \;
