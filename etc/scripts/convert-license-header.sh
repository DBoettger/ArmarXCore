#!/bin/bash
files=$(find . -iname *.h)
files+=$(find . -iname *.cpp)

for file in ${files}
do
    # replace /** with /*
    echo "changing ${file}"
    sed -i "" "1s/^\/\*\*/\/\*/" ${file}
done
