# Macros for ArmarX Statechart Libraries

add_custom_target(GENERATE_ALL_STATECHARTS)

include(CMakeParseArguments)

function(armarx_add_generator_target)
    ###############
    #define params#
    ###############
    set(single_param
        TARGET
    )
    set(multi_param
        COMMAND
        ARGS
        DEPENDS
        COMMENT
        DEPENDENCY_OF
        OUTPUT
    )
    cmake_parse_arguments(PARAM "${flags}" "${single_param}" "${multi_param}" ${ARGN} )
    ##############
    #check params#
    ##############
    if("" STREQUAL "${PARAM_TARGET}")
        message(FATAL_ERROR "Parameter TARGET for generator target was not set! ${PARAM_TARGET}")
    endif()
    if("" STREQUAL "${PARAM_COMMAND}")
        message(FATAL_ERROR "Parameter COMMAND for generator target '${PARAM_TARGET}' was not set! ${PARAM_COMMAND}")
    endif()
    if("" STREQUAL "${PARAM_DEPENDS}")
        message(FATAL_ERROR "Parameter DEPENDS for generator target '${PARAM_TARGET}' was not set! ${PARAM_DEPENDS}")
    endif()
    #############
    #add targets#
    #############
    set(touch_file "${CMAKE_CURRENT_BINARY_DIR}/${PARAM_TARGET}.touch")
    add_custom_command(
        OUTPUT   ${touch_file}
        COMMAND  ${PARAM_COMMAND}   ARGS     ${PARAM_ARGS}
        COMMAND  touch              ARGS    "${touch_file}"
        DEPENDS  ${PARAM_DEPENDS}
        COMMENT "${PARAM_COMMENT}"
    )
    add_custom_target(${PARAM_TARGET} DEPENDS ${touch_file})

    foreach(dependency ${PARAM_DEPENDENCY_OF})
        add_dependencies(${dependency} ${PARAM_TARGET})
    endforeach()

    foreach(output ${PARAM_OUTPUT})
        if(NOT EXISTS "${output}")
            execute_process(COMMAND touch "${output}")
        endif()
    endforeach()
endfunction()

macro(add_statechart_generation_dependency HEADERS)
    foreach(CURRENT_HEADER_FILE ${HEADERS})
        string(REGEX MATCH "^(.+)\\.scgxml$" VOID ${CURRENT_HEADER_FILE})
        if (NOT "${CMAKE_MATCH_1}" STREQUAL "")
            set(GROUP_NAME_NO_SUFFIX ${CMAKE_MATCH_1})
            set(GROUP_FILE "${GROUP_NAME_NO_SUFFIX}.scgxml")
            FILE(STRINGS ${CURRENT_HEADER_FILE} GROUP_FILE_MATCHES REGEX "generateContext=\"true\"")
            if(GROUP_FILE_MATCHES)
                set(CONTEXT_GENERATION_ENABLED TRUE)
            endif()
        endif()
    endforeach()
    if(GROUP_FILE)
        add_dependencies(${ARMARX_LIB_NAME} GENERATE_ALL_STATECHARTS)# needed for transition functions (to generate state code files at the right time)
    endif()
endmacro()

macro(generate_statechart_headers HEADERS)

    foreach(CURRENT_HEADER_FILE ${HEADERS})
        string(REGEX MATCH "^(.+)\\.scgxml$" VOID ${CURRENT_HEADER_FILE})
        if (NOT "${CMAKE_MATCH_1}" STREQUAL "")
            set(GROUP_NAME_NO_SUFFIX ${CMAKE_MATCH_1})
            set(GROUP_FILE "${GROUP_NAME_NO_SUFFIX}.scgxml")
            FILE(STRINGS ${CURRENT_HEADER_FILE} GROUP_FILE_MATCHES REGEX "generateContext=\"true\"")
            if(GROUP_FILE_MATCHES)
                set(CONTEXT_GENERATION_ENABLED TRUE)
            endif()
        endif()
    endforeach()
    if(GROUP_FILE)
        set(GROUP_FILE_ABS "${CMAKE_CURRENT_SOURCE_DIR}/${GROUP_NAME_NO_SUFFIX}.scgxml")
        MESSAGE(STATUS "Statechartgroup: ${GROUP_FILE} in ${CMAKE_CURRENT_BINARY_DIR}")

        add_custom_target(${ARMARX_LIB_NAME}_GENERATE_STATECHARTS)
        add_dependencies(GENERATE_ALL_STATECHARTS ${ARMARX_LIB_NAME}_GENERATE_STATECHARTS)

        if(CONTEXT_GENERATION_ENABLED)
            MESSAGE(STATUS "Context generation enabled")
            armarx_add_generator_target(
                TARGET        ${ARMARX_PROJECT_NAME}_${ARMARX_LIB_NAME}_GENERATE_STATECHARTS_CONTEXT
                DEPENDENCY_OF ${ARMARX_LIB_NAME}_GENERATE_STATECHARTS
                DEPENDS       "${ArmarXCore_BINARY_DIR}/StatechartGroupGeneratorAppRun"
                              "${GROUP_FILE_ABS}"
                COMMAND       "${ArmarXCore_BINARY_DIR}/StatechartGroupGeneratorAppRun"
                ARGS          "context" "${GROUP_FILE_ABS}" "${CMAKE_BINARY_DIR}"
                COMMENT       "Generating ${GROUP_NAME_NO_SUFFIX}StatechartContext.generated.h from ${GROUP_FILE_ABS}"
                OUTPUT        "${CMAKE_CURRENT_BINARY_DIR}/${GROUP_NAME_NO_SUFFIX}StatechartContext.generated.h"
                              "${CMAKE_CURRENT_BINARY_DIR}/${GROUP_NAME_NO_SUFFIX}StatechartContextBase.generated.h"
            )

            list(APPEND HEADERS
                "${CMAKE_CURRENT_BINARY_DIR}/${GROUP_NAME_NO_SUFFIX}StatechartContext.generated.h"
                "${CMAKE_CURRENT_BINARY_DIR}/${GROUP_NAME_NO_SUFFIX}StatechartContextBase.generated.h"
            )
        endif()
        #does not make sense to install CMakeLists without installing the CPP files
        #list(APPEND HEADERS "CMakeLists.txt")
        SET(GENERATED_FILES "")
        SET(HEADERS_COPY "${HEADERS}")
        SET(ANY_XML_FOUND FALSE)
        foreach(CURRENT_HEADER_FILE ${HEADERS_COPY})
            string(REGEX MATCH "^(.+)\\.xml$" VOID ${CURRENT_HEADER_FILE})
            set(STATE_PATH_NO_SUFFIX ${CMAKE_MATCH_1})
            if (NOT "${STATE_PATH_NO_SUFFIX}" STREQUAL "")
                string(REGEX MATCH "([^\\/]+)\\.xml$" VOID ${CURRENT_HEADER_FILE})
                set(STATE_NAME ${CMAKE_MATCH_1})
                SET(ANY_XML_FOUND TRUE)

                set(ABS_STATE_PATH "${CMAKE_CURRENT_SOURCE_DIR}/${STATE_PATH_NO_SUFFIX}.xml")

                set(generator_target ${ARMARX_PROJECT_NAME}_${ARMARX_LIB_NAME}_GENERATE_STATECHARTS_${STATE_NAME})
                if(TARGET ${generator_target})
                    message("The target ${generator_target} is already defined! Check your CMakeLists.txt or ${GROUP_NAME_NO_SUFFIX}.scgxml. It probably contains the files '${STATE_PATH_NO_SUFFIX}.{xml, h, cpp}' multiple times.")
                endif()
                armarx_add_generator_target(
                    TARGET        ${generator_target}
                    DEPENDENCY_OF ${ARMARX_LIB_NAME}_GENERATE_STATECHARTS
                    DEPENDS       "${ArmarXCore_BINARY_DIR}/StatechartGroupGeneratorAppRun"
                                  "${CMAKE_CURRENT_SOURCE_DIR}/${GROUP_FILE}"
                                  "${ABS_STATE_PATH}"                                  
                    COMMAND       "${ArmarXCore_BINARY_DIR}/StatechartGroupGeneratorAppRun"
                    ARGS          "${CMAKE_CURRENT_SOURCE_DIR}/${GROUP_FILE}" "${ABS_STATE_PATH}" "${CMAKE_BINARY_DIR}"
                    COMMENT       "Generating ${CMAKE_CURRENT_BINARY_DIR}/${STATE_NAME}.generated.h from ${ABS_STATE_PATH}"
                    OUTPUT        "${CMAKE_CURRENT_BINARY_DIR}/${STATE_NAME}.generated.h"
                )

                list(APPEND HEADERS "${CMAKE_CURRENT_BINARY_DIR}/${STATE_NAME}.generated.h")
            endif()

        endforeach()
        if(ANY_XML_FOUND)
            include_directories("${CMAKE_CURRENT_BINARY_DIR}")
            include_directories("${CMAKE_CURRENT_SOURCE_DIR}")
        endif()
    endif()

endmacro()

macro(armarx_generate_statechart_cmake_lists)
    # next line triggers cmake when scgxml was changed
    configure_file("${CMAKE_CURRENT_SOURCE_DIR}/${ARMARX_COMPONENT_NAME}.scgxml" "${CMAKE_CURRENT_BINARY_DIR}/${ARMARX_COMPONENT_NAME}.scgxml.touch")
    configure_file("${ArmarXCore_BINARY_DIR}/StatechartGroupGeneratorAppRun" "${CMAKE_CURRENT_BINARY_DIR}/StatechartGroupGeneratorAppRun.touch")

#    message(STATUS "${ARMARX_PROJECT_PACKAGE_DEPENDENCY_PATHS}")
    # For newer cmakes: use this for dependency injection
    #set_property(DIRECTORY APPEND PROPERTY CMAKE_CONFIGURE_DEPENDS "${CMAKE_CURRENT_SOURCE_DIR}/${ARMARX_COMPONENT_NAME}.scgxml")

    get_property(STATECHART_CMAKE_FILES_GENERATED GLOBAL PROPERTY statechart_cmake_files_generated)
    if(NOT STATECHART_CMAKE_FILES_GENERATED)
        set_property(GLOBAL PROPERTY statechart_cmake_files_generated TRUE)
        execute_process(COMMAND "${ArmarXCore_BINARY_DIR}/StatechartGroupGeneratorAppRun" "package-cmake" "${PROJECT_NAME}"
            "${PROJECT_SOURCECODE_DIR}/${PROJECT_NAME}/statecharts" "${PROJECT_BINARY_DIR}" "${PROJECT_DATA_DIR}"
            "${ARMARX_PROJECT_PACKAGE_DEPENDENCY_PATHS}" RESULT_VARIABLE CMD_RESULT)
    endif()

#    if (NOT "${CMD_RESULT}" STREQUAL "0")
#        message(FATAL_ERROR "Generation of cmake files for statechart group ${ARMARX_COMPONENT_NAME} failed!")
#    endif()

    set(INCLUDE_FILE_PATH "${CMAKE_CURRENT_BINARY_DIR}/${ARMARX_COMPONENT_NAME}Files.generated.cmake")
    message(STATUS "Using statechart CMake file  ${INCLUDE_FILE_PATH}")

    if(EXISTS "${INCLUDE_FILE_PATH}")
        include("${INCLUDE_FILE_PATH}")
    else()
        message(WARNING "${INCLUDE_FILE_PATH} was not found - this statechart group won't compile correctly - you need to compile ArmarXCore first")
    endif()



endmacro()
