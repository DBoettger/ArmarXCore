# Setup ArmarX framework environment
# To be used by projects using ArmarX framework to get all the setup work done

if(NOT DEFINED CMAKE_SUPPRESS_DEVELOPER_WARNINGS)
     set(CMAKE_SUPPRESS_DEVELOPER_WARNINGS 1 CACHE INTERNAL "No dev warnings")
endif()
SET(OLD_POLICIES CMP0026 CMP0043 CMP0045 CMP0054)
foreach(OLD_POLICY ${OLD_POLICIES})
#    message(STATUS "Checking ${OLD_POLICY} policy")
    if(POLICY ${OLD_POLICY})
        cmake_policy(SET ${OLD_POLICY} OLD)
#        message(STATUS "Setting ${OLD_POLICY} policy to OLD")
    endif()
endforeach()


# Default project name is ArmarXCore
if (NOT ARMARX_PROJECT_NAME)
    set(ARMARX_PROJECT_NAME "ArmarXCore")
endif()

# Used for printing a list of enabled/disabled components after cmake finished
# see install_project() macro in file Installation.cmake
include(FeatureSummary)

# Utility functions and macros
include(${ArmarXCore_CMAKE_DIR}/CMakeUtil.cmake)
# Utility functions and macros for debugging
include(${ArmarXCore_CMAKE_DIR}/CMakeDebugging.cmake)
# Identify OS and setup
include(${ArmarXCore_CMAKE_DIR}/SetupOs.cmake)
# Setup directories and paths
include(${ArmarXCore_CMAKE_DIR}/SetupDirs.cmake)
#required while armarx supports qt4 and qt5
include(${ArmarXCore_CMAKE_DIR}/ArmarXQt4Qt5DualSupport.cmake)

include(${${PROJECT_NAME}_CMAKE_DIR}/ArmarXPackageVersion.cmake)

# Find Ice
find_package(Ice 3.4.2 REQUIRED)
include_directories( ${Ice_INCLUDE_DIRS} )
include(${ArmarXCore_CMAKE_DIR}/IceUse.cmake)



# pthread
find_package(Threads REQUIRED)

# Fix for the Qt4 FindPackage script which determines
# the path to Qt4 according to the variable QT_QMAKE_EXECUTABLE
# which should be set to the path of the qmake executable

IF(NOT "$ENV{QT_QMAKE_EXECUTABLE}" STREQUAL "")
    MESSAGE (STATUS "USING QT-PATH from environment variable QT_QMAKE_EXECUTABLE: $ENV{QT_QMAKE_EXECUTABLE}")
    file(TO_CMAKE_PATH "$ENV{QT_QMAKE_EXECUTABLE}" QT_QMAKE_EXECUTABLE)
ENDIF()

include(${ArmarXCore_CMAKE_DIR}/CompileSettings.cmake)
include(${ArmarXCore_CMAKE_DIR}/Installation.cmake)
include(${ArmarXCore_CMAKE_DIR}/ArmarXStatechart.cmake)
include(${ArmarXCore_CMAKE_DIR}/ArmarXCommands.cmake)
include(${ArmarXCore_CMAKE_DIR}/ArmarXInterfaces.cmake)
include(${ArmarXCore_CMAKE_DIR}/ArmarXComponent.cmake)
include(${ArmarXCore_CMAKE_DIR}/ArmarXCodingStyle.cmake)
include(${ArmarXCore_CMAKE_DIR}/ArmarXCodeAnalysis.cmake)
include(${ArmarXCore_CMAKE_DIR}/Testing.cmake)
include(${ArmarXCore_CMAKE_DIR}/Documentation.cmake)
include(${ArmarXCore_CMAKE_DIR}/ArmarXScenario.cmake)
include(${ArmarXCore_CMAKE_DIR}/ArmarXPackaging.cmake)
#include(${ArmarXCore_CMAKE_DIR}/ArmarXDeployment.cmake)

# Boost
# look for boost after including testing.cmake because it checks whether the test framework of boost is available and overwrites the boost variables
MESSAGE(STATUS "Looking for Boost")
set(Boost_USE_MULTITHREADED ON)
set(Boost_ADDITIONAL_VERSIONS "1.48.0" "1.48")
#set(Boost_NO_SYSTEM_PATHS ON) # prevent mixing different Boost installations
find_package(Boost 1.48 REQUIRED COMPONENTS thread regex filesystem system program_options)
include_directories(${Boost_INCLUDE_DIR})
MESSAGE(STATUS "Boost found at ${Boost_INCLUDE_DIR}")

export_project()
