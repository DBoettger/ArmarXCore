# Macros for formating all source files according to the ArmarX CodingStyle

find_package(AStyle QUIET)

option(ARMARX_ENABLE_AUTO_CODE_FORMATTING "format changed sourcefiles with asyle before compilation if this is set to TRUE" FALSE)

if(AStyle_FOUND)
    if (${ARMARX_ENABLE_AUTO_CODE_FORMATTING})
        # this target gets executed automatically before compilation
        # only untracked or modified files will be formatted
        add_custom_target(auto_code_format ALL
            COMMAND "${CMAKE_COMMAND}" -DAStyle_EXECUTABLE="${AStyle_EXECUTABLE}" -DAStyle_OPTIONS_FILE="${ArmarXCore_TEMPLATES_DIR}/armarx.astylerc" -DPROJECT_SOURCECODE_DIR="${PROJECT_SOURCECODE_DIR}" -DGIT_EXECUTABLE="${GIT_EXECUTABLE}" -DFORCE_ALL_FILES=FALSE -P "${ArmarXCore_CMAKE_DIR}/FormatSourcecode.cmake"
            WORKING_DIRECTORY "${PROJECT_SOURCECODE_DIR}"
            COMMENT "Formatting source files in: ${PROJECT_SOURCECODE_DIR}")
    endif()

    # format all sourcecode files found in PROJECT_SOURCECODE_DIR
    add_custom_target(code_format
        COMMAND "${CMAKE_COMMAND}" -DAStyle_EXECUTABLE="${AStyle_EXECUTABLE}" -DAStyle_OPTIONS_FILE="${ArmarXCore_TEMPLATES_DIR}/armarx.astylerc" -DPROJECT_SOURCECODE_DIR="${PROJECT_SOURCECODE_DIR}" -DGIT_EXECUTABLE="${GIT_EXECUTABLE}" -DFORCE_ALL_FILES=TRUE -P "${ArmarXCore_CMAKE_DIR}/FormatSourcecode.cmake"
        WORKING_DIRECTORY "${PROJECT_SOURCECODE_DIR}"
        COMMENT "Formatting source files in: ${PROJECT_SOURCECODE_DIR}")
else()
    add_custom_target(code_format ALL
        COMMENT "Unable to format source files: AStyle was not found!")
endif()
