# Utility and convenience functions and macros
# for ArmarX framework

# Prints a list
function(printlist PREFIX L)
    foreach (ITEM ${L})
        message(STATUS "${PREFIX}${ITEM}")
    endforeach()
endfunction()

macro(ARMARX_MESSAGE VERBOSITY MSG)
    if(${VERBOSE})
        message(${VERBOSITY} "${MSG}")
    endif()
endmacro()


# Prints header/source/library files for a given target
macro(printtarget HEADERS SOURCES LIBS)
    if(${VERBOSE})
        ARMARX_MESSAGE(STATUS "        Headers:")
        printlist("            " "${HEADERS}")
        ARMARX_MESSAGE(STATUS "        Sources:")
        printlist("            " "${SOURCES}")
        ARMARX_MESSAGE(STATUS "        Libraries:")
        printlist("            " "${LIBS}")
    endif()
endmacro()

# Sets variables required for compilation macros and prints the target getting configured
macro(armarx_set_target TARGET_NAME)
    set(ARMARX_TARGET_NAME "${TARGET_NAME}")
    set(ARMARX_BUILD TRUE)
    message(STATUS "\n== Configuring target ${TARGET_NAME} ... ==")
endmacro()

# Conditional building of targets
macro(armarx_build_if CONDITION FAIL_REASON)
    if (ARMARX_BUILD)
        if (NOT ${CONDITION})
            message(STATUS "NOT Building \"${ARMARX_TARGET_NAME}\" because: ${FAIL_REASON}.")
            set(ARMARX_BUILD FALSE)
        endif()
    endif()
endmacro()


# ARMARX_ASSERT(TEST COMMENT_FAIL [COMMENT_PASS=''] [IS_FATAL=FALSE])
macro(ARMARX_ASSERT TEST COMMENT_FAIL)

    if (${TEST})
        # ARG2 holds COMMENT_PASS
        if (${ARGC} GREATER 2)
            message(STATUS ${ARGV2})
        endif()
    else()
        set(IS_FATAL 0)
        if (${ARGC} GREATER 3)
            set(IS_FATAL ${ARGV3})
        endif()

        if (${IS_FATAL})
            message(FATAL_ERROR ${COMMENT_FAIL})
        else()
            message(STATUS ${COMMENT_FAIL})
        endif()
    endif()

endmacro()

macro(armarx_cache_append_value VARIABLE_NAME ENTRY)
    if (${VARIABLE_NAME})
        SET(${VARIABLE_NAME} "${${VARIABLE_NAME}} ${ENTRY}" CACHE INTERNAL "" FORCE)
    else()
        set(${VARIABLE_NAME} "${ENTRY}" CACHE INTERNAL "" FORCE)
    endif()
endmacro()

#convert a list to a string with a given delim
#see https://stackoverflow.com/a/31880875
function(list_to_string result delim)
    list(GET ARGV 2 temp)
    math(EXPR N "${ARGC}-1")
    foreach(IDX RANGE 3 ${N})
        list(GET ARGV ${IDX} STR)
        set(temp "${temp}${delim}${STR}")
    endforeach()
    set(${result} "${temp}" PARENT_SCOPE)
endfunction()
