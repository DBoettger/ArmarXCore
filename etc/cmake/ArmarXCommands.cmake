# Macros for ArmarX Framework Libraries

define_property(GLOBAL PROPERTY ARMARX_APPEND_TO_CONFIG_CMAKE_STRING
    BRIEF_DOCS "String to append to the config file"
    FULL_DOCS "String to append to the config file. This string may contain libs and includes of all build libraries")

function(export_variable_to_package_config var)
    set_property(GLOBAL APPEND_STRING
        PROPERTY ARMARX_APPEND_TO_CONFIG_CMAKE_STRING
        "set(${var} \"${${var}}\")\n")
endfunction()

# remove ".ice" suffix
macro(stripXmlSuffix XML_FILE STRIPPED_XML_FILE)
    string(LENGTH ${SLICE_FILE} LENGTH)
    math(EXPR LENGTH "${LENGTH} - 4")
    string(SUBSTRING ${SLICE_FILE} 0 ${LENGTH} STRIPPED_SLICE_FILE)
endmacro()

function(armarx_target_enable TARGET_NAME TARGET_ENABLED)
    if (${TARGET_ENABLED})
        set_target_properties(${TARGET_NAME} PROPERTIES EXCLUDE_FROM_ALL 0 EXCLUDE_FROM_DEFAULT_BUILD 0)
        add_feature_info(${TARGET_NAME} TRUE "")
        # the item needs to be removed in a temporary variable since the REMOVE_ITEM operation is not possible on a CACHE variable
        set(TEMP_DEPENDENCY_LIST ${ARMARX_PROJECT_DISABLED_TARGETS})
        list(LENGTH TEMP_DEPENDENCY_LIST DEPENDENCIES_AVAILABLE)
        if (DEPENDENCIES_AVAILABLE)
            list(REMOVE_ITEM TEMP_DEPENDENCY_LIST ${TARGET_NAME})
            set(ARMARX_PROJECT_DISABLED_TARGETS "${TEMP_DEPENDENCY_LIST}" CACHE INTERNAL "Disabled Targets from dependent ArmarX Packages")
        endif()
    else()
        set_target_properties(${TARGET_NAME} PROPERTIES EXCLUDE_FROM_ALL 1 EXCLUDE_FROM_DEFAULT_BUILD 1)
        add_feature_info(${TARGET_NAME} FALSE "")
        set(ARMARX_PROJECT_DISABLED_TARGETS "${ARMARX_PROJECT_DISABLED_TARGETS}" "${TARGET_NAME}" CACHE INTERNAL "Disabled Targets from dependent ArmarX Packages")
    endif()
endfunction()


function(armarx_add_library ARMARX_LIB_NAME SOURCES HEADERS DEPENDEND_LIBS)
    armarx_create_library("${ARMARX_LIB_NAME}" "${SOURCES}" "${HEADERS}" "${DEPENDEND_LIBS}" FALSE)
endfunction()


function(armarx_add_static_library ARMARX_LIB_NAME SOURCES HEADERS DEPENDEND_LIBS)
    armarx_create_library("${ARMARX_LIB_NAME}" "${SOURCES}" "${HEADERS}" "${DEPENDEND_LIBS}" TRUE)
endfunction()


function(armarx_create_library ARMARX_LIB_NAME SOURCES HEADERS DEPENDEND_LIBS BUILD_STATIC_LIB)
    # only build the library if ${SOURCES} is not empty
    # to do this get the length of the list and compare it to 0
    # see http://i61wiki.itec.uka.de/redmine/issues/47
    list(LENGTH SOURCES ARMARX_BUILD_LIBRARY)
    generate_statechart_headers("${HEADERS}")

    ARMARX_MESSAGE(STATUS "    Library ${ARMARX_LIB_NAME} :")
    printtarget("${HEADERS}" "${SOURCES}" "${DEPENDEND_LIBS}")

    # disable building library if any of the dependent libraries does not get built
    foreach(DEPENDENT_LIB ${DEPENDEND_LIBS})
        # check if library in current package does not get built
        get_target_property(EXCLUDE_FROM_INSTALL ${DEPENDENT_LIB} EXCLUDE_FROM_ALL)
        # check if library from dependent pacakge has been disabled
        list(FIND ARMARX_PROJECT_DISABLED_DEPENDENT_TARGETS ${DEPENDENT_LIB} DEPENDENCY_DISABLED)
        if (EXCLUDE_FROM_INSTALL)
            message(STATUS "    ${ARMARX_LIB_NAME} disabled because ${DEPENDENT_LIB} does not get built")
            set(ARMARX_BUILD_LIBRARY FALSE)
        elseif (NOT "${DEPENDENCY_DISABLED}" STREQUAL "-1")
            set(ARMARX_BUILD_LIBRARY FALSE)
            message(STATUS "    ${ARMARX_EXE_NAME} disabled because ${DEPENDENT_LIB} does not get built")
        endif()
    endforeach()



    option(DISABLE_LIBRARY_${ARMARX_LIB_NAME} "Disable  Library ${ARMARX_LIB_NAME}" OFF)

    # check if this component has been disabled via CMake option
    if ("${DISABLE_LIBRARY_${ARMARX_LIB_NAME}}")
        armarx_build_if("0" "Library disabled. Enable by calling cmake with -DDISABLE_LIBRARY_${ARMARX_LIB_NAME}=OFF.")
    endif()

    # set target properties which are then checked in library_settings()
    # to determine if the target should be installed or not
    # ARMARX_BUILD is set by armarx_set_target() armarx_component_set_name()
    # and can be disabled with armarx_build_if()
    if (NOT ${ARMARX_BUILD})
        set(ARMARX_BUILD_LIBRARY FALSE)
    endif()

    # the library target
    if (${BUILD_STATIC_LIB})
        add_library(${ARMARX_LIB_NAME} STATIC ${SOURCES} ${HEADERS} )
    else()
        add_library(${ARMARX_LIB_NAME} SHARED ${SOURCES} ${HEADERS})
    endif()
    if(TARGET ${ARMARX_LIB_NAME}_GENERATE_STATECHARTS)
        add_dependencies(${ARMARX_LIB_NAME} ${ARMARX_LIB_NAME}_GENERATE_STATECHARTS)
    endif()

    add_statechart_generation_dependency("${HEADERS}")

    set_target_properties(${ARMARX_LIB_NAME} PROPERTIES LINKER_LANGUAGE CXX)

    armarx_target_enable(${ARMARX_LIB_NAME} ${ARMARX_BUILD_LIBRARY})

    if (${ARMARX_BUILD_LIBRARY})
        library_settings("${ARMARX_LIB_NAME}" "${HEADERS}")
        target_link_ice(${ARMARX_LIB_NAME})
        target_link_libraries(${ARMARX_LIB_NAME} ${DEPENDEND_LIBS})
    endif()

    if(ARMARX_CMAKE_DEBUGGING)
        print_cmake_target_properties(${ARMARX_LIB_NAME})
        foreach(src_file ${SOURCES})
            print_cmake_source_properties(${src_file})
        endforeach(src_file)
    endif()
endfunction()


function(armarx_add_executable ARMARX_EXE_NAME ARMARX_EXE_SOURCES DEPENDENT_LIBRARIES)

    cmake_parse_arguments(ARG "NO_INSTALL" "" "" ${ARGN})


    list(LENGTH ARMARX_EXE_SOURCES ARMARX_BUILD_EXE)
    # disable building executable if any of the dependent libraries does not get built
    foreach(DEPENDENT_LIB ${DEPENDENT_LIBRARIES})
        # check if library in current package does not get built
        get_target_property(EXCLUDE_FROM_INSTALL ${DEPENDENT_LIB} EXCLUDE_FROM_ALL)
        # check if library from dependent pacakge has been disabled
        list(FIND ARMARX_PROJECT_DISABLED_DEPENDENT_TARGETS ${DEPENDENT_LIB} DEPENDENCY_DISABLED)
        if (EXCLUDE_FROM_INSTALL)
            set(ARMARX_BUILD_EXE FALSE)
            message(STATUS "    ${ARMARX_EXE_NAME} disabled because ${DEPENDENT_LIB} does not get built")
        elseif (NOT "${DEPENDENCY_DISABLED}" STREQUAL "-1")
            set(ARMARX_BUILD_EXE FALSE)
            message(STATUS "    ${ARMARX_EXE_NAME} disabled because ${DEPENDENT_LIB} does not get built")
        endif()
    endforeach()

    add_executable(${ARMARX_EXE_NAME} ${ARMARX_EXE_SOURCES})
    set_target_properties(${ARMARX_EXE_NAME} PROPERTIES LINKER_LANGUAGE CXX)

    # set target properties which are then checked in executable_settings()
    # to determine if the target should be installed or not
    # ARMARX_BUILD is set by armarx_set_target() armarx_component_set_name()
    # and can be disabled with armarx_build_if()
    if (NOT ${ARMARX_BUILD})
        set(ARMARX_BUILD_EXE FALSE)
    endif()
    armarx_target_enable(${ARMARX_EXE_NAME} ${ARMARX_BUILD_EXE})

    if (${ARMARX_BUILD_EXE})
        executable_settings("${ARMARX_EXE_NAME}")
        target_link_ice("${ARMARX_EXE_NAME}")
        target_link_libraries("${ARMARX_EXE_NAME}" ${DEPENDENT_LIBRARIES})

        if(NOT ${ARG_NO_INSTALL})
            executable_install("${ARMARX_EXE_NAME}")
        endif()
    endif()

    if(ARMARX_CMAKE_DEBUGGING)
        print_cmake_target_properties(${ARMARX_EXE_NAME})
        foreach(src_file ${ARMARX_EXE_SOURCES})
            print_cmake_source_properties(${src_file})
        endforeach(src_file)
    endif()
endfunction()

function(armarx_add_external_executable EXECTUABLE_PATH)
    get_filename_component(EXECTUABLE_NAME "${EXECTUABLE_PATH}" NAME)
    get_filename_component(EXECTUABLE_DIR "${EXECTUABLE_PATH}" DIRECTORY)
    file(COPY "${EXECTUABLE_PATH}" DESTINATION "${ARMARX_BIN_DIR}/${EXECTUABLE_DIR}" USE_SOURCE_PERMISSIONS)
    armarx_external_executable_install("${EXECTUABLE_DIR}/${EXECTUABLE_NAME}")

endfunction()

function(armarx_add_external_executable_directory EXECTUABLE_DIRECTORY_PATH)
    file(GLOB_RECURSE fileList ${EXECTUABLE_DIRECTORY_PATH})
    message("Copying files from ${EXECTUABLE_DIRECTORY_PATH} to build dir")
    foreach(filePath ${fileList})
        file(RELATIVE_PATH relativePath ${CMAKE_CURRENT_SOURCE_DIR} ${filePath})
        get_filename_component(EXECTUABLE_NAME "${relativePath}" NAME)
        get_filename_component(EXECTUABLE_DIR "${relativePath}" DIRECTORY)
        file(COPY "${filePath}" DESTINATION "${ARMARX_BIN_DIR}/${EXECTUABLE_DIR}" USE_SOURCE_PERMISSIONS)
        armarx_external_executable_install("${EXECTUABLE_DIR}/${EXECTUABLE_NAME}")
    endforeach()

endfunction()
