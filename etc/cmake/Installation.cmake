# macros for handling installation of all ArmarX Package resources

macro(export_project)
    # Export the package for use from the build-tree
    # (this registers the build-tree with a global CMake-registry)
    export(PACKAGE ${ARMARX_PROJECT_NAME})

    # all libs & exes are added to this
    set(ARMARX_PROJECT_LIBS "" CACHE INTERNAL "" FORCE)
    set(ARMARX_PROJECT_EXES "" CACHE INTERNAL "" FORCE)
    set(ARMARX_PROJECT_JARS "" CACHE INTERNAL "" FORCE)
    set(ARMARX_PROJECT_EXTERNAL_EXES "" CACHE INTERNAL "" FORCE)
endmacro()


macro(install_project)

    foreach(ARMARX_DEPENDENCY ${ARMARX_PROJECT_DEPENDENCIES})
        string(TOLOWER ${ARMARX_DEPENDENCY} ARMARX_DEPENDENCY)
        list(APPEND CPACK_DEBIAN_PACKAGE_RECOMMENDS "armarx-${ARMARX_DEPENDENCY}")
    endforeach()

    if("${PROJECT_NAME}" STREQUAL "ArmarXCore")
        list(REMOVE_ITEM CPACK_DEBIAN_PACKAGE_RECOMMENDS  "armarx-armarxcore")
    endif()

    string(REPLACE ";" ", " CPACK_DEBIAN_PACKAGE_RECOMMENDS "${CPACK_DEBIAN_PACKAGE_RECOMMENDS}")
    string(REPLACE ";" ", " CPACK_DEBIAN_PACKAGE_DEPENDS "${CPACK_DEBIAN_PACKAGE_DEPENDS}")

    include(CPack)

    set(PROJECT_NAME "${ARMARX_PROJECT_NAME}")
    set(PROJECT_VERSION "${ARMARX_PACKAGE_LIBRARY_VERSION}")
    set(PROJECT_VERSION_MAJOR "${ARMARX_PACKAGE_LIBRARY_VERSION_MAJOR}")
    set(PROJECT_VERSION_MINOR "${ARMARX_PACKAGE_LIBRARY_VERSION_MINOR}")
    set(PROJECT_VERSION_PATCH "${ARMARX_PACKAGE_LIBRARY_VERSION_PATCH}")

    # generate file for checking the version number of the current ArmarX Package
    configure_file(${ArmarXCore_TEMPLATES_DIR}/cmake/ProjectConfigVersion.cmake.in
                   "${PROJECT_BINARY_DIR}/${PROJECT_NAME}ConfigVersion.cmake"
                   @ONLY)

    set(PROJECT_LIBS "${ARMARX_PROJECT_LIBS}")
    set(PROJECT_EXES "${ARMARX_PROJECT_EXES}")
    list(LENGTH ARMARX_PROJECT_EXTERNAL_EXES Size)
    if(Size GREATER 0)
        set(PROJECT_EXES "${PROJECT_EXES} ${ARMARX_PROJECT_EXTERNAL_EXES}")
    endif()
    set(PROJECT_JARS "${ARMARX_PROJECT_JARS}")
    set(PROJECT_INTERFACE_LIBRARY "${ARMARX_PROJECT_INTERFACE_LIBRARY}")
    set(PROJECT_DEPENDENCIES ${ARMARX_PROJECT_DEPENDENCIES})
    set(PROJECT_SOURCE_PACKAGE_DEPENDENCIES ${ARMARX_PROJECT_SOURCE_PACKAGE_DEPENDENCIES})
    set(PROJECT_PACKAGE_DEPENDENCY_PATHS ${ARMARX_PROJECT_PACKAGE_DEPENDENCY_PATHS})
    set(PROJECT_DISABLED_TARGETS ${ARMARX_PROJECT_DISABLED_TARGETS})
    if("${PROJECT_NAME}" STREQUAL "ArmarXCore")
        list(REMOVE_ITEM PROJECT_DEPENDENCIES "ArmarXCore")
    endif()
    list(LENGTH PROJECT_SOURCE_PACKAGE_DEPENDENCIES Size)
    if("${PROJECT_NAME}" STREQUAL "ArmarXCore" AND Size GREATER 0 )
        list(REMOVE_ITEM PROJECT_SOURCE_PACKAGE_DEPENDENCIES "ArmarXCore")
    endif()

    list(LENGTH ARMARX_MISSING_PROJECT_DEPENDENCIES MISSING_DEPENDENCY_COUNT)
    if (${MISSING_DEPENDENCY_COUNT} GREATER 0)
        configure_file(${ArmarXCore_TEMPLATES_DIR}/cmake/ProjectConfigMissingDeps.cmake.in
                       "${PROJECT_BINARY_DIR}/${PROJECT_NAME}Config.cmake"
                       @ONLY)
        message(STATUS "Required Dependencies not found")
        printlist("   " "${ARMARX_MISSING_PROJECT_DEPENDENCIES}")
        message(FATAL_ERROR "")
    endif()

    # check if any executables or libraries were built and should be installed
    # variable must be checked before calling export()
    # which produces errors and aborts the CMake process if it encounters unbuilt targets
    set(INSTALL_TARGETS True)
    if("${ARMARX_PROJECT_EXES}" STREQUAL "")
        if("${ARMARX_PROJECT_LIBS}" STREQUAL "")
            set(INSTALL_TARGETS False)
        endif()
    endif()

    # Add targets to the build-tree export set
    # first the string must be split into a list before it can be exported
    string(REPLACE " " ";" TARGET_LIST "${ARMARX_PROJECT_LIBS} ${ARMARX_PROJECT_EXES}")

    if(INSTALL_TARGETS)
        export(TARGETS ${TARGET_LIST}
               FILE "${PROJECT_BINARY_DIR}/${ARMARX_PROJECT_NAME}LibraryDepends.cmake")
    endif()

    #
    # Create a ${PROJECT_NAME}BuildTreeSettings.cmake file for the use from the build tree
    #
    set(PROJECT_INCLUDE_DIRS "${PROJECT_INCLUDE_DIRECTORIES}")
    set(PROJECT_LIB_DIR "${ARMARX_LIB_DIR}")
    set(PROJECT_BIN_DIR "${ARMARX_BIN_DIR}")
    set(PROJECT_CMAKE_DIR "${${ARMARX_PROJECT_NAME}_CMAKE_DIR}")
    set(PROJECT_INTERFACE_DIRS "${PROJECT_SOURCECODE_DIR}")
    # set in SetupDirs.cmake
    set(PROJECT_SCENARIOS_DIR "${PROJECT_SCENARIOS_DIR}")
    set(PROJECT_DATA_DIR "${PROJECT_DATA_DIR}")
    set(PROJECT_TEMPLATES_DIR "${PROJECT_ETC_DIR}/templates")
    set(PROJECT_JAR_DIR "${PROJECT_BINARY_DIR}/lib")
    set(PROJECT_CONFIG_DIR "${PROJECT_BINARY_DIR}/config")
    set(PROJECT_ICEGRID_DIR "${PROJECT_BINARY_DIR}/icegrid")
    # defined in armarx_project macro in ArmarXCore/etc/ArmarXProject.cmake
    set(PROJECT_USE_FILE "${PROJECT_USE_FILE}")
    set(PROJECT_DOXYGEN_TAG_FILE "${DOXYGEN_DOCUMENTATION_DIR}/${DOXYGEN_DOCUMENTATION_HTML_OUTPUT_DIR_REL}/${PROJECT_NAME}.tag")

    configure_file(${ArmarXCore_TEMPLATES_DIR}/cmake/ProjectBuildTreeSettings.cmake.in
                   "${PROJECT_BINARY_DIR}/${PROJECT_NAME}BuildTreeSettings.cmake"
                   @ONLY)

    if (EXISTS "${PROJECT_DATA_DIR}")
        install(DIRECTORY "${PROJECT_DATA_DIR}"
            DESTINATION "share/${PROJECT_NAME}"
            USE_SOURCE_PERMISSIONS
            OPTIONAL
            COMPONENT data
            PATTERN ".gitkeep" EXCLUDE)
    endif()

    #
    # Create a ${PROJECT_NAME}Config.cmake file for the use from the install tree
    #
    set(PROJECT_INCLUDE_DIRS "include" "include/${ARMARX_PROJECT_NAME}/interface/cpp")
    set(PROJECT_LIB_DIR "lib")
    set(PROJECT_BIN_DIR "bin")
    set(PROJECT_CMAKE_DIR "share/${PROJECT_NAME}/cmake")
    set(PROJECT_INTERFACE_DIRS "share/${PROJECT_NAME}/slice")
    set(PROJECT_SCENARIOS_DIR "share/${PROJECT_NAME}/scenarios")
    set(PROJECT_DATA_DIR "share/${PROJECT_NAME}/data")
    set(PROJECT_TEMPLATES_DIR "share/${PROJECT_NAME}/templates")
    set(PROJECT_CONFIG_DIR "share/${PROJECT_NAME}/config")
    set(PROJECT_ICEGRID_DIR "share/${PROJECT_NAME}/icegrid")
    set(PROJECT_JAR_DIR "lib")

    # rewrite to use the PROJECT_CMAKE_DIR of the installed package
    set(PROJECT_USE_FILE "${PROJECT_CMAKE_DIR}/Use${ARMARX_PROJECT_NAME}.cmake")
    set(PROJECT_DOXYGEN_TAG_FILE "share/${PROJECT_NAME}/documentation/${PROJECT_NAME}.tag")

    file(RELATIVE_PATH RELATIVE_BASE_DIR
         "${CMAKE_INSTALL_PREFIX}/share/cmake/${PROJECT_NAME}"
         "${CMAKE_INSTALL_PREFIX}")
    configure_file(${ArmarXCore_TEMPLATES_DIR}/cmake/ProjectConfig.cmake.in
                   "${PROJECT_BINARY_DIR}/${PROJECT_NAME}Config.cmake"
                   @ONLY)

    get_property(val GLOBAL PROPERTY ARMARX_APPEND_TO_CONFIG_CMAKE_STRING)
    file(APPEND
        "${PROJECT_BINARY_DIR}/${PROJECT_NAME}Config.cmake"
        "${val}"
    )

    # Install the export set for use with the install-tree (belongs to the files generated above)
    if(INSTALL_TARGETS)
        install(EXPORT "${PROJECT_NAME}LibraryDepends"
                DESTINATION "share/cmake/${PROJECT_NAME}"
                COMPONENT cmake)
    endif()

    # Install the Config files into the install tree
    install(FILES
            "${PROJECT_BINARY_DIR}/${PROJECT_NAME}Config.cmake"
            "${PROJECT_BINARY_DIR}/${PROJECT_NAME}ConfigVersion.cmake"
            DESTINATION "share/cmake/${PROJECT_NAME}"
            COMPONENT cmake)

    # Install files located in etc/cmake/
    if (EXISTS "${${PROJECT_NAME}_CMAKE_DIR}")
        install(DIRECTORY "${${PROJECT_NAME}_CMAKE_DIR}"
            DESTINATION share/${PROJECT_NAME}
            COMPONENT cmake)
    endif()

    # Install templates
    if (EXISTS "${${ARMARX_PROJECT_NAME}_TEMPLATES_DIR}")
        install(DIRECTORY "${${ARMARX_PROJECT_NAME}_TEMPLATES_DIR}"
            DESTINATION share/${PROJECT_NAME}
            COMPONENT templates)
    endif()

    # Install generated config files
    if (EXISTS "${PACKAGE_CONFIG_DIR}")
        install(DIRECTORY "${PACKAGE_CONFIG_DIR}"
            DESTINATION share/${PROJECT_NAME}
            COMPONENT config)
    endif()

    # Install Ice interface files
    install(DIRECTORY "${PROJECT_SOURCECODE_DIR}/${ARMARX_PROJECT_NAME}/interface"
            DESTINATION "share/${PROJECT_NAME}/slice/${ARMARX_PROJECT_NAME}"
            COMPONENT interfaces
            FILES_MATCHING PATTERN "*.ice")

    # Install scenario configuration files
    if (EXISTS "${PROJECT_SOURCE_DIR}/scenarios")
        install(DIRECTORY "${PROJECT_SOURCE_DIR}/scenarios"
                DESTINATION "share/${PROJECT_NAME}"
                COMPONENT scenarios
                FILES_MATCHING
                    PATTERN "*.scx"
                    PATTERN "*.cfg"
                    PATTERN "*.icegrid.xml"
                    PATTERN "*.xml")
    endif()

    # Print feature summary here since this is the macro which needs to be executed
    # as the last statement in the toplevel CMakeLists.txt file
    feature_summary(WHAT ALL INCLUDE_QUIET_PACKAGES)
endmacro()


macro(executable_install EXE_NAME)
    # check if EXCLUDE_FROM_ALL has been set because of missing dependencies
    # and omit installation in that case
    get_target_property(EXCLUDE_FROM_INSTALL ${EXE_NAME} EXCLUDE_FROM_ALL)

    if (NOT EXCLUDE_FROM_INSTALL)
        install(TARGETS "${EXE_NAME}"
            EXPORT "${ARMARX_PROJECT_NAME}LibraryDepends"
            COMPONENT binaries
            RUNTIME DESTINATION bin )

        armarx_cache_append_value(ARMARX_PROJECT_EXES "${EXE_NAME}")
    endif()
endmacro()


macro(armarx_external_executable_install EXECTUABLE_NAME)
    armarx_cache_append_value(ARMARX_PROJECT_EXTERNAL_EXES "${EXECTUABLE_NAME}")
    install(PROGRAMS "${ARMARX_BIN_DIR}/${EXECTUABLE_NAME}"
        COMPONENT binaries
        DESTINATION bin )
endmacro()


macro(library_install LIB_NAME LIB_HEADERS HEADER_DIR)
    # check if EXCLUDE_FROM_ALL has been set because of missing dependencies
    # and omit installation in that case
    get_target_property(EXCLUDE_FROM_INSTALL ${LIB_NAME} EXCLUDE_FROM_ALL)

    if (NOT EXCLUDE_FROM_INSTALL)
        foreach (HEADER ${LIB_HEADERS})
            # strip HEADER_DIR, so that only subdirectory + filename remain
            string(REPLACE "${PROJECT_SOURCECODE_DIR}/${HEADER_DIR}" "" STRIP_HEADER "${HEADER}")
            # strip the binary path if present
            # required, so generated slice files to install into the correct location
            string(REPLACE "${CMAKE_BINARY_DIR}/source/${HEADER_DIR}" "" STRIP_HEADER "${HEADER}")
            # required for files from subdirectories
            get_filename_component(EXTRA_DIR "${STRIP_HEADER}" PATH)
            # install files into respective subdirectory in $PREFIX/include/
            install(FILES ${HEADER}
                COMPONENT headers
                DESTINATION "include/${HEADER_DIR}/${EXTRA_DIR}")
        endforeach()

        if (ARMARX_OS_WIN)
            install(TARGETS ${LIB_NAME}
                COMPONENT libraries
                DESTINATION lib)
        else()
            install(TARGETS ${LIB_NAME}
                EXPORT ${ARMARX_PROJECT_NAME}LibraryDepends
                COMPONENT libraries
                RUNTIME DESTINATION bin
                LIBRARY DESTINATION lib
                ARCHIVE DESTINATION lib
                PUBLIC_HEADER DESTINATION include/${ARMARX_PROJECT_NAME}/${LIB_NAME}
            )
        endif()
        armarx_cache_append_value(ARMARX_PROJECT_LIBS "${LIB_NAME}")
    endif()
endmacro()


macro(jar_install JAR_NAME)
#    install(TARGETS ${JAR_NAME}
#        EXPORT ${ARMARX_PROJECT_NAME}LibraryDepends
#        RUNTIME DESTINATION lib
#        COMPONENT java
#    )

    armarx_cache_append_value(ARMARX_PROJECT_JARS "${JAR_NAME}")
endmacro()
