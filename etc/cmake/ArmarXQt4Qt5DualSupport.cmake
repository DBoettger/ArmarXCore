

macro(armarx_build_if_not_qt5)
    if(ARMARX_USE_QT5)
        armarx_build_if(dummy_variable_to_disable "not build with qt5")
    endif()
endmacro()


macro(armarx_find_qt_base find_qt_base_libs find_qt_base_mode)
    if (ArmarXGui_FOUND)
        #qt 4 components are passed as QtCore
        #qt 5 components are passed as Core
        #setup some version dependent variables
        if(ARMARX_USE_QT5)
            set(find_qt_pkg_str "Qt5")          # the pkg is called by this name
            set(find_qt_found_var "Qt5_FOUND")  # this var is set if qt was found
            set(find_qt_component_pre "")       # this is the prefix for a qt component
            set(find_qt_component_replace       # these libs are removed from the components
            )
            set(find_qt_component_replacement   # these libs are replacements for the removed components
            )
        else()
            set(find_qt_pkg_str "Qt4")          # the pkg is called by this name
            set(find_qt_found_var "QT_FOUND")   # this var is set if qt was found
            set(find_qt_component_pre "Qt")     # this is the prefix for a qt component
            set(find_qt_component_replace       # these libs are removed from the components
                Widgets
            )
            set(find_qt_component_replacement  # these libs are replacements for the removed components
                "QtOpenGl QtGui"
            )
        endif()

        #rewrite all names to correct version with/without Qt prefix
        set(find_qt_components "")
        foreach(find_qt_component ${find_qt_base_libs})
            list(FIND find_qt_component_replace ${find_qt_component} find_qt_index)
            #apply replacements
            if(${find_qt_index} EQUAL -1)
                #no replacement -< only fix prefix
                string(REGEX REPLACE "^(Qt)+" "${find_qt_component_pre}" find_qt_component Qt${find_qt_component})
                list(APPEND find_qt_components "${find_qt_component}")
            else()
                #apply replacement
                list(GET find_qt_component_replacement ${find_qt_index} replacement)
                string(REPLACE " " ";" replacement "${replacement}")
                list(APPEND find_qt_components ${replacement})
            endif()
        endforeach(find_qt_component)

        #find qt package + components
        find_package(${find_qt_pkg_str} COMPONENTS ${find_qt_components} ${find_qt_base_mode})
        set(QT_FOUND ${${find_qt_found_var}})
        message(STATUS "find_package(${find_qt_pkg_str} COMPONENTS ${find_qt_components} QUIET) -> FOUND = ${QT_FOUND}")
        #setup libraries variable
        set(QT_LIBRARIES)
        set(QT_INCLUDE_DIRS)
        set(QT_DEFINITIONS)
        set(QT_COMPILE_DEFINITIONS)
        if(QT_FOUND)
            if(ARMARX_USE_QT5)
                foreach(var ${find_qt_components})
                    list(APPEND QT_LIBRARIES ${Qt5${var}_LIBRARIES})
                    list(APPEND QT_INCLUDE_DIRS ${Qt5${var}_INCLUDE_DIRS})
                    list(APPEND QT_DEFINITIONS ${Qt5${var}_DEFINITIONS})
                    list(APPEND QT_COMPILE_DEFINITIONS ${Qt5${var}_COMPILE_DEFINITIONS})
                endforeach(var)

                list(LENGTH QT_LIBRARIES           QT_LIBRARIES_LEN)
                list(LENGTH QT_INCLUDE_DIRS        QT_INCLUDE_DIRS_LEN)
                list(LENGTH QT_DEFINITIONS         QT_DEFINITIONS_LEN)
                list(LENGTH QT_COMPILE_DEFINITIONS QT_COMPILE_DEFINITIONS_LEN)

                if(NOT 0 EQUAL ${QT_LIBRARIES_LEN})
                    list(REMOVE_DUPLICATES QT_LIBRARIES)
                endif()

                if(NOT 0 EQUAL ${QT_INCLUDE_DIRS_LEN})
                    list(REMOVE_DUPLICATES QT_INCLUDE_DIRS)
                endif()

                if(NOT 0 EQUAL ${QT_DEFINITIONS_LEN})
                    list(REMOVE_DUPLICATES QT_DEFINITIONS)
                endif()

                if(NOT 0 EQUAL ${QT_COMPILE_DEFINITIONS_LEN})
                    list(REMOVE_DUPLICATES QT_COMPILE_DEFINITIONS)
                endif()
            else()
                include(${QT_USE_FILE})
                include_directories(SYSTEM  ${QT_INCLUDES})
            endif()
            list(REMOVE_DUPLICATES QT_LIBRARIES)
            link_libraries(${QT_LIBRARIES})
        endif()
    endif()
    armarx_build_if(QT_FOUND "Qt was not found")
endmacro()

macro(armarx_find_qt)
    armarx_find_qt_base("${ARGN}" QUIET)
endmacro()

macro(armarx_qt_add_resources)
    if (ArmarXGui_FOUND)
        if(ARMARX_USE_QT5)
            QT5_ADD_RESOURCES(${ARGN})
        else()
            QT4_ADD_RESOURCES(${ARGN})
        endif()
    endif()
endmacro(armarx_qt_add_resources)

macro(armarx_qt_wrap_cpp)
    if (ArmarXGui_FOUND)
        if(ARMARX_USE_QT5)
            qt5_wrap_cpp(${ARGN})
        else()
            qt4_wrap_cpp(${ARGN})
        endif()
    endif()
endmacro(armarx_qt_wrap_cpp)

macro(armarx_qt_wrap_ui)
    if (ArmarXGui_FOUND)
        if(ARMARX_USE_QT5)
            qt5_wrap_ui(${ARGN})
        else()
            qt4_wrap_ui(${ARGN})
        endif()
    endif()
endmacro(armarx_qt_wrap_ui)

macro(armarx_qt_add_translation)
    if (ArmarXGui_FOUND)
        if(ARMARX_USE_QT5)
            qt5_add_translation(${ARGN})
        else()
            QT4_ADD_TRANSLATION(${ARGN})
        endif()
    endif()
endmacro(armarx_qt_add_translation)
