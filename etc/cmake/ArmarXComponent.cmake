# Macros for ArmarX Framework Components

macro(armarx_component_set_name NAME)
    set(ARMARX_COMPONENT_NAME ${NAME})
    set(ARMARX_COMPONENT_LIB_NAME ${NAME})
    set(ARMARX_COMPONENT_EXE "${NAME}Run")
    set(ARMARX_COMPONENT_NODE "NodeMain") # default Ice Node

    armarx_set_target("Component: ${ARMARX_COMPONENT_NAME}")
endmacro()

function(armarx_add_component SOURCES HEADERS)
    # the library target
    armarx_add_library("${ARMARX_COMPONENT_LIB_NAME}" "${SOURCES}" "${HEADERS}" "${COMPONENT_LIBS}")

    # add the component library to COMPONENT_LIBS
    # to link it into the created executable
    set(COMPONENT_LIBS ${COMPONENT_LIBS} ${ARMARX_COMPONENT_LIB_NAME} PARENT_SCOPE)
endfunction()

function(armarx_add_component_executable SOURCES)
    ARMARX_MESSAGE(STATUS "    Building ${ARMARX_COMPONENT_EXE}")
    armarx_add_executable(${ARMARX_COMPONENT_EXE} "${SOURCES}" "${COMPONENT_LIBS}")
endfunction()

macro(armarx_add_component_test TEST_NAME TEST_FILE)
    set(TEST_LINK_LIBRARIES ${ARMARX_COMPONENT_LIB_NAME} ${COMPONENT_LIBS})
    armarx_add_test(${TEST_NAME} ${TEST_FILE} "${TEST_LINK_LIBRARIES}")
endmacro()
