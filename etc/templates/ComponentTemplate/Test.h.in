/*
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License version 2 as
* published by the Free Software Foundation.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @author     Nils Adermann (naderman at naderman dot de)
* @date       2010
* @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
*             GNU General Public License
*/

#ifndef @ARMARX_PROJECT_NAME@_TEST_H
#define @ARMARX_PROJECT_NAME@_TEST_H

#define BOOST_TEST_IGNORE_NON_ZERO_CHILD_CODE

#ifdef ARMARX_BOOST_TEST
    #ifdef WIN32
        #include <boost/test/included/unit_test.hpp>
    #else
        #define BOOST_TEST_DYN_LINK
        #include <boost/test/included/unit_test.hpp>
    #endif
#endif

#include <string>
#include <map>
#include <fstream>
#include <iostream>

#include <boost/iostreams/tee.hpp>
#include <boost/iostreams/stream.hpp>
#include <ArmarXCore/core/util/StringHelpers.h>

namespace testout
{
    /**
     * typedef for tee_device redirecting output stream to another output stream
     */
    typedef boost::iostreams::tee_device<std::ostream, std::ostream> ostream_tee_device;
    /**
     * typedef for tee_device to output stream redirection
     */
    typedef boost::iostreams::stream<ostream_tee_device> tee_ostream;
}

namespace armarx
{
    namespace test
    {
        std::map<std::string, std::string> readCMakeVars()
        {
            std::map<std::string, std::string> result;
            std::string test_H_Dir = "@ARMARX_TEST_H_DIRECTORY@/cmakevars.cfg";

            std::ifstream f(test_H_Dir);
            std::string line;
            while(std::getline(f,line))
            {
                size_t pos = line.find_first_of("=");
                if(pos != std::string::npos)
                {
                    result[line.substr(0, pos)] = line.substr(pos+1);
                }
            }
            return result;
        }

        std::string getCmakeValue(const std::string& varName)
        {
            static std::map<std::string, std::string> cmakeVars = readCMakeVars();

            return cmakeVars[varName];
        }


    }
}

/**
 * The class OutputConfiguration redirects the output of all
 * testcases into files located in @ARMARX_BIN_DIR@ as well as onto the console.
 */
struct OutputConfiguration
{
    /**
     * Setup for writing the output to file and redirect it to stout.
     */
    OutputConfiguration()
    {
        // The path where the tests are put into is taken from CMake
        std::string logFileName("@ARMARX_BIN_DIR@/");
        logFileName.append(boost::unit_test::framework::master_test_suite().p_name);
        logFileName.append(".xml");
        logFile.open(logFileName.c_str());

        tee = new testout::ostream_tee_device(std::cout, logFile);
        teeStream = new testout::tee_ostream(*tee);

        boost::unit_test::unit_test_log.set_stream(*teeStream);
    }

    /**
     * Flush the stream and append a tag missing from boost::unit_test framework.
     * Afterwards reset the output to stout.
     */
    ~OutputConfiguration()
    {
        // this line is required to write the final "</TestLog>" tag
        // after the testrun is complete
        boost::unit_test::unit_test_log.test_finish();
        boost::unit_test::unit_test_log.set_stream(std::cout);

        teeStream->flush();

        delete tee;
        delete teeStream;

        logFile.close();
    }
    /**
     * Tee device for redirecting the console output to a file specified in OutputConfiguration::logFile.
     */
    testout::ostream_tee_device* tee;
    /**
     * Tee device for redirecting the output of OutputConfiguration::tee to stdout.
     */
    testout::tee_ostream* teeStream;
    /**
     * Path to the output file into which the test results get written.
     */
    std::ofstream logFile;
};

#if  BOOST_VERSION  < 106200
    BOOST_GLOBAL_FIXTURE(OutputConfiguration)
#else
    BOOST_GLOBAL_FIXTURE(OutputConfiguration);
#endif

#endif
